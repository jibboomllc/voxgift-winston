#!/usr/bin/php
<?php
    // Copyright 2011-2012 VoxGift, Inc.
    // All rights reserved.
    
    const FNAME = "dictionary";
    const FEXT = "csv";
    
    function getTranslationDir($basedir, $lang) {
        if ($lang == "en") {
            return ($basedir . '/raw');
        } else {
            return ($basedir . '/raw-' . $lang);
        }
	}

	function parseStrings($file) {
		$lines = file($file);
		$multiline = "";
		foreach ($lines as $line) {
			
			// Empty lines
			$line = trim($line);
			if ($line == '') {
				//$out[]['type'] = 'empty';
			} else {
				$parts = explode(',', $line);
				$out[]['type'] = 'string';
				$out[count($out)-1]['name'] = $parts[0];
				$out[count($out)-1]['value'] = substr($line, strlen($parts[0])+1);
			}
		}
		return $out;
	}

    function createFiles($basedir, $lang) {
        $voices = array (
                         "en" => "Samantha" //"Vicki"
                         , "en_UK" => "Serena"
                         , "fr" => "Audrey"
                         , "es" => "Paulina"
                         , "de" => "Anna"
                         , "ja" => "Kyoko"
                         , "ko" => "Narae"
                         , "zh" => "Ting-Ting"
                         );
        
        $res_dir = getTranslationDir($basedir, $lang);
        $strings = parseStrings($res_dir . '/' . FNAME . '.' . FEXT);
        foreach ($strings as $string) {
            $command = "say -v " . $voices[$lang] . " -o " . $res_dir . "/" . urlencode($string['name']) . ".m4a --data-format=alac  \"" . str_replace(",", " ", $string['value']) . "\"";
            print($command . "\n");
            exec ($command);
            //break;
        }
    }
    
    /* main */
    
    if ($argc != 3 || in_array($argv[1], array('--help', '-help', '-h', '-?'))) : ?>

This is a command line PHP script with two options.

Usage:
<?php echo $argv[0]; ?> [basedir] [lang]

e.g. <?php echo $argv[0]; ?> ./winston/trunk/client/ios/WinstonLib/WinstonLibRes/res de

<?php else: createFiles($argv[1], $argv[2]); endif?>




