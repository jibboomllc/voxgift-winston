<?php
include_once('controllers/inputUtilities.php');
include_once('controllers/Request.php');
include_once('controllers/Message.php');

  $status = "error";

  if ($meta["action"] == "ack") {
    $empties = array("id", "device_id");
    $inputError = array("id"=>"", "device_id"=>"");
    if (validInput($meta["params"], $empties, $inputError)) {
      $request = new Request($meta["db"]);
      if ($request->ackRequest($meta["params"]["id"], $meta["params"]["device_id"])) {
        $status = "acknowledged";
      }
    }
    echo '<?xml version="1.0" encoding="utf-8"?>';
    echo '<submit_status value="'.$status.'"/>';

  } else if ($meta["action"] == "close") {
    $empties = array("id", "device_id");
    $inputError = array("id"=>"", "device_id"=>"");
    if (validInput($meta["params"], $empties, $inputError)) {
      $request = new Request($meta["db"]);
      if ($request->closeRequest($meta["params"]["id"], $meta["params"]["device_id"])) {
        $status = "closed";
      }
    }
    echo '<?xml version="1.0" encoding="utf-8"?>';
    echo '<submit_status value="'.$status.'"/>';

  } else if ($meta["action"] == "request") {
    $empties = array("req_device_id", "request");
    $inputError = array("req_device_id"=>"", "request"=>"");
    if (validInput($meta["params"], $empties, $inputError)) {
      $request = new Request($meta["db"]);
      if ($request->logRequest($meta["params"]["req_device_id"], $meta["params"]["request"])) {
        $status = "submitted";
      }
    }
    echo '<?xml version="1.0" encoding="utf-8"?>';
    echo '<submit_status value="'.$status.'"/>';

  } else if ($meta["action"] == "request_log") {
    // connect to the DB
    include('library/adodb5/adodb.inc.php'); // http://adodb.sourceforge.net/
    $connectString = "mysql://".$meta["db"]["user"].":".$meta["db"]["pwd"]."@".$meta["db"]["server"]."/".$meta["db"]["db"]."?persist";
    $DB = NewADOConnection($connectString);

    $rs = requestLog($DB, $meta["params"]);
    if ($rs) {
      include_once("library/pb4php/message/pb_message.php");
      include_once("pb/request.php");
      
      $log = new RequestLog();
      foreach ($rs as $row) {
        $request_entry = $log->add_request_entry();
        $request_entry->set_id($row["id"]);
        $request_entry->set_request($row["request"]);
        $request_entry->set_req_ts(strtotime($row["req_ts"]));
        $request_entry->set_ack_ts(strtotime($row["ack_ts"]));
        $request_entry->set_res_ts(strtotime($row["res_ts"]));
        $request_entry->set_ack_name($row["ack_name"]);
        $request_entry->set_res_name($row["res_name"]);
      }
      // serialize
      $string = $log->SerializeToString();
      header("Pragma: public"); // required 
      header("Expires: 0"); 
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
      header("Cache-Control: private",false); // required for certain browsers 
      header("Content-Type: application/octet-stream"); 
      header("Content-Transfer-Encoding: binary"); 
      header("Content-Length: ".strlen($string));
      ob_clean(); 
      flush();
      print($string);
      flush();
    }
  } else if ($meta["action"] == "open_requests") {
    // connect to the DB
    include('library/adodb5/adodb.inc.php'); // http://adodb.sourceforge.net/
    $connectString = "mysql://".$meta["db"]["user"].":".$meta["db"]["pwd"]."@".$meta["db"]["server"]."/".$meta["db"]["db"]."?persist";
    $DB = NewADOConnection($connectString);

    $rs = openRequests($DB, $meta["params"]);
    if ($rs) {
      include_once("library/pb4php/message/pb_message.php");
      include_once("pb/request.php");
      
      $log = new RequestLog();
      foreach ($rs as $row) {
        $request_entry = $log->add_request_entry();
        $request_entry->set_id($row["id"]);
        $request_entry->set_requestor($row["requestor"]);
        $request_entry->set_request($row["request"]);
        $request_entry->set_req_ts(strtotime($row["req_ts"]));
        $request_entry->set_ack_ts(strtotime($row["ack_ts"]));
        $request_entry->set_ack_name($row["ack_name"]);
      }
      // serialize
      $string = $log->SerializeToString();
      header("Pragma: public"); // required 
      header("Expires: 0"); 
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
      header("Cache-Control: private",false); // required for certain browsers 
      header("Content-Type: application/octet-stream"); 
      header("Content-Transfer-Encoding: binary"); 
      header("Content-Length: ".strlen($string));
      ob_clean(); 
      flush();
      print($string);
      flush();
    }

  } else if ($meta["action"] == "message_log") {
    // connect to the DB
    include('library/adodb5/adodb.inc.php'); // http://adodb.sourceforge.net/
    $connectString = "mysql://".$meta["db"]["user"].":".$meta["db"]["pwd"]."@".$meta["db"]["server"]."/".$meta["db"]["db"]."?persist";
    $DB = NewADOConnection($connectString);

    $rs = messageLog($DB, $meta["params"]);
    if ($rs) {
      include_once("library/pb4php/message/pb_message.php");
      include_once("pb/message.php");
      
      $log = new MessageLog();
      foreach ($rs as $row) {
        $message_entry = $log->add_message_entry();
        $message_entry->set_id($row["id"]);
        $message_entry->set_topic($row["topic"]);
        $message_entry->set_note($row["note"]);
        $message_entry->set_create_ts(strtotime($row["create_ts"]));
      }
      // serialize
      $string = $log->SerializeToString();
      header("Pragma: public"); // required 
      header("Expires: 0"); 
      header("Cache-Control: must-revalidate, post-check=0, pre-check=0"); 
      header("Cache-Control: private",false); // required for certain browsers 
      header("Content-Type: application/octet-stream"); 
      header("Content-Transfer-Encoding: binary"); 
      header("Content-Length: ".strlen($string));
      ob_clean(); 
      flush();
      print($string);
      flush();
    }
  } else if ($meta["action"] == "status") {
    $empties = array("device_id", "on_duty", "availability");
    $inputError = array("device_id"=>"", "on_duty"=>"", "availability"=>"");
    if (validInput($meta["params"], $empties, $inputError)) {
      $request = new Request($meta["db"]);
      if ($request->updateResponderStatus($meta["params"]["device_id"], $meta["params"]["on_duty"], $meta["params"]["availability"])) {
        $status = "updated";
      }
    }
    echo '<?xml version="1.0" encoding="utf-8"?>';
    echo '<submit_status value="'.$status.'"/>';

  } else if ($meta["action"] == "pb_parse") {
    /* the following creates & d/l a pb file based on a proto file 
    * PLUS it creates from the proto file a pb_prot[NAME].php file
    * 
    * Java equivalent is something like:
    * C:\dev\voxgift\winston\trunk\server\php\private_html\pb>\dev\voxgift\winston\trunk\tools\protoc\protoc.exe --java_out=. request.proto
    */
 
    require_once('library/pb4php/parser/pb_parser.php');
    $test = new PBParser();
    print ($meta["path"].'/pb/request.proto');
    $test->parse($meta["path"].'/pb/request.proto', $meta["path"].'/pb/request.php');
    print ("B");
    $test->parse($meta["path"].'/pb/message.proto', $meta["path"].'/pb/message.php');
    print ("C");
  }
?>
