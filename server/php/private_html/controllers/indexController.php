<?php
include_once('controllers/inputUtilities.php');
include_once('controllers/Message.php');
include_once('controllers/Request.php');
include_once('library/SAM/php_sam.php');

// connect to the DB
include('library/adodb5/adodb.inc.php'); // http://adodb.sourceforge.net/
$connectString = "mysql://".$meta["db"]["user"].":".$meta["db"]["pwd"]."@".$meta["db"]["server"]."/".$meta["db"]["db"]."?persist";
$DB = NewADOConnection($connectString);

// if we are changing departments, take care of it now
if ($meta["action"] == "changedepartment") {
  $meta["departmentid"] = $_POST["departmentid"];
  setcookie("departmentid", $_POST["departmentid"]);
  $meta["departmentname"] = null;
  $meta["action"] = "index";
}

// initialize some values
if (!array_key_exists("departmentname", $meta) || $meta["departmentname"] == null) {
  $rsDeptName = $DB->Execute("SELECT name FROM department WHERE id=?", array($meta["departmentid"]));
  $meta["departmentname"] = $rsDeptName->fields['name'];
}

$userInput = array("req_device_id"=>"bb3d1633c332870f", "request"=>"I am cold");
$inputError = array("req_device_id"=>"", "request"=>"");

if ($meta["action"] == "index") {
  $rsDept = $DB->Execute("
    SELECT id, name
    FROM department
    ORDER BY name ASC
    ");
} elseif ($meta["action"] == "newrequest") {
  $userInput = array_merge($userInput, $_POST);
  $empties = array("req_device_id", "request");
  $request = new Request($meta["db"]);
  
  if (validInput($userInput, $empties, $inputError) && $request->logRequest($userInput["req_device_id"], $userInput["request"])) {
    $meta["action"] = "submitted";
  } else {
    $meta["action"] = "error";
  }

} elseif ($meta["action"] == "openrequestboard") {
  if (array_key_exists("action", $_REQUEST)){ 
    $request = new Request($meta["db"]);
    if ($_REQUEST["action"] == "ack") {
      $request->ackRequest($_REQUEST["id"], $_REQUEST["ackDeviceId"]);
    } elseif ($_REQUEST["action"] == "close") {
      $request->closeRequest($_REQUEST["id"], $_REQUEST["resDeviceId"]);
    }
  }
  $columns = array('Requested', 'Patient', 'Request');
  $rs = $DB->Execute("
    SELECT rl.id AS id
    , rl.req_ts AS Requested
    , p.uname AS Patient
    , p.id AS RequestorID
    , rl.request AS Request
    , rl.ack_ts AS ack_ts
    , cg.uname AS ack_name
    FROM request_log AS rl
      RIGHT JOIN patient_device_assignment AS pda
      ON rl.req_device_id = pda.device_id
        AND rl.req_ts >= pda.create_ts
        AND pda.delete_ts = 0
      LEFT JOIN patient AS p
      ON pda.patient_id = p.id
      LEFT JOIN caregiver_device_assignment AS cgda
      ON rl.ack_device_id = cgda.device_id
      LEFT JOIN caregiver AS cg
      ON cgda.caregiver_id = cg.id
    WHERE rl.res_ts=0
      AND p.department_id = ?
    ORDER BY rl.id DESC
        ", array($meta["departmentid"]));
  
} elseif ($meta["action"] == "patientboard"
          || $meta["action"] == "newnote") {
  $inputError = array("topic"=>"", "note"=>"");
  if (array_key_exists("params", $meta) && array_key_exists("patient_id", $meta["params"])) {
    if ($meta["params"]["patient_id"] == "none") {
      $columns = array('requestor'=>'Patient', 'start_date'=>'Admit Date', 'end_date'=>'Discharge Date');
      $rs = $DB->Execute("
        SELECT p.id AS requestor_id
        , p.uname AS requestor
        , p.create_ts AS start_date
        , if(p.delete_ts>0, p.delete_ts, '') AS end_date
        FROM patient AS p
        WHERE p.department_id = ?
        ORDER BY requestor ASC
        ", array($meta["departmentid"]));
    } else {

      if ($meta["action"] == "newnote") {
        $userInput = array_merge($userInput, $_POST);
        $empties = array("patient_id", "topic");
        $message = new Message($meta["db"]);
        
        if (validInput($userInput, $empties, $inputError)) {
          if ($message->createMessage($userInput["patient_id"], $userInput["topic"], $userInput["note"])) {
            $meta["status"] = "Note saved";
          } else {
            $meta["status"] = "Error saving";
          }
        } else {
          $meta["status"] = "Input Error";
        }
        $meta["action"] = "patientboard";
      } else {
        $meta["status"] = "";
      }

      $rs0 = $DB->Execute("
        SELECT id, uname, create_ts, delete_ts
        FROM patient WHERE id = ?
        ",  array($meta["params"]["patient_id"]));
      
      $columns1 = array('device_id'=>'Device ID', 'device_name'=>'Device Name'
                       , 'create_ts' => 'Activated', 'checkin_ts'=>'Last Checkin');
      $rs1 = $DB->Execute("
        SELECT pda.id
        , pda.device_id
        , pda.device_name
        , pda.create_ts
        , pda.checkin_ts
        FROM patient_device_assignment AS pda
        WHERE patient_id = ?
        ", array($meta["params"]["patient_id"]));

      $columns2 = array('create_ts'=>'Date/Time', 'topic'=>'Topic'
                       , 'note' => 'Note');
      $rs2 = $DB->Execute("
        SELECT pn.id
        , pn.topic
        , pn.note
        , pn.create_ts
        FROM patient_note AS pn
        WHERE patient_id = ?
        ", array($meta["params"]["patient_id"]));

      $columns = array('id'=>'Id', 'req_ts'=>'Requested', 'request'=>'Request'
                       , 'ack_ts'=>'Acknowledged', 'ack_name'=>'Acknowledged by'
                       , 'res_ts'=>'Closed', 'res_name'=>'Closed by');
      $rs = $DB->Execute("
        SELECT rl.id AS id
        , rl.request AS request
        , rl.req_ts AS req_ts
        , if(rl.ack_ts>0, rl.ack_ts, '') AS ack_ts
        , cg1.uname AS ack_name
        , if(rl.res_ts>0, rl.res_ts, '') AS res_ts
        , cg2.uname AS res_name
        FROM request_log AS rl
          RIGHT JOIN patient_device_assignment AS pda
            ON rl.req_device_id = pda.device_id
          LEFT JOIN caregiver_device_assignment AS cgda1
            ON rl.ack_device_id = cgda1.device_id
          LEFT JOIN caregiver AS cg1
            ON cgda1.caregiver_id = cg1.id
          LEFT JOIN caregiver_device_assignment AS cgda2
            ON rl.res_device_id = cgda2.device_id
          LEFT JOIN caregiver AS cg2
            ON cgda2.caregiver_id = cg2.id
        WHERE pda.patient_id = ?
          AND rl.req_ts >= pda.create_ts
          AND (rl.req_ts <= pda.delete_ts
            OR pda.delete_ts = 0)
        ORDER BY rl.id DESC
        ", array($meta["params"]["patient_id"]));
    }
  } else {
      $columns = array('requestor'=>'Patient', 'start_date'=>'Admit Date');
      $rs = $DB->Execute("
        SELECT p.id AS requestor_id
        , p.uname AS requestor
        , p.create_ts AS start_date
        , p.delete_ts AS end_date
        FROM patient AS p
        WHERE p.department_id = ?
          AND p.delete_ts = 0
        ORDER BY requestor ASC
        ", array($meta["departmentid"]));
  }

} elseif ($meta["action"] == "staffboard") {
  if (array_key_exists("params", $meta) && array_key_exists("filter", $meta["params"])) {
    if ($meta["params"]["filter"] == "none") {
      $columns = array('cg_name'=>'Name', 'cg_on_duty' => 'On Duty', 'cg_availability'=>'Availability');
      $rs = $DB->Execute("
        SELECT cg.id AS cg_id
        , cg.uname AS cg_name
        , IF(cg.on_duty, 'Yes', 'No') AS cg_on_duty
        , a.name AS cg_availability
        FROM caregiver AS cg
        RIGHT JOIN availability AS a
        ON cg.availability_id = a.id
        WHERE cg.department_id = ?
        ORDER BY cg_name ASC
        ", array($meta["departmentid"]));
    } else {
      $columns0 = array('cg_name'=>'Name', 'cg_department'=>'Department', 'cg_on_duty'=>'On Duty', 'cg_availability'=>'Availability');
      $rs0 = $DB->Execute("
        SELECT cg.id AS cg_id
        , cg.uname AS cg_name
        , d.name AS cg_department
        , IF(cg.on_duty, 'Yes', 'No') AS cg_on_duty
        , a.name AS cg_availability
        FROM caregiver AS cg
        RIGHT JOIN availability AS a
        ON cg.availability_id = a.id
        RIGHT JOIN department AS d
        ON cg.department_id = d.id
        WHERE cg.id = ?
        ", array($meta["params"]["filter"]));

      $columns = array('device_id'=>'Device ID', 'device_name'=>'Device Name'
                       , 'create_ts' => 'Activated', 'checkin_ts'=>'Last Checkin');
      $rs = $DB->Execute("
        SELECT cgda.id
        , cgda.device_id
        , cgda.device_name
        , cgda.create_ts
        , cgda.checkin_ts
        FROM caregiver_device_assignment AS cgda
        WHERE cgda.caregiver_id = ?
          AND cgda.delete_ts = 0
        ", array($meta["params"]["filter"]));
    }
  } else {
    $columns = array('cg_name'=>'Name', 'cg_availability'=>'Availability');
    $rs = $DB->Execute("
      SELECT cg.id AS cg_id
      , cg.uname AS cg_name
      , a.name AS cg_availability
      FROM caregiver AS cg
      RIGHT JOIN availability AS a
      ON cg.availability_id = a.id
      WHERE cg.department_id = ?
      AND cg.on_duty = TRUE
      ORDER BY cg_name ASC
      ", array($meta["departmentid"]));
  }
}

if (array_key_exists("params", $meta) && array_key_exists("refresh", $meta["params"]) &&  $meta["params"]["refresh"] == true) {
  $meta["view"] = "views/".$meta["controller"]."/content/".$meta["action"].".phtml";
  include_once($meta["view"]);
} else {
  $meta["view"] = "views/".$meta["controller"]."/".$meta["action"].".phtml";
  include_once("layouts/layout.phtml");
}

?>
