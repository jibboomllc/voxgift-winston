<?php

function validInput($input, $empties, &$error) {
  $returnValue = TRUE;

  foreach ($empties as $empty) {
    if (!array_key_exists($empty, $input) || !strlen($input[$empty])) {
      $returnValue = FALSE;
      $error[$empty]="Required";
      }
  }
  return $returnValue;
}

// Request log functions

// get for the given time period
function getLog($DB, $deviceID, $startTime, $endTime, $limit) {
  $rs = $DB->SelectLimit("
    SELECT rl.id , rl.request, rl.req_ts
     , if(rl.ack_ts>0, rl.ack_ts, '') AS ack_ts
     , if(rl.res_ts>0, rl.res_ts, '') AS res_ts
     , cg1.uname AS ack_name
     , cg2.uname AS res_name
     FROM request_log AS rl
      LEFT JOIN caregiver_device_assignment AS cgda1
      ON rl.ack_device_id = cgda1.device_id
      LEFT JOIN caregiver AS cg1
      ON cgda1.caregiver_id = cg1.id
      LEFT JOIN caregiver_device_assignment AS cgda2
      ON rl.res_device_id = cgda2.device_id
      LEFT JOIN caregiver AS cg2
      ON cgda2.caregiver_id = cg2.id
     WHERE rl.req_device_id = '".$deviceID."'
        AND rl.req_ts >= '".$startTime."'
     ORDER BY rl.id DESC
     ", $limit, 0);
  
  return $rs;
  //       AND (rl.req_ts <= ".$endTime."
}

function requestLog($DB, $input) {
  if (array_key_exists("device_id", $input)){
    $deviceID = $input["device_id"];
    if (array_key_exists("limit", $input)){
      $limit = $input["limit"];
    } else {
      $limit = 16;
    }
    $rs = $DB->Execute("
      SELECT pda.create_ts
      FROM patient_device_assignment AS pda
      WHERE pda.device_id = ?
        AND pda.delete_ts = 0"
      , array($deviceID));
  
    if ($rs) {
      //echo "<pre>"; print_r($rs); echo"</pre>";
      $startTime = $rs->fields['create_ts'];
      return getLog($DB, $deviceID, $startTime, 0, $limit);
    }
  }
  return false;
}

function openRequests($DB, $input) {
  // FIXME add lotsa error checking
  if (array_key_exists("device_id", $input)){
    $deviceID = $input["device_id"];

    $rs0 = $DB->Execute("
      SELECT cg.department_id
      FROM caregiver_device_assignment AS cgda
      RIGHT JOIN caregiver AS cg
      ON cgda.caregiver_id = cg.id
      WHERE cgda.device_id = ?
        AND cgda.delete_ts = 0"
      , array($deviceID));
  
    if ($rs0) {
      $departmentID = $rs0->fields['department_id'];
      if (array_key_exists("limit", $input)){
        $limit = $input["limit"];
      } else {
        $limit = 16;
      }
      $rs = $DB->SelectLimit("
        SELECT rl.id AS id
        , rl.req_ts AS req_ts
        , if(rl.ack_ts>0, rl.ack_ts, '') AS ack_ts
        , p.uname AS requestor
        , rl.request AS request
        , cg.uname AS ack_name
        FROM request_log AS rl
        RIGHT JOIN patient_device_assignment AS pda
        ON rl.req_device_id = pda.device_id
          AND rl.req_ts >= pda.create_ts
          AND pda.delete_ts = 0
        LEFT JOIN patient AS p
        ON pda.patient_id = p.id
        LEFT JOIN caregiver_device_assignment AS cgda
        ON rl.ack_device_id = cgda.device_id
        LEFT JOIN caregiver AS cg
        ON cgda.caregiver_id = cg.id
        WHERE rl.res_ts=0
          AND p.department_id = '".$departmentID."'
        ORDER BY rl.id DESC
        ", $limit, 0);
      return $rs;
    }
  }
  return false;
}

function messageLog($DB, $input) {
  if (array_key_exists("device_id", $input)){
    $deviceID = $input["device_id"];
    if (array_key_exists("limit", $input)){
      $limit = $input["limit"];
    } else {
      $limit = 16;
    }
    
    $rs0 = $DB->Execute("
      SELECT
        pda.patient_id
        , pda.create_ts
      FROM patient_device_assignment AS pda
      WHERE pda.device_id = ?
        AND pda.delete_ts = 0"
      , array($deviceID));
//var_dump($rs0); 
    if ($rs0) {
      $rs = $DB->SelectLimit("
        SELECT pt.id AS id
        , pt.create_ts AS create_ts
        , pt.topic AS topic
        , pt.note AS note
        FROM patient_note AS pt
        WHERE pt.patient_id = '".$rs0->fields['patient_id']."'
        ", $limit, 0);
//var_dump($rs);
      return $rs;
    }
  }
  return false;
}


?>