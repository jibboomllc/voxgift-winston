<?php
// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

include_once('library/adodb5/adodb.inc.php');
include_once('library/SAM/php_sam.php');

class Request {

  protected $mDb;

  public function __construct($db) {
    $connectString = "mysql://".$db["user"].":".$db["pwd"]."@".$db["server"]."/".$db["db"]."?persist";
    $this->mDb = NewADOConnection($connectString);
  }

  public function ackRequest($id, $ackDeviceId) {
    // FIXME add lotsa error checking
    $rs = $this->mDb->Execute("
        UPDATE request_log
        SET ack_ts=NOW()
        , ack_device_id=?
        WHERE id =?"
      , array($ackDeviceId, $id));
  
    $requestInfo = $this->mDb->Execute("
      SELECT req_device_id
      , request
      FROM request_log 
      WHERE id=?"
      , array($id));

    $conn = new SAMConnection();
    if($conn) {
      if($conn->connect(SAM_MQTT, array('SAM_HOST' => '127.0.0.1', 'SAM_PORT' => 1883))) {
        $msgCpu = new SAMMessage($this->getRespondorsName($ackDeviceId)." acknowledges your request: ".$requestInfo->fields['1']);
        $conn->send('topic://wins/'.$requestInfo->fields['0'], $msgCpu);

        $msgCpu = new SAMMessage($this->getRespondorsName($ackDeviceId)." acknowledged ".$this->getRequestorsName($requestInfo->fields['0'])."'s request: ".$requestInfo->fields['1']);
        $responderIds = $this->getResponderIds($requestInfo->fields['0']);
        foreach ($responderIds as $responderId) {
          if ($responderId != $ackDeviceId) {
            $conn->send('topic://wins/'.$responderId, $msgCpu);
          }
        }

        $conn->disconnect();
      }
    }
    return $rs;
  }
  
  public function closeRequest($id, $resDeviceId) {
    $rs = $this->mDb->Execute("
        UPDATE request_log
        SET res_ts=NOW()
        , res_device_id=?
        WHERE id =?"
      , array($resDeviceId, $id));

    $requestInfo = $this->mDb->Execute("
      SELECT req_device_id
      , request
      FROM request_log 
      WHERE id=?"
      , array($id));

    $conn = new SAMConnection();
    if($conn) {
      if($conn->connect(SAM_MQTT, array('SAM_HOST' => '127.0.0.1', 'SAM_PORT' => 1883))) {
        $msgCpu = new SAMMessage($this->getRespondorsName($resDeviceId)." closed ".$this->getRequestorsName($requestInfo->fields['0'])."'s request: ".$requestInfo->fields['1']);
        $responderIds = $this->getResponderIds($requestInfo->fields['0']);
        foreach ($responderIds as $responderId) {
          if ($responderId != $resDeviceId) {
            $conn->send('topic://wins/'.$responderId, $msgCpu);
          }
        }


        $conn->disconnect();
      }
    }

    return $rs;
  }
  
  public function logRequest($reqDeviceId, $request) {
    $rs = $this->mDb->Execute("
        INSERT INTO request_log
        SET req_device_id=?
        , request=?"
      , array($reqDeviceId, $request));

    $conn = new SAMConnection();
    if($conn) {
      if($conn->connect(SAM_MQTT, array('SAM_HOST' => '127.0.0.1', 'SAM_PORT' => 1883))) {
        $msgCpu = new SAMMessage($this->getRequestorsName($reqDeviceId)." requests: ".$request);
        $ids = $this->getResponderIds($reqDeviceId);
        foreach ($ids as $id) {
          $conn->send('topic://wins/'.$id, $msgCpu);
        }
        $conn->disconnect();
      }
    }

    return $rs;
  }

  public function getRequestorsName($reqDeviceId) {
    $requestorInfo = $this->mDb->Execute("
      SELECT p.uname
      FROM patient AS p
      WHERE p.id IN (
        SELECT pda.patient_id
        FROM patient_device_assignment AS pda
        WHERE pda.device_id = ?
        AND pda.delete_ts = 0
        )
      ", array($reqDeviceId));
    return $requestorInfo->fields['0'];
  }

  public function getRespondorsName($ackDeviceId) {
  
    $deviceInfo = $this->mDb->Execute("
      SELECT cg.uname
      FROM caregiver_device_assignment AS cgda
      LEFT JOIN caregiver AS cg
      ON cgda.caregiver_id = cg.id
      WHERE cgda.device_id=?
      AND cgda.delete_ts = 0
    ", array($ackDeviceId));

    return $deviceInfo->fields['0'];
  }

  public function getResponderIds($reqDeviceId) {
    $ids = array();
    
    $cgdas = $this->mDb->Execute("
      SELECT cgda.device_id AS cgda_id
      FROM caregiver_device_assignment AS cgda
      RIGHT JOIN caregiver AS cg
      ON cgda.caregiver_id = cg.id
      WHERE cg.department_id IN (
        SELECT p.department_id
        FROM patient AS p
        WHERE p.id IN (
          SELECT pda.patient_id
          FROM patient_device_assignment AS pda
          WHERE pda.device_id = ?
          AND pda.delete_ts = 0
          )
        )
      AND cg.on_duty = '1'
      ", array($reqDeviceId));
    
    foreach ($cgdas as $cgda) {
      $ids[] = $cgda['cgda_id'];
    }
    
    return $ids;
  }

  public function updateResponderStatus($resDeviceId, $onDuty, $availability) {
    // FIXME add lotsa error checking
    $rs = $this->mDb->Execute("
        UPDATE caregiver AS cg
        SET cg.on_duty=?
        , cg.availability_id=?
        WHERE cg.id IN (
          SELECT cgda.caregiver_id
          FROM caregiver_device_assignment AS cgda
          WHERE cgda.device_id=?
          AND cgda.delete_ts = 0
        )
      ", array(($onDuty == 'true')?1:0, $availability, $resDeviceId));

    return $rs;
  }
  

}

?>
