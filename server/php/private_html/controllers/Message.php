<?php
// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

include_once('library/adodb5/adodb.inc.php');
include_once('library/SAM/php_sam.php');

class Message {

  protected $mDb;

  public function __construct($db) {
    $connectString = "mysql://".$db["user"].":".$db["pwd"]."@".$db["server"]."/".$db["db"]."?persist";
    $this->mDb = NewADOConnection($connectString);
  }

  public function createMessage($patientId, $topic, $note) {
    $rs = $this->mDb->Execute("
        INSERT INTO patient_note
        SET patient_id=?
        , topic=?
        , note=?"
      , array($patientId, $topic, $note));
    return $rs;
  }
}
?>
