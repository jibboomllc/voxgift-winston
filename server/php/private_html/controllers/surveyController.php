<?php
  $survey = null;
  $inputController = null;
  $resultsColumns = null;
  $resultsQuery = null;
  
  if (array_key_exists("params", $meta) && array_key_exists("survey", $meta["params"])) {
    $survey = $meta["params"]["survey"];
  } else {
    $survey = 'winston_feedback_v1'; //'diabetes_daily_v1'; // FIXME Un-hardcode this instead use db to look up and then put in session
  }

  $controller_action_file = $path."/controllers/survey/".$survey.".php"; // file that sets params, above
  if (file_exists($controller_action_file)) {
    include_once($controller_action_file);
  }  

if ($meta["action"] == "input") {
  if ($survey == null || $inputController == null) {
    print("404 or something");
  } else {
    $inputController->setConnectString("mysql://".$meta["db"]["user"].":".$meta["db"]["pwd"]."@".$meta["db"]["server"]."/".$meta["db"]["db"]."?persist");
  
    if (array_key_exists('language_code', $meta['params'])) {
      $inputController->setLanguageCode($meta['params']['language_code']);
    }
    if (array_key_exists('device_id', $meta['params'])) {
      $inputController->setDeviceID($meta['params']['device_id']);
    }

    if (array_key_exists('hw_manufacturer', $meta['params'])) {
      $inputController->setHwManufacturer($meta['params']['hw_manufacturer']);
    }
    if (array_key_exists('hw_model', $meta['params'])) {
      $inputController->setHwModel($meta['params']['hw_model']);
    }
    if (array_key_exists('os_version', $meta['params'])) {
      $inputController->setOsVersion($meta['params']['os_version']);
    }
    if (array_key_exists('app_version_name', $meta['params'])) {
      $inputController->setAppVersionName($meta['params']['app_version_name']);
    }
    if (array_key_exists('app_package_name', $meta['params'])) {
      $inputController->setAppPackageName($meta['params']['app_package_name']);
    }
    if (array_key_exists('lat', $meta['params'])) {
      $inputController->setLat($meta['params']['lat']);
    }
    if (array_key_exists('lon', $meta['params'])) {
      $inputController->setLon($meta['params']['lon']);
    }

    $currentPage = $inputController->newPage(array_merge($meta['params'], $_REQUEST));
    $baseUrl = "/survey/input/survey/".$survey."/page";
    $currentValue = $inputController->getCurrentValue();
    
    $meta["view"] = "views/survey/input/".$survey.".phtml";

    include_once("layouts/layoutEmbedded.phtml");  
  }

} else {
  // connect to the DB
  include_once('library/adodb5/adodb.inc.php'); // http://adodb.sourceforge.net/
  $connectString = "mysql://".$meta["db"]["user"].":".$meta["db"]["pwd"]."@".$meta["db"]["server"]."/".$meta["db"]["db"]."?persist";
  $DB = NewADOConnection($connectString);
  
  if ($meta["action"] == "index") {
    $rs = $DB->Execute("
      SELECT id, name, description
      FROM surveys
      ORDER BY name ASC
      ");
  } elseif ($meta["action"] == "results" && $survey != null) {
    $rs = $DB->Execute($inputController->getResultsQuery());
  } elseif ($meta["action"] == "questions" && array_key_exists("params", $meta) && array_key_exists("survey", $meta["params"])) {
    $survey = $meta["params"]["survey"];
  } else {
    print("DARN IT - Unsupported action: ". $meta["action"]);
  }
  
  if (array_key_exists("params", $meta) && array_key_exists("refresh", $meta["params"]) &&  $meta["params"]["refresh"] == true) {
    $meta["view"] = "views/".$meta["controller"]."/content/".$meta["action"].".phtml";
    include_once($meta["view"]);
  } else {
    $meta["view"] = "views/".$meta["controller"]."/".$meta["action"].".phtml";
    //print("View: ". $meta["view"]);
    include_once("layouts/layout.phtml");
  }
}
?>
