<?php
$terms = array (
  array (
    'term' => 'CareClient'
    , 'definition' => 'The application, typically WinstonCare, used to monitor and acknowledge requests.
    Supports language translation. Runs on an Android or iOS device like a smartphone or tablet.'
  )
  , array (
    'term' => 'Caregiver'
    , 'definition' => 'The person using a CareClient and Console.'
  )
  , array (
    'term' => 'Client'
    , 'definition' => 'The application, typically Winston or WinstonPro, used to make requests.
    Supports language translation. Runs on an Android or iOS device like a smartphone or tablet.'
  )
  , array (
    'term' => 'Console'
    , 'definition' => 'The browser-based user interface to the server. Supports resonding to requests.
    Displays request information (statistics, etc.) and includes a management interface.'
  )
  , array (
    'term' => 'Server'
    , 'definition' => 'A web server with database and message queue.'
  )
  , array (
    'term' => 'User'
    , 'definition' => 'The person using a Client.'
  )
);

$wsds = array (
  'talk_request' => array (
    'title' => 'Talk'
    , 'description' => 'A Spanish-speaking user is in a hospital (or hotel or wherever) in Germany. The user
         has Winston\'s input language set to Spanish and the output language set to German.
         The user needs Winston to tell the staff that he is cold.'
    , 'file' => 'talk.txt'
  )
  , 'page_request' => array (
    'title' => 'Page'
    , 'description' => 'A user is in a hospital on a ventilator and in pain.
    Nobody else is in the room. The user wants Winston to let the staff know that she needs meds.
    (For the next step, see <a href="#ack">page response</a>.)'
    , 'file' => 'page.txt'
  )
  , 'ack' => array (
    'title' => 'Page&nbsp;response'
    , 'description' => 'A nurse notices a patient requested meds and lets her know that he is on his way.
    (For details on the previous step, see <a href="#page">page</a>.)'
    , 'file' => 'ack.txt'
  )
  , 'feedback' => array (
    'title' => 'Protocol&nbsp;compliance&nbsp;monitoring'
    , 'description' => 'A newsly-diagnosed diabetes patient just received a bag full of meds and care
    instructions. To make sure that she is following protocols, the patient\'s Endocrinolgist wants
    to collect daily data on how she is taking care of herself.'
    , 'file' => 'diabetes_daily_v1.txt'
  )
);


if (array_key_exists("params", $meta) && array_key_exists("refresh", $meta["params"]) &&  $meta["params"]["refresh"] == true) {
  $meta["view"] = "views/".$meta["controller"]."/content/".$meta["action"].".phtml";
  include_once($meta["view"]);
} else {
  $meta["view"] = "views/".$meta["controller"]."/".$meta["action"].".phtml";
  include_once("layouts/layout.phtml");
}

?>
