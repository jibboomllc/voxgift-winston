<?php
include_once('controllers/survey/inputController.php'); // http://adodb.sourceforge.net/

class WinstonFeedbackController extends InputController {
  private $usage;
  private $requestedPhrase;
  private $requestedLanguage;
  private $generalFeedback;

  function __construct($name) {
    parent::__construct($name);
    $this->path .= '_feedback';

    $this->usage = '';
    $this->requestedPhrase = '';
    $this->requestedLanguage = '';
    $this->generalFeedback = '';

  }

  function readCookies() {
    parent::readCookies();

    if(isset($_SESSION[$this->path.'_usage'])) {
      $this->usage = $_SESSION[$this->path.'_usage'];
    }
    if(isset($_SESSION[$this->path.'_requestedPhrase'])) {
      $this->requestedPhrase = $_SESSION[$this->path.'_requestedPhrase'];
    }
    if(isset($_SESSION[$this->path.'_requestedLanguage'])) {
      $this->requestedLanguage = $_SESSION[$this->path.'_requestedLanguage'];
    }
    if(isset($_SESSION[$this->path.'_generalFeedback'])) {
      $this->generalFeedback = $_SESSION[$this->path.'_generalFeedback'];
    }
  }

  function writeCookies() {
    parent::writeCookies();

    $_SESSION[$this->path.'_usage'] = $this->usage;
    $_SESSION[$this->path.'_requestedPhrase'] = $this->requestedPhrase;
    $_SESSION[$this->path.'_requestedLanguage'] = $this->requestedLanguage;
    $_SESSION[$this->path.'_generalFeedback'] = $this->generalFeedback;
  }

////////
  function persistData() {
    $DB = NewADOConnection($this->connectString);

    //$DB->debug = true;
    $rs = $DB->Execute("
      INSERT INTO survey_winston_feedback_v1 (device_id, language_code
        , hw_manufacturer, hw_model, os_version
        , app_version_name, app_package_name, lat, lon
        , usage_fb, languages_fb, phrases_fb, general_fb
        ) VALUES (?, ?
        , ?, ?, ?
        , ?, ?, ?, ?
        , ?, ?, ?, ?
        )
      ", array($this->deviceId, $this->languageCode
        , $this->hwManufacturer, $this->hwModel, $this->osVersion
        , $this->appVersionName, $this->appPackageName, $this->lat, $this->lon
        , $this->usage, $this->requestedLanguage, $this->requestedPhrase, $this->generalFeedback
      ));

  }
  
  function getResultsColumns() {
    return (array('Submitted', 'Patient', 'Usage', 'Languages', 'Phrases', 'General'));
  }
  
  function getResultsQuery() {
    return ('
      SELECT swf.id AS id
      , swf.create_ts AS Submitted
      , swf.usage_fb AS "Usage"
      , swf.languages_fb AS Languages
      , swf.phrases_fb AS Phrases
      , swf.general_fb AS General
      , p.uname AS Patient
      FROM survey_winston_feedback_v1 AS swf
        LEFT JOIN patient_device_assignment AS pda
        ON swf.device_id = pda.device_id
        LEFT JOIN patient AS p
        ON pda.patient_id = p.id
      ORDER BY swf.id DESC
      ');
  }

  function getCurrentValue() {
    $currentValue = null;
    switch ($this->currentPage) {
      case 'usage':
        $currentValue = $this->usage;
        break;
      case 'requestedPhrase':
        $currentValue = $this->requestedPhrase;
        break;
      case 'requestedLanguage':
        $currentValue = $this->requestedLanguage;
        break;
      case 'generalFeedback':
        $currentValue = $this->generalFeedback;
        break;
      default:
        break;
    }
    return $currentValue;
  }

  function nextPage($params) {
    $newPage = null;
    switch ($this->currentPage) {
      case 'first':
        $newPage = 'usage';
        break;
      case 'usage':
        $this->usage = $params['usage'];
        $newPage = 'requestedPhrase';
        break;
      case 'requestedPhrase':
        $this->requestedPhrase = $params['requestedPhrase'];
        $newPage = 'requestedLanguage';
        break;
      case 'requestedLanguage':
        $this->requestedLanguage = $params['requestedLanguage'];
        $newPage = 'generalFeedback';
        break;
      case 'generalFeedback':
        $this->generalFeedback = $params['generalFeedback'];
        $newPage = 'submit';
        break;
      case 'submit':
        $this->persistData();
        $newPage = 'last';
        break;
      case 'last':
        $this->deleteCookies();
        $newPage = $this->firstPage;
        break;
      default:
        print ("DRATS -- case not defined on Next switch, case=".$this->currentPage);
        break;
    }
    return $newPage;
  }
}
?>
