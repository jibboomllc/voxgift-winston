<?php
include_once('library/adodb5/adodb.inc.php');

class InputController {
  protected $connectString;
  protected $languageCode;
  protected $deviceId;

  protected $hwManufacturer;
  protected $hwModel;
  protected $osVersion;
  protected $appVersionName;
  protected $appPackageName;
  protected $lat;
  protected $lon;

  protected $firstPage;
  protected $lastPage;

  protected $baseUrl;// = "/".$meta["controller"]."/".$meta["action"]."/page";
  protected $newPage;
  protected $currentPage;
  protected $prevPages;

  protected $name;
  protected $path;

  function __construct($name) {
    $this->connectString = null;
    $this->languageCode = 'en';
    $this->deviceId = '';

    $this->hwManufacturer = '';
    $this->hwModel = '';
    $this->osVersion = '';
    $this->appVersionName = '';
    $this->appPackageName = '';
    $this->lat = '0.0';
    $this->lon = '0.0';

    $this->firstPage = 'first';
    $this->lastPage = 'last';

    $this->baseUrl = null;
    $this->newPage = null;
    $this->currentPage = 'first';
    $this->prevPages = null;

    $this->name = $name;
    $this->path = 'com_voxgift_winston_survey';
    if(session_id() == '') {
      session_start();
    }
  }

  function readCookies() {
    if(isset($_SESSION[$this->path.'_languageCode'])) {
      $this->languageCode = $_SESSION[$this->path.'_languageCode'];
    }
    if(isset($_SESSION[$this->path.'_deviceId'])) {
      $this->deviceId = $_SESSION[$this->path.'_deviceId'];
    }

    if(isset($_SESSION[$this->path.'_hwManufacturer'])) {
      $this->hwManufacturer = $_SESSION[$this->path.'_hwManufacturer'];
    }
    if(isset($_SESSION[$this->path.'_hwModel'])) {
      $this->hwModel = $_SESSION[$this->path.'_hwModel'];
    }
    if(isset($_SESSION[$this->path.'_osVersion'])) {
      $this->osVersion = $_SESSION[$this->path.'_osVersion'];
    }
    if(isset($_SESSION[$this->path.'_appVersionName'])) {
      $this->appVersionName = $_SESSION[$this->path.'_appVersionName'];
    }
    if(isset($_SESSION[$this->path.'_appPackageName'])) {
      $this->appPackageName = $_SESSION[$this->path.'_appPackageName'];
    }
    if(isset($_SESSION[$this->path.'_lat'])) {
      $this->lat = $_SESSION[$this->path.'_lat'];
    }
    if(isset($_SESSION[$this->path.'_lon'])) {
      $this->lon = $_SESSION[$this->path.'_lon'];
    }


    if(isset($_SESSION[$this->path.'_currentPage'])) {
      $this->currentPage = $_SESSION[$this->path.'_currentPage'];
    }
    if(isset($_SESSION[$this->path.'_prevPages'])) {
      $this->prevPages = $_SESSION[$this->path.'_prevPages'];
    }
  }

  function writeCookies() {
    $_SESSION[$this->path.'_languageCode'] = $this->languageCode;
    $_SESSION[$this->path.'_deviceId'] = $this->deviceId;

    $_SESSION[$this->path.'_hwManufacturer'] = $this->hwManufacturer;
    $_SESSION[$this->path.'_hwModel'] = $this->hwModel;
    $_SESSION[$this->path.'_osVersion'] = $this->osVersion;
    $_SESSION[$this->path.'_appVersionName'] = $this->appVersionName;
    $_SESSION[$this->path.'_appPackageName'] = $this->appPackageName;
    $_SESSION[$this->path.'_lat'] = $this->lat;
    $_SESSION[$this->path.'_lon'] = $this->lon;

    $_SESSION[$this->path.'_currentPage'] = $this->currentPage;
    $_SESSION[$this->path.'_prevPages'] = $this->prevPages;
  }

  function deleteCookies() {
    session_destroy();
  }
/////////
  function getConnectString() {
    return $this->connectString;
  }

  function setConnectString($connectString) {
    $this->connectString = $connectString;
  }

  function getLanguageCode() {
    return $this->languageCode;
  }

  function setLanguageCode($languageCode) {
    $this->languageCode = $languageCode;
  }

  function getDeviceId() {
    return $this->deviceId;
  }

  function setDeviceId($deviceId) {
    $this->deviceId = $deviceId;
  }

  function getHwManufacturer() {
    return $this->hwManufacturer;
  }
  function setHwManufacturer($hwManufacturer) {
    $this->hwManufacturer = $hwManufacturer;
  }
  
  function getHwModel() {
    return $this->hwModel;
  }
  function setHwModel($hwModel) {
    $this->hwModel = $hwModel;
  }
  
  function getOsVersion() {
    return $this->osVersion;
  }
  function setOsVersion($osVersion) {
    $this->osVersion = $osVersion;
  }
  
  function getAppVersionName() {
    return $this->appVersionName;
  }
  function setAppVersionName($appVersionName) {
    $this->appVersionName = $appVersionName;
  }
  
  function getAppPackageName() {
    return $this->appPackageName;
  }
  function setAppPackageName($appPackageName) {
    $this->appPackageName = $appPackageName;
  }
  
  function getLat() {
    return $this->lat;
  }
  function setLat($lat) {
    $this->lat = $lat;
  }
  
  function getLon() {
    return $this->lon;
  }
  function setLon($lon) {
    $this->lon = $lon;
  }

  function getFirstPage() {
    return $this->firstPage;
  }

  function getLastPage() {
    return $this->lastPage;
  }

  function getBaseUrl() {
    return $this->baseUrl;
  }

  function getCurrentValue() {
    return null;
  }

  function getCurrentPage() {
    return $this->currentPage;
  }

  function getPrevPages() {
    return $this->prevPages;
  }

  function popPage () {
    if($this->prevPages == null) {
      return $this->firstPage;
    } else {
      $stack = explode(',', $this->prevPages);
      $current = array_pop($stack);
      $this->prevPages = implode(',', $stack);
      return $current;
    }
  }

  function pushPage ($current) {
    if($this->prevPages == null) {
      $this->prevPages = $current;
    } else {
      $stack = explode(',', $this->prevPages);
      array_push($stack, $current);
      $this->prevPages = implode(',', $stack);
    }
  }

  function nextPage($params) {
    return null;
  }

  function newPage($params) {
    $newPage = null;
    //print ("nextPage, page = [".$params['page']."], current = [".$this->currentPage."]");
    if ($params == null || !array_key_exists('page', $params) || $params['page'] == null) {
      //$this->deleteCookies();
      $newPage = $this->firstPage;
    } else {
      $this->readCookies();
      switch ($params['page']) {
        case 'next':
          $newPage = $this->nextPage($params);
          $this->pushPage($newPage);
          break;
        case 'previous':
          $newPage = $this->popPage();
          // check for top-of-stack special case
          if ($newPage == $this->currentPage) {
            $newPage = $this->popPage();
          }
          break;
        case 'cancel':
        case 'close':
          $this->deleteCookies();
          $newPage = $this->firstPage;
          // this case should be handled by the webview client
          break;
        case null:
        default:
          print ("WHOOPS -- case not defined on Main switch, case=".$params['page']);
          break;
      }
    }
    $this->currentPage = $newPage;
    $this->writeCookies();

    return $newPage;
  }

  /////////////////
  
  function getChoices($xpathvar, $tagName) {
    $choices = array();
  
    $queryResult = $xpathvar->query('//option_list[@name="' . $tagName . '"]/option');
    foreach($queryResult as $result){
      //echo $result->attributes->getNamedItem("id")->nodeValue. "\t" . $result->nodeValue."\n";
      $choices[$result->attributes->getNamedItem("id")->nodeValue] = $result->nodeValue;
    }
    
    return $choices;
  }
  
  function getDomxpath($filepath) {
    $xmldoc = new DOMDocument();
    
    $contentFile = $filepath . '/views/survey/input/content-' . $this->languageCode . '/' . $this->name . '.xml';
    if (!file_exists($contentFile) && strlen($this->languageCode) > 2) {
      $contentFile = $filepath . '/views/survey/input/content-' . substr($this->languageCode,0, 2) . '/' . $this->name . '.xml';
    }
    if (!file_exists($contentFile)) {
      $contentFile = $filepath . '/views/survey/input/content/' . $this->name . '.xml';
    }
    
    if (!file_exists($contentFile)) {
      die("401 or someting");
    }
    
    $xmldoc->load($contentFile);
    return(new Domxpath($xmldoc));
  }
  
  function getDomValue ($domxpath, $path) {
    if(($entries = $domxpath->query($path)) != FALSE && $entries->length > 0) {
      return($entries->item(0)->textContent);
    } else {
      return(null);
    }
  }

  function getResultsColumns() {
    return(null);
  }
  
  function getResultsQuery() {
    return(null);
  }

}
?>
