<?php

function getRealtimeStatistics($DB, &$stats) {
  $rs = $DB->Execute("SELECT COUNT(id) AS count
                     , SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(NOW(),req_ts)))) AS wait
                     FROM request_log WHERE ack_ts=0 AND res_ts=0");
//echo "<pre>"; print_r($rs); echo"</pre>";
  $stats["Waiting to be acknowledged"] = $rs->fields['count'];
  $stats["Average acknowledge wait time"] = $rs->fields['wait'];

  $rs = $DB->Execute("SELECT COUNT(id) AS count
                     , SEC_TO_TIME(AVG(TIME_TO_SEC(TIMEDIFF(NOW(),req_ts)))) AS wait
                     FROM request_log WHERE res_ts=0");
  $stats["Waiting to be closed"] = $rs->fields['count'];
  $stats["Average close wait time"] = $rs->fields['wait'];
}



function getResponseTimes($DB, &$bins) {
  $rs = $DB->Execute("SELECT TIME_TO_SEC(TIMEDIFF(ack_ts, req_ts)) AS ack
                     , TIME_TO_SEC(TIMEDIFF(res_ts, req_ts)) AS close FROM request_log");
  if ($rs) {
    //echo "<pre>"; print_r($rs); echo"</pre>";
    foreach ($rs as $row) {
      if ($row['close'] > 0) {
        foreach ($bins as $key=>$value) {
          if ($row['close'] < $key) {
            $bins[$key]++;
            break;
          }    
        }
      }
    }
  }
}    

function getRequestDistribution($DB, &$bins) {
  $rs = $DB->Execute("SELECT HOUR(req_ts) AS req FROM request_log");
    //echo "<pre>"; print_r($rs); echo"</pre>";
  if ($rs) {
    foreach ($rs as $row) {
      if ($row['req'] > 0) {
        foreach ($bins as $key=>$value) {
          if ($row['req'] < $key) {
            $bins[$key]++;
            break;
          }    
        }
      }
    }
  }
}    

?>