<?php

class MetaData {

  // Data elements
  private $meta = array();

  // Constructor  
  function MetaData ($path) {
    // create metadata  array and set default values
    $this->meta = array();
    $this->meta["path"] = $path;
    $this->meta["controller"] = "index";
    $this->meta["action"] = "index";
    $this->meta["params"] = array();
    $this->meta["style"] = "style.css";

    // parse URI
    $data = preg_split("/\//", $_SERVER['REQUEST_URI'],NULL,PREG_SPLIT_NO_EMPTY);
    if (sizeof($data)>0) {
      $this->meta["controller"] = $data[0];
      if (sizeof($data)>1) {
        $this->meta["action"] = $data[1];
        if (sizeof($data)>2) {
          for ($i=2; $i<sizeof($data); $i++) {
            if (sizeof($data)>$i+1) {
              $this->meta["params"][$data[$i]] = urldecode ($data[++$i]);
            } else {
              $this->meta["params"][$data[$i]] = "";
            }
          }
        }
      }
    }

    // check for mobile
    if(array_key_exists("mobile", $this->meta["params"]) || $this->detectIfMobile()) {
      $this->meta["mobile"] = ".m";
    }

    // set data from property files
    $property_data = array();
    // start with primary property file
    $property_file = $path."/index.ini";
    if(file_exists($property_file)) {
      $property_data = parse_ini_file($property_file, TRUE);
    }
    // next merge in anything in controller/action-based property files
    $this->readPropertyFile($path."/layouts/".$this->meta["controller"].".ini", $property_data);
    $this->readPropertyFile($path."/layouts/".$this->meta["controller"]."/".$this->meta["action"].".ini", $property_data);
  }

  private function readPropertyFile($property_file, $property_data) {
    //echo $property_file."<br/>";
    if(file_exists($property_file)) {
      $action_property = parse_ini_file($property_file, TRUE);
      foreach($action_property as $key => $value) {
        if(array_key_exists($key, $property_data)) {
          $property_data[$key] .= " ".$value;
        } else {
          $property_data[$key] = $value;
        }
      }
    }
    $this->meta = array_merge($this->meta, $property_data); 
    return;
  }

  public function get() {
    return($this->meta);
  }

  function detectIfMobile() {
    $mobile_browser = FALSE;
       
    if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android)/i', strtolower($_SERVER['HTTP_USER_AGENT'])) ||
        ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) ||
        (in_array(strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4)),
         array(
         'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
         'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
         'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
         'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
         'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
         'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
         'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
         'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
         'wapr','webc','winw','winw','xda ','xda-')
         ))
        ) {
      $mobile_browser = TRUE;
    }
       
    if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows') > 0) {
      $mobile_browser = FALSE;
    }
      
    return $mobile_browser;
  }
}
?>
