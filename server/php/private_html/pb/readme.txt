Ref: http://code.google.com/apis/protocolbuffers/docs/overview.html

- Edit request.proto

- To keep size down, set the "option optimize_for = "LITE_RUNTIME";" in your proto file and use the protobuf-lite library. 

- May have to comment out the options in the proto file when building for php

- To build in php, visit http://localhost:94/rest/pb_parse and then look in \trunk\server\php\private_html\pb for request.php

- To build java:

cd c:\dev\voxgift\winston\trunk\server\php\private_html\pb

\dev\voxgift\winston\trunk\tools\protoc\protoc.exe --java_out=\dev\voxgift\winston\trunk\client\android\WinstonLib\src request.proto

- To build Objective C:

cd ~/Development/winston/trunk/server/php/private_html/pb

~/Development/ProtocolBuffers-2.2.0-Source/src/protoc --proto_path=. --objc_out=../../../../client/ios/WinstonLib ./request.proto


