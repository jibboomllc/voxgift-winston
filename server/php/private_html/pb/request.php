<?php
class RequestEntry extends PBMessage
{
  var $wired_type = PBMessage::WIRED_LENGTH_DELIMITED;
  public function __construct($reader=null)
  {
    parent::__construct($reader);
    self::$fields["RequestEntry"]["1"] = "PBInt";
    $this->values["1"] = "";
    self::$fieldNames["RequestEntry"]["1"] = "id";
    self::$fields["RequestEntry"]["2"] = "PBString";
    $this->values["2"] = "";
    self::$fieldNames["RequestEntry"]["2"] = "request";
    self::$fields["RequestEntry"]["3"] = "PBString";
    $this->values["3"] = "";
    self::$fieldNames["RequestEntry"]["3"] = "requestor";
    self::$fields["RequestEntry"]["4"] = "PBString";
    $this->values["4"] = "";
    self::$fieldNames["RequestEntry"]["4"] = "req_device_id";
    self::$fields["RequestEntry"]["5"] = "PBInt";
    $this->values["5"] = "";
    self::$fieldNames["RequestEntry"]["5"] = "req_ts";
    self::$fields["RequestEntry"]["6"] = "PBString";
    $this->values["6"] = "";
    self::$fieldNames["RequestEntry"]["6"] = "ack_device_id";
    self::$fields["RequestEntry"]["7"] = "PBInt";
    $this->values["7"] = "";
    self::$fieldNames["RequestEntry"]["7"] = "ack_ts";
    self::$fields["RequestEntry"]["8"] = "PBString";
    $this->values["8"] = "";
    self::$fieldNames["RequestEntry"]["8"] = "ack_name";
    self::$fields["RequestEntry"]["9"] = "PBString";
    $this->values["9"] = "";
    self::$fieldNames["RequestEntry"]["9"] = "res_device_id";
    self::$fields["RequestEntry"]["10"] = "PBInt";
    $this->values["10"] = "";
    self::$fieldNames["RequestEntry"]["10"] = "res_ts";
    self::$fields["RequestEntry"]["11"] = "PBString";
    $this->values["11"] = "";
    self::$fieldNames["RequestEntry"]["11"] = "res_name";
  }
  function id()
  {
    return $this->_get_value("1");
  }
  function set_id($value)
  {
    return $this->_set_value("1", $value);
  }
  function request()
  {
    return $this->_get_value("2");
  }
  function set_request($value)
  {
    return $this->_set_value("2", $value);
  }
  function requestor()
  {
    return $this->_get_value("3");
  }
  function set_requestor($value)
  {
    return $this->_set_value("3", $value);
  }
  function req_device_id()
  {
    return $this->_get_value("4");
  }
  function set_req_device_id($value)
  {
    return $this->_set_value("4", $value);
  }
  function req_ts()
  {
    return $this->_get_value("5");
  }
  function set_req_ts($value)
  {
    return $this->_set_value("5", $value);
  }
  function ack_device_id()
  {
    return $this->_get_value("6");
  }
  function set_ack_device_id($value)
  {
    return $this->_set_value("6", $value);
  }
  function ack_ts()
  {
    return $this->_get_value("7");
  }
  function set_ack_ts($value)
  {
    return $this->_set_value("7", $value);
  }
  function ack_name()
  {
    return $this->_get_value("8");
  }
  function set_ack_name($value)
  {
    return $this->_set_value("8", $value);
  }
  function res_device_id()
  {
    return $this->_get_value("9");
  }
  function set_res_device_id($value)
  {
    return $this->_set_value("9", $value);
  }
  function res_ts()
  {
    return $this->_get_value("10");
  }
  function set_res_ts($value)
  {
    return $this->_set_value("10", $value);
  }
  function res_name()
  {
    return $this->_get_value("11");
  }
  function set_res_name($value)
  {
    return $this->_set_value("11", $value);
  }
}
class RequestLog extends PBMessage
{
  var $wired_type = PBMessage::WIRED_LENGTH_DELIMITED;
  public function __construct($reader=null)
  {
    parent::__construct($reader);
    self::$fields["RequestLog"]["1"] = "RequestEntry";
    $this->values["1"] = array();
    self::$fieldNames["RequestLog"]["1"] = "request_entry";
  }
  function request_entry($offset)
  {
    return $this->_get_arr_value("1", $offset);
  }
  function add_request_entry()
  {
    return $this->_add_arr_value("1");
  }
  function set_request_entry($index, $value)
  {
    $this->_set_arr_value("1", $index, $value);
  }
  function set_all_request_entrys($values)
  {
    return $this->_set_arr_values("1", $values);
  }
  function remove_last_request_entry()
  {
    $this->_remove_last_arr_value("1");
  }
  function request_entrys_size()
  {
    return $this->_get_arr_size("1");
  }
  function get_request_entrys()
  {
    return $this->_get_value("1");
  }
}
?>