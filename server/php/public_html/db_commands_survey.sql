-- USE `stdemo`;


DROP TABLE IF EXISTS surveys;
CREATE TABLE surveys (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , name VARCHAR(64) NOT NULL UNIQUE
  , description TEXT
  , url VARCHAR(128)
  , datatable VARCHAR(128)
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
);
INSERT INTO surveys (name, description, url, datatable) VALUES ('diabetes_daily_v1', 'Daily diabetes self care survey', '/diabetes', 'survey_diabetes_daily_v1');
INSERT INTO surveys (name, description, url, datatable) VALUES ('winston_feedback_v1', 'General feedback survey', '/feedback', 'survey_winston_feedback_v1');

-- survey_diabetes_daily_v1

DROP TABLE IF EXISTS survey_diabetes_daily_v1;
CREATE TABLE survey_diabetes_daily_v1 (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , device_id VARCHAR(128)
  , language_code VARCHAR(8)
  , create_ts TIMESTAMP DEFAULT NOW()
  
  , hw_manufacturer VARCHAR(128)
  , hw_model VARCHAR(128)
  , os_version VARCHAR(128)
  , app_version_name VARCHAR(32)
  , app_package_name VARCHAR(128)
  , lat DOUBLE
  , lon DOUBLE

  , number_of_tests VARCHAR(32)   
  , first_reading VARCHAR(32)   
  , low_reading VARCHAR(32)   
  , high_reading VARCHAR(32)  
  , ketones_tested VARCHAR(32)
  , number_of_injections VARCHAR(32)

  , low_treat_did_not BOOLEAN
  , low_treat_juice BOOLEAN
  , low_treat_glu_tab BOOLEAN
  , low_treat_milk BOOLEAN
  , low_treat_candy BOOLEAN
  , low_treat_other BOOLEAN

  , sites_arms BOOLEAN
  , sites_legs BOOLEAN
  , sites_butt BOOLEAN
  , sites_lback BOOLEAN
  , sites_stomach BOOLEAN
  , sites_other BOOLEAN

  , symptoms_shaky BOOLEAN
  , symptoms_lhead BOOLEAN
  , symptoms_sweaty BOOLEAN
  , symptoms_hungry BOOLEAN
  , symptoms_headache BOOLEAN
);

INSERT INTO survey_diabetes_daily_v1 (device_id, language_code, create_ts
  , number_of_tests, first_reading, low_reading, high_reading  
  , ketones_tested, number_of_injections)
  VALUES ('ade11875d37dc6d28485201eda7d3ff1', 'en', '2012-07-01 01:00:00'
  , '4-5', '<70', '<70', '>300'
  , 'Y', '2-3');
  
INSERT INTO survey_diabetes_daily_v1 (device_id, language_code, create_ts
  , number_of_tests, first_reading, low_reading, high_reading  
  , ketones_tested, number_of_injections)
  VALUES ('ade11875d37dc6d28485201eda7d3ff1', 'en', '2012-07-02 01:00:00'
  , '0', '', '', ''
  , '', '0');
INSERT INTO survey_diabetes_daily_v1 (device_id, language_code, create_ts
  , number_of_tests, first_reading, low_reading, high_reading  
  , ketones_tested, number_of_injections)
  VALUES ('ade11875d37dc6d28485201eda7d3ff1', 'en', '2012-07-03 01:00:00'
  , '4-5', '<70', '<70', '>300'
  , 'Y', '2-3');
  
INSERT INTO survey_diabetes_daily_v1 (device_id, language_code, create_ts
  , number_of_tests, first_reading, low_reading, high_reading  
  , ketones_tested, number_of_injections)
  VALUES ('ade11875d37dc6d28485201eda7d3ff1', 'en', '2012-07-04 01:00:00'
  , '2-3', '<70', '<70', '>300'
  , 'N', '4-5');
  
-- survey_notes

DROP TABLE IF EXISTS survey_notes;
CREATE TABLE survey_notes (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , survey_datatable VARCHAR(128) NOT NULL
  , survey_id INT NOT NULL
  , caregiver_id INT NOT NULL
  , note TEXT
  , create_ts TIMESTAMP DEFAULT NOW()
);
INSERT INTO survey_notes (survey_datatable, survey_id, caregiver_id, note, create_ts)
  VALUES ('survey_diabetes_daily_v1', '2', '2', 'Left message for pt', '2012-07-02 06:00:00');
INSERT INTO survey_notes (survey_datatable, survey_id, caregiver_id, note, create_ts)
  VALUES ('survey_diabetes_daily_v1', '2', '2', 'Pt cd/b. He is traveling and had his supplies stolen.', '2012-07-02 08:00:00');

-- survey_winston_feedback_v1

DROP TABLE IF EXISTS survey_winston_feedback_v1;
CREATE TABLE survey_winston_feedback_v1 (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , device_id VARCHAR(128)
  , language_code VARCHAR(8)
  , create_ts TIMESTAMP DEFAULT NOW()

  , hw_manufacturer VARCHAR(128)
  , hw_model VARCHAR(128)
  , os_version VARCHAR(128)
  , app_version_name VARCHAR(32)
  , app_package_name VARCHAR(128)
  , lat DOUBLE
  , lon DOUBLE

  , usage_fb VARCHAR(512)
  , languages_fb VARCHAR(512)
  , phrases_fb VARCHAR(512)
  , general_fb TEXT
);

INSERT INTO survey_winston_feedback_v1 (device_id, language_code, create_ts
  , hw_manufacturer, hw_model, os_version, app_version_name, app_package_name, lat, lon
  , usage_fb, languages_fb, phrases_fb, general_fb)
  VALUES ('bb3d1633c332870f', 'en', '2012-07-04 01:00:00'
  , 'samsung', 'GT-P1010', '2.2.1', '1.0', 'com.voxgift.winston', '30.37563284', '-97.79705914'
  , 'ATX', 'No', 'No', 'None');

INSERT INTO survey_winston_feedback_v1 (device_id, language_code, create_ts
  , hw_manufacturer, hw_model, os_version, app_version_name, app_package_name, lat, lon
  , usage_fb, languages_fb, phrases_fb, general_fb)
  VALUES ('966c9305a7aab0aa', 'en', '2012-07-05 02:00:00'
  , 'motorola', 'DROID BIONIC', '2.3.4', '1.0', 'com.voxgift.winston', '30.3752041666667', '-97.7965112833333'
  , 'St. Demos, Austin, Texas', 'Arabic', 'Good bye', 'Why not use Siri for the iOS version of Winston?');
