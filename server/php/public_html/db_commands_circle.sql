-- USE `circle`;


-- -----------------------------------------------------------------------------
-- TBD:
--   logged-in / device table


DROP TABLE IF EXISTS circle_users;
DROP TABLE IF EXISTS circle;
DROP TABLE IF EXISTS user_emails;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS role;

-- -----------------------------------------------------------------------------
-- reference tables

CREATE TABLE role (
  id INT NOT NULL PRIMARY KEY
  , name VARCHAR(64)
);
INSERT INTO role (id, name) VALUES (1, 'family');
INSERT INTO role (id, name) VALUES (2, 'friend');
INSERT INTO role (id, name) VALUES (3, 'provider');

-- -----------------------------------------------------------------------------
-- data tables

CREATE TABLE user (
  uuid VARCHAR(35) NOT NULL PRIMARY KEY
  , fname VARCHAR(64)
  , lname VARCHAR(64)
  , primary_email VARCHAR(128) NOT NULL UNIQUE
  , avatar_mime_type varchar(255)
  , avatar_size int
  , avatar_data blob
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
);
-- password
-- (phone)
-- billing_acct

INSERT INTO user (uuid, fname, lname, primary_email) VALUES ('067e6162-3b6f-4ae2-a171-2470b63dff00', 'Tom', 'Giesberg', 'tom@giesberg.net');
INSERT INTO user (uuid, fname, lname, primary_email) VALUES ('54947df8-0e9e-4471-a2f9-9af509fb5889', 'Melissa', 'Giesberg', 'melissa@giesberg.net');
INSERT INTO user (uuid, fname, lname, primary_email) VALUES ('1a9bcab6-1df1-4e40-ad4d-2b9f84f54632', 'Laura', 'Weiss', 'elweiss@comcast.net');
INSERT INTO user (uuid, fname, lname, primary_email) VALUES ('a21a5b63-2ba5-4c8f-b170-e1d124942d6b', 'Steve', 'Lake', 'smlake07@comcast.net');
INSERT INTO user (uuid, fname, lname, primary_email) VALUES ('8b749998-efeb-49b7-b480-84eb4a444cd3', 'Betty', 'Lake', 'fblake@blomand.net');
INSERT INTO user (uuid, fname, lname, primary_email) VALUES ('3df236cc-ea1a-4dbd-b70c-947c2e95c9e1', 'Frank', 'Lake', 'carnut914@yahoo.com');
INSERT INTO user (uuid, fname, lname, primary_email) VALUES ('9f7fa857-6683-4d19-b58d-310ca4e2f856', 'Eric', 'Weiss', 'thesavannahkid@gmail.com');
INSERT INTO user (uuid, fname, lname, primary_email) VALUES ('2c9cffe4-8f9f-48d0-abf0-b7fecd8c8fe1', 'Sue', 'Marx', '609-705-9071');
INSERT INTO user (uuid, fname, lname, primary_email) VALUES ('fcaff82f-3bb4-48ec-accf-bf88e3c8aa4a', 'Test 1', '', 'giesberg@yahoo.com');

CREATE TABLE user_emails (
  email VARCHAR(128) NOT NULL PRIMARY KEY
  , user_uuid VARCHAR(35) NOT NULL
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , confirmed_ts TIMESTAMP
  , FOREIGN KEY (user_uuid) REFERENCES user(uuid) ON DELETE CASCADE
  );

INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('067e6162-3b6f-4ae2-a171-2470b63dff00', 'tom@giesberg.net', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('067e6162-3b6f-4ae2-a171-2470b63dff00', 'tom@voxgift.com', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('54947df8-0e9e-4471-a2f9-9af509fb5889', 'melissa@giesberg.net', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('54947df8-0e9e-4471-a2f9-9af509fb5889', 'mlg@giesberg.net', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('1a9bcab6-1df1-4e40-ad4d-2b9f84f54632', 'elweiss@comcast.net', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('a21a5b63-2ba5-4c8f-b170-e1d124942d6b', 'smlake07@comcast.net', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('8b749998-efeb-49b7-b480-84eb4a444cd3', 'fblake@blomand.net', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('3df236cc-ea1a-4dbd-b70c-947c2e95c9e1', 'carnut914@yahoo.com', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('9f7fa857-6683-4d19-b58d-310ca4e2f856', 'thesavannahkid@gmail.com', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email) VALUES ('2c9cffe4-8f9f-48d0-abf0-b7fecd8c8fe1', '609-705-9071');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('fcaff82f-3bb4-48ec-accf-bf88e3c8aa4a', 'giesberg@yahoo.com', '2012-12-01 01:00:00');

CREATE TABLE circle (
  uuid VARCHAR(35) NOT NULL PRIMARY KEY
  , name VARCHAR(64)
  , owner_uuid VARCHAR(35) NOT NULL
  , center_uuid VARCHAR(35) NOT NULL UNIQUE
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , FOREIGN KEY (owner_uuid) REFERENCES user(uuid)
  , FOREIGN KEY (center_uuid) REFERENCES user(uuid)
);
-- sponsor
-- billing

INSERT INTO circle (uuid, name, owner_uuid, center_uuid) VALUES ('db346bc2-7ac3-4c34-9606-44e23aaf7c34', "Steve's Circle-of-care", '1a9bcab6-1df1-4e40-ad4d-2b9f84f54632', 'a21a5b63-2ba5-4c8f-b170-e1d124942d6b');
INSERT INTO circle (uuid, name, owner_uuid, center_uuid) VALUES ('6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3', "Test Circle", 'fcaff82f-3bb4-48ec-accf-bf88e3c8aa4a', 'fcaff82f-3bb4-48ec-accf-bf88e3c8aa4a');

CREATE TABLE circle_users (
  circle_uuid VARCHAR(35) NOT NULL
  , user_uuid VARCHAR(35) NOT NULL
  , roll_id INT NOT NULL
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , PRIMARY KEY (circle_uuid, user_uuid)
  , FOREIGN KEY (circle_uuid) REFERENCES circle(uuid) ON DELETE CASCADE
  , FOREIGN KEY (user_uuid) REFERENCES user(uuid) ON DELETE CASCADE
  , FOREIGN KEY (roll_id) REFERENCES role(id)
  );

INSERT INTO circle_users (circle_uuid, user_uuid, roll_id) VALUES ('db346bc2-7ac3-4c34-9606-44e23aaf7c34', '067e6162-3b6f-4ae2-a171-2470b63dff00', 1);
INSERT INTO circle_users (circle_uuid, user_uuid, roll_id) VALUES ('db346bc2-7ac3-4c34-9606-44e23aaf7c34', '54947df8-0e9e-4471-a2f9-9af509fb5889', 1);
INSERT INTO circle_users (circle_uuid, user_uuid, roll_id) VALUES ('db346bc2-7ac3-4c34-9606-44e23aaf7c34', '1a9bcab6-1df1-4e40-ad4d-2b9f84f54632', 1);
INSERT INTO circle_users (circle_uuid, user_uuid, roll_id) VALUES ('db346bc2-7ac3-4c34-9606-44e23aaf7c34', 'a21a5b63-2ba5-4c8f-b170-e1d124942d6b', 1);
INSERT INTO circle_users (circle_uuid, user_uuid, roll_id) VALUES ('db346bc2-7ac3-4c34-9606-44e23aaf7c34', '8b749998-efeb-49b7-b480-84eb4a444cd3', 1);
INSERT INTO circle_users (circle_uuid, user_uuid, roll_id) VALUES ('db346bc2-7ac3-4c34-9606-44e23aaf7c34', '3df236cc-ea1a-4dbd-b70c-947c2e95c9e1', 1);
INSERT INTO circle_users (circle_uuid, user_uuid, roll_id) VALUES ('db346bc2-7ac3-4c34-9606-44e23aaf7c34', '9f7fa857-6683-4d19-b58d-310ca4e2f856', 1);
INSERT INTO circle_users (circle_uuid, user_uuid, roll_id) VALUES ('db346bc2-7ac3-4c34-9606-44e23aaf7c34', '2c9cffe4-8f9f-48d0-abf0-b7fecd8c8fe1', 2);
