-- CREATE DATABASE `stdemo`

-- USE `stdemo`;

-- caregiver tables
DROP TABLE IF EXISTS department;
CREATE TABLE department (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , name VARCHAR(128)
);
INSERT INTO department (name) VALUES ('ICU');
INSERT INTO department (name) VALUES ('Surgery');
INSERT INTO department (name) VALUES ('ER');

DROP TABLE IF EXISTS availability;
CREATE TABLE availability (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , name VARCHAR(128)
);
INSERT INTO availability (name) VALUES ('Available');
INSERT INTO availability (name) VALUES ('Not available');
INSERT INTO availability (name) VALUES ('On a break');
INSERT INTO availability (name) VALUES ('At lunch');
INSERT INTO availability (name) VALUES ('In a meeting');

DROP TABLE IF EXISTS caregiver;
CREATE TABLE caregiver (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , uname VARCHAR(128)
  , password VARCHAR(128)
  , fname VARCHAR(128)
  , lname VARCHAR(128)
  , department_id INT DEFAULT 1
  , on_duty BOOLEAN DEFAULT FALSE
  , availability_id INT DEFAULT 1
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
);
INSERT INTO caregiver (uname, department_id, on_duty, availability_id, create_ts) VALUES ('Nurses Station', 1, TRUE, 1, '2012-06-22 01:00:00');
INSERT INTO caregiver (uname, department_id, on_duty, availability_id, create_ts) VALUES ('Nurse Cassie', 1, TRUE, 1, '2012-06-22 01:00:00');
INSERT INTO caregiver (uname, department_id, on_duty, availability_id) VALUES ('Nurse Lynne', 3, TRUE, 3);
INSERT INTO caregiver (uname, department_id, on_duty, availability_id) VALUES ('Nurse Emily', 3, FALSE, 1);
INSERT INTO caregiver (uname, department_id, on_duty, availability_id) VALUES ('Nurse Mary', 1, FALSE, 2);

DROP TABLE IF EXISTS caregiver_device_assignment;
CREATE TABLE caregiver_device_assignment (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , caregiver_id INT NOT NULL
  , device_id VARCHAR(128)
  , device_name VARCHAR(128)
  , create_ts TIMESTAMP DEFAULT NOW()
  , checkin_ts TIMESTAMP
  , delete_ts TIMESTAMP
  , INDEX careg_ind (caregiver_id),
  FOREIGN KEY (caregiver_id) REFERENCES caregiver(id)
  ON DELETE CASCADE
);
INSERT INTO caregiver_device_assignment (caregiver_id, device_id, device_name, create_ts) VALUES ('1', '0123456789', 'Web Interface', '2012-03-22 01:00:00');
INSERT INTO caregiver_device_assignment (caregiver_id, device_id, device_name, create_ts) VALUES ('2', '22a0000015ba7207', 'Droid Phone', '2012-03-22 01:00:00');

-- patient tables
DROP TABLE IF EXISTS patient;
CREATE TABLE patient (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , uname VARCHAR(128)
  , password VARCHAR(128)
  , fname VARCHAR(128)
  , lname VARCHAR(128)
  , department_id INT DEFAULT 1
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
);
INSERT INTO patient (uname, department_id, create_ts, delete_ts) VALUES ('Previous Dept 1 Patient', '1', '2012-06-22 01:00:00', '2012-06-22 01:59:00');
INSERT INTO patient (uname, department_id, create_ts, delete_ts) VALUES ('Previous Dept 2 Patient', '2', '2012-06-22 02:00:00', '2012-06-22 02:59:00');
INSERT INTO patient (uname, department_id, create_ts, delete_ts) VALUES ('Previous Dept 3 Patient', '3', '2012-06-22 03:00:00', '2012-06-22 03:59:00');
INSERT INTO patient (uname) VALUES ('Patient Steve');
INSERT INTO patient (uname) VALUES ('Patient Mike');

DROP TABLE IF EXISTS patient_device_assignment;
CREATE TABLE patient_device_assignment (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , patient_id INT NOT NULL
  , device_id VARCHAR(128)
  , device_name VARCHAR(128)
  , create_ts TIMESTAMP DEFAULT NOW()
  , checkin_ts TIMESTAMP
  , delete_ts TIMESTAMP
  , INDEX pat_ind (patient_id),
  FOREIGN KEY (patient_id) REFERENCES patient(id)
  ON DELETE CASCADE
);

INSERT INTO patient_device_assignment (patient_id, device_id, device_name, create_ts, delete_ts) VALUES ('1', 'bb3d1633c332870f', "Dept 1 device", '2012-03-22 01:00:00', '2012-03-22 01:59:00');
INSERT INTO patient_device_assignment (patient_id, device_id, device_name, create_ts, delete_ts) VALUES ('2', 'bb3d1633c332870f', "Dept 2 device", '2012-03-22 02:00:00', '2012-03-22 02:59:00');
INSERT INTO patient_device_assignment (patient_id, device_id, device_name, create_ts, delete_ts) VALUES ('3', 'bb3d1633c332870f', "Dept 3 device", '2012-03-22 03:00:00', '2012-03-22 03:59:00');
INSERT INTO patient_device_assignment (patient_id, device_id, device_name) VALUES ('4', 'bb3d1633c332870f', "Samsung Galaxy Tab 7");

DROP TABLE IF EXISTS patient_listeners;
CREATE TABLE patient_listeners (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , patient_id INT NOT NULL
  , caregiver_id INT NOT NULL
);

DROP TABLE IF EXISTS patient_listener_departments;
CREATE TABLE patient_listener_departments (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , patient_id INT NOT NULL
  , department_id INT NOT NULL
);


DROP TABLE IF EXISTS request_log;
CREATE TABLE request_log (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , req_device_id VARCHAR(32)
  , request VARCHAR(256)
  , req_ts TIMESTAMP DEFAULT NOW()
  , ack_device_id VARCHAR(32)
  , ack_ts TIMESTAMP
  , res_device_id VARCHAR(32)
  , res_ts TIMESTAMP
);
/*
INSERT INTO request_log (req_device_id, request, req_ts, ack_device_id, ack_ts, res_device_id, res_ts)
VALUES ('bb3d1633c332870f', 'Test A', '2012-03-22 01:01:01', '22a0000015ba7207', '2012-03-22 01:10:01', '22a0000015ba7207', '2012-03-22 01:20:01');

INSERT INTO request_log (req_device_id, request, req_ts, ack_device_id, ack_ts)
VALUES ('bb3d1633c332870f', 'Test B', '2012-03-22 01:02:02', '22a0000015ba7207', '2012-03-22 01:10:01');

INSERT INTO request_log (req_device_id, request, req_ts)
VALUES ('bb3d1633c332870f', 'Test C', '2012-03-22 01:03:01');

INSERT INTO request_log (req_device_id, request)
VALUES ('bb3d1633c332870f', 'I am thirsty');

INSERT INTO request_log (req_device_id, request)
VALUES ('bb3d1633c332870f', 'I need a blanket');
*/

DROP TABLE IF EXISTS patient_note;
CREATE TABLE patient_note (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , patient_id INT NOT NULL
  , topic VARCHAR(128)
  , note TEXT
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , INDEX pat_ind (patient_id),
  FOREIGN KEY (patient_id) REFERENCES patient(id)
  ON DELETE CASCADE
);

INSERT INTO patient_note (patient_id, topic, note) VALUES ('4', "Admission Note", "Patient admitted complaining of sore ankle after stepping off curb");
INSERT INTO patient_note (patient_id, topic, note) VALUES ('4', "Nurse Observation", "Contusion on left ankle");


