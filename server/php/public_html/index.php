<?php
$path = '../private_html';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);

include_once('library/MetaData.php');

//set data from property file
$metaData = new MetaData($path);
$meta = $metaData->get();
//$meta = array_merge($meta, $_REQUEST);

// go to the controller
$controller_file = $path."/controllers/".$meta["controller"]."Controller.php";
if (!file_exists($controller_file)) {
  $controller_file = $path."/controllers/indexController.php";
}
include_once($controller_file);

?>
