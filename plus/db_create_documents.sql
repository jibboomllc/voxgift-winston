-- Use the following link for uuid creation:
-- http://www.famkruithof.net/uuid/uuidgen

USE `voxcircle`;

DROP TABLE IF EXISTS documents;
DROP TABLE IF EXISTS document_type;

-- -----------------------------------------------------------------------------
-- data tables - documents

CREATE TABLE IF NOT EXISTS document_type (
  id INT NOT NULL PRIMARY KEY
  , name VARCHAR(64)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO document_type (id, name) VALUES (0, 'Legal');
INSERT INTO document_type (id, name) VALUES (1, 'Help');
INSERT INTO document_type (id, name) VALUES (2, 'Email');

CREATE TABLE IF NOT EXISTS documents (
  uuid VARCHAR(36) NOT NULL
  , language VARCHAR(2) NOT NULL
  , locale VARCHAR(2) NOT NULL
  , type_id INT NOT NULL
  , title VARCHAR(64)
  , content TEXT
  , sort_order INT
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , PRIMARY KEY (uuid, language, locale)
  , FOREIGN KEY (type_id) REFERENCES document_type(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO documents (uuid, language, locale, type_id, sort_order, title, content)
VALUES ('7934c64c-5233-4cb7-9e58-460fe4143a00', 'en', 'US', 0, 0, 'Privacy Policy',
'<h2>PRIVACY POLICY</h2>

<p>
The security of your personal information is important to us. Please do not enter any personal information about yourself except the information needed to create and maintain your user account. We do not hold any liability for any personal data or any sensitive information you provided during opening of the account, which is not expressly and specifically required for a proper registration.
</p>

<p>
We follow generally accepted industry standards to protect the personal information submitted, both during transmission and once we receive it. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, while our goal to use commercially acceptable ways to protect your personal information, we cannot guarantee it is absolutely secure. Please keep it in mind before submitting any information about yourself. Please note that information that you voluntarily make public in your user profile, or which you disclose by posting comments will be publicly available and viewable by others. We do not hold any liability for any information that you voluntarily choose to be public through such and/or other explicit actions. 
</p>

<p>
You may be interested in inviting your friends to use our products and sharing information with your friends via our products. In the event you wish to use referral service, the information provided, such as the name and/or email address of your friends, will only be used for the purposes of sending invitations and reminders to your friends. By entering your friend�s details you confirm that your friend has consented to the supply of such details. Please be mindful what you say about your friends and what you share about their personal information. User name accounts do not necessarily resemble people whom you think you know or who they say they are.
</p>

<p>
You might be interested in inviting random people, whom you do not know but with whom you are interested in communicating through our products. In this event, please make sure you truly wish to share any of your information or data with such a person. Once you provide the information to such a person, we will not be able to guarantee that this person will not use this information against you, trade it or misuse it in any other way. We will not be able to prevent such misuse, nor will we be liable for it. We will only be able to block the account or delete the account of any user who violate our privacy policy. However, once any information is provided by you directly to any person which is your friend or not through our products, we are not responsible for any consequences.    
</p>

<p>
During registration you are required to give contact information such as name and email address. We use this information to verify that you are a real person interested in using such site features for their designed purpose.
</p>

<p>
Certain information about users can be gathered automatically and stored inside log files. This information might include Internet protocol (IP) addresses, the browser type, Internet service providers (ISP), referring/exit pages, operating systems, date/time stamps, and clickstream data. However, this information does not identify individual users and we use it exclusively to analyze trends, to administer the site, to track user movements around the site as a whole to improve the services provided. We do not link this automatically-collected data to any personally identifiable information.
</p>

<p>
We do not allow gathering personal information and we do not share it with anyone. Your information with limited access rights (including personal information) can only be accessed by our authorized employees or consultants or the concerned group entities that need to have access to this data in order to be able to fulfill their given duties. However, we shall take appropriate organizational and technical measures to protect your personal data provided to it or collected by it with due observance of the applicable obligations and exceptions under the relevant legislation. 
</p>

<p>
We reserve the right to disclose your personally identifiable information as required by any law and also when we believe that disclosure is necessary to protect our rights and/or to comply with judicial proceedings, court orders, or legal process served on our Web site.
</p>

<p>
We will retain your information for as long as is necessary to: (1) fulfill any of the services we provide or (2) comply with applicable legislation, regulatory requests and relevant orders from competent courts.
</p>

<p>
If we do decide to change our privacy policy, we will post any changes to this privacy statement so that you are all aware of what information we collect, how we use that information, and under what circumstances, if any, we may disclose it. 
</p>

<p>
We reserve the right to modify this privacy statement at any time, so please review it relatively frequently. If we make any changes to this policy, we will notify you here.
</p>

<p>
If you have any questions or suggestions regarding our privacy policy, please contact us at:  support@voxgift.com</p>

<h2>
JIBBOOM LLC.
</h2>

<p>
MAY 28, 2013
</p>
');

INSERT INTO documents (uuid, language, locale, type_id, sort_order, title, content)
VALUES ('99f68786-65e2-4663-b9f2-4ebf5fa4b02f', 'en', 'US', 0, 1, 'End User License Agreement',
'<h2>END USER LICENSE AGREEMENT (\'EULA\')</h2>
<p><i>
WinstonPlus gives patients the gift of voice. WinstonPlus is the product of Voxgift, a subsidiary of Jibboom LLC, which is its exclusive owner and operator. 
</i>
\'Service(s)\' refers to your use of WinstonPlus for any purpose whatsoever.
</p>

<h2>
PLEASE READ CAREFULLY BEFORE USING WINSTONPLUS. YOUR USE OF WINSTONPLUS OR ANY SERVICES PROVIDED THROUGH WINSTONPLUS IS SUBJECT TO THESE TERMS. YOU AGREE TO BE BOUND BY THE TERMS SET FORTH IN THE END USER LICENSE AGREEMENT (\'EULA\') PRESENTED BELOW. IF YOU DO NOT AGREE TO THESE TERMS, DO NOT USE WINSTONPLUS IN ANY WAY OR FOR ANY PURPOSE WHATSOEVER. 
</h2>

<p>
This EULA sets out the legal terms between you (either an individual or legal entity) and JIBBOOM LLC.  (\'JIBBOOM\' or \'we\' or \'us\'), whose principal place of business is in Austin, Texas, USA. 
</p>

<h2>
ACCEPTANCE OF THE TERMS 
</h2>

<p>
These Terms are accepted by you (a) when you click to accept or agree to the Terms, whenever this option is made available to you; or (b) when you actually use the Services. You may print a copy of these Terms for your records and we highly recommend you do so. These Terms remain effective from the date of acceptance until terminated by you or JIBBOOM in accordance with this Agreement.
</p>

<p>
You cannot accept these Terms if: (a) you are not lawfully entitled to use the Services under any applicable laws in the country in which you are located or resident; or (b) if you are not of legal age to form a binding agreement with JIBBOOM. By accepting these terms you acknowledge and agree that you have reached the age to enter into such binding agreement in the country in which you are located or resident or that you have your parent or legal guardian\'s permission if required by applicable law. It is your responsibility to ensure that you are legally allowed to enter into this Agreement and that you do not violate any applicable laws.
</p>

<p>
In some countries there exist legal and/or other restrictions on usage of the Services and/or downloading, installing and/or using of products, portions of products, software which are or might be available through the Services. It is your responsibility to ensure that you comply with all applicable legal restrictions in your jurisdiction. 
</p>

<h2>
Intellectual property RIGHTS 
</h2>

<p>
We are exclusive owners of WinstonPlus, except for personal data of users, and all intellectual property therein (whether registered or unregistered and anywhere in the world) and are protected by intellectual property laws and other laws of the U.S., other countries and international treaties. Furthermore, nothing in the Agreement gives you a right to use any of JIBBOOM\'s trade names, trade marks, service marks, logos or domain names unless you have our express written permission.
</p>

<p>
The content and compilation of content included in WinstonPlus, such as sounds, text, graphics, logos, icons, images, audio clips, digital downloads and software, are the property of JIBBOOM, its affiliates or licensors and are protected by United States and international copyright laws. Such copyright protected content cannot be reproduced without JIBBOOM\'s express permission. JIBBOOM reserves all rights not expressly granted under these Terms.
</p>

<h2>
LICENSE 
</h2>

<p>
Subject to your compliance with these Terms, you are granted a limited, non-exclusive, non-sublicensable, non-assignable license to use the Services on a personal computer, cell phone or other device. We reserve all rights not expressly granted to you under these Terms.
</p>

<h2>
USE OF THE SERVICES BY YOU
</h2>

<p>
You agree that you are responsible for providing and maintaining all equipment required to access and use WinstonPlus. 
</p>

<p>
You commit not to do anything that might jeopardize the security of your WinstonPlus account. Your WinstonPlus account should not be used by anyone other than you. You are responsible for all usage or activity on the Service by users using your password, including but not limited to use of your password by any third-party. 
</p>

<p>
The responsibility over any published material or content lies solely on the user that submitted it to the Service. You acknowledge and agree that any published material or content is entirely the responsibility of the person from whom it is originated. You are responsible for the content you choose to communicate and access using the Services. We cannot assume any responsibility or liability over any material or content published by you or other users on the Service.
</p>

<p>
In particular, you are responsible for ensuring that you do not submit material that is (i) offensive, unlawful, obscene, defamatory, libelous, threatening, abusive, inappropriate, pornographic, harassing, hateful, or otherwise unlawful or violates any law; (ii) fraudulent or misrepresentation; (iii) protected by any applicable copyright laws, trade secret or that otherwise  infringe the privacy rights, property rights, or any other rights of any person; (iv) an advertisement or solicitation of business, funds, goods or services; or (v) impersonating another person or invading the privacy of any third party. We reserve the right (but shall have no obligation) to decide whether any content that you use complies with these Terms and we may in our sole discretion remove such content, suspend and/or terminate this Agreement and your WinstonPlus account if you use any content that is in breach of these Terms or to comply with applicable law or the order or requirement of a court, administrative agency or other governmental body. You acknowledge and agree that JIBBOOM is under no obligation to put back such material at any time. 
</p>

<p>
We reserve the right to make changes or updates to the Services in our sole discretion. In order to be able to download or otherwise take advantage of any updates, you may be required to enter into an updated version of these Terms. We may in our sole discretion require you to automatically download and install updates when available in order to allow you to use the Services. You must accept such Updates subject to these Terms in order to use the Services. 
</p>

<p>
Furthermore, in order to provide the best possible experience for our users we reserve the right to change technical features, perform maintenance or the underlying infrastructure of WinstonPlus in our sole discretion and without any prior notice. This may require us to temporarily suspend or limit your use of some or all of the Services (or any features related hereto). You will not be entitled to claim damages for such suspension or limitation of the use of any Services. You agree and acknowledge that if you do not agree with any changes to the Services you may terminate this Agreement in accordance with terms thereto. 
</p>

<h2>
YOUR OBLIGATIONS 
</h2>

<p>
You acknowledge and agree to use WinstonPlus solely for your individual purpose and in accordance with the laws of jurisdiction where you are located and applicable international law. We cannot assume any responsibility in ensuring your compliance with any applicable laws. 
</p>

<p>
Furthermore, the Services may be subject to international rules that govern the export of software. You shall comply with all applicable international and national laws that apply to the software with this respect.
</p>

<p>
You shall not and you agree not to:
</p>

<ul>
    <li>(a) use WinstonPlus for any unlawful purposes;</li>
    <li>(b) post or transmit in any manner any contact information including, but not limited to, email addresses, nicknames, telephone numbers, postal addresses, URLs, or full names through your publicly posted information. You agree not to circumvent, disable or otherwise interfere with the security related features of WinstonPlus or features that prevent or restrict the use of any content thereof;</li>
<li>(c) use any type of spider, virus, worm, trojan-horse, time bomb or any other codes or instructions that are designed to distort, delete or damage the Services or personal computers of third parties; </li>
<li>(d) post or transmit unsolicited communications, including but not limited to �SPAM� or �SPIT� or any communication not permitted by applicable laws;</li>
<li>(e) post, transmit, or otherwise expose any third party to material which is offensive, threatening, harassing, inappropriate, libelous, slanderous, abusive or defamatory, or racist, pornographic, obscene, harmful to a minor or an infringement of any intellectual property rights or a trade secret of a third party, or would otherwise violate the rights of any third party or give rise to civil or criminal liability;</li>
<li>(f) use any material or content that is subject to any third party proprietary rights, unless you have a license or permission from the owner of such rights; </li>
<li>(g) take any action to infringe, limit or otherwise interfere with JIBBOOM\'s intellectual property rights;</li>
<li>(h) register any trade name, trademark, logo, domain name or any other name or sign that incorporates any of JIBBOOM\'s intellectual property (in whole or part) or that is confusingly similar thereto; </li>
<li>(i) use the Services or cause the Services (or any part of it) to be used within or to provide commercial products or services to third parties.</li>
</ul>

<p>
It is your responsibility to keep all information provided to you through the Services as private and confidential and will not give such information to anyone without the permission of the person who provided it to you. You shall not collect or try to collect and/or misuse any personal information protected by applicable laws. 
</p>

<p>
You represent and warrant that you are lawfully entitled to use the Services under any applicable laws in the country of your jurisdiction. You certify that you are in no violation, if applicable, of U.S. export control and sanctions regulations.
</p>

<p>
If you notice any suspicious misconduct or misuse of WinstonPlus we encourage you to report it by sending us an email to: support@voxgift.com 
</p>

<h2>
TERMINATION OF THIS AGREEMENT 
</h2>

<p>
You may terminate this Agreement and your relationship with JIBBOOM at any time by closuring your WinstonPlus account, or otherwise ceasing to use the Services. You acknowledge and agree that by terminating your relationship with JIBBOOM in the way stipulated in this section you will lose all access to your WinstonPlus account and it no longer be available to you. We will not be responsible and will not have any obligation to restore and/or reinstate your WinstonPlus account or any data lost. 
</p>

<p>
Without limiting other remedies, JIBBOOM may suspend or terminate this Agreement with you, or may terminate or suspend your use of the Services at any time if: 
</p>

<ul>
    <li>(a) you are in default of these Terms;
</li>
    <li>(b) you are engaged in illegal actions, or infringe proprietary rights, rights of privacy, or intellectual property rights of any person, or any third party rights, or otherwise create risk or possible legal exposure for us; 
</li>
<li>(g) required by applicable laws/regulations and within the time limits as required by such laws/regulations; 
</li>
<li>(h) we decide to cease offering the Services to users generally. 
</li>
</ul>

<p>
Upon termination of this Agreement: (a) all licenses and rights to use the Services shall immediately terminate; (b) you will immediately cease any and all use of the Services.
</p>

<p>
Any suspension or termination of this Agreement will not affect your obligations to JIBBOOM under these Terms (including, without limitation, proprietary rights and ownership, indemnification and limitation of liability, confidentiality), which reasonably are intended to survive such suspension or termination.
</p>

<h2>
EXCLUSION OF WARRANTIES, LIMITATION OF LIABILITY AND INDEMNITY 
</h2>

<p>
No Warranties: TO THE MAXIMUM EXTENT PERMITTED BY LAW: THE SERVICES PROVIDED \'AS IS\' AND USED AT YOUR SOLE RISK WITH NO WARRANTIES WHATSOEVER; JIBBOOM DOES NOT MAKE ANY WARRANTIES, CLAIMS OR REPRESENTATIONS AND EXPRESSLY DISCLAIMS ALL SUCH WARRANTIES OF ANY KIND, WHETHER EXPRESS, IMPLIED OR STATUTORY, WITH RESPECT TO THE SERVICES INCLUDING, WITHOUT LIMITATION, WARRANTIES OR CONDITIONS OF QUALITY, PERFORMANCE, NON-INFRINGEMENT, MERCHANTABILITY, OR FITNESS FOR USE FOR A PARTICULAR PURPOSE. JIBBOOM FURTHER DOES NOT REPRESENT OR WARRANT THAT THE SERVICES WILL ALWAYS BE AVAILABLE, ACCESSIBLE, UNINTERRUPTED, TIMELY, SECURE, ACCURATE, COMPLETE AND ERROR-FREE OR WILL OPERATE WITHOUT PACKET LOSS, NOR DOES JIBBOOM WARRANT ANY CONNECTION TO OR TRANSMISSION FROM THE INTERNET.
</p>

<p>
No Liability: YOU ACKNOWLEDGE AND AGREE THAT JIBBOOM SHALL HAVE NO LIABILITY WHATSOEVER, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR ANY OTHER THEORY OF LIABILITY, AND WHETHER OR NOT THE POSSIBILITY OF SUCH DAMAGES OR LOSSES HAS BEEN NOTIFIED TO JIBBOOM, IN CONNECTION WITH OR ARISING FROM YOUR USE OF THE SERVICES PROVIDED TO YOU THROUGH WINSTONPLUS. YOUR ONLY RIGHT OR REMEDY WITH RESPECT TO ANY PROBLEMS OR DISSATISFACTION WITH SUCH SERVICES IS TO IMMEDIATELY CEASE USE OF SERVICES.
</p>

<p>FURTHERMORE, YOU ACKNOWLEDGE AND AGREE THAT NEITHER JIBBOOM NOR ANYONE ELSE WHO HAS BEEN INVOLVED IN THE CREATION, PRODUCTION OR DELIVERY OF THE SERVICES shall be liable to you, whether in contract, tort (including negligence) or any other theory of liability, and whether or not the possibility of such damages or losses has been notified to JIBBOOM, for (a) any indirect, special, incidental or consequential damages; or (b) any loss of income, business, actual or anticipated profits, opportunity, goodwill or reputation (whether direct or indirect); or (c) any damage to or corruption of data (whether direct or indirect)
</p>

<p>
Nothing in this these Terms shall exclude or restrict JIBBOOM\'s any liability which cannot be limited or excluded by applicable law in your jurisdiction and our liability will be limited to the maximum extent permitted by law.
</p>

<p>
If any third party brings a claim against JIBBOOM in connection with, or arising out of (i) your breach of these Terms; (ii) your breach of any applicable law or regulation; (iii) your infringement or violation of the rights of any third parties (including intellectual property rights), you will indemnify and hold JIBBOOM  harmless from and against all damages, liability, loss, costs and expenses (including reasonable legal fees and costs) related to such claim. 
</p>

<h2>
CONFIDENTIALITY AND SECURITY
</h2>

<p>
We are committed to respecting your privacy and the confidentiality of your personal information. We will process your personal information, the traffic data and the content of your communication(s) in accordance with our Privacy Policy. Please read them carefully before starting using the Services. 
</p>

<h2>
MISCELLANEOUS
</h2>

<p>
These Terms constitute the entire agreement between you and JIBBOOM with respect to your use of the Services.
</p>

<p>
You may not assign these Terms or any rights or obligations contained in them. JIBBOOM may, without prior notice, assign these Terms or any rights or obligations contained in them to any third party.
</p>

<p>
If any provision of these Terms (or part of it), is found by any court or administrative body of competent jurisdiction to be illegal, invalid or unenforceable, then such provision (or part of it) shall be removed from the Terms without affecting the legality, validity or enforceability of the remainder of the Terms.
</p>

<p>
The failure by JIBBOOM to exercise, or delay in exercising, a legal right or remedy provided by these Terms or by law shall not constitute a waiver of JIBBOOM\'s right or remedy. 
</p>

<p>
We may make changes to these Terms from time to time. The changes will be effective when published. Please review the Terms on a regular basis. You acknowledge and agree that your express acceptance of the Terms or your use of the Services after the date of publication shall constitute your agreement to the updated Terms. If you do not agree with the amended Terms, you may terminate this agreement in accordance with the terms set forth below.
</p>

<p>
If we are unable to provide the Services as a result of force majeure, we will not be in breach of any of its obligations towards you under these Terms. 
</p>

<p>
These Terms shall be governed by and interpreted in accordance with the laws of State of Texas without regard to its conflict of laws provisions. The exclusive jurisdiction and venue of any action with respect to the subject matter of these Terms will be the U.S. federal courts and state courts located in Austin, Texas, and each of the parties hereto waives any objection to jurisdiction and venue in such courts. However, we reserve the right to seek injunction relief in any jurisdiction when we deem it necessary.
</p>

<p>
This agreement was drafted in English (US). We may provide you with a translation of the English language version of these Terms. In the event of any inconsistency between a non-English version of these Terms and the English version, the English version shall prevail.
</p>

<h2>
YOU EXPRESSLY ACKNOWLEDGE AND AGREE THAT YOU HAVE READ THESE TERMS AND UNDERSTAND ALL RIGHTS, OBLIGATIONS, TERMS AND CONDITIONS SET FORTH HEREIN. BY CLICKING ON THE ACCEPT BUTTON AND/OR CONTINUING TO USE THE SERVICES YOU EXPRESSLY CONSENT TO BE BOUND BY THESE TERMS AND CONDITIONS AND GRANT TO JIBBOOM THE RIGHTS SET FORTH HEREIN. 
</h2>

<p>
MAY 27, 2013
</p>
');

-- --------------------------------------------------------------------------------------------------------

INSERT INTO documents (uuid, language, locale, type_id, sort_order, title, content)
VALUES ('f2191d3f-b9d9-4ae5-98b7-b445de932342', 'en', 'US', 1, 0, 'Login Screen',
'<h2>Login Screen</h2>
<p>The following distinct actions can be performed on the login page:
  <ul>
    <li>Login</li>
    <li>Reset password</li>
    <li>Sign out</li>
    <li>Register</li>
  </ul>
To change the action, select the current action and then the desired action from the available options./p>
<p>Additional topics: session, logging out</p>'
);
INSERT INTO documents (uuid, language, locale, type_id, sort_order, title, content)
VALUES ('6032cf90-f052-489a-83e1-331bd08e93ef', 'en', 'US', 1, 1, 'Screen Layout',
'<h2>Screen Layout</h2>
<p>Phone vs tablet, high level overview of three areas</p>'
);
INSERT INTO documents (uuid, language, locale, type_id, sort_order, title, content)
VALUES ('97d8fc68-8815-48c6-aa3e-44ae7aac0b4f', 'en', 'US', 1, 2, 'Circles',
'<h2>Circles</h2>
<p>In order to see messages from others and post your own messages, you must be a member of a circle.
To become a member, you have to either be invited to someone\'s circle or create your own circle.</p>
<p>To create your own circle, go to Settings > Circle Settings > (Create new circle)</p>'
);
INSERT INTO documents (uuid, language, locale, type_id, sort_order, title, content)
VALUES ('8e3ee11a-f9c4-40c3-97f5-bf1139a402b8', 'en', 'US', 1, 3, 'Compose',
'<h2>Compose</h2>
<p>The compose interface is used to build message that you either have the application speak for you or that you post to a circle.</p>
<p>Talking, posting, dictionaries, favorites, languauge dependencies</p>'
);
INSERT INTO documents (uuid, language, locale, type_id, sort_order, title, content)
VALUES ('81be5cdd-0362-49b8-81f5-8824ff6e22ee', 'en', 'US', 1, 4, 'Settings',
'<h2>Settings</h2>
<p>User, Circle, Legal, logout, version</p>'
);
