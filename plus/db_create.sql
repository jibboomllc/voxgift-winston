-- CREATE DATABASE IF NOT EXISTS `voxcircle`;
-- CREATE USER 'circle_user'@'localhost' IDENTIFIED BY 'c1rcl3';
-- GRANT SELECT, UPDATE, INSERT ON `voxcircle`.* TO 'circle_user'@'localhost';

-- http://www.famkruithof.net/uuid/uuidgen
-- 7fc90d6c-1cf1-4c0c-9a0d-26ecdc152773
-- 

USE `voxcircle`;

DROP TABLE IF EXISTS favorites_dictionary_entry;
DROP TABLE IF EXISTS adhoc_dictionary_entry;

DROP TABLE IF EXISTS event;
DROP TABLE IF EXISTS message;
DROP TABLE IF EXISTS push_device_gcm;
DROP TABLE IF EXISTS push_device_apn;
DROP TABLE IF EXISTS push_device;
DROP TABLE IF EXISTS session;
DROP TABLE IF EXISTS confirmation;
DROP TABLE IF EXISTS circle_users;
DROP TABLE IF EXISTS circle;
DROP TABLE IF EXISTS user_emails;
DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS sponsor;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS circle_user_status;
DROP TABLE IF EXISTS circle_status;
DROP TABLE IF EXISTS push_service_type;
DROP TABLE IF EXISTS event_type;

-- -----------------------------------------------------------------------------
-- reference tables

CREATE TABLE IF NOT EXISTS role (
  id INT NOT NULL PRIMARY KEY
  , name VARCHAR(64)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO role (id, name) VALUES (0, 'center');
INSERT INTO role (id, name) VALUES (1, 'family');
INSERT INTO role (id, name) VALUES (2, 'friend');
INSERT INTO role (id, name) VALUES (3, 'provider');

CREATE TABLE IF NOT EXISTS circle_user_status (
  id INT NOT NULL PRIMARY KEY
  , name VARCHAR(64)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO circle_user_status (id, name) VALUES (0, 'invited');
INSERT INTO circle_user_status (id, name) VALUES (1, 'accepted');
INSERT INTO circle_user_status (id, name) VALUES (2, 'rejected');
INSERT INTO circle_user_status (id, name) VALUES (3, 'unsubscribed');
INSERT INTO circle_user_status (id, name) VALUES (4, 'removed');

CREATE TABLE IF NOT EXISTS circle_status (
  id INT NOT NULL PRIMARY KEY
  , name VARCHAR(64)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO circle_status (id, name) VALUES (0, 'open');
INSERT INTO circle_status (id, name) VALUES (1, 'closed');
INSERT INTO circle_status (id, name) VALUES (2, 'deleted');

CREATE TABLE IF NOT EXISTS push_service_type (
  id INT NOT NULL PRIMARY KEY
  , name VARCHAR(64)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO push_service_type (id, name) VALUES (0, 'GCM');
INSERT INTO push_service_type (id, name) VALUES (1, 'APN');

CREATE TABLE IF NOT EXISTS event_type (
  id INT NOT NULL PRIMARY KEY
  , name VARCHAR(64)
  , datavalue1 VARCHAR(64)
  , datavalue2 VARCHAR(64)
  , datavalue3 VARCHAR(64)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO event_type (id, name, datavalue1, datavalue2) VALUES (0, 'circle_status_changed', 'circle_uuid', 'circle_status_id');
INSERT INTO event_type (id, name, datavalue1, datavalue2, datavalue3) VALUES (1, 'circle_user_status_changed', 'circle_uuid', 'user_uuid', 'circle_user_status_id');

-- -----------------------------------------------------------------------------
-- data tables - user

CREATE TABLE IF NOT EXISTS user (
  uuid VARCHAR(36) NOT NULL PRIMARY KEY
  , fname VARCHAR(64)
  , lname VARCHAR(64)
  , primary_email VARCHAR(128) NOT NULL UNIQUE
  , password VARCHAR(128) NOT NULL DEFAULT ""
  , temp_password VARCHAR(128)
  , language VARCHAR(2) NOT NULL DEFAULT "en" 
  , locale VARCHAR(2) NOT NULL DEFAULT "US"
  , gender VARCHAR(1) NOT NULL DEFAULT "f"
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , temp_password_expire_ts TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- (phone)
-- billing_acct

INSERT INTO user (uuid, fname, lname, primary_email, password, language, locale, gender) VALUES ('067e6162-3b6f-4ae2-a171-2470b63dff00', 'Tom', 'Giesberg', 'tom@giesberg.net', 'k9UG0+12P42xAHYoKHGAUNZG09oB0hdNNqwkA+qKFg0=$VaRqB0SI06cm3Ca9w+JDq49zAJySDllvS7ZOYZK4nq8=', 'en', 'US', 'm');
INSERT INTO user (uuid, fname, lname, primary_email, password, gender) VALUES ('54947df8-0e9e-4471-a2f9-9af509fb5889', 'Melissa', 'Giesberg', 'melissa@giesberg.net', 'iEjz+C3bN3OE4VlDdO1GQGnDnp1+Ps4bs+jDikyfqaM=$rwoUphY2j88i2P0weazW4cyMcqyclAtwFjEwQHbf0RY=', 'f');

INSERT INTO user (uuid, fname, primary_email, password, gender) VALUES ('1a9bcab6-1df1-4e40-ad4d-2b9f84f54632'
  , 'Janie', 'janie@demo.com', 'k9UG0+12P42xAHYoKHGAUNZG09oB0hdNNqwkA+qKFg0=$VaRqB0SI06cm3Ca9w+JDq49zAJySDllvS7ZOYZK4nq8=', 'f');
INSERT INTO user (uuid, fname, primary_email, password, gender) VALUES ('2c9cffe4-8f9f-48d0-abf0-b7fecd8c8fe1'
  , 'Mom', 'mom@demo.com', 'k9UG0+12P42xAHYoKHGAUNZG09oB0hdNNqwkA+qKFg0=$VaRqB0SI06cm3Ca9w+JDq49zAJySDllvS7ZOYZK4nq8=', 'f');
INSERT INTO user (uuid, fname, primary_email, password, gender) VALUES ('3df236cc-ea1a-4dbd-b70c-947c2e95c9e1'
  , 'Dad', 'dad@demo.com', 'k9UG0+12P42xAHYoKHGAUNZG09oB0hdNNqwkA+qKFg0=$VaRqB0SI06cm3Ca9w+JDq49zAJySDllvS7ZOYZK4nq8=', 'm');
INSERT INTO user (uuid, fname, primary_email, password, gender) VALUES ('8b749998-efeb-49b7-b480-84eb4a444cd3'
  , 'Grandmom', 'grandmom@demo.com', 'k9UG0+12P42xAHYoKHGAUNZG09oB0hdNNqwkA+qKFg0=$VaRqB0SI06cm3Ca9w+JDq49zAJySDllvS7ZOYZK4nq8=', 'f');
INSERT INTO user (uuid, fname, primary_email, password, gender) VALUES ('9f7fa857-6683-4d19-b58d-310ca4e2f856'
  , 'Grandpa', 'grandpa@demo.com', 'k9UG0+12P42xAHYoKHGAUNZG09oB0hdNNqwkA+qKFg0=$VaRqB0SI06cm3Ca9w+JDq49zAJySDllvS7ZOYZK4nq8=', 'm');
INSERT INTO user (uuid, fname, primary_email, password, gender) VALUES ('a21a5b63-2ba5-4c8f-b170-e1d124942d6b'
  , 'Susie', 'susie@demo.com', 'k9UG0+12P42xAHYoKHGAUNZG09oB0hdNNqwkA+qKFg0=$VaRqB0SI06cm3Ca9w+JDq49zAJySDllvS7ZOYZK4nq8=', 'f');

-- https://voxgift.com/imageupload?userId=067e6162-3b6f-4ae2-a171-2470b63dff00&userEmail=tom@giesberg.net
-- https://voxgift.com/imageupload?userId=54947df8-0e9e-4471-a2f9-9af509fb5889&userEmail=melissa@giesberg.net

-- https://voxgift.com/imageupload?userId=1a9bcab6-1df1-4e40-ad4d-2b9f84f54632&userEmail=janie@demo.com
-- https://voxgift.com/imageupload?userId=2c9cffe4-8f9f-48d0-abf0-b7fecd8c8fe1&userEmail=mom@demo.com
-- https://voxgift.com/imageupload?userId=3df236cc-ea1a-4dbd-b70c-947c2e95c9e1&userEmail=dad@demo.com
-- https://voxgift.com/imageupload?userId=8b749998-efeb-49b7-b480-84eb4a444cd3&userEmail=grandmom@demo.com
-- https://voxgift.com/imageupload?userId=9f7fa857-6683-4d19-b58d-310ca4e2f856&userEmail=grandpa@demo.com
-- https://voxgift.com/imageupload?userId=a21a5b63-2ba5-4c8f-b170-e1d124942d6b&userEmail=susie@demo.com

CREATE TABLE IF NOT EXISTS user_emails (
  email VARCHAR(128) NOT NULL PRIMARY KEY
  , user_uuid VARCHAR(36) NOT NULL
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , confirmed_ts TIMESTAMP
  , FOREIGN KEY (user_uuid) REFERENCES user(uuid) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('067e6162-3b6f-4ae2-a171-2470b63dff00', 'tom@giesberg.net', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('067e6162-3b6f-4ae2-a171-2470b63dff00', 'tom@voxgift.com', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('54947df8-0e9e-4471-a2f9-9af509fb5889', 'melissa@giesberg.net', '2012-12-01 01:00:00');

INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('1a9bcab6-1df1-4e40-ad4d-2b9f84f54632', 'janie@demo.com', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('2c9cffe4-8f9f-48d0-abf0-b7fecd8c8fe1', 'mom@demo.com', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('3df236cc-ea1a-4dbd-b70c-947c2e95c9e1', 'dad@demo.com', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('8b749998-efeb-49b7-b480-84eb4a444cd3', 'grandmom@demo.com', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('9f7fa857-6683-4d19-b58d-310ca4e2f856', 'grandpa@demo.com', '2012-12-01 01:00:00');
INSERT INTO user_emails (user_uuid, email, confirmed_ts) VALUES ('a21a5b63-2ba5-4c8f-b170-e1d124942d6b', 'susie@demo.com', '2012-12-01 01:00:00');

-- -----------------------------------------------------------------------------
-- data tables - sponsor

CREATE TABLE IF NOT EXISTS sponsor (
  uuid VARCHAR(36) NOT NULL PRIMARY KEY
  , name VARCHAR(64)
  , url VARCHAR(256)
  , email VARCHAR(128)
  , password VARCHAR(128) NOT NULL DEFAULT ""
  , temp_password VARCHAR(128)
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , temp_password_expire_ts TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO sponsor (uuid, name, url, email) VALUES ('9acdfb83-845a-4c3e-8419-237098064b7f', 'Voxgift', 'https://voxgift.com', 'support@voxgift.com');
INSERT INTO sponsor (uuid, name, url, email) VALUES ('4322d500-9be1-4aa6-878a-b0f9f0ea40f1', 'Ronald McDonald House Charities - Austin & Central Texas', 'http://rmhc-austin.org', 'dlesnau@rmhc-austin.org');

-- -----------------------------------------------------------------------------
-- data tables - circle

CREATE TABLE IF NOT EXISTS circle (
  uuid VARCHAR(36) NOT NULL PRIMARY KEY
  , name VARCHAR(64)
  , owner_uuid VARCHAR(36) NOT NULL
  , sponsor_uuid VARCHAR(36) DEFAULT '9acdfb83-845a-4c3e-8419-237098064b7f'
  , status_id INT NOT NULL
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , FOREIGN KEY (owner_uuid) REFERENCES user(uuid)
  , FOREIGN KEY (sponsor_uuid) REFERENCES sponsor(uuid)
  , FOREIGN KEY (status_id) REFERENCES circle_status(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- TODO: billing

INSERT INTO circle (uuid, name, owner_uuid, sponsor_uuid, status_id) VALUES ('db346bc2-7ac3-4c34-9606-44e23aaf7c34', 'Tom\'s Circle-of-care', '54947df8-0e9e-4471-a2f9-9af509fb5889', '9acdfb83-845a-4c3e-8419-237098064b7f', 0);
INSERT INTO circle (uuid, name, owner_uuid, sponsor_uuid, status_id) VALUES ('6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3', 'Janie\'s Circle', '067e6162-3b6f-4ae2-a171-2470b63dff00', '4322d500-9be1-4aa6-878a-b0f9f0ea40f1', 1);

CREATE TABLE IF NOT EXISTS circle_users (
  circle_uuid VARCHAR(36) NOT NULL
  , user_uuid VARCHAR(36) NOT NULL
  , role_id INT NOT NULL
  , status_id INT NOT NULL
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , PRIMARY KEY (circle_uuid, user_uuid)
  , FOREIGN KEY (circle_uuid) REFERENCES circle(uuid) ON DELETE CASCADE
  , FOREIGN KEY (user_uuid) REFERENCES user(uuid) ON DELETE CASCADE
  , FOREIGN KEY (role_id) REFERENCES role(id)
  , FOREIGN KEY (status_id) REFERENCES circle_user_status(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO circle_users (circle_uuid, user_uuid, role_id, status_id) VALUES ('db346bc2-7ac3-4c34-9606-44e23aaf7c34', '067e6162-3b6f-4ae2-a171-2470b63dff00', 0, 1);
INSERT INTO circle_users (circle_uuid, user_uuid, role_id, status_id) VALUES ('db346bc2-7ac3-4c34-9606-44e23aaf7c34', '54947df8-0e9e-4471-a2f9-9af509fb5889', 1, 1);

INSERT INTO circle_users (circle_uuid, user_uuid, role_id, status_id) VALUES ('6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3', '1a9bcab6-1df1-4e40-ad4d-2b9f84f54632', 0, 1);
INSERT INTO circle_users (circle_uuid, user_uuid, role_id, status_id) VALUES ('6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3', '2c9cffe4-8f9f-48d0-abf0-b7fecd8c8fe1', 1, 1);
INSERT INTO circle_users (circle_uuid, user_uuid, role_id, status_id) VALUES ('6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3', '3df236cc-ea1a-4dbd-b70c-947c2e95c9e1', 1, 1);
INSERT INTO circle_users (circle_uuid, user_uuid, role_id, status_id) VALUES ('6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3', '8b749998-efeb-49b7-b480-84eb4a444cd3', 1, 1);
INSERT INTO circle_users (circle_uuid, user_uuid, role_id, status_id) VALUES ('6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3', '9f7fa857-6683-4d19-b58d-310ca4e2f856', 1, 1);
INSERT INTO circle_users (circle_uuid, user_uuid, role_id, status_id) VALUES ('6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3', 'a21a5b63-2ba5-4c8f-b170-e1d124942d6b', 2, 1);

-- -----------------------------------------------------------------------------
-- data tables - confirmation

CREATE TABLE IF NOT EXISTS confirmation (
  uuid VARCHAR(36) NOT NULL PRIMARY KEY
  , user_uuid VARCHAR(36) NOT NULL
  , circle_uuid VARCHAR(36)
  , email VARCHAR(128)
  , create_ts TIMESTAMP DEFAULT NOW()
  , expire_ts TIMESTAMP
  , FOREIGN KEY (user_uuid) REFERENCES user(uuid) ON DELETE CASCADE
  , FOREIGN KEY (circle_uuid) REFERENCES circle(uuid) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------------------------
-- data tables - session

CREATE TABLE IF NOT EXISTS session (
  uuid VARCHAR(36) NOT NULL PRIMARY KEY
  , user_uuid VARCHAR(36) NOT NULL
  , create_ts TIMESTAMP DEFAULT NOW()
  , expire_ts TIMESTAMP
  , FOREIGN KEY (user_uuid) REFERENCES user(uuid) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -----------------------------------------------------------------------------
-- data tables - push_device
-- APA91bHI_LjBYM8-lcbshSu2NTGnMxuWgm4U783YUPyNWk9EDBdUKCXgUebhBTdAtlThwoQaAD9F3ADmEjZLMdhjrWN9Qj_COozelx5NnjRnS88D6alot6XYtUQi9cQOnW6Ax91PeQWUioB9GogJiDb_dLmh2KEUKA

CREATE TABLE IF NOT EXISTS push_device (
  uuid VARCHAR(256) NOT NULL
  , push_service_type_id INT NOT NULL DEFAULT '-1'
  , user_uuid VARCHAR(36)
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , PRIMARY KEY (uuid, push_service_type_id)
  , FOREIGN KEY (push_service_type_id) REFERENCES push_service_type(id) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=ascii;

-- -----------------------------------------------------------------------------
-- data tables - messages

CREATE TABLE IF NOT EXISTS message (
  uuid VARCHAR(36) NOT NULL PRIMARY KEY
  , circle_uuid VARCHAR(36) NOT NULL
  , user_uuid VARCHAR(36) NOT NULL
  , message TEXT
  , attachment_mime_type varchar(255)
  , attachment_size int
  , attachment_data blob
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , FOREIGN KEY (circle_uuid) REFERENCES circle(uuid) ON DELETE CASCADE
  , FOREIGN KEY (user_uuid) REFERENCES user(uuid) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO message (uuid, circle_uuid, user_uuid, message, create_ts) VALUES ('762f0c70-0da9-4346-a871-a195b7bf1839', 'db346bc2-7ac3-4c34-9606-44e23aaf7c34', '067e6162-3b6f-4ae2-a171-2470b63dff00',
  'Can someone bring me a book from home? Say Moby Dick?',
  NOW() - INTERVAL 120 SECOND);
INSERT INTO message (uuid, circle_uuid, user_uuid, message, create_ts) VALUES ('5bb3bc18-d34a-4182-b28d-250b3d4a584f', 'db346bc2-7ac3-4c34-9606-44e23aaf7c34', '54947df8-0e9e-4471-a2f9-9af509fb5889',
  "I\'ll bring it to you!",
  NOW() - INTERVAL 90 SECOND);
INSERT INTO message (uuid, circle_uuid, user_uuid, message, create_ts) VALUES ('4bb3bc18-d34a-4182-b28d-250b3d4a584e', 'db346bc2-7ac3-4c34-9606-44e23aaf7c34', '54947df8-0e9e-4471-a2f9-9af509fb5889',
  'Call me Ishmael. Some years ago -- never mind how long precisely -- having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world.', 
  NOW() - INTERVAL 60 SECOND);
INSERT INTO message (uuid, circle_uuid, user_uuid, message, create_ts) VALUES ('862f0c70-0da9-4346-a871-a195b7bf1830', 'db346bc2-7ac3-4c34-9606-44e23aaf7c34', '067e6162-3b6f-4ae2-a171-2470b63dff00',
  'Thank you Melissa',  NOW());

-- ----------------------

INSERT INTO message (uuid, circle_uuid, user_uuid, message, create_ts) VALUES ('05e22944-30fb-4a80-9ffb-38d2d631288b', '6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3'
, '2c9cffe4-8f9f-48d0-abf0-b7fecd8c8fe1', 'Janie\'s surgery is scheduled for tomorrow morning at 8:00. I\'ll be sending out updates as soon as we know anything.',  NOW() - INTERVAL 600 SECOND);

INSERT INTO message (uuid, circle_uuid, user_uuid, message, create_ts) VALUES ('1f9b51f7-19c0-4e58-8932-0fcf07bc3938', '6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3'
, '8b749998-efeb-49b7-b480-84eb4a444cd3', 'Good luck, Janie! I know you\'ll be glad to have that behind you!',  NOW() - INTERVAL 570 SECOND);

INSERT INTO message (uuid, circle_uuid, user_uuid, message, create_ts) VALUES ('267f0c70-9da9-9346-9871-9195b7bf1938', '6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3'
, 'a21a5b63-2ba5-4c8f-b170-e1d124942d6b', 'Get well quick - everyone misses you at school!',  NOW() - INTERVAL 540 SECOND);

INSERT INTO message (uuid, circle_uuid, user_uuid, message, create_ts) VALUES ('3d1ca2fa-5889-49fe-8ca2-97e19f4bde19', '6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3'
, '1a9bcab6-1df1-4e40-ad4d-2b9f84f54632', 'Thanks everyone!',  NOW() - INTERVAL 510 SECOND);

INSERT INTO message (uuid, circle_uuid, user_uuid, message, create_ts) VALUES ('4e2ca2fa-5889-49fe-8ca2-97e19f4bde19', '6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3'
, '1a9bcab6-1df1-4e40-ad4d-2b9f84f54632', 'Can someone bring me my phone charger?',  NOW() - INTERVAL 180 SECOND);

INSERT INTO message (uuid, circle_uuid, user_uuid, message, create_ts) VALUES ('59cf9713-c08a-4b84-831e-d9be9a2b3ca5', '6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3'
, '9f7fa857-6683-4d19-b58d-310ca4e2f856', 'Hugs and kisses Janie. We\'ll see you tomorrow.',  NOW() - INTERVAL 150 SECOND);

INSERT INTO message (uuid, circle_uuid, user_uuid, message, create_ts) VALUES ('653ca2fa-5889-49fe-8ca2-97e19f4bde19', '6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3'
, '3df236cc-ea1a-4dbd-b70c-947c2e95c9e1', 'I\'ll bring the charger when I come to see you in a little bit.',  NOW() - INTERVAL 120 SECOND);

INSERT INTO message (uuid, circle_uuid, user_uuid, message, create_ts) VALUES ('76f22944-30fb-4a80-9ffb-38d2d631288b', '6e9d3c62-f8b2-43f9-85e8-eebea38c9ef3'
, '2c9cffe4-8f9f-48d0-abf0-b7fecd8c8fe1', 'They are taking Janie to X-ray.',  NOW());

-- -----------------------------------------------------------------------------
-- data tables - event

CREATE TABLE IF NOT EXISTS event (
  ts TIMESTAMP DEFAULT NOW()
  , type_id INT NOT NULL
  , data1 VARCHAR(64)
  , data2 VARCHAR(64)
  , data3 VARCHAR(64)
  , FOREIGN KEY (type_id) REFERENCES event_type(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS adhoc_dictionary_entry (
  uuid VARCHAR(36) NOT NULL
  , language VARCHAR(2) NOT NULL
  , locale VARCHAR(2) NOT NULL
  , translation VARCHAR(512)
  , created_by_user_uuid VARCHAR(36)
  , usage_count INT DEFAULT 0 
  , create_ts TIMESTAMP DEFAULT NOW()
  , last_usage_ts TIMESTAMP
  , delete_ts TIMESTAMP
  , PRIMARY KEY (uuid)
  , FOREIGN KEY (created_by_user_uuid) REFERENCES user(uuid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--   , UNIQUE (translation, language, locale)

CREATE TABLE IF NOT EXISTS favorites_dictionary_entry (
  uuid VARCHAR(36) NOT NULL
  , user_uuid VARCHAR(36)
  , language VARCHAR(2) NOT NULL
  , locale VARCHAR(2) NOT NULL
  , translation VARCHAR(512)
  , sort_order INT DEFAULT 0
  , usage_count INT DEFAULT 0
  , create_ts TIMESTAMP DEFAULT NOW()
  , last_usage_ts TIMESTAMP
  , delete_ts TIMESTAMP
  , PRIMARY KEY (uuid)
  , FOREIGN KEY (user_uuid) REFERENCES user(uuid) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

