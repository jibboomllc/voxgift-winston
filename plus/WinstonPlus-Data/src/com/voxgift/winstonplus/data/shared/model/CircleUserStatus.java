package com.voxgift.winstonplus.data.shared.model;

public class CircleUserStatus {
	public final static int UNDEFINED = -1;
	public final static int INVITED = 0;
	public final static int ACCEPTED = 1;
	public final static int REJECTED = 2;
	public final static int UNSUBSCRIBED = 3;
	public final static int REMOVED = 4;
}
