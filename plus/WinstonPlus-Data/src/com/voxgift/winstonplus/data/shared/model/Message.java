package com.voxgift.winstonplus.data.shared.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Message implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String circleId;
	private String userId;
	private String message;
	private Date createTs;
	private String userName;
	private Integer userRoleId;
	private String elapsedTime;

	public Message() {}

	public Message (String id, String circleId, String userId, String message, Date createTs){
		this.id = id;
		this.circleId = circleId;
		this.userId = userId;
		this.message = message;
		this.createTs = createTs;
		this.userName = "";
		this.userRoleId = 0;
        this.elapsedTime = "";
	}
	public Message (String circleId, String userId, String message){
		this.id = String.valueOf(UUID.randomUUID());
		this.circleId = circleId;
		this.userId = userId;
		this.message = message;
	}
    public MessageDetails getDetails() {
    	return new MessageDetails (id, circleId, userId, message, userName, userRoleId, elapsedTime);
    }
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCircleId() {
		return circleId;
	}
	public void setCircleId(String circleId) {
		this.circleId = circleId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getCreateTs() {
		return createTs;
	}
	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String fname, String lname, String email) {
		if(fname != null && !fname.trim().isEmpty()) {
            this.userName = fname.trim();

    		if(lname != null && !lname.trim().isEmpty()) {
                this.userName += " " + lname.trim();
            }
		} else if (lname != null && !lname.trim().isEmpty()) {
                this.userName = lname.trim();
        } else if (email != null && !email.trim().isEmpty()) {
            this.userName = email.trim();
        } else {
            this.userName = getUserId();
        }
	}

	public Integer getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(Integer userRoleId) {
        this.userRoleId = userRoleId;
    }
    
	public String getElapsedTime() {
		return elapsedTime;
	}
	public void setElapsedTime(Date nowTs) {
        long sec = (nowTs.getTime() - getCreateTs().getTime()) / 1000L;
        long min = sec / 60L;
        if (min == 0L) {
    		this.elapsedTime = String.valueOf(sec) + "s";
        } else {
            long hr = min / 60L;
            if (hr == 0L) {
                this.elapsedTime = String.valueOf(min) + "m";
            } else {
                long days = hr / 24L;
                if (days == 0L) {
                    this.elapsedTime = String.valueOf(hr) + "h";
                } else {
                    long weeks = days / 7L;
                    if (weeks == 0L) {
                        this.elapsedTime = String.valueOf(days) + "d";
                    } else {
                        DateFormat df = new SimpleDateFormat("MMM-d");
                        this.elapsedTime = df.format(getCreateTs().getTime());
                    }
                }
            }
        }
	}
	
}
