package com.voxgift.winstonplus.data.shared.model;

import java.io.Serializable;

public class Circle implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String name;
	private String ownerId;
	private String sponsorId;
	private int statusId;

	public Circle() {}
	
	public Circle(String id, String name, String ownerId, String sponsorId, int statusId){
		this.id = id;
		this.name = name;
		this.ownerId = ownerId;
		this.sponsorId = sponsorId;
		this.statusId = statusId;
	}

    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public String getSponsorId() {
		return sponsorId;
	}
	public void setSponsorId(String sponsorId) {
		this.sponsorId = sponsorId;
	}
    public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
}
