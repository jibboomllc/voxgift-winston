package com.voxgift.winstonplus.data.shared.model;

import java.io.Serializable;
import java.util.List;

public class DictionaryDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private String contextId;
	private String language;
	private String locale;
	private List<DictionaryEntryDetails> entries;

	public DictionaryDetails() {}
	
	public DictionaryDetails (String contextId, String language, String locale, List<DictionaryEntryDetails> entries){
		this.contextId = contextId;
		this.language = language;
		this.locale = locale;
		this.entries = entries;
	}
	
	public String getContextId() {
		return contextId;
	}
	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public List<DictionaryEntryDetails> getEntries() {
		return entries;
	}
	public void setEntries(List<DictionaryEntryDetails> entries) {
		this.entries = entries;
	}
}
