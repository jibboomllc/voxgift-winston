package com.voxgift.winstonplus.data.shared.model;

import java.io.Serializable;
import java.util.Date;

public class UserEmailDetails implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String email;
	private Date confirmed;

	public UserEmailDetails() {}
	
	public UserEmailDetails (String email, Date confirmed){
		this.email = email;
		this.confirmed = confirmed;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getConfirmed() {
		return confirmed;
	}
	public void setConfirmed(Date confirmed) {
		this.confirmed = confirmed;
	}
}
