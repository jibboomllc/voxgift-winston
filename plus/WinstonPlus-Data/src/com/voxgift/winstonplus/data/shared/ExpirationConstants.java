package com.voxgift.winstonplus.data.shared;

public class ExpirationConstants {

	public static int SESSION_EXPIRATION_IN_DAYS = 3650;
	public static int TEMP_PASSWORD_EXPIRATION_IN_HOURS = 2;
	public static int REGISTRATION_CONFIRMATION_EXPIRATION_IN_HOURS = 2;
	public static int INVITATION_CONFIRMATION_EXPIRATION_IN_DAYS = 7;

}
