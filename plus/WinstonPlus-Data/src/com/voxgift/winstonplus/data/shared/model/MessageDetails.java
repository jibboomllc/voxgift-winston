package com.voxgift.winstonplus.data.shared.model;

import java.io.Serializable;

public class MessageDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String circleId;
	private String userId;
	private String message;
	private String userName;
	private Integer userRoleId;
	private String elapsedTime;

	public MessageDetails() {}
	
	public MessageDetails (String id, String circleId, String userId, String message, String userName, Integer userRoleId, String elapsedTime){
		this.id = id;
		this.circleId = circleId;
		this.userId = userId;
		this.message = message;
		this.userName = userName;
		this.userRoleId = userRoleId;
        this.elapsedTime = elapsedTime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCircleId() {
		return circleId;
	}
	public void setCircleId(String circleId) {
		this.circleId = circleId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
        this.userName = userName;
	}

	public Integer getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(Integer userRoleId) {
        this.userRoleId = userRoleId;
    }

	public String getElapsedTime() {
		return elapsedTime;
	}
	public void setElapsedTime(String elapsedTime) {
        this.elapsedTime = elapsedTime;
	}
	
}
