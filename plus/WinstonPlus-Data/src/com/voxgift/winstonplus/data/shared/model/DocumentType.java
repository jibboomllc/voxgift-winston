package com.voxgift.winstonplus.data.shared.model;

public class DocumentType {
	public final static int UNDEFINED = -1;
	public final static int LEGAL = 0;
	public final static int HELP = 1;
}
