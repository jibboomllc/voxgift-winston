package com.voxgift.winstonplus.data.shared.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String primaryEmail;
	private String password;
	private String firstName;
	private String lastName;
	private String language;
	private String locale;
	private String gender;
	private List<UserEmailDetails> userEmails;
	private Map<String, UserCircleDetails> userCircles;

	public UserDetails() {}
	
	public UserDetails (String id, String primaryEmail, String password, String firstName, String lastName, String language, String locale, String gender){
		this.id = id;
		this.primaryEmail = primaryEmail;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.language = language;
		this.locale = locale;
		this.gender = gender;
		this.userEmails = new ArrayList<UserEmailDetails>();
		this.userCircles = new HashMap<String, UserCircleDetails>();
	}

    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPrimaryEmail() {
		return primaryEmail;
	}
	public void setPrimaryEmail(String primaryEmail) {
		this.primaryEmail = primaryEmail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
    public List<UserEmailDetails> getUserEmails() {
		return userEmails;
	}
	public void setUserEmails(List<UserEmailDetails> userEmails) {
		this.userEmails = userEmails;
	}
    public Map<String, UserCircleDetails> getUserCircles() {
		return userCircles;
	}
	public void setUserCircles(Map<String, UserCircleDetails> userCircles) {
		this.userCircles = userCircles;
	}
}
