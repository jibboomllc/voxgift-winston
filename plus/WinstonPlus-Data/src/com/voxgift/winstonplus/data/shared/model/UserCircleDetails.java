package com.voxgift.winstonplus.data.shared.model;

import java.io.Serializable;

public class UserCircleDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private String circleId;
	private String circleName;
	private String sponsorId;
	private String centerId;
	private int circleStatusId;
	private int roleId;
	private int statusId;

	public UserCircleDetails() {}
	
	public UserCircleDetails (String circleId, String circleName, String sponsorId, String centerId, int circleStatusId, int roleId, int statusId){
		this.circleId = circleId;
		this.circleName = circleName;
		this.sponsorId = sponsorId;
		this.centerId = centerId;
		this.circleStatusId = circleStatusId;
		this.roleId = roleId;
		this.statusId = statusId;
	}

    public String getCircleId() {
		return circleId;
	}
	public void setCircleId(String circleId) {
		this.circleId = circleId;
	}
	public String getCircleName() {
		return circleName;
	}
	public void setCircleName(String circleName) {
		this.circleName = circleName;
	}
	public String getSponsorId() {
		return sponsorId;
	}
	public void setSponsorId(String sponsorId) {
		this.sponsorId = sponsorId;
	}
	public String getCenterId() {
		return centerId;
	}
	public void setCenterId(String centerId) {
		this.centerId = centerId;
	}
    public int getCircleStatusId() {
		return circleStatusId;
	}
	public void setCircleStatusId(int circleStatusId) {
		this.circleStatusId = circleStatusId;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
}
