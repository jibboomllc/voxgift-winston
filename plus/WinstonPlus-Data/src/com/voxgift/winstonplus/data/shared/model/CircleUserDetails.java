package com.voxgift.winstonplus.data.shared.model;

import java.io.Serializable;

public class CircleUserDetails implements Serializable {
	private static final long serialVersionUID = 1L;

	private String userId;
	private String email;
	private Boolean emailConfirmed;
	private String firstName;
	private String lastName;
	private int roleId;
	private int statusId;

	public CircleUserDetails() {}

	public CircleUserDetails (String userId, String email, Boolean emailConfirmed, String firstName, String lastName, int roleId, int statusId) {
		this.userId = userId;
		this.email = email;
		this.emailConfirmed = emailConfirmed;
		this.firstName = firstName;
		this.lastName = lastName;
		this.roleId = roleId;
		this.statusId = statusId;
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getEmailConfirmed() {
		return emailConfirmed;
	}
	public void setEmailConfirmed(Boolean emailConfirmed) {
		this.emailConfirmed = emailConfirmed;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getStatusId() {
		return statusId;
	}
	public void setStatusId(int statusId) {
		this.statusId = statusId;
	}
}
