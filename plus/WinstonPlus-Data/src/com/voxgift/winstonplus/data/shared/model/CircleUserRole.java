package com.voxgift.winstonplus.data.shared.model;

public class CircleUserRole {
	public final static int UNDEFINED = -1;
	public final static int CENTER = 0;
	public final static int FAMILY = 1;
	public final static int FRIEND = 2;
	public final static int PROVIDER = 3;
}
