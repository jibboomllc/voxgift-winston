package com.voxgift.winstonplus.data.shared.model;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

public class User implements Serializable{

	private static final long serialVersionUID = 1L;

	private String id;
	private String firstName;
	private String lastName;
	private String primaryEmail;
	private String password;
	private String language;
	private String locale;
	private String gender;
	private Date createTs;
	
	public User() {}
	
	public User (String id, String firstName, String lastName, String primaryEmail, String password, String language, String locale, String gender, Date createTs){
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.primaryEmail = primaryEmail;
		this.password = password;
		this.language = language;
		this.locale = locale;
		this.gender = gender;
		this.createTs = createTs;
	}

	public User (String firstName, String lastName, String primaryEmail, String password, String language, String locale, String gender){
		this.id = String.valueOf(UUID.randomUUID());
		this.firstName = firstName;
		this.lastName = lastName;
		this.primaryEmail = primaryEmail;
		this.password = password;
		this.language = language;
		this.locale = locale;
		this.gender = gender;
	}
	
	public UserDetails getDetails () {
		return new UserDetails (id, firstName, lastName, primaryEmail, password, language, locale, gender);
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getPrimaryEmail() {
		return primaryEmail;
	}
	public void setPrimaryEmail(String primaryEmail) {
		this.primaryEmail = primaryEmail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreateTs() {
		return createTs;
	}
	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}

	public User cloneMe()  {
		try {
			return (User) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;  
		}
	}
	
	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", primaryEmail=" + primaryEmail
				+ ", password=" + password + ", createTs=" + createTs + ", id=" + id
				+ "]";
	}

}
