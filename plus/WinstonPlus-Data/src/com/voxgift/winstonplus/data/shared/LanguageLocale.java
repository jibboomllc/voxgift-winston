package com.voxgift.winstonplus.data.shared;

public class LanguageLocale {
	private String language;
	private String locale;
	private String label;

	public LanguageLocale (String language, String locale, String label) {
		this.language = language;
		this.locale = locale;
		this.label = label;			
	}

	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getLocale() {
		return locale;
	}
	public void setLocale(String locale) {
		this.locale = locale;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
}
