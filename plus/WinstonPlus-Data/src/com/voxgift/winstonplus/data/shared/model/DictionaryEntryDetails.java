package com.voxgift.winstonplus.data.shared.model;

import java.io.Serializable;

public class DictionaryEntryDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	private String entryId;
	private String entryText;
	private int sortOrder;

	public DictionaryEntryDetails() {}
	
	public DictionaryEntryDetails (String entryId, String entryText, int sortOrder){
		this.entryId = entryId;
		this.entryText = entryText;
		this.sortOrder = sortOrder;
	}
	
	public String getEntryId() {
		return entryId;
	}
	public void setEntryId(String entryId) {
		this.entryId = entryId;
	}
	public String getEntryText() {
		return entryText;
	}
	public void setEntryText(String entryText) {
		this.entryText = entryText;
	}
	public int getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(int sortOrder) {
		this.sortOrder = sortOrder;
	}
}

