package com.voxgift.winstonplus.data.shared.model;

import java.util.ArrayList;
import java.util.List;

public class CircleDetails extends Circle {

	private static final long serialVersionUID = 1L;

	private List<CircleUserDetails> circleUsers;

	public CircleDetails() {}
	
	public CircleDetails (String id, String name, String ownerId, String sponsorId, int statusId){
		super(id, name, ownerId, sponsorId, statusId);
		this.circleUsers = new ArrayList<CircleUserDetails>();
	}

    public List<CircleUserDetails> getCircleUsers() {
		return circleUsers;
	}
	public void setCircleUsers(List<CircleUserDetails> circleUsers) {
		this.circleUsers = circleUsers;
	}
}
