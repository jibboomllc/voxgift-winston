package com.voxgift.winstonplus.data.shared.model;

public class CircleStatus {
	public final static int OPEN = 0;
	public final static int CLOSED = 1;
	public final static int DELETED = 2;
}
