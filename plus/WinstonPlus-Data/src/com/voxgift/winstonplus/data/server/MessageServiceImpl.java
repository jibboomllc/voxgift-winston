package com.voxgift.winstonplus.data.server;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import com.voxgift.winstonplus.data.client.rpc.MessageService;
import com.voxgift.winstonplus.data.shared.model.Message;
import com.voxgift.winstonplus.data.shared.model.MessageDetails;

public class MessageServiceImpl extends JdbcResource implements MessageService {

	private final static Logger LOGGER = Logger.getLogger(MessageServiceImpl.class.getName()); 

	static final String PUSH_SERVER_URL = "https://voxgift.com/push";

	@Override
	public MessageDetails[] getMessages(String circleId, Date startDate, Integer limit) {

		// TODO Return a List instead of an array
		// TODO use date and limit

		List<MessageDetails> messageDetailsList = listMessages(circleId);

		MessageDetails[] messageDetails = new MessageDetails[messageDetailsList.size()];
		for (int i = 0; i < messageDetailsList.size(); i++) {
			messageDetails[i] = messageDetailsList.get(i);
		}
		return messageDetails;
	}

	public List<MessageDetails> listMessages(final String circleId) {
		return doList(circleId, "m.create_ts DESC");
	}

	private List<MessageDetails> doList(final String circleId, final String orderBy) {
		return withDB(new JDBCRunner<List<MessageDetails>>(){
			@Override
			public List<MessageDetails> run(Connection connection) throws Exception {

				List<MessageDetails> result = new ArrayList<MessageDetails>();				
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					String query = 
							"SELECT m.uuid, m.user_uuid, m.message, m.create_ts, u.fname, u.lname, u.primary_email, cu.role_id" 
									+ " FROM message AS m "
									+ " LEFT JOIN circle_users AS cu ON m.user_uuid = cu.user_uuid AND cu.circle_uuid = m.circle_uuid "
									+ " LEFT JOIN user AS u ON m.user_uuid = u.uuid "
									+ " WHERE m.circle_uuid = ? "
									+ (orderBy != null ? " ORDER BY " + orderBy + ";" : ";");

					statement = connection.prepareStatement(query);
					statement.setString(1, circleId);
					
					//LOGGER.info(query + ", " + circleId);
					rs = statement.executeQuery();
					
					if (rs != null) {
					Date now = Calendar.getInstance().getTime();
					while (rs.next()) {
						Message message = new Message(rs.getString("m.uuid")
								, circleId
								, rs.getString("m.user_uuid")
								, rs.getString("m.message")
								, rs.getTimestamp("m.create_ts"));
						message.setUserName(rs.getString("u.fname"), rs.getString("u.lname"), rs.getString("u.primary_email"));
						message.setUserRoleId(rs.getInt("cu.role_id"));
						message.setElapsedTime(now);

						result.add(message.getDetails());
					}
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public String postMessage(final String circleId, final String userId, final String messageText) {
		String messageId = saveMessage(circleId, userId, messageText);
		if (messageId != null) {
			pushMessage(circleId, userId, messageId);
		}
		return messageId;
	}

	@Override
	public String saveMessage(final String circleId, final String userId, final String messageText) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {

				String result = null;
				PreparedStatement statement = null;
				try {
					String uuid = String.valueOf(UUID.randomUUID());
					final String query = 
							"INSERT INTO message (uuid, circle_uuid, user_uuid, message)"
									+ " VALUES (?, ?, ?, ?)";
					statement = connection.prepareStatement(query);
					statement.setString(1, uuid);
					statement.setString(2, circleId);
					statement.setString(3, userId);
					statement.setString(4, messageText);

					if (statement.executeUpdate() == 1) {
						result = uuid;
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Boolean pushMessage(final String circleId, final String userId, final String messageId) {
		Boolean result = true;

		String serverUrl = PUSH_SERVER_URL + "/apn/send";
		Map<String, String> params = new HashMap<String, String>();
		params.put("circleId", circleId);
		params.put("userId", userId);
		params.put("messageId", messageId);
		try {
			Utilities.post(serverUrl, params);
		} catch (IOException e) {
			result = false;
		}

		serverUrl = PUSH_SERVER_URL + "/gcm/send";
		try {
			Utilities.post(serverUrl, params);
		} catch (IOException e) {
			result = false;
		}

		return result;
	}
}
