package com.voxgift.winstonplus.data.server;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import com.voxgift.winstonplus.data.client.rpc.CircleService;
import com.voxgift.winstonplus.data.shared.ExpirationConstants;
import com.voxgift.winstonplus.data.shared.model.CircleDetails;
import com.voxgift.winstonplus.data.shared.model.CircleUserDetails;
import com.voxgift.winstonplus.data.shared.model.CircleUserRole;
import com.voxgift.winstonplus.data.shared.model.CircleUserStatus;
import com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails;

public class CircleServiceImpl extends JdbcResource implements CircleService {

	private final static Logger LOGGER = Logger.getLogger(CircleServiceImpl.class.getName()); 

	private final static String DEFAULT_SPONSOR_ID = "9acdfb83-845a-4c3e-8419-237098064b7f";
	
	@Override
	public CircleDetails getCircle(final String circleId) {
		return withDB(new JDBCRunner<CircleDetails>(){
			@Override
			public CircleDetails run(Connection connection) throws Exception {
				CircleDetails result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					final String query = 
							"SELECT * FROM circle AS c WHERE c.uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, circleId);

					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						result = new CircleDetails(rs.getString("c.uuid")
								, rs.getString("c.name")
								, rs.getString("c.owner_uuid")
								, rs.getString("c.sponsor_uuid")
								, rs.getInt("c.status_id")
								);

						final String query2 = "SELECT "
								+ "  u.uuid"
								+ "  , u.primary_email"
								+ "  , (ue.confirmed_ts <> 0) AS email_confirmed"
								+ "  , u.fname"
								+ "  , u.lname"
								+ "  , cu.role_id"
								+ "  , cu.status_id"
								+ " FROM"
								+ "  circle_users AS cu"
								+ "  LEFT JOIN user AS u"
								+ "  ON u.uuid = cu.user_uuid"
								+ "  LEFT JOIN user_emails AS ue"
								+ "  ON u.primary_email = ue.email"
								+ " WHERE"
								+ "  cu.circle_uuid = ?"
								+ " ORDER BY"
								+ "  u.lname"
								+ "  , u.fname";
						statement = connection.prepareStatement(query2);
						statement.setString(1, circleId);

						rs = statement.executeQuery();

						if (rs != null) {
							List<CircleUserDetails> circleUsers = new ArrayList<CircleUserDetails>();
							while (rs.next()) {
								CircleUserDetails userDetails = new CircleUserDetails(rs.getString("u.uuid")
										, rs.getString("u.primary_email")
										, rs.getBoolean("email_confirmed")
										, rs.getString("u.fname")
										, rs.getString("u.lname")
										, rs.getInt("cu.role_id")
										, rs.getInt("cu.status_id")
										);
								circleUsers.add(userDetails);
							}
							result.setCircleUsers(circleUsers);
						}
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public String createCircle(final String ownerId, final String name, final int statusId, final int ownerRoleId) {
		return createCircle(ownerId, name, statusId, ownerRoleId, DEFAULT_SPONSOR_ID);
	}

	@Override
	public String createCircle(final String ownerId, final String name, final int statusId, final int ownerRoleId, final String sponsorId) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = null;
				PreparedStatement statement = null;
				try {
					String circleId = String.valueOf(UUID.randomUUID());
					final String query = 
							"INSERT INTO circle (uuid, owner_uuid, name, status_id, sponsor_uuid)"
									+ " VALUES (?, ?, ?, ?, ?)";
					statement = connection.prepareStatement(query);
					statement.setString(1, circleId);
					statement.setString(2, ownerId);
					statement.setString(3, name);
					statement.setInt(4, statusId);
					statement.setString(5, sponsorId);

					if (statement.executeUpdate() == 1) {
						result = circleId;

						if (ownerRoleId != CircleUserRole.UNDEFINED) {
							final String query2 = 
									"INSERT INTO circle_users (circle_uuid, user_uuid, role_id, status_id)"
											+ " VALUES (?, ?, ?, ?)";
							statement = connection.prepareStatement(query2);
							statement.setString(1, circleId);
							statement.setString(2, ownerId);
							statement.setInt(3, ownerRoleId);
							statement.setInt(4, CircleUserStatus.ACCEPTED);
							statement.executeUpdate();					
						}
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {

					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public Boolean updateCircleName(final String circleId, final String name) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				boolean result = false;
				PreparedStatement statement = null;
				try {
					final String query = 
							"UPDATE circle SET"
									+ " name = ?"
									+ " WHERE uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, name);
					statement.setString(2, circleId);

					if (statement.executeUpdate() == 1) {
						result = true;
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public Boolean updateCircleStatus(final String circleId, final int statusId) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				boolean result = false;
				PreparedStatement statement = null;
				try {
					final String query = 
							"UPDATE circle SET"
									+ " status_id = ?"
									+ " WHERE uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setInt(1, statusId);
					statement.setString(2, circleId);

					if (statement.executeUpdate() == 1) {
						result = true;
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public CircleUserStatusDetails getCircleUserStatus(final String circleId, final String userId) {
		return withDB(new JDBCRunner<CircleUserStatusDetails>(){
			@Override
			public CircleUserStatusDetails run(Connection connection) throws Exception {
				CircleUserStatusDetails result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					final String query = "SELECT "
							+ "  u.primary_email"
							+ "  , u.fname"
							+ "  , u.lname"
							+ "  , cu.role_id"
							+ "  , cu.status_id"
							+ " FROM"
							+ "  circle_users AS cu"
							+ "  LEFT JOIN user AS u"
							+ "  ON u.uuid = cu.user_uuid"
							+ " WHERE"
							+ "  cu.circle_uuid = ?"
							+ " AND"
							+ "  cu.user_uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, circleId);
					statement.setString(2, userId);

					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						result = new CircleUserStatusDetails(userId
								, rs.getString("u.primary_email")
								, rs.getString("u.fname")
								, rs.getString("u.lname")
								, rs.getInt("cu.role_id")
								, rs.getInt("cu.status_id")
								);
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Boolean setCircleUserStatus(final String circleId, final String userId, final int statusId) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				boolean result = false;
				PreparedStatement statement = null;
				try {
					final String query = 
							"UPDATE circle_users AS cu SET"
									+ "  cu.status_id = ?"
									+ " WHERE"
									+ "  cu.circle_uuid = ?"
									+ " AND"
									+ "  cu.user_uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setInt(1, statusId);
					statement.setString(2, circleId);
					statement.setString(3, userId);

					if (statement.executeUpdate() == 1) {
						result = true;
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Boolean updateUserRole(final String circleId, final String userId, final int roleId) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				boolean result = false;
				PreparedStatement statement = null;
				try {
					final String query = 
							"UPDATE circle_users AS cu SET"
									+ "  cu.role_id = ?"
									+ " WHERE"
									+ "  cu.circle_uuid = ?"
									+ " AND"
									+ "  cu.user_uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setInt(1, roleId);
					statement.setString(2, circleId);
					statement.setString(3, userId);

					if (statement.executeUpdate() == 1) {
						result = true;
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Integer inviteUsersByEmails(final String circleId, final String[] emails, final int roleId) {
		for (int i = 0; i < emails.length; i++) {
			if (inviteUser(circleId, null, emails[i], null, null, roleId) == null) {
				return i;
			}
		}
		
		return emails.length;
	}

	@Override
	public String inviteUser(final String circleId, final String userId, final String email,
			final String firstName, final String lastName, final int roleId) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String userIdResult = null;
				boolean isNewUser = false;
				final String tempPassword = Password.generate();
				PreparedStatement statement = null;
				ResultSet rs = null;
				boolean auto = connection.getAutoCommit();
				try {
					connection.setAutoCommit(false);

					if (userId != null) {
						userIdResult = userId;
					} else {
						// lookup by email
						final String query = 
								"SELECT ue.user_uuid FROM user_emails AS ue" 
										+ " WHERE ue.email = ?";
						statement = connection.prepareStatement(query);
						statement.setString(1, email);

						rs = statement.executeQuery();

						if (rs != null && rs.next()) {
							userIdResult = rs.getString("ue.user_uuid");
						} else {
							// create if not existing
							String newUserId = String.valueOf(UUID.randomUUID());
							final String dummyPassword = Password.generate();
							final String query2 = 
									"INSERT INTO user (uuid, primary_email, password, fname, lname, temp_password, temp_password_expire_ts)"
											+ " VALUES (?, ?, ?, ?, ?, ?" 
											+ ", date_add(NOW(), INTERVAL " 
											+ ExpirationConstants.INVITATION_CONFIRMATION_EXPIRATION_IN_DAYS + " DAY))";
							statement = connection.prepareStatement(query2);
							statement.setString(1, newUserId);
							statement.setString(2, email);
							statement.setString(3, Password.getSaltedHash(dummyPassword));
							statement.setString(4, firstName);
							statement.setString(5, lastName);
							statement.setString(6, Password.getSaltedHash(tempPassword));

							if (statement.executeUpdate() == 1) {
								final String query3 = 
										"INSERT INTO user_emails (email, user_uuid)"
												+ " VALUES (?, ?)";
								statement.close();
								statement = connection.prepareStatement(query3);
								statement.setString(1, email);
								statement.setString(2, newUserId);

								if (statement.executeUpdate() == 1) {
									isNewUser = true;
									userIdResult = newUserId;
								}
							}
						}
					}

					// either update status or add to circle as appropriate
					if (userIdResult != null) {
						final String query = 
								"SELECT cu.status_id FROM circle_users AS cu" 
										+ " WHERE"
										+ "  cu.circle_uuid = ?"
										+ " AND"
										+ "  cu.user_uuid = ?";
						statement = connection.prepareStatement(query);
						statement.setString(1, circleId);
						statement.setString(2, userIdResult);

						rs = statement.executeQuery();

						if (rs != null && rs.next()) { // the user was in the circle
							final String query2 = 
									"UPDATE circle_users AS cu SET"
											+ "  cu.status_id = ?"
											+ " WHERE"
											+ "  cu.circle_uuid = ?"
											+ " AND"
											+ "  cu.user_uuid = ?";
							statement = connection.prepareStatement(query2);
							statement.setInt(1, CircleUserStatus.INVITED);
							statement.setString(2, circleId);
							statement.setString(3, userIdResult);
						} else {
							final String query2 = 
									"INSERT INTO circle_users (circle_uuid, user_uuid, role_id, status_id)"
											+ " VALUES (?, ?, ?, ?)";
							statement = connection.prepareStatement(query2);
							statement.setString(1, circleId);
							statement.setString(2, userIdResult);
							statement.setInt(3, roleId);
							statement.setInt(4, CircleUserStatus.INVITED);
						}

						if (statement.executeUpdate() != 1) {
							userIdResult = null;
						}
					}

					// create appropriate conf record and email
					if (userIdResult != null) {
						String confId = String.valueOf(UUID.randomUUID());
						SupportEmail supportEmail = new SupportEmail();

						// first get meta data
						String circleName = null;
						String ownerName = null;
						String sponsorName = null;
						String sponsorUrl = null;
						String sponsorId = null;

						final String queryMeta = 
								"SELECT c.name"
										+ ", u.fname, u.lname, u.primary_email"
										+ ", c.sponsor_uuid, s.name AS sponsor_name, s.url AS sponsor_url"
										+ " FROM circle AS c"
										+ " LEFT JOIN user AS u ON u.uuid = c.owner_uuid"
										+ " LEFT JOIN sponsor AS s ON s.uuid = c.sponsor_uuid"
										+ " WHERE"
										+ "  c.uuid = ?";
						statement = connection.prepareStatement(queryMeta);
						statement.setString(1, circleId);

						rs = statement.executeQuery();

						if (rs != null && rs.next()) { 
							circleName = rs.getString("c.name");
							ownerName = rs.getString("u.fname") 
									+ " " + rs.getString("u.lname") 
									+ " (" + rs.getString("u.primary_email") + ")";
							sponsorName = rs.getString("sponsor_name");
							sponsorUrl = rs.getString("sponsor_url");							
							sponsorId = rs.getString("c.sponsor_uuid");							
						}
						
						if (isNewUser) {
							final String query = 
									"INSERT INTO confirmation (uuid, user_uuid, circle_uuid, email, expire_ts)"
											+ " VALUES (?, ?, ?, ? "
											+ ", date_add(NOW(), INTERVAL " 
											+ ExpirationConstants.INVITATION_CONFIRMATION_EXPIRATION_IN_DAYS + " DAY)"
											+ ")";
							statement.close();
							statement = connection.prepareStatement(query);
							statement.setString(1, confId);
							statement.setString(2, userIdResult);
							statement.setString(3, circleId);
							statement.setString(4, email);
							 
							if (statement.executeUpdate() != 1 || !supportEmail.sendInvitationConfirmationEmail(email, confId, tempPassword, circleName, ownerName, sponsorName, sponsorUrl, sponsorId)) {
								userIdResult = null;
							} else {
								URL url = new URL("https://voxgift.com/imageupload?userId=" + userIdResult + "&userEmail=" + email);
								url.openStream();
							}
						} else {
							final String query = 
									"INSERT INTO confirmation (uuid, user_uuid, circle_uuid, expire_ts)"
											+ " VALUES (?, ?, ? "
											+ ", date_add(NOW(), INTERVAL " 
											+ ExpirationConstants.INVITATION_CONFIRMATION_EXPIRATION_IN_DAYS + " DAY)"
											+ ")";
							statement.close();
							statement = connection.prepareStatement(query);
							statement.setString(1, confId);
							statement.setString(2, userIdResult);
							statement.setString(3, circleId);

							if (statement.executeUpdate() != 1 || !supportEmail.sendInvitationConfirmationEmail(email, confId, circleName, ownerName, sponsorName, sponsorUrl, sponsorId)) {
								userIdResult = null;
							}
						}
					}

					if (userIdResult == null) {
						connection.rollback();
					} else {
						connection.commit();
					}
				} catch (Exception ex) {
					connection.rollback();
					LOGGER.warning("Exception: " + ex);
				} finally {
					connection.setAutoCommit(auto);

					if (rs != null) {
						rs.close();
					}

					if (statement != null) {
						statement.close();
					}
				}
				return userIdResult;
			}});		
	}


}
