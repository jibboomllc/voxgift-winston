package com.voxgift.winstonplus.data.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.voxgift.winstonplus.data.client.rpc.DocumentService;
import com.voxgift.winstonplus.data.shared.model.DocumentDetails;

public class DocumentServiceImpl extends JdbcResource implements DocumentService {

	private final static Logger LOGGER = Logger.getLogger(DocumentServiceImpl.class.getName()); 

	@Override
	public List<DocumentDetails> getDocuments(final String language, final String locale, final int typeId) {
		return withDB(new JDBCRunner<List<DocumentDetails>>(){
			@Override
			public List<DocumentDetails> run(Connection connection) throws Exception {
				List<DocumentDetails>  result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					final String query = 
							"SELECT uuid, title FROM documents AS ld" 
									+ " WHERE UNIX_TIMESTAMP(`delete_ts`) = 0"
									+ " AND language = ?"
									+ " AND locale = ?"
									+ " AND type_id = ?"
									+ " ORDER BY sort_order ASC";
					statement = connection.prepareStatement(query);
					statement.setString(1, language == null ? "en" : language); // may be called before we have user settings
					statement.setString(2, locale == null ? "US" : locale); // may be called before we have user settings
					statement.setInt(3, typeId);

					rs = statement.executeQuery();

					if (rs != null) {
						result = new ArrayList<DocumentDetails> ();
						while (rs.next()) {
							result.add(new DocumentDetails(rs.getString("uuid"), rs.getString("title")));
						}
					}
					
					// if we didn't get anything and not en_US, try again
					if ((rs == null || result.size() < 1) && !language.equals("en") && !locale.equals("US")) {
						LOGGER.warning("getDocuments(" + language + ", " + locale + ", " + typeId + "): no results, trying en_US");
						statement.setString(1, "en");
						statement.setString(2, "US");

						rs = statement.executeQuery();

						if (rs != null) {
							result = new ArrayList<DocumentDetails> ();
							while (rs.next()) {
								result.add(new DocumentDetails(rs.getString("uuid"), rs.getString("title")));
							}
						}
					}
					
					if (rs == null || result.size() < 1) {
						LOGGER.warning("getDocuments(" + language + ", " + locale + ", " + typeId + "): no results");
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public String getDocument(final String documentId, final String language, final String locale) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					final String query = 
							"SELECT content FROM documents AS ld" 
									+ " WHERE uuid = ?"
									+ " AND language = ?"
									+ " AND locale = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, documentId);
					statement.setString(2, language == null ? "en" : language); // may be called before we have user settings
					statement.setString(3, locale == null ? "US" : locale); // may be called before we have user settings

					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						result = rs.getString("content");
					}
					
					// if we didn't get anything and not en_US, try again
					if ((rs == null || result == null) && !language.equals("en") && !locale.equals("US")) {
						LOGGER.warning("getDocument(" + documentId + ", " + language + ", " + locale + "): no results, trying en_US");
						statement.setString(2, "en");
						statement.setString(3, "US");

						rs = statement.executeQuery();

						if (rs != null && rs.next()) {
							result = rs.getString("content");
						}
					}
					
					if (rs == null || result == null) {
						LOGGER.warning("getDocument(" + documentId + ", " + language + ", " + locale + "): no results");
					}				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		

	}
}
