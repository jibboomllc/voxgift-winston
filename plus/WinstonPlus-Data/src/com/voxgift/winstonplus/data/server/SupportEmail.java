package com.voxgift.winstonplus.data.server;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.voxgift.winstonplus.data.shared.ExpirationConstants;

public class SupportEmail {

	private final static String header = "<div><h1>Voxgift</h1><p>Giving patients the gift of voice</p><hr/></div>";

	private final static String footer = "<div><hr/>Copyright &copy; 2011-2013 All Rights Reserved - <a href='http://www.jibboom.com/'>Jibboom, LLC</a></div>";

	public Boolean sendSupportEmail (String to, String subject, String content) {
		final String username = "support@voxgift.com";
		final String password = "w1nsupp0rt";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		content = header + "<ul>" + content;
		content += "<li>If you have not already done so, please download WinstonPlus from the"
				+ " <a href='https://itunes.apple.com/us/app/voxgift-winstonplus/id646885435?mt=8&amp;uo=4' target='itunes_store'>iTunes App Store</a>"
				+ " or <a href='https://play.google.com/store/apps/details?id=com.voxgift.winstonplus.android'>Google Play</a>.</li></ul>";

		content += "<p>If you have any questions or concerns, please reply to this email or visit us at <a href='https://voxgift.com'>https://voxgift.com</a>.</p>";

		content += "<br/><br/><a href='https://itunes.apple.com/us/app/voxgift-winstonplus/id646885435?mt=8&amp;uo=4' target='itunes_store'> <img src='http://r.mzstatic.com/images/web/linkmaker/badge_appstore-lrg.gif' alt='Get WinstonPlus on the iTunes App Store' style='border: 0;' /></a>";
		content += "<br/><br/><a href='https://play.google.com/store/apps/details?id=com.voxgift.winstonplus.android'><img src='https://developer.android.com/images/brand/en_generic_rgb_wo_45.png' alt='Get WinstonPlus on Google Play'/></a>";

		content += footer;

		try{
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username, "Voxgift Support"));
			message.addRecipient(Message.RecipientType.TO,
					new InternetAddress(to));
			message.setSubject(subject);
			message.setContent(content, "text/html" );
			Transport.send(message);
			System.out.println("Sent message successfully....");
			return true;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (AddressException aex) {
			aex.printStackTrace();
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
		return false;
	}

	public boolean sendTestEmail(String to) {
		Calendar cal = new GregorianCalendar();
		Date creationDate = cal.getTime();
		SimpleDateFormat date_format = new SimpleDateFormat("MMM dd,yyyy HH:mm");
		System.out.println(date_format.format(creationDate));

		String subject = "Voxgift test email";
		String content = "<li>This test email was sent: " + date_format.format(creationDate) + "</li>";
		return sendSupportEmail(to, subject, content);
	}

	public boolean sendTempPasswordEmail(String to, String tempPassword) {
		String subject = "Voxgift password reset";
		String content = "<li>Your temporary password is: " + tempPassword + "</li>"
				+ "<li>Note that this password will expire in " 
				+ ExpirationConstants.TEMP_PASSWORD_EXPIRATION_IN_HOURS + " hours.</li>"
				+ "<li>Also note that this password can only be used once so please set a new password after logging in.</li>";
		return sendSupportEmail(to, subject, content);
	}

	public boolean sendRegistrationConfirmationEmail(String to, String confId) {
		final String confirmationUrl = "https://www.voxgift.com/confirmation?id=";
		String subject = "Welcome to Voxgift";
		String content = "<li>To confirm your registration, please click on this <a href=\"" + confirmationUrl + confId + "\">link</a>.</li>"
				+ "<li>Note that this registration link will expire in " 
				+ ExpirationConstants.REGISTRATION_CONFIRMATION_EXPIRATION_IN_HOURS + " hours.</li>";
		return sendSupportEmail(to, subject, content);
	}

	public boolean sendInvitationConfirmationEmail(String to, String confId, String circleName, String ownerName, String sponsorName, String sponsorUrl, String sponsorId) {
		final String confirmationUrl = "https://www.voxgift.com/confirmation?id=";
		String subject = "Voxgift Invitation";
		String content = "<li>You've been invited by " + ownerName + " to " + circleName + ", a <a href='" + sponsorUrl + "'>" + sponsorName + "</a> circle-of-care.</li>"
				+ "<li>To confirm your invitation, please click on this <a href=\"" + confirmationUrl + confId + "\">link</a>.</li>"
				+ "<li>Note that this invitation link will expire in " 
				+ ExpirationConstants.INVITATION_CONFIRMATION_EXPIRATION_IN_DAYS + " days.</li>";
		return sendSupportEmail(to, subject, content);
	}

	public boolean sendInvitationConfirmationEmail(String to, String confId, String tempPassword, String circleName, String ownerName, String sponsorName, String sponsorUrl, String sponsorId) {
		final String confirmationUrl = "https://www.voxgift.com/confirmation?id=";
		String subject = "Voxgift Invitation";
		String content = "<li>You've been invited by " + ownerName + " to " + circleName + ", a <a href='" + sponsorUrl + "'>" + sponsorName + "</a> circle-of-care.</li>"
				+ "<li>Your temporary password is: " + tempPassword + "</li>"
				+ "<li>To confirm your invitation, please click on this <a href=\"" + confirmationUrl + confId + "\">link</a>.</li>"
				+ "<li>Note that this invitation link will expire in " 
				+ ExpirationConstants.INVITATION_CONFIRMATION_EXPIRATION_IN_DAYS + " days.</li>";
		return sendSupportEmail(to, subject, content);
	}

}
