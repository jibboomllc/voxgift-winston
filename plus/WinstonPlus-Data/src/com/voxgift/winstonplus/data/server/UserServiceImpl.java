package com.voxgift.winstonplus.data.server;

import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

import com.voxgift.winstonplus.data.client.rpc.UserService;
import com.voxgift.winstonplus.data.shared.ExpirationConstants;
import com.voxgift.winstonplus.data.shared.model.Circle;
import com.voxgift.winstonplus.data.shared.model.CircleStatus;
import com.voxgift.winstonplus.data.shared.model.CircleUserRole;
import com.voxgift.winstonplus.data.shared.model.CircleUserStatus;
import com.voxgift.winstonplus.data.shared.model.UserCircleDetails;
import com.voxgift.winstonplus.data.shared.model.UserDetails;
import com.voxgift.winstonplus.data.shared.model.UserEmailDetails;

public class UserServiceImpl extends JdbcResource implements UserService {

	private final static Logger LOGGER = Logger.getLogger(UserServiceImpl.class.getName()); 

	/*
	 * (non-Javadoc)
	 * @see com.voxgift.winstonplus.client.rpc.UserService#ping()
	 * executes a quick query to test end-to-end connection
	 */
	@Override
	public Boolean ping () {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				Boolean result = false;
				PreparedStatement statement = null;
				ResultSet rs = null;

				try {
					final String query = 
							"SELECT id FROM event_type";
					statement = connection.prepareStatement(query);

					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						result = true;
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Boolean sendTestEmail(final String email) {
		return (new SupportEmail()).sendTestEmail(email);
	}

	@Override
	public String login(final String email, final String password) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;

				try {
					final String query = 
							"SELECT u.uuid, u.password FROM user AS u"
									+ " WHERE u.uuid = (SELECT ue.user_uuid FROM user_emails AS ue"  
									+ " WHERE ue.email = ?"
									+ "   AND ue.confirmed_ts <> 0)";
					statement = connection.prepareStatement(query);
					statement.setString(1, email);

					rs = statement.executeQuery();

					if (rs == null || !rs.next()) {
						LOGGER.info("login: Could not find user with validated email = " +  email);
					} else if (!Password.check(password, rs.getString("u.password"))) {
						LOGGER.info("login: Invalid password supplied for email = " +  email);
					} else {
						// create session
						String sid = String.valueOf(UUID.randomUUID());
						final String query4 = 
								"INSERT INTO session (uuid, user_uuid, expire_ts)"
										+ " VALUES (?, ?" 
										+ ", date_add(NOW(), INTERVAL " + ExpirationConstants.SESSION_EXPIRATION_IN_DAYS + " DAY)"
										+ ")";
						statement = connection.prepareStatement(query4);
						statement.setString(1, sid);
						statement.setString(2, rs.getString("u.uuid"));

						if (statement.executeUpdate() == 1) {
							result = sid;
						} else {
							LOGGER.warning("login: Could not create session for user_uuid = " +  rs.getString("u.uuid"));
						}
					}

				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public String loginWithTempPassword(final String email, final String password) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;

				try {
					final String query = 
							"SELECT u.uuid, u.temp_password FROM user AS u"
									+ " WHERE u.uuid = (SELECT ue.user_uuid FROM user_emails AS ue"  
									+ " WHERE ue.email = ?)"
									+ " AND u.temp_password_expire_ts > NOW()";
					statement = connection.prepareStatement(query);
					statement.setString(1, email);

					rs = statement.executeQuery();


					if (rs == null || !rs.next()) {
						LOGGER.info("loginWithTempPassword: Could not find user with unexpired temp password and email = " +  email);
					} else if (!Password.check(password, rs.getString("u.temp_password"))) {
						LOGGER.info("loginWithTempPassword: Invalid temp password supplied for email = " +  email);
					} else {
						String userId = rs.getString("u.uuid");

						// clear temp password
						final String query2 = 
								"UPDATE user AS u "
										+ "SET u.temp_password = NULL"
										+ ", u.temp_password_expire_ts = NOW()"
										+ " WHERE u.uuid = ?";
						statement.close();
						statement = connection.prepareStatement(query2);
						statement.setString(1, userId);

						statement.executeUpdate();

						// set email to verified/confirmed
						final String query3 = 
								"UPDATE user_emails AS ue "
										+ "SET ue.confirmed_ts = NOW()"
										+ " WHERE ue.email = ?";
						statement.close();
						statement = connection.prepareStatement(query3);
						statement.setString(1, email);

						statement.executeUpdate();

						// create session
						String sid = String.valueOf(UUID.randomUUID());
						final String query4 = 
								"INSERT INTO session (uuid, user_uuid, expire_ts)"
										+ " VALUES (?, ?" 
										+ ", date_add(NOW(), INTERVAL " + ExpirationConstants.SESSION_EXPIRATION_IN_DAYS + " DAY)"
										+ ")";
						statement.close();
						statement = connection.prepareStatement(query4);
						statement.setString(1, sid);
						statement.setString(2, userId);

						if (statement.executeUpdate() == 1) {
							result = sid;
						} else {
							LOGGER.warning("loginWithTempPassword: Could not create session for user_uuid = " +  rs.getString("u.uuid"));
						}
					} 
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public String loginFromSession(final String sid) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;

				try {
					final String query = 
							"SELECT user_uuid FROM session AS s WHERE s.uuid = ?"
									+ " AND s.expire_ts > NOW()";
					statement = connection.prepareStatement(query);
					statement.setString(1, sid);

					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						result = rs.getString("user_uuid");
					} else {
						LOGGER.info("loginFromSession: Could not find unexpired session = " + sid);
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}


	@Override
	public Boolean associateUserWithPushRegistration(final String userId, final String regId, final int pushServiceTypeId) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				Boolean result = false;
				PreparedStatement statement = null;
				ResultSet rs = null;

				try {
					final String query = 
							"SELECT user_uuid, UNIX_TIMESTAMP(`delete_ts`) AS dts FROM push_device"
									+ " WHERE uuid = ?"
									+ " AND push_service_type_id = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, regId);
					statement.setInt(2, pushServiceTypeId);

					LOGGER.info("associateUserWithPushRegistration: looking up current user: " + query + " " + regId);
					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						if (userId.equals(rs.getString("user_uuid")) && rs.getInt("dts") == 0) {
							result = true;
						} else {
							final String query2 = 
									"UPDATE push_device SET delete_ts = 0, user_uuid = ?"
											+ " WHERE uuid = ?"
											+ " AND push_service_type_id = ?";
							statement.close();
							statement = connection.prepareStatement(query2);
							statement.setString(1, userId);
							statement.setString(2, regId);
							statement.setInt(3, pushServiceTypeId);

							LOGGER.info("associateUserWithPushRegistration: updating device with current user: " + query2 + " " + userId + " " + regId);
							if (statement.executeUpdate() == 1) {
								result = true;
							} else {
								LOGGER.warning("associateUserWithPushRegistration: Could not update push_device: userId = "
										+ userId + ", regId=" + regId + ", pushServiceTypeId=" + pushServiceTypeId);
							}
						}
					} else {
						final String query2 = 
								"INSERT INTO push_device (uuid, push_service_type_id, user_uuid)"
										+ " VALUES (?, ?, ?)";
						statement.close();
						statement = connection.prepareStatement(query2);
						statement.setString(1, regId);
						statement.setInt(2, pushServiceTypeId);
						statement.setString(3, userId);

						LOGGER.info("associateUserWithPushRegistration: inserting device with current user: " + query2 + " " + userId + " " + regId);
						if (statement.executeUpdate() == 1) {
							result = true;
						} else {
							LOGGER.warning("associateUserWithPushRegistration: Could not insert push_device: userId = "
									+ userId + ", regId=" + regId + ", pushServiceTypeId=" + pushServiceTypeId);
						}			
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Boolean disassociateUserWithPushRegistration(final String userId, final String regId, final int pushServiceTypeId) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				Boolean result = true;
				PreparedStatement statement = null;

				try {
					final String query = 
							"UPDATE push_device SET delete_ts = NOW()"
									+ " WHERE uuid = ?"
									+ " AND push_service_type_id = ?"
									+ " AND user_uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, regId);
					statement.setInt(2, pushServiceTypeId);
					statement.setString(3, userId);

					LOGGER.info(query);
					statement.executeUpdate();
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Boolean disassociateUserWithAllPushRegistrations(final String userId) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				Boolean result = true;
				PreparedStatement statement = null;

				try {
					final String query = 
							"UPDATE push_device SET delete_ts = NOW()"
									+ " WHERE user_uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, userId);

					LOGGER.info(query);
					statement.executeUpdate();
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Boolean expireOtherUserSessions(final String userId, final String sid) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				Boolean result = false;
				PreparedStatement statement = null;

				try {
					final String query = 
							"UPDATE session AS s SET s.expire_ts = NOW()"
									+ " WHERE s.user_uuid = ?"
									+ " AND s.uuid <> ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, userId);
					statement.setString(2, sid);

					if (statement.executeUpdate() == 1) {
						result = true;
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Boolean logout(final String sid) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				Boolean result = false;
				PreparedStatement statement = null;

				try {
					final String query = 
							"UPDATE session AS s SET s.expire_ts = NOW() WHERE s.uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, sid);

					if (statement.executeUpdate() == 1) {
						result = true;
					} else {
						LOGGER.info("logout: Could not find session = " + sid);
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	/**
	 * This method wraps a call to generateTempPassword and sends the p/w
	 * via email.
	 */
	@Override
	public Boolean createTempPassword(final String email) {
		String tempPassword = generateTempPassword(email);

		if (tempPassword != null) {
			return (new SupportEmail()).sendTempPasswordEmail(email, tempPassword);
		} else {
			return false;
		}
	}

	/**
	 * This method is broken out of createTempPassword so that the logic can 
	 * be tested without sending an email. It would defeat the purpose to 
	 * return the p/w directly to the user.
	 */
	@Override
	public String generateTempPassword(final String email) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = null;
				PreparedStatement statement = null;

				try {
					final String tempPassword = Password.generate();
					final String query = 
							"UPDATE user AS u "
									+ "SET u.temp_password = ?"
									+ ", u.temp_password_expire_ts = date_add(NOW(), INTERVAL " 
									+ ExpirationConstants.TEMP_PASSWORD_EXPIRATION_IN_HOURS + " HOUR)"
									+ " WHERE u.uuid = (SELECT ue.user_uuid FROM user_emails AS ue WHERE ue.email = ?)";
					statement = connection.prepareStatement(query);
					statement.setString(1, Password.getSaltedHash(tempPassword));
					statement.setString(2, email);

					if (statement.executeUpdate() == 1) {
						result = tempPassword;
					} else {
						LOGGER.warning("generateTempPassword: Could not update temp_password for email = " + email);
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});			}

	@Override
	public String lookupIdByEmail(final String email) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					final String query = 
							"SELECT ue.user_uuid FROM user_emails AS ue"  
									+ " WHERE ue.email = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, email);

					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						result = rs.getString("ue.user_uuid");
					} else {
						LOGGER.info("lookupIdByEmail: Could not find user with email = " +  email);
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public Boolean register(final String email, final String password) {
		return register(email, password, null, null);
	}

	@Override
	public Boolean register(final String email, final String password, final String firstName, final String lastName) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				Boolean result = null;
				PreparedStatement statement = null;
				boolean auto = connection.getAutoCommit();
				try {
					connection.setAutoCommit(false);

					String userId = String.valueOf(UUID.randomUUID());
					final String query = 
							"INSERT INTO user (uuid, primary_email, password, fname, lname)"
									+ " VALUES (?, ?, ?, ?, ?)";
					statement = connection.prepareStatement(query);
					statement.setString(1, userId);
					statement.setString(2, email);
					statement.setString(3, Password.getSaltedHash(password));
					statement.setString(4, firstName);
					statement.setString(5, lastName);

					if (statement.executeUpdate() == 1) {
						final String query2 = 
								"INSERT INTO user_emails (email, user_uuid)"
										+ " VALUES (?, ?)";
						statement.close();
						statement = connection.prepareStatement(query2);
						statement.setString(1, email);
						statement.setString(2, userId);

						if (statement.executeUpdate() == 1) {
							String confId = String.valueOf(UUID.randomUUID());
							final String query3 = 
									"INSERT INTO confirmation (uuid, user_uuid, email, expire_ts)"
											+ " VALUES (?, ?, ? "
											+ ", date_add(NOW(), INTERVAL " 
											+ ExpirationConstants.REGISTRATION_CONFIRMATION_EXPIRATION_IN_HOURS + " HOUR)"
											+ ")";
							statement.close();
							statement = connection.prepareStatement(query3);
							statement.setString(1, confId);
							statement.setString(2, userId);
							statement.setString(3, email);

							if (statement.executeUpdate() == 1 && (new SupportEmail()).sendRegistrationConfirmationEmail(email, confId)) {
								connection.commit();

								URL url = new URL("https://voxgift.com/imageupload?userId=" + userId + "&userEmail=" + email);
								url.openStream();
								result = true;
							} else {
								connection.rollback();
							}
						} else {
							connection.rollback();
						}
					} else {
						connection.rollback();
					}
				} catch (Exception ex) {
					connection.rollback();
					LOGGER.warning("Exception: " + ex);
				} finally {
					connection.setAutoCommit(auto);

					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public UserDetails getUser(final String sid) {
		return withDB(new JDBCRunner<UserDetails>(){
			@Override
			public UserDetails run(Connection connection) throws Exception {

				UserDetails result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;

				try {
					final String query = 
							"SELECT * FROM user AS u"
									+ " LEFT JOIN session AS s"
									+ " ON u.uuid = s.user_uuid"
									+ " WHERE s.uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, sid);

					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						UserDetails details = new UserDetails(rs.getString("u.uuid")
								, rs.getString("u.primary_email")
								, rs.getString("u.password")
								, rs.getString("u.fname")
								, rs.getString("u.lname")
								, rs.getString("u.language")
								, rs.getString("u.locale")
								, rs.getString("u.gender")
								);

						final String query2 = 
								"SELECT ue.email, ue.confirmed_ts FROM user_emails AS ue WHERE ue.user_uuid = ?";
						statement.close();
						statement = connection.prepareStatement(query2);
						statement.setString(1, details.getId());

						rs.close();
						rs = statement.executeQuery();

						if (rs != null) {
							List<UserEmailDetails> listUserEmailDetails = new ArrayList<UserEmailDetails>();
							while (rs.next()) {
								UserEmailDetails userEmailDetails = new UserEmailDetails(rs.getString("ue.email")
										, rs.getDate("ue.confirmed_ts"));
								listUserEmailDetails.add(userEmailDetails);
							}
							details.setUserEmails(listUserEmailDetails);

							result = details;
						}
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Map<String, UserCircleDetails> getUsersCircleSubscriptions(final String userId) {
		return withDB(new JDBCRunner<Map<String, UserCircleDetails>>(){
			@Override
			public Map<String, UserCircleDetails> run(Connection connection) throws Exception {
				Map<String, UserCircleDetails> result = null;
				PreparedStatement statement = null;
				PreparedStatement statement2 = null;
				ResultSet rs = null;
				ResultSet rs2 = null;
				try {
					final String query = "SELECT "
							+ "   c.uuid"
							+ "   , c.name"
							+ "   , c.sponsor_uuid"
							+ "   , c.status_id"
							+ "   , cu.role_id"
							+ "   , cu.status_id"
							+ " FROM"
							+ "   circle AS c"
							+ "   LEFT JOIN circle_users AS cu"
							+ "   ON cu.circle_uuid =  c.uuid"
							+ " WHERE"
							+ "   cu.user_uuid = ?"
							+ " AND"
							+ "   cu.status_id = ?"
							+ " AND"
							+ "   c.status_id <> ?"
							+ " ORDER BY"
							+ "   c.status_id";
					statement = connection.prepareStatement(query);
					statement.setString(1, userId);
					statement.setInt(2, CircleUserStatus.ACCEPTED);
					statement.setInt(3, CircleStatus.DELETED);

					rs = statement.executeQuery();

					if (rs != null) {
						result = new HashMap<String, UserCircleDetails>();

						String centerId = null;
						final String query2 = "SELECT "
								+ "   cu.user_uuid"
								+ " FROM"
								+ "   circle_users AS cu"
								+ " WHERE"
								+ "   cu.circle_uuid = ?"
								+ " AND"
								+ "   cu.role_id = ?";
						statement2 = connection.prepareStatement(query2);

						while (rs.next()) {

							statement2.setString(1, rs.getString("c.uuid"));
							statement2.setInt(2, CircleUserRole.CENTER);

							rs2 = statement2.executeQuery();

							if (rs2 != null && rs2.next()) {
								centerId = rs2.getString("cu.user_uuid");
							} else {
								centerId = null;
							}

							UserCircleDetails userCircleDetails = new UserCircleDetails(rs.getString("c.uuid")
									, rs.getString("c.name")
									, rs.getString("c.sponsor_uuid")
									, centerId
									, rs.getInt("c.status_id")
									, rs.getInt("cu.role_id")
									, rs.getInt("cu.status_id"));
							result.put(rs.getString("c.uuid"), userCircleDetails);
						}
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (rs2 != null) {
						rs2.close();
					}
					if (statement != null) {
						statement.close();
					}
					if (statement2 != null) {
						statement2.close();
					}
				}
				return result;
			}});
	}


	@Override
	public List<Circle> getUsersCircles(final String userId) {
		return withDB(new JDBCRunner<List<Circle>>(){
			@Override
			public List<Circle> run(Connection connection) throws Exception {
				List<Circle> result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					final String query = "SELECT "
							+ " DISTINCT c.uuid"
							+ "   , c.name"
							+ "   , c.owner_uuid"
							+ "   , c.sponsor_uuid"
							+ "   , c.status_id"
							+ " FROM"
							+ "   circle AS c"
							+ "   LEFT JOIN circle_users AS cu"
							+ "   ON cu.circle_uuid =  c.uuid"
							+ " WHERE"
							+ "   (c.owner_uuid = ?"
							+ " OR"
							+ "   (cu.user_uuid = ? AND (cu.status_id = ? OR cu.status_id = ?)))"
							+ " AND"
							+ "   c.status_id <> ?"
							+ " ORDER BY"
							+ "   c.status_id";
					statement = connection.prepareStatement(query);
					statement.setString(1, userId);
					statement.setString(2, userId);
					statement.setInt(3, CircleUserStatus.INVITED);
					statement.setInt(4, CircleUserStatus.ACCEPTED);
					statement.setInt(5, CircleStatus.DELETED);

					rs = statement.executeQuery();

					if (rs != null) {
						result = new ArrayList<Circle>();
						while (rs.next()) {
							Circle circle = new Circle(rs.getString("c.uuid")
									, rs.getString("c.name")
									, rs.getString("c.owner_uuid")
									, rs.getString("c.sponsor_uuid")
									, rs.getInt("c.status_id"));
							result.add(circle);
						}
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public Boolean updateUser(final String id, final String primaryEmail, final String password
			, final String firstName, final String lastName
			, final String language, final String locale, final String gender) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				Boolean result = false;
				PreparedStatement statement = null;

				try {
					if (password == null || password.isEmpty()) {
						final String query = 
								"UPDATE user SET"
										+ " primary_email = ?"
										+ ", fname = ?"
										+ ", lname = ?"
										+ ", language = ?"
										+ ", locale = ?"
										+ ", gender = ?"
										+ " WHERE uuid = ?";
						statement = connection.prepareStatement(query);
						statement.setString(1, primaryEmail);
						statement.setString(2, firstName);
						statement.setString(3, lastName);
						statement.setString(4, language);
						statement.setString(5, locale);
						statement.setString(6, gender);
						statement.setString(7, id);

						if (statement.executeUpdate() == 1) {
							result = true;
						}
					} else {
						final String query = 
								"UPDATE user SET"
										+ " primary_email = ?"
										+ ", password = ?"
										+ ", fname = ?"
										+ ", lname = ?"
										+ ", language = ?"
										+ ", locale = ?"
										+ ", gender = ?"
										+ " WHERE uuid = ?";
						statement = connection.prepareStatement(query);
						statement.setString(1, primaryEmail);
						statement.setString(2, Password.getSaltedHash(password));
						statement.setString(3, firstName);
						statement.setString(4, lastName);
						statement.setString(5, language);
						statement.setString(6, locale);
						statement.setString(7, gender);
						statement.setString(8, id);

						if (statement.executeUpdate() == 1) {
							result = true;
						}
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

}
