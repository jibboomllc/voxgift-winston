package com.voxgift.winstonplus.data.server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.logging.Logger;

import com.voxgift.winstonplus.data.client.rpc.DictionaryService;
import com.voxgift.winstonplus.data.shared.model.DictionaryDetails;
import com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails;

public class DictionaryServiceImpl extends JdbcResource implements DictionaryService {

	private final static Logger LOGGER = Logger.getLogger(DictionaryServiceImpl.class.getName()); 

	private List<DictionaryDetails> getDictionaries(Connection connection,
			final String languageId, final String localeId) throws Exception {
		List<DictionaryDetails> dictionaries = new ArrayList<DictionaryDetails>();				
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {

			String query = "SELECT" 
					+ "  d.context_uuid" 
					+ "  , d.entry_uuid" 
					+ "  , det.translation"
					+ "  , d.sort_order"
					+ " FROM" 
					+ "  dictionary AS d" 
					+ "  LEFT JOIN dictionary_entry_translation AS det" 
					+ "  ON d.entry_uuid = det.entry_uuid" 
					+ " WHERE" 
					+ "  det.language = ?"; 
			if (localeId != null) {
				query += "  AND det.locale = ?";
			}
			query += " ORDER BY" 
					+ "  d.context_uuid" 
					+ "  , d.sort_order" 
					+ ";";
			statement = connection.prepareStatement(query);
			statement.setString(1, languageId);
			if (localeId != null) {
				statement.setString(2, localeId);
			}

			rs = statement.executeQuery();
			String contextId = null;
			List<DictionaryEntryDetails> entries = null;
			while (rs != null && rs.next()) {
				if (!rs.getString("d.context_uuid").equals(contextId)) {
					if (contextId != null) {
						dictionaries.add(new DictionaryDetails (contextId, languageId, localeId, entries));
					}
					contextId = rs.getString("d.context_uuid");
					entries = new ArrayList<DictionaryEntryDetails> ();
				}
				DictionaryEntryDetails entry = new DictionaryEntryDetails(rs.getString("d.entry_uuid")
						, rs.getString("det.translation"), rs.getInt("d.sort_order"));
				entries.add(entry);
			}
			if (contextId != null) {
				dictionaries.add(new DictionaryDetails (contextId, languageId, localeId, entries));
			}
		} catch (Exception ex) {
			LOGGER.warning("Exception: " + ex);
			ex.printStackTrace();
		} finally {
			if (rs != null) {
				rs.close();
			}
			if (statement != null) {
				statement.close();
			}
		}
		return dictionaries;
	}

	@Override
	public List<DictionaryDetails> getDictionaries(final String languageId,
			final String localeId) {
		return withDB(new JDBCRunner<List<DictionaryDetails>>(){
			@Override
			public List<DictionaryDetails> run(Connection connection) throws Exception {
				List<DictionaryDetails> dictionaries = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					if (localeId != null && !localeId.isEmpty()) {
						dictionaries = getDictionaries(connection, languageId, localeId);
						if (dictionaries == null || dictionaries.size() == 0) {
							dictionaries = getDictionaries(connection, languageId, null);
						}
					} else {
						dictionaries = getDictionaries(connection, languageId, null);
					}
					if (dictionaries == null || dictionaries.size() == 0) {
						dictionaries = getDictionaries(connection, "en", null);
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return dictionaries;
			}});		

	}

	@Override
	public DictionaryDetails getFavoritesDictionary(final String userId, final String languageId, final String localeId) {
		final String FAVORITES_CONTEXT = "c148d0d2-2936-4a30-a4de-e6f66ea92782";
		return withDB(new JDBCRunner<DictionaryDetails>(){
			@Override
			public DictionaryDetails run(Connection connection) throws Exception {
				DictionaryDetails dictionary = null;				
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {

					String query = "SELECT" 
							+ "  fd.uuid" 
							+ "  , fd.translation" 
							+ "  , fd.sort_order"
							+ " FROM" 
							+ "  favorites_dictionary_entry AS fd" 
							+ " WHERE" 
							+ "  fd.user_uuid = ?"
							+ "  AND UNIX_TIMESTAMP(`fd`.`delete_ts`) = 0"
							+ "  AND fd.language = ?"; 
					if (localeId != null) {
						query += "  AND fd.locale = ?";
					}
					query += " ORDER BY" 
							+ "  fd.sort_order" 
							+ ";";
					statement = connection.prepareStatement(query);
					statement.setString(1, userId);
					statement.setString(2, languageId);
					if (localeId != null) {
						statement.setString(3, localeId);
					}

					rs = statement.executeQuery();
					if (rs != null) {
						List<DictionaryEntryDetails> entries = new ArrayList<DictionaryEntryDetails> ();
						while (rs.next()) {
							DictionaryEntryDetails entry = new DictionaryEntryDetails(rs.getString("fd.uuid")
									, rs.getString("fd.translation"), rs.getInt("fd.sort_order"));
							entries.add(entry);
						}
						dictionary = new DictionaryDetails (FAVORITES_CONTEXT, languageId, localeId, entries);
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return dictionary;
			}});
	}


	@Override
	public String lookupTranslation(final String id, final String language, final String locale) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {

				String result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					statement = connection.prepareStatement("SELECT translation FROM dictionary_entry_translation WHERE entry_uuid = ? AND language = ? AND locale = ?");
					statement.setString(1, id);
					statement.setString(2, language);
					statement.setString(3, locale);
					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						result = rs.getString("translation");
					}

				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public Integer logUsage(final String id, final String language, final String locale, String gender) {
		return withDB(new JDBCRunner<Integer>(){
			@Override
			public Integer run(Connection connection) throws Exception {

				Integer result = null;
				PreparedStatement statement = null;
				try {
					statement = connection.prepareStatement("UPDATE dictionary_entry_translation SET usage_count = usage_count + 1, last_usage_ts = NOW() WHERE entry_uuid = ? AND language = ? AND locale = ?");
					statement.setString(1, id);
					statement.setString(2, language);
					statement.setString(3, locale);

					result = statement.executeUpdate();
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public String createAdhocEntry(final String langOut, final String locOut, final String messageText, final String userId) {
		String uuid = lookupAdhocEntryByText(messageText, langOut, locOut);
		if (uuid != null) {
			return uuid;
		}

		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = null;
				PreparedStatement statement = null;
				try {
					String uuid = String.valueOf(UUID.randomUUID());
					final String query = 
							"INSERT INTO adhoc_dictionary_entry (uuid, language, locale, translation, created_by_user_uuid)"
									+ " VALUES (?, ?, ?, ?, ?)";
					statement = connection.prepareStatement(query);
					statement.setString(1, uuid);
					statement.setString(2, langOut);
					statement.setString(3, locOut);
					statement.setString(4, messageText);
					statement.setString(5, userId);

					if (statement.executeUpdate() == 1) {
						result = uuid;
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public String lookupAdhocEntry(final String id, final String language, final String locale) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {

				String result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					statement = connection.prepareStatement("SELECT translation FROM adhoc_dictionary_entry WHERE uuid = ? AND language = ? AND locale = ?");
					statement.setString(1, id);
					statement.setString(2, language);
					statement.setString(3, locale);
					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						result = rs.getString("translation");
					}

				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public String lookupAdhocEntryByText(final String messageText, final String language, final String locale) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {

				String result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					statement = connection.prepareStatement("SELECT uuid FROM adhoc_dictionary_entry WHERE translation = ? AND language = ? AND locale = ?");
					statement.setString(1, messageText);
					statement.setString(2, language);
					statement.setString(3, locale);
					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						result = rs.getString("uuid");
					}

				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public Integer logAdhocEntryUsage(final String id, final String language, final String locale, String gender) {
		return withDB(new JDBCRunner<Integer>(){
			@Override
			public Integer run(Connection connection) throws Exception {

				Integer result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					statement = connection.prepareStatement("UPDATE adhoc_dictionary_entry SET usage_count = usage_count + 1, last_usage_ts = NOW() WHERE uuid = ? AND language = ? AND locale = ?");
					statement.setString(1, id);
					statement.setString(2, language);
					statement.setString(3, locale);

					result = statement.executeUpdate();
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public String addFavorite(final String messageText, final String userId,
			final String language, final String locale) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;

				try {
					final String query = 
							"SELECT uuid, UNIX_TIMESTAMP(`delete_ts`) AS dts"
									+ " FROM favorites_dictionary_entry"
									+ " WHERE user_uuid = ?"
									+ " AND translation = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, userId);
					statement.setString(2, messageText);

					LOGGER.info(query);
					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						final String id = rs.getString("uuid");
						if (rs.getInt("dts") == 0) {
							result = id;
						} else {
							final String query2 = 
									"UPDATE favorites_dictionary_entry SET delete_ts = 0"
											+ " WHERE uuid = ?";
							statement.close();
							statement = connection.prepareStatement(query2);
							statement.setString(1, id);

							LOGGER.info(query2);
							if (statement.executeUpdate() == 1) {
								result = id;
							}
						}
					} else {
						String uuid = String.valueOf(UUID.randomUUID());
						final String query2 = 
								"INSERT INTO favorites_dictionary_entry (uuid, user_uuid, language, locale, translation)"
										+ " VALUES (?, ?, ?, ?, ?)";
						statement.close();
						statement = connection.prepareStatement(query2);
						statement.setString(1, uuid);
						statement.setString(2, userId);
						statement.setString(3, language);
						statement.setString(4, locale);
						statement.setString(5, messageText);

						LOGGER.info(query2);
						if (statement.executeUpdate() == 1) {
							result = uuid;
						}			
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Boolean setFavoritesSortOrder(final Map<String, Integer> entries) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				Boolean result = true;
				PreparedStatement statement = null;
				try {
					statement = connection.prepareStatement(
							"UPDATE favorites_dictionary_entry"
									+ " SET sort_order = ?"
									+ " WHERE uuid = ?");
					for (Entry<String, Integer> entry: entries.entrySet()) {
						statement.setInt(1, entry.getValue());
						statement.setString(2, entry.getKey());

						if (statement.executeUpdate() != 1) {
							result = false;
							break;
						}
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public Boolean setFavoriteSortOrder(final String id, final int sortOrder) {
		Map<String, Integer>entries = new HashMap<String, Integer>();
		entries.put(id, sortOrder);
		return setFavoritesSortOrder(entries);
	}

	@Override
	public Boolean deleteFavorite(final String id) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {

				Boolean result = null;
				PreparedStatement statement = null;
				try {
					statement = connection.prepareStatement(
							"UPDATE favorites_dictionary_entry"
									+ " SET delete_ts = NOW()"
									+ " WHERE uuid = ?");
					statement.setString(1, id);

					result = (statement.executeUpdate() == 1);
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {	
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public Integer logFavoritesEntryUsage(final String id) {
		return withDB(new JDBCRunner<Integer>(){
			@Override
			public Integer run(Connection connection) throws Exception {

				Integer result = null;
				PreparedStatement statement = null;
				try {
					statement = connection.prepareStatement(
							"UPDATE favorites_dictionary_entry"
									+ " SET usage_count = usage_count + 1, last_usage_ts = NOW()"
									+ " WHERE uuid = ?");
					statement.setString(1, id);

					result = statement.executeUpdate();
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}
}
