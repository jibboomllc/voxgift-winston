package com.voxgift.winstonplus.data.server;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

import javax.sql.DataSource;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class JdbcResource {

	private final static Logger LOGGER = Logger.getLogger(JdbcResource.class.getName()); 

	private DataSource dataSource;

	protected DataSource getDataSource() {

		if (dataSource == null) {
			InitialContext ic;
			try {
				ic = new InitialContext();
				dataSource = (DataSource)ic.lookup("java:comp/env/jdbc/CircleJDBCResource");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		return dataSource;
	}

	static interface JDBCRunner <T> {
		T run(Connection connection) throws Exception;
	}

	protected  <T> T withDB(JDBCRunner<T> runner) {
		Connection connection = null;
		try {
			connection = getDataSource().getConnection();
			boolean auto = connection.getAutoCommit();
			connection.setAutoCommit(false);
			T result = runner.run(connection);
			connection.commit();
			connection.setAutoCommit(auto); //set it to what it was previously.
			return result;
		} catch (Exception ex) {
			if (connection!=null) {
				try {
					connection.rollback();
				} catch (SQLException e) {
					LOGGER.warning("SQLException: " + e);
				}
			}
			//throw new MessageStoreException(ex);
			LOGGER.warning("Exception: " + ex);
		} finally {
			if (connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					LOGGER.warning("SQLException: " + e);
				}
			}
		}
		return null;
	}

}
