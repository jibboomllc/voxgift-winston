package com.voxgift.winstonplus.data.client.rpc;

import java.util.Date;

import com.voxgift.winstonplus.data.shared.model.MessageDetails;

public interface MessageService {

	MessageDetails[] getMessages(String circleId, Date startDate, Integer limit);
	String postMessage(String circleId, String userId, String messageText);
	String saveMessage(String circleId, String userId, String messageText);
	Boolean pushMessage(String circleId, String userId, String messageId);
}
