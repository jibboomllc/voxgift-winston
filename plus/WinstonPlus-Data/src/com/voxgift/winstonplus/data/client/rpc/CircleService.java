package com.voxgift.winstonplus.data.client.rpc;

import com.voxgift.winstonplus.data.shared.model.CircleDetails;
import com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails;

public interface CircleService {	
	CircleDetails getCircle(final String circleId);
	String createCircle(final String ownerId, final String name, final int statusId, final int ownerRoleId);
	String createCircle(final String ownerId, final String name, final int statusId, final int ownerRoleId, final String sponsorId);
	Boolean updateCircleName(final String circleId, final String name);
	Boolean updateCircleStatus(final String circleId, final int statusId);

	CircleUserStatusDetails getCircleUserStatus(final String circleId, final String userId);
	Boolean setCircleUserStatus(final String circleId, final String userId, final int statusId); 
	Boolean updateUserRole(final String circleId, final String userId, final int roleId);
	String inviteUser(String circleId, String userId, final String userEmail, final String firstName, final String lastName, final int roleId);
	Integer inviteUsersByEmails(final String circleId, final String[] emails, final int roleId);
}
