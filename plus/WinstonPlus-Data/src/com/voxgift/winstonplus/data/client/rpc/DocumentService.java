package com.voxgift.winstonplus.data.client.rpc;

import java.util.List;

import com.voxgift.winstonplus.data.shared.model.DocumentDetails;

public interface DocumentService {
	List<DocumentDetails> getDocuments(final String language, final String locale, final int typeId);
	String getDocument(final String documentId, final String language, final String locale);
}
