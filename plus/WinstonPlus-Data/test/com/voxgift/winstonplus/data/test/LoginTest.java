package com.voxgift.winstonplus.data.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.BeforeClass;
import org.junit.Test;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import com.voxgift.winstonplus.data.client.rpc.UserService;
import com.voxgift.winstonplus.data.server.UserServiceImpl;
import com.voxgift.winstonplus.data.shared.model.UserDetails;

public class LoginTest {
	private String email = "tom@giesberg.net";
	private String password = "tom";
	private String id = "067e6162-3b6f-4ae2-a171-2470b63dff00";

	@BeforeClass
	public static void setUpClass() throws Exception {
		try {
			// Create initial context
			System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
					"org.apache.naming.java.javaURLContextFactory");
			System.setProperty(Context.URL_PKG_PREFIXES, 
					"org.apache.naming");            
			InitialContext ic = new InitialContext();

			ic.createSubcontext("java:");
			ic.createSubcontext("java:comp");
			ic.createSubcontext("java:comp/env");
			ic.createSubcontext("java:comp/env/jdbc");

			// Construct DataSource
			MysqlConnectionPoolDataSource ds = new MysqlConnectionPoolDataSource();

			ds.setURL("jdbc:mysql://localhost:3306/voxcircle");
			ds.setUser("circle_user");
			ds.setPassword("c1rcl3");

			ic.bind("java:comp/env/jdbc/CircleJDBCResource", ds);
		} catch (NamingException ex) {
			Logger.getLogger(LoginTest.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	@Test
	public void testBasic() {
		UserService userSvc = new UserServiceImpl();
		String response = userSvc.login(email, password);
		assertNotNull(response);
		response = userSvc.login(email, "bar");
		assertNull(response);
		response = userSvc.login("testBasic_foo1", password);
		assertNull(response);
		response = userSvc.login("testBasic_foo2", "bar");
		assertNull(response);
	}
	
	@Test
	public void testLookup() {
		UserService userSvc = new UserServiceImpl();
		String userId = userSvc.lookupIdByEmail(email);
		assertNotNull(userId);
		assertEquals(userId, id);
		userId = userSvc.lookupIdByEmail("testLookup_foo");
		assertNull(userId);
	}

	@Test
	public void testSession() {
		UserService userSvc = new UserServiceImpl();
		String sid = userSvc.login(email, password);
		assertNotNull(sid);
		String userId = userSvc.loginFromSession(sid);
		assertNotNull(userId);
		assertEquals(userId, id);
		
		Boolean response = userSvc.logout(sid);
		assertTrue(response);
		userId = userSvc.loginFromSession(sid);
		assertNull(userId);
		
		response = userSvc.logout("testSession_foo");
		assertFalse(response);
		
		userId = userSvc.loginFromSession("testSession_bar");
		assertNull(userId);
	}

	@Test
	public void testTempPassword() {
		UserService userSvc = new UserServiceImpl();
		String tempPassword = userSvc.generateTempPassword(email);
		assertNotNull(tempPassword);
		
		// the old password should still work
		String sid = userSvc.login(email, password);
		assertNotNull(sid);

		// get details so we can reset the password
		UserDetails userDetails = userSvc.getUser(sid);
		assertNotNull(userDetails);
		assertEquals(userDetails.getId(), id);

		// now try the temporary one
		sid = userSvc.loginWithTempPassword(email, tempPassword);
		assertNotNull(sid);
		
		// a second try should not work
		sid = userSvc.loginWithTempPassword(email, tempPassword);
		assertNull(sid);
	
		// wrong data should not work
		sid = userSvc.loginWithTempPassword(email, "testTempPassword_bar");
		assertNull(sid);
		sid = userSvc.loginWithTempPassword("testTempPassword_foo", tempPassword);
		assertNull(sid);
		sid = userSvc.loginWithTempPassword("testTempPassword_foo", "testTempPassword_bar");
		assertNull(sid);		
		
		// reset password
		Boolean bResult = userSvc.updateUser(id, userDetails.getPrimaryEmail(), password
				, userDetails.getFirstName(), userDetails.getLastName()
				, userDetails.getLanguage(), userDetails.getLocale()
				, userDetails.getGender());
		assertTrue(bResult);
	
	}
	
}
