package com.voxgift.winstonplus.data.test;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.junit.BeforeClass;
import org.junit.Test;
import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;
import com.voxgift.winstonplus.data.client.rpc.DocumentService;
import com.voxgift.winstonplus.data.server.DocumentServiceImpl;
import com.voxgift.winstonplus.data.shared.model.DocumentDetails;

import static org.junit.Assert.*;

public class DocumentServiceTest {
	@BeforeClass
	public static void setUpClass() throws Exception {
		try {
			// Create initial context
			System.setProperty(Context.INITIAL_CONTEXT_FACTORY,
					"org.apache.naming.java.javaURLContextFactory");
			System.setProperty(Context.URL_PKG_PREFIXES, 
					"org.apache.naming");            
			InitialContext ic = new InitialContext();

			ic.createSubcontext("java:");
			ic.createSubcontext("java:comp");
			ic.createSubcontext("java:comp/env");
			ic.createSubcontext("java:comp/env/jdbc");

			// Construct DataSource
			MysqlConnectionPoolDataSource ds = new MysqlConnectionPoolDataSource();

			ds.setURL("jdbc:mysql://localhost:3306/voxcircle");
			ds.setUser("circle_user");
			ds.setPassword("c1rcl3");

			ic.bind("java:comp/env/jdbc/CircleJDBCResource", ds);
		} catch (NamingException ex) {
			Logger.getLogger(DocumentServiceTest.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	@Test
	public void testBasic() {
		DocumentService docSvc = new DocumentServiceImpl();
		String contents = docSvc.getDocument("f2191d3f-b9d9-4ae5-98b7-b445de932342", "en", "US");
		assertNotNull(contents);
		contents = docSvc.getDocument("f2191d3f-b9d9-4ae5-98b7-b445de932342", "xx", "YY");
		assertNotNull(contents);
		contents = docSvc.getDocument("1", "en", "US");
		assertNull(contents);
	}

	@Test
	public void testBasicList() {
		DocumentService docSvc = new DocumentServiceImpl();
		List<DocumentDetails> list = docSvc.getDocuments("en", "US", 1);
		assertTrue(list.size() > 0);
		list = docSvc.getDocuments("xx", "YY", 1);
		assertTrue(list.size() > 0);
		list = docSvc.getDocuments("en", "US", 999);
		assertEquals(list.size(), 0);
	}
}
