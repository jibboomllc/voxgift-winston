/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class UserSettingsPlace extends Place {
	public static class UserSettingsPlaceTokenizer implements PlaceTokenizer<UserSettingsPlace> {

		@Override
		public UserSettingsPlace getPlace(String token) {
			return new UserSettingsPlace();
		}

		@Override
		public String getToken(UserSettingsPlace place) {
			return "";
		}

	}
}
