/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;
import com.voxgift.winstonplus.client.activities.compose.ComposePlace.ComposePlaceTokenizer;
import com.voxgift.winstonplus.client.activities.messagelist.MessageListPlace.MessageListPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.settings.SettingsPlace.SettingsPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsPlace.CircleSettingsPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsPlace.BasicCircleSettingsPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsPlace.UserCircleSettingsPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsPlace.LegalDocumentsPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentPlace.LegalDocumentPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.settings.user.UserSettingsPlace.UserSettingsPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsPlace.BasicUserSettingsPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsPlace.FavoritesUserSettingsPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsPlace.ImageUserSettingsPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckPlace.ConnectCheckPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace.HelpPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace.LoginPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.welcome.register.RegisterPlace.RegisterPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordPlace.ResetPasswordPlaceTokenizer;
import com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace.WelcomePlaceTokenizer;
import com.voxgift.winstonplus.client.places.HomePlace.HomePlaceTokenizer;

/**
 * @author Tom Giesberg
 * 
 */
@WithTokenizers({ HomePlaceTokenizer.class
	, ConnectCheckPlaceTokenizer.class
	, WelcomePlaceTokenizer.class
	, RegisterPlaceTokenizer.class
	, LoginPlaceTokenizer.class
	, ResetPasswordPlaceTokenizer.class
	, HelpPlaceTokenizer.class
	, ComposePlaceTokenizer.class
	, MessageListPlaceTokenizer.class
	, SettingsPlaceTokenizer.class
	, UserSettingsPlaceTokenizer.class
	, BasicUserSettingsPlaceTokenizer.class
	, ImageUserSettingsPlaceTokenizer.class
	, FavoritesUserSettingsPlaceTokenizer.class
	, CircleSettingsPlaceTokenizer.class
	, UserCircleSettingsPlaceTokenizer.class
	, BasicCircleSettingsPlaceTokenizer.class
	, LegalDocumentsPlaceTokenizer.class
	, LegalDocumentPlaceTokenizer.class
	})
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {
}
