package com.voxgift.winstonplus.client.activities.compose;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.media.client.Audio;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.gwtphonegap.client.PhoneGap;
import com.googlecode.gwtphonegap.client.media.Media;
import com.googlecode.gwtphonegap.client.media.MediaCallback;
import com.googlecode.gwtphonegap.client.media.MediaError;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedEvent;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.DecoratedActivity;
import com.voxgift.winstonplus.shared.AppConstants;
import com.voxgift.winstonplus.data.shared.model.DictionaryDetails;
import com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDictionaryService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDictionaryServiceAsync;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcMessageService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcMessageServiceAsync;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserServiceAsync;

public class ComposeActivity extends DecoratedActivity {

	//private Logger LOGGER = Logger.getLogger(getClass().getName());

	private static int MAX_INPUT_LENGTH = 512;

	private static int SHORTENED_MESSAGE_LENGTH = 16;

	private static String FAVORITES_CONTEXT = "c148d0d2-2936-4a30-a4de-e6f66ea92782";

	private ComposeView view;

	private RpcMessageServiceAsync messageSvc = GWT.create(RpcMessageService.class);

	private RpcDictionaryServiceAsync dictionarySvc = GWT.create(RpcDictionaryService.class);

	private RpcUserServiceAsync userSvc = GWT.create(RpcUserService.class);

	private final PhoneGap phoneGap;

	private Media media;

	private List<DictionaryDetails> dictionaries = new ArrayList<DictionaryDetails>();

	private int selectedDictionaryIndex = 0;

	private int selectedDictionaryItemIndex = -1;

	public ComposeActivity(ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getComposeView());

		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) messageSvc, appConstants.webAppUrl(), "rpc/message");
		PhonegapUtil.prepareService((ServiceDefTarget) dictionarySvc, appConstants.webAppUrl(), "rpc/dictionary");
		PhonegapUtil.prepareService((ServiceDefTarget) userSvc, appConstants.webAppUrl(), "rpc/user");

		view = getClientFactory().getComposeView();

		phoneGap = getClientFactory().getPhoneGap();
	}

	private class DCellSelectedHandler implements CellSelectedHandler {

		@Override
		public void onCellSelected(CellSelectedEvent event) {
			int index = event.getIndex();
			view.setSelectedIndex(selectedDictionaryIndex, selectedDictionaryItemIndex, false);
			view.setSelectedIndex(selectedDictionaryIndex, index, true);
			selectedDictionaryItemIndex = index;

			view.getInputText().setText(dictionaries.get(selectedDictionaryIndex).getEntries().get(index).getEntryText());
		}
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		panel.setWidget(view);

		getClientFactory().getFrameView().showBackbutton(false);

		refreshDictionaries(getClientFactory().getUser().getLanguage(), getClientFactory().getUser().getLocale());
	}

	@Override
	public void onDataReady() {
		super.onDataReady();

		addHandlerRegistration(view.getDictionaryCarousel().addSelectionHandler(new SelectionHandler<Integer>() {

			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				if (selectedDictionaryIndex < dictionaries.size() 
						&& dictionaries.get(selectedDictionaryIndex) != null
						&& dictionaries.get(selectedDictionaryIndex).getEntries() != null
						&& selectedDictionaryItemIndex < dictionaries.get(selectedDictionaryIndex).getEntries().size()
						) {
					view.setSelectedIndex(selectedDictionaryIndex, selectedDictionaryItemIndex, false);
				}
				selectedDictionaryIndex = event.getSelectedItem();
				selectedDictionaryItemIndex = -1;
			}
		}));

		addHandlerRegistration(view.getCurrentInput().addKeyUpHandler(new KeyUpHandler() {

			@Override
			public void onKeyUp(KeyUpEvent event) {
				String text = view.getInputText().getText();
				if (text.length() > MAX_INPUT_LENGTH) {
					view.getInputText().setText(text.substring(0, MAX_INPUT_LENGTH-1));
				}
			}
		}));

		addHandlerRegistration(view.getInputEditor().addValueChangeHandler(new ValueChangeHandler<String>() {

			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				String text = view.getInputText().getText();
				if (text.length() > MAX_INPUT_LENGTH) {
					view.getInputText().setText(text.substring(0, MAX_INPUT_LENGTH-1));
				}				
			}
		}));

		addHandlerRegistration(view.getClearButton().addTapHandler(
				new TapHandler() {

					@Override
					public void onTap(TapEvent event) {
						view.getInputText().setText("");
					}
				}));

		addHandlerRegistration(view.getTalkButton().addTapHandler(
				new TapHandler() {

					@Override
					public void onTap(TapEvent event) {
						if (view.getInputText().getText().isEmpty()) {
							setStatusText(" Please enter some text before pressing Speak", 1000);
						} else {
							// TODO I18n Should lang out =? in
							playMessage(getClientFactory().getUser().getId()
									, getClientFactory().getUser().getLanguage(), getClientFactory().getUser().getLocale()
									, getClientFactory().getUser().getLanguage(), getClientFactory().getUser().getLocale()
									, getClientFactory().getUser().getGender()
									, view.getInputText().getText());

						}
					}
				}));

		addHandlerRegistration(view.getPostButton().addTapHandler(
				new TapHandler() {

					@Override
					public void onTap(TapEvent event) {
						if (view.getInputText().getText().isEmpty()) {
							setStatusText("Please enter some text before pressing Send", 1000);
						} else {
							if (getClientFactory().getCircle() != null && getClientFactory().getCircle().getCircleId() != null) {
								postMessage(getClientFactory().getCircle().getCircleId()
										, getClientFactory().getUser().getId()
										, view.getInputText().getText());
							} else {
								setStatusText("You must be in a circle in order to send a message", 1000);
							}

						}
					}
				}));

		addHandlerRegistration(view.getAddFavoriteButton().addTapHandler(
				new TapHandler() {

					@Override
					public void onTap(TapEvent event) {
						if (view.getInputText().getText().isEmpty()) {
							setStatusText("Please enter some text before adding as a Favorite", 1000);
						} else {
							addFavorite(view.getInputText().getText()
									, getClientFactory().getUser().getId()
									, getClientFactory().getUser().getLanguage()
									, getClientFactory().getUser().getLocale());
						}
					}
				}));

		view.renderDictionaries(dictionaries);

		for(int i=0; i<dictionaries.size(); i++) {
			addHandlerRegistration(view.getList(i).addCellSelectedHandler(new DCellSelectedHandler()));
		}
	}

	private void refreshDictionaries(final String languageId, final String localeId) {
		final AsyncCallback<List<DictionaryDetails>> callbackStandard = new AsyncCallback<List< DictionaryDetails>>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(List<DictionaryDetails> result) {
				if(result != null) {
					dictionaries.addAll(result);
				}
				if(view != null && dictionaries != null) {
					onDataReady();
				}
			}
		};

		final AsyncCallback<DictionaryDetails> callbackFavorites = new AsyncCallback< DictionaryDetails>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(DictionaryDetails result) {
				if(result != null && result.getEntries() != null && !result.getEntries().isEmpty()) {
					dictionaries.add(result);
				}
				if (serverIsReachable()) {
					dictionarySvc.getDictionaries(languageId, localeId, callbackStandard);
				}
			}
		};

		dictionaries.clear();
		if (serverIsReachable()) {
			dictionarySvc.getFavoritesDictionary(getClientFactory().getUser().getId()
					, getClientFactory().getUser().getLanguage(), getClientFactory().getUser().getLocale()
					, callbackFavorites);
		}
	}

	public String lookUpMessage(final String langIn, final String locIn, final String messageText) {
		String id = null;
		for (DictionaryEntryDetails de : dictionaries.get(selectedDictionaryIndex).getEntries()) {
			if (de.getEntryText().equals(messageText)) {
				return de.getEntryId();  
			}
		}
		return id;
	}

	public void playAudio(final String url) {
		if (serverIsReachable()) {
			if (MGWT.getOsDetection().isDesktop()) {
				//LOGGER.log(Level.INFO, "playAudio using Audio: " + url);

				final Audio messageAudio = Audio.createIfSupported();
				if (messageAudio != null) {
					messageAudio.setSrc(url);
					messageAudio.play();
				} else {
					//LOGGER.log(Level.WARNING, "Audio.createIfSupported() returned null");
				}

			} else {
				//LOGGER.log(Level.INFO, "playAudio using phoneGap.getMedia: " + url);

				media = phoneGap.getMedia().create(url, new MediaCallback() {

					@Override
					public void onSuccess() {
						setStatusText("");
						media.release();
					}

					@Override
					public void onStatusChange() {
					//	setStatusText(view.getStatusText().getText() + ".");
					}

					@Override
					public void onError(MediaError error) {
						setErrorText("error: " + error.getErrorCode() + " " + error.getErrorMessage(), 5000);
						media.release();
					}
				});
				setStatusText("Audio playing");
				//LOGGER.log(Level.INFO, "playAudio getMedia created now playing");
				media.play();

			}
		}
	}

	public void playMessage(final String userId, final String langIn, final String locIn, final String langOut, final String locOut, final String gender, final String messageText) {
		AppConstants appConstants = GWT.create(AppConstants.class);
		final String url = appConstants.audioUrl(); 

		String entryID = lookUpMessage(langIn, locIn, messageText);
		if (entryID != null) {
			playAudio(url + entryID + "_" + langOut + "_" + locOut + "_" + gender + ".mp3");
			logMessagePlay(entryID, langOut, locOut, gender, true);
		} else {
			setStatusText("Creating audio");

			AsyncCallback<String> callback = new AsyncCallback<String>() {
				public void onFailure(Throwable caught) {
					setErrorText("Error creating audio: " + caught.getLocalizedMessage(), 5000);
					checkServerCallIssue(caught);
				}

				public void onSuccess(String result) {
					if (result != null) {

						String shortenedMessage = messageText;
						if (shortenedMessage.length() > SHORTENED_MESSAGE_LENGTH) {
							shortenedMessage = shortenedMessage.substring(0, SHORTENED_MESSAGE_LENGTH - 2) + "..";
						}
						setStatusText("'" + shortenedMessage + "' audio created", 1000);
						view.getInputText().setText("");

						playAudio(url + result + "_" + langOut + "_" + locOut + "_" + gender + ".mp3");
						logMessagePlay(result, langOut, locOut, gender, false);
					}
				}
			};

			if (serverIsReachable()) {
				dictionarySvc.createAdhocEntry(langOut, locOut, messageText, userId, callback);
			}
		}
	}

	public void logMessagePlay(final String id, final String language, final String locale, final String gender, Boolean inDictionary) {
		AsyncCallback<Integer> callback = new AsyncCallback<Integer>() {
			public void onFailure(Throwable caught) {
			}

			public void onSuccess(Integer result) {
			}
		};

		if (serverIsReachable()) {
			if(inDictionary) {
				if (dictionaries.get(selectedDictionaryIndex).getContextId().equals(FAVORITES_CONTEXT)) {
					dictionarySvc.logFavoritesEntryUsage(id, callback);
				} else {
					dictionarySvc.logUsage(id, language, locale, gender, callback);
				}
			} else {
				dictionarySvc.logAdhocEntryUsage(id, language, locale, gender, callback);
			}
		}
	}

	public void postMessage(final String circleId, final String userId, final String messageText) {

		if (!messageText.isEmpty()) {
			setStatusText("Sending");

			final AsyncCallback<Boolean> pushCallback = new AsyncCallback<Boolean>() {
				public void onFailure(Throwable caught) {
					setErrorText("Server error pushing message: " + caught.getLocalizedMessage(), 5000);
					checkServerCallIssue(caught);
				}

				public void onSuccess(Boolean result) {
				}
			};

			final AsyncCallback<String> saveCallback = new AsyncCallback<String>() {
				public void onFailure(Throwable caught) {
					setErrorText("Server error saving message: " + caught.getLocalizedMessage(), 5000);
					checkServerCallIssue(caught);
				}

				public void onSuccess(String result) {
					if (result != null && serverIsReachable()) {
						messageSvc.pushMessage(circleId, userId, result, pushCallback);

						String shortenedMessage = messageText;
						if (shortenedMessage.length() > SHORTENED_MESSAGE_LENGTH) {
							shortenedMessage = shortenedMessage.substring(0, SHORTENED_MESSAGE_LENGTH - 2) + "..";
						}
						setStatusText("'" + shortenedMessage + "' sent", 1000);
						view.getInputText().setText("");
					} else {
						setStatusText("Error saving message");
					}

				}
			};

			if (serverIsReachable()) {
				messageSvc.saveMessage(circleId, userId, messageText, saveCallback);
			}
		}
	}

	public void addFavorite(final String messageText, final String userId
			, final String language, final String locale) {
		setStatusText("Adding favorite");

		AsyncCallback<String> callback = new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				setErrorText("Error adding favorite: " + caught.getLocalizedMessage(), 5000);
				checkServerCallIssue(caught);
			}

			public void onSuccess(String result) {
				if (result != null) {
					refreshDictionaries(getClientFactory().getUser().getLanguage(), getClientFactory().getUser().getLocale());
					setStatusText("'" + messageText + "' favorite added", 1000);
					view.getInputText().setText("");
				}
			}
		};

		if (serverIsReachable()) {
			dictionarySvc.addFavorite(messageText, userId, language, locale, callback);
		} else {
			setStatusText(null);
		}
	}

}
