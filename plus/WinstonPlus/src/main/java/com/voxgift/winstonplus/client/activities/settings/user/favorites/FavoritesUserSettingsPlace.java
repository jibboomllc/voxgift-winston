/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user.favorites;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class FavoritesUserSettingsPlace extends Place {
	public static class FavoritesUserSettingsPlaceTokenizer implements PlaceTokenizer<FavoritesUserSettingsPlace> {

		@Override
		public FavoritesUserSettingsPlace getPlace(String token) {
			return new FavoritesUserSettingsPlace();
		}

		@Override
		public String getToken(FavoritesUserSettingsPlace place) {
			return "";
		}

	}
}
