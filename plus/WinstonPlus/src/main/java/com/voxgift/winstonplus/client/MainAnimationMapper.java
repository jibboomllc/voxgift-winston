package com.voxgift.winstonplus.client;

import com.google.gwt.place.shared.Place;
import com.googlecode.mgwt.mvp.client.Animation;
import com.googlecode.mgwt.mvp.client.AnimationMapper;
import com.voxgift.winstonplus.client.activities.settings.SettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsPlace;
import com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentPlace;
import com.voxgift.winstonplus.client.activities.settings.user.UserSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsPlace;

public class MainAnimationMapper implements AnimationMapper {

	@Override
	public Animation getAnimation(Place oldPlace, Place newPlace) {

		if (newPlace instanceof SettingsPlace && 
				((oldPlace instanceof UserSettingsPlace)||(oldPlace instanceof CircleSettingsPlace))) {
			return Animation.SLIDE_REVERSE;
		}
		
		if (newPlace instanceof UserSettingsPlace && 
				((oldPlace instanceof BasicUserSettingsPlace)
						||(oldPlace instanceof ImageUserSettingsPlace)
						||(oldPlace instanceof FavoritesUserSettingsPlace)
						)) {
			return Animation.SLIDE_REVERSE;
		}
		
		if (newPlace instanceof CircleSettingsPlace && oldPlace instanceof BasicCircleSettingsPlace) {
			return Animation.SLIDE_REVERSE;
		}
		
		if (newPlace instanceof BasicCircleSettingsPlace && oldPlace instanceof UserCircleSettingsPlace) {
			return Animation.SLIDE_REVERSE;
		}
		
		if (newPlace instanceof LegalDocumentsPlace && oldPlace instanceof LegalDocumentPlace) {
			return Animation.SLIDE_REVERSE;
		}

		if (newPlace instanceof UserSettingsPlace
				|| newPlace instanceof BasicUserSettingsPlace
				|| newPlace instanceof ImageUserSettingsPlace
				|| newPlace instanceof FavoritesUserSettingsPlace
				|| newPlace instanceof CircleSettingsPlace
				|| newPlace instanceof BasicCircleSettingsPlace
				|| newPlace instanceof UserCircleSettingsPlace
				|| newPlace instanceof LegalDocumentsPlace
				|| newPlace instanceof LegalDocumentPlace
				) {
			return Animation.SLIDE;
		}

		return null;
	}

}
