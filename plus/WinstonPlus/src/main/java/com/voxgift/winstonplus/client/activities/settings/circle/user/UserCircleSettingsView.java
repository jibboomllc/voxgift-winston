/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.circle.user;

import java.util.List;

import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.user.client.ui.HasText;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.dialog.ConfirmDialog.ConfirmCallback;
import com.googlecode.mgwt.ui.client.dialog.Dialogs.OptionsDialogEntry;
import com.voxgift.winstonplus.client.DecoratedView;

/**
 * @author Tom Giesberg
 * 
 */
public interface UserCircleSettingsView extends DecoratedView {

	public HasText getLookupText();

	public HasText getEmailAddress();
	public HasChangeHandlers getEmailAddressEditor();
	
	public HasText getFirstName();
	public HasChangeHandlers getFirstNameEditor();
	
	public HasText getLastName();
	public HasChangeHandlers getLastNameEditor();
	
	public void addRoleItem(String item);
	public void clearRoleItems();
	public int getSelectedRoleIndex();
	public void setSelectedRoleIndex(int index, boolean selected);
	public HasChangeHandlers getRoleList();

	public HasText getUserStatus();

	public HasTapHandlers getLookupButton();
	public void showLookupResults(List<OptionsDialogEntry> optionText, com.googlecode.mgwt.ui.client.dialog.Dialogs.OptionCallback callback);
	
	public HasTapHandlers getInviteButton();
	
	public HasTapHandlers getRemoveButton();

	public void enableEditing(boolean isEditable);

	public void confirmRemove(String title, String text, ConfirmCallback callback);
}
