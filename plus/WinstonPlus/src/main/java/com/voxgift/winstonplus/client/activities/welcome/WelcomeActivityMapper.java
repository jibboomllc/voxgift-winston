package com.voxgift.winstonplus.client.activities.welcome;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckActivity;
import com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckPlace;
import com.voxgift.winstonplus.client.activities.welcome.help.HelpActivity;
import com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace;
import com.voxgift.winstonplus.client.activities.welcome.login.LoginActivity;
import com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace;
import com.voxgift.winstonplus.client.activities.welcome.register.RegisterActivity;
import com.voxgift.winstonplus.client.activities.welcome.register.RegisterPlace;
import com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordActivity;
import com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordPlace;
import com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomeActivity;
import com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace;

public class WelcomeActivityMapper implements ActivityMapper {

	private final ClientFactory clientFactory;

	private Place lastPlace;

	public WelcomeActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public Activity getActivity(Place place) {
		Activity activity = getActivity(lastPlace, place);
		lastPlace = place;
		return activity;
	}

	private ConnectCheckActivity connectCheckActivity;

	private ConnectCheckActivity getConnectCheckActivity() {
		if (connectCheckActivity == null) {
			connectCheckActivity = new ConnectCheckActivity(clientFactory);
		}

		return connectCheckActivity;
	}

	private WelcomeActivity welcomeActivity;

	private WelcomeActivity getWelcomeActivity() {
		if (welcomeActivity == null) {
			welcomeActivity = new WelcomeActivity(clientFactory);
		}

		return welcomeActivity;
	}

	private RegisterActivity registerActivity;

	private RegisterActivity getRegisterActivity() {
		if (registerActivity == null) {
			registerActivity = new RegisterActivity(clientFactory);
		}

		return registerActivity;
	}

	private LoginActivity loginActivity;

	private LoginActivity getLoginActivity() {
		if (loginActivity == null) {
			loginActivity = new LoginActivity(clientFactory);
		}

		return loginActivity;
	}

	private ResetPasswordActivity resetPasswordActivity;

	private ResetPasswordActivity getResetPasswordActivity() {
		if (resetPasswordActivity == null) {
			resetPasswordActivity = new ResetPasswordActivity(clientFactory);
		}

		return resetPasswordActivity;
	}

	private HelpActivity helpActivity;

	private HelpActivity getHelpActivity(HelpPlace place) {
		if (helpActivity == null) {
			helpActivity = new HelpActivity(clientFactory);
		}

		helpActivity.setTopicId(place.getTopicId());

		return helpActivity;
	}

	private Activity getActivity(Place lastPlace, Place newPlace) {

		if (newPlace instanceof ConnectCheckPlace) {
			return getConnectCheckActivity();
		}

		if (newPlace instanceof WelcomePlace) {
			return getWelcomeActivity();
		}

		if (newPlace instanceof RegisterPlace) {
			return getRegisterActivity();
		}

		if (newPlace instanceof LoginPlace) {
			return getLoginActivity();
		}

		if (newPlace instanceof ResetPasswordPlace) {
			return getResetPasswordActivity();
		}

		if (newPlace instanceof HelpPlace) {
			return getHelpActivity((HelpPlace) newPlace);
		}
		
		return null;
	}

}
