package com.voxgift.winstonplus.client.activities.frame;

import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.IsWidget;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;

public interface FrameView extends IsWidget, HasWidgets.ForIsWidget{
	
	public HasText getTitle();
	
	public HasHTML getSponsorIcon();
	
	public HasTapHandlers getSponsorButton();
	
	public HasHTML getCircleIcon();

	public HasTapHandlers getCircleButton();

	public HasText getBackbuttonText();

	public HasTapHandlers getBackbuttonTarget();

	public void showBackbutton(boolean show);
}
