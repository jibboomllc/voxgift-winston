package com.voxgift.winstonplus.client.widget.tabbar;

import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.MGWTStyle;
import com.googlecode.mgwt.ui.client.theme.base.TabBarCss;
import com.googlecode.mgwt.ui.client.widget.tabbar.TabBarButton;
import com.voxgift.winstonplus.client.theme.ExtendedClientBundle;

public class GearTabBarButton extends TabBarButton {

	public GearTabBarButton() {
		this(MGWTStyle.getTheme().getMGWTClientBundle().getTabBarCss(), "Gear");
	}

	public GearTabBarButton(String text) {
		this(MGWTStyle.getTheme().getMGWTClientBundle().getTabBarCss(), text);
	}

    public GearTabBarButton(TabBarCss css, String text) {
	    super(css, MGWT.getOsDetection().isIOs() || MGWT.getOsDetection().isDesktop() ? 
		    	((ExtendedClientBundle) MGWTStyle.getTheme().getMGWTClientBundle()).tabBarGearImage() : null);
				setText(text);
	}
}
