package com.voxgift.winstonplus.shared;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

public interface AppResources extends ClientBundle {
	public static final AppResources INSTANCE =  GWT.create(AppResources.class);

	//@Source("my.css")
	//public CssResource css();

	@Source("AppConfig.xml")
	public TextResource initialConfiguration();

	//@Source("manual.pdf")
	//public DataResource ownersManual();
}