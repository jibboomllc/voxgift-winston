/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.circle;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class CircleSettingsPlace extends Place {
	public static class CircleSettingsPlaceTokenizer implements PlaceTokenizer<CircleSettingsPlace> {

		@Override
		public CircleSettingsPlace getPlace(String token) {
			return new CircleSettingsPlace();
		}

		@Override
		public String getToken(CircleSettingsPlace place) {
			return "";
		}

	}
}
