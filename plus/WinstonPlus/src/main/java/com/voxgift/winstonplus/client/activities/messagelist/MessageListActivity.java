package com.voxgift.winstonplus.client.activities.messagelist;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.HasHTML;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.widget.base.PullArrowStandardHandler;
import com.googlecode.mgwt.ui.client.widget.base.PullArrowStandardHandler.PullActionHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.DecoratedActivity;
import com.voxgift.winstonplus.client.event.ActionEvent;
import com.voxgift.winstonplus.client.event.ActionNames;
import com.voxgift.winstonplus.shared.AppConstants;
import com.voxgift.winstonplus.data.shared.model.MessageDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentServiceAsync;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcMessageService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcMessageServiceAsync;


public class MessageListActivity extends DecoratedActivity {

	private static String NO_CIRCLES_HELP_TOPIC = "97d8fc68-8815-48c6-aa3e-44ae7aac0b4f";
	private static String NO_MESSAGES_HELP_TOPIC = "8e3ee11a-f9c4-40c3-97f5-bf1139a402b8";
	
	private final MessageListView view;
	private List<MessageDetails> list = new LinkedList<MessageDetails>();

	private boolean failedHeader = false;
	private boolean failedFooter = false;

	private RpcMessageServiceAsync messageSvc = GWT.create(RpcMessageService.class);
	private RpcDocumentServiceAsync docSvc = GWT.create(RpcDocumentService.class);

	public MessageListActivity(ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getHomeView());

		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) messageSvc, appConstants.webAppUrl(), "rpc/message");
		PhonegapUtil.prepareService((ServiceDefTarget) docSvc, appConstants.webAppUrl(), "rpc/document");

		view = clientFactory.getHomeView();
	}

	@Override
	public void start(AcceptsOneWidget panel, final EventBus eventBus) {
		super.start(panel, eventBus);

		panel.setWidget(view);

		getClientFactory().getFrameView().showBackbutton(false);
		
		refreshList();
	}

	@Override
	public void onDataReady() {
		super.onDataReady();

		if (list == null || list.isEmpty()) {
			if (getClientFactory().getCircle() == null || getClientFactory().getCircle().getCircleId() ==  null) {
				showHelp(docSvc, NO_CIRCLES_HELP_TOPIC, getClientFactory().getUser().getLanguage(), getClientFactory().getUser().getLocale(), view.getHelpScreen());
			} else {
				showHelp(docSvc, NO_MESSAGES_HELP_TOPIC, getClientFactory().getUser().getLanguage(), getClientFactory().getUser().getLocale(), view.getHelpScreen());
			}
			view.getRefreshButtonText().setText("Reload");
		} else {
			view.getPullHeader().setHTML("pull down");
			view.getPullFooter().setHTML("pull up");

			PullArrowStandardHandler headerHandler = new PullArrowStandardHandler(view.getPullHeader(), view.getPullPanel());

			headerHandler.setErrorText("Error");
			headerHandler.setLoadingText("Loading");
			headerHandler.setNormalText("pull down");
			headerHandler.setPulledText("release to load");
			headerHandler.setPullActionHandler(new PullActionHandler() {
				@Override
				public void onPullAction(final AsyncCallback<Void> callback) {
					new Timer() {
						@Override
						public void run() {
							refreshList();
							if (failedHeader) {
								callback.onFailure(null);
							} else {
								callback.onSuccess(null);
							}
							failedHeader = !failedHeader;
						}
					}.schedule(1000);
				}
			});
			view.setHeaderPullHandler(headerHandler);

			PullArrowStandardHandler footerHandler = new PullArrowStandardHandler(view.getPullFooter(), view.getPullPanel());

			footerHandler.setErrorText("Error");
			footerHandler.setLoadingText("Loading");
			footerHandler.setNormalText("pull up");
			footerHandler.setPulledText("release to load");
			footerHandler.setPullActionHandler(new PullActionHandler() {
				@Override
				public void onPullAction(final AsyncCallback<Void> callback) {
					new Timer() {
						@Override
						public void run() {
							refreshList();
							if (failedFooter) {
								callback.onFailure(null);
							} else {
								callback.onSuccess(null);
							}
							failedFooter = !failedFooter;
						}
					}.schedule(1000);
				}
			});
			view.setFooterPullHandler(footerHandler);
		}
		view.render(list);
		
		addHandlerRegistration(ActionEvent.register(getEventBus(), ActionNames.CIRCLE_CHOSEN, new ActionEvent.Handler() {
			@Override
			public void onAction(ActionEvent event) {
				refreshList();
			}
		}));

		addHandlerRegistration(view.getRefreshButton().addTapHandler(new TapHandler() {
			@Override
			public void onTap(TapEvent event) {
				refreshList();
			}
		}));
	}

	private void refreshList() {

		AsyncCallback<MessageDetails[]> callback = new AsyncCallback<MessageDetails[]>() {
			public void onFailure(Throwable caught) {
				failedHeader = true;
				checkServerCallIssue(caught);
			}

			public void onSuccess(MessageDetails[] result) {
				failedHeader = false;
				list = new LinkedList<MessageDetails>();
				for (int i=0; i < result.length; i++) {
					list.add(result[i]);
				}
				onDataReady();
			}
		};

		if (serverIsReachable() && getClientFactory().getCircle() != null && getClientFactory().getCircle().getCircleId() !=  null) {
			messageSvc.getMessages(getClientFactory().getCircle().getCircleId(), new Date(), 5, callback);
		} else {
			list = new LinkedList<MessageDetails>();
			onDataReady();
		}
	}

	private void showHelp(RpcDocumentServiceAsync documentService, final String id, final String language, final String locale, final HasHTML helpView) {
		final AsyncCallback<String> getDocumentCallback = new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(String result) {
				if (result == null) {
					helpView.setHTML("Topic not found.");
				} else {
					helpView.setHTML(result);
				}
			}
		};
		if (serverIsReachable()) {
			documentService.getDocument(id, language, locale, getDocumentCallback);
		}
	}
}
