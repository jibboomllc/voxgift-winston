/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user.favorites;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedEvent;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.DecoratedActivity;
import com.voxgift.winstonplus.client.Item;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent.SettingsEntry;
import com.voxgift.winstonplus.shared.AppConstants;
import com.voxgift.winstonplus.data.shared.model.DictionaryDetails;
import com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDictionaryService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDictionaryServiceAsync;

/**
 * @author Tom Giesberg
 * 
 */
public class FavoritesUserSettingsActivity extends DecoratedActivity {

	private FavoritesUserSettingsView view;

	private List<Item> items;

	private List<DictionaryEntryDetails> dictionary;

	private RpcDictionaryServiceAsync dictionarySvc = GWT.create(RpcDictionaryService.class);

	public FavoritesUserSettingsActivity(ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getFavoritesUserSettingsView());

		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) dictionarySvc, appConstants.webAppUrl(), "rpc/dictionary");

		view = clientFactory.getFavoritesUserSettingsView();
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		getClientFactory().getFrameView().showBackbutton(!MGWT.getOsDetection().isAndroid());
		getClientFactory().getFrameView().getBackbuttonText().setText("User");
		getClientFactory().getFrameView().getTitle().setText("Favorites Settings");
		panel.setWidget(view);

		refreshData(getClientFactory().getUser().getId(), getClientFactory().getUser().getLanguage(), getClientFactory().getUser().getLocale());
	}

	@Override
	public void onDataReady() {
		super.onDataReady();

		view.renderItems(items);

		addHandlerRegistration(view.getList().addCellSelectedHandler(new CellSelectedHandler() {

			@Override
			public void onCellSelected(CellSelectedEvent event) {
				String favoritesId = items.get(event.getIndex()).getEntry();
				String id = event.getTargetElement().getId();

				if (id.equals("up")) {
					adjustFavoriteSortOrder(favoritesId, -1);
				} else if (id.equals("down")) {
					adjustFavoriteSortOrder(favoritesId, 1);
				} else if (id.equals("delete")) {
					deleteFavorite(favoritesId);
				} else if (id.equals("add")) {

				}
			}
		}));

		addHandlerRegistration(getClientFactory().getFrameView().getBackbuttonTarget().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				SettingsEntrySelectedEvent.fire(getEventBus(), SettingsEntry.USER); 
			}
		}));
	}

	private void refreshData(final String userId, final String languageId, final String localeId) {
		final AsyncCallback<DictionaryDetails> callback = new AsyncCallback<DictionaryDetails>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(DictionaryDetails result) {
				if (result == null) {
					setErrorText("User circle details not found.");
				} else {
					dictionary = result.getEntries();
					items = createItems();
					onDataReady();
				}
			}
		};

		if (serverIsReachable()) {
			dictionarySvc.getFavoritesDictionary(userId, languageId, localeId, callback);
		}
	}

	/**
	 * @return
	 */
	private List<Item> createItems() {
		ArrayList<Item> list = new ArrayList<Item>();

		int size = dictionary.size();
		if (size > 0) {
			DictionaryEntryDetails entry = dictionary.get(0);
			if (size == 1) {
				list.add(new Item(entry.getEntryText(), entry.getEntryId(), Item.Attribute.ONLY));			
			} else {
				list.add(new Item(entry.getEntryText(), entry.getEntryId(), Item.Attribute.FIRST));			

				for (int i = 1; i < size - 1; i++) {
					entry = dictionary.get(i);
					list.add(new Item(entry.getEntryText(), entry.getEntryId(), Item.Attribute.MIDDLE));			
				}

				entry = dictionary.get(size - 1);
				list.add(new Item(entry.getEntryText(), entry.getEntryId(), Item.Attribute.LAST));			
			}
		}
		//list.add(new Item("(Create new favorite)", null, Item.Attribute.ADD));
		return list;
	}

	public void adjustFavoriteSortOrder(final String id, final int adjustment) {
		view.getStatusText().setText("Sorting favorite");

		AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(Boolean result) {
				if (result != null) {
					refreshData(getClientFactory().getUser().getId(), getClientFactory().getUser().getLanguage(), getClientFactory().getUser().getLocale());
					view.getStatusText().setText("Favorite sorted");
					new Timer() {
						@Override
						public void run() {
							view.getStatusText().setText("");
						}
					}.schedule(1000);
				}
			}
		};

		Map<String, Integer> changes = new HashMap<String, Integer>();
		int size = dictionary.size();
		for (int i = 0; i < size; i++) {
			if (dictionary.get(i).getEntryId().equals(id)) {
				if (adjustment == -1) {
					if (i > 0) {
						changes.put(dictionary.get(i - 1).getEntryId(), i);				
						changes.put(dictionary.get(i).getEntryId(), i - 1);				
					}
				} else if (adjustment == 1) {
					if (i < size - 1) {
						changes.put(dictionary.get(i).getEntryId(), i + 1);				
						changes.put(dictionary.get(i + 1).getEntryId(), i);				
						i++;
					}
				}
			} else if (dictionary.get(i).getSortOrder() != i) {
				changes.put(dictionary.get(i).getEntryId(), i);				
			}
		}
		if (serverIsReachable()) {
			dictionarySvc.setFavoritesSortOrder(changes, callback);
		}
	}
	
	public void deleteFavorite(final String id) {
		view.getStatusText().setText("Deleting favorite");

		AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(Boolean result) {
				if (result != null) {
					refreshData(getClientFactory().getUser().getId(), getClientFactory().getUser().getLanguage(), getClientFactory().getUser().getLocale());
					view.getStatusText().setText("Favorite deleted");
					new Timer() {
						@Override
						public void run() {
							view.getStatusText().setText("");
						}
					}.schedule(1000);
				}
			}
		};

		if (serverIsReachable()) {
			dictionarySvc.deleteFavorite(id, callback);
		}
	}

}
