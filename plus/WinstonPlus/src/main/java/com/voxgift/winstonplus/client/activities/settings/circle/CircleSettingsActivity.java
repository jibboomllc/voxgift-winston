/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.circle;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedEvent;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.DecoratedActivity;
import com.voxgift.winstonplus.client.Item;
import com.voxgift.winstonplus.client.activities.UIEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.UIEntrySelectedEvent.UIEntry;
import com.voxgift.winstonplus.shared.AppConstants;
import com.voxgift.winstonplus.data.shared.model.Circle;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserServiceAsync;

/**
 * @author Tom Giesberg
 * 
 */
public class CircleSettingsActivity extends DecoratedActivity {

	private CircleSettingsView view;

	private int oldIndex;
	private List<Item> items;

	private RpcUserServiceAsync userSvc = GWT.create(RpcUserService.class);

	public CircleSettingsActivity(ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getCircleSettingsView());

		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) userSvc, appConstants.webAppUrl(), "rpc/user");

		view = clientFactory.getCircleSettingsView();
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		getClientFactory().getFrameView().showBackbutton(!MGWT.getOsDetection().isAndroid());
		getClientFactory().getFrameView().getBackbuttonText().setText("Settings");
		getClientFactory().getFrameView().getTitle().setText("Circle Settings");
		panel.setWidget(view);

		refreshData(getClientFactory().getUser().getId());
	}

	@Override
	public void onDataReady() {
		super.onDataReady();

		view.renderItems(items);

		addHandlerRegistration(view.getList().addCellSelectedHandler(new CellSelectedHandler() {

			@Override
			public void onCellSelected(CellSelectedEvent event) {
				int index = event.getIndex();

				view.setSelectedIndex(oldIndex, false);
				view.setSelectedIndex(index, true);
				oldIndex = index;

				if (index > items.size() - 2) { // -1 for zero-based, -1 for "new"
					CircleSettingsEntrySelectedEvent.fire(getEventBus(), null);
				} else {
					CircleSettingsEntrySelectedEvent.fire(getEventBus(), items.get(index).getEntry());
				}
			}
		}));

		addHandlerRegistration(getClientFactory().getFrameView().getBackbuttonTarget().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				UIEntrySelectedEvent.fire(getEventBus(), UIEntry.SETTINGS); 
			}
		}));
	}

	private void refreshData(final String userId) {
		final AsyncCallback<List<Circle>> callback = new AsyncCallback<List<Circle>>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(List<Circle> result) {
				if (result == null) {
					setErrorText("User circle details not found.");
				} else {
					items = createItems(result);
					onDataReady();
				}
			}
		};

		if (serverIsReachable()) {
			userSvc.getUsersCircles(userId, callback);
		}
	}

	/**
	 * @return
	 */
	private List<Item> createItems(List<Circle> usersCircles) {
		ArrayList<Item> list = new ArrayList<Item>();

		for (Circle circle : usersCircles) {
			list.add(new Item(circle.getName(), circle.getId()));
		}
		list.add(new Item("(Create new circle)", null));
		return list;
	}
}
