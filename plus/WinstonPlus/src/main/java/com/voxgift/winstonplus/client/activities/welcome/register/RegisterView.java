/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.register;

import com.google.gwt.user.client.ui.HasText;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.voxgift.winstonplus.client.activities.welcome.LoginBaseView;

/**
 * @author Tom Giesberg
 * 
 */
public interface RegisterView extends LoginBaseView {

	public HasText getPassword();

	public HasText getPasswordRepeated();

	public HasText getEmailAddress();

	public HasTapHandlers getGoButton();
}
