/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.resetpassword;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class ResetPasswordPlace extends Place {
	public static class ResetPasswordPlaceTokenizer implements PlaceTokenizer<ResetPasswordPlace> {

		@Override
		public ResetPasswordPlace getPlace(String token) {
			return new ResetPasswordPlace();
		}

		@Override
		public String getToken(ResetPasswordPlace place) {
			return "";
		}

	}
}
