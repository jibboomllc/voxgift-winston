package com.voxgift.winstonplus.client.widget.actionbar;

import com.googlecode.mgwt.ui.client.MGWTStyle;
import com.voxgift.winstonplus.client.css.ActionBarCss;
import com.voxgift.winstonplus.client.css.CssBundle;
import com.voxgift.winstonplus.client.theme.ExtendedClientBundle;

public class FavoritesAddButton extends ActionBarButton {

	public FavoritesAddButton() {
		this(CssBundle.INSTANCE.actionBarCss(), "Favorites");
	}

	public FavoritesAddButton(String text) {
		this(CssBundle.INSTANCE.actionBarCss(), text);
	}

	public FavoritesAddButton(ActionBarCss css, String text) {
		super(css, ((ExtendedClientBundle) MGWTStyle.getTheme().getMGWTClientBundle()).tabBarStarImage());
		setText(text);
	}
}