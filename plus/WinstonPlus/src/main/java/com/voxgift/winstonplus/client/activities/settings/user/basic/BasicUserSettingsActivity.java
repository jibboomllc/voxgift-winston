/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user.basic;

import java.util.ArrayList;
import java.util.List;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.MGWT;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.DecoratedActivity;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent.SettingsEntry;
import com.voxgift.winstonplus.shared.AppConstants;
import com.voxgift.winstonplus.data.shared.LanguageLocale;
import com.voxgift.winstonplus.data.shared.model.UserDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserServiceAsync;

/**
 * @author Tom Giesberg
 * 
 */
public class BasicUserSettingsActivity extends DecoratedActivity {

	private BasicUserSettingsView view;

	private RpcUserServiceAsync userSvc = GWT.create(RpcUserService.class);

	private List<LanguageLocale> languageLocales;

	public BasicUserSettingsActivity(ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getBasicUserSettingsView());
		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) userSvc, appConstants.webAppUrl(), "rpc/user");

		languageLocales = new ArrayList<LanguageLocale> ();
		languageLocales.add(new LanguageLocale("en", "US", "English (American)"));
		//languageLocales.add(new LanguageLocale("en", "GB", "English (British)"));
		languageLocales.add(new LanguageLocale("fr", "FR", "Française"));
		languageLocales.add(new LanguageLocale("de", "DE", "Deutsch"));
		languageLocales.add(new LanguageLocale("es", "US", "Español"));
		//languageLocales.add(new LanguageLocale("zh", "CN", "中国的"));
		//languageLocales.add(new LanguageLocale("ja", "JP", "日本語"));
		//languageLocales.add(new LanguageLocale("ko", "KR", "한국의"));

		view = clientFactory.getBasicUserSettingsView();
		for (LanguageLocale ll : languageLocales) {
			view.addOutputLanguageItem(ll.getLabel());
		}
		view.addGenderItem("Male");
		view.addGenderItem("Female");
		
		getClientFactory().getFrameView().showBackbutton(!MGWT.getOsDetection().isAndroid());
		getClientFactory().getFrameView().getBackbuttonText().setText("User");
		getClientFactory().getFrameView().getTitle().setText("Basic User Settings");
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		panel.setWidget(view);

		refreshData();
	}

	@Override
	public void onDataReady() {
		super.onDataReady();

		addHandlerRegistration(view.getUpdateButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				if (view.getEmailAddress().getText().length() < 5) {
					view.getStatusText().setText("Please enter a valid email address");
				} else if ((!view.getPassword().getText().isEmpty() ||
						!view.getPasswordRepeated().getText().isEmpty()) &&
						!view.getPassword().getText().equals(view.getPasswordRepeated().getText())) {
					view.getStatusText().setText("New passwords do not match");
				} else {
					updateUser(getClientFactory().getUser().getId()
							, view.getEmailAddress().getText().toLowerCase()
							, view.getPassword().getText()
							, view.getFirstName().getText()
							, view.getLastName().getText()
							, languageLocales.get(view.getSelectedOutputLanguageIndex()).getLanguage()
							, languageLocales.get(view.getSelectedOutputLanguageIndex()).getLocale()
							, (view.getSelectedGenderIndex() == 0 ? "m" : "f")
							);
				}
			}
		}));

		addHandlerRegistration(getClientFactory().getFrameView().getBackbuttonTarget().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				SettingsEntrySelectedEvent.fire(getEventBus(), SettingsEntry.USER); 
			}
		}));
}

	private void refreshData() {

		AsyncCallback<UserDetails> callback = new AsyncCallback<UserDetails>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(UserDetails result) {
				if (result == null) {
					view.getStatusText().setText("No data returned for user");
				} else {
					view.getEmailAddress().setText(result.getPrimaryEmail());					
					view.getFirstName().setText(result.getFirstName());					
					view.getLastName().setText(result.getLastName());
					if (result.getGender().equalsIgnoreCase("m")) {
						view.setSelectedGenderIndex(0, true);
					} else {
						view.setSelectedGenderIndex(1, true);
					}
					// TODO Fragile logic -- should use built-in Java logic
					boolean matched = false;
					for (int i = 0; i < languageLocales.size(); i++) {
						if (result.getLanguage().equalsIgnoreCase(languageLocales.get(i).getLanguage())
								&& (result.getLocale().equalsIgnoreCase(languageLocales.get(i).getLocale())
										|| !matched)
								) {
							matched = true;
							view.setSelectedOutputLanguageIndex(i, true);
						}
					}
					if (!matched) {
						view.setSelectedOutputLanguageIndex(0, true);
					}
				}
				onDataReady();
			}
		};

		if (serverIsReachable()) {
			userSvc.getUser(getClientFactory().getSessionId(), callback);
		}
	}

	public void updateUser(final String id, final String primaryEmail, final String password
			, final String firstName, final String lastName
			, final String language, final String locale, final String gender) {

		if (!primaryEmail.isEmpty()) {
			view.getStatusText().setText("Updating");

			final AsyncCallback<Boolean> callback2 = new AsyncCallback<Boolean>() {
				public void onFailure(Throwable caught) {
					checkServerCallIssue(caught);
			    }

				public void onSuccess(Boolean result) {
				}
			};

			AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
				public void onFailure(Throwable caught) {
					checkServerCallIssue(caught);
				}

				public void onSuccess(Boolean result) {
					if (result) {
						view.getStatusText().setText("User updated");
						UserDetails user = getClientFactory().getUser();
						user.setFirstName(firstName);
						user.setLastName(lastName);
						user.setLanguage(language);
						user.setLocale(locale);
						user.setGender(gender);

						if (serverIsReachable()) {
							userSvc.expireOtherUserSessions(id, getClientFactory().getSessionId(), callback2);
						}
					} else {
						view.getStatusText().setText("User update error");
					}
					new Timer() {
						@Override
						public void run() {
							view.getStatusText().setText("");
						}
					}.schedule(1000);

				}
			};

			if (serverIsReachable()) {
				userSvc.updateUser(id, primaryEmail, password, firstName, lastName, language, locale, gender, callback);
			}
		}
	}
}
