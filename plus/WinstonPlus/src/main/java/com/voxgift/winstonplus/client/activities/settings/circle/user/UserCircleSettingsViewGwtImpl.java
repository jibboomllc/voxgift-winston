/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.circle.user;

import java.util.List;

import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.dialog.ConfirmDialog.ConfirmCallback;
import com.googlecode.mgwt.ui.client.dialog.Dialogs;
import com.googlecode.mgwt.ui.client.dialog.Dialogs.OptionCallback;
import com.googlecode.mgwt.ui.client.dialog.Dialogs.OptionsDialogEntry;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.googlecode.mgwt.ui.client.widget.FormListEntry;
import com.googlecode.mgwt.ui.client.widget.LayoutPanel;
import com.googlecode.mgwt.ui.client.widget.MListBox;
import com.googlecode.mgwt.ui.client.widget.MTextBox;
import com.googlecode.mgwt.ui.client.widget.RoundPanel;
import com.googlecode.mgwt.ui.client.widget.ScrollPanel;
import com.googlecode.mgwt.ui.client.widget.WidgetList;
import com.voxgift.winstonplus.client.DecoratedViewGwtImpl;

/**
 * @author Tom Giesberg
 * 
 */
public class UserCircleSettingsViewGwtImpl extends DecoratedViewGwtImpl implements UserCircleSettingsView {

	private boolean isEditable;

	private RoundPanel lookupPanel;
	private MTextBox lookupEditor;
	private Button lookupButton;
	private Label emailAddressStatic;
	private MTextBox emailAddressEditor;
	private Label firstNameStatic;
	private MTextBox firstNameEditor;
	private Label lastNameStatic;
	private MTextBox lastNameEditor;
	private MListBox role;
	private Label userStatus;
	private Button inviteButton;
	private Button removeButton;

	public UserCircleSettingsViewGwtImpl() {
		isEditable = false;

		ScrollPanel scrollPanel = new ScrollPanel();
		main.add(scrollPanel);

		LayoutPanel container = new LayoutPanel();

		container.add(statusText);
		container.add(errorText);

		lookupPanel = new RoundPanel();
		lookupPanel.addStyleName("search_panel");
		lookupEditor = new MTextBox();
		lookupEditor.addStyleName("text");
		lookupEditor.setPlaceHolder("Search contacts");
		lookupPanel.add(lookupEditor);
		lookupButton = new Button();
		lookupButton.addStyleName("button");
		lookupButton.setText("Lookup");
		lookupPanel.add(lookupButton);
		container.add(lookupPanel);

		WidgetList widgetList = new WidgetList();
		widgetList.setRound(true);

		LayoutPanel emailAddressPanel = new LayoutPanel();
		emailAddressStatic = new Label();
		emailAddressPanel.add(emailAddressStatic);
		emailAddressEditor = new MTextBox();
		emailAddressEditor.setPlaceHolder("Required");
		emailAddressPanel.add(emailAddressEditor);
		widgetList.add(new FormListEntry("Email address", emailAddressPanel));

		LayoutPanel firstNamePanel = new LayoutPanel();
		firstNameStatic = new Label();
		firstNamePanel.add(firstNameStatic);
		firstNameEditor = new MTextBox();
		firstNameEditor.setPlaceHolder("Optional");
		firstNamePanel.add(firstNameEditor);
		widgetList.add(new FormListEntry("First name", firstNamePanel));

		LayoutPanel lastNamePanel = new LayoutPanel();
		lastNameStatic = new Label();
		lastNamePanel.add(lastNameStatic);
		lastNameEditor = new MTextBox();
		lastNameEditor.setPlaceHolder("Optional");
		lastNamePanel.add(lastNameEditor);
		widgetList.add(new FormListEntry("Last name", lastNamePanel));

		role = new MListBox();
		LayoutPanel indicatorContainer = new LayoutPanel();
		indicatorContainer.setHorizontal(true);
		indicatorContainer.add(role);
		LayoutPanel indicator = new LayoutPanel();
		indicator.setStyleName("sub-menu-indicator");
		indicatorContainer.add(indicator);
		widgetList.add(new FormListEntry("Role", indicatorContainer));

		LayoutPanel statusPanel = new LayoutPanel();
		userStatus = new Label();
		statusPanel.add(userStatus);
		widgetList.add(new FormListEntry("Status", statusPanel));
		container.add(widgetList);

		inviteButton = new Button();
		inviteButton.setText("Send invitation");
		container.add(inviteButton);

		removeButton = new Button();
		removeButton.setText("Remove from circle");
		container.add(removeButton);

		scrollPanel.setWidget(container);
		
		// workaround for android formfields jumping around when using
	    // -webkit-transform
	    scrollPanel.setUsePos(MGWT.getOsDetection().isAndroid());
	}

	@Override
	public HasText getLookupText() {
		return lookupEditor;
	}
	
	@Override
	public HasText getEmailAddress() {
		if (isEditable) {
			return emailAddressEditor;
		}
		return emailAddressStatic;
	}

	@Override
	public HasChangeHandlers getEmailAddressEditor() {
		if (isEditable) {
			return emailAddressEditor;
		}
		return null;
	}

	@Override
	public HasText getFirstName() {
		if (isEditable) {
			return firstNameEditor;
		}
		return firstNameStatic;
	}

	@Override
	public HasChangeHandlers getFirstNameEditor() {
		if (isEditable) {
			return firstNameEditor;
		}
		return null;
	}

	@Override
	public HasChangeHandlers getLastNameEditor() {
		if (isEditable) {
			return lastNameEditor;
		}
		return null;
	}

	@Override
	public HasText getLastName() {
		if (isEditable) {
			return lastNameEditor;
		}
		return lastNameStatic;
	}


	@Override
	public void addRoleItem(String item) {
		role.addItem(item);
	}

	@Override
	public void clearRoleItems() {
		role.clear();
	}

	@Override
	public int getSelectedRoleIndex() {
		return role.getSelectedIndex();
	}

	@Override
	public void setSelectedRoleIndex(int index, boolean selected) {
		role.setItemSelected(index, selected);
	}

	@Override
	public HasChangeHandlers getRoleList() {
		return role;
	}

	@Override
	public HasText getUserStatus() {
		return userStatus;
	}

	@Override
	public HasTapHandlers getInviteButton() {
		return inviteButton;
	}

	@Override
	public HasTapHandlers getRemoveButton() {
		return removeButton;
	}

	@Override
	public HasTapHandlers getLookupButton() {
		return lookupButton;
	}

	@Override
	public void showLookupResults(List<OptionsDialogEntry> optionText, OptionCallback callback) {
		Dialogs.options(optionText, callback, main);

	}

	@Override
	public void enableEditing(boolean isEditable) {
		this.isEditable = isEditable;

		emailAddressEditor.setVisible(isEditable);
		emailAddressStatic.setVisible(!isEditable);

		firstNameEditor.setVisible(isEditable);
		firstNameStatic.setVisible(!isEditable);

		lastNameEditor.setVisible(isEditable);
		lastNameStatic.setVisible(!isEditable);
		
		lookupPanel.setVisible(isEditable);
		removeButton.setVisible(!isEditable);
	}

	@Override
	public void confirmRemove(String title, String text,
			ConfirmCallback callback) {
		Dialogs.confirm(title, text, callback);
	}
}
