/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.legal;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class LegalDocumentsPlace extends Place {
	public static class LegalDocumentsPlaceTokenizer implements PlaceTokenizer<LegalDocumentsPlace> {

		@Override
		public LegalDocumentsPlace getPlace(String token) {
			return new LegalDocumentsPlace();
		}

		@Override
		public String getToken(LegalDocumentsPlace place) {
			return "";
		}

	}
}
