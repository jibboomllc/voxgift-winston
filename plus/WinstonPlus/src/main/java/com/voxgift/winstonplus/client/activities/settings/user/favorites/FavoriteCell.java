/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user.favorites;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.googlecode.mgwt.ui.client.widget.celllist.Cell;
import com.voxgift.winstonplus.client.Item;

/**
 * @author Tom Giesberg
 *
 */
public class FavoriteCell implements Cell<Item> {

	private static Template TEMPLATE = GWT.create(Template.class);

	public interface Template extends SafeHtmlTemplates {
		String htmlString =
				"<div class='sorting_cell'>"
						+ "<div class='text'>{0}</div>"
						+ "<div class='controls'>"
							+ "<div class='up' id='up'>[ ^ ]</div>"
							+ "<div class='down' id='down'>[ v ]</div>"
							+ "<div class='blank'></div>"
							+ "<div class='delete' id='delete'>[ X ]</div>"
						+ "</div>"
						+ "</div>";

		@SafeHtmlTemplates.Template(htmlString)
		SafeHtml content(String message);
	}

	private static TemplateFirst TEMPLATE_FIRST = GWT.create(TemplateFirst.class);
	public interface TemplateFirst extends SafeHtmlTemplates {
		String htmlString =
				"<div class='sorting_cell'>"
						+ "<div class='text'>{0}</div>"
						+ "<div class='controls'>"
							+ "<div class='blank'></div>"
							+ "<div class='down' id='down'>[ v ]</div>"
							+ "<div class='blank'></div>"
							+ "<div class='delete' id='delete'>[ X ]</div>"
						+ "</div>"
						+ "</div>";

		@SafeHtmlTemplates.Template(htmlString)
		SafeHtml content(String message);
	}

	private static TemplateOnly TEMPLATE_ONLY = GWT.create(TemplateOnly.class);
	public interface TemplateOnly extends SafeHtmlTemplates {
		String htmlString =
				"<div class='sorting_cell'>"
						+ "<div class='text'>{0}</div>"
						+ "<div class='controls'>"
							+ "<div class='blank'></div>"
							+ "<div class='blank'></div>"
							+ "<div class='blank'></div>"
							+ "<div class='delete' id='delete'>[ X ]</div>"
						+ "</div>"
						+ "</div>";

		@SafeHtmlTemplates.Template(htmlString)
		SafeHtml content(String message);
	}

	private static TemplateLast TEMPLATE_LAST = GWT.create(TemplateLast.class);
	public interface TemplateLast extends SafeHtmlTemplates {
		String htmlString =
				"<div class='sorting_cell'>"
						+ "<div class='text'>{0}</div>"
						+ "<div class='controls'>"
							+ "<div class='up' id='up'>[ ^ ]</div>"
							+ "<div class='blank'></div>"
							+ "<div class='blank'></div>"
							+ "<div class='delete' id='delete'>[ X ]</div>"
						+ "</div>"
						+ "</div>";

		@SafeHtmlTemplates.Template(htmlString)
		SafeHtml content(String message);
	}

	private static TemplateAdd TEMPLATE_ADD = GWT.create(TemplateAdd.class);
	public interface TemplateAdd extends SafeHtmlTemplates {
		String htmlString =
				"<div>{0}</div>"
						+ "<div><button id='add'>[ + ]</button>";

		@SafeHtmlTemplates.Template(htmlString)
		SafeHtml content(String message);
	}

	@Override
	public void render(SafeHtmlBuilder safeHtmlBuilder, final Item model) {
		switch(model.getAttribute()) {
		case ADD:
			safeHtmlBuilder.append(TEMPLATE_ADD.content(model.getDisplayString()));
			break;
		case FIRST:
			safeHtmlBuilder.append(TEMPLATE_FIRST.content(model.getDisplayString()));
			break;
		case LAST:
			safeHtmlBuilder.append(TEMPLATE_LAST.content(model.getDisplayString()));
			break;
		case ONLY:
			safeHtmlBuilder.append(TEMPLATE_ONLY.content(model.getDisplayString()));
			break;
		case MIDDLE:
		default:
			safeHtmlBuilder.append(TEMPLATE.content(model.getDisplayString()));
			break;
		}
	}

	@Override
	public boolean canBeSelected(Item model) {
		return false;
	}

}
