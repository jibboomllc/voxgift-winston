package com.voxgift.winstonplus.client.activities.settings;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class SettingsEntrySelectedEvent extends Event<SettingsEntrySelectedEvent.Handler> {

	public enum SettingsEntry {
		USER, CIRCLE, LEGAL
	}

	public interface Handler {
		void onAnimationSelected(SettingsEntrySelectedEvent event);
	}

	private static final Type<SettingsEntrySelectedEvent.Handler> TYPE = new Type<SettingsEntrySelectedEvent.Handler>();
	private final SettingsEntry entry;

	public static void fire(EventBus eventBus, SettingsEntry entry) {
		eventBus.fireEvent(new SettingsEntrySelectedEvent(entry));
	}

	public static HandlerRegistration register(EventBus eventBus, Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	@Override
	public com.google.web.bindery.event.shared.Event.Type<Handler> getAssociatedType() {
		return TYPE;
	}

	protected SettingsEntrySelectedEvent(SettingsEntry entry) {
		this.entry = entry;

	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onAnimationSelected(this);

	}

	public static Type<SettingsEntrySelectedEvent.Handler> getType() {
		return TYPE;
	}

	public SettingsEntry getEntry() {
		return entry;
	}
}
