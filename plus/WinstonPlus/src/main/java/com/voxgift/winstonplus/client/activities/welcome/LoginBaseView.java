/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome;

import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.IsWidget;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.dialog.Dialogs.AlertCallback;

/**
 * @author Tom Giesberg
 * 
 */
public interface LoginBaseView extends IsWidget {

	public HasVisibility getWorking();

	public HasHTML getLoginIcon();
	
	public HasText getStatusText();

	public HasText getErrorText();

	public HasTapHandlers getCancelButton();

	public void alertTempPasswordEmailed(String title, String text, AlertCallback callback);

	public void alertVerificationLinkEmailed(String title, String text, AlertCallback callback);

	public void alertUsingTempPassword(String title, String text, AlertCallback callback);

	public void alertServerCommunicationsProblem(String title, String text, AlertCallback callback);
}
