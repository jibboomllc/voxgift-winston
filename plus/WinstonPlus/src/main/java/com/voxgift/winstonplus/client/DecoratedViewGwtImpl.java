package com.voxgift.winstonplus.client;

import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.ui.client.dialog.Dialogs;
import com.googlecode.mgwt.ui.client.dialog.Dialogs.AlertCallback;
import com.googlecode.mgwt.ui.client.widget.LayoutPanel;

public abstract class DecoratedViewGwtImpl implements DecoratedView {

	protected LayoutPanel main;
	protected Label statusText;
	protected Label errorText;
 	
	public DecoratedViewGwtImpl() {
		main = new LayoutPanel();

		statusText = new Label();
		statusText.addStyleName("status");

		errorText = new Label();
		errorText.addStyleName("error");
	}

	@Override
	public Widget asWidget() {
		return main;
	}

	@Override
	public HasText getStatusText() {
		return statusText;
	}

	@Override
	public HasText getErrorText() {
		return errorText;
	}

	@Override
	public void alertServerCommunicationsProblem(String title, String text, AlertCallback callback) {
		Dialogs.alert(title, text, callback);
	}
}
