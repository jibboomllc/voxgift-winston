package com.voxgift.winstonplus.client.activities.compose;

import java.util.List;

import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.user.client.ui.HasText;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.widget.celllist.HasCellSelectedHandler;
import com.voxgift.winstonplus.client.DecoratedView;
import com.voxgift.winstonplus.data.shared.model.DictionaryDetails;

public interface ComposeView extends DecoratedView{

	public HasText getInputText();

	public HasKeyUpHandlers getCurrentInput();
	
	public HasValueChangeHandlers<String> getInputEditor();
	
	public HasTapHandlers getClearButton();

	public HasTapHandlers getAddFavoriteButton();

	public HasTapHandlers getTalkButton();

	public HasTapHandlers getPostButton();

	public HasTapHandlers getPageButton();
	
	public void renderDictionaries(List<DictionaryDetails> dictionaries);
	
	public HasSelectionHandlers<Integer> getDictionaryCarousel();
	
	public HasCellSelectedHandler getList(int listIndex);
	
	public void setSelectedIndex(int ListIndex, int index, boolean selected);
}
