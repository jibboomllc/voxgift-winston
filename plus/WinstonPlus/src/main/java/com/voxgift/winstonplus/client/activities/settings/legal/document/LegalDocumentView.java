/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.legal.document;

import com.google.gwt.user.client.ui.HasHTML;
import com.voxgift.winstonplus.client.DecoratedView;

/**
 * @author Tom Giesberg
 * 
 */
public interface LegalDocumentView extends DecoratedView {

	public HasHTML getContent();

	public void refresh();
}
