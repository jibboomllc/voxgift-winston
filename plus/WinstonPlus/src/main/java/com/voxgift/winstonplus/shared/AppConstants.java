package com.voxgift.winstonplus.shared;

import com.google.gwt.i18n.client.Constants;

public interface AppConstants extends Constants {
	@DefaultStringValue("https://www.voxgift.com/c/WinstonPlus-1.0.2/WinstonPlus/")
	String webAppUrl();
	
	@DefaultStringValue("https://www.voxgift.com/")
	String resourcesUrl();
	
	@DefaultStringValue("https://www.voxgift.com/c/WinstonPlus-1.0.2/")
	@Description("Where to get custom icons etc.")
	String customResourcesUrl();

	@DefaultStringValue("https://www.voxgift.com/confirmation?id=")
	String confirmationUrl();

	@DefaultStringValue("https://www.voxgift.com/audio?file=")
	String audioUrl();

	@DefaultStringValue("https://www.voxgift.com/imageupload")
	String imageUploadUrl();
	
	@DefaultStringValue("WinstonPlus")
	String title();

	@DefaultStringValue("1.0.2")
	String buildVersion();
}
