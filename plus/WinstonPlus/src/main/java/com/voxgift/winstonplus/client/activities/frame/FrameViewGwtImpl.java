/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.frame;

import java.util.Iterator;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.widget.HeaderButton;
import com.googlecode.mgwt.ui.client.widget.HeaderPanel;
import com.googlecode.mgwt.ui.client.widget.LayoutPanel;
import com.googlecode.mgwt.ui.client.widget.touch.TouchPanel;

/**
 * @author Tom Giesberg
 * 
 */
public class FrameViewGwtImpl implements FrameView {

	protected LayoutPanel main;
	protected HeaderPanel headerPanel;
	protected LayoutPanel container;
	protected HTML title;
	protected HTML sponsorIcon;
	protected TouchPanel sponsorButton;
	protected HTML circleIcon;
	protected TouchPanel circleButton;

	private HeaderButton headerBackButton;

	public FrameViewGwtImpl() {
		main = new LayoutPanel();
		main.getElement().setId("frame.main");
		main.addStyleName("frame_panel");

		headerPanel = new HeaderPanel();
		headerPanel.getElement().setId("frame.header");
		sponsorIcon = new HTML("");
		sponsorButton = new TouchPanel();
		sponsorButton.add(sponsorIcon);
		headerBackButton = new HeaderButton();
		headerBackButton.setBackButton(true);
        showBackbutton(false);

		title = new HTML("");
		headerPanel.setCenterWidget(title);

		circleIcon = new HTML("");
		circleButton = new TouchPanel();
		circleButton.add(circleIcon);
		headerPanel.setRightWidget(circleButton);

		main.add(headerPanel);

		container = new LayoutPanel();
		container.addStyleName("body_panel");
		main.add(container);
	}

	@Override
	public Widget asWidget() {
		return main;
	}

	@Override
	public HasText getTitle() {
		return title;
	}

	@Override
	public HasHTML getSponsorIcon() {
		return sponsorIcon;
	}

	@Override
	public HasTapHandlers getSponsorButton() {
		return sponsorButton;
	}

	@Override
	public HasHTML getCircleIcon() {
		return circleIcon;
	}

	@Override
	public HasTapHandlers getCircleButton() {
		return circleButton;
	}

	@Override
	public HasText getBackbuttonText() {
		return headerBackButton;
	}

	@Override
	public HasTapHandlers getBackbuttonTarget() {
		return headerBackButton;
	}

	@Override
	public void showBackbutton(boolean show) {
		if(show) {
			headerPanel.setLeftWidget(headerBackButton);
		} else {
			headerPanel.setLeftWidget(sponsorButton);
		}
	}

	///////////////
	
	@Override
	public void add(Widget w) {
		container.add(w);
	}

	@Override
	public void clear() {
		container.clear();
	}

	@Override
	public Iterator<Widget> iterator() {
		return container.iterator();
	}

	@Override
	public boolean remove(Widget w) {
		return container.remove(w);
	}

	@Override
	public void add(IsWidget w) {
		container.add(w.asWidget());
	}

	@Override
	public boolean remove(IsWidget w) {
		return container.remove(w.asWidget());
	}

}
