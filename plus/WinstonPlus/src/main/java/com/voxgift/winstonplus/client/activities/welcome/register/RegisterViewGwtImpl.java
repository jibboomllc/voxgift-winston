/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.register;

import com.google.gwt.user.client.ui.HasText;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.googlecode.mgwt.ui.client.widget.FormListEntry;
import com.googlecode.mgwt.ui.client.widget.MEmailTextBox;
import com.googlecode.mgwt.ui.client.widget.MPasswordTextBox;
import com.voxgift.winstonplus.client.activities.welcome.LoginBaseViewGwtImpl;
import com.voxgift.winstonplus.client.css.CssBundle;

/**
 * @author Tom Giesberg
 * 
 */
public class RegisterViewGwtImpl extends LoginBaseViewGwtImpl implements RegisterView {
	
	private MEmailTextBox emailAddress;
	private MPasswordTextBox password;
	private MPasswordTextBox passwordRepeated;
	private Button goButton;
	
	public RegisterViewGwtImpl() {
		
		container.add(controls);
		container.add(inner_container);

		emailAddress = new MEmailTextBox(CssBundle.INSTANCE.loginInputCss());
	    controls.add(new FormListEntry(CssBundle.INSTANCE.loginListCss(), "Email address", emailAddress));

		password = new MPasswordTextBox(CssBundle.INSTANCE.loginInputCss());
		controls.add(new FormListEntry(CssBundle.INSTANCE.loginListCss(), "Password", password));

		passwordRepeated = new MPasswordTextBox(CssBundle.INSTANCE.loginInputCss());
		controls.add(new FormListEntry(CssBundle.INSTANCE.loginListCss(), "Repeat password", passwordRepeated));

		goButton = new Button();
		goButton.setText("Go");
		inner_container.add(goButton);
		
		cancelButton = new Button();
		cancelButton.setText("Cancel");
		inner_container.add(cancelButton);
	}


	@Override
	public HasText getEmailAddress() {
		return emailAddress;
	}
	
	@Override
	public HasText getPassword() {
		return password;
	}

	@Override
	public HasText getPasswordRepeated() {
		return passwordRepeated;
	}

	@Override
	public HasTapHandlers getGoButton() {
		return goButton;
	}
}
