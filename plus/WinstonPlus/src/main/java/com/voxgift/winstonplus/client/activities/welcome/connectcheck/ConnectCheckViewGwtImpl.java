/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.connectcheck;

import com.google.gwt.user.client.ui.HasVisibility;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.googlecode.mgwt.ui.client.widget.ProgressIndicator;
import com.voxgift.winstonplus.client.activities.welcome.LoginBaseViewGwtImpl;

/**
 * @author Tom Giesberg
 * 
 */
public class ConnectCheckViewGwtImpl extends LoginBaseViewGwtImpl implements ConnectCheckView {

	private Button tryAgainButton;
	private ProgressIndicator progressIndicator;
	
	public ConnectCheckViewGwtImpl() {
		super();

		container.add(inner_container);

		tryAgainButton = new Button();
		tryAgainButton.setText("Try connection again");
		inner_container.add(tryAgainButton);

		progressIndicator = new ProgressIndicator();
		progressIndicator.getElement().setAttribute("style", "margin:auto; margin-top: 50px");
		inner_container.add(progressIndicator);

	}

	@Override
	public HasTapHandlers getTryAgainButton() {
		return tryAgainButton;
	}

	@Override
	public HasVisibility getTryAgainButtonVisibility() {
		return tryAgainButton;
	}


	@Override
	public HasVisibility getProgressIndicatorVisibility() {
		return progressIndicator;
	}
}
