/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.legal;

/**
 * @author Tom Giesberg
 * 
 */
public class Item {
	private String displayString;
	private String id;

	public Item(String displayString, String id) {
		this.displayString = displayString;
		this.id = id;
	}

	/**
	 * @return the displayString
	 */
	public String getDisplayString() {
		return displayString;
	}

	public String getId() {
		return id;
	}
}
