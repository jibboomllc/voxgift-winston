package com.voxgift.winstonplus.client;

import com.google.gwt.place.shared.Place;
import com.google.gwt.user.client.History;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

import com.googlecode.mgwt.dom.client.event.mouse.HandlerRegistrationCollection;
import com.googlecode.mgwt.mvp.client.history.HistoryHandler;
import com.googlecode.mgwt.mvp.client.history.HistoryObserver;

import com.voxgift.winstonplus.client.activities.UIEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.UIEntrySelectedEvent.UIEntry;
import com.voxgift.winstonplus.client.activities.compose.ComposePlace;
import com.voxgift.winstonplus.client.activities.messagelist.MessageListPlace;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent.SettingsEntry;
import com.voxgift.winstonplus.client.activities.settings.SettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsPlace;
import com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentPlace;
import com.voxgift.winstonplus.client.activities.settings.user.UserSettingsEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.settings.user.UserSettingsEntrySelectedEvent.UserSettingsEntry;
import com.voxgift.winstonplus.client.activities.settings.user.UserSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsPlace;
import com.voxgift.winstonplus.client.activities.welcome.help.HelpEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace;
import com.voxgift.winstonplus.client.event.ActionEvent;
import com.voxgift.winstonplus.client.event.ActionNames;
import com.voxgift.winstonplus.client.places.HomePlace;

public class AppHistoryObserver implements HistoryObserver {

	boolean usingSinglePane;

	public AppHistoryObserver(boolean usingSinglePane) {
		this.usingSinglePane = usingSinglePane;
	}

	@Override
	public void onPlaceChange(Place place, HistoryHandler handler) {

	}

	@Override
	public void onHistoryChanged(Place place, HistoryHandler handler) {

	}

	@Override
	public void onAppStarted(Place place, HistoryHandler historyHandler) {
		if (this.usingSinglePane) {
			onSinglePaneNav(place, historyHandler);
		} else {
			// tablet
			onDoublePaneNav(place, historyHandler);

		}

	}

	@Override
	public HandlerRegistration bind(EventBus eventBus, final HistoryHandler historyHandler) {

		HandlerRegistration register8 = HelpEntrySelectedEvent.register(eventBus, new HelpEntrySelectedEvent.Handler() {

			@Override
			public void onAnimationSelected(HelpEntrySelectedEvent event) {

				Place place = new HelpPlace(event.getTopicId());

				if (usingSinglePane) {
					historyHandler.goTo(place);
				} else {
					historyHandler.replaceCurrentPlace(place);
					historyHandler.goTo(place, true);
				}

			}
		});

		HandlerRegistration register7 = LegalDocumentsEntrySelectedEvent.register(eventBus, new LegalDocumentsEntrySelectedEvent.Handler() {

			@Override
			public void onAnimationSelected(LegalDocumentsEntrySelectedEvent event) {

				Place place = new LegalDocumentPlace(event.getTitle(), event.getUrl());

				if (usingSinglePane) {
					historyHandler.goTo(place);
				} else {
					historyHandler.replaceCurrentPlace(place);
					historyHandler.goTo(place, true);
				}

			}
		});

		HandlerRegistration register6 = BasicCircleSettingsEntrySelectedEvent.register(eventBus, new BasicCircleSettingsEntrySelectedEvent.Handler() {

			@Override
			public void onAnimationSelected(BasicCircleSettingsEntrySelectedEvent event) {

				Place place = new UserCircleSettingsPlace(event.getCircleId(), event.getUserId());

				if (usingSinglePane) {
					historyHandler.goTo(place);
				} else {
					historyHandler.replaceCurrentPlace(place);
					historyHandler.goTo(place, true);
				}

			}
		});

		HandlerRegistration register5 = CircleSettingsEntrySelectedEvent.register(eventBus, new CircleSettingsEntrySelectedEvent.Handler() {

			@Override
			public void onAnimationSelected(CircleSettingsEntrySelectedEvent event) {

				String entry = event.getEntry();

				Place place = new BasicCircleSettingsPlace(entry);

				if (usingSinglePane) {
					historyHandler.goTo(place);
				} else {
					historyHandler.replaceCurrentPlace(place);
					historyHandler.goTo(place, true);
				}

			}
		});

		HandlerRegistration register4 = UserSettingsEntrySelectedEvent.register(eventBus, new UserSettingsEntrySelectedEvent.Handler() {

			@Override
			public void onAnimationSelected(UserSettingsEntrySelectedEvent event) {

				UserSettingsEntry entry = event.getEntry();

				Place place = null;

				switch (entry) {
				case BASIC:
					place = new BasicUserSettingsPlace();
					break;
				case IMAGE:
					place = new ImageUserSettingsPlace();
					break;
				case FAVORITES:
					place = new FavoritesUserSettingsPlace();
					break;
				default:
					break;
				}

				if (usingSinglePane) {
					historyHandler.goTo(place);
				} else {
					historyHandler.replaceCurrentPlace(place);
					historyHandler.goTo(place, true);
				}

			}
		});
		
		HandlerRegistration register3 = SettingsEntrySelectedEvent.register(eventBus, new SettingsEntrySelectedEvent.Handler() {

			@Override
			public void onAnimationSelected(SettingsEntrySelectedEvent event) {

				SettingsEntry entry = event.getEntry();

				Place place = null;

				switch (entry) {
				case USER:
					place = new UserSettingsPlace();
					break;
				case CIRCLE:
				    place = new CircleSettingsPlace();
					break;
				case LEGAL:
				    place = new LegalDocumentsPlace();
					break;
				default:
					break;
				}

				if (usingSinglePane) {
					historyHandler.goTo(place);
				} else {
					historyHandler.replaceCurrentPlace(place);
					historyHandler.goTo(place, true);
				}

			}
		});

		HandlerRegistration register2 = UIEntrySelectedEvent.register(eventBus, new UIEntrySelectedEvent.Handler() {

			@Override
			public void onAnimationSelected(UIEntrySelectedEvent event) {

				UIEntry entry = event.getEntry();

				Place place = null;

				switch (entry) {
				case LIST:
					place = new HomePlace();
					break;
				case COMPOSE:
					place = new ComposePlace();
					break;
				case SETTINGS:
					place = new SettingsPlace();
					break;
				default:
					break;
				}

				if (usingSinglePane) {
					historyHandler.goTo(place);
				} else {
					historyHandler.replaceCurrentPlace(place);
					historyHandler.goTo(place, true);
				}

			}
		});

		HandlerRegistration register = ActionEvent.register(eventBus, ActionNames.BACK, new ActionEvent.Handler() {

			@Override
			public void onAction(ActionEvent event) {

				History.back();

			}
		});

		HandlerRegistrationCollection col = new HandlerRegistrationCollection();
		col.addHandlerRegistration(register);
		col.addHandlerRegistration(register2);
		col.addHandlerRegistration(register3);
		col.addHandlerRegistration(register4);
		col.addHandlerRegistration(register5);
		col.addHandlerRegistration(register6);
		col.addHandlerRegistration(register7);
		col.addHandlerRegistration(register8);
		return col;
	}

	private void onSinglePaneNav(Place place, HistoryHandler historyHandler) {

		if (place instanceof ComposePlace || place instanceof SettingsPlace) {
			historyHandler.replaceCurrentPlace(new HomePlace());

			historyHandler.pushPlace(new MessageListPlace());
		}
	}

	private void onDoublePaneNav(Place place, HistoryHandler historyHandler) {

		if (place instanceof ComposePlace) {
			historyHandler.replaceCurrentPlace(new HomePlace());
		} else {
			if (place instanceof SettingsPlace) {
				historyHandler.replaceCurrentPlace(new HomePlace());
			}
		}
	}
}
