/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.legal.document;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class LegalDocumentPlace extends Place {

	private String title;
	private String url;

	public LegalDocumentPlace(String title, String url) {
		this.title = title;
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

	public static class LegalDocumentPlaceTokenizer implements PlaceTokenizer<LegalDocumentPlace> {
	    static final String SEPARATOR = "!";

	    @Override
		public LegalDocumentPlace getPlace(String token) {
	        String bits[] = token.split(SEPARATOR);

	        if (bits.length != 2) {
	          return null;
	        }
			return new LegalDocumentPlace(bits[0], bits[1]);
		}

		@Override
		public String getToken(LegalDocumentPlace place) {
			return place.getTitle() + SEPARATOR + place.getUrl();
		}

	}
}
