package com.voxgift.winstonplus.client.widget.tabbar;

import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.MGWTStyle;
import com.googlecode.mgwt.ui.client.theme.base.TabBarCss;
import com.googlecode.mgwt.ui.client.widget.tabbar.TabBarButton;
import com.voxgift.winstonplus.client.theme.ExtendedClientBundle;

public class ListTabBarButton extends TabBarButton {

	public ListTabBarButton() {
		this(MGWTStyle.getTheme().getMGWTClientBundle().getTabBarCss(), "Type");
	}

	public ListTabBarButton(String text) {
		this(MGWTStyle.getTheme().getMGWTClientBundle().getTabBarCss(), text);
	}

    public ListTabBarButton(TabBarCss css, String text) {
	    super(css, MGWT.getOsDetection().isIOs() || MGWT.getOsDetection().isDesktop() ? 
	    	((ExtendedClientBundle) MGWTStyle.getTheme().getMGWTClientBundle()).tabBarListImage() : null);
			setText(text);
	}
}
