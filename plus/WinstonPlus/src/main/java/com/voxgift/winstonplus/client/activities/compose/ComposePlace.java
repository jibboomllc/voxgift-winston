package com.voxgift.winstonplus.client.activities.compose;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class ComposePlace extends Place {
	public static class ComposePlaceTokenizer implements PlaceTokenizer<ComposePlace> {

		@Override
		public ComposePlace getPlace(String token) {
			return new ComposePlace();
		}

		@Override
		public String getToken(ComposePlace place) {
			return "";
		}

	}
}
