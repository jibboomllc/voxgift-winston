/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.circle.basic;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.dialog.ConfirmDialog.ConfirmCallback;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedEvent;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.DecoratedActivity;
import com.voxgift.winstonplus.client.Item;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent.SettingsEntry;
import com.voxgift.winstonplus.shared.AppConstants;
import com.voxgift.winstonplus.data.shared.model.CircleDetails;
import com.voxgift.winstonplus.data.shared.model.CircleStatus;
import com.voxgift.winstonplus.data.shared.model.CircleUserDetails;
import com.voxgift.winstonplus.data.shared.model.CircleUserRole;
import com.voxgift.winstonplus.data.shared.model.CircleUserStatus;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcCircleService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcCircleServiceAsync;

/**
 * @author Tom Giesberg
 * 
 */
public class BasicCircleSettingsActivity extends DecoratedActivity {

	// private final static Logger LOGGER = Logger.getLogger(BasicCircleSettingsActivity.class.getName()); 

	private BasicCircleSettingsView view;

	private String circleId;

	private CircleDetails circleDetails;

	private int oldIndex;
	private List<Item> items;

	private RpcCircleServiceAsync circleSvc = GWT.create(RpcCircleService.class);

	public BasicCircleSettingsActivity(ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getBasicCircleSettingsView());

		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) circleSvc, appConstants.webAppUrl(), "rpc/circle");

		view = clientFactory.getBasicCircleSettingsView();
	}

	public void setCircleId(String circleId) {
		this.circleId = circleId;
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		getClientFactory().getFrameView().showBackbutton(!MGWT.getOsDetection().isAndroid());
		getClientFactory().getFrameView().getBackbuttonText().setText("Select");
		getClientFactory().getFrameView().getTitle().setText("Basic Circle Settings");
		panel.setWidget(view);

		refreshData(circleId);
	}

	@Override
	public void onDataReady() {
		super.onDataReady();

		getClientFactory().getFrameView().getTitle().setText(circleDetails.getName() + " Settings");

		boolean isEditable = circleDetails.getOwnerId().equals(getClientFactory().getUser().getId());
		view.enableEditing(isEditable);

		items = createItems(circleDetails.getCircleUsers(), isEditable);
		view.renderItems(items);

		view.getName().setText(circleDetails.getName());

		if (isEditable) {
			addHandlerRegistration(view.getNameEditor().addChangeHandler(new ChangeHandler() {
				@Override
				public void onChange(ChangeEvent event) {
					updateCircleName(view.getName().getText());
				}
			}));

			addHandlerRegistration(view.getList().addCellSelectedHandler(new CellSelectedHandler() {

				@Override
				public void onCellSelected(CellSelectedEvent event) {
					int index = event.getIndex();

					view.setSelectedIndex(oldIndex, false);
					view.setSelectedIndex(index, true);
					oldIndex = index;

					if (index > items.size() - 2) {  // -1 for zero-based, -1 for "new"
						viewUser(null);
					} else {
						viewUser(items.get(index).getEntry());
					}
				}
			}));

			addHandlerRegistration(view.getDeleteButton().addTapHandler(new TapHandler() {

				@Override
				public void onTap(TapEvent event) {
					deleteCircle(circleId);
				}
			}));
		}

		addHandlerRegistration(getClientFactory().getFrameView().getBackbuttonTarget().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				SettingsEntrySelectedEvent.fire(getEventBus(), SettingsEntry.CIRCLE); 
			}
		}));
	}

	private void refreshData(final String id) {

		final AsyncCallback<CircleDetails> callbackGetCircle = new AsyncCallback<CircleDetails>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(CircleDetails result) {
				if (result == null) {
					setErrorText("Circle not found");
				} else {
					circleDetails = result;
					onDataReady();
				}
			}
		};
		final AsyncCallback<String> callbackCreateCircle = new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(String result) {
				if (result != null) {
					circleId = result;
					if (serverIsReachable()) {
						circleSvc.getCircle(circleId, callbackGetCircle);
					}
				} else {
					setErrorText("Circle create error");
				}
			}
		};

		if (serverIsReachable()) {
			if (id == null) {
				circleSvc.createCircle(getClientFactory().getUser().getId(), "New Circle"
						, CircleStatus.OPEN, CircleUserRole.FAMILY, callbackCreateCircle);
			} else {
				circleSvc.getCircle(id, callbackGetCircle);
			}
		}
	}

	private void deleteCircle(final String id) {

		if (id == null) {
			SettingsEntrySelectedEvent.fire(getEventBus(), SettingsEntry.CIRCLE); 
		} else {
			view.confirmDelete("Confirm Delete", "Please confirm that you wish to delete this circle.", new ConfirmCallback() {

				@Override
				public void onOk() {
					final AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
						public void onFailure(Throwable caught) {
							checkServerCallIssue(caught);
						}

						public void onSuccess(Boolean result) {
							if (result == null || result == false) {
								setErrorText("Circle not found");
							} else {
								SettingsEntrySelectedEvent.fire(getEventBus(), SettingsEntry.CIRCLE); 
							}
						}
					};
					if (serverIsReachable()) {
						circleSvc.updateCircleStatus(id, CircleStatus.DELETED, callback);
					}
				}

				@Override
				public void onCancel() {
				}
			});

		}
	}

	/**
	 * @return
	 */
	private List<Item> createItems(List<CircleUserDetails> circleUsers, boolean ownerView) {
		ArrayList<Item> list = new ArrayList<Item>();

		for (CircleUserDetails circleUser : circleUsers) {
			if ((ownerView && circleUser.getStatusId() != CircleUserStatus.REMOVED)
					|| circleUser.getStatusId() == CircleUserStatus.ACCEPTED) {
				if ((circleUser.getFirstName() != null && !circleUser.getFirstName().isEmpty())
						|| (circleUser.getLastName() != null && !circleUser.getLastName().isEmpty())) {
					list.add(new Item(circleUser.getFirstName() + " " + circleUser.getLastName(), circleUser.getUserId()));
				} else {
					list.add(new Item(circleUser.getEmail(), circleUser.getUserId()));
				}
			}
		}

		if (ownerView) {
			list.add(new Item("(Add user)", null));
		}

		return list;
	}

	private void updateCircleName(final String name) {
		//					LOGGER.info(event.toDebugString());
		setStatusText("Updating");

		final AsyncCallback<Boolean> callbackUpdateName = new AsyncCallback<Boolean>() {

			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(Boolean result) {
				if (result) {
					circleDetails.setName(name);
					setStatusText("Circle updated", 1000);

					if (getClientFactory().getUser().getUserCircles().containsKey(circleId)) {
						getClientFactory().getUser().getUserCircles().get(circleId).setCircleName(name);
					}

				} else {
					setErrorText("Circle update error");
				}
			}
		};

		if (serverIsReachable()) {
			circleSvc.updateCircleName(circleId, name, callbackUpdateName);
		}
	}

	private void viewUser(final String userId) {
		BasicCircleSettingsEntrySelectedEvent.fire(getEventBus(), circleId, userId);
	}
}
