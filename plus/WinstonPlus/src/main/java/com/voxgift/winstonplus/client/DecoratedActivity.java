package com.voxgift.winstonplus.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.shared.UriUtils;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;
import com.voxgift.winstonplus.client.activities.frame.FrameView;
import com.voxgift.winstonplus.client.activities.welcome.help.HelpEntrySelectedEvent;
import com.voxgift.winstonplus.client.event.ActionEvent;
import com.voxgift.winstonplus.client.event.ActionNames;
import com.voxgift.winstonplus.shared.AppConstants;

public class DecoratedActivity extends MGWTAbstractActivity {

	private static String SCREEN_HELP_TOPIC ="6032cf90-f052-489a-83e1-331bd08e93ef";

	private final ClientFactory clientFactory;
	private final DecoratedView decoratedView;
	private final FrameView frameView;
	private EventBus eventBus;

	public DecoratedActivity(ClientFactory clientFactory, DecoratedView decoratedView) {
		this.clientFactory = clientFactory;
		this.decoratedView = decoratedView;
		this.eventBus = null;
		frameView = clientFactory.getFrameView();
	}

	public void start(AcceptsOneWidget panel, final EventBus eventBus) {
		this.eventBus = eventBus;
		decoratedView.getStatusText().setText(null);
		decoratedView.getErrorText().setText(null);

		updateHeader();
	}

	public ClientFactory getClientFactory() {
		return clientFactory;
	}

	public DecoratedView getView() {
		return decoratedView;
	}

	public EventBus getEventBus() {
		return eventBus;
	}

	public void onDataReady() {
		this.cancelAllHandlerRegistrations();

		addHandlerRegistration(ActionEvent.register(getEventBus(), ActionNames.CIRCLE_CHOSEN, new ActionEvent.Handler() {

			@Override
			public void onAction(ActionEvent event) {
				updateHeader();
			}
		}));

		addHandlerRegistration(clientFactory.getFrameView().getSponsorButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				HelpEntrySelectedEvent.fire(eventBus, SCREEN_HELP_TOPIC);
			}
		}));
	}

	public void updateHeader() {
		AppConstants appConstants = GWT.create(AppConstants.class);
		String style = " style='padding-top: 4px; height: 32px;'";
		if (clientFactory.getCircle() != null && clientFactory.getCircle().getCircleId() !=  null) {
			frameView.getTitle().setText(clientFactory.getCircle().getCircleName());
			if (clientFactory.getCircle().getSponsorId() !=  null) {
				frameView.getSponsorIcon().setHTML("<img src='" 
						+ UriUtils.fromSafeConstant(appConstants.resourcesUrl() + "sponsors/images/header/" 
								+ getClientFactory().getCircle().getSponsorId() + ".png").asString() + "'" + style + "/>");
			} else {
				frameView.getSponsorIcon().setHTML("<img src='" 
						+ UriUtils.fromSafeConstant(appConstants.resourcesUrl() + "sponsors/images/header/default.png").asString() + "'" + style + "/>");
			}
			if (clientFactory.getCircle().getCenterId() !=  null) {
				frameView.getCircleIcon().setHTML("<img src='" 
						+ UriUtils.fromSafeConstant(appConstants.resourcesUrl() + "users/" 
								+ getClientFactory().getCircle().getCenterId() + ".png").asString() + "'" + style + "/>");
			} else {
				frameView.getCircleIcon().setHTML("<img src='" 
						+ UriUtils.fromSafeConstant(appConstants.resourcesUrl() + "users/default.png").asString() + "'" + style + "/>");
			}
		} else {
			frameView.getTitle().setText(appConstants.title());
			frameView.getSponsorIcon().setHTML("<img src='" 
					+ UriUtils.fromSafeConstant(appConstants.resourcesUrl() + "sponsors/images/header/default.png").asString() + "'" + style + "/>");
			frameView.getCircleIcon().setHTML("<img src='" 
					+ UriUtils.fromSafeConstant(appConstants.resourcesUrl() + "users/default.png").asString() + "'" + style + "/>");
		}
	}

	public void setStatusText(String text) {
		decoratedView.getStatusText().setText(text);
		decoratedView.getErrorText().setText(null);
	}

	public void setStatusText(String text, int delayMsec) {
		setStatusText(text);
		new Timer() {
			@Override
			public void run() {
				decoratedView.getStatusText().setText(null);
			}
		}.schedule(delayMsec);
	}

	public void setErrorText(String text) {
		decoratedView.getStatusText().setText(null);
		decoratedView.getErrorText().setText(text);
	}

	public void setErrorText(String text, int delayMsec) {
		setErrorText(text);
		new Timer() {
			@Override
			public void run() {
				decoratedView.getErrorText().setText(null);
			}
		}.schedule(delayMsec);
	}

	public boolean serverIsReachable() {
		if (clientFactory.getPhoneGap().getConnection().getType().equalsIgnoreCase("NONE")) {
			decoratedView.alertServerCommunicationsProblem("Server Communications Problem"
					, "Please verify that you have a network connection and then try again.", null);
			return false;
		} 
		return true;
	}

	public void checkServerCallIssue(Throwable caught) {
		String text = new String("Unknown issue calling server. Please try again.");
		try {
			throw caught;
		} catch (IncompatibleRemoteServiceException e) {
			text = "This client is not compatible with the server. Please make sure that you are running the latest version of the app or, as appropriate, refresh the browser.";
		} catch (InvocationException e) {
			if (clientFactory.getPhoneGap().getConnection().getType().equalsIgnoreCase("NONE")) {
				text = "Please verify that you have a network connection and then try again.";
			} else {
				text = "The server call did not complete cleanly. Please try again.";
			}
		} catch (Throwable e) {
			text = caught.getLocalizedMessage();
		} finally {
			decoratedView.alertServerCommunicationsProblem("Server Communications Problem"
					, text + " If this issue persists, please contact support@voxgift.com.", null);
		}
	}
}
