package com.voxgift.winstonplus.client.activities.compose;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.googlecode.mgwt.ui.client.widget.celllist.Cell;
import com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails;

public class DictionaryEntryCell implements Cell<DictionaryEntryDetails> {

	private static Template TEMPLATE = GWT.create(Template.class);

	public interface Template extends SafeHtmlTemplates {
		String htmlString = "<div class='dictionary_entry'>{0}</div>";

		@SafeHtmlTemplates.Template(htmlString)
		SafeHtml content(String entryText);
	}

	@Override
	public void render(SafeHtmlBuilder safeHtmlBuilder, final DictionaryEntryDetails model) {
		// safeHtmlBuilder.append(TEMPLATE.content(SafeHtmlUtils.htmlEscape(model.getEntryText())));
		// doing  the above will mess up quotes
		safeHtmlBuilder.append(TEMPLATE.content(model.getEntryText()));
	}

	@Override
	public boolean canBeSelected(DictionaryEntryDetails model) {
		return true;
	}

}
