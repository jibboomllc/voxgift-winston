/*
 * http://google-web-toolkit.googlecode.com/svn/javadoc/latest/com/google/gwt/i18n/client/Messages.html
 */

package com.voxgift.winstonplus.shared;

import com.google.gwt.i18n.client.LocalizableResource.DefaultLocale;
import com.google.gwt.i18n.client.LocalizableResource.Generate;
import com.google.gwt.i18n.client.Messages;

@Generate(format = "com.google.gwt.i18n.rebind.format.PropertiesFormat")
@DefaultLocale("en_US")
public interface AppMessages extends Messages {

	// login screen prompts
	@DefaultMessage("Email address")
	String emailAddress();
	
	@DefaultMessage("Password")
	String password();

	@DefaultMessage("Repeat password")
	String repeatPassword();
	
/*
  @Key("1234")
  @DefaultMessage("This is a plain string.")
  String oneTwoThreeFour();
  
  @DefaultMessage("You have {0} widgets")
  @AlternateMessage({"one", "You have one widget"})
  String widgetCount(@PluralCount int count);
  
  @DefaultMessage("No reference to the argument")
  String optionalArg(@Optional String ignored);
  
  @DefaultMessage("Your cart total is {0,number,currency}")
  @Description("The total value of the items in the shopping cart in local currency")
  String totalAmount(@Example("$5.00") double amount);
  
  @Meaning("the color")
  @DefaultMessage("orange")
  String orangeColor();
  
  @Meaning("the fruit")
  @DefaultMessage("orange")
  String orangeFruit();
 */
}

