package com.voxgift.winstonplus.client.theme.custom;

import com.google.gwt.core.client.GWT;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.theme.MGWTClientBundle;
import com.googlecode.mgwt.ui.client.theme.MGWTTheme;

public class CustomTheme implements MGWTTheme {

	private MGWTClientBundle bundle;

	public CustomTheme() {
		if (MGWT.getOsDetection().isIOs()) {
			if (MGWT.getOsDetection().isIPhone()) {
				if (MGWT.getOsDetection().isRetina()) {
					bundle = (CustomBundleRetina) GWT.create(CustomBundleRetina.class);
				} else {
					bundle = (CustomBundleNonRetina) GWT.create(CustomBundleNonRetina.class);
				}
			} else {
				if (MGWT.getOsDetection().isIPadRetina()) {
					bundle = (CustomBundleThemeIPadRetina) GWT.create(CustomBundleThemeIPadRetina.class);
				} else {
					bundle = (CustomBundleThemeIPad) GWT.create(CustomBundleThemeIPad.class);
				}
			}
		} else if (MGWT.getOsDetection().isAndroid()) {
			if (MGWT.getOsDetection().isAndroidTablet()) {
				bundle = (CustomBundleThemeAndroidTablet) GWT.create(CustomBundleThemeAndroidTablet.class);
			} else {
				bundle = (CustomBundleThemeAndroid) GWT.create(CustomBundleThemeAndroid.class);
			}
		} else if (MGWT.getOsDetection().isBlackBerry()) {
			bundle = (CustomBundleThemeBlackberry) GWT.create(CustomBundleThemeBlackberry.class);
		} else if (MGWT.getOsDetection().isDesktop()) {
			bundle = (CustomBundleThemeDesktop) GWT.create(CustomBundleThemeDesktop.class);
		} else {
			bundle = (CustomBundleNonRetina) GWT.create(CustomBundleNonRetina.class);
		}
	}
	@Override
	public MGWTClientBundle getMGWTClientBundle() {
		return bundle;
	}

}
