/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user.image;

import java.util.HashMap;
import java.util.Map;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.googlecode.gwtphonegap.client.PhoneGap;
import com.googlecode.gwtphonegap.client.camera.PictureCallback;
import com.googlecode.gwtphonegap.client.camera.PictureOptions;
import com.googlecode.gwtphonegap.client.file.FileTransfer;
import com.googlecode.gwtphonegap.client.file.FileTransferError;
import com.googlecode.gwtphonegap.client.file.FileUploadCallback;
import com.googlecode.gwtphonegap.client.file.FileUploadOptions;
import com.googlecode.gwtphonegap.client.file.FileUploadResult;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.MGWT;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.DecoratedActivity;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent.SettingsEntry;
import com.voxgift.winstonplus.shared.AppConstants;

/**
 * @author Tom Giesberg
 * 
 */
public class ImageUserSettingsActivity extends DecoratedActivity {
	private java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(getClass().getName());

	private ImageUserSettingsView view;

	private final PhoneGap phoneGap;
	
	private String imageUrl;

	public ImageUserSettingsActivity(ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getImageUserSettingsView());

		phoneGap = clientFactory.getPhoneGap();
		view = clientFactory.getImageUserSettingsView();
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		getClientFactory().getFrameView().showBackbutton(!MGWT.getOsDetection().isAndroid());
		getClientFactory().getFrameView().getBackbuttonText().setText("User");
		getClientFactory().getFrameView().getTitle().setText("User Image");
		panel.setWidget(view);

		refreshData(getClientFactory().getUser().getId());
	}

	@Override
	public void onDataReady() {
		super.onDataReady();

		view.getUploadStatusText().setText("");

		if (view.getUploadForm() != null) {
			addHandlerRegistration(((FormPanel) view.getUploadForm()).addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
				@Override
				public void onSubmitComplete(SubmitCompleteEvent event) {
					view.getUploadStatusText().setText(event.getResults());
					view.setImageUrl("");
					view.setImageUrl(imageUrl);
				}
			}));
		}

		if (view.getUploadButton() != null) {
			addHandlerRegistration(view.getUploadButton().addTapHandler(new TapHandler() {

				@Override
				public void onTap(TapEvent event) {
					uploadImage();
				}
			}));
		}

		addHandlerRegistration(getClientFactory().getFrameView().getBackbuttonTarget().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				SettingsEntrySelectedEvent.fire(getEventBus(), SettingsEntry.USER); 
			}
		}));
	}
	
	public void uploadImage() {
		if (MGWT.getOsDetection().isDesktop()) {
			if (view.getFileToUploadName() == null || view.getFileToUploadName().isEmpty()) {
				view.getUploadStatusText().setText("Please pick a file before pressing upload");
			} else {
				view.getUploadStatusText().setText("Uploading file");
			    view.startImageUpload();
			}
		} else {
			PictureOptions pictureOptions = new PictureOptions();
			pictureOptions.setSourceType(PictureOptions.PICTURE_SOURCE_TYPE_PHOTO_LIBRARY);
			pictureOptions.setDestinationType(PictureOptions.DESTINATION_TYPE_FILE_URI);
			phoneGap.getCamera().getPicture(pictureOptions, new PictureCallback() {

				@Override
				public void onSuccess(String fileUrl) {
					if (fileUrl == null) {
						view.getUploadStatusText().setText("Error getting file");
					} else {
						view.getUploadStatusText().setText("Uploading file");
						AppConstants appConstants = GWT.create(AppConstants.class);
					
						LOGGER.log(java.util.logging.Level.INFO, "Calling createFileTransfer");
						FileTransfer trans = phoneGap.getFile().createFileTransfer();
						FileUploadOptions fileUploadOptions = new FileUploadOptions();
						fileUploadOptions.setMimeType("multipart");
						Map<String, String> params = new HashMap<String, String>();
						params.put("userId", getClientFactory().getUser().getId());
						fileUploadOptions.setParams(params);

						LOGGER.log(java.util.logging.Level.INFO, "Calling upload");
						trans.upload(fileUrl, appConstants.imageUploadUrl(), fileUploadOptions, new FileUploadCallback() {
	
							@Override
							public void onSuccess(FileUploadResult result) {
								view.getUploadStatusText().setText("File upload result: " + result.getResponse());
							}
	
							@Override
							public void onFailure(FileTransferError error) {
								view.getUploadStatusText().setText("Error uploading file: " + error.getCode());
							}
							
						});
					}
				}

				@Override
				public void onFailure(String message) {
					view.getUploadStatusText().setText("Error reading file: " + message);
				}
			});
		}
	}

	private void refreshData(final String id) {
		AppConstants appConstants = GWT.create(AppConstants.class);
		imageUrl = new String (appConstants.resourcesUrl() + "users/" + id + ".png");
		view.setImageUrl(imageUrl);
		if (view.getUploadFormUserId() != null) {
			view.getUploadFormUserId().setValue(id);
		}
		onDataReady();
	}
}
