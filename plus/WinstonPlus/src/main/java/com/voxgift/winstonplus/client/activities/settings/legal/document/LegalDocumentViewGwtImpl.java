/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.legal.document;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHTML;
import com.googlecode.mgwt.ui.client.widget.LayoutPanel;
import com.googlecode.mgwt.ui.client.widget.ScrollPanel;
import com.voxgift.winstonplus.client.DecoratedViewGwtImpl;

/**
 * @author Tom Giesberg
 * 
 */
public class LegalDocumentViewGwtImpl extends DecoratedViewGwtImpl implements LegalDocumentView {

	private HTML content;
    private ScrollPanel scrollPanel;
    
	public LegalDocumentViewGwtImpl() {
		main.addStyleName("legal");
		
		content = new HTML();

		LayoutPanel container = new LayoutPanel();
		container.add(content);

		scrollPanel = new ScrollPanel();
		scrollPanel.setWidget(container);
		scrollPanel.setScrollingEnabledX(false);
		main.add(scrollPanel);
	}

	@Override
	public HasHTML getContent() {
		return content;
	}

	@Override
	public void refresh() {
		scrollPanel.refresh();
	}
}
