/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.circle.basic;

import java.util.List;

import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.user.client.ui.HasText;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.dialog.ConfirmDialog.ConfirmCallback;
import com.googlecode.mgwt.ui.client.widget.celllist.HasCellSelectedHandler;
import com.voxgift.winstonplus.client.DecoratedView;
import com.voxgift.winstonplus.client.Item;

/**
 * @author Tom Giesberg
 * 
 */
public interface BasicCircleSettingsView extends DecoratedView {

	public HasCellSelectedHandler getList();

	public void renderItems(List<Item> items);

	public void setSelectedIndex(int index, boolean selected);

	public void enableEditing(boolean isEditable);

	public HasTapHandlers getDeleteButton();

	public HasChangeHandlers getNameEditor();
	
	public HasText getName();

	public void confirmDelete(String title, String text, ConfirmCallback callback);
}
