package com.voxgift.winstonplus.client.event;

public interface ActionNames {
	public static final String BACK = "back";
	public static final String ANIMATION_END = "ae";
	public static final String CIRCLE_CHOSEN = "cc";
	public static final String LOGOUT = "logout";
}
