package com.voxgift.winstonplus.client.css;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.TextResource;

public interface WinstonBundle extends ClientBundle {
	@Source("winston.css")
	TextResource css();

	public static final WinstonBundle INSTANCE = GWT.create(WinstonBundle.class);

}
