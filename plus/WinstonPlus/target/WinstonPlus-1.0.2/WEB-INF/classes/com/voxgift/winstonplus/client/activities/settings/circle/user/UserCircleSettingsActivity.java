/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.circle.user;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.gwtphonegap.client.PhoneGap;
import com.googlecode.gwtphonegap.client.contacts.Contact;
import com.googlecode.gwtphonegap.client.contacts.ContactError;
import com.googlecode.gwtphonegap.client.contacts.ContactField;
import com.googlecode.gwtphonegap.client.contacts.ContactFindCallback;
import com.googlecode.gwtphonegap.client.contacts.ContactFindOptions;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.gwtphonegap.collection.shared.CollectionFactory;
import com.googlecode.gwtphonegap.collection.shared.LightArray;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.dialog.ConfirmDialog.ConfirmCallback;
import com.googlecode.mgwt.ui.client.dialog.Dialogs.ButtonType;
import com.googlecode.mgwt.ui.client.dialog.Dialogs.OptionCallback;
import com.googlecode.mgwt.ui.client.dialog.Dialogs.OptionsDialogEntry;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.DecoratedActivity;
import com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsEntrySelectedEvent;
import com.voxgift.winstonplus.shared.AppConstants;
import com.voxgift.winstonplus.data.shared.model.CircleUserStatus;
import com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcCircleService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcCircleServiceAsync;

/**
 * @author Tom Giesberg
 * 
 */
public class UserCircleSettingsActivity extends DecoratedActivity {

	private UserCircleSettingsView view;

	private String circleId;
	private String userId;
	private CircleUserStatusDetails circleUserStatusDetails;

	private RpcCircleServiceAsync circleSvc = GWT.create(RpcCircleService.class);

	private final PhoneGap phoneGap;

	public class ContactLookupRecord {
		private String familyName;
		private String givenName;
		private String email;
		public ContactLookupRecord(String familyName, String givenName, String email) {
			this.familyName = familyName;
			this.givenName = givenName;
			this.email = email;
		}

		@Override
		public String toString() {
			String text = new String();
			if(givenName != null && !givenName.isEmpty()) {
				text = givenName + " ";
			}
			if(familyName != null && !familyName.isEmpty()) {
				text = text + familyName + " ";
			}
			text = text + "[" + email + "]";
			return text;
		}
	}

	private List<ContactLookupRecord> contactLookupRecords;
	private static int RESULTS_MAX_PAGE_SIZE = 5;

	public UserCircleSettingsActivity(ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getUserCircleSettingsView());
		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) circleSvc, appConstants.webAppUrl(), "rpc/circle");
		phoneGap = getClientFactory().getPhoneGap();

		view = clientFactory.getUserCircleSettingsView();

		view.addRoleItem("Center");
		view.addRoleItem("Family");
		view.addRoleItem("Friend");
		view.addRoleItem("Provider");

		getClientFactory().getFrameView().showBackbutton(!MGWT.getOsDetection().isAndroid());
		getClientFactory().getFrameView().getBackbuttonText().setText("Basic");
		getClientFactory().getFrameView().getTitle().setText("Circle User Settings");

		contactLookupRecords = new ArrayList<ContactLookupRecord> ();
	}

	public void setCircleId(String circleId) {
		this.circleId = circleId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		panel.setWidget(view);

		setStatusText("");
		refreshData();
	}

	@Override
	public void onDataReady() {
		super.onDataReady();

		boolean isEditable = (userId == null);
		view.enableEditing(isEditable);

		view.getLookupText().setText(null);

		view.getEmailAddress().setText(circleUserStatusDetails.getEmail());					
		view.getFirstName().setText(circleUserStatusDetails.getFirstName());					
		view.getLastName().setText(circleUserStatusDetails.getLastName());
		view.setSelectedRoleIndex(circleUserStatusDetails.getRoleId(), true);

		switch (circleUserStatusDetails.getStatusId()) {
		case CircleUserStatus.INVITED:
			view.getUserStatus().setText("Invited");
			break;
		case CircleUserStatus.ACCEPTED:
			view.getUserStatus().setText("Accepted");
			break;
		case CircleUserStatus.REJECTED:
			view.getUserStatus().setText("Rejected");
			break;
		case CircleUserStatus.UNSUBSCRIBED:
			view.getUserStatus().setText("Unsubscribed");
			break;
		case CircleUserStatus.REMOVED:
			view.getUserStatus().setText("Removed");
			break;
		default:
			view.getUserStatus().setText("");
			break;

		}

		addHandlerRegistration(view.getRoleList().addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				updateRole();
			}
		}));

		addHandlerRegistration(view.getLookupButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				lookupUser();
			}
		}));

		addHandlerRegistration(view.getInviteButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				inviteUser();
			}
		}));

		addHandlerRegistration(view.getRemoveButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				removeUser();
			}
		}));

		addHandlerRegistration(getClientFactory().getFrameView().getBackbuttonTarget().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				CircleSettingsEntrySelectedEvent.fire(getEventBus(), circleId);
			}
		}));
	}

	private void refreshData() {

		if (userId == null) {
			circleUserStatusDetails = new CircleUserStatusDetails();
			onDataReady();
		} else {
			AsyncCallback<CircleUserStatusDetails> callback = new AsyncCallback<CircleUserStatusDetails>() {
				public void onFailure(Throwable caught) {
					checkServerCallIssue(caught);
				}

				public void onSuccess(CircleUserStatusDetails result) {
					if (result == null) {
						setErrorText("No data returned for user: " + userId);
					} else {
						circleUserStatusDetails = result;
					}
					onDataReady();
				}
			};

			if (serverIsReachable()) {
				circleSvc.getCircleUserStatus(circleId, userId, callback);
			}
		}
	}

	private void updateRole() {
		if (userId != null) {
			if (view.getSelectedRoleIndex() != circleUserStatusDetails.getRoleId()) {

				setStatusText("Updating");

				AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {

					public void onFailure(Throwable caught) {
						checkServerCallIssue(caught);
					}

					public void onSuccess(Boolean result) {
						if (result) {
							circleUserStatusDetails.setRoleId(view.getSelectedRoleIndex());
							setStatusText("User updated");
						} else {
							setErrorText("User update error");
						}
					}
				};

				if (serverIsReachable()) {
					circleSvc.updateUserRole(circleId, userId, view.getSelectedRoleIndex(), callback);
				}
			}
		}
	}

	private void lookupUser() {
		if (view.getLookupText().getText().length() < 1) {
			setStatusText("Please enter something to lookup");
			/*
		// Test code
		} else if (true) {
			contactLookupRecords.clear();
			for(int i = 0; i < new Integer(view.getLookupText().getText()); i++) {
				contactLookupRecords.add(new ContactLookupRecord("familyName"+i, "givenName"+i, "email"+i));
			}
			showLookupResults(0);
			 */
		} else {
			setStatusText("Looking up");

			final ContactFindCallback callback = new ContactFindCallback() {

				@Override
				public void onFailure(ContactError error) {
					setErrorText("Error looking up user: " + error.toString());
				}

				@Override
				public void onSuccess(final LightArray<Contact> contacts) {
					if(contacts == null || contacts.length() == 0) {
						setStatusText("No matching contacts found.");
					} else {
						contactLookupRecords.clear();
						ContactLookupRecord contactLookupRecord;

						for(int i = 0; i < contacts.length(); i++) {
							Contact contact = contacts.get(i);
							String familyName = contact.getName().getFamilyName();
							String givenName = contact.getName().getGivenName();
							LightArray<ContactField> emails = contact.getEmails();
							for(int j = 0; j < emails.length(); j++) {
								if (emails.get(j).getValue() != null && !emails.get(j).getValue().isEmpty()) {
									contactLookupRecord = new ContactLookupRecord(familyName, givenName, emails.get(j).getValue());
									contactLookupRecords.add(contactLookupRecord);
								}
							}
						}
						showLookupResults(0);
					}
				}
			};

			LightArray<String> fields = CollectionFactory.<String> constructArray();
			fields.push("name");
			fields.push("emails");
			ContactFindOptions contactFindOptions = new ContactFindOptions();
			contactFindOptions.setFilter(view.getLookupText().getText());
			contactFindOptions.setMutiple(true);
			if(phoneGap != null && phoneGap.getContacts() != null) {
				phoneGap.getContacts().find(fields, callback, contactFindOptions);
			}
		}
	}

	void showLookupResults (final int lookupResultsStartingIndex) {
		List<OptionsDialogEntry> list = new ArrayList<OptionsDialogEntry>();

		if (lookupResultsStartingIndex > 0) {
			list.add(new OptionsDialogEntry("<<", ButtonType.NORMAL));
		}

		for (int i = lookupResultsStartingIndex; i < lookupResultsStartingIndex + RESULTS_MAX_PAGE_SIZE && i < contactLookupRecords.size(); i++) {
			list.add(new OptionsDialogEntry(contactLookupRecords.get(i).toString(), ButtonType.NORMAL));
		}

		if (contactLookupRecords.size() > lookupResultsStartingIndex + RESULTS_MAX_PAGE_SIZE) {
			list.add(new OptionsDialogEntry(">>", ButtonType.NORMAL));
		}

		list.add(new OptionsDialogEntry("Cancel", ButtonType.NORMAL));

		view.showLookupResults(list, new OptionCallback() {
			@Override
			public void onOptionSelected(int index) { // index is 1-based not 0-based
				int resultsPageSize = contactLookupRecords.size() > lookupResultsStartingIndex + RESULTS_MAX_PAGE_SIZE ? RESULTS_MAX_PAGE_SIZE : contactLookupRecords.size() - lookupResultsStartingIndex;
				int adjustedIndex = lookupResultsStartingIndex > 0 ? index - 2 : index - 1;
				if (lookupResultsStartingIndex > 0 && index - 1 == 0) {
					// Previous button
					showLookupResults(lookupResultsStartingIndex - RESULTS_MAX_PAGE_SIZE);
				} else if (adjustedIndex < resultsPageSize) {
					int lookupIndex = lookupResultsStartingIndex + adjustedIndex;
					view.getFirstName().setText(contactLookupRecords.get(lookupIndex).givenName);
					view.getLastName().setText(contactLookupRecords.get(lookupIndex).familyName);
					view.getEmailAddress().setText(contactLookupRecords.get(lookupIndex).email);
				} else if ((contactLookupRecords.size() > lookupResultsStartingIndex + RESULTS_MAX_PAGE_SIZE)
						&& (adjustedIndex == resultsPageSize)) {
					// Next button
					showLookupResults(lookupResultsStartingIndex + RESULTS_MAX_PAGE_SIZE);
				} else {
					// Cancel button therefore no-op
				}
			}
		});
	}


	private void inviteUser() {
		if (view.getEmailAddress().getText().length() < 5) {
			setStatusText("Please enter a valid email address");
		} else {
			setStatusText("Inviting");

			final AsyncCallback<String> callback = new AsyncCallback<String>() {

				public void onFailure(Throwable caught) {
					checkServerCallIssue(caught);
				}

				public void onSuccess(String result) {
					if (result != null) {
						userId = result;
						refreshData();
						setStatusText("User invited");
					} else {
						setErrorText("User invitation error");
					}
				}
			};

			if (serverIsReachable()) {
				circleSvc.inviteUser(circleId, userId, view.getEmailAddress().getText().toLowerCase()
						, view.getFirstName().getText(), view.getLastName().getText()
						, view.getSelectedRoleIndex(), callback);
			}
		}
	}

	private void removeUser() {
		if (userId == null) {
			CircleSettingsEntrySelectedEvent.fire(getEventBus(), circleId);
		} else {
			view.confirmRemove("Confirm Remove", "Please confirm that you wish to remove this user.", new ConfirmCallback() {

				@Override
				public void onOk() {
					final AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
						public void onFailure(Throwable caught) {
							checkServerCallIssue(caught);
						}

						public void onSuccess(Boolean result) {
							if (result == null || result == false) {
								setErrorText("Circle user not found");
							} else {
								setStatusText("User status has been updated"); 
							}
						}
					};
					if (serverIsReachable()) {
						circleSvc.setCircleUserStatus(circleId, userId, CircleUserStatus.REMOVED, callback);
					}
				}

				@Override
				public void onCancel() {
				}
			});

		}

	}

}
