/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user.image;

import com.google.gwt.event.shared.HasHandlers;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.HasText;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.voxgift.winstonplus.client.DecoratedView;

/**
 * @author Tom Giesberg
 * 
 */
public interface ImageUserSettingsView extends DecoratedView {
	public void setImageUrl(String url);
	public HasText getUploadStatusText();
	public HasHandlers getUploadForm();
	public String getFileToUploadName();
	public TakesValue<String> getUploadFormUserId();
	public HasTapHandlers getUploadButton();
	public void startImageUpload();
}
