package com.voxgift.winstonplus.client.activities.messagelist;

import java.util.List;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Widget;

import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.googlecode.mgwt.ui.client.widget.CellList;
import com.googlecode.mgwt.ui.client.widget.LayoutPanel;
import com.googlecode.mgwt.ui.client.widget.base.HasRefresh;
import com.googlecode.mgwt.ui.client.widget.base.PullArrowFooter;
import com.googlecode.mgwt.ui.client.widget.base.PullArrowHeader;
import com.googlecode.mgwt.ui.client.widget.base.PullArrowWidget;
import com.googlecode.mgwt.ui.client.widget.base.PullPanel;
import com.googlecode.mgwt.ui.client.widget.base.PullPanel.Pullhandler;

import com.voxgift.winstonplus.client.DecoratedViewGwtImpl;
import com.voxgift.winstonplus.data.shared.model.MessageDetails;

public class MessageListViewGwtImpl  extends DecoratedViewGwtImpl implements MessageListView {

	private PullPanel pullToRefresh;
	private CellList<MessageDetails> cellList;

	private PullArrowHeader pullArrowHeader;
	private PullArrowFooter pullArrowFooter;

	private LayoutPanel helpPanel;
	private Button refreshButton;
	private HTML helpHtml;
	
	public MessageListViewGwtImpl() {
		pullToRefresh = new PullPanel();
		pullArrowHeader = new PullArrowHeader();
		pullToRefresh.setHeader(pullArrowHeader);
		pullArrowFooter = new PullArrowFooter();
		pullToRefresh.setFooter(pullArrowFooter);
		cellList = new CellList<MessageDetails>(new MessageCell());
		cellList.setGroup(false);
		pullToRefresh.add(cellList);
		main.add(pullToRefresh);
		
		helpPanel = new LayoutPanel();
		helpPanel.addStyleName("help");
		helpHtml = new HTML();
		helpPanel.add(helpHtml);
		refreshButton = new Button();
		helpPanel.add(refreshButton);
		main.add(helpPanel);
	}

	@Override
	public Widget asWidget() {
		return main;
	}

	@Override
	public void render(List<MessageDetails> messages) {
		if (messages == null || messages.isEmpty()) {
			pullToRefresh.setVisible(false);
			helpPanel.setVisible(true);
		} else {
			helpPanel.setVisible(false);
			pullToRefresh.setVisible(true);
			cellList.render(messages);
		}
	}

	@Override
	public void setHeaderPullHandler(Pullhandler pullHandler) {
		pullToRefresh.setHeaderPullhandler(pullHandler);

	}

	@Override
	public PullArrowWidget getPullHeader() {
		return pullArrowHeader;
	}

	@Override
	public void refresh() {
		pullToRefresh.refresh();

	}

	@Override
	public void setFooterPullHandler(Pullhandler pullHandler) {
		pullToRefresh.setFooterPullHandler(pullHandler);

	}

	@Override
	public PullArrowWidget getPullFooter() {
		return pullArrowFooter;
	}

	@Override
	public HasRefresh getPullPanel() {
		return pullToRefresh;
	}

	@Override
	public HasText getRefreshButtonText() {
		return refreshButton;
	}

	@Override
	public HasTapHandlers getRefreshButton() {
		return refreshButton;
	}

	@Override
	public HasHTML getHelpScreen() {
		return helpHtml;
	}
}
