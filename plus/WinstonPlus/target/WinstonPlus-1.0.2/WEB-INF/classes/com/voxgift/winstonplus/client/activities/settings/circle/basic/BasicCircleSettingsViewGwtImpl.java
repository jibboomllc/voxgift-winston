/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.circle.basic;

import java.util.List;

import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.dialog.ConfirmDialog.ConfirmCallback;
import com.googlecode.mgwt.ui.client.dialog.Dialogs;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.googlecode.mgwt.ui.client.widget.CellList;
import com.googlecode.mgwt.ui.client.widget.FormListEntry;
import com.googlecode.mgwt.ui.client.widget.LayoutPanel;
import com.googlecode.mgwt.ui.client.widget.MTextBox;
import com.googlecode.mgwt.ui.client.widget.ScrollPanel;
import com.googlecode.mgwt.ui.client.widget.WidgetList;
import com.googlecode.mgwt.ui.client.widget.celllist.BasicCell;
import com.googlecode.mgwt.ui.client.widget.celllist.HasCellSelectedHandler;
import com.voxgift.winstonplus.client.DecoratedViewGwtImpl;
import com.voxgift.winstonplus.client.Item;

/**
 * @author Tom Giesberg
 * 
 */
public class BasicCircleSettingsViewGwtImpl extends DecoratedViewGwtImpl implements BasicCircleSettingsView {

	private CellList<Item> cellListWithHeader;

	private boolean isEditable;

	private Label circleNameStatic;

	private MTextBox circleNameEditor;

	private Button deleteButton;

	public BasicCircleSettingsViewGwtImpl() {
		isEditable = false;

		ScrollPanel scrollPanel = new ScrollPanel();
		main.add(scrollPanel);

		LayoutPanel container = new LayoutPanel();

		container.add(statusText);
		container.add(errorText);

		WidgetList widgetList = new WidgetList();
		widgetList.setRound(true);

		LayoutPanel circleNamePanel = new LayoutPanel();
		circleNameStatic = new Label();
		circleNamePanel.add(circleNameStatic);
		circleNameEditor = new MTextBox();
		circleNamePanel.add(circleNameEditor);

		widgetList.add(new FormListEntry("Name", circleNamePanel));

		container.add(widgetList);

		cellListWithHeader = new CellList<Item>(new BasicCell<Item>() {

			@Override
			public String getDisplayString(Item model) {
				return model.getDisplayString();
			}

			@Override
			public boolean canBeSelected(Item model) {
				return isEditable;
			}
		});
		cellListWithHeader.setRound(true);
		cellListWithHeader.setGroup(isEditable);

		container.add(cellListWithHeader);

		deleteButton = new Button("Delete Circle");
		container.add(deleteButton);

		scrollPanel.setWidget(container);
		scrollPanel.setScrollingEnabledX(false);
		
		// workaround for android formfields jumping around when using
	    // -webkit-transform
	    scrollPanel.setUsePos(MGWT.getOsDetection().isAndroid());
	}


	@Override
	public HasCellSelectedHandler getList() {
		return cellListWithHeader;
	}

	@Override
	public void renderItems(List<Item> items) {
		cellListWithHeader.render(items);
	}

	@Override
	public void setSelectedIndex(int index, boolean selected) {
		cellListWithHeader.setSelectedIndex(index, selected);

	}

	@Override
	public void enableEditing(boolean isEditable) {
		this.isEditable = isEditable;

		this.cellListWithHeader.setGroup(isEditable);

		circleNameEditor.setVisible(isEditable);
		circleNameStatic.setVisible(!isEditable);

		deleteButton.setVisible(isEditable);
	}

	@Override
	public HasTapHandlers getDeleteButton() {
		return deleteButton;
	}

	@Override
	public HasChangeHandlers getNameEditor() {
		if (isEditable) {
			return circleNameEditor;
		}
		return null;
	}

	@Override
	public HasText getName() {
		if (isEditable) {
			return circleNameEditor;
		}
		return circleNameStatic;
	}


	@Override
	public void confirmDelete(String title, String text, ConfirmCallback callback) {
		Dialogs.confirm(title, text, callback);
	}

}
