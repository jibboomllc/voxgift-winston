/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.help;

import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.dialog.Dialogs;
import com.googlecode.mgwt.ui.client.dialog.Dialogs.AlertCallback;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.googlecode.mgwt.ui.client.widget.HeaderPanel;
import com.googlecode.mgwt.ui.client.widget.LayoutPanel;
import com.googlecode.mgwt.ui.client.widget.ScrollPanel;
import com.voxgift.winstonplus.client.widget.FlipWidget;

/**
 * @author Tom Giesberg
 * 
 */
public class HelpViewGwtImpl implements HelpView {

	private LayoutPanel main;
    private ScrollPanel scrollPanel;
	private HTML title;
	private HTML content;
	private Button closeButton;
	private Button nextButton;
	private Button previousButton;
	private FlipWidget nextFlipWidget;
	private FlipWidget previousFlipWidget;

	public HelpViewGwtImpl() {
		main = new LayoutPanel();
		main.addStyleName("help");
		
		HeaderPanel headerPanel = new HeaderPanel();
		title = new HTML();
		headerPanel.setCenterWidget(title);

		HorizontalPanel leftWidget = new HorizontalPanel();
		leftWidget.addStyleName("help_controls");
		closeButton = new Button();
		closeButton.addStyleName("close");
		leftWidget.add(closeButton);
		headerPanel.setLeftWidget(leftWidget);

		HorizontalPanel rightWidget = new HorizontalPanel();
		rightWidget.addStyleName("help_controls");
		previousButton = new Button();
		previousButton.addStyleName("previous");
		Button disabledPreviousButton = new Button();
		disabledPreviousButton.addStyleName("previous_disabled");
		previousFlipWidget = new FlipWidget(previousButton, disabledPreviousButton);
		rightWidget.add(previousFlipWidget);
		nextButton = new Button();
		nextButton.addStyleName("next");
		Button disabledNextButton = new Button();
		disabledNextButton.addStyleName("next_disabled");
		nextFlipWidget = new FlipWidget(nextButton, disabledNextButton);
		rightWidget.add(nextFlipWidget);
		headerPanel.setRightWidget(rightWidget);

		main.add(headerPanel);

		content = new HTML();

		LayoutPanel container = new LayoutPanel();
		container.add(content);

		scrollPanel = new ScrollPanel();
		scrollPanel.setWidget(container);
		scrollPanel.setScrollingEnabledX(false);
		main.add(scrollPanel);
	}

	@Override
	public Widget asWidget() {
		return main;
	}

	@Override
	public HasHTML getTitle() {
		return title;
	}

	@Override
	public HasHTML getContent() {
		return content;
	}

	@Override
	public HasTapHandlers getCloseButton() {
		return closeButton;
	}

	@Override
	public HasTapHandlers getNextButton() {
		return nextButton;
	}

	@Override
	public HasVisibility getNextButtonVisibility() {
		return nextFlipWidget;
	}

	@Override
	public HasTapHandlers getPreviousButton() {
		return previousButton;
	}

	@Override
	public HasVisibility getPreviousButtonVisibility() {
		return previousFlipWidget;
	}

	@Override
	public void refresh() {
		scrollPanel.refresh();
	}

	@Override
	public void alertServerCommunicationsProblem(String title, String text, AlertCallback callback) {
		Dialogs.alert(title, text, callback);
	}
}
