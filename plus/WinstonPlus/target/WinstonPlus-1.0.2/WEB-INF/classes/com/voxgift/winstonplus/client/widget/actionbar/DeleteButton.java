package com.voxgift.winstonplus.client.widget.actionbar;

import com.googlecode.mgwt.ui.client.MGWTStyle;
import com.voxgift.winstonplus.client.css.ActionBarCss;
import com.voxgift.winstonplus.client.css.CssBundle;
import com.voxgift.winstonplus.client.theme.ExtendedClientBundle;

public class DeleteButton extends ActionBarButton {

	public DeleteButton() {
		this(CssBundle.INSTANCE.actionBarCss(), "Delete");
	}

	public DeleteButton(String text) {
		this(CssBundle.INSTANCE.actionBarCss(), text);
	}

	public DeleteButton(ActionBarCss css, String text) {
		super(css, ((ExtendedClientBundle) MGWTStyle.getTheme().getMGWTClientBundle()).tabBarDeleteImage());
		setText(text);
	}
}
