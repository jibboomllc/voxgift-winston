package com.voxgift.winstonplus.client.widget.actionbar;

import com.googlecode.mgwt.ui.client.MGWTStyle;
import com.voxgift.winstonplus.client.css.ActionBarCss;
import com.voxgift.winstonplus.client.css.CssBundle;
import com.voxgift.winstonplus.client.theme.ExtendedClientBundle;

public class SendButton extends ActionBarButton {

	public SendButton() {
		this(CssBundle.INSTANCE.actionBarCss(), "Send");
	}

	public SendButton(String text) {
		this(CssBundle.INSTANCE.actionBarCss(), text);
	}

	public SendButton(ActionBarCss css, String text) {
		super(css, ((ExtendedClientBundle) MGWTStyle.getTheme().getMGWTClientBundle()).tabBarCommentPlusImage());
		setText(text);
	}
}
