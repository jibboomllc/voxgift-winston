package com.voxgift.winstonplus.client.activities.settings.circle;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class CircleSettingsEntrySelectedEvent extends Event<CircleSettingsEntrySelectedEvent.Handler> {

	public interface Handler {
		void onAnimationSelected(CircleSettingsEntrySelectedEvent event);
	}

	private static final Type<CircleSettingsEntrySelectedEvent.Handler> TYPE = new Type<CircleSettingsEntrySelectedEvent.Handler>();
	private final String entry;

	public static void fire(EventBus eventBus, String string) {
		eventBus.fireEvent(new CircleSettingsEntrySelectedEvent(string));
	}

	public static HandlerRegistration register(EventBus eventBus, Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	@Override
	public com.google.web.bindery.event.shared.Event.Type<Handler> getAssociatedType() {
		return TYPE;
	}

	protected CircleSettingsEntrySelectedEvent(String string) {
		this.entry = string;

	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onAnimationSelected(this);

	}

	public static Type<CircleSettingsEntrySelectedEvent.Handler> getType() {
		return TYPE;
	}

	public String getEntry() {
		return entry;
	}
}
