/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user.basic;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class BasicUserSettingsPlace extends Place {
	public static class BasicUserSettingsPlaceTokenizer implements PlaceTokenizer<BasicUserSettingsPlace> {

		@Override
		public BasicUserSettingsPlace getPlace(String token) {
			return new BasicUserSettingsPlace();
		}

		@Override
		public String getToken(BasicUserSettingsPlace place) {
			return "";
		}

	}
}
