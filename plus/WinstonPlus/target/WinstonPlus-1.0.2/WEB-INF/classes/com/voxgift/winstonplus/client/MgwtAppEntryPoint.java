/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client;

import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.StyleInjector;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import com.googlecode.gwtphonegap.client.PhoneGap;
import com.googlecode.gwtphonegap.client.PhoneGapAvailableEvent;
import com.googlecode.gwtphonegap.client.PhoneGapAvailableHandler;
import com.googlecode.gwtphonegap.client.PhoneGapTimeoutEvent;
import com.googlecode.gwtphonegap.client.PhoneGapTimeoutHandler;
import com.googlecode.mgwt.mvp.client.AnimatableDisplay;
import com.googlecode.mgwt.mvp.client.AnimatingActivityManager;
import com.googlecode.mgwt.mvp.client.AnimationMapper;
import com.googlecode.mgwt.mvp.client.history.MGWTPlaceHistoryHandler;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.MGWTSettings;
import com.googlecode.mgwt.ui.client.MGWTSettings.ViewPort;
import com.googlecode.mgwt.ui.client.MGWTSettings.ViewPort.DENSITY;
import com.googlecode.mgwt.ui.client.MGWTStyle;
import com.googlecode.mgwt.ui.client.layout.OrientationRegionHandler;
import com.googlecode.mgwt.ui.client.util.SuperDevModeUtil;
import com.googlecode.mgwt.ui.client.widget.tabbar.RootTabPanel;
import com.voxgift.winstonplus.client.activities.UIEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.UIEntrySelectedEvent.UIEntry;
import com.voxgift.winstonplus.client.activities.frame.FrameView;
import com.voxgift.winstonplus.client.activities.welcome.WelcomeActivityMapper;
import com.voxgift.winstonplus.client.activities.welcome.WelcomeAnimationMapper;
import com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckPlace;
import com.voxgift.winstonplus.client.css.AppBundle;
import com.voxgift.winstonplus.client.css.WinstonBundle;
import com.voxgift.winstonplus.client.theme.custom.CustomTheme;
import com.voxgift.winstonplus.client.widget.tabbar.GearTabBarButton;
import com.voxgift.winstonplus.client.widget.tabbar.ListTabBarButton;
import com.voxgift.winstonplus.client.widget.tabbar.PencilTabBarButton;

public class MgwtAppEntryPoint implements EntryPoint {

	//private Logger LOGGER = Logger.getLogger(getClass().getName());

	@Override
	public void onModuleLoad() {

		GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {

			@Override
			public void onUncaughtException(Throwable e) {
				Window.alert("uncaught: " + e.getLocalizedMessage());
				Window.alert(e.getMessage());
				//LOGGER.log(Level.SEVERE, "uncaught exception", e);
			}
		});

		final PhoneGap phoneGap = GWT.create(PhoneGap.class);

		phoneGap.addHandler(new PhoneGapAvailableHandler() {

			@Override
			public void onPhoneGapAvailable(PhoneGapAvailableEvent event) {
				//LOGGER.log(Level.INFO, "PhoneGapAvailableEvent");
				start(phoneGap);
			}
		});

		phoneGap.addHandler(new PhoneGapTimeoutHandler() {

			@Override
			public void onPhoneGapTimeout(PhoneGapTimeoutEvent event) {
				//LOGGER.log(Level.INFO, "can not load phonegap");
				Window.alert("can not load phonegap");
			}
		});

		//LOGGER.log(Level.INFO, "initializePhoneGap");
		phoneGap.initializePhoneGap();

	}

	private void start(PhoneGap phoneGap) {
		final ClientFactory clientFactory = new ClientFactoryImpl();
		clientFactory.setPhoneGap(phoneGap);

		SuperDevModeUtil.showDevMode();

		MGWTStyle.setTheme(new CustomTheme());

		ViewPort viewPort = new MGWTSettings.ViewPort();
		viewPort.setTargetDensity(DENSITY.MEDIUM);
		viewPort.setUserScaleAble(false).setMinimumScale(1.0)
		.setMinimumScale(1.0).setMaximumScale(1.0);

		MGWTSettings settings = new MGWTSettings();
		settings.setViewPort(viewPort);
		settings.setIconUrl("logo.png");
		settings.setAddGlosToIcon(true);
		settings.setFullscreen(true);
		settings.setPreventScrolling(true);

		MGWT.applySettings(settings);

		// TODO Eliminate the need for these StyleInjectors
		StyleInjector.inject(WinstonBundle.INSTANCE.css().getText());
		StyleInjector.inject(AppBundle.INSTANCE.css().getText());

		if (clientFactory.usingSinglePane()) {
			createSinglePaneDisplay(clientFactory);
		} else {
			createDoublePaneDisplay(clientFactory);
		}
		createWelcomeDisplay(clientFactory);

		// Start PlaceHistoryHandler with our PlaceHistoryMapper
		AppPlaceHistoryMapper historyMapper = GWT
				.create(AppPlaceHistoryMapper.class);

		AppHistoryObserver historyObserver = new AppHistoryObserver(
				clientFactory.usingSinglePane());

		MGWTPlaceHistoryHandler historyHandler = new MGWTPlaceHistoryHandler(
				historyMapper, historyObserver);

		historyHandler.register(clientFactory.getPlaceController(),
				clientFactory.getEventBus(), new ConnectCheckPlace());
		historyHandler.handleCurrentHistory();
	}

	private void createWelcomeDisplay(ClientFactory clientFactory) {
		AnimatableDisplay display = GWT.create(AnimatableDisplay.class);

		WelcomeActivityMapper appActivityMapper = new WelcomeActivityMapper(
				clientFactory);

		WelcomeAnimationMapper appAnimationMapper = new WelcomeAnimationMapper();

		AnimatingActivityManager activityManager = new AnimatingActivityManager(
				appActivityMapper, appAnimationMapper,
				clientFactory.getEventBus());

		activityManager.setDisplay(display);

		display.asWidget().getElement().setId("welcome");

		clientFactory.setWelcomeWidget(display.asWidget());

		RootPanel.get().add(display);
	}


	private void createSinglePaneDisplay(final ClientFactory clientFactory) {
		FrameView frameView = clientFactory.getFrameView();
		AnimatableDisplay display = GWT.create(AnimatableDisplay.class);

		SinglePaneActivityMapper appActivityMapper = new SinglePaneActivityMapper(clientFactory);

		MainAnimationMapper appAnimationMapper = new MainAnimationMapper();

		AnimatingActivityManager activityManager = new AnimatingActivityManager(
				appActivityMapper, appAnimationMapper,
				clientFactory.getEventBus());

		RootTabPanel mainContainer = new RootTabPanel(MGWTStyle.getTheme().getMGWTClientBundle().getTabBarCss(), display);
		mainContainer.add(new ListTabBarButton("Messages"));
		mainContainer.add(new PencilTabBarButton("Compose"));
		mainContainer.add(new GearTabBarButton("Settings"));
		mainContainer.getElement().setId("single");
		mainContainer.addSelectionHandler(new SelectionHandler<Integer>() {
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				if (event.getSelectedItem() == 1) {
					UIEntrySelectedEvent.fire(clientFactory.getEventBus(), UIEntry.COMPOSE); 
				} else if (event.getSelectedItem() == 2) {
					UIEntrySelectedEvent.fire(clientFactory.getEventBus(), UIEntry.SETTINGS); 
				} else {
					UIEntrySelectedEvent.fire(clientFactory.getEventBus(), UIEntry.LIST); 
				}
			}
		});

		activityManager.setDisplay(display);

		frameView.add(mainContainer);
		RootPanel.get().add(frameView);
	}

	private void createDoublePaneDisplay(final ClientFactory clientFactory) {
		FrameView frameView = clientFactory.getFrameView();
		SimplePanel altContainer = new SimplePanel();
		altContainer.getElement().setId("nav");
		AnimatableDisplay altDisplay = GWT.create(AnimatableDisplay.class);

		new OrientationRegionHandler(altContainer, altContainer, altDisplay);

		ActivityMapper altActivityMapper = new DoublePaneAltActivityMapper(clientFactory);

		AnimationMapper altAnimationMapper = new DoublePaneAltAnimationMapper();

		AnimatingActivityManager altActivityManager = new AnimatingActivityManager(
				altActivityMapper, altAnimationMapper,
				clientFactory.getEventBus());

		altActivityManager.setDisplay(altDisplay);

		frameView.add(altContainer);
		
		DoublePaneMainActivityMapper doublePaneMainActivityMapper = new DoublePaneMainActivityMapper(clientFactory);

		AnimationMapper tabletMainAnimationMapper = new MainAnimationMapper();

		AnimatingActivityManager mainActivityManager = new AnimatingActivityManager(
				doublePaneMainActivityMapper, tabletMainAnimationMapper,
				clientFactory.getEventBus());

		AnimatableDisplay mainDisplay = GWT.create(AnimatableDisplay.class);

		RootTabPanel mainContainer = new RootTabPanel(MGWTStyle.getTheme().getMGWTClientBundle().getTabBarCss(), mainDisplay);
		mainContainer.add(new PencilTabBarButton("Compose"));
		mainContainer.add(new GearTabBarButton("Settings"));
		mainContainer.getElement().setId("main");
		mainContainer.addSelectionHandler(new SelectionHandler<Integer>() {
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				if (event.getSelectedItem() == 1) {
					UIEntrySelectedEvent.fire(clientFactory.getEventBus(), UIEntry.SETTINGS); 
				} else {
					UIEntrySelectedEvent.fire(clientFactory.getEventBus(), UIEntry.COMPOSE); 
				}
			}
		});

		mainActivityManager.setDisplay(mainDisplay);

		frameView.add(mainContainer);
		RootPanel.get().add(frameView);

	}

}
