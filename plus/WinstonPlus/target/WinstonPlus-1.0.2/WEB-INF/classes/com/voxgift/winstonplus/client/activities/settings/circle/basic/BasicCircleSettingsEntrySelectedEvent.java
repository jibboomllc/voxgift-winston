package com.voxgift.winstonplus.client.activities.settings.circle.basic;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class BasicCircleSettingsEntrySelectedEvent extends Event<BasicCircleSettingsEntrySelectedEvent.Handler> {

	public interface Handler {
		void onAnimationSelected(BasicCircleSettingsEntrySelectedEvent event);
	}

	private static final Type<BasicCircleSettingsEntrySelectedEvent.Handler> TYPE = new Type<BasicCircleSettingsEntrySelectedEvent.Handler>();
	private final String circleId;
	private final String userId;

	public static void fire(EventBus eventBus, String circleId, String userId) {
		eventBus.fireEvent(new BasicCircleSettingsEntrySelectedEvent(circleId, userId));
	}

	public static HandlerRegistration register(EventBus eventBus, Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	@Override
	public com.google.web.bindery.event.shared.Event.Type<Handler> getAssociatedType() {
		return TYPE;
	}

	protected BasicCircleSettingsEntrySelectedEvent(String circleId, String userId) {
		this.circleId = circleId;
        this.userId = userId;
	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onAnimationSelected(this);
	}

	public static Type<BasicCircleSettingsEntrySelectedEvent.Handler> getType() {
		return TYPE;
	}

	public String getCircleId() {
		return circleId;
	}

	public String getUserId() {
		return userId;
	}
}
