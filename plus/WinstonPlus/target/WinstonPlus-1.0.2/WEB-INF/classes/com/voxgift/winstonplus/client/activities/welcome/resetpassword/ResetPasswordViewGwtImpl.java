/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.resetpassword;

import com.google.gwt.user.client.ui.HasText;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.googlecode.mgwt.ui.client.widget.FormListEntry;
import com.googlecode.mgwt.ui.client.widget.MEmailTextBox;
import com.voxgift.winstonplus.client.activities.welcome.LoginBaseViewGwtImpl;
import com.voxgift.winstonplus.client.css.CssBundle;

/**
 * @author Tom Giesberg
 * 
 */
public class ResetPasswordViewGwtImpl extends LoginBaseViewGwtImpl implements ResetPasswordView {
	
	private MEmailTextBox emailAddress;
	private Button goButton;
	
	public ResetPasswordViewGwtImpl() {
		
		container.add(controls);
		container.add(inner_container);

		emailAddress = new MEmailTextBox(CssBundle.INSTANCE.loginInputCss());
	    controls.add(new FormListEntry(CssBundle.INSTANCE.loginListCss(), "Email address", emailAddress));

		goButton = new Button();
		goButton.setText("Go");
		inner_container.add(goButton);
		
		cancelButton = new Button();
		cancelButton.setText("Cancel");
		inner_container.add(cancelButton);
	}


	@Override
	public HasText getEmailAddress() {
		return emailAddress;
	}
	
	@Override
	public HasTapHandlers getGoButton() {
		return goButton;
	}
}
