/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user.image;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class ImageUserSettingsPlace extends Place {
	public static class ImageUserSettingsPlaceTokenizer implements PlaceTokenizer<ImageUserSettingsPlace> {

		@Override
		public ImageUserSettingsPlace getPlace(String token) {
			return new ImageUserSettingsPlace();
		}

		@Override
		public String getToken(ImageUserSettingsPlace place) {
			return "";
		}

	}
}
