/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome;

import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.dialog.Dialogs;
import com.googlecode.mgwt.ui.client.dialog.Dialogs.AlertCallback;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.googlecode.mgwt.ui.client.widget.LayoutPanel;
import com.googlecode.mgwt.ui.client.widget.ProgressIndicator;
import com.googlecode.mgwt.ui.client.widget.ScrollPanel;

/**
 * @author Tom Giesberg
 * 
 */
public class LoginBaseViewGwtImpl implements LoginBaseView {

	protected LayoutPanel main;
	protected LayoutPanel container;
	protected LayoutPanel controls;
	protected LayoutPanel inner_container;
	protected LayoutPanel workingPanel;
	
	protected Label statusText;
	protected Label errorText;
	protected HTML headerIcon;
	protected Button cancelButton;
	
	public LoginBaseViewGwtImpl() {
		main = new LayoutPanel();
		main.addStyleName("login");

		ScrollPanel scrollPanel = new ScrollPanel();
		main.add(scrollPanel);

		workingPanel = new LayoutPanel();
		workingPanel.addStyleName("working");
		ProgressIndicator progressIndicator = new ProgressIndicator();
		progressIndicator.getElement().setAttribute("style", "margin:auto; margin-top: 50px");
		workingPanel.add(progressIndicator);
		workingPanel.setVisible(false);
		main.add(workingPanel);

		container = new LayoutPanel();
		container.addStyleName("container");
		
		headerIcon = new HTML();
		container.add(headerIcon);

		scrollPanel.setWidget(container);
		
		statusText = new Label();
		statusText.addStyleName("status");
		container.add(statusText);

		errorText = new Label();
		errorText.addStyleName("error");
		container.add(errorText);

		controls = new LayoutPanel();
		controls.addStyleName("controls");

		inner_container = new LayoutPanel();
		inner_container.addStyleName("inner_container");

	}

	@Override
	public HasHTML getLoginIcon() {
		return headerIcon;
	}

	@Override
	public Widget asWidget() {
		return main;
	}

	@Override
	public HasVisibility getWorking() {
		return workingPanel;
	}

	@Override
	public HasText getStatusText() {
		return statusText;
	}

	@Override
	public HasText getErrorText() {
		return errorText;
	}

	@Override
	public HasTapHandlers getCancelButton() {
		return cancelButton;
	}

	@Override
	public void alertUsingTempPassword(String title, String text, AlertCallback callback) {
		Dialogs.alert(title, text, callback);
	}

	@Override
	public void alertTempPasswordEmailed(String title, String text, AlertCallback callback) {
		Dialogs.alert(title, text, callback);
	}

	@Override
	public void alertVerificationLinkEmailed(String title, String text, AlertCallback callback) {
		Dialogs.alert(title, text, callback);
	}

	@Override
	public void alertServerCommunicationsProblem(String title, String text, AlertCallback callback) {
		Dialogs.alert(title, text, callback);
	}
}
