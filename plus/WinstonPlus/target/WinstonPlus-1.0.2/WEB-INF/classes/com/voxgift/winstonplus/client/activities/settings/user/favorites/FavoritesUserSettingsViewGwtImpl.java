/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user.favorites;

import java.util.List;

import com.googlecode.mgwt.ui.client.widget.CellList;
import com.googlecode.mgwt.ui.client.widget.ScrollPanel;
import com.googlecode.mgwt.ui.client.widget.celllist.HasCellSelectedHandler;
import com.voxgift.winstonplus.client.DecoratedViewGwtImpl;
import com.voxgift.winstonplus.client.Item;

/**
 * @author Tom Giesberg
 * 
 */
public class FavoritesUserSettingsViewGwtImpl extends DecoratedViewGwtImpl implements FavoritesUserSettingsView {

	private CellList<Item> cellListWithHeader;

	public FavoritesUserSettingsViewGwtImpl() {
		ScrollPanel scrollPanel = new ScrollPanel();
		main.add(scrollPanel);

		cellListWithHeader = new CellList<Item>(new FavoriteCell());
		cellListWithHeader.setRound(true);
		cellListWithHeader.setGroup(false);
		scrollPanel.setWidget(cellListWithHeader);
		scrollPanel.setScrollingEnabledX(false);
	}


	@Override
	public HasCellSelectedHandler getList() {
		return cellListWithHeader;
	}

	@Override
	public void renderItems(List<Item> items) {
		cellListWithHeader.render(items);

	}

	@Override
	public void setSelectedIndex(int index, boolean selected) {
		cellListWithHeader.setSelectedIndex(index, selected);

	}

}
