/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.connectcheck;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.activities.welcome.LoginBaseActivity;
import com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace;

/**
 * @author Tom Giesberg
 * 
 */
public class ConnectCheckActivity extends LoginBaseActivity {
	//private java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(getClass().getName());

	public ConnectCheckActivity(ClientFactory clientFactory) {
		super(clientFactory);
		view = clientFactory.getConnectCheckView();
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		addHandlerRegistration(((ConnectCheckView)view).getTryAgainButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				verifyServerIsReachable();
			}
		}));

		panel.setWidget(view);

		//updateHeader();
		view.getLoginIcon().setHTML("<img src='sponsors/images/login/default.png' style='padding-top: 4px;'/>");

		verifyServerIsReachable();
	}

	public void verifyServerIsReachable()
	{
		((ConnectCheckView)view).getTryAgainButtonVisibility().setVisible(false);
		((ConnectCheckView)view).getProgressIndicatorVisibility().setVisible(true);

		final AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
			public void onFailure(Throwable caught) {
				setStatusText("Error connecting to Voxgift server. Please try again.", true);
				((ConnectCheckView)view).getProgressIndicatorVisibility().setVisible(false);
				((ConnectCheckView)view).getTryAgainButtonVisibility().setVisible(true);
			}

			public void onSuccess(Boolean result) {
				((ConnectCheckView)view).getProgressIndicatorVisibility().setVisible(false);
				((ConnectCheckView)view).getTryAgainButtonVisibility().setVisible(true);
				
				if (result == null || result == false) {
					setStatusText("Error returned from Voxgift server. Please try again.", true);
				} else {
					setStatusText("Successfully communicated with Voxgift server.", false);
					clientFactory.getPlaceController().goTo(new WelcomePlace());
				}
			}
		};

		setStatusText("Checking network connection.", false);
		//LOGGER.log(java.util.logging.Level.INFO, "Connection type = " + clientFactory.getPhoneGap().getConnection().getType());
		if (clientFactory.getPhoneGap().getConnection().getType().equalsIgnoreCase("NONE")) {
			setStatusText("Please verify that you have a network connection and then try again.", true);
			((ConnectCheckView)view).getProgressIndicatorVisibility().setVisible(false);
			((ConnectCheckView)view).getTryAgainButtonVisibility().setVisible(true);			
		} else {
			setStatusText("Connecting to Voxgift server.", false);
			userSvc.ping(callback);
		}
	}
}
