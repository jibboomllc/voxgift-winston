/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.help;

import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.IsWidget;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.dialog.Dialogs.AlertCallback;

/**
 * @author Tom Giesberg
 * 
 */
public interface HelpView extends IsWidget {

	public HasHTML getTitle();

	public HasHTML getContent();
	
	public HasTapHandlers getCloseButton();
	
	public HasTapHandlers getNextButton();
	
	public HasVisibility getNextButtonVisibility();
	
	public HasTapHandlers getPreviousButton();

	public HasVisibility getPreviousButtonVisibility();

	public void refresh();

	public void alertServerCommunicationsProblem(String title, String text, AlertCallback callback);
}
