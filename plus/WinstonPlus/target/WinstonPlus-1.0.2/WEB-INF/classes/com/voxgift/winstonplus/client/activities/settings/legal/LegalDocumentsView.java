/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.legal;

import java.util.List;
import com.googlecode.mgwt.ui.client.widget.celllist.HasCellSelectedHandler;
import com.voxgift.winstonplus.client.DecoratedView;

/**
 * @author Tom Giesberg
 * 
 */
public interface LegalDocumentsView extends DecoratedView {

	public HasCellSelectedHandler getList();

	public void renderItems(List<Item> items);

	public void setSelectedIndex(int index, boolean selected);
}
