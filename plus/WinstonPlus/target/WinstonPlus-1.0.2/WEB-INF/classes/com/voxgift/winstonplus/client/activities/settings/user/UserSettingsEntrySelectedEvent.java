package com.voxgift.winstonplus.client.activities.settings.user;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class UserSettingsEntrySelectedEvent extends Event<UserSettingsEntrySelectedEvent.Handler> {

	public enum UserSettingsEntry {
		BASIC, IMAGE, FAVORITES
	}

	public interface Handler {
		void onAnimationSelected(UserSettingsEntrySelectedEvent event);
	}

	private static final Type<UserSettingsEntrySelectedEvent.Handler> TYPE = new Type<UserSettingsEntrySelectedEvent.Handler>();
	private final UserSettingsEntry entry;

	public static void fire(EventBus eventBus, UserSettingsEntry entry) {
		eventBus.fireEvent(new UserSettingsEntrySelectedEvent(entry));
	}

	public static HandlerRegistration register(EventBus eventBus, Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	@Override
	public com.google.web.bindery.event.shared.Event.Type<Handler> getAssociatedType() {
		return TYPE;
	}

	protected UserSettingsEntrySelectedEvent(UserSettingsEntry entry) {
		this.entry = entry;

	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onAnimationSelected(this);

	}

	public static Type<UserSettingsEntrySelectedEvent.Handler> getType() {
		return TYPE;
	}

	public UserSettingsEntry getEntry() {
		return entry;
	}
}
