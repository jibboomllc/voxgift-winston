/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user.basic;

import com.google.gwt.user.client.ui.HasText;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.voxgift.winstonplus.client.DecoratedView;

/**
 * @author Tom Giesberg
 * 
 */
public interface BasicUserSettingsView extends DecoratedView {

	public HasText getStatusText();

	public HasText getPassword();

	public HasText getPasswordRepeated();

	public HasText getEmailAddress();

	public HasText getFirstName();

	public HasText getLastName();

	public void addOutputLanguageItem(String item);
	public void clearOutputLanguageItems();
	public int getSelectedOutputLanguageIndex();
	public void setSelectedOutputLanguageIndex(int index, boolean selected);
	
	public void addGenderItem(String item);
	public void clearGenderItems();
	public int getSelectedGenderIndex();
	public void setSelectedGenderIndex(int index, boolean selected);
	
	public HasTapHandlers getUpdateButton();
}
