/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedEvent;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.DecoratedActivity;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent.SettingsEntry;
import com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace;
import com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace;
import com.voxgift.winstonplus.client.event.ActionEvent;
import com.voxgift.winstonplus.client.event.ActionNames;
import com.voxgift.winstonplus.data.shared.model.UserCircleDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserServiceAsync;
import com.voxgift.winstonplus.shared.AppConstants;

/**
 * @author Tom Giesberg
 * 
 */
public class SettingsActivity extends DecoratedActivity {

	private static String SETTINGS_HELP_TOPIC ="81be5cdd-0362-49b8-81f5-8824ff6e22ee";

	private SettingsView view;
	private int oldIndex;
	private List<Item> items;

	private RpcUserServiceAsync userSvc = GWT.create(RpcUserService.class);

	public SettingsActivity(ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getSettingsView());

		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) userSvc, appConstants.webAppUrl(), "rpc/user");

		view = getClientFactory().getSettingsView();
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		getClientFactory().getFrameView().showBackbutton(false);
		getClientFactory().getFrameView().getTitle().setText("Settings");
		panel.setWidget(view);

		view.getLoggedInAs().setText(getClientFactory().getUserEmail());
		AppConstants appConstants = GWT.create(AppConstants.class);
		view.getVersion().setText(appConstants.buildVersion());

		refreshData();
	}

	@Override
	public void onDataReady() {
		super.onDataReady();

		items = createItems();
		view.renderItems(items);

		addHandlerRegistration(view.getList().addCellSelectedHandler(new CellSelectedHandler() {

			@Override
			public void onCellSelected(CellSelectedEvent event) {
				int index = event.getIndex();

				view.setSelectedIndex(oldIndex, false);
				view.setSelectedIndex(index, true);
				oldIndex = index;

				SettingsEntrySelectedEvent.fire(getEventBus(), items.get(index).getEntry());

			}
		}));


		view.clearCircleItems();
		if (getClientFactory().getUser().getUserCircles() != null && getClientFactory().getUser().getUserCircles().size() > 0) {
			int index = 0;
			int selectedIndex = 0;
			for (UserCircleDetails circle : getClientFactory().getUser().getUserCircles().values()) {
				view.addCircleItem(circle.getCircleName(), circle.getCircleId());
				if (circle.getCircleId().equals(getClientFactory().getCircleId())) {
					selectedIndex = index;
				}
				index++;
			}
			view.setSelectedCircleIndex(selectedIndex, true);
		}

		addHandlerRegistration(view.getCircleList().addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {
				updateCircle();
			}
		}));

		addHandlerRegistration(view.getLogoutButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				logoutUser();
			}
		}));

		addHandlerRegistration(view.getHelpButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				getClientFactory().getPlaceController().goTo(new HelpPlace(SETTINGS_HELP_TOPIC));
			}
		}));
	}

	private void refreshData() {
		final AsyncCallback<Map<String, UserCircleDetails>> callbackGetUserCircles = new AsyncCallback<Map<String, UserCircleDetails>>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
				onDataReady();
			}

			public void onSuccess(Map<String, UserCircleDetails> result) {
				if (result != null) {
					getClientFactory().getUser().setUserCircles(result);
					if (result.size() > 0) {
						if (getClientFactory().getCircleId() == null || !result.containsKey(getClientFactory().getCircleId())) {
							Map.Entry<String, UserCircleDetails> entry = result.entrySet().iterator().next();
							getClientFactory().setCircleId(entry.getKey());
						}
						getClientFactory().setCircle(result.get(getClientFactory().getCircleId()));
					} else {
						getClientFactory().setCircle(null);
					}

					ActionEvent.fire(getEventBus(), ActionNames.CIRCLE_CHOSEN);
				}
				onDataReady();
			}};
			if (serverIsReachable()) {
				userSvc.getUsersCircleSubscriptions(getClientFactory().getUser().getId(), callbackGetUserCircles);
			}
	}

	/**
	 * @return
	 */
	private List<Item> createItems() {
		ArrayList<Item> list = new ArrayList<Item>();
		list.add(new Item("User Settings", SettingsEntry.USER));
		list.add(new Item("Circle Settings", SettingsEntry.CIRCLE));
		list.add(new Item("Legal Documents", SettingsEntry.LEGAL));
		return list;
	}

	private void updateCircle() {
		getClientFactory().setCircleId(view.getSelectedCircleValue());
		getClientFactory().setCircle(getClientFactory().getUser().getUserCircles().get(view.getSelectedCircleValue()));
		ActionEvent.fire(getEventBus(), ActionNames.CIRCLE_CHOSEN); 
	}

	private void logoutUser() {
		getClientFactory().getPlaceController().goTo(new WelcomePlace());
		ActionEvent.fire(getEventBus(), ActionNames.LOGOUT); 
	}
}
