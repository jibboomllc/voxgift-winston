package com.voxgift.winstonplus.client.activities.compose;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.event.logical.shared.HasSelectionHandlers;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.user.client.ui.HasText;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.widget.Carousel;
import com.googlecode.mgwt.ui.client.widget.CellList;
import com.googlecode.mgwt.ui.client.widget.LayoutPanel;
import com.googlecode.mgwt.ui.client.widget.MTextArea;
import com.googlecode.mgwt.ui.client.widget.RoundPanel;
import com.googlecode.mgwt.ui.client.widget.ScrollPanel;
import com.googlecode.mgwt.ui.client.widget.celllist.HasCellSelectedHandler;
import com.voxgift.winstonplus.client.DecoratedViewGwtImpl;
import com.voxgift.winstonplus.client.widget.actionbar.ActionBarButton;
import com.voxgift.winstonplus.client.widget.actionbar.DeleteButton;
import com.voxgift.winstonplus.client.widget.actionbar.FavoritesAddButton;
import com.voxgift.winstonplus.client.widget.actionbar.SendButton;
import com.voxgift.winstonplus.client.widget.actionbar.SpeakButton;
import com.voxgift.winstonplus.data.shared.model.DictionaryDetails;
import com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails;

/**
 * @author Tom Giesberg
 * 
 */
public class ComposeViewGwtImpl extends DecoratedViewGwtImpl implements ComposeView {

	private MTextArea inputText;
	private Carousel dictionaryCarousel;
	private List<CellList<DictionaryEntryDetails>> cellLists;
	private ActionBarButton clearButton;
	private ActionBarButton addFavoriteButton;
	private ActionBarButton talkButton;
	private ActionBarButton postButton;
	private ActionBarButton pageButton;

	public ComposeViewGwtImpl() {
		
		final RoundPanel topPanel = new RoundPanel();
		main.add(topPanel);

		topPanel.add(statusText);

		inputText = new MTextArea();
		inputText.setVisibleLines(3);
		inputText.setPlaceHolder("Enter text");
		topPanel.add(inputText);

		final LayoutPanel buttonPanel = new LayoutPanel();
		buttonPanel.setHorizontal(true);

		talkButton = new SpeakButton();
		talkButton.setText("Speak");
		buttonPanel.add(talkButton);

		postButton = new SendButton();
		postButton.setText("Send");
		buttonPanel.add(postButton);

		addFavoriteButton = new FavoritesAddButton();
		addFavoriteButton.setText("Save");
		buttonPanel.add(addFavoriteButton);

		clearButton = new DeleteButton();
		clearButton.setText("Clear");
		buttonPanel.add(clearButton);
				
		topPanel.add(buttonPanel);

		// Bottom panel

		final LayoutPanel bottomPanel = new LayoutPanel();
		bottomPanel.asWidget().getElement().setId("bottomPanel");
		bottomPanel.addStyleName("box_flex_1");
		main.add(bottomPanel);

		dictionaryCarousel = new Carousel();
		dictionaryCarousel.asWidget().getElement().setId("dictionaryCarousel");
		dictionaryCarousel.addStyleName("box_flex_1");
		bottomPanel.add(dictionaryCarousel);

		cellLists = new ArrayList<CellList<DictionaryEntryDetails>>();	
	}

	@Override
	public void renderDictionaries(List<DictionaryDetails> dictionaries) {

		dictionaryCarousel.clear();
		cellLists.clear();
		
		for (DictionaryDetails dictionary : dictionaries)
		{
			ScrollPanel scrollPanel = new ScrollPanel();
			scrollPanel.setScrollingEnabledX(false);

			LayoutPanel flowPanel3 = new LayoutPanel();

			CellList<DictionaryEntryDetails> cellList;	
			cellList = new CellList<DictionaryEntryDetails>(new DictionaryEntryCell());
			cellLists.add(cellList);
			cellList.setGroup(false);
			cellList.render(dictionary.getEntries());
			flowPanel3.add(cellList);
			scrollPanel.setWidget(flowPanel3);
			scrollPanel.refresh();

			dictionaryCarousel.add(scrollPanel);
		}

		dictionaryCarousel.refresh();

	}

	@Override
	public HasCellSelectedHandler getList(int listIndex) {
		return cellLists.get(listIndex);
	}

	@Override
	public void setSelectedIndex(int listIndex, int index, boolean selected) {
		if (listIndex >= 0 && index >= 0 && cellLists.get(listIndex) != null) {
			cellLists.get(listIndex).setSelectedIndex(index, selected);
		}
	}

	@Override
	public HasSelectionHandlers<Integer> getDictionaryCarousel() {
		return dictionaryCarousel;
	}

	@Override
	public HasText getInputText() {
		return inputText;
	}

	@Override
	public HasKeyUpHandlers getCurrentInput() {
		return inputText;
	}
	
	@Override
	public HasValueChangeHandlers<String> getInputEditor() {
		return inputText;
	}
	
	@Override
	public HasTapHandlers getClearButton() {
		return clearButton;
	}

	@Override
	public HasTapHandlers getAddFavoriteButton() {
		return addFavoriteButton;
	}

	@Override
	public HasTapHandlers getTalkButton() {
		return talkButton;
	}

	@Override
	public HasTapHandlers getPostButton() {
		return postButton;
	}

	@Override
	public HasTapHandlers getPageButton() {
		return pageButton;
	}

}
