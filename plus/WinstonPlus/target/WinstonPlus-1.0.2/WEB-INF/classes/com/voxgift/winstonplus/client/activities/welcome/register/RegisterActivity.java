/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.register;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.activities.welcome.LoginBaseActivity;

/**
 * @author Tom Giesberg
 * 
 */
public class RegisterActivity extends LoginBaseActivity {
	//private java.util.logging.Logger LOGGER = Logger.getLogger(getClass().getName());

	public RegisterActivity(ClientFactory clientFactory) {
		super(clientFactory);
		view = clientFactory.getRegisterView();
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		addHandlerRegistration(((RegisterView)view).getGoButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
					registerUser(((RegisterView)view).getEmailAddress().getText().toLowerCase().trim()
							, ((RegisterView)view).getPassword().getText()
							, ((RegisterView)view).getPasswordRepeated().getText());
			}
			}));
		panel.setWidget(view);

		updateHeader();

		if (clientFactory.getUserEmail() != null && !clientFactory.getUserEmail().isEmpty())  {
			((RegisterView)view).getEmailAddress().setText(clientFactory.getUserEmail());
		}
	}
}
