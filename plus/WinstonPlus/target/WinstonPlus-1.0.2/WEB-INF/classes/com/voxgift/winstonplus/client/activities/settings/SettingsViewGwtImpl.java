/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings;

import java.util.List;

import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.googlecode.mgwt.ui.client.widget.CellList;
import com.googlecode.mgwt.ui.client.widget.FormListEntry;
import com.googlecode.mgwt.ui.client.widget.LayoutPanel;
import com.googlecode.mgwt.ui.client.widget.MListBox;
import com.googlecode.mgwt.ui.client.widget.ScrollPanel;
import com.googlecode.mgwt.ui.client.widget.WidgetList;
import com.googlecode.mgwt.ui.client.widget.celllist.BasicCell;
import com.googlecode.mgwt.ui.client.widget.celllist.HasCellSelectedHandler;
import com.voxgift.winstonplus.client.DecoratedViewGwtImpl;

/**
 * @author Tom Giesberg
 * 
 */
public class SettingsViewGwtImpl extends DecoratedViewGwtImpl implements SettingsView {

	private CellList<Item> cellListWithHeader;

	private MListBox circle;

	private Button logoutButton;
	
	private HTML loggedInAs;
	
	private Label version;

	private Button helpButton;

	public SettingsViewGwtImpl() {
		ScrollPanel scrollPanel = new ScrollPanel();
		main.add(scrollPanel);

		LayoutPanel container = new LayoutPanel();
		
		cellListWithHeader = new CellList<Item>(new BasicCell<Item>() {

			@Override
			public String getDisplayString(Item model) {
				return model.getDisplayString();
			}

			@Override
			public boolean canBeSelected(Item model) {
				return true;
			}
		});
		cellListWithHeader.setRound(true);
		container.add(cellListWithHeader);

		WidgetList widgetList = new WidgetList();
		widgetList.setRound(true);

		circle = new MListBox();
		LayoutPanel indicatorContainer = new LayoutPanel();
		indicatorContainer.setHorizontal(true);
		indicatorContainer.add(circle);
		LayoutPanel indicator = new LayoutPanel();
		indicator.setStyleName("sub-menu-indicator");
		indicatorContainer.add(indicator);
		widgetList.add(new FormListEntry("Selected Circle", indicatorContainer));

		LayoutPanel logoutPanelContainer = new LayoutPanel();
		LayoutPanel logoutPanel = new LayoutPanel();
		//logoutPanel.setHorizontal(true);
		loggedInAs = new HTML();
		logoutPanel.add(loggedInAs);
		logoutButton = new Button();
		logoutButton.setText("Logout");
		logoutPanel.add(logoutButton);
		logoutPanelContainer.add(logoutPanel);
		widgetList.add(new FormListEntry("Logged In As", logoutPanelContainer));

		LayoutPanel versionPanel = new LayoutPanel();
		//versionPanel.setHorizontal(true);
		version = new Label();
		versionPanel.add(version);
		helpButton = new Button();
		helpButton.setText("Help");
		versionPanel.add(helpButton);
		widgetList.add(new FormListEntry("Version", versionPanel));

		container.add(widgetList);

		scrollPanel.setWidget(container);
		scrollPanel.setScrollingEnabledX(false);
		
		// workaround for android formfields jumping around when using
	    // -webkit-transform
	    scrollPanel.setUsePos(MGWT.getOsDetection().isAndroid());
	}


	@Override
	public HasCellSelectedHandler getList() {
		return cellListWithHeader;
	}

	@Override
	public void renderItems(List<Item> items) {
		cellListWithHeader.render(items);

	}

	@Override
	public void setSelectedIndex(int index, boolean selected) {
		cellListWithHeader.setSelectedIndex(index, selected);

	}

	@Override
	public void addCircleItem(String item, String value) {
		circle.addItem(item, value);
	}

	@Override
	public void clearCircleItems() {
		circle.clear();
	}

	@Override
	public String getSelectedCircleValue() {
		return circle.getValue(circle.getSelectedIndex());
	}

	@Override
	public int getSelectedCircleIndex() {
		return circle.getSelectedIndex();
	}

	@Override
	public void setSelectedCircleIndex(int index, boolean selected) {
		circle.setItemSelected(index, selected);
	}

	@Override
	public HasChangeHandlers getCircleList() {
		return circle;
	}

	@Override
	public HasTapHandlers getLogoutButton() {
		return logoutButton;
	}

	@Override
	public HasText getLoggedInAs() {
		return loggedInAs;
	}

	@Override
	public HasText getVersion() {
		return version;
	}

	@Override
	public HasTapHandlers getHelpButton() {
		return helpButton;
	}

}
