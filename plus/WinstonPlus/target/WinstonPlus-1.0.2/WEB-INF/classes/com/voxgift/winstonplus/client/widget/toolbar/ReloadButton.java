package com.voxgift.winstonplus.client.widget.toolbar;

import com.googlecode.mgwt.ui.client.MGWTStyle;
import com.googlecode.mgwt.ui.client.widget.buttonbar.ButtonBarButtonBase;
import com.voxgift.winstonplus.client.theme.ExtendedClientBundle;

public class ReloadButton extends ButtonBarButtonBase {

	public ReloadButton() {
		super(((ExtendedClientBundle) MGWTStyle.getTheme().getMGWTClientBundle()).getButtonBarReloadImage());
	}

}
