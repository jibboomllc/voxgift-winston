
package com.voxgift.winstonplus.client;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;

import com.voxgift.winstonplus.client.activities.compose.ComposeActivity;
import com.voxgift.winstonplus.client.activities.compose.ComposePlace;
import com.voxgift.winstonplus.client.activities.messagelist.MessageListActivity;
import com.voxgift.winstonplus.client.activities.settings.SettingsActivity;
import com.voxgift.winstonplus.client.activities.settings.SettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsActivity;
import com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsActivity;
import com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsActivity;
import com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsActivity;
import com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsPlace;
import com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentActivity;
import com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentPlace;
import com.voxgift.winstonplus.client.activities.settings.user.UserSettingsActivity;
import com.voxgift.winstonplus.client.activities.settings.user.UserSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsActivity;
import com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsActivity;
import com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsPlace;
import com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsActivity;
import com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsPlace;
import com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckPlace;
import com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace;
import com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace;
import com.voxgift.winstonplus.client.activities.welcome.register.RegisterPlace;
import com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordPlace;
import com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace;
import com.voxgift.winstonplus.client.places.HomePlace;

/**
 * 
 */
public class SinglePaneActivityMapper implements ActivityMapper {

	private final ClientFactory clientFactory;

	public SinglePaneActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	private ComposeActivity composeActivity;

	private ComposeActivity getComposeActivity() {
		if (composeActivity == null) {
			composeActivity = new ComposeActivity(clientFactory);
		}

		return composeActivity;
	}

	private SettingsActivity settingsActivity;

	private SettingsActivity getSettingsActivity() {
		if (settingsActivity == null) {
			settingsActivity = new SettingsActivity(clientFactory);
		}

		return settingsActivity;
	}

	private UserSettingsActivity userSettingsActivity;

	private UserSettingsActivity getUserSettingsActivity() {
		if (userSettingsActivity == null) {
			userSettingsActivity = new UserSettingsActivity(clientFactory);
		}

		return userSettingsActivity;
	}

	private BasicUserSettingsActivity basicUserSettingsActivity;

	private BasicUserSettingsActivity getBasicUserSettingsActivity() {
		if (basicUserSettingsActivity == null) {
			basicUserSettingsActivity = new BasicUserSettingsActivity(clientFactory);
		}

		return basicUserSettingsActivity;
	}

	private ImageUserSettingsActivity imageUserSettingsActivity;

	private ImageUserSettingsActivity getImageUserSettingsActivity() {
		if (imageUserSettingsActivity == null) {
			imageUserSettingsActivity = new ImageUserSettingsActivity(clientFactory);
		}

		return imageUserSettingsActivity;
	}

	private FavoritesUserSettingsActivity favoritesUserSettingsActivity;

	private FavoritesUserSettingsActivity getFavoritesUserSettingsActivity() {
		if (favoritesUserSettingsActivity == null) {
			favoritesUserSettingsActivity = new FavoritesUserSettingsActivity(clientFactory);
		}

		return favoritesUserSettingsActivity;
	}

	private CircleSettingsActivity circleSettingsActivity;

	private CircleSettingsActivity getCircleSettingsActivity() {
		if (circleSettingsActivity == null) {
			circleSettingsActivity = new CircleSettingsActivity(clientFactory);
		}

		return circleSettingsActivity;
	}

	private BasicCircleSettingsActivity basicCircleSettingsActivity;

	private BasicCircleSettingsActivity getBasicCircleSettingsActivity(BasicCircleSettingsPlace place) {
		if (basicCircleSettingsActivity == null) {
			basicCircleSettingsActivity = new BasicCircleSettingsActivity(clientFactory);
		}

		basicCircleSettingsActivity.setCircleId(place.getCircleId());

		return basicCircleSettingsActivity;
	}

	private UserCircleSettingsActivity userCircleSettingsActivity;

	private UserCircleSettingsActivity getUserCircleSettingsActivity(UserCircleSettingsPlace place) {
		if (userCircleSettingsActivity == null) {
			userCircleSettingsActivity = new UserCircleSettingsActivity(clientFactory);
		}

		userCircleSettingsActivity.setCircleId(place.getCircleId());
		userCircleSettingsActivity.setUserId(place.getUserId());

		return userCircleSettingsActivity;
	}

	private LegalDocumentsActivity legalDocumentsActivity;

	private LegalDocumentsActivity getLegalDocumentsActivity() {
		if (legalDocumentsActivity == null) {
			legalDocumentsActivity = new LegalDocumentsActivity(clientFactory);
		}

		return legalDocumentsActivity;
	}

	private LegalDocumentActivity legalDocumentActivity;

	private LegalDocumentActivity getLegalDocumentActivity(LegalDocumentPlace place) {
		if (legalDocumentActivity == null) {
			legalDocumentActivity = new LegalDocumentActivity(clientFactory);
		}

		legalDocumentActivity.setTitle(place.getTitle());
		legalDocumentActivity.setId(place.getUrl());

		return legalDocumentActivity;
	}

	@Override
	public Activity getActivity(Place newPlace) {
		if (newPlace instanceof HomePlace) {
			return new MessageListActivity(clientFactory);
		}

		if (newPlace instanceof ConnectCheckPlace || newPlace instanceof WelcomePlace 
				|| newPlace instanceof RegisterPlace || newPlace instanceof LoginPlace 
				|| newPlace instanceof ResetPasswordPlace || newPlace instanceof HelpPlace) {
			return null;
		}

		if (newPlace instanceof ComposePlace) {
			return getComposeActivity();
		}

		if (newPlace instanceof SettingsPlace) {
			return getSettingsActivity();
		}

		if (newPlace instanceof UserSettingsPlace) {
			return getUserSettingsActivity();
		}

		if (newPlace instanceof BasicUserSettingsPlace) {
			return getBasicUserSettingsActivity();
		}

		if (newPlace instanceof ImageUserSettingsPlace) {
			return getImageUserSettingsActivity();
		}

		if (newPlace instanceof FavoritesUserSettingsPlace) {
			return getFavoritesUserSettingsActivity();
		}

		if (newPlace instanceof CircleSettingsPlace) {
			return getCircleSettingsActivity();
		}

		if (newPlace instanceof BasicCircleSettingsPlace) {
			return getBasicCircleSettingsActivity((BasicCircleSettingsPlace) newPlace);
		}

		if (newPlace instanceof UserCircleSettingsPlace) {
			return getUserCircleSettingsActivity((UserCircleSettingsPlace) newPlace);
		}

		if (newPlace instanceof LegalDocumentsPlace) {
			return getLegalDocumentsActivity();
		}

		if (newPlace instanceof LegalDocumentPlace) {
			return getLegalDocumentActivity((LegalDocumentPlace) newPlace);
		}

		return new MessageListActivity(clientFactory);
	}
}
