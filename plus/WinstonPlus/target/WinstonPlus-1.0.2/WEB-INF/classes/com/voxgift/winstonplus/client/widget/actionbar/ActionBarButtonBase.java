package com.voxgift.winstonplus.client.widget.actionbar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.widget.base.ButtonBase;
import com.voxgift.winstonplus.client.css.ActionBarCss;

/**
 * The base class for all action bar buttonss
 * 
 * @author Daniel Kurka
 */

public class ActionBarButtonBase extends ButtonBase {

	protected final ActionBarButtonStylerHandler actionBarButtonStylerHandler = GWT.create(ActionBarButtonStylerHandlerWebkit.class);

	/**
	 * 
	 * @author Daniel Kurka
	 * 
	 */
	public interface ActionBarButtonStylerHandler {
		/**
		 * set an image as background
		 * 
		 * @param element the element to set the image on
		 * @param imageResource the image to use
		 */
		public void applyImage(Element element, ImageResource imageResource);
	}

	// used by deferred binding...
//	@SuppressWarnings("unused")
	private static class ActionBarButtonStylerHandlerWebkit implements ActionBarButtonStylerHandler {

		@Override
		public void applyImage(Element element, ImageResource imageResource) {
			if (imageResource == null)
				return;
			if (MGWT.getOsDetection().isAndroid()) {
				element.getStyle().setProperty("backgroundColor", "transparent");
				element.getStyle().setProperty("backgroundImage", "url(" + imageResource.getSafeUri().asString() + ")");
				element.getStyle().setProperty("backgroundSize", "100%");
			} else {
				element.getStyle().setProperty("WebkitMaskBoxImage", "url(" + imageResource.getSafeUri().asString() + ")");
			}
			//element.getStyle().setProperty("WebkitMaskBoxImage", "url(" + imageResource.getSafeUri().asString() + ")");
			element.getStyle().setHeight(imageResource.getHeight(), Unit.PX);
			element.getStyle().setWidth(imageResource.getWidth(), Unit.PX);
		}

	}

	@SuppressWarnings("unused")
	private static class ActionBarButtonStylerHandlerBackground implements ActionBarButtonStylerHandler {

		@Override
		public void applyImage(Element element, ImageResource imageResource) {
			if (imageResource == null)
				return;
			element.getStyle().setBackgroundImage("url(" + imageResource.getSafeUri().asString() + ")");
			element.getStyle().setHeight(imageResource.getHeight(), Unit.PX);
			element.getStyle().setWidth(imageResource.getWidth(), Unit.PX);
		}

	}

	protected final ActionBarCss css;
	protected Element icon;
	protected Element text;
	protected final ImageResource imageResource;

	/**
	 * Construct
	 * 
	 * @param css
	 * @param imageResource
	 */
	public ActionBarButtonBase(ActionBarCss css, ImageResource imageResource) {
		super(css);
		this.css = css;
		this.imageResource = imageResource;
		addStyleName(css.button());

		icon = DOM.createDiv();
		icon.addClassName(css.icon());
		getElement().appendChild(icon);

		text = DOM.createDiv();
		text.addClassName(css.text());
		getElement().appendChild(text);

		actionBarButtonStylerHandler.applyImage(icon, imageResource);
	}

	/**
	 * <p>
	 * setSelected
	 * </p>
	 * 
	 * @param selected a boolean.
	 */
	public void setSelected(boolean selected) {
		if (selected) {
			addStyleName(css.selected());
		} else {
			removeStyleName(css.selected());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getText() {
		return icon.getInnerText();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setText(String newText) {
		text.setInnerText(newText);
	}

}
