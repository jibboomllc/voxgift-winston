package com.voxgift.winstonplus.client.css;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.resources.client.ClientBundle;

public interface CssBundle extends ClientBundle {

	public static final CssBundle INSTANCE =  GWT.create(CssBundle.class);

	@Source("actionBar.css")
	public ActionBarCss actionBarCss();

	@Source("loginInput.css")
	public LoginInputCss loginInputCss();

	@Source("loginList.css")
	public LoginListCss loginListCss();

}
