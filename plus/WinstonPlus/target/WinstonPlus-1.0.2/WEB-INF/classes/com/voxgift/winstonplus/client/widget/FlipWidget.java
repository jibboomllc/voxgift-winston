/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.widget;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.Widget;

/**
 * @author Tom Giesberg
 * 
 */
public class FlipWidget extends Composite implements HasVisibility {

	private Widget primaryWidget;
	private Widget secondaryWidget;
	
	public FlipWidget(Widget primaryWidget, Widget secondaryWidget) {
		HorizontalPanel panel = new HorizontalPanel();
		this.primaryWidget = primaryWidget;
		panel.add(primaryWidget);
		this.secondaryWidget = secondaryWidget;
		panel.add(secondaryWidget);
		initWidget(panel);
	}
	
	@Override
	public boolean isVisible() {
		return primaryWidget.isVisible();
	}

	@Override
	public void setVisible(boolean visible) {
		primaryWidget.setVisible(visible);
		secondaryWidget.setVisible(!visible);
	}
	
}

