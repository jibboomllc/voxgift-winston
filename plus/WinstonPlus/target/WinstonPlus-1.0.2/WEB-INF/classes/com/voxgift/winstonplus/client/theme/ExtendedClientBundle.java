package com.voxgift.winstonplus.client.theme;

import com.google.gwt.resources.client.ImageResource;
import com.googlecode.mgwt.ui.client.theme.MGWTClientBundle;

public interface ExtendedClientBundle extends MGWTClientBundle {

	public ImageResource tabBarCommentPlusImage();

	public ImageResource tabBarDeleteImage();

	public ImageResource tabBarSpeakerImage();

	public ImageResource tabBarStarImage();

	public ImageResource tabBarGearImage();

	public ImageResource tabBarListImage();

	public ImageResource tabBarPencilImage();

	
	public ImageResource getButtonBarCommentImage();

	public ImageResource getButtonBarDeleteImage();

	public ImageResource getButtonBarFavoritesAddImage();

	public ImageResource getButtonBarGearImage();

	public ImageResource getButtonBarListImage();

	public ImageResource getButtonBarMedImage();

	public ImageResource getButtonBarPencilImage();

	public ImageResource getButtonBarReloadImage();

	public ImageResource getButtonBarSpeakerImage();

	public ImageResource getButtonBarStarImage();
}
