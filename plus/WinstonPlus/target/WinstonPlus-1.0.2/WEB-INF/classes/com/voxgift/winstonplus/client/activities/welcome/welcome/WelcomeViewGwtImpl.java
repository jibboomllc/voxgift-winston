/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.welcome;

import com.google.gwt.user.client.ui.Label;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.voxgift.winstonplus.client.activities.welcome.LoginBaseViewGwtImpl;

/**
 * @author Tom Giesberg
 * 
 */
public class WelcomeViewGwtImpl extends LoginBaseViewGwtImpl implements WelcomeView {

	private Label registerPrompt;
	private Label loginPrompt;
	private Label resetPasswordPrompt;
	private Button registerButton;
	private Button loginButton;
	private Button resetPasswordButton;
	
	public WelcomeViewGwtImpl() {
		super();

		container.add(inner_container);

		registerPrompt = new Label();
		registerPrompt.addStyleName("prompt");
		registerPrompt.setText("Don't have an account?");
		inner_container.add(registerPrompt);
		registerButton = new Button();
		registerButton.setText("Sign up");
		inner_container.add(registerButton);

		loginPrompt = new Label();
		loginPrompt.addStyleName("prompt");
		loginPrompt.setText("Already have an account?");
		inner_container.add(loginPrompt);
		loginButton = new Button();
		loginButton.setText("Login");
		inner_container.add(loginButton);

		resetPasswordPrompt = new Label();
		resetPasswordPrompt.addStyleName("prompt");
		resetPasswordPrompt.setText("Forgot your password?");
		inner_container.add(resetPasswordPrompt);
		resetPasswordButton = new Button();
		resetPasswordButton.setText("Reset password");
		inner_container.add(resetPasswordButton);

	}

	@Override
	public HasTapHandlers getRegisterButton() {
		return registerButton;
	}


	@Override
	public HasTapHandlers getLoginButton() {
		return loginButton;
	}


	@Override
	public HasTapHandlers getResetPasswordButton() {
		return resetPasswordButton;
	}
}
