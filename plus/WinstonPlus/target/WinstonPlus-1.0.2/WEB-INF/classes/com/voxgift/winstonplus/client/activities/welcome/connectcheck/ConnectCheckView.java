/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.connectcheck;

import com.google.gwt.user.client.ui.HasVisibility;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.voxgift.winstonplus.client.activities.welcome.LoginBaseView;

/**
 * @author Tom Giesberg
 * 
 */
public interface ConnectCheckView extends LoginBaseView {

	public HasTapHandlers getTryAgainButton();
	
	public HasVisibility getTryAgainButtonVisibility();
	
	public HasVisibility getProgressIndicatorVisibility();
}
