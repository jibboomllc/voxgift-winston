package com.voxgift.winstonplus.client.css;

import com.googlecode.mgwt.ui.client.theme.base.ButtonBaseCss;

public interface ActionBarCss extends ButtonBaseCss {

	@ClassName("vg-ActionPanel")
	public String actionPanel();

	@ClassName("vg-ActionPanel-container")
	public String actionPanelContainer();

	@ClassName("vg-ActionBar")
	public String actionbar();

	@ClassName("vg-ActionBar-Button")
	public String button();

	@ClassName("vg-ActionBar-Button-selected")
	public String selected();

	@ClassName("vg-ActionBar-Button-active")
	public String active();

	@ClassName("vg-ActionBar-Button-icon")
	public String icon();

	@ClassName("vg-ActionBar-Button-text")
	public String text();

}
