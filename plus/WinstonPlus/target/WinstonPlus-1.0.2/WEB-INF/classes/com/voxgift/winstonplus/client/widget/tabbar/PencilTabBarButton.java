package com.voxgift.winstonplus.client.widget.tabbar;

import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.MGWTStyle;
import com.googlecode.mgwt.ui.client.theme.base.TabBarCss;
import com.googlecode.mgwt.ui.client.widget.tabbar.TabBarButton;
import com.voxgift.winstonplus.client.theme.ExtendedClientBundle;

public class PencilTabBarButton extends TabBarButton {

	public PencilTabBarButton() {
		this(MGWTStyle.getTheme().getMGWTClientBundle().getTabBarCss(), "Type");
	}

	public PencilTabBarButton(String text) {
		this(MGWTStyle.getTheme().getMGWTClientBundle().getTabBarCss(), text);
	}

    public PencilTabBarButton(TabBarCss css, String text) {
	    super(css, MGWT.getOsDetection().isIOs() || MGWT.getOsDetection().isDesktop() ? 
	    	((ExtendedClientBundle) MGWTStyle.getTheme().getMGWTClientBundle()).tabBarPencilImage() : null);
			setText(text);
	}
}
