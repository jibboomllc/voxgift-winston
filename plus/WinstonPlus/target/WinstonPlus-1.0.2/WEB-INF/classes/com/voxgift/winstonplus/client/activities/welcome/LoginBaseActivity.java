/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome;

import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.safehtml.shared.UriUtils;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.gwtphonegap.client.file.DirectoryEntry;
import com.googlecode.gwtphonegap.client.file.EntryBase;
import com.googlecode.gwtphonegap.client.file.FileCallback;
import com.googlecode.gwtphonegap.client.file.FileEntry;
import com.googlecode.gwtphonegap.client.file.FileError;
import com.googlecode.gwtphonegap.client.file.FileReader;
import com.googlecode.gwtphonegap.client.file.FileSystem;
import com.googlecode.gwtphonegap.client.file.Flags;
import com.googlecode.gwtphonegap.client.file.ReaderCallback;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;
import com.googlecode.mgwt.ui.client.MGWT;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace;
import com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace;
import com.voxgift.winstonplus.client.event.ActionEvent;
import com.voxgift.winstonplus.client.event.ActionNames;
import com.voxgift.winstonplus.client.places.HomePlace;
import com.voxgift.winstonplus.data.shared.model.PushServiceType;
import com.voxgift.winstonplus.data.shared.model.UserCircleDetails;
import com.voxgift.winstonplus.data.shared.model.UserDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserServiceAsync;
import com.voxgift.winstonplus.shared.AppConstants;

/**
 * @author Tom Giesberg
 * 
 */
public class LoginBaseActivity extends MGWTAbstractActivity {
	//private java.util.logging.Logger LOGGER = Logger.getLogger(getClass().getName());

	private static String LOGIN_HELP_TOPIC ="f2191d3f-b9d9-4ae5-98b7-b445de932342";

	protected LoginBaseView view;
	protected ClientFactory clientFactory;
	protected RpcUserServiceAsync userSvc = GWT.create(RpcUserService.class);

	public LoginBaseActivity(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) userSvc, appConstants.webAppUrl(), "rpc/user");
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		view.getWorking().setVisible(false);
		setStatusText(null, false);

		addHandlerRegistration(ActionEvent.register(eventBus, ActionNames.CIRCLE_CHOSEN, new ActionEvent.Handler() {

			@Override
			public void onAction(ActionEvent event) {
				updateHeader();
			}
		}));

		if (view.getCancelButton() != null) {
			addHandlerRegistration(view.getCancelButton().addTapHandler(new TapHandler() {

				@Override
				public void onTap(TapEvent event) {
					clientFactory.getPlaceController().goTo(new WelcomePlace());
				}
			}));
		}
	}

	public void updateHeader() {
		AppConstants appConstants = GWT.create(AppConstants.class);
		String style = " style='padding-top: 4px;'";
		if (clientFactory.getCircle() != null && clientFactory.getCircle().getCircleId() !=  null
				&& clientFactory.getCircle().getSponsorId() !=  null) {
			view.getLoginIcon().setHTML("<img src='" 
					+ UriUtils.fromSafeConstant(appConstants.resourcesUrl() + "sponsors/images/login/" 
							+ clientFactory.getCircle().getSponsorId() + ".png").asString() + "'" + style + "/>");
		} else {
			view.getLoginIcon().setHTML("<img src='" 
					+ UriUtils.fromSafeConstant(appConstants.resourcesUrl() + "sponsors/images/login/default.png").asString() + "'" + style + "/>");
		}
	}

	public static native void closeBrowser()
	/*-{
        $wnd.close();
    }-*/;

	public void setStatusText(String text, boolean isError) {
		if (view.getStatusText() != null && view.getErrorText() != null) {
			if (isError) {
				view.getStatusText().setText(null);
				view.getErrorText().setText(text);
			} else {
				view.getStatusText().setText(text);
				view.getErrorText().setText(null);
			}
		}
	}

	protected void loginSession(final String sid) {

		if (sid != null) {
			final AsyncCallback<String> callback = new AsyncCallback<String>() {
				public void onFailure(Throwable caught) {
					checkServerCallIssue(caught);
					view.getWorking().setVisible(false);
				}

				public void onSuccess(String result) {
					//LOGGER.log(java.util.logging.Level.INFO, "loginSession returned: " + result);

					if (result == null) {
						view.getWorking().setVisible(false);

						if (clientFactory.getUser() != null) {
							logoutUser();
						}
					} else {
						getUserDetails(sid, false);
					}
				}
			};

			view.getWorking().setVisible(true);
			//LOGGER.log(java.util.logging.Level.INFO, "loginFromSession sid: " + sid);
			if (serverIsReachable()) {
				userSvc.loginFromSession(sid, callback);
			} else {
				view.getWorking().setVisible(false);
			}

		} else {
			view.getWorking().setVisible(false);

			if (clientFactory.getUser() != null) {
				logoutUser();
			}
		}
	}

	protected void loginUser(final String email, final String password) {

		if (email == null || password == null || email.length() < 5 || password.length() < 1) {
			setStatusText("Please enter a valid email address and password.", true);
		} else {
			final AsyncCallback<String> callback = new AsyncCallback<String>() {
				public void onFailure(Throwable caught) {
					checkServerCallIssue(caught);
					view.getWorking().setVisible(false);
				}

				public void onSuccess(String result) {
					if (result == null) {
						// try again in case the user was using a temp password
						loginUserWithTempPassword(email, password);
					} else {
						clientFactory.setUserEmail(email);
						clientFactory.setSessionId(result);
						getUserDetails(result, false);
					}
				}
			};

			view.getWorking().setVisible(true);
			if (serverIsReachable()) {
				userSvc.login(email, password, callback);
			} else {
				view.getWorking().setVisible(false);
			}
		}
	}

	protected void loginUserWithTempPassword(final String email, final String password) {

		if (email == null || password == null || email.length() < 5 || password.length() < 1) {
			setStatusText("Please enter a valid email address and password.", true);
		} else {
			final AsyncCallback<String> callback = new AsyncCallback<String>() {
				public void onFailure(Throwable caught) {
					checkServerCallIssue(caught);
					view.getWorking().setVisible(false);
				}

				public void onSuccess(String result) {
					if (result == null) {
						setStatusText("User not found or invalid password supplied. Please make sure that the email and password are correct and that you are using a confirmed email address.", true);
						view.getWorking().setVisible(false);
					} else {
						clientFactory.setUserEmail(email);
						clientFactory.setSessionId(result);
						getUserDetails(result, true);
					}
				}
			};

			view.getWorking().setVisible(true);
			if (serverIsReachable()) {
				userSvc.loginWithTempPassword(email, password, callback);
			} else {
				view.getWorking().setVisible(false);
			}
		}
	}

	protected void resetUserPw(final String email) {

		if (email == null || email.length() < 5) {
			setStatusText("Please enter a valid email address", true);
		} else {

			final AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
				public void onFailure(Throwable caught) {
					checkServerCallIssue(caught);
					view.getWorking().setVisible(false);
				}

				public void onSuccess(Boolean result) {
					if (result == null || !result) {
						setStatusText("User not found.", true);
					} else {
						view.alertTempPasswordEmailed("Temporary Password Emailed"
								, "A temporary password has been emailed to "
										+ email +  ". Please use that password to login.", null);
						clientFactory.getPlaceController().goTo(new LoginPlace());
					}
					view.getWorking().setVisible(false);
				}
			};

			view.getWorking().setVisible(true);
			if (serverIsReachable()) {
				userSvc.createTempPassword(email, callback);
			} else {
				view.getWorking().setVisible(false);
			}
		}
	}

	public void registerUser(final String email, final String password, final String passwordRepeated) {

		if (email == null || password == null || email.length() < 5 || password.length() < 1) {
			setStatusText("Please enter a valid email address and password.", true);
		} else if (!password.equals(passwordRepeated)) {
			setStatusText("Passwords do not match", true);
		} else {

			final AsyncCallback<Boolean> callbackRegister = new AsyncCallback<Boolean>() {
				public void onFailure(Throwable caught) {
					checkServerCallIssue(caught);
					view.getWorking().setVisible(false);
				}

				public void onSuccess(Boolean result) {
					if (result == null || result == false) {
						setStatusText("There was a problem registering " + email
								+ ". Please try registering with a different email.", true);
					} else {
						view.alertVerificationLinkEmailed("Verification Link Emailed", "A verification link has been emailed to "
								+ email + ". Please click on link and then return here to login.", null);
						clientFactory.getPlaceController().goTo(new LoginPlace());
					}
					view.getWorking().setVisible(false);
				}
			};

			final AsyncCallback<String> callbackUserEmailExists = new AsyncCallback<String>() {
				public void onFailure(Throwable caught) {
					checkServerCallIssue(caught);
					view.getWorking().setVisible(false);
				}

				public void onSuccess(String result) {
					if (result == null) {
						if (serverIsReachable()) {
							userSvc.register(email, password, callbackRegister);
						} else {
							view.getWorking().setVisible(false);
						}
					} else {
						setStatusText(email + " appears to already be in our system."
								+ " Please try logging in with that email or register with a different email.", true);
						view.getWorking().setVisible(false);
					}
				}
			};

			view.getWorking().setVisible(true);
			if (serverIsReachable()) {
				userSvc.lookupIdByEmail(email, callbackUserEmailExists);
			} else {
				view.getWorking().setVisible(false);
			}
		}
	}

	private void getUserDetails(final String sid, final boolean usedTempPassword) {

		final AsyncCallback<Map<String, UserCircleDetails>> callbackGetUserCircles = new AsyncCallback<Map<String, UserCircleDetails>>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
				view.getWorking().setVisible(false);
			}

			public void onSuccess(Map<String, UserCircleDetails> result) {
				if (result == null) {
					setStatusText("User circle details not found.", true);
					view.getWorking().setVisible(false);
				} else {
					clientFactory.getUser().setUserCircles(result);
					if (result.size() > 0) {
						if (clientFactory.getCircleId() == null || !result.containsKey(clientFactory.getCircleId())) {
							Map.Entry<String, UserCircleDetails> entry = result.entrySet().iterator().next();
							clientFactory.setCircleId(entry.getKey());
						}
						clientFactory.setCircle(result.get(clientFactory.getCircleId()));
					} else {
						clientFactory.setCircle(null);
					}
					clientFactory.getPlaceController().goTo(new HomePlace());
					if (usedTempPassword) {
						view.alertUsingTempPassword("Temporary Password Login"
								, "The password that you signed in with cannot be used again."
										+ " Please set your permanent password in Settings > User Settings > Basic Settings", null);
					}

					associateUserWithPushRegistration(clientFactory.getUser().getId());

					clientFactory.setWelcomeVisible(false);
				}
			}
		};


		final AsyncCallback<UserDetails> callbackGetUser = new AsyncCallback<UserDetails>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);

				view.getWorking().setVisible(false);
			}

			public void onSuccess(UserDetails result) {
				if (result == null) {
					setStatusText("User details not found.", true);
					view.getWorking().setVisible(false);
				} else {
					clientFactory.setUser(result);
					if (serverIsReachable()) {
						userSvc.getUsersCircleSubscriptions(result.getId(), callbackGetUserCircles);
					}
				}
			}
		};

		view.getWorking().setVisible(true);
		if (serverIsReachable()) {
			userSvc.getUser(sid, callbackGetUser);
		} else {
			view.getWorking().setVisible(false);
		}
	}

	public void logoutUser() {

		AsyncCallback<Boolean> callback = new AsyncCallback<Boolean>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
				setStatusText("Server logout failure", false);
				view.getWorking().setVisible(false);
			}

			public void onSuccess(Boolean result) {
				disassociateUserWithPushRegistration(clientFactory.getUser().getId());
				clientFactory.setUserLoggedOut();
				view.getWorking().setVisible(false);
			}
		};

		view.getWorking().setVisible(true);
		if (serverIsReachable()) {
			userSvc.logout(clientFactory.getSessionId(), callback);
		} else {
			view.getWorking().setVisible(false);
		}
	}


	static String GCM_REG_ROOT = "file:///data/data/com.voxgift.winstonplus.android"; // required when SDCard present
	static String GCM_REG_DIRECTORY = "files";
	static String GCM_REG_FILE = "GCMRegistrationId";

	static String APN_REG_ROOT = "/var/mobile/Applications/5FB37B7B-77FD-41F9-B016-624999CA1A6C/Documents"; // should never be used
	static String APN_REG_DIRECTORY = ".";
	static String APN_REG_FILE = "APNRegistrationId";

	private void associateUserWithPushRegistration(String userId) {
		if (MGWT.getOsDetection().isAndroid()) {
			associateUserWithPushRegistration(userId, GCM_REG_ROOT, GCM_REG_DIRECTORY, GCM_REG_FILE, PushServiceType.GCM, true);
		} else if (MGWT.getOsDetection().isIOs()) {
			associateUserWithPushRegistration(userId, APN_REG_ROOT, APN_REG_DIRECTORY, APN_REG_FILE, PushServiceType.APN, true);
		}
	}

	private void disassociateUserWithPushRegistration(String userId) {
		if (MGWT.getOsDetection().isAndroid()) {
			associateUserWithPushRegistration(userId, GCM_REG_ROOT, GCM_REG_DIRECTORY, GCM_REG_FILE, PushServiceType.GCM, false);
		} else if (MGWT.getOsDetection().isIOs()) {
			associateUserWithPushRegistration(userId, APN_REG_ROOT, APN_REG_DIRECTORY, APN_REG_FILE, PushServiceType.APN, false);
		}
	}

	private void associateUserWithPushRegistration(final String userId, final String rootDirectoryName, final String directoryName, final String fileName, final int pushServiceTypeId, final boolean flag) {

		final AsyncCallback<Boolean> associateUserWithPushRegistrationCallback = new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
				//LOGGER.log(java.util.logging.Level.INFO, "associateUserWithPushRegistration returned error");
			}

			@Override
			public void onSuccess(Boolean result) {
				//LOGGER.log(java.util.logging.Level.INFO, "associateUserWithPushRegistration succeeded");
			}
		};

		final FileReader reader = clientFactory.getPhoneGap().getFile().createReader();

		reader.setOnloadCallback(new ReaderCallback<FileReader>() {
			@Override
			public void onCallback(final FileReader result) {
				//LOGGER.log(java.util.logging.Level.INFO, "FileReader returned: " + result.getResult());
				if (serverIsReachable()) {
					if (flag) {
						userSvc.associateUserWithPushRegistration(userId, result.getResult()
								, pushServiceTypeId, associateUserWithPushRegistrationCallback);
					} else {
						userSvc.disassociateUserWithPushRegistration(userId, result.getResult()
								, pushServiceTypeId, associateUserWithPushRegistrationCallback);
					}
				}
			}
		});

		reader.setOnErrorCallback(new ReaderCallback<FileReader>() {
			@Override
			public void onCallback(FileReader result) {
				//LOGGER.log(java.util.logging.Level.INFO, "FileReader returned error: " + result.getResult());
			}
		});

		final FileCallback<EntryBase, FileError> resolveLocalFileSystemURICallback = new FileCallback<EntryBase, FileError>() {
			@Override
			public void onSuccess(EntryBase entryBase) {
				FileEntry entry = entryBase.getAsFileEntry();
				//LOGGER.log(java.util.logging.Level.INFO, "Resolved file: " + entry.getFullPath());
				reader.readAsText(entry);
			}
			@Override
			public void onFailure(FileError error) {
				//LOGGER.log(java.util.logging.Level.INFO, "Failed to resolve file with error code: " + error.getErrorCode());
			}
		};

		final FileCallback<FileEntry, FileError> getFileCallback = new FileCallback<FileEntry, FileError>() {
			@Override
			public void onSuccess(final FileEntry entry) {
				//LOGGER.log(java.util.logging.Level.INFO, "Found file: " + entry.getFullPath());
				reader.readAsText(entry);
			}
			@Override
			public void onFailure(FileError error) {
				String url = rootDirectoryName + "/" + directoryName + "/" + fileName;
				//LOGGER.log(java.util.logging.Level.INFO, "Failed to get file with error code: " + error.getErrorCode() + " trying to resolve: " + url);
				clientFactory.getPhoneGap().getFile().resolveLocalFileSystemURI(url, resolveLocalFileSystemURICallback);
			}
		};

		final FileCallback<DirectoryEntry, FileError> getDirectoryCallback = new FileCallback<DirectoryEntry, FileError>() {
			@Override
			public void onSuccess(DirectoryEntry entry) {
				//LOGGER.log(java.util.logging.Level.INFO, "Found directory: " + entry.getFullPath());
				entry.getFile(fileName, new Flags(false, false), getFileCallback);
			}
			@Override
			public void onFailure(FileError error) {
				String url = rootDirectoryName + "/" + directoryName + "/" + fileName;
				//LOGGER.log(java.util.logging.Level.INFO, "Failed to get directory with error code: " + error.getErrorCode() + " trying to resolve: " + url);
				clientFactory.getPhoneGap().getFile().resolveLocalFileSystemURI(url, resolveLocalFileSystemURICallback);
			}
		};

		final FileCallback<FileSystem, FileError> fileSystemCallback = new FileCallback<FileSystem, FileError>() {
			@Override
			public void onSuccess(FileSystem entry) {
				//LOGGER.log(java.util.logging.Level.INFO, "Looking in root: " + entry.getRoot().getFullPath());
				entry.getRoot().getDirectory(directoryName, new Flags(false, false), getDirectoryCallback);
			}
			@Override
			public void onFailure(FileError error) {
				//LOGGER.log(java.util.logging.Level.INFO, "Failed to request file system with error code: " + error.getErrorCode());
			}
		};

		clientFactory.getPhoneGap().getFile().requestFileSystem(FileSystem.LocalFileSystem_PERSISTENT, 0, fileSystemCallback);
	}

	public boolean serverIsReachable() {
		if (clientFactory.getPhoneGap().getConnection().getType().equalsIgnoreCase("NONE")) {
			view.alertServerCommunicationsProblem("Server Communications Problem"
					, "Please verify that you have a network connection and then try again.", null);
			return false;
		} 
		return true;
	}

	public void checkServerCallIssue(Throwable caught) {
		String text = new String("Unknown issue calling server. Please try again.");
		try {
			throw caught;
		} catch (IncompatibleRemoteServiceException e) {
			text = "This client is not compatible with the server. Please make sure that you are running the latest version of the app or, as appropriate, refresh the browser.";
		} catch (InvocationException e) {
			if (clientFactory.getPhoneGap().getConnection().getType().equalsIgnoreCase("NONE")) {
				text = "Please verify that you have a network connection and then try again.";
			} else {
				text = "The server call did not complete cleanly. Please try again.";
			}
		} catch (Throwable e) {
			text = caught.getLocalizedMessage();
		} finally {
			view.alertServerCommunicationsProblem("Server Communications Problem"
					, text + " If this issue persists, please contact support@voxgift.com.", null);
		}
	}
}
