package com.voxgift.winstonplus.client.activities.messagelist;

import java.util.List;

import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.widget.base.HasRefresh;
import com.googlecode.mgwt.ui.client.widget.base.PullArrowWidget;
import com.googlecode.mgwt.ui.client.widget.base.PullPanel.Pullhandler;

import com.voxgift.winstonplus.client.DecoratedView;
import com.voxgift.winstonplus.data.shared.model.MessageDetails;

public interface MessageListView extends DecoratedView  {

	public void render(List<MessageDetails> messages);

	public void setHeaderPullHandler(Pullhandler pullHandler);

	public void setFooterPullHandler(Pullhandler pullHandler);

	public PullArrowWidget getPullHeader();

	public PullArrowWidget getPullFooter();

	public void refresh();

	public HasRefresh getPullPanel();

	public HasText getRefreshButtonText();
	
	public HasTapHandlers getRefreshButton();
	
	public HasHTML getHelpScreen();
}
