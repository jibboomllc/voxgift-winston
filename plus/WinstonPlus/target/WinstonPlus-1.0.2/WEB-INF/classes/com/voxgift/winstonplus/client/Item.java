/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client;

/**
 * @author Tom Giesberg
 * 
 */
public class Item {
	public enum Attribute {FIRST, MIDDLE, LAST, ADD, ONLY};
	
	private String displayString;
	private final String entry;
	private final Attribute attribute;
	
	public Item(String displayString, String entry, Attribute attribute) {
		this.displayString = displayString;
		this.entry = entry;
		this.attribute = attribute;
	}
	
	public Item(String displayString, String entry) {
		this(displayString, entry, Attribute.MIDDLE);
	}

	/**
	 * @return the displayString
	 */
	public String getDisplayString() {
		return displayString;
	}

	public String getEntry() {
		return entry;
	}

	public Attribute getAttribute() {
		return attribute;
	}
}
