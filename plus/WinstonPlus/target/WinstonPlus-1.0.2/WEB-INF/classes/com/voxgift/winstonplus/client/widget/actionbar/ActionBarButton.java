package com.voxgift.winstonplus.client.widget.actionbar;

import com.google.gwt.resources.client.ImageResource;
import com.voxgift.winstonplus.client.css.ActionBarCss;
import com.voxgift.winstonplus.client.css.CssBundle;

public class ActionBarButton extends ActionBarButtonBase {
  /**
   * @param imageResource the image to use
   */
	public ActionBarButton(ImageResource imageResource) {
		this(CssBundle.INSTANCE.actionBarCss(), imageResource);
	}

  /**
   * @param css the css to use
   * @param imageResource the image to use
   */
	public ActionBarButton(ActionBarCss css, ImageResource imageResource) {
		super(css, imageResource);

	}

}
