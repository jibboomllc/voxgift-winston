package com.voxgift.winstonplus.client.activities.settings.legal;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class LegalDocumentsEntrySelectedEvent extends Event<LegalDocumentsEntrySelectedEvent.Handler> {

	public interface Handler {
		void onAnimationSelected(LegalDocumentsEntrySelectedEvent event);
	}

	private static final Type<LegalDocumentsEntrySelectedEvent.Handler> TYPE = new Type<LegalDocumentsEntrySelectedEvent.Handler>();
	private final String title;
	private final String url;

	public static void fire(EventBus eventBus, String title, String url) {
		eventBus.fireEvent(new LegalDocumentsEntrySelectedEvent(title, url));
	}

	public static HandlerRegistration register(EventBus eventBus, Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	@Override
	public com.google.web.bindery.event.shared.Event.Type<Handler> getAssociatedType() {
		return TYPE;
	}

	protected LegalDocumentsEntrySelectedEvent(String title, String url) {
		this.title = title;
        this.url = url;
	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onAnimationSelected(this);
	}

	public static Type<LegalDocumentsEntrySelectedEvent.Handler> getType() {
		return TYPE;
	}

	public String getTitle() {
		return title;
	}

	public String getUrl() {
		return url;
	}

}
