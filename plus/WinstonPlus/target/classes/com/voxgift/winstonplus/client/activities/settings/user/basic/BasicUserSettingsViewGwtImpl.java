/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user.basic;

import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.googlecode.mgwt.ui.client.widget.FormListEntry;
import com.googlecode.mgwt.ui.client.widget.LayoutPanel;
import com.googlecode.mgwt.ui.client.widget.MEmailTextBox;
import com.googlecode.mgwt.ui.client.widget.MListBox;
import com.googlecode.mgwt.ui.client.widget.MPasswordTextBox;
import com.googlecode.mgwt.ui.client.widget.MTextBox;
import com.googlecode.mgwt.ui.client.widget.ScrollPanel;
import com.googlecode.mgwt.ui.client.widget.WidgetList;
import com.voxgift.winstonplus.client.DecoratedViewGwtImpl;

/**
 * @author Tom Giesberg
 * 
 */
public class BasicUserSettingsViewGwtImpl extends DecoratedViewGwtImpl implements BasicUserSettingsView {

	private Label statusText;
	private MEmailTextBox emailAddress;
	private MPasswordTextBox password;
	private MPasswordTextBox passwordRepeated;
	private MTextBox firstName;
	private MTextBox lastName;
	private MListBox outputLanguage;
	private MListBox gender;
	private Button updateButton;

	public BasicUserSettingsViewGwtImpl() {
		ScrollPanel scrollPanel = new ScrollPanel();
		main.add(scrollPanel);

		LayoutPanel container = new LayoutPanel();

		WidgetList widgetList = new WidgetList();
		widgetList.setRound(true);

		statusText = new Label();
		statusText.setText("");
		container.add(statusText);

		emailAddress = new MEmailTextBox();
		widgetList.add(new FormListEntry("Email address", emailAddress));

		password = new MPasswordTextBox();
		widgetList.add(new FormListEntry("New password", password));

		passwordRepeated = new MPasswordTextBox();
		widgetList.add(new FormListEntry("Repeat new password", passwordRepeated));

		firstName = new MTextBox();
		firstName.setPlaceHolder("Optional");
		widgetList.add(new FormListEntry("First name", firstName));

		lastName = new MTextBox();
		lastName.setPlaceHolder("Optional");
		widgetList.add(new FormListEntry("Last name", lastName));

		outputLanguage = new MListBox();
		LayoutPanel indicatorContainer = new LayoutPanel();
		indicatorContainer.setHorizontal(true);
		indicatorContainer.add(outputLanguage);
		LayoutPanel indicator = new LayoutPanel();
		indicator.setStyleName("sub-menu-indicator");
		indicatorContainer.add(indicator);
		widgetList.add(new FormListEntry("Output Language", indicatorContainer));

		gender = new MListBox();
		LayoutPanel indicatorContainer2 = new LayoutPanel();
		indicatorContainer2.setHorizontal(true);
		indicatorContainer2.add(gender);
		LayoutPanel indicator2 = new LayoutPanel();
		indicator2.setStyleName("sub-menu-indicator");
		indicatorContainer2.add(indicator2);
		widgetList.add(new FormListEntry("Gender", indicatorContainer2));

		container.add(widgetList);

		updateButton = new Button();
		updateButton.setText("Update user settings");
		container.add(updateButton);

		scrollPanel.setWidget(container);
		
		// workaround for android formfields jumping around when using
	    // -webkit-transform
	    scrollPanel.setUsePos(MGWT.getOsDetection().isAndroid());
	}

	@Override
	public HasText getStatusText() {
		return statusText;
	}

	@Override
	public HasText getEmailAddress() {
		return emailAddress;
	}

	@Override
	public HasText getPassword() {
		return password;
	}

	@Override
	public HasText getPasswordRepeated() {
		return passwordRepeated;
	}

	@Override
	public HasText getFirstName() {
		return firstName;
	}

	@Override
	public HasText getLastName() {
		return lastName;
	}

	@Override
	public void addOutputLanguageItem(String item) {
		outputLanguage.addItem(item);
	}

	@Override
	public void clearOutputLanguageItems() {
		outputLanguage.clear();
	}

	@Override
	public int getSelectedOutputLanguageIndex() {
		return outputLanguage.getSelectedIndex();
	}

	@Override
	public void setSelectedOutputLanguageIndex(int index, boolean selected) {
		outputLanguage.setItemSelected(index, selected);
	}


	@Override
	public void addGenderItem(String item) {
		gender.addItem(item);
	}

	@Override
	public void clearGenderItems() {
		gender.clear();
	}

	@Override
	public int getSelectedGenderIndex() {
		return gender.getSelectedIndex();
	}

	@Override
	public void setSelectedGenderIndex(int index, boolean selected) {
		gender.setItemSelected(index, selected);
	}

	@Override
	public HasTapHandlers getUpdateButton() {
		return updateButton;
	}
}
