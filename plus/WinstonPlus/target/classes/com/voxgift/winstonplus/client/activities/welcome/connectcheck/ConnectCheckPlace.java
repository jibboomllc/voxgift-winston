/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.connectcheck;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class ConnectCheckPlace extends Place {
	public static class ConnectCheckPlaceTokenizer implements PlaceTokenizer<ConnectCheckPlace> {

		@Override
		public ConnectCheckPlace getPlace(String token) {
			return new ConnectCheckPlace();
		}

		@Override
		public String getToken(ConnectCheckPlace place) {
			return "";
		}

	}
}
