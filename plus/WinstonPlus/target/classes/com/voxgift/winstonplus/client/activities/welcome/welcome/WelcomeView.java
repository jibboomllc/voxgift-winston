/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.welcome;

import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.voxgift.winstonplus.client.activities.welcome.LoginBaseView;

/**
 * @author Tom Giesberg
 * 
 */
public interface WelcomeView extends LoginBaseView {

	public HasTapHandlers getRegisterButton();
	
	public HasTapHandlers getLoginButton();
	
	public HasTapHandlers getResetPasswordButton();
}
