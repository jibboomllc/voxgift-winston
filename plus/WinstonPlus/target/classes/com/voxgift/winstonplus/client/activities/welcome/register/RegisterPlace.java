/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.register;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class RegisterPlace extends Place {
	public static class RegisterPlaceTokenizer implements PlaceTokenizer<RegisterPlace> {

		@Override
		public RegisterPlace getPlace(String token) {
			return new RegisterPlace();
		}

		@Override
		public String getToken(RegisterPlace place) {
			return "";
		}

	}
}
