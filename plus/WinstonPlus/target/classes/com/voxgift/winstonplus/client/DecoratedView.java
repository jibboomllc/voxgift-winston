package com.voxgift.winstonplus.client;

import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.IsWidget;
import com.googlecode.mgwt.ui.client.dialog.Dialogs.AlertCallback;

public interface DecoratedView extends IsWidget{
	
	public HasText getStatusText();

	public HasText getErrorText();

	public void alertServerCommunicationsProblem(String title, String text, AlertCallback callback);
}
