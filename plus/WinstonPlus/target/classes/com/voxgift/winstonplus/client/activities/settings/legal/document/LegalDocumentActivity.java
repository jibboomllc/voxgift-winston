/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.legal.document;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.MGWT;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.DecoratedActivity;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent.SettingsEntry;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentServiceAsync;
import com.voxgift.winstonplus.shared.AppConstants;

/**
 * @author Tom Giesberg
 * 
 */
public class LegalDocumentActivity extends DecoratedActivity {

	// private final static Logger LOGGER = Logger.getLogger(BasicCircleSettingsActivity.class.getName()); 

	private static LegalDocumentView view;
	private String title;
	private String id;

	private RpcDocumentServiceAsync docSvc = GWT.create(RpcDocumentService.class);

	public LegalDocumentActivity(ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getLegalDocumentView());

		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) docSvc, appConstants.webAppUrl(), "rpc/document");

		view = clientFactory.getLegalDocumentView();
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		getClientFactory().getFrameView().showBackbutton(!MGWT.getOsDetection().isAndroid());
		getClientFactory().getFrameView().getBackbuttonText().setText("Select");
		getClientFactory().getFrameView().getTitle().setText(title);

		getDocument(id, getClientFactory().getUser().getLanguage(), getClientFactory().getUser().getLocale());

		panel.setWidget(view);
		addHandlerRegistration(getClientFactory().getFrameView().getBackbuttonTarget().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				SettingsEntrySelectedEvent.fire(getEventBus(), SettingsEntry.LEGAL); 
			}
		}));
	}

	private void getDocument(final String documentId, final String language, final String locale) {
		final AsyncCallback<String> callback = new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(String result) {
				if (result == null) {
					view.getContent().setHTML("Legal document not found.");
					view.refresh();
				} else {
					view.getContent().setHTML(result);
					view.refresh();
				}
			}
		};
		
		if (serverIsReachable()) {
			docSvc.getDocument(documentId, language, locale, callback);
		}
	}
}
