/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user.image;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HasHandlers;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.widget.Button;
import com.googlecode.mgwt.ui.client.widget.LayoutPanel;
import com.googlecode.mgwt.ui.client.widget.RoundPanel;
import com.googlecode.mgwt.ui.client.widget.ScrollPanel;
import com.voxgift.winstonplus.client.DecoratedViewGwtImpl;
import com.voxgift.winstonplus.shared.AppConstants;

/**
 * @author Tom Giesberg
 * 
 */
public class ImageUserSettingsViewGwtImpl extends DecoratedViewGwtImpl implements ImageUserSettingsView {

	private Image image;
	private Label uploadStatusText;
	private FormPanel imageUploadForm;
	private FileUpload fileToUpload;
	private Hidden imageUploadFormUserId;
	private Button uploadButton;

	public ImageUserSettingsViewGwtImpl() {
		ScrollPanel scrollPanel = new ScrollPanel();
		main.add(scrollPanel);

		LayoutPanel container = new LayoutPanel();

		RoundPanel imagePanel = new RoundPanel();
		image = new Image();
		imagePanel.add(image);

		uploadStatusText = new Label();
		uploadStatusText.setText("");
		imagePanel.add(uploadStatusText);

		if (MGWT.getOsDetection().isDesktop()) {
			imageUploadForm = new FormPanel();
			AppConstants appConstants = GWT.create(AppConstants.class);
			imageUploadForm.setAction(appConstants.imageUploadUrl());

			// Because we're going to add a FileUpload widget, we'll need to set the
			// form to use the POST method, and multipart MIME encoding.
			imageUploadForm.setEncoding(FormPanel.ENCODING_MULTIPART);
			imageUploadForm.setMethod(FormPanel.METHOD_POST);

			imageUploadForm.setWidget(imagePanel);

			imageUploadFormUserId = new Hidden();
			imageUploadFormUserId.setName("userId");
			imagePanel.add(imageUploadFormUserId);

			fileToUpload = new FileUpload();
			fileToUpload.setName("uploadFormElement");
			imagePanel.add(fileToUpload);

			uploadButton = new Button();
			uploadButton.setText("Upload image");
			imagePanel.add(uploadButton);

			container.add(imageUploadForm);
		} else {
			uploadButton = new Button();
			uploadButton.setText("Upload image");
			imagePanel.add(uploadButton);

			container.add(imagePanel);
		}

		scrollPanel.setWidget(container);
	}

	@Override
	public void setImageUrl(String url) {
		image.setUrl(url);
	}

	@Override
	public HasText getUploadStatusText() {
		return uploadStatusText;
	}

	@Override
	public HasHandlers getUploadForm() {
		return imageUploadForm;
	}

	@Override
	public String getFileToUploadName() {
		return fileToUpload.getFilename();
	}

	@Override
	public TakesValue<String> getUploadFormUserId() {
		return imageUploadFormUserId;
	}

	@Override
	public HasTapHandlers getUploadButton() {
		return uploadButton;
	}

	@Override
	public void startImageUpload() {
		imageUploadForm.submit();
	}

}
