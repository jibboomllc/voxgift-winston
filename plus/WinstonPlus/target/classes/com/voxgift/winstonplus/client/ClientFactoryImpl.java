/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client;

import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.googlecode.gwtphonegap.client.PhoneGap;
import com.googlecode.mgwt.ui.client.MGWT;
import com.voxgift.winstonplus.client.activities.compose.ComposeView;
import com.voxgift.winstonplus.client.activities.compose.ComposeViewGwtImpl;
import com.voxgift.winstonplus.client.activities.frame.FrameView;
import com.voxgift.winstonplus.client.activities.frame.FrameViewGwtImpl;
import com.voxgift.winstonplus.client.activities.messagelist.MessageListView;
import com.voxgift.winstonplus.client.activities.messagelist.MessageListViewGwtImpl;
import com.voxgift.winstonplus.client.activities.settings.SettingsView;
import com.voxgift.winstonplus.client.activities.settings.SettingsViewGwtImpl;
import com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsView;
import com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsViewGwtImpl;
import com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsView;
import com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsViewGwtImpl;
import com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsView;
import com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsViewGwtImpl;
import com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsView;
import com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsViewGwtImpl;
import com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentView;
import com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentViewGwtImpl;
import com.voxgift.winstonplus.client.activities.settings.user.UserSettingsView;
import com.voxgift.winstonplus.client.activities.settings.user.UserSettingsViewGwtImpl;
import com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsView;
import com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsViewGwtImpl;
import com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsView;
import com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsViewGwtImpl;
import com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsView;
import com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsViewGwtImpl;
import com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckView;
import com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckViewGwtImpl;
import com.voxgift.winstonplus.client.activities.welcome.help.HelpView;
import com.voxgift.winstonplus.client.activities.welcome.help.HelpViewGwtImpl;
import com.voxgift.winstonplus.client.activities.welcome.login.LoginView;
import com.voxgift.winstonplus.client.activities.welcome.login.LoginViewGwtImpl;
import com.voxgift.winstonplus.client.activities.welcome.register.RegisterView;
import com.voxgift.winstonplus.client.activities.welcome.register.RegisterViewGwtImpl;
import com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordView;
import com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordViewGwtImpl;
import com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomeView;
import com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomeViewGwtImpl;
import com.voxgift.winstonplus.data.shared.model.UserCircleDetails;
import com.voxgift.winstonplus.data.shared.model.UserDetails;

/**
 * @author Tom Giesberg
 * 
 */
public class ClientFactoryImpl implements ClientFactory {

	//private Logger LOGGER = Logger.getLogger(getClass().getName());

	private PhoneGap phoneGap;

	private EventBus eventBus;
	private PlaceController placeController;

	private boolean usingSinglePane;
	private Widget welcomeWidget;

	private FrameView frameViewImpl;
	private MessageListView homeViewImpl;
	private ConnectCheckView connectCheckView;
	private WelcomeView welcomeView;
	private RegisterView registerView;
	private LoginView loginView;
	private ResetPasswordView resetPasswordView;
	private HelpView helpView;
	
	private ComposeView composeView;
	private SettingsView settingsView;
	private UserSettingsView userSettingsView;
	private BasicUserSettingsView basicUserSettingsView;
	private ImageUserSettingsView imageUserSettingsView;
	private FavoritesUserSettingsView favoritesUserSettingsView;
	private CircleSettingsView circleSettingsView;
	private BasicCircleSettingsView basicCircleSettingsView;
	private UserCircleSettingsView userCircleSettingsView;
	private LegalDocumentsView legalDocumentsView;
	private LegalDocumentView legalDocumentView;

	private String sessionId;
	private String userEmail;
	private UserDetails user;
	private String circleId;
	private UserCircleDetails circle;

	public ClientFactoryImpl() {
		eventBus = new SimpleEventBus();

		placeController = new PlaceController(eventBus);
/*
		//LOGGER.log(Level.INFO, "OS Detected"
				+ ", isAndroid:" + MGWT.getOsDetection().isAndroid()
				+ ", isAndroidPhone:" + MGWT.getOsDetection().isAndroidPhone()
				+ ", isAndroidTablet:" + MGWT.getOsDetection().isAndroidTablet()
				+ ", isIOs:" + MGWT.getOsDetection().isIOs()
				+ ", isIPad:" + MGWT.getOsDetection().isIPad()
				+ ", isIPadRetina:" + MGWT.getOsDetection().isIPadRetina()
				+ ", isIPhone:" + MGWT.getOsDetection().isIPhone()
				+ ", isRetina:" + MGWT.getOsDetection().isRetina()
				+ ", isTablet:" + MGWT.getOsDetection().isTablet()
		);
*/
		if (MGWT.getOsDetection().isTablet()) {
			usingSinglePane = false;
		} else {
			usingSinglePane = true;
		}
	}

	@Override
	public void setPhoneGap(PhoneGap phoneGap) {
		this.phoneGap = phoneGap;
	}

	@Override
	public PhoneGap getPhoneGap() {
		return phoneGap;
	}

	@Override
	public FrameView getFrameView() {
		if (frameViewImpl == null) {
			frameViewImpl = new FrameViewGwtImpl();
		}
		return frameViewImpl;
	}

	@Override
	public MessageListView getHomeView() {
		if (homeViewImpl == null) {
			homeViewImpl = new MessageListViewGwtImpl();
		}
		return homeViewImpl;
	}

	@Override
	public EventBus getEventBus() {
		return eventBus;
	}

	@Override
	public PlaceController getPlaceController() {
		return placeController;
	}

	@Override
	public ConnectCheckView getConnectCheckView() {
		if (connectCheckView == null) {
			connectCheckView = new ConnectCheckViewGwtImpl();
		}
		return connectCheckView;
	}

	@Override
	public WelcomeView getWelcomeView() {
		if (welcomeView == null) {
			welcomeView = new WelcomeViewGwtImpl();
		}
		return welcomeView;
	}

	@Override
	public RegisterView getRegisterView() {
		if (registerView == null) {
			registerView = new RegisterViewGwtImpl();
		}
		return registerView;
	}

	@Override
	public LoginView getLoginView() {
		if (loginView == null) {
			loginView = new LoginViewGwtImpl();
		}
		return loginView;
	}

	@Override
	public ResetPasswordView getResetPasswordView() {
		if (resetPasswordView == null) {
			resetPasswordView = new ResetPasswordViewGwtImpl();
		}
		return resetPasswordView;
	}
	
	@Override
	public HelpView getHelpView() {
		if (helpView == null) {
			helpView = new HelpViewGwtImpl();
		}
		return helpView;
	}

	@Override
	public ComposeView getComposeView() {
		if (composeView == null) {
			composeView = new ComposeViewGwtImpl();
		}
		return composeView;
	}

	@Override
	public SettingsView getSettingsView() {
		if (settingsView == null) {
			settingsView = new SettingsViewGwtImpl();
		}
		return settingsView;
	}

	@Override
	public UserSettingsView getUserSettingsView() {
		if (userSettingsView == null) {
			userSettingsView = new UserSettingsViewGwtImpl();
		}
		return userSettingsView;
	}

	@Override
	public BasicUserSettingsView getBasicUserSettingsView() {
		if (basicUserSettingsView == null) {
			basicUserSettingsView = new BasicUserSettingsViewGwtImpl();
		}
		return basicUserSettingsView;
	}

	@Override
	public ImageUserSettingsView getImageUserSettingsView() {
		if (imageUserSettingsView == null) {
			imageUserSettingsView = new ImageUserSettingsViewGwtImpl();
		}
		return imageUserSettingsView;
	}

	@Override
	public FavoritesUserSettingsView getFavoritesUserSettingsView() {
		if (favoritesUserSettingsView == null) {
			favoritesUserSettingsView = new FavoritesUserSettingsViewGwtImpl();
		}
		return favoritesUserSettingsView;
	}


	@Override
	public CircleSettingsView getCircleSettingsView() {
		if (circleSettingsView == null) {
			circleSettingsView = new CircleSettingsViewGwtImpl();
		}
		return circleSettingsView;
	}

	@Override
	public BasicCircleSettingsView getBasicCircleSettingsView() {
		if (basicCircleSettingsView == null) {
			basicCircleSettingsView = new BasicCircleSettingsViewGwtImpl();
		}
		return basicCircleSettingsView;
	}

	@Override
	public UserCircleSettingsView getUserCircleSettingsView() {
		if (userCircleSettingsView == null) {
			userCircleSettingsView = new UserCircleSettingsViewGwtImpl();
		}
		return userCircleSettingsView;
	}

	@Override
	public LegalDocumentsView getLegalDocumentsView() {
		if (legalDocumentsView == null) {
			legalDocumentsView = new LegalDocumentsViewGwtImpl();
		}
		return legalDocumentsView;
	}

	@Override
	public LegalDocumentView getLegalDocumentView() {
		if (legalDocumentView == null) {
			legalDocumentView = new LegalDocumentViewGwtImpl();
		}
		return legalDocumentView;
	}

	@Override
	public boolean usingSinglePane() {
		return usingSinglePane;
	}	

	@Override
	public void setWelcomeWidget(Widget asWidget) {
		welcomeWidget = asWidget;
	}	

	@Override
	public void setWelcomeVisible(boolean visible) {
		if(welcomeWidget != null) {
			welcomeWidget.setVisible(visible);
		}
	}

	@Override
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
		Storage localStorage = Storage.getLocalStorageIfSupported();
		if (localStorage != null) {
			if (sessionId == null) {
				localStorage.removeItem("sessionId");
			} else {
				localStorage.setItem("sessionId", sessionId);
			}
		}
	}

	@Override
	public String getSessionId() {
		if (sessionId == null) {
			Storage localStorage = Storage.getLocalStorageIfSupported();
			if (localStorage != null) {
				sessionId = localStorage.getItem("sessionId");
			}
		}

		return sessionId;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
		Storage localStorage = Storage.getLocalStorageIfSupported();
		if (localStorage != null) {
			if (userEmail == null) {
				localStorage.removeItem("userEmail");
			} else {
				localStorage.setItem("userEmail", userEmail);
			}
		}
	}

	@Override
	public String getUserEmail() {
		if (userEmail == null) {
			Storage localStorage = Storage.getLocalStorageIfSupported();
			if (localStorage != null) {
				userEmail = localStorage.getItem("userEmail");
			}
		}

		return userEmail;
	}

	@Override
	public void setUser(UserDetails user) {
		this.user = user;
	}

	@Override
	public UserDetails getUser() {
		return user;
	}

	public void setCircleId(String circleId) {
		this.circleId = circleId;
		Storage localStorage = Storage.getLocalStorageIfSupported();
		if (localStorage != null) {
			if (circleId == null) {
				localStorage.removeItem("circleId");
			} else {
				localStorage.setItem("circleId", circleId);
			}
		}
	}

	@Override
	public String getCircleId() {
		if (circleId == null) {
			Storage localStorage = Storage.getLocalStorageIfSupported();
			if (localStorage != null) {
				circleId = localStorage.getItem("circleId");
			}
		}

		return circleId;
	}
	
	@Override
	public void setCircle(UserCircleDetails circle) {
		this.circle = circle;
	}

	@Override
	public UserCircleDetails getCircle() {
		return circle;
	}

	@Override
	public void setUserLoggedOut() {
		setSessionId(null);
		setUser(null);
        setUserEmail(null);
		//setCircle(null);
		setWelcomeVisible(true);
	}

}
