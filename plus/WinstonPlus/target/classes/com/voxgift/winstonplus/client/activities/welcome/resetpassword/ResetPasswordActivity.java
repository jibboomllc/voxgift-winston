/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.resetpassword;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.activities.welcome.LoginBaseActivity;

/**
 * @author Tom Giesberg
 * 
 */
public class ResetPasswordActivity extends LoginBaseActivity {
	//private java.util.logging.Logger LOGGER = Logger.getLogger(getClass().getName());

	public ResetPasswordActivity(ClientFactory clientFactory) {
		super(clientFactory);
		view = clientFactory.getResetPasswordView();
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		addHandlerRegistration(((ResetPasswordView)view).getGoButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				resetUserPw(((ResetPasswordView)view).getEmailAddress().getText().toLowerCase().trim());
			}
		}));
		panel.setWidget(view);

		updateHeader();

		if (clientFactory.getUserEmail() != null && !clientFactory.getUserEmail().isEmpty())  {
			((ResetPasswordView)view).getEmailAddress().setText(clientFactory.getUserEmail());
		}
	}
}
