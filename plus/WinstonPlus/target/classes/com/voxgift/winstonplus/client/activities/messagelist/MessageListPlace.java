/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.messagelist;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class MessageListPlace extends Place {
	public static class MessageListPlaceTokenizer implements PlaceTokenizer<MessageListPlace> {

		@Override
		public MessageListPlace getPlace(String token) {
			return new MessageListPlace();
		}

		@Override
		public String getToken(MessageListPlace place) {
			return "";
		}

	}
}
