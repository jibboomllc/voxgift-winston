/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client;

import com.google.gwt.place.shared.PlaceController;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.googlecode.gwtphonegap.client.PhoneGap;
import com.voxgift.winstonplus.client.activities.compose.ComposeView;
import com.voxgift.winstonplus.client.activities.frame.FrameView;
import com.voxgift.winstonplus.client.activities.messagelist.MessageListView;
import com.voxgift.winstonplus.client.activities.settings.SettingsView;
import com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsView;
import com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsView;
import com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsView;
import com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsView;
import com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentView;
import com.voxgift.winstonplus.client.activities.settings.user.UserSettingsView;
import com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsView;
import com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsView;
import com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsView;
import com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckView;
import com.voxgift.winstonplus.client.activities.welcome.help.HelpView;
import com.voxgift.winstonplus.client.activities.welcome.login.LoginView;
import com.voxgift.winstonplus.client.activities.welcome.register.RegisterView;
import com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordView;
import com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomeView;
import com.voxgift.winstonplus.data.shared.model.UserCircleDetails;
import com.voxgift.winstonplus.data.shared.model.UserDetails;

public interface ClientFactory {

	public PhoneGap getPhoneGap();
	public void setPhoneGap(PhoneGap phoneGap);

	public EventBus getEventBus();
	public PlaceController getPlaceController();

	public FrameView getFrameView();
	public MessageListView getHomeView();
	public ConnectCheckView getConnectCheckView();
	public WelcomeView getWelcomeView();
	public RegisterView getRegisterView();
	public LoginView getLoginView();
	public ResetPasswordView getResetPasswordView();
	public HelpView getHelpView();
	
	public ComposeView getComposeView();
	public SettingsView getSettingsView();
	public UserSettingsView getUserSettingsView();
	public BasicUserSettingsView getBasicUserSettingsView();
	public ImageUserSettingsView getImageUserSettingsView();
	public FavoritesUserSettingsView getFavoritesUserSettingsView();
	public CircleSettingsView getCircleSettingsView();
	public BasicCircleSettingsView getBasicCircleSettingsView();
	public UserCircleSettingsView getUserCircleSettingsView();
	public LegalDocumentsView getLegalDocumentsView();
	public LegalDocumentView getLegalDocumentView();
	
	public boolean usingSinglePane();
	
	public void setWelcomeWidget(Widget asWidget);
	public void setWelcomeVisible(boolean visible);
    
	public void setSessionId(String sessionId);
	public String getSessionId();
    
	public void setUserEmail(String email);
	public String getUserEmail();
	
	public void setUser(UserDetails user);
	public UserDetails getUser();
	
    public void setCircleId(String circle);
	public String getCircleId();
	
    public void setCircle(UserCircleDetails circle);
	public UserCircleDetails getCircle();
	
	public void setUserLoggedOut();
	
}
