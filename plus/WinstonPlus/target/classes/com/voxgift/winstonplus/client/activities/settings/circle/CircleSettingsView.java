/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.circle;

import java.util.List;
import com.googlecode.mgwt.ui.client.widget.celllist.HasCellSelectedHandler;
import com.voxgift.winstonplus.client.DecoratedView;
import com.voxgift.winstonplus.client.Item;

/**
 * @author Tom Giesberg
 * 
 */
public interface CircleSettingsView extends DecoratedView {

	public HasCellSelectedHandler getList();

	public void renderItems(List<Item> items);

	public void setSelectedIndex(int index, boolean selected);
}
