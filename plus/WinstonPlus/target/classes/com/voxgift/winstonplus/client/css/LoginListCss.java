package com.voxgift.winstonplus.client.css;

import com.googlecode.mgwt.ui.client.theme.base.ListCss;

public interface LoginListCss extends ListCss {

	@ClassName("mgwt-FormList-Element")
	public String formListElement();

	@ClassName("mgwt-FormList-Element-label")
	public String formListElementLabel();

	@ClassName("mgwt-FormList-Element-container")
	public String formListElementContainer();

}
