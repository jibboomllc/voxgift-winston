/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.help;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class HelpPlace extends Place {

	private String topicId;

	public HelpPlace(String topicId) {
		this.topicId = topicId;
	}

	public String getTopicId() {
		return topicId;
	}

	public static class HelpPlaceTokenizer implements PlaceTokenizer<HelpPlace> {
		static final String SEPARATOR = "!";

		@Override
		public HelpPlace getPlace(String token) {

			return new HelpPlace(token);
		}

		@Override
		public String getToken(HelpPlace place) {
			return place.getTopicId();
		}

	}
}
