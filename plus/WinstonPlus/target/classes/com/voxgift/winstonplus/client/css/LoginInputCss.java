package com.voxgift.winstonplus.client.css;

import com.googlecode.mgwt.ui.client.theme.base.InputCss;

public interface LoginInputCss extends InputCss {

	@ClassName("mgwt-TextBox")
	String textBox();

	@ClassName("mgwt-TextArea")
	String textArea();

	@ClassName("mgwt-PasswordTextBox")
	String passwordBox();

	@ClassName("mgwt-RadioButton")
	String radioButton();

	@ClassName("mgwt-ListBox")
	String listBox();

	@ClassName("mgwt-InputBox-box")
	String box();

	@ClassName("mgwt-InputBox-disabled")
	String disabled();

}
