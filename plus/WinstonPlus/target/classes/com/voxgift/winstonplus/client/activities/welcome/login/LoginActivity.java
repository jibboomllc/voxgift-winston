/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.login;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.activities.welcome.LoginBaseActivity;
import com.voxgift.winstonplus.client.event.ActionEvent;
import com.voxgift.winstonplus.client.event.ActionNames;

/**
 * @author Tom Giesberg
 * 
 */
public class LoginActivity extends LoginBaseActivity {
	//private java.util.logging.Logger LOGGER = Logger.getLogger(getClass().getName());

	public LoginActivity(ClientFactory clientFactory) {
		super(clientFactory);
		view = clientFactory.getLoginView();
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		((LoginView)view).getPassword().setText(null);

		addHandlerRegistration(ActionEvent.register(eventBus, ActionNames.LOGOUT, new ActionEvent.Handler() {

			@Override
			public void onAction(ActionEvent event) {
				((LoginView)view).getEmailAddress().setText(null);
			}
		}));

		addHandlerRegistration(((LoginView)view).getGoButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
					loginUser(((LoginView)view).getEmailAddress().getText().toLowerCase().trim()
							, ((LoginView)view).getPassword().getText());
			}
			}));
		panel.setWidget(view);

		updateHeader();

		if (clientFactory.getUserEmail() != null && !clientFactory.getUserEmail().isEmpty())  {
			((LoginView)view).getEmailAddress().setText(clientFactory.getUserEmail());
		}
	}
}
