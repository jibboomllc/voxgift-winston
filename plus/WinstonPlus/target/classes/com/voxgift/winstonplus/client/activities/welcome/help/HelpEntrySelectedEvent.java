package com.voxgift.winstonplus.client.activities.welcome.help;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class HelpEntrySelectedEvent extends Event<HelpEntrySelectedEvent.Handler> {

	public interface Handler {
		void onAnimationSelected(HelpEntrySelectedEvent event);
	}

	private static final Type<HelpEntrySelectedEvent.Handler> TYPE = new Type<HelpEntrySelectedEvent.Handler>();
	private final String topicId;

	public static void fire(EventBus eventBus, String topicId) {
		eventBus.fireEvent(new HelpEntrySelectedEvent(topicId));
	}

	public static HandlerRegistration register(EventBus eventBus, Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	@Override
	public com.google.web.bindery.event.shared.Event.Type<Handler> getAssociatedType() {
		return TYPE;
	}

	protected HelpEntrySelectedEvent(String topicId) {
		this.topicId = topicId;
	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onAnimationSelected(this);
	}

	public static Type<HelpEntrySelectedEvent.Handler> getType() {
		return TYPE;
	}

	public String getTopicId() {
		return topicId;
	}

}
