/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.welcome;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.activities.welcome.LoginBaseActivity;
import com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace;
import com.voxgift.winstonplus.client.activities.welcome.register.RegisterPlace;
import com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordPlace;
import com.voxgift.winstonplus.client.event.ActionEvent;
import com.voxgift.winstonplus.client.event.ActionNames;

/**
 * @author Tom Giesberg
 * 
 */
public class WelcomeActivity extends LoginBaseActivity {
	//private java.util.logging.Logger LOGGER = Logger.getLogger(getClass().getName());

	public WelcomeActivity(ClientFactory clientFactory) {
		super(clientFactory);
		view = clientFactory.getWelcomeView();
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		addHandlerRegistration(ActionEvent.register(eventBus, ActionNames.LOGOUT, new ActionEvent.Handler() {

			@Override
			public void onAction(ActionEvent event) {
				logoutUser();
			}
		}));

		addHandlerRegistration(((WelcomeView)view).getRegisterButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				clientFactory.getPlaceController().goTo(new RegisterPlace());
			}
		}));

		addHandlerRegistration(((WelcomeView)view).getLoginButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				clientFactory.getPlaceController().goTo(new LoginPlace());
			}
		}));

		addHandlerRegistration(((WelcomeView)view).getResetPasswordButton().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				clientFactory.getPlaceController().goTo(new ResetPasswordPlace());
			}
		}));

		panel.setWidget(view);

		updateHeader();

		//		AppConstants appConstants = GWT.create(AppConstants.class);
		//		view.getStatusText().setText("v." + appConstants.buildVersion());

		if (clientFactory.getUser() == null) {
			view.getWorking().setVisible(true);

			loginSession(clientFactory.getSessionId());
		}
	}
}
