/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.welcome.help;

import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.IncompatibleRemoteServiceException;
import com.google.gwt.user.client.rpc.InvocationException;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.mvp.client.MGWTAbstractActivity;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.event.ActionEvent;
import com.voxgift.winstonplus.client.event.ActionNames;
import com.voxgift.winstonplus.shared.AppConstants;
import com.voxgift.winstonplus.data.shared.model.DocumentDetails;
import com.voxgift.winstonplus.data.shared.model.DocumentType;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentServiceAsync;

/**
 * @author Tom Giesberg
 * 
 */
public class HelpActivity extends MGWTAbstractActivity {
	private List<DocumentDetails> documents;
	private int currentIndex;
	private String topicId;
	private String language;
	private String locale;

	private final HelpView view;
	private final ClientFactory clientFactory;
	private RpcDocumentServiceAsync docSvc = GWT.create(RpcDocumentService.class);

	public HelpActivity(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) docSvc, appConstants.webAppUrl(), "rpc/document");
		view = clientFactory.getHelpView();
	}

	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}

	@Override
	public void start(AcceptsOneWidget panel, final EventBus eventBus) {
		super.start(panel, eventBus);

		if (clientFactory.getUser() != null) {
			language = clientFactory.getUser().getLanguage();
			locale = clientFactory.getUser().getLocale();
		} else {
			language = null;
			locale = null;
		}

		addHandlerRegistration(view.getCloseButton().addTapHandler(new TapHandler() {
			@Override
			public void onTap(TapEvent event) {
				ActionEvent.fire(eventBus, ActionNames.BACK);

				if (clientFactory.getUser() != null) {
					clientFactory.setWelcomeVisible(false);
				}
			}
		}));

		addHandlerRegistration(view.getPreviousButton().addTapHandler(new TapHandler() {
			@Override
			public void onTap(TapEvent event) {
				if (currentIndex > 0) {
					currentIndex--;
					topicId = documents.get(currentIndex).getId();
					refreshContent();
				}
			}
		}));

		addHandlerRegistration(view.getNextButton().addTapHandler(new TapHandler() {
			@Override
			public void onTap(TapEvent event) {
				if (currentIndex + 1 < documents.size()) {
					currentIndex++;
					topicId = documents.get(currentIndex).getId();
					refreshData();
				}
			}
		}));

		refreshData();

		panel.setWidget(view);
		clientFactory.setWelcomeVisible(true);
	}


	private void refreshData() {

		view.getTitle().setHTML("Help");
		
		final AsyncCallback<List<DocumentDetails>> getDocumentsCallback = new AsyncCallback<List<DocumentDetails>>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(List<DocumentDetails> result) {
				if (result == null) {
					view.getContent().setHTML("Help documents not found.");
				} else {
					documents = result;
					if (topicId == null || topicId.isEmpty()) {
						topicId = documents.get(0).getId();
						currentIndex = 0;
					} else {
						currentIndex = -1;
						for (int i= 0; i < documents.size() && currentIndex < 0; i++) {
							if (documents.get(i).getId().equals(topicId)) {
								currentIndex = i;
							}
						}
					}
					refreshContent();
				}
			}
		};

		if (serverIsReachable()) {
			docSvc.getDocuments(language, locale, DocumentType.HELP, getDocumentsCallback);
		}
	}

	private void refreshContent() {
		final AsyncCallback<String> getDocumentCallback = new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(String result) {
				if (result == null) {
					view.getContent().setHTML("Topic not found.");
					view.refresh();
					view.getPreviousButtonVisibility().setVisible(false);
					view.getNextButtonVisibility().setVisible(false);
				} else {
					view.getContent().setHTML(result);
					view.refresh();
					view.getPreviousButtonVisibility().setVisible(currentIndex > 0);
					view.getNextButtonVisibility().setVisible(currentIndex + 1 < documents.size());
				}
			}
		};
		
		if (serverIsReachable()) {
			docSvc.getDocument(topicId, language, locale, getDocumentCallback);
		}
	}


	public boolean serverIsReachable() {
		if (clientFactory.getPhoneGap().getConnection().getType().equalsIgnoreCase("NONE")) {
			view.alertServerCommunicationsProblem("Server Communications Problem"
					, "Please verify that you have a network connection and then try again.", null);
			return false;
		} 
		return true;
	}

	public void checkServerCallIssue(Throwable caught) {
		String text = new String("Unknown issue calling server. Please try again.");
		try {
			throw caught;
		} catch (IncompatibleRemoteServiceException e) {
			text = "This client is not compatible with the server. Please make sure that you are running the latest version of the app or, as appropriate, refresh the browser.";
		} catch (InvocationException e) {
			if (clientFactory.getPhoneGap().getConnection().getType().equalsIgnoreCase("NONE")) {
				text = "Please verify that you have a network connection and then try again.";
			} else {
				text = "The server call did not complete cleanly. Please try again.";
			}
		} catch (Throwable e) {
			text = caught.getLocalizedMessage();
		} finally {
			view.alertServerCommunicationsProblem("Server Communications Problem"
					, text + " If this issue persists, please contact support@voxgift.com.", null);
		}
	}
}
