package com.voxgift.winstonplus.client;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.voxgift.winstonplus.client.activities.messagelist.MessageListActivity;
import com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckPlace;
import com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace;
import com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace;
import com.voxgift.winstonplus.client.activities.welcome.register.RegisterPlace;
import com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordPlace;
import com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace;
import com.voxgift.winstonplus.client.places.HomePlace;

public class DoublePaneAltActivityMapper implements ActivityMapper {

	private final ClientFactory clientFactory;

	public DoublePaneAltActivityMapper(ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	private MessageListActivity messageListActivity;

	private Activity getMessagesActivity() {
		if (messageListActivity == null) {
			messageListActivity = new MessageListActivity(clientFactory);
		}
		return messageListActivity;
	}

	@Override
	public Activity getActivity(Place newPlace) {
		if (newPlace instanceof HomePlace) {
			return getMessagesActivity();
		}

		if (newPlace instanceof ConnectCheckPlace || newPlace instanceof WelcomePlace 
				|| newPlace instanceof RegisterPlace || newPlace instanceof LoginPlace 
				|| newPlace instanceof ResetPasswordPlace || newPlace instanceof HelpPlace) {
			return null;
		}

		return getMessagesActivity();
	}
}
