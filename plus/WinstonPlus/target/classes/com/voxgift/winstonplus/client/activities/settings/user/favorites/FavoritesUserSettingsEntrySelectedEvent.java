package com.voxgift.winstonplus.client.activities.settings.user.favorites;

import com.google.web.bindery.event.shared.Event;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.HandlerRegistration;

public class FavoritesUserSettingsEntrySelectedEvent extends Event<FavoritesUserSettingsEntrySelectedEvent.Handler> {

	public interface Handler {
		void onAnimationSelected(FavoritesUserSettingsEntrySelectedEvent event);
	}

	private static final Type<FavoritesUserSettingsEntrySelectedEvent.Handler> TYPE = new Type<FavoritesUserSettingsEntrySelectedEvent.Handler>();
	private final String entry;

	public static void fire(EventBus eventBus, String string) {
		eventBus.fireEvent(new FavoritesUserSettingsEntrySelectedEvent(string));
	}

	public static HandlerRegistration register(EventBus eventBus, Handler handler) {
		return eventBus.addHandler(TYPE, handler);
	}

	@Override
	public com.google.web.bindery.event.shared.Event.Type<Handler> getAssociatedType() {
		return TYPE;
	}

	protected FavoritesUserSettingsEntrySelectedEvent(String string) {
		this.entry = string;

	}

	@Override
	protected void dispatch(Handler handler) {
		handler.onAnimationSelected(this);

	}

	public static Type<FavoritesUserSettingsEntrySelectedEvent.Handler> getType() {
		return TYPE;
	}

	public String getEntry() {
		return entry;
	}
}
