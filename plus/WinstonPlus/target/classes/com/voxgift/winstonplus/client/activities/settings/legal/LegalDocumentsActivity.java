/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.legal;

import java.util.ArrayList;
import java.util.List;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedEvent;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.DecoratedActivity;
import com.voxgift.winstonplus.client.activities.UIEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.UIEntrySelectedEvent.UIEntry;
import com.voxgift.winstonplus.shared.AppConstants;
import com.voxgift.winstonplus.data.shared.model.DocumentDetails;
import com.voxgift.winstonplus.data.shared.model.DocumentType;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentServiceAsync;

/**
 * @author Tom Giesberg
 * 
 */
public class LegalDocumentsActivity extends DecoratedActivity {
	//private Logger LOGGER = Logger.getLogger(getClass().getName());

	private LegalDocumentsView view;
	private int oldIndex;
	private List<Item> items;

	private RpcDocumentServiceAsync docSvc = GWT.create(RpcDocumentService.class);

	public LegalDocumentsActivity(ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getLegalDocumentsView());

		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) docSvc, appConstants.webAppUrl(), "rpc/document");

		view = clientFactory.getLegalDocumentsView();
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		getClientFactory().getFrameView().showBackbutton(!MGWT.getOsDetection().isAndroid());
		getClientFactory().getFrameView().getBackbuttonText().setText("Settings");
		getClientFactory().getFrameView().getTitle().setText("Legal Documents");
		panel.setWidget(view);

		refreshData(getClientFactory().getUser().getLanguage(), getClientFactory().getUser().getLocale());
	}

	@Override
	public void onDataReady() {
		super.onDataReady();

		view.renderItems(items);

		addHandlerRegistration(view.getList().addCellSelectedHandler(new CellSelectedHandler() {

			@Override
			public void onCellSelected(CellSelectedEvent event) {
				int index = event.getIndex();

				view.setSelectedIndex(oldIndex, false);
				view.setSelectedIndex(index, true);
				oldIndex = index;

				LegalDocumentsEntrySelectedEvent.fire(getEventBus(), items.get(index).getDisplayString(), items.get(index).getId());
			}
		}));

		addHandlerRegistration(getClientFactory().getFrameView().getBackbuttonTarget().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				UIEntrySelectedEvent.fire(getEventBus(), UIEntry.SETTINGS); 
			}
		}));
	}

	private void refreshData(final String language, final String locale) {
		final AsyncCallback<List<DocumentDetails>> callback = new AsyncCallback<List<DocumentDetails>>() {
			public void onFailure(Throwable caught) {
				checkServerCallIssue(caught);
			}

			public void onSuccess(List<DocumentDetails> result) {
				if (result == null) {
					setErrorText("No documents found.");
				} else {
					items = createItems(result);
					onDataReady();
				}
			}
		};
		
		if (serverIsReachable()) {
			docSvc.getDocuments(language, locale, DocumentType.LEGAL, callback);
		}
	}

	/**
	 * @return
	 */
	 private List<Item> createItems(List<DocumentDetails> documents) {
		 ArrayList<Item> list = new ArrayList<Item>();
		 for (DocumentDetails document : documents) {
			 list.add(new Item(document.getTitle(), document.getId()));
		 }
		 return list;
	 }
}
