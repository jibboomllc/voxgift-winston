/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.messagelist;

import com.google.gwt.core.client.GWT;
import com.google.gwt.safehtml.client.SafeHtmlTemplates;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeUri;
import com.google.gwt.safehtml.shared.UriUtils;
import com.googlecode.mgwt.ui.client.widget.celllist.Cell;
import com.voxgift.winstonplus.shared.AppConstants;
import com.voxgift.winstonplus.data.shared.model.MessageDetails;


/**
 * @author Tom Giesberg
 *
 */
public class MessageCell implements Cell<MessageDetails> {

	private static Template TEMPLATE = GWT.create(Template.class);

	public interface Template extends SafeHtmlTemplates {
		String htmlString =
                "<div class='message_cell'>"
	              + "<div class='avatar'><img src='{3}'/></div>"
	              + "<div class='text'>"
		              + "<div class='title'>"
			              + "<div class='user'>{2}</div>"
			              + "<div class='time'>{1}</div>"
		              + "</div>"
		              + "<div class='message'>{0}</div>"
	              + "</div>"
              + "</div>";

		@SafeHtmlTemplates.Template(htmlString)
		SafeHtml content(String message, String elapsedTime, String userName, SafeUri imageUrl);
	}

	@Override
	public void render(SafeHtmlBuilder safeHtmlBuilder, final MessageDetails model) {
		AppConstants appConstants = GWT.create(AppConstants.class);
        // safeHtmlBuilder.append(TEMPLATE.content(SafeHtmlUtils.htmlEscape(model.getMessage()), model.getElapsedTime(), model.getUserName(), model.getUserId(), appConstants.resourcesUrl()));
		// doing the above will mess up quotes
		safeHtmlBuilder.append(TEMPLATE.content(model.getMessage()
				, model.getElapsedTime(), model.getUserName()
				, UriUtils.fromSafeConstant(appConstants.resourcesUrl() + "users/" + model.getUserId() + ".png")));
	}

	@Override
	public boolean canBeSelected(MessageDetails model) {
		return false;
	}

}
