/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.circle.basic;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class BasicCircleSettingsPlace extends Place {

	private String circleId;

	public BasicCircleSettingsPlace(String token) {
		this.circleId = token;
	}

	public String getCircleId() {
		return circleId;
	}

	public static class BasicCircleSettingsPlaceTokenizer implements PlaceTokenizer<BasicCircleSettingsPlace> {
		@Override
		public BasicCircleSettingsPlace getPlace(String token) {
			return new BasicCircleSettingsPlace(token);
		}

		@Override
		public String getToken(BasicCircleSettingsPlace place) {
			return place.getCircleId();
		}

	}
}
