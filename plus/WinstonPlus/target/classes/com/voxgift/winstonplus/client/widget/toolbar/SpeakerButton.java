package com.voxgift.winstonplus.client.widget.toolbar;

import com.googlecode.mgwt.ui.client.MGWTStyle;
import com.googlecode.mgwt.ui.client.widget.buttonbar.ButtonBarButtonBase;
import com.voxgift.winstonplus.client.theme.ExtendedClientBundle;

public class SpeakerButton extends ButtonBarButtonBase {

	public SpeakerButton() {
		super(((ExtendedClientBundle) MGWTStyle.getTheme().getMGWTClientBundle()).getButtonBarSpeakerImage());
	}

}
