package com.voxgift.winstonplus.client.theme.custom;

import com.google.gwt.resources.client.ImageResource;
import com.googlecode.mgwt.ui.client.theme.base.MGWTClientBundleBaseThemeDesktop;
import com.voxgift.winstonplus.client.theme.ExtendedClientBundle;

public interface CustomBundleThemeDesktop extends ExtendedClientBundle, MGWTClientBundleBaseThemeDesktop {

	@Source("resources/tabbar/comment_plus.png")
	public ImageResource tabBarCommentPlusImage();

	@Source("resources/tabbar/delete.png")
	public ImageResource tabBarDeleteImage();

	@Source("resources/tabbar/speaker.png")
	public ImageResource tabBarSpeakerImage();

	@Source("resources/tabbar/star.png")
	public ImageResource tabBarStarImage();

	@Source("resources/tabbar/gear.png")
	public ImageResource tabBarGearImage();

	@Source("resources/tabbar/list.png")
	public ImageResource tabBarListImage();

	@Source("resources/tabbar/pencil.png")
	public ImageResource tabBarPencilImage();

	@Source("resources/toolbar/comment_24.png")
	public ImageResource getButtonBarCommentImage();

	@Source("resources/toolbar/delete_24.png")
	public ImageResource getButtonBarDeleteImage();

	@Source("resources/toolbar/favorites_add_24.png")
	public ImageResource getButtonBarFavoritesAddImage();

	@Source("resources/toolbar/gear_24.png")
	public ImageResource getButtonBarGearImage();

	@Source("resources/toolbar/list.png")
	public ImageResource getButtonBarListImage();

	@Source("resources/toolbar/med_24.png")
	public ImageResource getButtonBarMedImage();

	@Source("resources/toolbar/pencil_24.png")
	public ImageResource getButtonBarPencilImage();

	@Source("resources/toolbar/reload_24.png")
	public ImageResource getButtonBarReloadImage();

	@Source("resources/toolbar/speaker_24.png")
	public ImageResource getButtonBarSpeakerImage();

	@Source("resources/toolbar/star_24.png")
	public ImageResource getButtonBarStarImage();
}
