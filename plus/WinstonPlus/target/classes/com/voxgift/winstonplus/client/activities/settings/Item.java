/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings;

import com.voxgift.winstonplus.client.activities.settings.SettingsEntrySelectedEvent.SettingsEntry;

/**
 * @author Tom Giesberg
 * 
 */
public class Item {
	private String displayString;
	private final SettingsEntry entry;

	public Item(String displayString, SettingsEntry entry) {
		this.displayString = displayString;
		this.entry = entry;

	}

	/**
	 * @return the displayString
	 */
	public String getDisplayString() {
		return displayString;
	}

	public SettingsEntry getEntry() {
		return entry;
	}
}
