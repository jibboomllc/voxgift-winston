/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.circle.user;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

/**
 * @author Tom Giesberg
 *
 */
public class UserCircleSettingsPlace extends Place {

	private String circleId;
	private String userId;

	public UserCircleSettingsPlace(String circleId, String userId) {
		this.circleId = circleId;
		this.userId = userId;
	}

	public String getCircleId() {
		return circleId;
	}

	public String getUserId() {
		return userId;
	}

	public static class UserCircleSettingsPlaceTokenizer implements PlaceTokenizer<UserCircleSettingsPlace> {
	    static final String SEPARATOR = "!";

	    @Override
		public UserCircleSettingsPlace getPlace(String token) {
	        String bits[] = token.split(SEPARATOR);

	        if (bits.length != 2) {
	          return null;
	        }
			return new UserCircleSettingsPlace(bits[0], bits[1]);
		}

		@Override
		public String getToken(UserCircleSettingsPlace place) {
			return place.getCircleId() + SEPARATOR + place.getUserId();
		}

	}
}
