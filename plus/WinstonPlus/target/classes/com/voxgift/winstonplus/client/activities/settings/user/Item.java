/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user;

import com.voxgift.winstonplus.client.activities.settings.user.UserSettingsEntrySelectedEvent.UserSettingsEntry;

/**
 * @author Tom Giesberg
 * 
 */
public class Item {
	private String displayString;
	private final UserSettingsEntry entry;

	public Item(String displayString, UserSettingsEntry entry) {
		this.displayString = displayString;
		this.entry = entry;

	}

	/**
	 * @return the displayString
	 */
	public String getDisplayString() {
		return displayString;
	}

	public UserSettingsEntry getEntry() {
		return entry;
	}
}
