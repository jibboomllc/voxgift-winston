/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings;

import java.util.List;

import com.google.gwt.event.dom.client.HasChangeHandlers;
import com.google.gwt.user.client.ui.HasText;
import com.googlecode.mgwt.dom.client.event.tap.HasTapHandlers;
import com.googlecode.mgwt.ui.client.widget.celllist.HasCellSelectedHandler;
import com.voxgift.winstonplus.client.DecoratedView;

/**
 * @author Tom Giesberg
 * 
 */
public interface SettingsView extends DecoratedView {

	public HasCellSelectedHandler getList();

	public void renderItems(List<Item> items);

	public void setSelectedIndex(int index, boolean selected);
	
	public void addCircleItem(String item, String value);
	public void clearCircleItems();
	public String getSelectedCircleValue();
	public int getSelectedCircleIndex();
	public void setSelectedCircleIndex(int index, boolean selected);
	public HasChangeHandlers getCircleList();

	public HasText getLoggedInAs();
	
	public HasTapHandlers getLogoutButton();

	public HasText getVersion();

	public HasTapHandlers getHelpButton();

}
