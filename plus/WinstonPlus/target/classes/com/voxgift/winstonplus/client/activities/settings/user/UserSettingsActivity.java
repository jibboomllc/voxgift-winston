/*
 * Copyright 2013 Jibboom, LLC
 * All rights reserved.
 */
package com.voxgift.winstonplus.client.activities.settings.user;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.googlecode.gwtphonegap.client.util.PhonegapUtil;
import com.googlecode.mgwt.dom.client.event.tap.TapEvent;
import com.googlecode.mgwt.dom.client.event.tap.TapHandler;
import com.googlecode.mgwt.ui.client.MGWT;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedEvent;
import com.googlecode.mgwt.ui.client.widget.celllist.CellSelectedHandler;
import com.voxgift.winstonplus.client.ClientFactory;
import com.voxgift.winstonplus.client.DecoratedActivity;
import com.voxgift.winstonplus.client.activities.UIEntrySelectedEvent;
import com.voxgift.winstonplus.client.activities.UIEntrySelectedEvent.UIEntry;
import com.voxgift.winstonplus.client.activities.settings.user.UserSettingsEntrySelectedEvent.UserSettingsEntry;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserServiceAsync;
import com.voxgift.winstonplus.shared.AppConstants;

/**
 * @author Tom Giesberg
 * 
 */
public class UserSettingsActivity extends DecoratedActivity {

	private UserSettingsView view;
	private int oldIndex;
	private List<Item> items;

	private RpcUserServiceAsync userSvc = GWT.create(RpcUserService.class);

	public UserSettingsActivity(ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getUserSettingsView());

		AppConstants appConstants = GWT.create(AppConstants.class);
		PhonegapUtil.prepareService((ServiceDefTarget) userSvc, appConstants.webAppUrl(), "rpc/user");
		view = clientFactory.getUserSettingsView();
	}

	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);

		getClientFactory().getFrameView().showBackbutton(!MGWT.getOsDetection().isAndroid());
		getClientFactory().getFrameView().getBackbuttonText().setText("Settings");
		getClientFactory().getFrameView().getTitle().setText("User Settings");
		panel.setWidget(view);

		refreshData(getClientFactory().getUser().getId());
	}

	@Override
	public void onDataReady() {
		super.onDataReady();

		items = createItems();
		view.renderItems(items);

		addHandlerRegistration(view.getList().addCellSelectedHandler(new CellSelectedHandler() {

			@Override
			public void onCellSelected(CellSelectedEvent event) {
				int index = event.getIndex();

				view.setSelectedIndex(oldIndex, false);
				view.setSelectedIndex(index, true);
				oldIndex = index;

				UserSettingsEntrySelectedEvent.fire(getEventBus(), items.get(index).getEntry());
			}
		}));

		addHandlerRegistration(getClientFactory().getFrameView().getBackbuttonTarget().addTapHandler(new TapHandler() {

			@Override
			public void onTap(TapEvent event) {
				UIEntrySelectedEvent.fire(getEventBus(), UIEntry.SETTINGS); 
			}
		}));
	}

	private void refreshData(final String id) {
		onDataReady();
	}

	/**
	 * @return
	 */
	 private List<Item> createItems() {
		 ArrayList<Item> list = new ArrayList<Item>();
		 list.add(new Item("Basic", UserSettingsEntry.BASIC));
		 list.add(new Item("Image", UserSettingsEntry.IMAGE));
		 list.add(new Item("Favorites", UserSettingsEntry.FAVORITES));
		 return list;
	 }
}
