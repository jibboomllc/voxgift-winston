package com.googlecode.mgwt.mvp.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class MVPBundle_android_tablet_safari_default_InlineClientBundleGenerator implements com.googlecode.mgwt.mvp.client.resources.MVPBundle {
  private static MVPBundle_android_tablet_safari_default_InlineClientBundleGenerator _instance0 = new MVPBundle_android_tablet_safari_default_InlineClientBundleGenerator();
  private void animationCssInitializer() {
    animationCss = new com.googlecode.mgwt.mvp.client.resources.AnimationCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "animationCss";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GEG3RTUDFI{position:" + ("absolute")  + ";width:" + ("100%")  + ";height:" + ("100%")  + ";overflow:" + ("hidden")  + ";}.GEG3RTUDEI{position:" + ("absolute")  + ";top:" + ("0")  + ";right:" + ("0")  + ";left:" + ("0")  + ";bottom:" + ("0")  + ";overflow:" + ("hidden")  + ";}@-webkit-keyframes donothing {\n    from { opacity: 0.9; }\n    to { opacity: 1; }\n}.GEG3RTUDJI{-webkit-animation-timing-function:") + (("ease-in-out")  + ";-webkit-animation-duration:" + ("300ms")  + ";-webkit-animation-fill-mode:" + ("both")  + ";z-index:" + ("10")  + ";}.GEG3RTUDKI{-webkit-animation-timing-function:" + ("ease-in-out")  + ";-webkit-animation-duration:" + ("300ms")  + ";-webkit-animation-fill-mode:" + ("both")  + ";z-index:" + ("0")  + " !important;}.GEG3RTUDNI.GEG3RTUDJI{-webkit-animation-name:" + ("slideinfromright")  + ";}.GEG3RTUDNI.GEG3RTUDKI{-webkit-animation-name:" + ("slideouttoleft")  + ";}.GEG3RTUDNI.GEG3RTUDJI.GEG3RTUDMI{-webkit-animation-name:" + ("slideinfromleft") ) + (";}.GEG3RTUDNI.GEG3RTUDKI.GEG3RTUDMI{-webkit-animation-name:" + ("slideouttoright")  + ";}@-webkit-keyframes slideinfromright {\n    from { -webkit-transform: translateX(100%); }\n    to { -webkit-transform: translateX(0); }\n}@-webkit-keyframes slideinfromleft {\n    from { -webkit-transform: translateX(-100%); }\n    to { -webkit-transform: translateX(0); }\n}@-webkit-keyframes slideouttoleft {\n    from { -webkit-transform: translateX(0); }\n    to { -webkit-transform: translateX(-100%); }\n}@-webkit-keyframes slideouttoright {\n    from { -webkit-transform: translateX(0); }\n    to { -webkit-transform: translateX(100%); }\n}.GEG3RTUDII{-webkit-animation-duration:" + ("0.65s")  + ";-webkit-backface-visibility:" + ("hidden")  + ";}.GEG3RTUDII.GEG3RTUDJI{-webkit-animation-name:" + ("flipinfromleft")  + ";}.GEG3RTUDII.GEG3RTUDKI{-webkit-animation-name:" + ("flipouttoleft")  + ";}.GEG3RTUDII.GEG3RTUDJI.GEG3RTUDMI{-webkit-animation-name:" + ("flipinfromright")  + ";}.GEG3RTUDII.GEG3RTUDKI.GEG3RTUDMI{-webkit-animation-name:" + ("flipouttoright")  + ";}@-webkit-keyframes flipinfromright {\n    from { -webkit-transform: rotateY(-180deg) scale(.8); }\n    to { -webkit-transform: rotateY(0) scale(1); }\n}@-webkit-keyframes flipinfromleft {\n    from { -webkit-transform: rotateY(180deg) scale(.8); }\n    to { -webkit-transform: rotateY(0) scale(1); }\n}@-webkit-keyframes flipouttoleft {\n    from { -webkit-transform: rotateY(0) scale(1); }\n    to { -webkit-transform: rotateY(-180deg) scale(.8); }\n}@-webkit-keyframes flipouttoright {\n    from { -webkit-transform: rotateY(0) scale(1); }\n    to { -webkit-transform: rotateY(180deg) scale(.8); }\n}@-webkit-keyframes fadein {\n    from { opacity: 0; }\n    to { opacity: 1; }\n}@-webkit-keyframes fadeout {\n    from { opacity: 1; }\n    to { opacity: 0; }\n}.GEG3RTUDHI.GEG3RTUDJI{z-index:" + ("10")  + ";-webkit-animation-name:" + ("fadein")  + ";}.GEG3RTUDHI.GEG3RTUDKI{z-index:" + ("8")  + ";-webkit-animation-name:") + (("fadeout")  + ";}.GEG3RTUDGI.GEG3RTUDJI{-webkit-animation-name:" + ("fadein")  + ";}.GEG3RTUDGI.GEG3RTUDKI{-webkit-animation-name:" + ("fadeout")  + ";}.GEG3RTUDOI.GEG3RTUDJI{-webkit-animation-name:" + ("slideupfrombottom")  + ";z-index:" + ("10")  + ";}.GEG3RTUDOI.GEG3RTUDKI{-webkit-animation-name:" + ("slideupfrommiddle")  + ";z-index:" + ("0")  + ";}.GEG3RTUDOI.GEG3RTUDKI.GEG3RTUDMI{z-index:" + ("10")  + ";-webkit-animation-name:" + ("slidedownfrommiddle")  + ";}.GEG3RTUDOI.GEG3RTUDJI.GEG3RTUDMI{z-index:" + ("0")  + ";-webkit-animation-name:" + ("slidedownfromtop") ) + (";}@-webkit-keyframes slideupfrombottom {\n    from { -webkit-transform: translateY(100%); }\n    to { -webkit-transform: translateY(0); }\n}@-webkit-keyframes slidedownfrommiddle {\n    from { -webkit-transform: translateY(0); }\n    to { -webkit-transform: translateY(100%); }\n}@-webkit-keyframes slideupfrommiddle {\n    from { -webkit-transform: translateY(0); }\n    to { -webkit-transform: translateY(-100%); }\n}@-webkit-keyframes slidedownfromtop {\n    from { -webkit-transform: translateY(-100%); }\n    to { -webkit-transform: translateY(0%); }\n}.GEG3RTUDPI{-webkit-transform:" + ("perspective(" + "800" + ")")  + ";-webkit-animation-duration:" + ("0.7s")  + ";}.GEG3RTUDPI.GEG3RTUDKI{-webkit-animation-name:" + ("swapouttoleft")  + ";}.GEG3RTUDPI.GEG3RTUDJI{-webkit-animation-name:" + ("swapinfromright")  + ";}.GEG3RTUDPI.GEG3RTUDKI.GEG3RTUDMI{-webkit-animation-name:" + ("swapouttoright")  + ";}.GEG3RTUDPI.GEG3RTUDJI.GEG3RTUDMI{-webkit-animation-name:" + ("swapinfromleft")  + ";}@-webkit-keyframes swapouttoright {\n    0% {\n        -webkit-transform: translate3d(0px, 0px, 0px) rotateY(0deg);\n        -webkit-animation-timing-function: ease-in-out;\n    }\n    50% {\n    \n        -webkit-transform: translate3d(-180px, 0px, -400px) rotateY(20deg);\n        -webkit-animation-timing-function: ease-in;\n        opacity: 0.8;\n       \n    }\n    100% {\n        -webkit-transform:  translate3d(0px, 0px, -800px) rotateY(70deg);\n         opacity: 0;\n    }\n}@-webkit-keyframes swapouttoleft {\n    0% {\n        -webkit-transform: translate3d(0px, 0px, 0px) rotateY(0deg);\n        -webkit-animation-timing-function: ease-in-out;\n    }\n    50% {\n        -webkit-transform:  translate3d(180px, 0px, -400px) rotateY(-20deg);\n        -webkit-animation-timing-function: ease-in;\n        opacity: 0.8;\n    }\n    100% {\n        -webkit-transform: translate3d(0px, 0px, -800px) rotateY(-70deg);\n        opacity: 0;\n    }\n}@-webkit-keyframes swapinfromright {\n    0% {\n        -webkit-transform: translate3d(0px, 0px, -800px) rotateY(70deg);\n        -webkit-animation-timing-function: ease-out;\n    }\n    50% {\n        -webkit-transform: translate3d(-180px, 0px, -400px) rotateY(20deg);\n        -webkit-animation-timing-function: ease-in-out;\n    }\n    100% {\n        -webkit-transform: translate3d(0px, 0px, 0px) rotateY(0deg);\n    }\n}@-webkit-keyframes swapinfromleft {\n    0% {\n        -webkit-transform: translate3d(0px, 0px, -800px) rotateY(-70deg);\n        -webkit-animation-timing-function: ease-out;\n    }\n    50% {\n        -webkit-transform: translate3d(180px, 0px, -400px) rotateY(-20deg);\n        -webkit-animation-timing-function: ease-in-out;\n    }\n    100% {\n        -webkit-transform: translate3d(0px, 0px, 0px) rotateY(0deg);\n    }\n}.GEG3RTUDLI.GEG3RTUDJI{-webkit-animation-name:" + ("popin")  + ";}.GEG3RTUDLI.GEG3RTUDKI{-webkit-animation-name:" + ("popout")  + ";}@-webkit-keyframes popin {\n    from {\n        -webkit-transform: scale(.3);\n        opacity: 0;\n    }\n    to {\n        -webkit-transform: scale(1);\n        opacity: 1;\n    }\n}@-webkit-keyframes popout {\n    from {\n        -webkit-transform: scale(1);\n        opacity: 1;\n    }\n    to {\n        -webkit-transform: scale(.3);\n        opacity: 0;\n    }\n}")) : ((".GEG3RTUDFI{position:" + ("absolute")  + ";width:" + ("100%")  + ";height:" + ("100%")  + ";overflow:" + ("hidden")  + ";}.GEG3RTUDEI{position:" + ("absolute")  + ";top:" + ("0")  + ";left:" + ("0")  + ";right:" + ("0")  + ";bottom:" + ("0")  + ";overflow:" + ("hidden")  + ";}@-webkit-keyframes donothing {\n    from { opacity: 0.9; }\n    to { opacity: 1; }\n}.GEG3RTUDJI{-webkit-animation-timing-function:") + (("ease-in-out")  + ";-webkit-animation-duration:" + ("300ms")  + ";-webkit-animation-fill-mode:" + ("both")  + ";z-index:" + ("10")  + ";}.GEG3RTUDKI{-webkit-animation-timing-function:" + ("ease-in-out")  + ";-webkit-animation-duration:" + ("300ms")  + ";-webkit-animation-fill-mode:" + ("both")  + ";z-index:" + ("0")  + " !important;}.GEG3RTUDNI.GEG3RTUDJI{-webkit-animation-name:" + ("slideinfromright")  + ";}.GEG3RTUDNI.GEG3RTUDKI{-webkit-animation-name:" + ("slideouttoleft")  + ";}.GEG3RTUDNI.GEG3RTUDJI.GEG3RTUDMI{-webkit-animation-name:" + ("slideinfromleft") ) + (";}.GEG3RTUDNI.GEG3RTUDKI.GEG3RTUDMI{-webkit-animation-name:" + ("slideouttoright")  + ";}@-webkit-keyframes slideinfromright {\n    from { -webkit-transform: translateX(100%); }\n    to { -webkit-transform: translateX(0); }\n}@-webkit-keyframes slideinfromleft {\n    from { -webkit-transform: translateX(-100%); }\n    to { -webkit-transform: translateX(0); }\n}@-webkit-keyframes slideouttoleft {\n    from { -webkit-transform: translateX(0); }\n    to { -webkit-transform: translateX(-100%); }\n}@-webkit-keyframes slideouttoright {\n    from { -webkit-transform: translateX(0); }\n    to { -webkit-transform: translateX(100%); }\n}.GEG3RTUDII{-webkit-animation-duration:" + ("0.65s")  + ";-webkit-backface-visibility:" + ("hidden")  + ";}.GEG3RTUDII.GEG3RTUDJI{-webkit-animation-name:" + ("flipinfromleft")  + ";}.GEG3RTUDII.GEG3RTUDKI{-webkit-animation-name:" + ("flipouttoleft")  + ";}.GEG3RTUDII.GEG3RTUDJI.GEG3RTUDMI{-webkit-animation-name:" + ("flipinfromright")  + ";}.GEG3RTUDII.GEG3RTUDKI.GEG3RTUDMI{-webkit-animation-name:" + ("flipouttoright")  + ";}@-webkit-keyframes flipinfromright {\n    from { -webkit-transform: rotateY(-180deg) scale(.8); }\n    to { -webkit-transform: rotateY(0) scale(1); }\n}@-webkit-keyframes flipinfromleft {\n    from { -webkit-transform: rotateY(180deg) scale(.8); }\n    to { -webkit-transform: rotateY(0) scale(1); }\n}@-webkit-keyframes flipouttoleft {\n    from { -webkit-transform: rotateY(0) scale(1); }\n    to { -webkit-transform: rotateY(-180deg) scale(.8); }\n}@-webkit-keyframes flipouttoright {\n    from { -webkit-transform: rotateY(0) scale(1); }\n    to { -webkit-transform: rotateY(180deg) scale(.8); }\n}@-webkit-keyframes fadein {\n    from { opacity: 0; }\n    to { opacity: 1; }\n}@-webkit-keyframes fadeout {\n    from { opacity: 1; }\n    to { opacity: 0; }\n}.GEG3RTUDHI.GEG3RTUDJI{z-index:" + ("10")  + ";-webkit-animation-name:" + ("fadein")  + ";}.GEG3RTUDHI.GEG3RTUDKI{z-index:" + ("8")  + ";-webkit-animation-name:") + (("fadeout")  + ";}.GEG3RTUDGI.GEG3RTUDJI{-webkit-animation-name:" + ("fadein")  + ";}.GEG3RTUDGI.GEG3RTUDKI{-webkit-animation-name:" + ("fadeout")  + ";}.GEG3RTUDOI.GEG3RTUDJI{-webkit-animation-name:" + ("slideupfrombottom")  + ";z-index:" + ("10")  + ";}.GEG3RTUDOI.GEG3RTUDKI{-webkit-animation-name:" + ("slideupfrommiddle")  + ";z-index:" + ("0")  + ";}.GEG3RTUDOI.GEG3RTUDKI.GEG3RTUDMI{z-index:" + ("10")  + ";-webkit-animation-name:" + ("slidedownfrommiddle")  + ";}.GEG3RTUDOI.GEG3RTUDJI.GEG3RTUDMI{z-index:" + ("0")  + ";-webkit-animation-name:" + ("slidedownfromtop") ) + (";}@-webkit-keyframes slideupfrombottom {\n    from { -webkit-transform: translateY(100%); }\n    to { -webkit-transform: translateY(0); }\n}@-webkit-keyframes slidedownfrommiddle {\n    from { -webkit-transform: translateY(0); }\n    to { -webkit-transform: translateY(100%); }\n}@-webkit-keyframes slideupfrommiddle {\n    from { -webkit-transform: translateY(0); }\n    to { -webkit-transform: translateY(-100%); }\n}@-webkit-keyframes slidedownfromtop {\n    from { -webkit-transform: translateY(-100%); }\n    to { -webkit-transform: translateY(0%); }\n}.GEG3RTUDPI{-webkit-transform:" + ("perspective(" + "800" + ")")  + ";-webkit-animation-duration:" + ("0.7s")  + ";}.GEG3RTUDPI.GEG3RTUDKI{-webkit-animation-name:" + ("swapouttoleft")  + ";}.GEG3RTUDPI.GEG3RTUDJI{-webkit-animation-name:" + ("swapinfromright")  + ";}.GEG3RTUDPI.GEG3RTUDKI.GEG3RTUDMI{-webkit-animation-name:" + ("swapouttoright")  + ";}.GEG3RTUDPI.GEG3RTUDJI.GEG3RTUDMI{-webkit-animation-name:" + ("swapinfromleft")  + ";}@-webkit-keyframes swapouttoright {\n    0% {\n        -webkit-transform: translate3d(0px, 0px, 0px) rotateY(0deg);\n        -webkit-animation-timing-function: ease-in-out;\n    }\n    50% {\n    \n        -webkit-transform: translate3d(-180px, 0px, -400px) rotateY(20deg);\n        -webkit-animation-timing-function: ease-in;\n        opacity: 0.8;\n       \n    }\n    100% {\n        -webkit-transform:  translate3d(0px, 0px, -800px) rotateY(70deg);\n         opacity: 0;\n    }\n}@-webkit-keyframes swapouttoleft {\n    0% {\n        -webkit-transform: translate3d(0px, 0px, 0px) rotateY(0deg);\n        -webkit-animation-timing-function: ease-in-out;\n    }\n    50% {\n        -webkit-transform:  translate3d(180px, 0px, -400px) rotateY(-20deg);\n        -webkit-animation-timing-function: ease-in;\n        opacity: 0.8;\n    }\n    100% {\n        -webkit-transform: translate3d(0px, 0px, -800px) rotateY(-70deg);\n        opacity: 0;\n    }\n}@-webkit-keyframes swapinfromright {\n    0% {\n        -webkit-transform: translate3d(0px, 0px, -800px) rotateY(70deg);\n        -webkit-animation-timing-function: ease-out;\n    }\n    50% {\n        -webkit-transform: translate3d(-180px, 0px, -400px) rotateY(20deg);\n        -webkit-animation-timing-function: ease-in-out;\n    }\n    100% {\n        -webkit-transform: translate3d(0px, 0px, 0px) rotateY(0deg);\n    }\n}@-webkit-keyframes swapinfromleft {\n    0% {\n        -webkit-transform: translate3d(0px, 0px, -800px) rotateY(-70deg);\n        -webkit-animation-timing-function: ease-out;\n    }\n    50% {\n        -webkit-transform: translate3d(180px, 0px, -400px) rotateY(-20deg);\n        -webkit-animation-timing-function: ease-in-out;\n    }\n    100% {\n        -webkit-transform: translate3d(0px, 0px, 0px) rotateY(0deg);\n    }\n}.GEG3RTUDLI.GEG3RTUDJI{-webkit-animation-name:" + ("popin")  + ";}.GEG3RTUDLI.GEG3RTUDKI{-webkit-animation-name:" + ("popout")  + ";}@-webkit-keyframes popin {\n    from {\n        -webkit-transform: scale(.3);\n        opacity: 0;\n    }\n    to {\n        -webkit-transform: scale(1);\n        opacity: 1;\n    }\n}@-webkit-keyframes popout {\n    from {\n        -webkit-transform: scale(1);\n        opacity: 1;\n    }\n    to {\n        -webkit-transform: scale(.3);\n        opacity: 0;\n    }\n}"));
      }
      public java.lang.String display(){
        return "GEG3RTUDEI";
      }
      public java.lang.String displayContainer(){
        return "GEG3RTUDFI";
      }
      public java.lang.String dissolve(){
        return "GEG3RTUDGI";
      }
      public java.lang.String fade(){
        return "GEG3RTUDHI";
      }
      public java.lang.String flip(){
        return "GEG3RTUDII";
      }
      public java.lang.String in(){
        return "GEG3RTUDJI";
      }
      public java.lang.String out(){
        return "GEG3RTUDKI";
      }
      public java.lang.String pop(){
        return "GEG3RTUDLI";
      }
      public java.lang.String reverse(){
        return "GEG3RTUDMI";
      }
      public java.lang.String slide(){
        return "GEG3RTUDNI";
      }
      public java.lang.String slideup(){
        return "GEG3RTUDOI";
      }
      public java.lang.String swap(){
        return "GEG3RTUDPI";
      }
    }
    ;
  }
  private static class animationCssInitializer {
    static {
      _instance0.animationCssInitializer();
    }
    static com.googlecode.mgwt.mvp.client.resources.AnimationCss get() {
      return animationCss;
    }
  }
  public com.googlecode.mgwt.mvp.client.resources.AnimationCss animationCss() {
    return animationCssInitializer.get();
  }
  private void transitionCssInitializer() {
    transitionCss = new com.googlecode.mgwt.mvp.client.resources.TransistionCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "transitionCss";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GEG3RTUDAJ{position:" + ("absolute")  + ";top:" + ("0")  + ";right:" + ("0")  + ";left:" + ("0")  + ";bottom:" + ("0")  + ";overflow:" + ("hidden")  + ";}.GEG3RTUDBJ{position:" + ("absolute")  + ";width:" + ("100%")  + ";height:" + ("100%")  + ";overflow:" + ("hidden")  + ";}.GEG3RTUDKJ,.GEG3RTUDLJ{-webkit-transition-property:") + (("-webkit-transform")  + ";-webkit-transition-duration:" + ("350ms")  + ";-webkit-transition-timing-function:" + ("ease-in-out")  + ";}.GEG3RTUDKJ.GEG3RTUDGJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "100%" + ")")  + ";}.GEG3RTUDKJ.GEG3RTUDGJ.GEG3RTUDDJ,.GEG3RTUDKJ.GEG3RTUDHJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "0" + ")")  + ";}.GEG3RTUDKJ.GEG3RTUDHJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "-100%" + ")")  + ";}.GEG3RTUDKJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "0" + ")")  + ";}.GEG3RTUDKJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "100%" + ")")  + ";}.GEG3RTUDKJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "-100%" + ")")  + ";}.GEG3RTUDKJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "0" + ")")  + ";}.GEG3RTUDLJ.GEG3RTUDGJ.GEG3RTUDMJ{-webkit-transform:" + ("translateY(" + "100%" + ")") ) + (";}.GEG3RTUDLJ.GEG3RTUDGJ.GEG3RTUDDJ,.GEG3RTUDLJ.GEG3RTUDHJ.GEG3RTUDMJ{-webkit-transform:" + ("translateY(" + "0" + ")")  + ";}.GEG3RTUDLJ.GEG3RTUDHJ.GEG3RTUDDJ{-webkit-transform:" + ("translateY(" + "-100%" + ")")  + ";}.GEG3RTUDLJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("translateY(" + "0" + ")")  + ";}.GEG3RTUDLJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("translateY(" + "100%" + ")")  + ";}.GEG3RTUDLJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("translateY(" + "-100%" + ")")  + ";}.GEG3RTUDLJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("translateY(" + "0" + ")")  + ";}.GEG3RTUDCJ{-webkit-transition-property:" + ("opacity")  + ";-webkit-transition-duration:" + ("350ms")  + ";-webkit-transition-timing-function:" + ("ease-in-out")  + ";}.GEG3RTUDCJ.GEG3RTUDGJ.GEG3RTUDMJ{opacity:" + ("0")  + ";}.GEG3RTUDCJ.GEG3RTUDGJ.GEG3RTUDDJ,.GEG3RTUDCJ.GEG3RTUDHJ.GEG3RTUDMJ{opacity:") + (("1")  + ";}.GEG3RTUDCJ.GEG3RTUDHJ.GEG3RTUDDJ{opacity:" + ("0")  + ";}.GEG3RTUDCJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{opacity:" + ("1")  + ";}.GEG3RTUDCJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ,.GEG3RTUDCJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{opacity:" + ("0")  + ";}.GEG3RTUDCJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{opacity:" + ("1")  + ";}.GEG3RTUDFJ{-webkit-transition-property:" + ("-webkit-transform"+ ","+ " " +"opacity")  + ";-webkit-transition-duration:" + ("350ms"+ ","+ " " +"350ms")  + ";-webkit-transition-timing-function:" + ("ease-in-out"+ ","+ " " +"ease-in-out")  + ";-webkit-perspective:" + ("800")  + ";-webkit-transform-style:" + ("preserve-3d")  + ";-webkit-backface-visibility:" + ("hidden") ) + (";}.GEG3RTUDFJ.GEG3RTUDGJ.GEG3RTUDMJ{-webkit-transform:" + ("rotateY(" + "180deg" + ")"+ " " +"scale(" + "0.8" + ")")  + ";z-index:" + ("1")  + ";opacity:" + ("1")  + ";}.GEG3RTUDFJ.GEG3RTUDGJ.GEG3RTUDDJ{-webkit-transform:" + ("rotateY(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("1")  + ";opacity:" + ("1")  + ";}.GEG3RTUDFJ.GEG3RTUDHJ.GEG3RTUDMJ{-webkit-transform:" + ("rotateY(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("0")  + ";opacity:" + ("0")  + ";}.GEG3RTUDFJ.GEG3RTUDHJ.GEG3RTUDDJ{-webkit-transform:" + ("rotateY(" + "-180deg" + ")"+ " " +"scale(" + "0.8" + ")")  + ";z-index:") + (("0")  + ";opacity:" + ("0")  + ";}.GEG3RTUDFJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("rotateY(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("0")  + ";opacity:" + ("0")  + ";}.GEG3RTUDFJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("rotateY(" + "180deg" + ")"+ " " +"scale(" + "0.8" + ")")  + ";z-index:" + ("0")  + ";opacity:" + ("0")  + ";}.GEG3RTUDFJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("rotateY(" + "-180deg" + ")"+ " " +"scale(" + "0.8" + ")")  + ";z-index:" + ("1")  + ";opacity:" + ("1") ) + (";}.GEG3RTUDFJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("rotateY(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("1")  + ";opacity:" + ("1")  + ";}.GEG3RTUDNJ{-webkit-transition-property:" + ("-webkit-transform")  + ";-webkit-transition-duration:" + ("350ms"+ ","+ " " +"350ms")  + ";-webkit-transition-timing-function:" + ("ease-in-out")  + ";}.GEG3RTUDNJ.GEG3RTUDGJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "100%" + ")"+ " " +"scale(" + "0.3" + ")")  + ";z-index:" + ("1")  + ";}.GEG3RTUDNJ.GEG3RTUDGJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("1")  + ";}.GEG3RTUDNJ.GEG3RTUDHJ.GEG3RTUDMJ{-webkit-transform:") + (("translateX(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("0")  + ";}.GEG3RTUDNJ.GEG3RTUDHJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "-100%" + ")"+ " " +"scale(" + "0.3" + ")")  + ";z-index:" + ("0")  + ";}.GEG3RTUDNJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("0")  + ";}.GEG3RTUDNJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "100%" + ")"+ " " +"scale(" + "0.3" + ")")  + ";z-index:" + ("0")  + ";}.GEG3RTUDNJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "-100%" + ")"+ " " +"scale(" + "0.3" + ")")  + ";}.GEG3RTUDNJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";}.GEG3RTUDIJ{-webkit-transition-property:" + ("-webkit-transform"+ ","+ " " +"opacity") ) + (";-webkit-transition-duration:" + ("350ms"+ ","+ " " +"350ms")  + ";-webkit-transition-timing-function:" + ("ease-in-out")  + ";}.GEG3RTUDIJ.GEG3RTUDGJ.GEG3RTUDMJ{-webkit-transform:" + ("scale(" + "0.3" + ")")  + ";opacity:" + ("0")  + ";z-index:" + ("1")  + ";}.GEG3RTUDIJ.GEG3RTUDGJ.GEG3RTUDDJ{-webkit-transform:" + ("scale(" + "1" + ")")  + ";opacity:" + ("1")  + ";z-index:" + ("0")  + ";}.GEG3RTUDIJ.GEG3RTUDHJ.GEG3RTUDMJ{opacity:" + ("1")  + ";-webkit-transform:" + ("scale(" + "1" + ")")  + ";z-index:") + (("0")  + ";}.GEG3RTUDIJ.GEG3RTUDHJ.GEG3RTUDDJ{-webkit-transform:" + ("scale(" + "0.3" + ")")  + ";opacity:" + ("0")  + ";z-index:" + ("0")  + ";}.GEG3RTUDIJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("scale(" + "1" + ")")  + ";opacity:" + ("1")  + ";z-index:" + ("1")  + ";}.GEG3RTUDIJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("scale(" + "0.3" + ")")  + ";opacity:" + ("0")  + ";z-index:" + ("1")  + ";}.GEG3RTUDIJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("scale(" + "0.3" + ")") ) + (";opacity:" + ("0")  + ";z-index:" + ("0")  + ";}.GEG3RTUDIJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("scale(" + "1" + ")")  + ";opacity:" + ("1")  + ";z-index:" + ("0")  + ";}.GEG3RTUDEJ{-webkit-transition-property:" + ("opacity")  + ";-webkit-transition-duration:" + ("350ms")  + ";-webkit-transition-timing-function:" + ("ease-in-out")  + ";}.GEG3RTUDEJ.GEG3RTUDGJ.GEG3RTUDMJ{opacity:" + ("0")  + ";}.GEG3RTUDEJ.GEG3RTUDGJ.GEG3RTUDDJ,.GEG3RTUDEJ.GEG3RTUDHJ.GEG3RTUDMJ{opacity:" + ("1")  + ";}.GEG3RTUDEJ.GEG3RTUDHJ.GEG3RTUDDJ{opacity:") + (("0")  + ";}.GEG3RTUDEJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{opacity:" + ("1")  + ";}.GEG3RTUDEJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ,.GEG3RTUDEJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{opacity:" + ("0")  + ";}.GEG3RTUDEJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{opacity:" + ("1")  + ";}")) : ((".GEG3RTUDAJ{position:" + ("absolute")  + ";top:" + ("0")  + ";left:" + ("0")  + ";right:" + ("0")  + ";bottom:" + ("0")  + ";overflow:" + ("hidden")  + ";}.GEG3RTUDBJ{position:" + ("absolute")  + ";width:" + ("100%")  + ";height:" + ("100%")  + ";overflow:" + ("hidden")  + ";}.GEG3RTUDKJ,.GEG3RTUDLJ{-webkit-transition-property:") + (("-webkit-transform")  + ";-webkit-transition-duration:" + ("350ms")  + ";-webkit-transition-timing-function:" + ("ease-in-out")  + ";}.GEG3RTUDKJ.GEG3RTUDGJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "100%" + ")")  + ";}.GEG3RTUDKJ.GEG3RTUDGJ.GEG3RTUDDJ,.GEG3RTUDKJ.GEG3RTUDHJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "0" + ")")  + ";}.GEG3RTUDKJ.GEG3RTUDHJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "-100%" + ")")  + ";}.GEG3RTUDKJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "0" + ")")  + ";}.GEG3RTUDKJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "100%" + ")")  + ";}.GEG3RTUDKJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "-100%" + ")")  + ";}.GEG3RTUDKJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "0" + ")")  + ";}.GEG3RTUDLJ.GEG3RTUDGJ.GEG3RTUDMJ{-webkit-transform:" + ("translateY(" + "100%" + ")") ) + (";}.GEG3RTUDLJ.GEG3RTUDGJ.GEG3RTUDDJ,.GEG3RTUDLJ.GEG3RTUDHJ.GEG3RTUDMJ{-webkit-transform:" + ("translateY(" + "0" + ")")  + ";}.GEG3RTUDLJ.GEG3RTUDHJ.GEG3RTUDDJ{-webkit-transform:" + ("translateY(" + "-100%" + ")")  + ";}.GEG3RTUDLJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("translateY(" + "0" + ")")  + ";}.GEG3RTUDLJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("translateY(" + "100%" + ")")  + ";}.GEG3RTUDLJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("translateY(" + "-100%" + ")")  + ";}.GEG3RTUDLJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("translateY(" + "0" + ")")  + ";}.GEG3RTUDCJ{-webkit-transition-property:" + ("opacity")  + ";-webkit-transition-duration:" + ("350ms")  + ";-webkit-transition-timing-function:" + ("ease-in-out")  + ";}.GEG3RTUDCJ.GEG3RTUDGJ.GEG3RTUDMJ{opacity:" + ("0")  + ";}.GEG3RTUDCJ.GEG3RTUDGJ.GEG3RTUDDJ,.GEG3RTUDCJ.GEG3RTUDHJ.GEG3RTUDMJ{opacity:") + (("1")  + ";}.GEG3RTUDCJ.GEG3RTUDHJ.GEG3RTUDDJ{opacity:" + ("0")  + ";}.GEG3RTUDCJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{opacity:" + ("1")  + ";}.GEG3RTUDCJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ,.GEG3RTUDCJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{opacity:" + ("0")  + ";}.GEG3RTUDCJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{opacity:" + ("1")  + ";}.GEG3RTUDFJ{-webkit-transition-property:" + ("-webkit-transform"+ ","+ " " +"opacity")  + ";-webkit-transition-duration:" + ("350ms"+ ","+ " " +"350ms")  + ";-webkit-transition-timing-function:" + ("ease-in-out"+ ","+ " " +"ease-in-out")  + ";-webkit-perspective:" + ("800")  + ";-webkit-transform-style:" + ("preserve-3d")  + ";-webkit-backface-visibility:" + ("hidden") ) + (";}.GEG3RTUDFJ.GEG3RTUDGJ.GEG3RTUDMJ{-webkit-transform:" + ("rotateY(" + "180deg" + ")"+ " " +"scale(" + "0.8" + ")")  + ";z-index:" + ("1")  + ";opacity:" + ("1")  + ";}.GEG3RTUDFJ.GEG3RTUDGJ.GEG3RTUDDJ{-webkit-transform:" + ("rotateY(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("1")  + ";opacity:" + ("1")  + ";}.GEG3RTUDFJ.GEG3RTUDHJ.GEG3RTUDMJ{-webkit-transform:" + ("rotateY(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("0")  + ";opacity:" + ("0")  + ";}.GEG3RTUDFJ.GEG3RTUDHJ.GEG3RTUDDJ{-webkit-transform:" + ("rotateY(" + "-180deg" + ")"+ " " +"scale(" + "0.8" + ")")  + ";z-index:") + (("0")  + ";opacity:" + ("0")  + ";}.GEG3RTUDFJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("rotateY(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("0")  + ";opacity:" + ("0")  + ";}.GEG3RTUDFJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("rotateY(" + "180deg" + ")"+ " " +"scale(" + "0.8" + ")")  + ";z-index:" + ("0")  + ";opacity:" + ("0")  + ";}.GEG3RTUDFJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("rotateY(" + "-180deg" + ")"+ " " +"scale(" + "0.8" + ")")  + ";z-index:" + ("1")  + ";opacity:" + ("1") ) + (";}.GEG3RTUDFJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("rotateY(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("1")  + ";opacity:" + ("1")  + ";}.GEG3RTUDNJ{-webkit-transition-property:" + ("-webkit-transform")  + ";-webkit-transition-duration:" + ("350ms"+ ","+ " " +"350ms")  + ";-webkit-transition-timing-function:" + ("ease-in-out")  + ";}.GEG3RTUDNJ.GEG3RTUDGJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "100%" + ")"+ " " +"scale(" + "0.3" + ")")  + ";z-index:" + ("1")  + ";}.GEG3RTUDNJ.GEG3RTUDGJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("1")  + ";}.GEG3RTUDNJ.GEG3RTUDHJ.GEG3RTUDMJ{-webkit-transform:") + (("translateX(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("0")  + ";}.GEG3RTUDNJ.GEG3RTUDHJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "-100%" + ")"+ " " +"scale(" + "0.3" + ")")  + ";z-index:" + ("0")  + ";}.GEG3RTUDNJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";z-index:" + ("0")  + ";}.GEG3RTUDNJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "100%" + ")"+ " " +"scale(" + "0.3" + ")")  + ";z-index:" + ("0")  + ";}.GEG3RTUDNJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("translateX(" + "-100%" + ")"+ " " +"scale(" + "0.3" + ")")  + ";}.GEG3RTUDNJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("translateX(" + "0" + ")"+ " " +"scale(" + "1" + ")")  + ";}.GEG3RTUDIJ{-webkit-transition-property:" + ("-webkit-transform"+ ","+ " " +"opacity") ) + (";-webkit-transition-duration:" + ("350ms"+ ","+ " " +"350ms")  + ";-webkit-transition-timing-function:" + ("ease-in-out")  + ";}.GEG3RTUDIJ.GEG3RTUDGJ.GEG3RTUDMJ{-webkit-transform:" + ("scale(" + "0.3" + ")")  + ";opacity:" + ("0")  + ";z-index:" + ("1")  + ";}.GEG3RTUDIJ.GEG3RTUDGJ.GEG3RTUDDJ{-webkit-transform:" + ("scale(" + "1" + ")")  + ";opacity:" + ("1")  + ";z-index:" + ("0")  + ";}.GEG3RTUDIJ.GEG3RTUDHJ.GEG3RTUDMJ{opacity:" + ("1")  + ";-webkit-transform:" + ("scale(" + "1" + ")")  + ";z-index:") + (("0")  + ";}.GEG3RTUDIJ.GEG3RTUDHJ.GEG3RTUDDJ{-webkit-transform:" + ("scale(" + "0.3" + ")")  + ";opacity:" + ("0")  + ";z-index:" + ("0")  + ";}.GEG3RTUDIJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("scale(" + "1" + ")")  + ";opacity:" + ("1")  + ";z-index:" + ("1")  + ";}.GEG3RTUDIJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("scale(" + "0.3" + ")")  + ";opacity:" + ("0")  + ";z-index:" + ("1")  + ";}.GEG3RTUDIJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{-webkit-transform:" + ("scale(" + "0.3" + ")") ) + (";opacity:" + ("0")  + ";z-index:" + ("0")  + ";}.GEG3RTUDIJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{-webkit-transform:" + ("scale(" + "1" + ")")  + ";opacity:" + ("1")  + ";z-index:" + ("0")  + ";}.GEG3RTUDEJ{-webkit-transition-property:" + ("opacity")  + ";-webkit-transition-duration:" + ("350ms")  + ";-webkit-transition-timing-function:" + ("ease-in-out")  + ";}.GEG3RTUDEJ.GEG3RTUDGJ.GEG3RTUDMJ{opacity:" + ("0")  + ";}.GEG3RTUDEJ.GEG3RTUDGJ.GEG3RTUDDJ,.GEG3RTUDEJ.GEG3RTUDHJ.GEG3RTUDMJ{opacity:" + ("1")  + ";}.GEG3RTUDEJ.GEG3RTUDHJ.GEG3RTUDDJ{opacity:") + (("0")  + ";}.GEG3RTUDEJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDMJ{opacity:" + ("1")  + ";}.GEG3RTUDEJ.GEG3RTUDHJ.GEG3RTUDJJ.GEG3RTUDDJ,.GEG3RTUDEJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDMJ{opacity:" + ("0")  + ";}.GEG3RTUDEJ.GEG3RTUDGJ.GEG3RTUDJJ.GEG3RTUDDJ{opacity:" + ("1")  + ";}"));
      }
      public java.lang.String display(){
        return "GEG3RTUDAJ";
      }
      public java.lang.String displayContainer(){
        return "GEG3RTUDBJ";
      }
      public java.lang.String dissolve(){
        return "GEG3RTUDCJ";
      }
      public java.lang.String end(){
        return "GEG3RTUDDJ";
      }
      public java.lang.String fade(){
        return "GEG3RTUDEJ";
      }
      public java.lang.String flip(){
        return "GEG3RTUDFJ";
      }
      public java.lang.String in(){
        return "GEG3RTUDGJ";
      }
      public java.lang.String out(){
        return "GEG3RTUDHJ";
      }
      public java.lang.String pop(){
        return "GEG3RTUDIJ";
      }
      public java.lang.String reverse(){
        return "GEG3RTUDJJ";
      }
      public java.lang.String slide(){
        return "GEG3RTUDKJ";
      }
      public java.lang.String slideup(){
        return "GEG3RTUDLJ";
      }
      public java.lang.String start(){
        return "GEG3RTUDMJ";
      }
      public java.lang.String swap(){
        return "GEG3RTUDNJ";
      }
    }
    ;
  }
  private static class transitionCssInitializer {
    static {
      _instance0.transitionCssInitializer();
    }
    static com.googlecode.mgwt.mvp.client.resources.TransistionCss get() {
      return transitionCss;
    }
  }
  public com.googlecode.mgwt.mvp.client.resources.TransistionCss transitionCss() {
    return transitionCssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.googlecode.mgwt.mvp.client.resources.AnimationCss animationCss;
  private static com.googlecode.mgwt.mvp.client.resources.TransistionCss transitionCss;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      animationCss(), 
      transitionCss(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("animationCss", animationCss());
        resourceMap.put("transitionCss", transitionCss());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'animationCss': return this.@com.googlecode.mgwt.mvp.client.resources.MVPBundle::animationCss()();
      case 'transitionCss': return this.@com.googlecode.mgwt.mvp.client.resources.MVPBundle::transitionCss()();
    }
    return null;
  }-*/;
}
