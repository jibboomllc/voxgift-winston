package com.googlecode.gwtphonegap.client.file.browser.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FileSystemDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO getFileSystemEntry(com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO::fileSystemEntry;
  }-*/;
  
  private static native void setFileSystemEntry(com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO instance, com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO::fileSystemEntry = value;
  }-*/;
  
  private static native java.lang.String getName(com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO::name;
  }-*/;
  
  private static native void setName(com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO instance, java.lang.String value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO::name = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO instance) throws SerializationException {
    setFileSystemEntry(instance, (com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO) streamReader.readObject());
    setName(instance, streamReader.readString());
    
  }
  
  public static native com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO instantiate(SerializationStreamReader streamReader) throws SerializationException /*-{
    return @com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO::new()();
  }-*/;
  
  public static void serialize(SerializationStreamWriter streamWriter, com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO instance) throws SerializationException {
    streamWriter.writeObject(getFileSystemEntry(instance));
    streamWriter.writeString(getName(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO_FieldSerializer.deserialize(reader, (com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO_FieldSerializer.serialize(writer, (com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemDTO)object);
  }
  
}
