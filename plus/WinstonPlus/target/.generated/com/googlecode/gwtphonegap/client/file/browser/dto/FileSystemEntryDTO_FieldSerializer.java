package com.googlecode.gwtphonegap.client.file.browser.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FileSystemEntryDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getFile(com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO::file;
  }-*/;
  
  private static native void setFile(com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO instance, boolean value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO::file = value;
  }-*/;
  
  private static native java.lang.String getFullPath(com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO::fullPath;
  }-*/;
  
  private static native void setFullPath(com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO instance, java.lang.String value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO::fullPath = value;
  }-*/;
  
  private static native java.lang.String getName(com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO::name;
  }-*/;
  
  private static native void setName(com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO instance, java.lang.String value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO::name = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO instance) throws SerializationException {
    setFile(instance, streamReader.readBoolean());
    setFullPath(instance, streamReader.readString());
    setName(instance, streamReader.readString());
    
  }
  
  public static com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO instance) throws SerializationException {
    streamWriter.writeBoolean(getFile(instance));
    streamWriter.writeString(getFullPath(instance));
    streamWriter.writeString(getName(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO_FieldSerializer.deserialize(reader, (com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO_FieldSerializer.serialize(writer, (com.googlecode.gwtphonegap.client.file.browser.dto.FileSystemEntryDTO)object);
  }
  
}
