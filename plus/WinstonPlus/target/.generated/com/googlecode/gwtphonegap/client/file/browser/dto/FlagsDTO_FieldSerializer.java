package com.googlecode.gwtphonegap.client.file.browser.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FlagsDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native boolean getCreate(com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO::create;
  }-*/;
  
  private static native void setCreate(com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO instance, boolean value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO::create = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO instance) throws SerializationException {
    setCreate(instance, streamReader.readBoolean());
    
  }
  
  public static native com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO instantiate(SerializationStreamReader streamReader) throws SerializationException /*-{
    return @com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO::new()();
  }-*/;
  
  public static void serialize(SerializationStreamWriter streamWriter, com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO instance) throws SerializationException {
    streamWriter.writeBoolean(getCreate(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO_FieldSerializer.deserialize(reader, (com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO_FieldSerializer.serialize(writer, (com.googlecode.gwtphonegap.client.file.browser.dto.FlagsDTO)object);
  }
  
}
