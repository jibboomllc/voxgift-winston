package com.googlecode.gwtphonegap.client.file.browser.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FileWriterDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getFullPath(com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO::fullPath;
  }-*/;
  
  private static native void setFullPath(com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO instance, java.lang.String value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO::fullPath = value;
  }-*/;
  
  private static native java.lang.String getName(com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO::name;
  }-*/;
  
  private static native void setName(com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO instance, java.lang.String value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO::name = value;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getPosition(com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO::position;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setPosition(com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO instance, long value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO::position = value;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getSize(com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO::size;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setSize(com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO instance, long value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO::size = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO instance) throws SerializationException {
    setFullPath(instance, streamReader.readString());
    setName(instance, streamReader.readString());
    setPosition(instance, streamReader.readLong());
    setSize(instance, streamReader.readLong());
    
  }
  
  public static native com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO instantiate(SerializationStreamReader streamReader) throws SerializationException /*-{
    return @com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO::new()();
  }-*/;
  
  public static void serialize(SerializationStreamWriter streamWriter, com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO instance) throws SerializationException {
    streamWriter.writeString(getFullPath(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeLong(getPosition(instance));
    streamWriter.writeLong(getSize(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO_FieldSerializer.deserialize(reader, (com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO_FieldSerializer.serialize(writer, (com.googlecode.gwtphonegap.client.file.browser.dto.FileWriterDTO)object);
  }
  
}
