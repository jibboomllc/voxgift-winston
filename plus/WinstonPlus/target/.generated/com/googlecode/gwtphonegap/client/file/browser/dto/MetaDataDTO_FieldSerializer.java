package com.googlecode.gwtphonegap.client.file.browser.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class MetaDataDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getLastModified(com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO::lastModified;
  }-*/;
  
  private static native void setLastModified(com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO instance, java.util.Date value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO::lastModified = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO instance) throws SerializationException {
    setLastModified(instance, (java.util.Date) streamReader.readObject());
    
  }
  
  public static native com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO instantiate(SerializationStreamReader streamReader) throws SerializationException /*-{
    return @com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO::new()();
  }-*/;
  
  public static void serialize(SerializationStreamWriter streamWriter, com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO instance) throws SerializationException {
    streamWriter.writeObject(getLastModified(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO_FieldSerializer.deserialize(reader, (com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO_FieldSerializer.serialize(writer, (com.googlecode.gwtphonegap.client.file.browser.dto.MetaDataDTO)object);
  }
  
}
