package com.googlecode.gwtphonegap.client.file.browser.dto;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class FileObjectDTO_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getFullPath(com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO::fullPath;
  }-*/;
  
  private static native void setFullPath(com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instance, java.lang.String value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO::fullPath = value;
  }-*/;
  
  private static native java.util.Date getLastModified(com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO::lastModified;
  }-*/;
  
  private static native void setLastModified(com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instance, java.util.Date value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO::lastModified = value;
  }-*/;
  
  private static native java.lang.String getName(com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO::name;
  }-*/;
  
  private static native void setName(com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instance, java.lang.String value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO::name = value;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native long getSize(com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO::size;
  }-*/;
  
  @com.google.gwt.core.client.UnsafeNativeLong
  private static native void setSize(com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instance, long value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO::size = value;
  }-*/;
  
  private static native java.lang.String getType(com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instance) /*-{
    return instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO::type;
  }-*/;
  
  private static native void setType(com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instance, java.lang.String value) 
  /*-{
    instance.@com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO::type = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instance) throws SerializationException {
    setFullPath(instance, streamReader.readString());
    setLastModified(instance, (java.util.Date) streamReader.readObject());
    setName(instance, streamReader.readString());
    setSize(instance, streamReader.readLong());
    setType(instance, streamReader.readString());
    
  }
  
  public static com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO instance) throws SerializationException {
    streamWriter.writeString(getFullPath(instance));
    streamWriter.writeObject(getLastModified(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeLong(getSize(instance));
    streamWriter.writeString(getType(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO_FieldSerializer.deserialize(reader, (com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO_FieldSerializer.serialize(writer, (com.googlecode.gwtphonegap.client.file.browser.dto.FileObjectDTO)object);
  }
  
}
