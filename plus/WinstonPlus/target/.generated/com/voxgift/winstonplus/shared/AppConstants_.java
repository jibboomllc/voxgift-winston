package com.voxgift.winstonplus.shared;

public class AppConstants_ implements com.voxgift.winstonplus.shared.AppConstants {
  
  public java.lang.String webAppUrl() {
    return "https://www.voxgift.com/c/WinstonPlus-1.0.2/WinstonPlus/";
  }
  
  public java.lang.String resourcesUrl() {
    return "https://www.voxgift.com/";
  }
  
  public java.lang.String customResourcesUrl() {
    return "https://www.voxgift.com/c/WinstonPlus-1.0.2/";
  }
  
  public java.lang.String confirmationUrl() {
    return "https://www.voxgift.com/confirmation?id=";
  }
  
  public java.lang.String audioUrl() {
    return "https://www.voxgift.com/audio?file=";
  }
  
  public java.lang.String imageUploadUrl() {
    return "https://www.voxgift.com/imageupload";
  }
  
  public java.lang.String title() {
    return "WinstonPlus";
  }
  
  public java.lang.String buildVersion() {
    return "1.0.2/13-07-10_10:13/21";
  }
}
