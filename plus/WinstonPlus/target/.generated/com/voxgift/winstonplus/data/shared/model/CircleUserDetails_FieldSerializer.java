package com.voxgift.winstonplus.data.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CircleUserDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getEmail(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::email;
  }-*/;
  
  private static native void setEmail(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::email = value;
  }-*/;
  
  private static native java.lang.Boolean getEmailConfirmed(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::emailConfirmed;
  }-*/;
  
  private static native void setEmailConfirmed(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance, java.lang.Boolean value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::emailConfirmed = value;
  }-*/;
  
  private static native java.lang.String getFirstName(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::firstName;
  }-*/;
  
  private static native void setFirstName(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::firstName = value;
  }-*/;
  
  private static native java.lang.String getLastName(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::lastName;
  }-*/;
  
  private static native void setLastName(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::lastName = value;
  }-*/;
  
  private static native int getRoleId(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::roleId;
  }-*/;
  
  private static native void setRoleId(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance, int value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::roleId = value;
  }-*/;
  
  private static native int getStatusId(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::statusId;
  }-*/;
  
  private static native void setStatusId(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance, int value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::statusId = value;
  }-*/;
  
  private static native java.lang.String getUserId(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::userId;
  }-*/;
  
  private static native void setUserId(com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserDetails::userId = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance) throws SerializationException {
    setEmail(instance, streamReader.readString());
    setEmailConfirmed(instance, (java.lang.Boolean) streamReader.readObject());
    setFirstName(instance, streamReader.readString());
    setLastName(instance, streamReader.readString());
    setRoleId(instance, streamReader.readInt());
    setStatusId(instance, streamReader.readInt());
    setUserId(instance, streamReader.readString());
    
  }
  
  public static com.voxgift.winstonplus.data.shared.model.CircleUserDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.voxgift.winstonplus.data.shared.model.CircleUserDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.voxgift.winstonplus.data.shared.model.CircleUserDetails instance) throws SerializationException {
    streamWriter.writeString(getEmail(instance));
    streamWriter.writeObject(getEmailConfirmed(instance));
    streamWriter.writeString(getFirstName(instance));
    streamWriter.writeString(getLastName(instance));
    streamWriter.writeInt(getRoleId(instance));
    streamWriter.writeInt(getStatusId(instance));
    streamWriter.writeString(getUserId(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.voxgift.winstonplus.data.shared.model.CircleUserDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.CircleUserDetails_FieldSerializer.deserialize(reader, (com.voxgift.winstonplus.data.shared.model.CircleUserDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.CircleUserDetails_FieldSerializer.serialize(writer, (com.voxgift.winstonplus.data.shared.model.CircleUserDetails)object);
  }
  
}
