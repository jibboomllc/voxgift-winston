package com.voxgift.winstonplus.data.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CircleDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.List getCircleUsers(com.voxgift.winstonplus.data.shared.model.CircleDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleDetails::circleUsers;
  }-*/;
  
  private static native void setCircleUsers(com.voxgift.winstonplus.data.shared.model.CircleDetails instance, java.util.List value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleDetails::circleUsers = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.voxgift.winstonplus.data.shared.model.CircleDetails instance) throws SerializationException {
    setCircleUsers(instance, (java.util.List) streamReader.readObject());
    
    com.voxgift.winstonplus.data.shared.model.Circle_FieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.voxgift.winstonplus.data.shared.model.CircleDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.voxgift.winstonplus.data.shared.model.CircleDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.voxgift.winstonplus.data.shared.model.CircleDetails instance) throws SerializationException {
    streamWriter.writeObject(getCircleUsers(instance));
    
    com.voxgift.winstonplus.data.shared.model.Circle_FieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.voxgift.winstonplus.data.shared.model.CircleDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.CircleDetails_FieldSerializer.deserialize(reader, (com.voxgift.winstonplus.data.shared.model.CircleDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.CircleDetails_FieldSerializer.serialize(writer, (com.voxgift.winstonplus.data.shared.model.CircleDetails)object);
  }
  
}
