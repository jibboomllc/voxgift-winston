package com.voxgift.winstonplus.data.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DocumentDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getId(com.voxgift.winstonplus.data.shared.model.DocumentDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.DocumentDetails::id;
  }-*/;
  
  private static native void setId(com.voxgift.winstonplus.data.shared.model.DocumentDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.DocumentDetails::id = value;
  }-*/;
  
  private static native java.lang.String getTitle(com.voxgift.winstonplus.data.shared.model.DocumentDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.DocumentDetails::title;
  }-*/;
  
  private static native void setTitle(com.voxgift.winstonplus.data.shared.model.DocumentDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.DocumentDetails::title = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.voxgift.winstonplus.data.shared.model.DocumentDetails instance) throws SerializationException {
    setId(instance, streamReader.readString());
    setTitle(instance, streamReader.readString());
    
  }
  
  public static com.voxgift.winstonplus.data.shared.model.DocumentDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.voxgift.winstonplus.data.shared.model.DocumentDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.voxgift.winstonplus.data.shared.model.DocumentDetails instance) throws SerializationException {
    streamWriter.writeString(getId(instance));
    streamWriter.writeString(getTitle(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.voxgift.winstonplus.data.shared.model.DocumentDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.DocumentDetails_FieldSerializer.deserialize(reader, (com.voxgift.winstonplus.data.shared.model.DocumentDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.DocumentDetails_FieldSerializer.serialize(writer, (com.voxgift.winstonplus.data.shared.model.DocumentDetails)object);
  }
  
}
