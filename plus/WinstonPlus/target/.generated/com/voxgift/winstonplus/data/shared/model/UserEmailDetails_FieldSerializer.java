package com.voxgift.winstonplus.data.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UserEmailDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.util.Date getConfirmed(com.voxgift.winstonplus.data.shared.model.UserEmailDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserEmailDetails::confirmed;
  }-*/;
  
  private static native void setConfirmed(com.voxgift.winstonplus.data.shared.model.UserEmailDetails instance, java.util.Date value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserEmailDetails::confirmed = value;
  }-*/;
  
  private static native java.lang.String getEmail(com.voxgift.winstonplus.data.shared.model.UserEmailDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserEmailDetails::email;
  }-*/;
  
  private static native void setEmail(com.voxgift.winstonplus.data.shared.model.UserEmailDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserEmailDetails::email = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.voxgift.winstonplus.data.shared.model.UserEmailDetails instance) throws SerializationException {
    setConfirmed(instance, (java.util.Date) streamReader.readObject());
    setEmail(instance, streamReader.readString());
    
  }
  
  public static com.voxgift.winstonplus.data.shared.model.UserEmailDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.voxgift.winstonplus.data.shared.model.UserEmailDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.voxgift.winstonplus.data.shared.model.UserEmailDetails instance) throws SerializationException {
    streamWriter.writeObject(getConfirmed(instance));
    streamWriter.writeString(getEmail(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.voxgift.winstonplus.data.shared.model.UserEmailDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.UserEmailDetails_FieldSerializer.deserialize(reader, (com.voxgift.winstonplus.data.shared.model.UserEmailDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.UserEmailDetails_FieldSerializer.serialize(writer, (com.voxgift.winstonplus.data.shared.model.UserEmailDetails)object);
  }
  
}
