package com.voxgift.winstonplus.data.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UserDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getFirstName(com.voxgift.winstonplus.data.shared.model.UserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::firstName;
  }-*/;
  
  private static native void setFirstName(com.voxgift.winstonplus.data.shared.model.UserDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::firstName = value;
  }-*/;
  
  private static native java.lang.String getGender(com.voxgift.winstonplus.data.shared.model.UserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::gender;
  }-*/;
  
  private static native void setGender(com.voxgift.winstonplus.data.shared.model.UserDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::gender = value;
  }-*/;
  
  private static native java.lang.String getId(com.voxgift.winstonplus.data.shared.model.UserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::id;
  }-*/;
  
  private static native void setId(com.voxgift.winstonplus.data.shared.model.UserDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::id = value;
  }-*/;
  
  private static native java.lang.String getLanguage(com.voxgift.winstonplus.data.shared.model.UserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::language;
  }-*/;
  
  private static native void setLanguage(com.voxgift.winstonplus.data.shared.model.UserDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::language = value;
  }-*/;
  
  private static native java.lang.String getLastName(com.voxgift.winstonplus.data.shared.model.UserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::lastName;
  }-*/;
  
  private static native void setLastName(com.voxgift.winstonplus.data.shared.model.UserDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::lastName = value;
  }-*/;
  
  private static native java.lang.String getLocale(com.voxgift.winstonplus.data.shared.model.UserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::locale;
  }-*/;
  
  private static native void setLocale(com.voxgift.winstonplus.data.shared.model.UserDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::locale = value;
  }-*/;
  
  private static native java.lang.String getPassword(com.voxgift.winstonplus.data.shared.model.UserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::password;
  }-*/;
  
  private static native void setPassword(com.voxgift.winstonplus.data.shared.model.UserDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::password = value;
  }-*/;
  
  private static native java.lang.String getPrimaryEmail(com.voxgift.winstonplus.data.shared.model.UserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::primaryEmail;
  }-*/;
  
  private static native void setPrimaryEmail(com.voxgift.winstonplus.data.shared.model.UserDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::primaryEmail = value;
  }-*/;
  
  private static native java.util.Map getUserCircles(com.voxgift.winstonplus.data.shared.model.UserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::userCircles;
  }-*/;
  
  private static native void setUserCircles(com.voxgift.winstonplus.data.shared.model.UserDetails instance, java.util.Map value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::userCircles = value;
  }-*/;
  
  private static native java.util.List getUserEmails(com.voxgift.winstonplus.data.shared.model.UserDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::userEmails;
  }-*/;
  
  private static native void setUserEmails(com.voxgift.winstonplus.data.shared.model.UserDetails instance, java.util.List value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserDetails::userEmails = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.voxgift.winstonplus.data.shared.model.UserDetails instance) throws SerializationException {
    setFirstName(instance, streamReader.readString());
    setGender(instance, streamReader.readString());
    setId(instance, streamReader.readString());
    setLanguage(instance, streamReader.readString());
    setLastName(instance, streamReader.readString());
    setLocale(instance, streamReader.readString());
    setPassword(instance, streamReader.readString());
    setPrimaryEmail(instance, streamReader.readString());
    setUserCircles(instance, (java.util.Map) streamReader.readObject());
    setUserEmails(instance, (java.util.List) streamReader.readObject());
    
  }
  
  public static com.voxgift.winstonplus.data.shared.model.UserDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.voxgift.winstonplus.data.shared.model.UserDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.voxgift.winstonplus.data.shared.model.UserDetails instance) throws SerializationException {
    streamWriter.writeString(getFirstName(instance));
    streamWriter.writeString(getGender(instance));
    streamWriter.writeString(getId(instance));
    streamWriter.writeString(getLanguage(instance));
    streamWriter.writeString(getLastName(instance));
    streamWriter.writeString(getLocale(instance));
    streamWriter.writeString(getPassword(instance));
    streamWriter.writeString(getPrimaryEmail(instance));
    streamWriter.writeObject(getUserCircles(instance));
    streamWriter.writeObject(getUserEmails(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.voxgift.winstonplus.data.shared.model.UserDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.UserDetails_FieldSerializer.deserialize(reader, (com.voxgift.winstonplus.data.shared.model.UserDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.UserDetails_FieldSerializer.serialize(writer, (com.voxgift.winstonplus.data.shared.model.UserDetails)object);
  }
  
}
