package com.voxgift.winstonplus.data.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DictionaryDetails_Array_Rank_1_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  public static void deserialize(SerializationStreamReader streamReader, com.voxgift.winstonplus.data.shared.model.DictionaryDetails[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.deserialize(streamReader, instance);
  }
  
  public static com.voxgift.winstonplus.data.shared.model.DictionaryDetails[] instantiate(SerializationStreamReader streamReader) throws SerializationException {
    int size = streamReader.readInt();
    return new com.voxgift.winstonplus.data.shared.model.DictionaryDetails[size];
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.voxgift.winstonplus.data.shared.model.DictionaryDetails[] instance) throws SerializationException {
    com.google.gwt.user.client.rpc.core.java.lang.Object_Array_CustomFieldSerializer.serialize(streamWriter, instance);
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.voxgift.winstonplus.data.shared.model.DictionaryDetails_Array_Rank_1_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.DictionaryDetails_Array_Rank_1_FieldSerializer.deserialize(reader, (com.voxgift.winstonplus.data.shared.model.DictionaryDetails[])object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.DictionaryDetails_Array_Rank_1_FieldSerializer.serialize(writer, (com.voxgift.winstonplus.data.shared.model.DictionaryDetails[])object);
  }
  
}
