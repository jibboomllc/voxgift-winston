package com.voxgift.winstonplus.data.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class CircleUserStatusDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getEmail(com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails::email;
  }-*/;
  
  private static native void setEmail(com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails::email = value;
  }-*/;
  
  private static native java.lang.String getFirstName(com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails::firstName;
  }-*/;
  
  private static native void setFirstName(com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails::firstName = value;
  }-*/;
  
  private static native java.lang.String getLastName(com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails::lastName;
  }-*/;
  
  private static native void setLastName(com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails::lastName = value;
  }-*/;
  
  private static native int getRoleId(com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails::roleId;
  }-*/;
  
  private static native void setRoleId(com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance, int value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails::roleId = value;
  }-*/;
  
  private static native int getStatusId(com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails::statusId;
  }-*/;
  
  private static native void setStatusId(com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance, int value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails::statusId = value;
  }-*/;
  
  private static native java.lang.String getUserId(com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails::userId;
  }-*/;
  
  private static native void setUserId(com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails::userId = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance) throws SerializationException {
    setEmail(instance, streamReader.readString());
    setFirstName(instance, streamReader.readString());
    setLastName(instance, streamReader.readString());
    setRoleId(instance, streamReader.readInt());
    setStatusId(instance, streamReader.readInt());
    setUserId(instance, streamReader.readString());
    
  }
  
  public static com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails instance) throws SerializationException {
    streamWriter.writeString(getEmail(instance));
    streamWriter.writeString(getFirstName(instance));
    streamWriter.writeString(getLastName(instance));
    streamWriter.writeInt(getRoleId(instance));
    streamWriter.writeInt(getStatusId(instance));
    streamWriter.writeString(getUserId(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails_FieldSerializer.deserialize(reader, (com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails_FieldSerializer.serialize(writer, (com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails)object);
  }
  
}
