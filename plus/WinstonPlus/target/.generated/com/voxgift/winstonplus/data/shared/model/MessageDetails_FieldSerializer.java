package com.voxgift.winstonplus.data.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class MessageDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCircleId(com.voxgift.winstonplus.data.shared.model.MessageDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::circleId;
  }-*/;
  
  private static native void setCircleId(com.voxgift.winstonplus.data.shared.model.MessageDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::circleId = value;
  }-*/;
  
  private static native java.lang.String getElapsedTime(com.voxgift.winstonplus.data.shared.model.MessageDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::elapsedTime;
  }-*/;
  
  private static native void setElapsedTime(com.voxgift.winstonplus.data.shared.model.MessageDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::elapsedTime = value;
  }-*/;
  
  private static native java.lang.String getId(com.voxgift.winstonplus.data.shared.model.MessageDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::id;
  }-*/;
  
  private static native void setId(com.voxgift.winstonplus.data.shared.model.MessageDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::id = value;
  }-*/;
  
  private static native java.lang.String getMessage(com.voxgift.winstonplus.data.shared.model.MessageDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::message;
  }-*/;
  
  private static native void setMessage(com.voxgift.winstonplus.data.shared.model.MessageDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::message = value;
  }-*/;
  
  private static native java.lang.String getUserId(com.voxgift.winstonplus.data.shared.model.MessageDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::userId;
  }-*/;
  
  private static native void setUserId(com.voxgift.winstonplus.data.shared.model.MessageDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::userId = value;
  }-*/;
  
  private static native java.lang.String getUserName(com.voxgift.winstonplus.data.shared.model.MessageDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::userName;
  }-*/;
  
  private static native void setUserName(com.voxgift.winstonplus.data.shared.model.MessageDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::userName = value;
  }-*/;
  
  private static native java.lang.Integer getUserRoleId(com.voxgift.winstonplus.data.shared.model.MessageDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::userRoleId;
  }-*/;
  
  private static native void setUserRoleId(com.voxgift.winstonplus.data.shared.model.MessageDetails instance, java.lang.Integer value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.MessageDetails::userRoleId = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.voxgift.winstonplus.data.shared.model.MessageDetails instance) throws SerializationException {
    setCircleId(instance, streamReader.readString());
    setElapsedTime(instance, streamReader.readString());
    setId(instance, streamReader.readString());
    setMessage(instance, streamReader.readString());
    setUserId(instance, streamReader.readString());
    setUserName(instance, streamReader.readString());
    setUserRoleId(instance, (java.lang.Integer) streamReader.readObject());
    
  }
  
  public static com.voxgift.winstonplus.data.shared.model.MessageDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.voxgift.winstonplus.data.shared.model.MessageDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.voxgift.winstonplus.data.shared.model.MessageDetails instance) throws SerializationException {
    streamWriter.writeString(getCircleId(instance));
    streamWriter.writeString(getElapsedTime(instance));
    streamWriter.writeString(getId(instance));
    streamWriter.writeString(getMessage(instance));
    streamWriter.writeString(getUserId(instance));
    streamWriter.writeString(getUserName(instance));
    streamWriter.writeObject(getUserRoleId(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.voxgift.winstonplus.data.shared.model.MessageDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.MessageDetails_FieldSerializer.deserialize(reader, (com.voxgift.winstonplus.data.shared.model.MessageDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.MessageDetails_FieldSerializer.serialize(writer, (com.voxgift.winstonplus.data.shared.model.MessageDetails)object);
  }
  
}
