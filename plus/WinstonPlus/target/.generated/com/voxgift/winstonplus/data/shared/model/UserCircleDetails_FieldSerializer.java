package com.voxgift.winstonplus.data.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class UserCircleDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getCenterId(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::centerId;
  }-*/;
  
  private static native void setCenterId(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::centerId = value;
  }-*/;
  
  private static native java.lang.String getCircleId(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::circleId;
  }-*/;
  
  private static native void setCircleId(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::circleId = value;
  }-*/;
  
  private static native java.lang.String getCircleName(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::circleName;
  }-*/;
  
  private static native void setCircleName(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::circleName = value;
  }-*/;
  
  private static native int getCircleStatusId(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::circleStatusId;
  }-*/;
  
  private static native void setCircleStatusId(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance, int value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::circleStatusId = value;
  }-*/;
  
  private static native int getRoleId(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::roleId;
  }-*/;
  
  private static native void setRoleId(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance, int value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::roleId = value;
  }-*/;
  
  private static native java.lang.String getSponsorId(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::sponsorId;
  }-*/;
  
  private static native void setSponsorId(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::sponsorId = value;
  }-*/;
  
  private static native int getStatusId(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::statusId;
  }-*/;
  
  private static native void setStatusId(com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance, int value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.UserCircleDetails::statusId = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance) throws SerializationException {
    setCenterId(instance, streamReader.readString());
    setCircleId(instance, streamReader.readString());
    setCircleName(instance, streamReader.readString());
    setCircleStatusId(instance, streamReader.readInt());
    setRoleId(instance, streamReader.readInt());
    setSponsorId(instance, streamReader.readString());
    setStatusId(instance, streamReader.readInt());
    
  }
  
  public static com.voxgift.winstonplus.data.shared.model.UserCircleDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.voxgift.winstonplus.data.shared.model.UserCircleDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.voxgift.winstonplus.data.shared.model.UserCircleDetails instance) throws SerializationException {
    streamWriter.writeString(getCenterId(instance));
    streamWriter.writeString(getCircleId(instance));
    streamWriter.writeString(getCircleName(instance));
    streamWriter.writeInt(getCircleStatusId(instance));
    streamWriter.writeInt(getRoleId(instance));
    streamWriter.writeString(getSponsorId(instance));
    streamWriter.writeInt(getStatusId(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.voxgift.winstonplus.data.shared.model.UserCircleDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.UserCircleDetails_FieldSerializer.deserialize(reader, (com.voxgift.winstonplus.data.shared.model.UserCircleDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.UserCircleDetails_FieldSerializer.serialize(writer, (com.voxgift.winstonplus.data.shared.model.UserCircleDetails)object);
  }
  
}
