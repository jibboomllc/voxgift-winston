package com.voxgift.winstonplus.data.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class Circle_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getId(com.voxgift.winstonplus.data.shared.model.Circle instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.Circle::id;
  }-*/;
  
  private static native void setId(com.voxgift.winstonplus.data.shared.model.Circle instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.Circle::id = value;
  }-*/;
  
  private static native java.lang.String getName(com.voxgift.winstonplus.data.shared.model.Circle instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.Circle::name;
  }-*/;
  
  private static native void setName(com.voxgift.winstonplus.data.shared.model.Circle instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.Circle::name = value;
  }-*/;
  
  private static native java.lang.String getOwnerId(com.voxgift.winstonplus.data.shared.model.Circle instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.Circle::ownerId;
  }-*/;
  
  private static native void setOwnerId(com.voxgift.winstonplus.data.shared.model.Circle instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.Circle::ownerId = value;
  }-*/;
  
  private static native java.lang.String getSponsorId(com.voxgift.winstonplus.data.shared.model.Circle instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.Circle::sponsorId;
  }-*/;
  
  private static native void setSponsorId(com.voxgift.winstonplus.data.shared.model.Circle instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.Circle::sponsorId = value;
  }-*/;
  
  private static native int getStatusId(com.voxgift.winstonplus.data.shared.model.Circle instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.Circle::statusId;
  }-*/;
  
  private static native void setStatusId(com.voxgift.winstonplus.data.shared.model.Circle instance, int value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.Circle::statusId = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.voxgift.winstonplus.data.shared.model.Circle instance) throws SerializationException {
    setId(instance, streamReader.readString());
    setName(instance, streamReader.readString());
    setOwnerId(instance, streamReader.readString());
    setSponsorId(instance, streamReader.readString());
    setStatusId(instance, streamReader.readInt());
    
  }
  
  public static com.voxgift.winstonplus.data.shared.model.Circle instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.voxgift.winstonplus.data.shared.model.Circle();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.voxgift.winstonplus.data.shared.model.Circle instance) throws SerializationException {
    streamWriter.writeString(getId(instance));
    streamWriter.writeString(getName(instance));
    streamWriter.writeString(getOwnerId(instance));
    streamWriter.writeString(getSponsorId(instance));
    streamWriter.writeInt(getStatusId(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.voxgift.winstonplus.data.shared.model.Circle_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.Circle_FieldSerializer.deserialize(reader, (com.voxgift.winstonplus.data.shared.model.Circle)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.Circle_FieldSerializer.serialize(writer, (com.voxgift.winstonplus.data.shared.model.Circle)object);
  }
  
}
