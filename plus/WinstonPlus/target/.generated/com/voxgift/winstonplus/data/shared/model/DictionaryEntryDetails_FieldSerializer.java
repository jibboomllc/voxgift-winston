package com.voxgift.winstonplus.data.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DictionaryEntryDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getEntryId(com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails::entryId;
  }-*/;
  
  private static native void setEntryId(com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails::entryId = value;
  }-*/;
  
  private static native java.lang.String getEntryText(com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails::entryText;
  }-*/;
  
  private static native void setEntryText(com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails::entryText = value;
  }-*/;
  
  private static native int getSortOrder(com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails::sortOrder;
  }-*/;
  
  private static native void setSortOrder(com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails instance, int value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails::sortOrder = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails instance) throws SerializationException {
    setEntryId(instance, streamReader.readString());
    setEntryText(instance, streamReader.readString());
    setSortOrder(instance, streamReader.readInt());
    
  }
  
  public static com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails instance) throws SerializationException {
    streamWriter.writeString(getEntryId(instance));
    streamWriter.writeString(getEntryText(instance));
    streamWriter.writeInt(getSortOrder(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails_FieldSerializer.deserialize(reader, (com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails_FieldSerializer.serialize(writer, (com.voxgift.winstonplus.data.shared.model.DictionaryEntryDetails)object);
  }
  
}
