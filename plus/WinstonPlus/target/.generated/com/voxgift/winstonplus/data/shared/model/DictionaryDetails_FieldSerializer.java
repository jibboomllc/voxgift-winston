package com.voxgift.winstonplus.data.shared.model;

import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.SerializationStreamReader;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.user.client.rpc.impl.ReflectionHelper;

@SuppressWarnings("deprecation")
public class DictionaryDetails_FieldSerializer implements com.google.gwt.user.client.rpc.impl.TypeHandler {
  private static native java.lang.String getContextId(com.voxgift.winstonplus.data.shared.model.DictionaryDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.DictionaryDetails::contextId;
  }-*/;
  
  private static native void setContextId(com.voxgift.winstonplus.data.shared.model.DictionaryDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.DictionaryDetails::contextId = value;
  }-*/;
  
  private static native java.util.List getEntries(com.voxgift.winstonplus.data.shared.model.DictionaryDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.DictionaryDetails::entries;
  }-*/;
  
  private static native void setEntries(com.voxgift.winstonplus.data.shared.model.DictionaryDetails instance, java.util.List value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.DictionaryDetails::entries = value;
  }-*/;
  
  private static native java.lang.String getLanguage(com.voxgift.winstonplus.data.shared.model.DictionaryDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.DictionaryDetails::language;
  }-*/;
  
  private static native void setLanguage(com.voxgift.winstonplus.data.shared.model.DictionaryDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.DictionaryDetails::language = value;
  }-*/;
  
  private static native java.lang.String getLocale(com.voxgift.winstonplus.data.shared.model.DictionaryDetails instance) /*-{
    return instance.@com.voxgift.winstonplus.data.shared.model.DictionaryDetails::locale;
  }-*/;
  
  private static native void setLocale(com.voxgift.winstonplus.data.shared.model.DictionaryDetails instance, java.lang.String value) 
  /*-{
    instance.@com.voxgift.winstonplus.data.shared.model.DictionaryDetails::locale = value;
  }-*/;
  
  public static void deserialize(SerializationStreamReader streamReader, com.voxgift.winstonplus.data.shared.model.DictionaryDetails instance) throws SerializationException {
    setContextId(instance, streamReader.readString());
    setEntries(instance, (java.util.List) streamReader.readObject());
    setLanguage(instance, streamReader.readString());
    setLocale(instance, streamReader.readString());
    
  }
  
  public static com.voxgift.winstonplus.data.shared.model.DictionaryDetails instantiate(SerializationStreamReader streamReader) throws SerializationException {
    return new com.voxgift.winstonplus.data.shared.model.DictionaryDetails();
  }
  
  public static void serialize(SerializationStreamWriter streamWriter, com.voxgift.winstonplus.data.shared.model.DictionaryDetails instance) throws SerializationException {
    streamWriter.writeString(getContextId(instance));
    streamWriter.writeObject(getEntries(instance));
    streamWriter.writeString(getLanguage(instance));
    streamWriter.writeString(getLocale(instance));
    
  }
  
  public Object create(SerializationStreamReader reader) throws SerializationException {
    return com.voxgift.winstonplus.data.shared.model.DictionaryDetails_FieldSerializer.instantiate(reader);
  }
  
  public void deserial(SerializationStreamReader reader, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.DictionaryDetails_FieldSerializer.deserialize(reader, (com.voxgift.winstonplus.data.shared.model.DictionaryDetails)object);
  }
  
  public void serial(SerializationStreamWriter writer, Object object) throws SerializationException {
    com.voxgift.winstonplus.data.shared.model.DictionaryDetails_FieldSerializer.serialize(writer, (com.voxgift.winstonplus.data.shared.model.DictionaryDetails)object);
  }
  
}
