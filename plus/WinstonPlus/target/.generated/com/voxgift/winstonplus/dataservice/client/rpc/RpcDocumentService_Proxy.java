package com.voxgift.winstonplus.dataservice.client.rpc;

import com.googlecode.gwtphonegap.client.rpc.impl.PhoneGapRemoteServiceProxy;
import com.google.gwt.user.client.rpc.impl.ClientSerializationStreamWriter;
import com.google.gwt.user.client.rpc.SerializationStreamWriter;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.impl.RequestCallbackAdapter.ResponseReader;
import com.google.gwt.user.client.rpc.SerializationException;
import com.google.gwt.user.client.rpc.RpcToken;
import com.google.gwt.user.client.rpc.RpcTokenException;
import com.google.gwt.core.client.impl.Impl;
import com.google.gwt.user.client.rpc.impl.RpcStatsContext;

public class RpcDocumentService_Proxy extends PhoneGapRemoteServiceProxy implements com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentServiceAsync {
  private static final String REMOTE_SERVICE_INTERFACE_NAME = "com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentService";
  private static final String SERIALIZATION_POLICY ="7E356BF728C2988B6FF58AEEBF0BE9EA";
  private static final com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentService_TypeSerializer SERIALIZER = new com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentService_TypeSerializer();
  
  public RpcDocumentService_Proxy() {
    super(GWT.getModuleBaseURL(),
      "rpc/document", 
      SERIALIZATION_POLICY, 
      SERIALIZER);
  }
  
  public void getDocument(java.lang.String documentId, java.lang.String language, java.lang.String locale, com.google.gwt.user.client.rpc.AsyncCallback callback) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("RpcDocumentService_Proxy", "getDocument");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 3);
      streamWriter.writeString("java.lang.String/2004016611");
      streamWriter.writeString("java.lang.String/2004016611");
      streamWriter.writeString("java.lang.String/2004016611");
      streamWriter.writeString(documentId);
      streamWriter.writeString(language);
      streamWriter.writeString(locale);
      helper.finish(callback, ResponseReader.STRING);
    } catch (SerializationException ex) {
      callback.onFailure(ex);
    }
  }
  
  public void getDocuments(java.lang.String language, java.lang.String locale, int typeId, com.google.gwt.user.client.rpc.AsyncCallback callback) {
    com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper helper = new com.google.gwt.user.client.rpc.impl.RemoteServiceProxy.ServiceHelper("RpcDocumentService_Proxy", "getDocuments");
    try {
      SerializationStreamWriter streamWriter = helper.start(REMOTE_SERVICE_INTERFACE_NAME, 3);
      streamWriter.writeString("java.lang.String/2004016611");
      streamWriter.writeString("java.lang.String/2004016611");
      streamWriter.writeString("I");
      streamWriter.writeString(language);
      streamWriter.writeString(locale);
      streamWriter.writeInt(typeId);
      helper.finish(callback, ResponseReader.OBJECT);
    } catch (SerializationException ex) {
      callback.onFailure(ex);
    }
  }
  @Override
  public SerializationStreamWriter createStreamWriter() {
    ClientSerializationStreamWriter toReturn =
      (ClientSerializationStreamWriter) super.createStreamWriter();
    if (getRpcToken() != null) {
      toReturn.addFlags(ClientSerializationStreamWriter.FLAG_RPC_TOKEN_INCLUDED);
    }
    return toReturn;
  }
  @Override
  protected void checkRpcTokenType(RpcToken token) {
    if (!(token instanceof com.google.gwt.user.client.rpc.XsrfToken)) {
      throw new RpcTokenException("Invalid RpcToken type: expected 'com.google.gwt.user.client.rpc.XsrfToken' but got '" + token.getClass() + "'");
    }
  }
}
