package com.voxgift.winstonplus.client.activities.settings.user.favorites;

public class FavoriteCell_TemplateLastImpl implements com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoriteCell.TemplateLast {
  
  public com.google.gwt.safehtml.shared.SafeHtml content(java.lang.String arg0) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<div class='sorting_cell'><div class='text'>");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
    sb.append("</div><div class='controls'><div class='up' id='up'>[ ^ ]</div><div class='blank'></div><div class='blank'></div><div class='delete' id='delete'>[ X ]</div></div></div>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}
