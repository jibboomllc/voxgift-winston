package com.voxgift.winstonplus.client.activities.settings.user.favorites;

public class FavoriteCell_TemplateAddImpl implements com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoriteCell.TemplateAdd {
  
  public com.google.gwt.safehtml.shared.SafeHtml content(java.lang.String arg0) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<div>");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
    sb.append("</div><div><button id='add'>[ + ]</button>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}
