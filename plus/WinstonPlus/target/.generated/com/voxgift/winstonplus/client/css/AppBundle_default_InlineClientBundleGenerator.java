package com.voxgift.winstonplus.client.css;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class AppBundle_default_InlineClientBundleGenerator implements com.voxgift.winstonplus.client.css.AppBundle {
  private static AppBundle_default_InlineClientBundleGenerator _instance0 = new AppBundle_default_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new com.google.gwt.resources.client.TextResource() {
      // file:/Users/Tom/Documents/winston/trunk/plus/WinstonPlus/target/classes/com/voxgift/winstonplus/client/css/app.css
      public String getText() {
        return "#frame {\n	position: relative;\n}\n\n#single {\n	position: absolute;\n	left: 0px;\n	top: 0px;\n	right: 0px;\n	bottom: 0px;\n	overflow: hidden;\n}\n\n#main {\n	position: absolute;\n	left: 35%;\n	top: 0px;\n	width: 65%;\n	bottom: 0px;\n	overflow: hidden;\n	border-left: 1px solid black;\n}\n\n#nav {\n	position: absolute;\n	left: 0px;\n	top: 0px;\n	width: 35%;\n	bottom: 0px;\n	overflow: hidden;\n	border-right: 1px solid black;\n}\n";
      }
      public String getName() {
        return "css";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static com.google.gwt.resources.client.TextResource get() {
      return css;
    }
  }
  public com.google.gwt.resources.client.TextResource css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.google.gwt.resources.client.TextResource css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@com.voxgift.winstonplus.client.css.AppBundle::css()();
    }
    return null;
  }-*/;
}
