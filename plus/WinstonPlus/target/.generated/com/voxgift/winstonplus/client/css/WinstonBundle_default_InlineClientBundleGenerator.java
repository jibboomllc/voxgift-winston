package com.voxgift.winstonplus.client.css;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class WinstonBundle_default_InlineClientBundleGenerator implements com.voxgift.winstonplus.client.css.WinstonBundle {
  private static WinstonBundle_default_InlineClientBundleGenerator _instance0 = new WinstonBundle_default_InlineClientBundleGenerator();
  private void cssInitializer() {
    css = new com.google.gwt.resources.client.TextResource() {
      // file:/Users/Tom/Documents/winston/trunk/plus/WinstonPlus/target/classes/com/voxgift/winstonplus/client/css/winston.css
      public String getText() {
        return "/*  For more info see: http://code.google.com/p/mgwt/wiki/Styling */\n.message_cell {\n	display: table;\n	width: 100%;\n}\n\n.message_cell .avatar {\n	display: table-cell;\n	width: 64px;\n}\n\n.message_cell .text {\n	display: table-cell;\n	vertical-align: top;\n	padding-left: 10px;\n}\n\n.message_cell .text .title {\n	display: table;\n	width: 100%;\n}\n\n.message_cell .text .title .user {\n	display: table-cell;\n	font-weight: bold;\n}\n\n.message_cell .text .title .time {\n	display: table-cell;\n	font-weight: lighter;\n	text-align: right;\n}\n\n.message_cell .text .message {\n	display: table-cell;\n}\n\n/************************************/\n.dictionary_entry {\n	font-size: 1.2em;\n	padding-left: 10px;\n}\n\n/************************************/\n.main_panel {\n	display: -webkit-box;\n	display: box;\n	-webkit-box-orient: vertical;\n	box-orient: vertical;\n	width: 100%;\n	height: 100%;\n}\n\n.main_panel .create_panel {\n	-webkit-box-flex: 1.0;\n	box-flex: 1.0;\n	-webkit-box-orient: horizontal;\n	box-orient: horizontal;\n	position: relative;\n}\n\n.main_panel .footer_panel {\n	-webkit-box-flex: 0.0;\n	box-flex: 0.0;\n	height: auto;\n}\n\n/************************************/\n.frame_panel {\n	display: -webkit-box;\n	display: box;\n	-webkit-box-orient: vertical;\n	box-orient: vertical;\n}\n\n.frame_panel .body_panel {\n	-webkit-box-flex: 1.0;\n	box-flex: 1.0;\n	-webkit-box-orient: horizontal;\n	box-orient: horizontal;\n	position: relative; /* needed for  2 frame view (see app.css) */\n}\n\n.box_flex_1 {\n	-webkit-box-flex: 1.0;\n	box-flex: 1.0;\n}\n\n/************************************/\n.working {\n	background-color: rgba(255, 255, 255, 0.75);\n}\n\n.login {\n	background-color: #ffffff;\n}\n/*\n	background: #ffffff url('images/background.jpg') no-repeat fixed center;\n\n\n	border-top: 1px solid #000000;\n	border-bottom: 1px solid #000000;\n	background-color: rgba(255, 255, 255, 0.75);\n*/\n.login .container {\n	margin-top: 15px;\n	text-align: center;\n}\n\n.login .container .inner_container {\n	margin-left: auto;\n	margin-right: auto;\n	width: 90%;\n}\n\n.login .container .inner_container .prompt {\n	color: #666;\n	font-size: 0.8em;\n	margin-top: 10px;\n	text-align: left;\n	text-transform: uppercase;\n}\n\n.login .container .controls {\n	border-top: solid 1px #d3d3d3;\n	border-bottom: solid 1px #d3d3d3;\n}\n\n.login .status {\n	color: #000000;\n	font-size: 0.8em;\n	text-align: center;\n}\n\n.login .error {\n	color: #ff0000;\n	font-size: 0.8em;\n	text-align: center;\n}\n\n/************************************/\n.sub-menu-indicator {\n	width: 0px;\n	height: 0px;\n	border-style: solid;\n	border-width: 0 0 10px 10px;\n	border-color: transparent transparent rgba(128, 128, 128, 0.5)\n		transparent;\n	position: relative;\n	left: -10px;\n	top: 10px;\n}\n\n.sub-menu-indicator1:after {\n	content: \"\";\n	width: 0px;\n	height: 0px;\n	border-style: solid;\n	border-width: 0 0 10px 10px;\n	border-color: transparent transparent #d3d3d3 transparent;\n	position: relative;\n	left: -10px;\n	top: -10px;\n}\n\n/************************************/\n.help {\n	background: #ffffff url('images/background.jpg') no-repeat fixed center;\n}\n\n.help p,.help li,.help h2,.help h3 {\n	color: #000000;\n}\n\n.help p {\n	padding: 10px;\n}\n\n.help ul {\n	list-style-type: square;\n	padding-left: 20px;\n	margin-left: 20px;\n}\n\n/************************************/\n.help_controls {\n	display: table;\n}\n\n.help_controls .previous,.help_controls .previous_disabled,.help_controls .next,.help_controls .next_disabled,.help_controls .close,.help_controls .blank\n	{\n	display: table-cell;\n	width: 40px;\n	height: 40px;\n	border: none;\n	padding: 0;\n	margin: 0;\n}\n\n.help_controls .previous {\n	background: transparent url('images/arrow_left_24_white.png') no-repeat\n		center center;\n}\n\n.help_controls .previous_disabled {\n	background: transparent url('images/arrow_left_24.png') no-repeat center\n		center;\n}\n\n.help_controls .next {\n	background: transparent url('images/arrow_right_24_white.png') no-repeat\n		center center;\n}\n\n.help_controls .next_disabled {\n	background: transparent url('images/arrow_right_24.png') no-repeat\n		center center;\n}\n\n.help_controls .close {\n	background: transparent url('images/delete_24_white.png') no-repeat\n		center center;\n}\n\n.help_controls .blank {\n	background: transparent;\n}\n\n/************************************/\n\n.legal p {\n	padding: 10px;\n}\n\n/************************************/\n.search_panel {\n	display: table;\n}\n\n.search_panel .text {\n	display: table-cell;\n	vertical-align: top;\n	padding-right: 10px;\n}\n\n.search_panel .button {\n	display: table-cell;\n	width: 24px;\n	height: 24px;\n	border: none;\n	color: transparent;\n	font-size: 0;\n	padding: 0;\n	margin: 0;\n	background: transparent url('images/magnifier_24.png') no-repeat center\n		center;\n}\n\n/************************************/\n.sorting_cell {\n	display: table;\n	width: 100%;\n}\n\n.sorting_cell .text {\n	display: table-cell;\n	vertical-align: top;\n	padding-right: 10px;\n}\n\n.sorting_cell .controls {\n	float: right;\n	display: table;\n	width: 120px;\n	height: 30px;\n	border: none;\n	color: transparent;\n	font-size: 0;\n}\n\n.sorting_cell .controls .up {\n	display: table-cell;\n	width: 30px;\n	background: transparent url('images/arrow_up_24.png') no-repeat center\n		center;\n}\n\n.sorting_cell .controls .down {\n	display: table-cell;\n	width: 30px;\n	background: transparent url('images/arrow_down_24.png') no-repeat center\n		center;\n}\n\n.sorting_cell .controls .delete {\n	display: table-cell;\n	width: 30px;\n	background: transparent url('images/delete_24.png') no-repeat center\n		center;\n}\n\n.sorting_cell .controls .blank {\n	display: table-cell;\n	width: 30px;\n	background: transparent;\n}\n\n/************************************/";
      }
      public String getName() {
        return "css";
      }
    }
    ;
  }
  private static class cssInitializer {
    static {
      _instance0.cssInitializer();
    }
    static com.google.gwt.resources.client.TextResource get() {
      return css;
    }
  }
  public com.google.gwt.resources.client.TextResource css() {
    return cssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.google.gwt.resources.client.TextResource css;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      css(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("css", css());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'css': return this.@com.voxgift.winstonplus.client.css.WinstonBundle::css()();
    }
    return null;
  }-*/;
}
