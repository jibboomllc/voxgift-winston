package com.voxgift.winstonplus.client;

import com.google.gwt.place.impl.AbstractPlaceHistoryMapper;
import com.voxgift.winstonplus.client.AppPlaceHistoryMapper;
import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;
import com.google.gwt.place.impl.AbstractPlaceHistoryMapper.PrefixAndToken;
import com.google.gwt.core.client.GWT;

public class AppPlaceHistoryMapperImpl extends AbstractPlaceHistoryMapper<Void> implements AppPlaceHistoryMapper {
  
  protected PrefixAndToken getPrefixAndToken(Place newPlace) {
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.compose.ComposePlace) {
      com.voxgift.winstonplus.client.activities.compose.ComposePlace place = (com.voxgift.winstonplus.client.activities.compose.ComposePlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.compose.ComposePlace> t = GWT.create(com.voxgift.winstonplus.client.activities.compose.ComposePlace.ComposePlaceTokenizer.class);
      return new PrefixAndToken("ComposePlace", t.getToken((com.voxgift.winstonplus.client.activities.compose.ComposePlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.messagelist.MessageListPlace) {
      com.voxgift.winstonplus.client.activities.messagelist.MessageListPlace place = (com.voxgift.winstonplus.client.activities.messagelist.MessageListPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.messagelist.MessageListPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.messagelist.MessageListPlace.MessageListPlaceTokenizer.class);
      return new PrefixAndToken("MessageListPlace", t.getToken((com.voxgift.winstonplus.client.activities.messagelist.MessageListPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.settings.SettingsPlace) {
      com.voxgift.winstonplus.client.activities.settings.SettingsPlace place = (com.voxgift.winstonplus.client.activities.settings.SettingsPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.settings.SettingsPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.settings.SettingsPlace.SettingsPlaceTokenizer.class);
      return new PrefixAndToken("SettingsPlace", t.getToken((com.voxgift.winstonplus.client.activities.settings.SettingsPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsPlace) {
      com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsPlace place = (com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsPlace.CircleSettingsPlaceTokenizer.class);
      return new PrefixAndToken("CircleSettingsPlace", t.getToken((com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsPlace) {
      com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsPlace place = (com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsPlace.BasicCircleSettingsPlaceTokenizer.class);
      return new PrefixAndToken("BasicCircleSettingsPlace", t.getToken((com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsPlace) {
      com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsPlace place = (com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsPlace.UserCircleSettingsPlaceTokenizer.class);
      return new PrefixAndToken("UserCircleSettingsPlace", t.getToken((com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsPlace) {
      com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsPlace place = (com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsPlace.LegalDocumentsPlaceTokenizer.class);
      return new PrefixAndToken("LegalDocumentsPlace", t.getToken((com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentPlace) {
      com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentPlace place = (com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentPlace.LegalDocumentPlaceTokenizer.class);
      return new PrefixAndToken("LegalDocumentPlace", t.getToken((com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.settings.user.UserSettingsPlace) {
      com.voxgift.winstonplus.client.activities.settings.user.UserSettingsPlace place = (com.voxgift.winstonplus.client.activities.settings.user.UserSettingsPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.settings.user.UserSettingsPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.settings.user.UserSettingsPlace.UserSettingsPlaceTokenizer.class);
      return new PrefixAndToken("UserSettingsPlace", t.getToken((com.voxgift.winstonplus.client.activities.settings.user.UserSettingsPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsPlace) {
      com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsPlace place = (com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsPlace.BasicUserSettingsPlaceTokenizer.class);
      return new PrefixAndToken("BasicUserSettingsPlace", t.getToken((com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsPlace) {
      com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsPlace place = (com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsPlace.FavoritesUserSettingsPlaceTokenizer.class);
      return new PrefixAndToken("FavoritesUserSettingsPlace", t.getToken((com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsPlace) {
      com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsPlace place = (com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsPlace.ImageUserSettingsPlaceTokenizer.class);
      return new PrefixAndToken("ImageUserSettingsPlace", t.getToken((com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckPlace) {
      com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckPlace place = (com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckPlace.ConnectCheckPlaceTokenizer.class);
      return new PrefixAndToken("ConnectCheckPlace", t.getToken((com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace) {
      com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace place = (com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace.HelpPlaceTokenizer.class);
      return new PrefixAndToken("HelpPlace", t.getToken((com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace) {
      com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace place = (com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace.LoginPlaceTokenizer.class);
      return new PrefixAndToken("LoginPlace", t.getToken((com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.welcome.register.RegisterPlace) {
      com.voxgift.winstonplus.client.activities.welcome.register.RegisterPlace place = (com.voxgift.winstonplus.client.activities.welcome.register.RegisterPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.welcome.register.RegisterPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.welcome.register.RegisterPlace.RegisterPlaceTokenizer.class);
      return new PrefixAndToken("RegisterPlace", t.getToken((com.voxgift.winstonplus.client.activities.welcome.register.RegisterPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordPlace) {
      com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordPlace place = (com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordPlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordPlace> t = GWT.create(com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordPlace.ResetPasswordPlaceTokenizer.class);
      return new PrefixAndToken("ResetPasswordPlace", t.getToken((com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordPlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace) {
      com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace place = (com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace> t = GWT.create(com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace.WelcomePlaceTokenizer.class);
      return new PrefixAndToken("WelcomePlace", t.getToken((com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace) place));
    }
    if (newPlace instanceof com.voxgift.winstonplus.client.places.HomePlace) {
      com.voxgift.winstonplus.client.places.HomePlace place = (com.voxgift.winstonplus.client.places.HomePlace) newPlace;
      PlaceTokenizer<com.voxgift.winstonplus.client.places.HomePlace> t = GWT.create(com.voxgift.winstonplus.client.places.HomePlace.HomePlaceTokenizer.class);
      return new PrefixAndToken("HomePlace", t.getToken((com.voxgift.winstonplus.client.places.HomePlace) place));
    }
    return null;
  }
  
  protected PlaceTokenizer<?> getTokenizer(String prefix) {
    if ("UserCircleSettingsPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.settings.circle.user.UserCircleSettingsPlace.UserCircleSettingsPlaceTokenizer.class);
    }
    if ("RegisterPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.welcome.register.RegisterPlace.RegisterPlaceTokenizer.class);
    }
    if ("FavoritesUserSettingsPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.settings.user.favorites.FavoritesUserSettingsPlace.FavoritesUserSettingsPlaceTokenizer.class);
    }
    if ("LoginPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.welcome.login.LoginPlace.LoginPlaceTokenizer.class);
    }
    if ("ComposePlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.compose.ComposePlace.ComposePlaceTokenizer.class);
    }
    if ("BasicUserSettingsPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.settings.user.basic.BasicUserSettingsPlace.BasicUserSettingsPlaceTokenizer.class);
    }
    if ("ConnectCheckPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.welcome.connectcheck.ConnectCheckPlace.ConnectCheckPlaceTokenizer.class);
    }
    if ("LegalDocumentsPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.settings.legal.LegalDocumentsPlace.LegalDocumentsPlaceTokenizer.class);
    }
    if ("WelcomePlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.welcome.welcome.WelcomePlace.WelcomePlaceTokenizer.class);
    }
    if ("MessageListPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.messagelist.MessageListPlace.MessageListPlaceTokenizer.class);
    }
    if ("SettingsPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.settings.SettingsPlace.SettingsPlaceTokenizer.class);
    }
    if ("HomePlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.places.HomePlace.HomePlaceTokenizer.class);
    }
    if ("UserSettingsPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.settings.user.UserSettingsPlace.UserSettingsPlaceTokenizer.class);
    }
    if ("LegalDocumentPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.settings.legal.document.LegalDocumentPlace.LegalDocumentPlaceTokenizer.class);
    }
    if ("BasicCircleSettingsPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.settings.circle.basic.BasicCircleSettingsPlace.BasicCircleSettingsPlaceTokenizer.class);
    }
    if ("CircleSettingsPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.settings.circle.CircleSettingsPlace.CircleSettingsPlaceTokenizer.class);
    }
    if ("HelpPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.welcome.help.HelpPlace.HelpPlaceTokenizer.class);
    }
    if ("ImageUserSettingsPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.settings.user.image.ImageUserSettingsPlace.ImageUserSettingsPlaceTokenizer.class);
    }
    if ("ResetPasswordPlace".equals(prefix)) {
      return GWT.create(com.voxgift.winstonplus.client.activities.welcome.resetpassword.ResetPasswordPlace.ResetPasswordPlaceTokenizer.class);
    }
    return null;
  }

}
