package com.voxgift.winstonplus.client.activities.compose;

public class DictionaryEntryCell_TemplateImpl implements com.voxgift.winstonplus.client.activities.compose.DictionaryEntryCell.Template {
  
  public com.google.gwt.safehtml.shared.SafeHtml content(java.lang.String arg0) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<div class='dictionary_entry'>");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
    sb.append("</div>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}
