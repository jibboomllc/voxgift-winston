package com.voxgift.winstonplus.client.css;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ResourcePrototype;

public class CssBundle_safari_default_InlineClientBundleGenerator implements com.voxgift.winstonplus.client.css.CssBundle {
  private static CssBundle_safari_default_InlineClientBundleGenerator _instance0 = new CssBundle_safari_default_InlineClientBundleGenerator();
  private void actionBarCssInitializer() {
    actionBarCss = new com.voxgift.winstonplus.client.css.ActionBarCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "actionBarCss";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GEG3RTUDBR{display:" + ("-webkit-box")  + ";-webkit-box-flex:" + ("1")  + ";-webkit-box-orient:" + ("vertical")  + ";width:" + ("100%")  + ";height:" + ("100%")  + ";}.GEG3RTUDCR{display:" + ("-webkit-box")  + ";-webkit-box-flex:" + ("1")  + ";-webkit-box-orient:" + ("vertical")  + ";overflow:" + ("hidden")  + ";}.GEG3RTUDDR{display:" + ("-webkit-box")  + ";-webkit-box-orient:") + (("horizontal")  + ";display:" + ("-webkit-box")  + ";-webkit-box-orient:" + ("horizontal")  + ";height:" + ("48px")  + ";border-top:" + ("1px"+ " " +"solid"+ " " +"black")  + ";}.GEG3RTUDFR{min-width:" + ("60px")  + ";background-color:" + ("transparent")  + ";box-sizing:" + ("content-box")  + ";-webkit-appearance:" + ("none")  + ";-webkit-box-flex:" + ("1")  + ";margin:" + ("2px") ) + (";-webkit-border-radius:" + ("3px")  + ";}.GEG3RTUDHR{background-color:" + ("rgba(" + "255"+ ","+ " " +"255"+ ","+ " " +"255"+ ","+ " " +"0.15" + ")")  + ";}.GEG3RTUDHR .GEG3RTUDIR{color:" + ("white")  + ";}.GEG3RTUDIR{font-size:" + ("9px")  + ";text-align:" + ("center")  + ";z-index:" + ("1")  + ";margin-top:" + ("5px")  + ";}.GEG3RTUDGR{margin-right:" + ("auto")  + ";margin-left:" + ("auto")  + ";z-index:" + ("0")  + ";background-image:") + (("-webkit-gradient(linear, 30% 0%, 60% 70%, from(#888), to(#444))")  + ";}")) : ((".GEG3RTUDBR{display:" + ("-webkit-box")  + ";-webkit-box-flex:" + ("1")  + ";-webkit-box-orient:" + ("vertical")  + ";width:" + ("100%")  + ";height:" + ("100%")  + ";}.GEG3RTUDCR{display:" + ("-webkit-box")  + ";-webkit-box-flex:" + ("1")  + ";-webkit-box-orient:" + ("vertical")  + ";overflow:" + ("hidden")  + ";}.GEG3RTUDDR{display:" + ("-webkit-box")  + ";-webkit-box-orient:") + (("horizontal")  + ";display:" + ("-webkit-box")  + ";-webkit-box-orient:" + ("horizontal")  + ";height:" + ("48px")  + ";border-top:" + ("1px"+ " " +"solid"+ " " +"black")  + ";}.GEG3RTUDFR{min-width:" + ("60px")  + ";background-color:" + ("transparent")  + ";box-sizing:" + ("content-box")  + ";-webkit-appearance:" + ("none")  + ";-webkit-box-flex:" + ("1")  + ";margin:" + ("2px") ) + (";-webkit-border-radius:" + ("3px")  + ";}.GEG3RTUDHR{background-color:" + ("rgba(" + "255"+ ","+ " " +"255"+ ","+ " " +"255"+ ","+ " " +"0.15" + ")")  + ";}.GEG3RTUDHR .GEG3RTUDIR{color:" + ("white")  + ";}.GEG3RTUDIR{font-size:" + ("9px")  + ";text-align:" + ("center")  + ";z-index:" + ("1")  + ";margin-top:" + ("5px")  + ";}.GEG3RTUDGR{margin-left:" + ("auto")  + ";margin-right:" + ("auto")  + ";z-index:" + ("0")  + ";background-image:") + (("-webkit-gradient(linear, 30% 0%, 60% 70%, from(#888), to(#444))")  + ";}"));
      }
      public java.lang.String actionPanel(){
        return "GEG3RTUDBR";
      }
      public java.lang.String actionPanelContainer(){
        return "GEG3RTUDCR";
      }
      public java.lang.String actionbar(){
        return "GEG3RTUDDR";
      }
      public java.lang.String active(){
        return "GEG3RTUDER";
      }
      public java.lang.String button(){
        return "GEG3RTUDFR";
      }
      public java.lang.String icon(){
        return "GEG3RTUDGR";
      }
      public java.lang.String selected(){
        return "GEG3RTUDHR";
      }
      public java.lang.String text(){
        return "GEG3RTUDIR";
      }
    }
    ;
  }
  private static class actionBarCssInitializer {
    static {
      _instance0.actionBarCssInitializer();
    }
    static com.voxgift.winstonplus.client.css.ActionBarCss get() {
      return actionBarCss;
    }
  }
  public com.voxgift.winstonplus.client.css.ActionBarCss actionBarCss() {
    return actionBarCssInitializer.get();
  }
  private void loginInputCssInitializer() {
    loginInputCss = new com.voxgift.winstonplus.client.css.LoginInputCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "loginInputCss";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GEG3RTUDPR .GEG3RTUDJR,.GEG3RTUDMR .GEG3RTUDJR,.GEG3RTUDOR .GEG3RTUDJR{color:" + ("#777")  + ";border:" + ("0")  + ";font:" + ("normal"+ " " +"17px"+ " " +"Helvetica")  + ";padding:" + ("0")  + ";display:" + ("inline-block")  + ";margin-right:" + ("0")  + ";width:" + ("100%")  + ";-webkit-appearance:" + ("none")  + ";-moz-appearance:" + ("none")  + ";-webkit-user-select:" + ("text")  + ";-moz-user-select:") + (("text")  + ";-webkit-tap-highlight-color:" + ("rgba(" + "0"+ ","+ " " +"0"+ ","+ " " +"0"+ ","+ " " +"0" + ")")  + ";-webkit-user-modify:" + ("read-write-plaintext-only")  + ";}.GEG3RTUDPR .GEG3RTUDJR,.GEG3RTUDMR .GEG3RTUDJR,.GEG3RTUDOR .GEG3RTUDJR{display:" + ("-webkit-box")  + ";-webkit-box-flex:" + ("1")  + ";}.GEG3RTUDPR,.GEG3RTUDMR,.GEG3RTUDOR{position:" + ("relative")  + ";display:" + ("-webkit-box")  + ";-webkit-box-flex:" + ("1")  + ";}.GEG3RTUDNR{position:" + ("relative")  + ";display:" + ("-webkit-box")  + ";-webkit-box-orient:" + ("horizontal") ) + (";-webkit-box-flex:" + ("1")  + ";}.GEG3RTUDNR>label{display:" + ("-webkit-box")  + ";-webkit-box-flex:" + ("1")  + ";}.GEG3RTUDKR{color:" + ("#000")  + ";}.GEG3RTUDLR{color:" + ("#777")  + ";border:" + ("0")  + ";font:" + ("normal"+ " " +"17px"+ " " +"Helvetica")  + ";padding:" + ("0")  + ";margin-right:" + ("0")  + ";width:" + ("100%")  + ";height:") + (("100%")  + ";display:" + ("-webkit-box")  + ";-webkit-appearance:" + ("none")  + ";-webkit-user-select:" + ("text")  + ";}")) : ((".GEG3RTUDPR .GEG3RTUDJR,.GEG3RTUDMR .GEG3RTUDJR,.GEG3RTUDOR .GEG3RTUDJR{color:" + ("#777")  + ";border:" + ("0")  + ";font:" + ("normal"+ " " +"17px"+ " " +"Helvetica")  + ";padding:" + ("0")  + ";display:" + ("inline-block")  + ";margin-left:" + ("0")  + ";width:" + ("100%")  + ";-webkit-appearance:" + ("none")  + ";-moz-appearance:" + ("none")  + ";-webkit-user-select:" + ("text")  + ";-moz-user-select:") + (("text")  + ";-webkit-tap-highlight-color:" + ("rgba(" + "0"+ ","+ " " +"0"+ ","+ " " +"0"+ ","+ " " +"0" + ")")  + ";-webkit-user-modify:" + ("read-write-plaintext-only")  + ";}.GEG3RTUDPR .GEG3RTUDJR,.GEG3RTUDMR .GEG3RTUDJR,.GEG3RTUDOR .GEG3RTUDJR{display:" + ("-webkit-box")  + ";-webkit-box-flex:" + ("1")  + ";}.GEG3RTUDPR,.GEG3RTUDMR,.GEG3RTUDOR{position:" + ("relative")  + ";display:" + ("-webkit-box")  + ";-webkit-box-flex:" + ("1")  + ";}.GEG3RTUDNR{position:" + ("relative")  + ";display:" + ("-webkit-box")  + ";-webkit-box-orient:" + ("horizontal") ) + (";-webkit-box-flex:" + ("1")  + ";}.GEG3RTUDNR>label{display:" + ("-webkit-box")  + ";-webkit-box-flex:" + ("1")  + ";}.GEG3RTUDKR{color:" + ("#000")  + ";}.GEG3RTUDLR{color:" + ("#777")  + ";border:" + ("0")  + ";font:" + ("normal"+ " " +"17px"+ " " +"Helvetica")  + ";padding:" + ("0")  + ";margin-left:" + ("0")  + ";width:" + ("100%")  + ";height:") + (("100%")  + ";display:" + ("-webkit-box")  + ";-webkit-appearance:" + ("none")  + ";-webkit-user-select:" + ("text")  + ";}"));
      }
      public java.lang.String box(){
        return "GEG3RTUDJR";
      }
      public java.lang.String disabled(){
        return "GEG3RTUDKR";
      }
      public java.lang.String listBox(){
        return "GEG3RTUDLR";
      }
      public java.lang.String passwordBox(){
        return "GEG3RTUDMR";
      }
      public java.lang.String radioButton(){
        return "GEG3RTUDNR";
      }
      public java.lang.String textArea(){
        return "GEG3RTUDOR";
      }
      public java.lang.String textBox(){
        return "GEG3RTUDPR";
      }
    }
    ;
  }
  private static class loginInputCssInitializer {
    static {
      _instance0.loginInputCssInitializer();
    }
    static com.voxgift.winstonplus.client.css.LoginInputCss get() {
      return loginInputCss;
    }
  }
  public com.voxgift.winstonplus.client.css.LoginInputCss loginInputCss() {
    return loginInputCssInitializer.get();
  }
  private void loginListCssInitializer() {
    loginListCss = new com.voxgift.winstonplus.client.css.LoginListCss() {
      private boolean injected;
      public boolean ensureInjected() {
        if (!injected) {
          injected = true;
          com.google.gwt.dom.client.StyleInjector.inject(getText());
          return true;
        }
        return false;
      }
      public String getName() {
        return "loginListCss";
      }
      public String getText() {
        return com.google.gwt.i18n.client.LocaleInfo.getCurrentLocale().isRTL() ? ((".GEG3RTUDCS{background-color:" + ("#fff")  + ";display:" + ("-webkit-box")  + ";border-top:" + ("solid"+ " " +"1px"+ " " +"#d3d3d3")  + ";border-bottom:" + ("solid"+ " " +"1px"+ " " +"#d3d3d3")  + ";width:" + ("100%")  + ";}.GEG3RTUDES{color:" + ("#000")  + ";background-color:" + ("#f3f3f3")  + ";padding:" + ("10px")  + ";width:" + ("30%")  + ";overflow:" + ("hidden")  + ";text-overflow:") + (("ellipsis")  + ";text-align:" + ("left")  + ";}.GEG3RTUDDS{padding:" + ("10px")  + ";width:" + ("60%")  + ";}")) : ((".GEG3RTUDCS{background-color:" + ("#fff")  + ";display:" + ("-webkit-box")  + ";border-top:" + ("solid"+ " " +"1px"+ " " +"#d3d3d3")  + ";border-bottom:" + ("solid"+ " " +"1px"+ " " +"#d3d3d3")  + ";width:" + ("100%")  + ";}.GEG3RTUDES{color:" + ("#000")  + ";background-color:" + ("#f3f3f3")  + ";padding:" + ("10px")  + ";width:" + ("30%")  + ";overflow:" + ("hidden")  + ";text-overflow:") + (("ellipsis")  + ";text-align:" + ("right")  + ";}.GEG3RTUDDS{padding:" + ("10px")  + ";width:" + ("60%")  + ";}"));
      }
      public java.lang.String canbeSelected(){
        return "GEG3RTUDAS";
      }
      public java.lang.String first(){
        return "GEG3RTUDBS";
      }
      public java.lang.String formListElement(){
        return "GEG3RTUDCS";
      }
      public java.lang.String formListElementContainer(){
        return "GEG3RTUDDS";
      }
      public java.lang.String formListElementLabel(){
        return "GEG3RTUDES";
      }
      public java.lang.String group(){
        return "GEG3RTUDFS";
      }
      public java.lang.String last(){
        return "GEG3RTUDGS";
      }
      public java.lang.String listCss(){
        return "GEG3RTUDHS";
      }
      public java.lang.String listHeadElement(){
        return "GEG3RTUDIS";
      }
      public java.lang.String listHeader(){
        return "GEG3RTUDJS";
      }
      public java.lang.String round(){
        return "GEG3RTUDKS";
      }
      public java.lang.String selected(){
        return "GEG3RTUDLS";
      }
    }
    ;
  }
  private static class loginListCssInitializer {
    static {
      _instance0.loginListCssInitializer();
    }
    static com.voxgift.winstonplus.client.css.LoginListCss get() {
      return loginListCss;
    }
  }
  public com.voxgift.winstonplus.client.css.LoginListCss loginListCss() {
    return loginListCssInitializer.get();
  }
  private static java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype> resourceMap;
  private static com.voxgift.winstonplus.client.css.ActionBarCss actionBarCss;
  private static com.voxgift.winstonplus.client.css.LoginInputCss loginInputCss;
  private static com.voxgift.winstonplus.client.css.LoginListCss loginListCss;
  
  public ResourcePrototype[] getResources() {
    return new ResourcePrototype[] {
      actionBarCss(), 
      loginInputCss(), 
      loginListCss(), 
    };
  }
  public ResourcePrototype getResource(String name) {
    if (GWT.isScript()) {
      return getResourceNative(name);
    } else {
      if (resourceMap == null) {
        resourceMap = new java.util.HashMap<java.lang.String, com.google.gwt.resources.client.ResourcePrototype>();
        resourceMap.put("actionBarCss", actionBarCss());
        resourceMap.put("loginInputCss", loginInputCss());
        resourceMap.put("loginListCss", loginListCss());
      }
      return resourceMap.get(name);
    }
  }
  private native ResourcePrototype getResourceNative(String name) /*-{
    switch (name) {
      case 'actionBarCss': return this.@com.voxgift.winstonplus.client.css.CssBundle::actionBarCss()();
      case 'loginInputCss': return this.@com.voxgift.winstonplus.client.css.CssBundle::loginInputCss()();
      case 'loginListCss': return this.@com.voxgift.winstonplus.client.css.CssBundle::loginListCss()();
    }
    return null;
  }-*/;
}
