package com.voxgift.winstonplus.client.activities.messagelist;

public class MessageCell_TemplateImpl implements com.voxgift.winstonplus.client.activities.messagelist.MessageCell.Template {
  
  public com.google.gwt.safehtml.shared.SafeHtml content(java.lang.String arg0,java.lang.String arg1,java.lang.String arg2,com.google.gwt.safehtml.shared.SafeUri arg3) {
    StringBuilder sb = new java.lang.StringBuilder();
    sb.append("<div class='message_cell'><div class='avatar'><img src='");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg3.asString()));
    sb.append("'/></div><div class='text'><div class='title'><div class='user'>");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg2));
    sb.append("</div><div class='time'>");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg1));
    sb.append("</div></div><div class='message'>");
    sb.append(com.google.gwt.safehtml.shared.SafeHtmlUtils.htmlEscape(arg0));
    sb.append("</div></div></div>");
return new com.google.gwt.safehtml.shared.OnlyToBeUsedInGeneratedCodeStringBlessedAsSafeHtml(sb.toString());
}
}
