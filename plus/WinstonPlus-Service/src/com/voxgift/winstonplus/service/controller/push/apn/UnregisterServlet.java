/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.voxgift.winstonplus.service.controller.push.apn;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.voxgift.winstonplus.service.controller.BaseServlet;
import com.voxgift.winstonplus.service.model.PushDeviceRepository;
import com.voxgift.winstonplus.service.model.PushDeviceRepositoryJDBCImpl;
import com.voxgift.winstonplus.service.model.PushServiceType;

/**
Another way to keep your list of active device tokens up to date is to have your server application check in periodically with the APNs servers. Listing 5 shows how to query the APNs feedback service using JavaPNS to receive a list of invalid device tokens from the APNs sandbox:

Listing 5. Checking in to update active device tokens

List<Device> inactiveDevices = Push.feedback("keypair.p12", "password", false);
//* remove inactive devices from your own list of devices

It's important not to waste resources by sending messages to devices that have deleted your application or opted out of receiving notifications.
*/
@SuppressWarnings("serial")
public class UnregisterServlet extends BaseServlet {

	private static final String PARAMETER_REG_ID = "regId";

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException {
		String regId = getParameter(req, PARAMETER_REG_ID);
		PushDeviceRepository repo = new PushDeviceRepositoryJDBCImpl(PushServiceType.APN);
		repo.unregister(regId);
		setSuccess(resp);
	}

}
