package com.voxgift.winstonplus.service.controller;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

@SuppressWarnings("serial")
public class ImageUploadServlet extends HttpServlet {

	private final static Logger LOGGER = Logger.getLogger(ImageUploadServlet.class.getName()); 

	private static final String UPLOAD_DIRECTORY = "/Library/WebServer/Documents/public_html/users/raw";
	private static final String OUTPUT_DIRECTORY = "/Library/WebServer/Documents/public_html/users";
	private static final String DEFAULT_IMAGE = "mm.png";

	/*
	 * GET generates an image based on given data while PUT uses uploaded file
	 *
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//super.doGet(req, resp); calling this will throw a 405 GET not supported

		resp.setContentType("text/html; charset=UTF-8");
		String userId = (String) req.getParameter("userId");
		String userEmail = (String) req.getParameter("userEmail");

		if (userEmail != null && userId != null) {
			File uploadedFile = new File(UPLOAD_DIRECTORY, userId);
			if (!uploadedFile.exists()) {
				uploadedFile = new File(UPLOAD_DIRECTORY, DEFAULT_IMAGE);
			}
			File processedFile = new File(OUTPUT_DIRECTORY, userId + ".png");

			process(uploadedFile, processedFile);
			resp.setStatus(HttpServletResponse.SC_CREATED);
			LOGGER.info("default image file created based on " + uploadedFile.getCanonicalPath());
			resp.getWriter().print("The default image file was created successfully.");
		} else {
			if (userEmail == null) {
				LOGGER.warning("userEmail not found");
			}
			if (userId == null) {
				LOGGER.warning("userId not found");
			}
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST,
					"Request contents missing fields. Need both userId and userEmail."); // 400
		}
		resp.flushBuffer();
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		LOGGER.info("in doPost");
		resp.setContentType("text/html; charset=UTF-8");

		// process only multipart requests
		if (ServletFileUpload.isMultipartContent(req)) {

			// Create a factory for disk-based file items
			FileItemFactory factory = new DiskFileItemFactory();

			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);

			// Parse the request
			try {
				@SuppressWarnings("unchecked")
				List<FileItem> items = upload.parseRequest(req);
				FileItem fileItem = null;
				String userId = null;

				LOGGER.info("Num items in request: " + items.size());				
				for (FileItem item : items) {
					if (item.isFormField()) {
						if (item.getFieldName().equals("userId")) {
							userId = item.getString();
							LOGGER.info("userId found: " + userId);
						}
					} else {
						fileItem = item;
					}
				}

				if (fileItem != null && userId != null) {
					File uploadedFile = new File(UPLOAD_DIRECTORY, userId);
					File processedFile = new File(OUTPUT_DIRECTORY, userId + ".png");
					fileItem.write(uploadedFile);	
					process(uploadedFile, processedFile);
					resp.setStatus(HttpServletResponse.SC_CREATED);
					LOGGER.info("file created");
					resp.getWriter().print("The file was created successfully.");
					resp.flushBuffer();
				} else {
					if (fileItem == null) {
						LOGGER.warning("fileItem not found");
					}
					if (userId == null) {
						LOGGER.warning("userId not found");
					}
					resp.sendError(HttpServletResponse.SC_BAD_REQUEST,
							"Request contents missing fields."); // 400
				}
			} catch (Exception e) {
				LOGGER.warning("An error occurred while creating the file : " + e.getMessage());
				resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
						"An error occurred while creating the file : " + e.getMessage()); // 500
			}

		} else {
			LOGGER.warning("Request contents type is not supported by the servlet.");
			resp.sendError(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE,
					"Request contents type is not supported by the servlet."); // 415
		}
	}

	private static final int IMG_WIDTH = 48;
	private static final int IMG_HEIGHT = 48;
	private static final int CORNER_RADIUS = 10;

	public static void process (File inFile, File outFile) {

		try{

			BufferedImage originalImage = ImageIO.read(inFile);
			int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();

			BufferedImage resizeImageHintPng = resizeImageWithHint(originalImage, type);
			BufferedImage resizeImageHintRCPng = makeRoundedCorner(resizeImageHintPng, CORNER_RADIUS);
			ImageIO.write(resizeImageHintRCPng, "png", outFile); 

		}catch(IOException e){
			System.out.println(e.getMessage());
		}

	}

	private static BufferedImage resizeImageWithHint(BufferedImage originalImage, int type){

		BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
		g.dispose();	
		g.setComposite(AlphaComposite.Src);

		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		return resizedImage;
	}	

	public static BufferedImage makeRoundedCorner(BufferedImage image, int cornerRadius) {
		int w = image.getWidth();
		int h = image.getHeight();
		BufferedImage output = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);

		Graphics2D g2 = output.createGraphics();

		// This is what we want, but it only does hard-clipping, i.e. aliasing
		// g2.setClip(new RoundRectangle2D ...)

		// so instead fake soft-clipping by first drawing the desired clip shape
		// in fully opaque white with antialiasing enabled...
		g2.setComposite(AlphaComposite.Src);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2.setColor(Color.WHITE);
		g2.fill(new RoundRectangle2D.Float(0, 0, w, h, cornerRadius, cornerRadius));

		// ... then compositing the image on top,
		// using the white shape from above as alpha source
		g2.setComposite(AlphaComposite.SrcAtop);
		g2.drawImage(image, 0, 0, null);

		g2.dispose();

		return output;
	}
}
