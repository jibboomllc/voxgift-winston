/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.voxgift.winstonplus.service.controller.push.apn;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushedNotification;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.voxgift.winstonplus.service.controller.BaseServlet;
import com.voxgift.winstonplus.service.model.PushDeviceRepository;
import com.voxgift.winstonplus.service.model.PushDeviceRepositoryJDBCImpl;
import com.voxgift.winstonplus.service.model.PushServiceType;

/**
 * Servlet that adds a new message to all registered devices.
 * <p>
 * This servlet is used just by the browser (i.e., not device).
 */
@SuppressWarnings("serial")
public class SendAllMessagesServlet extends BaseServlet {
	private final static Logger LOGGER = Logger.getLogger(SendAllMessagesServlet.class.getName()); 

	static final String KEYSTORE_P12_FILE = "/WEB-INF/lib/DevPushCertificates.p12";
	static final String KEYSTORE_PASSWORD = "4pn_push";

	/**
	 * Processes the request to add a new message.
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		LOGGER.info("1");
		PushDeviceRepository repo = new PushDeviceRepositoryJDBCImpl(PushServiceType.APN);
		List<String> devices = repo.getDevices();
		String status = new String();
		if (devices.isEmpty()) {
			status = "Message ignored as there is no device registered!";
		} else {
			InputStream inputStream = getServletContext().getResourceAsStream(KEYSTORE_P12_FILE);
			if(inputStream == null) {
				LOGGER.info("Keystore file not found");
			} else {
//				for (String device : devices) {
					//byte[] decoded = Base64.decodeBase64(device);
					//System.out.println(new String(decoded, "UTF-8") + "\n");
					//String hexDevice = bytesToHex(decoded);
					DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
					Date date = new Date();

					//Push.alert("Hello World!", "keystore.p12", "keystore_password", false, "Your token");
					try {
//						LOGGER.info("Pushing to: " + device);
						List<PushedNotification> notifications = Push.alert("hello apn " + dateFormat.format(date), inputStream, KEYSTORE_PASSWORD, false, devices);
						for (PushedNotification notification : notifications) {
					        String token = notification.getDevice().getToken();
						    if (notification.isSuccessful()) {
						    	LOGGER.info("Apple accepted the notification and should deliver it: " + token);
						    } else {
						        /* Add code here to remove invalidToken from your database */
						    	LOGGER.info("Apple did NOT accept the notification: " + token);
						    }
						}
					
					} catch (CommunicationException e) {
				    	LOGGER.warning("CommunicationException: " + e.getLocalizedMessage());
					} catch (KeystoreException e) {
				    	LOGGER.warning("KeystoreException: " + e.getLocalizedMessage());
					}
//					status.concat("Message sent to: " + device);
//				}
			}
		}
		req.setAttribute(HomeServlet.ATTRIBUTE_STATUS, status.toString());
		getServletContext().getRequestDispatcher("/apn/home").forward(req, resp);
	}

	public static String bytesToHex(byte[] bytes) {
		final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
		char[] hexChars = new char[bytes.length * 2];
		int v;
		for ( int j = 0; j < bytes.length; j++ ) {
			v = bytes[j] & 0xFF;
			hexChars[j * 2] = hexArray[v >>> 4];
			hexChars[j * 2 + 1] = hexArray[v & 0x0F];
		}
		return new String(hexChars);
	}
}
