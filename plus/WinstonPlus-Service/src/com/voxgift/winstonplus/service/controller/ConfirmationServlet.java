package com.voxgift.winstonplus.service.controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.voxgift.winstonplus.service.model.CircleUserStatus;
import com.voxgift.winstonplus.service.model.Confirmation;
import com.voxgift.winstonplus.service.model.UserRepository;
import com.voxgift.winstonplus.service.model.UserRepositoryJDBCImpl;

@SuppressWarnings("serial")
public class ConfirmationServlet extends HttpServlet {

	private final static Logger LOGGER = Logger.getLogger(ConfirmationServlet.class.getName()); 

	private UserRepository userRepo;

	private final static String head = "<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8'><link href='https://voxgift.com/styles/layout.css' rel='stylesheet'></head>";
	private final static String header = "<body id='top'><div class='wrapper'><div id='header'><div class='fl_left'><h1><a href='https://voxgift.com/index'>Voxgift</a></h1><p>Giving patients the gift of voice</p></div><br class='clear' /></div>";
	private final static String footer = "<div id='copyright'><p class='fl_left'>Copyright &copy; 2011-2013 All Rights Reserved - <a href='http://www.jibboom.com/'>Jibboom, LLC</a></p><br class='clear' /></div><br class='clear' /></div></body></html>";

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		LOGGER.setLevel(Level.INFO);

		response.setContentType("text/html; charset=UTF-8");

		// get the confirmation id from the 'id' parameter
		String id = (String) request.getParameter("id");
		if (id == null || id.equals("")) {
			LOGGER.warning("id not found");

			response.sendError(HttpServletResponse.SC_BAD_REQUEST,
					"Request contents missing id field."); // 400	
		} else {
			String result = processConfirmationID(id);
			response.setStatus(HttpServletResponse.SC_CREATED);
			LOGGER.info(result);
			response.getWriter().print(result);
		}
		response.flushBuffer();

	} //end doGet

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request,response);
	}

	public String processConfirmationID(String id)
			throws ServletException, IOException {
		String result = "<p>";

		if (userRepo == null) {
			userRepo = new UserRepositoryJDBCImpl();
		}

		Confirmation confirmation = userRepo.getConfirmation(id);
		if (confirmation != null) {
			if (confirmation.getUserId() != null
					&& (confirmation.getEmail() != null || confirmation.getCircleId() != null)) {
				if (!confirmation.getExpired()) {
					if (confirmation.getEmail() != null) {
						if (userRepo.confirmEmail(confirmation.getUserId(), confirmation.getEmail()) == true) {
							result += "Your email address is confirmed. ";
						} else {
							result += "Sorry, your email address could NOT be confirmed. ";
						}
					}
					if (confirmation.getCircleId() != null) {
						if (userRepo.changeCircleStatus(confirmation.getUserId(), confirmation.getCircleId(), CircleUserStatus.ACCEPTED)) {
							result += "Your circle status has been changed to Accepted. ";
						} else {
							result += "Sorry, your circle status could NOT be changed to Accepted. ";
						}
					}
				} else {
					result += "This confirmation/invitation is expired. ";
				}
			} else {
				result += "Oops, the confirmation record is badly formed. ";
			}
		} else {
			result += "That confirmation record was not found. Please check the link and, if you have not already tried, click on the link a secoond time.";
		}

		result += "</p>";

		result += "<p>If you have not already done so, please download WinstonPlus from the"
				+ " <a href='https://itunes.apple.com/us/app/voxgift-winstonplus/id646885435?mt=8&amp;uo=4' target='itunes_store'>iTunes App Store</a>"
				+ " or <a href='https://play.google.com/store/apps/details?id=com.voxgift.winstonplus.android'>Google Play</a>.</p>";

		result += "<p>Please contact Voxgift Support at <a href='mailto:support@voxgift.com?subject=Confirmation record: "
				+ id + "'>support@voxgift.com</a>, if you have any questions or concerns.</p>";

		result += "<a href='https://itunes.apple.com/us/app/voxgift-winstonplus/id646885435?mt=8&amp;uo=4' target='itunes_store'> <img src='http://r.mzstatic.com/images/web/linkmaker/badge_appstore-lrg.gif' alt='Get WinstonPlus on the iTunes App Store' style='border: 0;' /></a>";
		result += "<a href='https://play.google.com/store/apps/details?id=com.voxgift.winstonplus.android'><img src='https://developer.android.com/images/brand/en_generic_rgb_wo_45.png' alt='Get WinstonPlus on Google Play'/></a>";

		return head + header + result + footer;
	}

}
