/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.voxgift.winstonplus.service.controller.push;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.voxgift.winstonplus.service.controller.BaseServlet;
import com.voxgift.winstonplus.service.model.PushDeviceRepository;
import com.voxgift.winstonplus.service.model.PushDeviceRepositoryJDBCImpl;
import com.voxgift.winstonplus.service.model.PushServiceType;

/**
 * Servlet that adds a new message to all registered devices for a circle.
 */
@SuppressWarnings("serial")
public class SendMessageServlet extends BaseServlet {

	/**
	 * Processes the request to add a new message.
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		String circleId = getParameter(req, "circleId");
		String userId = getParameter(req, "userId");
		String messageId = getParameter(req, "messageId");
		
		PushDeviceRepository repo = new PushDeviceRepositoryJDBCImpl(PushServiceType.APN);
		List<String> devices = repo.getCirclePushDevices(circleId, userId);
		if (devices.isEmpty()) {
			logger.info("APN Message sending skipped as there are no appropriate devices registered!");
		} else {
			String notificationText = repo.getNotificationText(messageId, 128);
			logger.info("APN Message [" + notificationText + "] sending to " + devices.size() + " devices!");
			com.voxgift.winstonplus.service.controller.push.apn.SendMessageServlet apn = new com.voxgift.winstonplus.service.controller.push.apn.SendMessageServlet();
			apn.send(devices, notificationText);
		}

		repo = new PushDeviceRepositoryJDBCImpl(PushServiceType.GCM);
		devices = repo.getCirclePushDevices(circleId, userId);
		if (devices.isEmpty()) {
			logger.info("GCM Message sending skipped as there are no appropriate devices registered!");
		} else {
			String notificationText = repo.getNotificationText(messageId, 2048);
			logger.info("GCM Message sending [" + notificationText + "] to " + devices.size() + " devices!");
			com.voxgift.winstonplus.service.controller.push.gcm.SendMessageServlet gcm = new com.voxgift.winstonplus.service.controller.push.gcm.SendMessageServlet();
			gcm.send(devices, notificationText);
		}

	}
}
