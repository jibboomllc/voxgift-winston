/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.voxgift.winstonplus.service.controller.push.apn;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Logger;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.ResponsePacket;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import com.voxgift.winstonplus.service.controller.BaseServlet;
import com.voxgift.winstonplus.service.model.PushDeviceRepository;
import com.voxgift.winstonplus.service.model.PushDeviceRepositoryJDBCImpl;
import com.voxgift.winstonplus.service.model.PushServiceType;

/**
 * Servlet that adds a new message to all registered devices for a circle.
 */
@SuppressWarnings("serial")
public class SendMessageServlet extends BaseServlet {
	private final static Logger LOGGER = Logger.getLogger(SendMessageServlet.class.getName()); 

	//static final Boolean PRODUCTION = false; /// sandbox
	static final Boolean PRODUCTION = true;
	//static final String KEYSTORE_P12_FILE = "/WEB-INF/lib/DevPushCertificates.p12";
	static final String KEYSTORE_P12_FILE = "/WEB-INF/lib/ProdPushCertificates.p12";
	static final String KEYSTORE_PASSWORD = "4pn_push";
	static final int MAX_NOTIFICATION_TEXT_SIZE = 128;

	private PushDeviceRepository repo;

	public SendMessageServlet() {
		repo = new PushDeviceRepositoryJDBCImpl(PushServiceType.APN);
	}

	/**
	 * Processes the request to add a new message.
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		send(getParameter(req, "circleId"), getParameter(req, "userId"), getParameter(req, "messageId"));
	}

	public void send(String circleId, String userId, String messageId) {
		List<String> devices = repo.getCirclePushDevices(circleId, userId);
		if (devices == null || devices.isEmpty()) {
			LOGGER.info("Message ignored as there are no new APN devices registered for circle=" + circleId);
		} else {
			String notificationText = repo.getNotificationText(messageId, MAX_NOTIFICATION_TEXT_SIZE);
			LOGGER.info("APN Message [" + notificationText + "] sending to " + devices.size() + " devices!");
			send(devices, notificationText);
		}
	}

	public void send(List<String> devices, String notificationText) {
		InputStream keystore = getServletContext().getResourceAsStream(KEYSTORE_P12_FILE);
		if(keystore == null) {
			LOGGER.info("Keystore file not found");
		} else {
			try {
				PushNotificationPayload payload = PushNotificationPayload.complex();
				payload.addAlert(notificationText);
				payload.addSound("notification.mp3");
				payload.addBadge(0); // TODO - replace 1 with the correct number per device
				// See FAQ on https://code.google.com/p/javapns/wiki/PushNotificationBasic
				
				List<PushedNotification> notifications = Push.payload(payload, keystore, KEYSTORE_PASSWORD, PRODUCTION, devices);
				//List<PushedNotification> notifications = Push.alert(notificationText, keystore, KEYSTORE_PASSWORD, PRODUCTION, devices);
				for (PushedNotification notification : notifications) {
					String token = notification.getDevice().getToken();
					if (notification.isSuccessful()) {
						LOGGER.info("Apple accepted the notification and should deliver it: " + token);
					} else {
						LOGGER.warning("Apple did NOT accept the notification token: " + token);
						repo.unregister(token);
						
						// Find out more about what the problem was  
                        Exception theProblem = notification.getException();
                        theProblem.printStackTrace();

                        // If the problem was an error-response packet returned by Apple, get it  
                        ResponsePacket theErrorResponse = notification.getResponse();
                        if (theErrorResponse != null) {
                        	LOGGER.warning(theErrorResponse.getMessage());
                        }
					}
				}

			} catch (CommunicationException e) {
				LOGGER.warning("CommunicationException: " + e.getLocalizedMessage());
			} catch (KeystoreException e) {
				LOGGER.warning("KeystoreException: " + e.getLocalizedMessage());
			} catch (JSONException e) {
				LOGGER.warning("JSONException: " + e.getLocalizedMessage());
			}
		}
	}
}
