package com.voxgift.winstonplus.service.controller;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.lang.Runtime;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.voxgift.winstonplus.service.model.DictionaryRepository;
import com.voxgift.winstonplus.service.model.DictionaryRepositoryJDBCImpl;

@SuppressWarnings("serial")
public class AudioServlet extends HttpServlet {

	private final static Logger LOGGER = Logger.getLogger(AudioServlet.class.getName()); 

	private static final String AUDIO_DIRECTORY = "/Library/WebServer/Documents/public_html/audio";

	private DictionaryRepository dictionaryRepo;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		LOGGER.setLevel(Level.INFO);
		
		// get the file name from the 'file' parameter
		// 83d26244-a138-4243-be60-e605198ed58d_en_US_m
		// 5d3a214a-138c-5670-ab40-2b5207d267ee_en_US_f
		String fileName = (String) request.getParameter("file");
		if (fileName == null || fileName.equals(""))
			throw new ServletException(
					"Invalid or non-existent file parameter in SendMp3 servlet.");

		// add the .mp3 suffix if it doesn't already exist
		if (fileName.indexOf(".mp3") == -1)
			fileName = fileName + ".mp3";

		// where are MP3 files kept?
		// PRODVSDEV
		String mp3Dir = AUDIO_DIRECTORY; //getServletContext().getInitParameter("mp3-dir");
		if (mp3Dir == null || mp3Dir.equals(""))
			throw new ServletException(
					"Invalid or non-existent mp3Dir context-param.");
		
        if (dictionaryRepo == null) {
			dictionaryRepo = new DictionaryRepositoryJDBCImpl();
		}
		
		ServletOutputStream stream = null;
		BufferedInputStream buf = null;
		try{
			stream = response.getOutputStream();
			File mp3 = new File(mp3Dir + "/" + fileName);

			String[] details = fileName.split("_");
			String id = details[0];
			String language = details.length > 1 ? details[1] : "en";
			String locale = details.length > 2 ? details[2] : "US";
			String gender = details.length > 3 ? details[3].substring(0, 1) : "f";

			String entry = dictionaryRepo.lookupFavorite(id, language, locale);

			if (entry == null) {
				entry = dictionaryRepo.lookupAdhocEntry(id, language, locale);

				if (entry == null) {
				    entry = dictionaryRepo.lookupTranslation(id, language, locale);
					if (entry == null) {
						throw new ServletException("Error looking up entry for: " + fileName);
					}
				}
			}
			
			if (!mp3.exists()) {
				if (createFile(mp3, entry, language, locale, gender) != 0) {
					throw new ServletException("Error creating: " + mp3.getAbsolutePath() );
				}
			}

			//set response headers
			response.setContentType("audio/mpeg");

			response.addHeader("Content-Disposition","attachment; filename="+fileName );
			response.addHeader("Cache-Control", "max-age=31556926");

			response.setContentLength( (int) mp3.length() );

			FileInputStream input = new FileInputStream(mp3);
			buf = new BufferedInputStream(input);
			int readBytes = 0;

			//read from the file; write to the ServletOutputStream
			while((readBytes = buf.read()) != -1)
				stream.write(readBytes);

		} catch (IOException ioe){

			throw new ServletException(ioe.getMessage());

		} finally {

			//close the input/output streams
			if(stream != null)
				stream.close();
			if(buf != null)
				buf.close();
		}

	} //end doGet

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request,response);
	}

	public int createFile(File outFile, String entry, String language, String locale, String gender)
	{
		try
		{
			File tempFile = new File(outFile + ".m4a");

			int exitVal = createM4a(tempFile, getVoice(language, locale, gender), entry);
			LOGGER.info("createM4a exitVal = " + exitVal);
			if (exitVal == 0) {
				exitVal = convertM4aToMp3(tempFile, outFile);
				LOGGER.info("convertM4aToMp3 exitVal = " + exitVal);
				if (exitVal != 0) {
					outFile.delete();
				}
			}
			tempFile.delete();
			return exitVal;
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return -1;
	}

    public String getVoice(String language, String locale, String gender) {
// PRODVSDEV
/*
		String voice = "Victoria";
		if (gender.equals("m")) { 
			voice = "Bruce";
		}
*/
// PRODVSDEV

		String voice = "Samantha";
		if (language.equals("en")) {
			if (locale.equals("GB")) {
				if (gender.equals("m")) { 
				    voice = "Daniel";
				} else {
					voice = "Serena";
				}
			} else {
				if (gender.equals("m")) { 
				    voice = "Tom";
				} else {
					voice = "Samantha";
				}
			}
		} else if (language.equals("es")) {
			if (gender.equals("m")) { 
				voice = "Diego";
			} else {
				voice = "Paulina";
			}
		} else if (language.equals("fr")) {
			if (gender.equals("m")) { 
				voice = "Thomas";
			} else {
				voice = "Audrey";
			}
		} else if (language.equals("de")) {
			if (gender.equals("m")) { 
				voice = "Yannick";
			} else {
				voice = "Anna";
			}
		} else if (language.equals("ja")) {
			voice = "Kyoko";
		} else if (language.equals("ko")) {
			voice = "Narae";
		} else if (language.equals("zh")) {
			voice = "Ting-Ting";
		}
		return voice;
	}

	public int createM4a(File outFile, String voice, String message)
	{
		try
		{
			Runtime rt = Runtime.getRuntime();
            String cmd = "say -v " + voice + " -o " + outFile.getAbsolutePath() + " " + message;
            LOGGER.info("createM4a cmd = " + cmd);
            Process proc = rt.exec(cmd);
			InputStream stderr = proc.getErrorStream();
			InputStreamReader isr = new InputStreamReader(stderr);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ( (line = br.readLine()) != null){
				LOGGER.info(line);
			}
			return proc.waitFor();
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return -1;
	}

	public int convertM4aToMp3(File inFile, File outFile)
	{
		try
		{
			Runtime rt = Runtime.getRuntime();
			// PRODVSDEV
            String cmd = "/Library/WebServer/Documents/public_html/audio/ffmpeg -i " + inFile.getAbsolutePath() + " " + outFile.getAbsolutePath();
			LOGGER.info("convertM4aToMp3 cmd = " + cmd);
            Process proc = rt.exec(cmd);
			InputStream stderr = proc.getErrorStream();
			InputStreamReader isr = new InputStreamReader(stderr);
			BufferedReader br = new BufferedReader(isr);
			String line = null;
			while ( (line = br.readLine()) != null){
				LOGGER.info(line);
			}
			return proc.waitFor();
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return -1;
	}
}
