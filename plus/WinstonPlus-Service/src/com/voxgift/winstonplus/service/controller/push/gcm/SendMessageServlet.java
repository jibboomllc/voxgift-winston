/*
 * Copyright 2012 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.voxgift.winstonplus.service.controller.push.gcm;

import com.google.android.gcm.server.Constants;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.voxgift.winstonplus.service.controller.BaseServlet;
import com.voxgift.winstonplus.service.model.PushDeviceRepository;
import com.voxgift.winstonplus.service.model.PushDeviceRepositoryJDBCImpl;
import com.voxgift.winstonplus.service.model.PushServiceType;

/**
 * Servlet that adds a new message to all registered devices for a circle.
 */
@SuppressWarnings("serial")
public class SendMessageServlet extends BaseServlet {
	private final static Logger LOGGER = Logger.getLogger(SendMessageServlet.class.getName()); 

	private static final int MAX_NOTIFICATION_TEXT_SIZE = 2048;

	private static final int MULTICAST_SIZE = 1000;

	private Sender sender;

	private static final Executor threadPool = Executors.newFixedThreadPool(5);

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		sender = newSender(config);
	}

	/**
	 * Creates the {@link Sender} based on the servlet settings.
	 */
	protected Sender newSender(ServletConfig config) {
		String key = (String) config.getServletContext()
				.getAttribute(ApiKeyInitializer.ATTRIBUTE_ACCESS_KEY);
		return new Sender(key);
	}

	/**
	 * Processes the request to add a new message.
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException, ServletException {
		send(getParameter(req, "circleId"), getParameter(req, "userId"), getParameter(req, "messageId"));
	}


	public void send(String circleId, String userId, String messageId) {
		PushDeviceRepository repo = new PushDeviceRepositoryJDBCImpl(PushServiceType.GCM);
		List<String> devices = repo.getCirclePushDevices(circleId, userId);
		if (devices == null || devices.isEmpty()) {
			LOGGER.info("Message ignored as there are no new GCM devices registered for circle=" + circleId);
		} else {
			String notificationText = repo.getNotificationText(messageId, MAX_NOTIFICATION_TEXT_SIZE);
			LOGGER.info("GCM Message [" + notificationText + "] sending to " + devices.size() + " devices!");
			send(devices, notificationText);
		}
	}

	public void send(List<String> devices, String notificationText) {
		// send a multicast message using JSON
		// must split in chunks of 1000 devices (GCM limit)
		int total = devices.size();
		List<String> partialDevices = new ArrayList<String>(total);
		int counter = 0;
		int tasks = 0;
		for (String device : devices) {
			counter++;
			partialDevices.add(device);
			int partialSize = partialDevices.size();
			if (partialSize == MULTICAST_SIZE || counter == total) {
				asyncSend(partialDevices, notificationText);
				partialDevices.clear();
				tasks++;
			}
		}
		String status = "Asynchronously sending " + tasks + " multicast messages to " +
				total + " devices";
		logger.fine(status);
	}

	private void asyncSend(List<String> partialDevices, final String notificationText) {
		// make a copy
		final List<String> devices = new ArrayList<String>(partialDevices);
		threadPool.execute(new Runnable() {

			public void run() {
				Message message = new Message.Builder()
					.delayWhileIdle(true)
					.addData("text", notificationText)
					.build();
				MulticastResult multicastResult;
				try {
					multicastResult = sender.send(message, devices, 5);
				} catch (IOException e) {
					logger.log(Level.SEVERE, "Error posting messages", e);
					return;
				}
				List<Result> results = multicastResult.getResults();
				// analyze the results
				for (int i = 0; i < devices.size(); i++) {
					String regId = devices.get(i);
					Result result = results.get(i);
					String messageId = result.getMessageId();
					if (messageId != null) {
						logger.fine("Succesfully sent message to device: " + regId +
								"; messageId = " + messageId);
						String canonicalRegId = result.getCanonicalRegistrationId();
						if (canonicalRegId != null) {
							// same device has more than on registration id: update it
							logger.info("canonicalRegId " + canonicalRegId);
							PushDeviceRepository repo = new PushDeviceRepositoryJDBCImpl(PushServiceType.GCM);
							repo.updateRegistration(regId, canonicalRegId);
						}
					} else {
						String error = result.getErrorCodeName();
						if (error.equals(Constants.ERROR_NOT_REGISTERED)) {
							// application has been removed from device - unregister it
							logger.info("Unregistered device: " + regId);
							PushDeviceRepository repo = new PushDeviceRepositoryJDBCImpl(PushServiceType.GCM);
							repo.unregister(regId);
						} else {
							logger.severe("Error sending message to " + regId + ": " + error);
						}
					}
				}
			}});
	}

}
