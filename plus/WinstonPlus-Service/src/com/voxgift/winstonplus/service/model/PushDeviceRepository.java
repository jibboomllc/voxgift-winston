package com.voxgift.winstonplus.service.model;

import java.util.List;

public interface PushDeviceRepository {
	public Boolean register(String regId);
	public Boolean unregister(String regId);
	public Boolean updateRegistration(String oldId, String newId);
	public List<String> getDevices();
	public List<String> getCirclePushDevices(String circleId, String userId);
	public String getNotificationText(String messageId, int maxBytes);
}
