package com.voxgift.winstonplus.service.model;

public class PushServiceType {
	public final static int UNDEFINED = -1;
	public final static int GCM = 0;
	public final static int APN = 1;
}
