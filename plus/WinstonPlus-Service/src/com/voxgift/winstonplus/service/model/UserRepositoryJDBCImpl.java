package com.voxgift.winstonplus.service.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

public class UserRepositoryJDBCImpl extends JDBCImpl implements UserRepository {

	private final static Logger LOGGER = Logger.getLogger(UserRepositoryJDBCImpl.class.getName()); 

	@Override
	public Confirmation getConfirmation(final String id) {
		return withDB(new JDBCRunner<Confirmation>(){
			@Override
			public Confirmation run(Connection connection) throws Exception {
				Confirmation result = null;
				PreparedStatement statement = null;
				try {
					final String query = 
							"SELECT *, NOW() > expire_ts AS expired"
									+ " FROM confirmation AS c"  
									+ " WHERE c.uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, id);

					ResultSet rs = statement.executeQuery();

					if (rs != null) {
						if (rs.next()) {
							result = new Confirmation(
									rs.getString("uuid")
									, rs.getString("user_uuid")
									, rs.getString("circle_uuid")
									, rs.getString("email")
									, rs.getBoolean("expired") //, rs.getInt("expired") > 0 ? true : false
									);
						}
						rs.close();
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		

	}

	@Override
	public Boolean confirmEmail(final String userId, final String email) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				boolean result = false;
				PreparedStatement statement = null;
				try {
					final String query = 
							"UPDATE user_emails SET confirmed_ts = NOW() WHERE "
									+ " email = ?"
									+ " AND user_uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setString(1, email);
					statement.setString(2, userId);

					//LOGGER.info(query + " " + email + " " + userId);
					if (statement.executeUpdate() == 1) {
						result = true;
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Boolean changeCircleStatus(final String userId, final String circleId, final int statusId) {
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				boolean result = false;
				PreparedStatement statement = null;
				try {
					final String query = 
							"UPDATE circle_users AS cu SET"
									+ "   cu.status_id = ?"
									+ " WHERE"
									+ "   cu.circle_uuid = ?"
									+ " AND"
									+ "   cu.user_uuid = ?";
					statement = connection.prepareStatement(query);
					statement.setInt(1, statusId);
					statement.setString(2, circleId);
					statement.setString(3, userId);

					//LOGGER.info(query + " " + circleId + " " + userId);
					if (statement.executeUpdate() == 1) {
						result = true;
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}
}

