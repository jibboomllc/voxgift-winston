package com.voxgift.winstonplus.service.model;

public class Confirmation implements Cloneable {
	private String id;
	private String userId;
	private String circleId;
	private String email;
	private Boolean expired;
	
	public Confirmation() {
		
	}
	
	public Confirmation(String id, String userId, String circleId, String email, Boolean expired) {
		this.id = id;
		this.userId = userId;
		this.circleId = circleId;
		this.email = email;
		this.expired = expired;
	}

    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCircleId() {
		return circleId;
	}
	public void setCircleId(String circleId) {
		this.circleId = circleId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getExpired() {
		return expired;
	}
	public void setExpired(Boolean expired) {
		this.expired = expired;
	}

	public Confirmation cloneMe()  {
		try {
			return (Confirmation) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;  
		}
	}
	
	@Override
	public String toString() {
		return "Confirmation [id=" + id + ", userId=" + userId + ", circleId=" + circleId
				+ ", email=" + email + ", expired=" + expired
				+ "]";
	}

}
