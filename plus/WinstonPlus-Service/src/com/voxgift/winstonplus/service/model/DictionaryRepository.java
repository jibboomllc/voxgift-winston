package com.voxgift.winstonplus.service.model;

public interface DictionaryRepository {
	String lookupTranslation(String id, String language, String locale);
	String lookupAdhocEntry(String id, String language, String locale);
	String lookupFavorite(String id, String language, String locale);
}
