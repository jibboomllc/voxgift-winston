package com.voxgift.winstonplus.service.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class PushDeviceRepositoryJDBCImpl extends JDBCImpl implements PushDeviceRepository {

	private final static Logger LOGGER = Logger.getLogger(PushDeviceRepositoryJDBCImpl.class.getName()); 

	private int pushServiceType;

	public PushDeviceRepositoryJDBCImpl(int pushServiceType) {
		this.pushServiceType = pushServiceType;
	}

	@Override
	public Boolean register(final String regId) {
		LOGGER.info("Registering type = " + pushServiceType +", regId = "+ regId);
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				boolean result = false;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					final String query = 
							"SELECT uuid, UNIX_TIMESTAMP(`delete_ts`) AS dts FROM push_device"
									+ " WHERE uuid = ?"
									+ " AND push_service_type_id = ?"
									;
					statement = connection.prepareStatement(query);
					statement.setString(1, regId);
					statement.setInt(2, pushServiceType);

					LOGGER.info("register: Checking if already registered: " + query + " " + regId + " " + pushServiceType);
					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						if (rs.getInt("dts") == 0) {
							result = true;
						} else {
							// "undelete" the device
							final String query1 = 
									"UPDATE push_device SET delete_ts = 0 "
											+ " WHERE uuid = ?"
											+ " AND push_service_type_id = ?"
											;
							statement = connection.prepareStatement(query1);
							statement.setString(1, regId);
							statement.setInt(2, pushServiceType);

				            LOGGER.info("register: " + query1 + " " + regId);
							if (statement.executeUpdate() == 1) {
								result = true;
							} else {
								LOGGER.warning("register: Problem updating (undeleting) = " + " " + regId + ", type = " + pushServiceType);
							}

						}
					} else {

						final String query2 = 
								"INSERT INTO push_device (uuid, push_service_type_id) "
										+ " VALUES (?, ?)"
										;
						statement.close();
						statement = connection.prepareStatement(query2);
						statement.setString(1, regId);
						statement.setInt(2, pushServiceType);

						LOGGER.info("register: " + query2 + " " + regId + " " + pushServiceType);
						if (statement.executeUpdate() == 1) {
							result = true;
						} else {
							LOGGER.warning("register: Problem inserting = " + regId + ", type = " + pushServiceType);
						}
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Boolean unregister(final String regId) {
		LOGGER.info("Unregistering " + regId);
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				boolean result = false;
				PreparedStatement statement = null;
				try {
					final String query = 
							"UPDATE push_device SET delete_ts = NOW() WHERE "
									+ " uuid = ?"
									+ " AND push_service_type_id = ?"
									;
					statement = connection.prepareStatement(query);
					statement.setString(1, regId);
					statement.setInt(2, pushServiceType);

					LOGGER.info("unregister: " + query + " " + regId);
					if (statement.executeUpdate() == 1) {
						result = true;
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public Boolean updateRegistration(final String oldId, final String newId) {
		LOGGER.info("Updating " + oldId + " to " + newId);
		return withDB(new JDBCRunner<Boolean>(){
			@Override
			public Boolean run(Connection connection) throws Exception {
				boolean result = false;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					// first check if newId is already registered
					// delete oldId registration if newId registration exists
					// update oldId to newId if newId registration does not exist
					final String query = 
							"SELECT uuid, UNIX_TIMESTAMP(`delete_ts`) AS dts FROM push_device"
									+ " WHERE uuid = ?"
									+ " AND push_service_type_id = ?"
									;
					statement = connection.prepareStatement(query);
					statement.setString(1, newId);
					statement.setInt(2, pushServiceType);

					LOGGER.info("updateRegistration: Checking if newId already registered: " + query + " " + newId + " " + pushServiceType);
					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						final String query1 = 
								"UPDATE push_device SET delete_ts = NOW() "
										+ " WHERE uuid = ?"
										+ " AND push_service_type_id = ?"
										;
						statement = connection.prepareStatement(query1);
						statement.setString(1, oldId);
						statement.setInt(2, pushServiceType);

						LOGGER.info("updateRegistration: deleting oldId registration since newId registration exists" + query1 + " " + oldId);
						if (statement.executeUpdate() == 1) {
							result = true;
						} else {
							LOGGER.warning("updateRegistration: Problem updating (deleting) = " + oldId + ", type = " + pushServiceType);
						}
					} else {
						final String query2 = 
								"UPDATE push_device SET uuid = ? WHERE "
										+ " uuid = ?"
										+ " AND push_service_type_id = ?"
										;
						statement = connection.prepareStatement(query2);
						statement.setString(1, newId);
						statement.setString(2, oldId);
						statement.setInt(3, pushServiceType);

						LOGGER.info("updateRegistration: updating oldId to newId since newId registration does not exist" + query2 + " " + newId + " " + oldId);
						if (statement.executeUpdate() == 1) {
							result = true;
						} else {
							LOGGER.warning("updateRegistration: Problem updating = " + oldId + ", type = " + pushServiceType);
						}
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public List<String> getDevices() {
		return withDB(new JDBCRunner<List<String>>(){
			@Override
			public List<String> run(Connection connection) throws Exception {
				List<String> result = new ArrayList<String>();				
				ResultSet rs = null;
				PreparedStatement statement = null;
				try {
					final String query = "SELECT uuid FROM push_device"
							+ " WHERE UNIX_TIMESTAMP(`delete_ts`) = 0"
							+ " AND push_service_type_id = ?"
							;
					statement = connection.prepareStatement(query);
					statement.setInt(1, pushServiceType);
					rs = statement.executeQuery();
					if (rs != null) {
						while (rs.next()) {
							result.add(rs.getString("uuid"));
						}
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public List<String> getCirclePushDevices(final String circleId, final String userId) {
		return withDB(new JDBCRunner<List<String>>(){
			@Override
			public List<String> run(Connection connection) throws Exception {
				List<String> result = new ArrayList<String>();				
				ResultSet rs = null;
				PreparedStatement statement = null;
				try {
					final String query = "SELECT uuid FROM push_device AS pd"
							+ " LEFT JOIN circle_users AS cu"
							+ " ON cu.user_uuid = pd.user_uuid"
							+ " WHERE UNIX_TIMESTAMP(`pd`.`delete_ts`) = 0"
							+ " AND pd.push_service_type_id = ?"
							+ " AND cu.circle_uuid = ?"
							+ " AND cu.user_uuid <> ?"
							+ " AND cu.status_id = " + CircleUserStatus.ACCEPTED
							;
					statement = connection.prepareStatement(query);
					statement.setInt(1, pushServiceType);
					statement.setString(2, circleId);
					statement.setString(3, userId);

					LOGGER.info("getCirclePushDevices: " + query + " " + circleId + " " + userId);
					rs = statement.executeQuery();
					if (rs != null) {
						while (rs.next()) {
							result.add(rs.getString("uuid"));
						}
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

	@Override
	public String getNotificationText(final String messageId, final int maxBytes) {
		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = new String();				
				ResultSet rs = null;
				PreparedStatement statement = null;
				try {
					final String query = 
							"SELECT SUBSTRING(m.message, 1, " + maxBytes + ") AS msg"
									+ ", u.fname, u.lname, u.primary_email" 
									+ " FROM message AS m "
									+ " LEFT JOIN user AS u ON m.user_uuid = u.uuid"
									+ " WHERE m.uuid = ?"
									;
					statement = connection.prepareStatement(query);
					statement.setString(1, messageId);

					rs = statement.executeQuery();
					if (rs != null && rs.next()) {
						String message = rs.getString("msg");
						String fName = rs.getString("u.fname");
						String lName = rs.getString("u.lname");
						String email = rs.getString("u.primary_email");

						if(fName != null && !fName.trim().isEmpty()) {
							result = result.concat(fName.trim());

							if(lName != null && !lName.trim().isEmpty()) {
								result = result.concat(" " + lName.trim());
							}
						} else if (lName != null && !lName.trim().isEmpty()) {
							result = result.concat(lName.trim());
						} else if (email != null && !email.trim().isEmpty()) {
							result = result.concat(email.trim());
						}

						if (message != null && !message.isEmpty()) {
							result = result.concat(": " + message);
						}

						if (result.length() > maxBytes) {
							result = result.substring(0, maxBytes - 3) + "..";
						} else if (result.isEmpty()) {
							result = null;
							LOGGER.warning("No useful data returned for: " + query + " " + messageId);
						}
					} else {
						LOGGER.warning("No records returned for: " + query + " " + messageId);
					}
				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});		
	}

}
