package com.voxgift.winstonplus.service.model;
/*
 * TODO: Validate logic vs. http://blog.christopherschultz.net/index.php/2009/03/16/properly-handling-pooled-jdbc-connections/
 */
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class JDBCImpl {
	private final static Logger LOGGER = Logger.getLogger(JDBCImpl.class.getName()); 

	private DataSource dataSource;

	protected DataSource getDataSource() {

		if (dataSource == null) {
			InitialContext ic;
			try {
				ic = new InitialContext();
				//LOGGER.info("ic [" + ic.getEnvironment() +"]");
				dataSource = (DataSource)ic.lookup("java:comp/env/jdbc/CircleJDBCResource");
			} catch (NamingException e) {
				e.printStackTrace();
			}
		}
		//LOGGER.info("dataSource [" + dataSource +"]");
		return dataSource;
	}

	static interface JDBCRunner <T> {
		T run(Connection connection) throws Exception;
	}

	protected  <T> T withDB(JDBCRunner<T> runner) {
		Connection connection = null;
		try {
			connection = getDataSource().getConnection();
			//LOGGER.info("connection [" + connection +"]");
			if (connection == null) {
				LOGGER.warning("Error getting connection for data source: " + getDataSource() + ". Check appropriate conf xml files.");
			}

			boolean auto = connection.getAutoCommit();
			connection.setAutoCommit(false);
			T result = runner.run(connection);
			connection.commit();
			connection.setAutoCommit(auto); //set it to what it was previously.
			return result;
		} catch (Exception ex) {
			ex.printStackTrace();
			if (connection!=null) {
				try {
					connection.rollback();
				} catch (SQLException e) {
					LOGGER.info("SQLException on connection [" + connection +"] rollback: " + e);
				}
			}
			LOGGER.info("Exception on connection [" + connection +"]: " + ex);
		} finally {
			if (connection!=null) {
				try {
					connection.close();
				} catch (SQLException e) {
					LOGGER.info("SQLException on connection [" + connection +"] close: " + e);
				}
			}
		}
		return null;
	}

}
