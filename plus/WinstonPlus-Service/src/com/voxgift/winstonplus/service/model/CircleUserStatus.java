package com.voxgift.winstonplus.service.model;

public class CircleUserStatus {
	public final static int INVITED = 0;
	public final static int ACCEPTED = 1;
	public final static int REJECTED = 2;
	public final static int UNSUBSCRIBED = 3;
}
