package com.voxgift.winstonplus.service.model;

import java.util.Date;
import java.util.UUID;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "user")
public class User implements Cloneable {
	private String id;
	private String firstName;
	private String lastName;
	private String primaryEmail;
	private String password;
	private Date createTs;

	public User (){

	}

	public User (String id, String firstName, String lastName, String primaryEmail, String password, Date createTs){
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.primaryEmail = primaryEmail;
		this.password = password;
		this.createTs = createTs;
	}

	public User (String firstName, String lastName, String primaryEmail, String password){
		this.id = String.valueOf(UUID.randomUUID());
		this.firstName = firstName;
		this.lastName = lastName;
		this.primaryEmail = primaryEmail;
		this.password = password;
	}
	@XmlElement(name="id", required=true)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	@XmlElement(name="firstName", required=true)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPrimaryEmail() {
		return primaryEmail;
	}
	public void setPrimaryEmail(String primaryEmail) {
		this.primaryEmail = primaryEmail;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreateTs() {
		return createTs;
	}
	public void setCreateTs(Date createTs) {
		this.createTs = createTs;
	}

	public User cloneMe()  {
		try {
			return (User) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;  
		}
	}

	@Override
	public String toString() {
		return "User [firstName=" + firstName + ", lastName=" + lastName + ", primaryEmail=" + primaryEmail
				+ ", password=" + password + ", createTs=" + createTs + ", id=" + id
				+ "]";
	}

}
