package com.voxgift.winstonplus.service.model;

@SuppressWarnings("serial")
public class UserStoreException extends Exception {

	public UserStoreException(Exception ex) {
		super(ex);
	}

	public UserStoreException(String message) {
		super(message);
	}

	public UserStoreException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
