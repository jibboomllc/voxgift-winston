package com.voxgift.winstonplus.service.model;

public interface UserRepository {

	public Confirmation getConfirmation(final String id);

	public Boolean confirmEmail(final String userID, final String email);

	public Boolean changeCircleStatus(final String userID, final String circleId, int status);
}
