package com.voxgift.winstonplus.service.model;

public class PushNotificationDevice implements Cloneable {
	private String id;
	private String userId;

	public PushNotificationDevice() {

	}

	public PushNotificationDevice(String id, String userId) {
		this.id = id;
		this.userId = userId;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public PushNotificationDevice cloneMe()  {
		try {
			return (PushNotificationDevice) super.clone();
		} catch (CloneNotSupportedException e) {
			return null;  
		}
	}

	@Override
	public String toString() {
		return "PushNotificationDevice [id=" + id + ", userId=" + userId
				+ "]";
	}

}
