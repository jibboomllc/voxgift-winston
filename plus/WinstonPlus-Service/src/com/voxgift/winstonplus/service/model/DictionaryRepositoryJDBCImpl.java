package com.voxgift.winstonplus.service.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

public class DictionaryRepositoryJDBCImpl  extends JDBCImpl implements DictionaryRepository {
	private final static Logger LOGGER = Logger.getLogger(DictionaryRepositoryJDBCImpl.class.getName()); 

	@Override
	public String lookupTranslation(final String id, final String language, final String locale) {

		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {
					statement = connection.prepareStatement("SELECT translation FROM dictionary_entry_translation WHERE entry_uuid = ? AND language = ? AND locale = ?");
					statement.setString(1, id);
					statement.setString(2, language);
					statement.setString(3, locale);
					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						result = rs.getString("translation");
					}

				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public String lookupAdhocEntry(final String id, final String language, final String locale) {

		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {

					statement = connection.prepareStatement("SELECT translation FROM adhoc_dictionary_entry WHERE uuid = ? AND language = ? AND locale = ?");
					statement.setString(1, id);
					statement.setString(2, language);
					statement.setString(3, locale);
					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						result = rs.getString("translation");
					}

				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}

	@Override
	public String lookupFavorite(final String id, final String language, final String locale) {

		return withDB(new JDBCRunner<String>(){
			@Override
			public String run(Connection connection) throws Exception {
				String result = null;
				PreparedStatement statement = null;
				ResultSet rs = null;
				try {

					statement = connection.prepareStatement("SELECT translation FROM favorites_dictionary_entry WHERE uuid = ? AND language = ? AND locale = ?");
					statement.setString(1, id);
					statement.setString(2, language);
					statement.setString(3, locale);
					rs = statement.executeQuery();

					if (rs != null && rs.next()) {
						result = rs.getString("translation");
					}

				} catch (Exception ex) {
					LOGGER.warning("Exception: " + ex);
					ex.printStackTrace();
				} finally {
					if (rs != null) {
						rs.close();
					}
					if (statement != null) {
						statement.close();
					}
				}
				return result;
			}});
	}
}
