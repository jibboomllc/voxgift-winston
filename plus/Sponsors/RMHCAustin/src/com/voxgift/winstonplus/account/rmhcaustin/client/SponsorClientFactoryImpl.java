package com.voxgift.winstonplus.account.rmhcaustin.client;

import com.voxgift.winstonplus.account.client.ClientFactoryImpl;
import com.voxgift.winstonplus.account.client.ui.WelcomeView;
import com.voxgift.winstonplus.account.rmhcaustin.client.ui.WelcomeViewImpl;

public class SponsorClientFactoryImpl extends ClientFactoryImpl
{	
	@Override
	public String getSponsorId()
	{
		return "4322d500-9be1-4aa6-878a-b0f9f0ea40f1";
	}

	protected static final WelcomeView welcomeView = new WelcomeViewImpl();

	@Override
	public WelcomeView getWelcomeView()
	{
		return welcomeView;
	}

}
