#!/bin/bash
SRC_DIR="./repo"
CATALINA_HOME="/Library/Tomcat/Home"
DOC_ROOT="/Library/WebServer/Documents/public_html"

$CATALINA_HOME/bin/shutdown.sh
echo "Sleeping..31 sec"
sleep 31

mv $SRC_DIR/winstonplus-account-1.0.0.zip $SRC_DIR/account.zip
mv $SRC_DIR/winstonplus-account-1.0.0.war $SRC_DIR/account.war
mv $SRC_DIR/rmhcaustin-1.0.0.zip $SRC_DIR/rmhcaustin.zip
mv $SRC_DIR/rmhcaustin-1.0.0.war $SRC_DIR/rmhcaustin.war

shopt -s nullglob

files="$SRC_DIR/*.war"
regex="$SRC_DIR/(.*).war"
for fp in $files
do
  if [[ $fp =~ $regex ]]; then
    f=${BASH_REMATCH[1]}
    echo "Processing $f.war"
    rm -rf $CATALINA_HOME/webapps/$f
    cp -rf $SRC_DIR/$f.war $CATALINA_HOME/webapps/$f.war
  fi
done

files="$SRC_DIR/*.zip"
regex="$SRC_DIR/(.*).zip"
for fp in $files
do
  if [[ $fp =~ $regex ]]; then
    f=${BASH_REMATCH[1]}
    echo "Processing $f.zip"
    rm -rf $DOC_ROOT/c/$f
    unzip  $SRC_DIR/$f.zip -d $DOC_ROOT/c/$f
  fi
done

mv $SRC_DIR/account.zip $SRC_DIR/winstonplus-account-1.0.0.zip
mv $SRC_DIR/account.war $SRC_DIR/winstonplus-account-1.0.0.war
mv $SRC_DIR/rmhcaustin.zip $SRC_DIR/rmhcaustin-1.0.0.zip
mv $SRC_DIR/rmhcaustin.war $SRC_DIR/rmhcaustin-1.0.0.war

$CATALINA_HOME/bin/startup.sh
