-- Use the following link for uuid creation:
-- http://www.famkruithof.net/uuid/uuidgen

USE `voxcircle`;

DROP TABLE IF EXISTS dictionary;
DROP TABLE IF EXISTS dictionary_entry_translation;
DROP TABLE IF EXISTS dictionary_context;
DROP TABLE IF EXISTS dictionary_entry;

-- -----------------------------------------------------------------------------
-- data tables - dictionary

CREATE TABLE IF NOT EXISTS dictionary_context (
  uuid VARCHAR(36) NOT NULL PRIMARY KEY
  , name VARCHAR(64)
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO dictionary_context (uuid, name) VALUES ('c148d0d2-2936-4a30-a4de-e6f66ea92782', 'Favorites');
INSERT INTO dictionary_context (uuid, name) VALUES ('0959dc0c-5ebb-4b96-ac12-8c612176b675', 'General');
INSERT INTO dictionary_context (uuid, name) VALUES ('92e8eee5-10f1-447e-a7df-716ad16bebf2', 'Medical - Patient');
INSERT INTO dictionary_context (uuid, name) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', 'Medical - Provider');
INSERT INTO dictionary_context (uuid, name) VALUES ('2dcd25ad-334c-457d-b507-7a4ff94ab033', 'Dentist - Patient');
INSERT INTO dictionary_context (uuid, name) VALUES ('59e23310-de97-4fc3-a2b2-ab16f30eb6d2', 'Dentist - Provider');

CREATE TABLE IF NOT EXISTS dictionary_entry (
  uuid VARCHAR(36) NOT NULL PRIMARY KEY
  , description VARCHAR(1024)
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO dictionary_entry (uuid, description) VALUES ('4d3a214a-138a-4670-ab40-2b5207d267d1', 'Yes');
INSERT INTO dictionary_entry (uuid, description) VALUES ('3fef20bc-cf64-4429-a442-da209192657c', 'No');
INSERT INTO dictionary_entry (uuid, description) VALUES ('83d26244-a138-4243-be60-e605198ed58d', 'Hello');
INSERT INTO dictionary_entry (uuid, description) VALUES ('dfc99696-a8f9-4b30-b032-818bc936f84e', 'Goodbye');

INSERT INTO dictionary_entry (uuid, description) VALUES ('ee0bd950-4e96-42b1-9378-5c47ba6236ca', 'Please');
INSERT INTO dictionary_entry (uuid, description) VALUES ('4c95451d-8df9-4928-9667-1452242b6edd', 'Thank you');
INSERT INTO dictionary_entry (uuid, description) VALUES ('518580e0-e561-482e-b995-e61da953d1ec', 'Where is the bathroom?');
INSERT INTO dictionary_entry (uuid, description) VALUES ('822ab852-d89c-4ffa-8ebb-e4082190c89f', 'Where can I connect to the internet?');
INSERT INTO dictionary_entry (uuid, description) VALUES ('751316be-537c-4d69-8f0d-d8670dfa67f8', 'Over there');
INSERT INTO dictionary_entry (uuid, description) VALUES ('374141ef-ed8c-4348-93cb-bcb15d345368', "I don't know");

INSERT INTO dictionary_entry (uuid, description) VALUES ('fa9ea43d-7ef7-4d10-a66a-426652032839', 'I am hot (temperature-wise)');
INSERT INTO dictionary_entry (uuid, description) VALUES ('27166da7-b96b-4ed1-a704-b454d8b44687', 'I am cold (temperature-wise)');
INSERT INTO dictionary_entry (uuid, description) VALUES ('e7427bbc-5679-4fb0-8a50-f2a98e6c5f50', 'I am hungry');
INSERT INTO dictionary_entry (uuid, description) VALUES ('59afe82b-aa1c-4231-a2b8-70577b704714', 'I am thirsty');
INSERT INTO dictionary_entry (uuid, description) VALUES ('c607580f-0762-4948-81be-ffed2264aa6e', 'I need medicine');
INSERT INTO dictionary_entry (uuid, description) VALUES ('56aa1e7c-6972-4f0d-9ea0-8ae9ec82d771', 'I need to use the bathroom');
INSERT INTO dictionary_entry (uuid, description) VALUES ('ac434b48-8741-43e0-9b8c-34aa47bbcbea', 'I need to be suctioned');
INSERT INTO dictionary_entry (uuid, description) VALUES ('d690eac9-651b-44ad-91b9-3c5c0c9d069c', 'I need the remote control');
INSERT INTO dictionary_entry (uuid, description) VALUES ('ec3fb599-c58e-428f-9bd8-b5a5f89f55dc', 'I need a rinse');
INSERT INTO dictionary_entry (uuid, description) VALUES ('1a5bdd05-b8e8-4635-a566-1a9a4c691cad', 'My jaw hurts');
INSERT INTO dictionary_entry (uuid, description) VALUES ('a8e5d592-7d18-492f-b985-e1fbd72c225d', 'How much longer?');
INSERT INTO dictionary_entry (uuid, description) VALUES ('49f272c9-889d-4123-88a6-abf9ca451889', 'My lips are dry');
INSERT INTO dictionary_entry (uuid, description) VALUES ('e3978634-b97d-4bef-96e6-c3112bf0f42b', 'I can feel pain');
INSERT INTO dictionary_entry (uuid, description) VALUES ('a8e7ca3d-4697-4026-b47b-d9ac22c4603c', 'That is uncomfortable');
INSERT INTO dictionary_entry (uuid, description) VALUES ('1f2f5e89-feb1-4a53-b20e-dd63cb4d74be', 'That feels good');
INSERT INTO dictionary_entry (uuid, description) VALUES ('b0b7418c-3eb7-44fb-a60b-dd66bb5ef279', 'That feels bad');
INSERT INTO dictionary_entry (uuid, description) VALUES ('184a43e9-8e46-40f7-8bd9-af5b59237df2', 'My throat hurts');
INSERT INTO dictionary_entry (uuid, description) VALUES ('db2464fd-eb0b-4e58-8a36-4f0975bba390', 'My head hurts');
INSERT INTO dictionary_entry (uuid, description) VALUES ('ff0349d3-5772-4edc-b2bc-e6bc0811c918', 'My chest hurts');
INSERT INTO dictionary_entry (uuid, description) VALUES ('a8b47c7b-cd93-4355-9e77-85f57c90cfc6', 'This hurts');
INSERT INTO dictionary_entry (uuid, description) VALUES ('b6a910c3-49b0-4269-95ca-541a049eae9e', 'What seems to be the problem?');
INSERT INTO dictionary_entry (uuid, description) VALUES ('d1bc447c-7f63-436b-b2c8-6a42418b7ec7', 'Are you in pain?');
INSERT INTO dictionary_entry (uuid, description) VALUES ('af5610d1-5479-4ea2-8072-5c94cc1e0d95', 'Does it hurt when I do this?');
INSERT INTO dictionary_entry (uuid, description) VALUES ('78395a6e-6bf0-4a1c-9cbd-d146638f78e1', 'I am going to listen to your lungs');
INSERT INTO dictionary_entry (uuid, description) VALUES ('fa0cd308-74bb-4f80-a032-3048a57d6d8a', 'Breathe deeply');
INSERT INTO dictionary_entry (uuid, description) VALUES ('64e032ea-9f73-4903-aa02-0f8b1adfc291', 'I am going to look in your throat');
INSERT INTO dictionary_entry (uuid, description) VALUES ('ec304f64-acdb-4971-8b33-7cd5573aa0b6', 'Open your mouth and stick out your tongue');
INSERT INTO dictionary_entry (uuid, description) VALUES ('b1ff3649-9723-4ac8-a934-f3285a9c924b', 'I am going to take your temperature');
INSERT INTO dictionary_entry (uuid, description) VALUES ('ae989b9c-5fcb-46e7-9421-b301b25fd561', 'I am going to take your blood pressure');

CREATE TABLE IF NOT EXISTS dictionary_entry_translation (
  entry_uuid VARCHAR(36) NOT NULL
  , language VARCHAR(2) NOT NULL
  , locale VARCHAR(2) NOT NULL
  , translation VARCHAR(512)
  , usage_count INT DEFAULT 0 
  , create_ts TIMESTAMP DEFAULT NOW()
  , last_usage_ts TIMESTAMP
  , delete_ts TIMESTAMP
  , PRIMARY KEY (entry_uuid, language, locale)
  , FOREIGN KEY (entry_uuid) REFERENCES dictionary_entry(uuid) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4d3a214a-138a-4670-ab40-2b5207d267d1', 'en', 'US', 'Yes');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('3fef20bc-cf64-4429-a442-da209192657c', 'en', 'US', 'No');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('83d26244-a138-4243-be60-e605198ed58d', 'en', 'US', 'Hello');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('dfc99696-a8f9-4b30-b032-818bc936f84e', 'en', 'US', 'Goodbye');

INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ee0bd950-4e96-42b1-9378-5c47ba6236ca', 'en', 'US', 'Please');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4c95451d-8df9-4928-9667-1452242b6edd', 'en', 'US', 'Thank you');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('518580e0-e561-482e-b995-e61da953d1ec', 'en', 'US', 'Where is the bathroom?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('822ab852-d89c-4ffa-8ebb-e4082190c89f', 'en', 'US', 'Where can I connect to the internet?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('751316be-537c-4d69-8f0d-d8670dfa67f8', 'en', 'US', 'Over there');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('374141ef-ed8c-4348-93cb-bcb15d345368', 'en', 'US', "I don\'t know");
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('fa9ea43d-7ef7-4d10-a66a-426652032839', 'en', 'US', 'I am hot');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('27166da7-b96b-4ed1-a704-b454d8b44687', 'en', 'US', 'I am cold');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('e7427bbc-5679-4fb0-8a50-f2a98e6c5f50', 'en', 'US', 'I am hungry');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('59afe82b-aa1c-4231-a2b8-70577b704714', 'en', 'US', 'I am thirsty');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('c607580f-0762-4948-81be-ffed2264aa6e', 'en', 'US', 'I need medicine');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('56aa1e7c-6972-4f0d-9ea0-8ae9ec82d771', 'en', 'US', 'I need to use the bathroom');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ac434b48-8741-43e0-9b8c-34aa47bbcbea', 'en', 'US', 'Can you please suction me');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('d690eac9-651b-44ad-91b9-3c5c0c9d069c', 'en', 'US', 'Can you please hand me the remote control');

INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ec3fb599-c58e-428f-9bd8-b5a5f89f55dc', 'en', 'US', 'I need a rinse');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('1a5bdd05-b8e8-4635-a566-1a9a4c691cad', 'en', 'US', 'My jaw hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('a8e5d592-7d18-492f-b985-e1fbd72c225d', 'en', 'US', 'How much longer?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('49f272c9-889d-4123-88a6-abf9ca451889', 'en', 'US', 'My lips are dry');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('e3978634-b97d-4bef-96e6-c3112bf0f42b', 'en', 'US', 'I can feel pain');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('a8e7ca3d-4697-4026-b47b-d9ac22c4603c', 'en', 'US', 'That is uncomfortable');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('1f2f5e89-feb1-4a53-b20e-dd63cb4d74be', 'en', 'US', 'That feels good');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('b0b7418c-3eb7-44fb-a60b-dd66bb5ef279', 'en', 'US', 'That feels bad');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('184a43e9-8e46-40f7-8bd9-af5b59237df2', 'en', 'US', 'My throat hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('db2464fd-eb0b-4e58-8a36-4f0975bba390', 'en', 'US', 'My head hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ff0349d3-5772-4edc-b2bc-e6bc0811c918', 'en', 'US', 'My chest hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('a8b47c7b-cd93-4355-9e77-85f57c90cfc6', 'en', 'US', 'This hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('b6a910c3-49b0-4269-95ca-541a049eae9e', 'en', 'US', 'What seems to be the problem?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('d1bc447c-7f63-436b-b2c8-6a42418b7ec7', 'en', 'US', 'Are you in pain?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('af5610d1-5479-4ea2-8072-5c94cc1e0d95', 'en', 'US', 'Does it hurt when I do this?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('78395a6e-6bf0-4a1c-9cbd-d146638f78e1', 'en', 'US', 'I am going to listen to your lungs');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('fa0cd308-74bb-4f80-a032-3048a57d6d8a', 'en', 'US', 'Breathe deeply');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('64e032ea-9f73-4903-aa02-0f8b1adfc291', 'en', 'US', 'I am going to look in your throat');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ec304f64-acdb-4971-8b33-7cd5573aa0b6', 'en', 'US', 'Open your mouth and stick out your tongue');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('b1ff3649-9723-4ac8-a934-f3285a9c924b', 'en', 'US', 'I am going to take your temperature');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ae989b9c-5fcb-46e7-9421-b301b25fd561', 'en', 'US', 'I am going to take your blood pressure');

-- ------------------
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4d3a214a-138a-4670-ab40-2b5207d267d1', 'es', 'US', 'Sí');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('3fef20bc-cf64-4429-a442-da209192657c', 'es', 'US', 'No');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('83d26244-a138-4243-be60-e605198ed58d', 'es', 'US', 'Hola');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('dfc99696-a8f9-4b30-b032-818bc936f84e', 'es', 'US', 'Adiós');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ee0bd950-4e96-42b1-9378-5c47ba6236ca', 'es', 'US', 'Por favor');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4c95451d-8df9-4928-9667-1452242b6edd', 'es', 'US', 'Gracias');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('518580e0-e561-482e-b995-e61da953d1ec', 'es', 'US', '¿Dónde está el baño?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('822ab852-d89c-4ffa-8ebb-e4082190c89f', 'es', 'US', '¿Dónde puedo conectarme a Internet?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('751316be-537c-4d69-8f0d-d8670dfa67f8', 'es', 'US', 'Allí');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('374141ef-ed8c-4348-93cb-bcb15d345368', 'es', 'US', "No sé");
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('fa9ea43d-7ef7-4d10-a66a-426652032839', 'es', 'US', 'Tengo calor');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('27166da7-b96b-4ed1-a704-b454d8b44687', 'es', 'US', 'Tengo frio');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('e7427bbc-5679-4fb0-8a50-f2a98e6c5f50', 'es', 'US', 'Tengo hambre');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('59afe82b-aa1c-4231-a2b8-70577b704714', 'es', 'US', 'Tengo sed');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('c607580f-0762-4948-81be-ffed2264aa6e', 'es', 'US', 'Necesito medicamentos');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('56aa1e7c-6972-4f0d-9ea0-8ae9ec82d771', 'es', 'US', 'Necesito ir al baño');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ac434b48-8741-43e0-9b8c-34aa47bbcbea', 'es', 'US', 'Necesito ser succionado');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('d690eac9-651b-44ad-91b9-3c5c0c9d069c', 'es', 'US', '¿Puedes por favor pásame el mando a distancia?');

INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ec3fb599-c58e-428f-9bd8-b5a5f89f55dc', 'es', 'US', '[es]I need a rinse');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('1a5bdd05-b8e8-4635-a566-1a9a4c691cad', 'es', 'US', '[es]My jaw hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('a8e5d592-7d18-492f-b985-e1fbd72c225d', 'es', 'US', '[es]How much longer?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('49f272c9-889d-4123-88a6-abf9ca451889', 'es', 'US', '[es]My lips are dry');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('e3978634-b97d-4bef-96e6-c3112bf0f42b', 'es', 'US', '[es]I can feel pain');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('a8e7ca3d-4697-4026-b47b-d9ac22c4603c', 'es', 'US', '[es]That is uncomfortable');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('1f2f5e89-feb1-4a53-b20e-dd63cb4d74be', 'es', 'US', '[es]That feels good');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('b0b7418c-3eb7-44fb-a60b-dd66bb5ef279', 'es', 'US', '[es]That feels bad');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('184a43e9-8e46-40f7-8bd9-af5b59237df2', 'es', 'US', '[es]My throat hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('db2464fd-eb0b-4e58-8a36-4f0975bba390', 'es', 'US', '[es]My head hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ff0349d3-5772-4edc-b2bc-e6bc0811c918', 'es', 'US', '[es]My chest hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('a8b47c7b-cd93-4355-9e77-85f57c90cfc6', 'es', 'US', '[es]This hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('b6a910c3-49b0-4269-95ca-541a049eae9e', 'es', 'US', '[es]What seems to be the problem?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('d1bc447c-7f63-436b-b2c8-6a42418b7ec7', 'es', 'US', '[es]Are you in pain?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('af5610d1-5479-4ea2-8072-5c94cc1e0d95', 'es', 'US', '[es]Does it hurt when I do this?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('78395a6e-6bf0-4a1c-9cbd-d146638f78e1', 'es', 'US', '[es]I am going to listen to your lungs');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('fa0cd308-74bb-4f80-a032-3048a57d6d8a', 'es', 'US', '[es]Breathe deeply');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('64e032ea-9f73-4903-aa02-0f8b1adfc291', 'es', 'US', '[es]I am going to look in your throat');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ec304f64-acdb-4971-8b33-7cd5573aa0b6', 'es', 'US', '[es]Open your mouth and stick out your tongue');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('b1ff3649-9723-4ac8-a934-f3285a9c924b', 'es', 'US', '[es]I am going to take your temperature');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ae989b9c-5fcb-46e7-9421-b301b25fd561', 'es', 'US', '[es]I am going to take your blood pressure');

-- -------------------
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4d3a214a-138a-4670-ab40-2b5207d267d1', 'fr', 'FR', 'Oui');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('3fef20bc-cf64-4429-a442-da209192657c', 'fr', 'FR', 'Non');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('83d26244-a138-4243-be60-e605198ed58d', 'fr', 'FR', 'Bonjour');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('dfc99696-a8f9-4b30-b032-818bc936f84e', 'fr', 'FR', 'Au revoir');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ee0bd950-4e96-42b1-9378-5c47ba6236ca', 'fr', 'FR', "S\'il vous plaît");
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4c95451d-8df9-4928-9667-1452242b6edd', 'fr', 'FR', 'Merci');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('518580e0-e561-482e-b995-e61da953d1ec', 'fr', 'FR', 'Où est la salle de bain?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('822ab852-d89c-4ffa-8ebb-e4082190c89f', 'fr', 'FR', "Où, puis-je me connecter à l\'Internet?");
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('751316be-537c-4d69-8f0d-d8670dfa67f8', 'fr', 'FR', 'Là-bas');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('374141ef-ed8c-4348-93cb-bcb15d345368', 'fr', 'FR', "Je ne sais pas");
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('fa9ea43d-7ef7-4d10-a66a-426652032839', 'fr', 'FR', "J\'ai chaud");
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('27166da7-b96b-4ed1-a704-b454d8b44687', 'fr', 'FR', "J\'ai froid");
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('e7427bbc-5679-4fb0-8a50-f2a98e6c5f50', 'fr', 'FR', "J\'ai faim");
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('59afe82b-aa1c-4231-a2b8-70577b704714', 'fr', 'FR', "J\'ai soif");
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('c607580f-0762-4948-81be-ffed2264aa6e', 'fr', 'FR', "J\'ai besoin de médicaments");
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('56aa1e7c-6972-4f0d-9ea0-8ae9ec82d771', 'fr', 'FR', "Je dois aller à la salle de bain");
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ac434b48-8741-43e0-9b8c-34aa47bbcbea', 'fr', 'FR', "Je dois d'être aspiré");
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('d690eac9-651b-44ad-91b9-3c5c0c9d069c', 'fr', 'FR', "Je voudrais la télécommande");

INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ec3fb599-c58e-428f-9bd8-b5a5f89f55dc', 'fr', 'FR', '[fr]I need a rinse');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('1a5bdd05-b8e8-4635-a566-1a9a4c691cad', 'fr', 'FR', '[fr]My jaw hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('a8e5d592-7d18-492f-b985-e1fbd72c225d', 'fr', 'FR', '[fr]How much longer?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('49f272c9-889d-4123-88a6-abf9ca451889', 'fr', 'FR', '[fr]My lips are dry');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('e3978634-b97d-4bef-96e6-c3112bf0f42b', 'fr', 'FR', '[fr]I can feel pain');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('a8e7ca3d-4697-4026-b47b-d9ac22c4603c', 'fr', 'FR', '[fr]That is uncomfortable');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('1f2f5e89-feb1-4a53-b20e-dd63cb4d74be', 'fr', 'FR', '[fr]That feels good');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('b0b7418c-3eb7-44fb-a60b-dd66bb5ef279', 'fr', 'FR', '[fr]That feels bad');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('184a43e9-8e46-40f7-8bd9-af5b59237df2', 'fr', 'FR', '[fr]My throat hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('db2464fd-eb0b-4e58-8a36-4f0975bba390', 'fr', 'FR', '[fr]My head hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ff0349d3-5772-4edc-b2bc-e6bc0811c918', 'fr', 'FR', '[fr]My chest hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('a8b47c7b-cd93-4355-9e77-85f57c90cfc6', 'fr', 'FR', '[fr]This hurts');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('b6a910c3-49b0-4269-95ca-541a049eae9e', 'fr', 'FR', '[fr]What seems to be the problem?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('d1bc447c-7f63-436b-b2c8-6a42418b7ec7', 'fr', 'FR', '[fr]Are you in pain?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('af5610d1-5479-4ea2-8072-5c94cc1e0d95', 'fr', 'FR', '[fr]Does it hurt when I do this?');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('78395a6e-6bf0-4a1c-9cbd-d146638f78e1', 'fr', 'FR', '[fr]I am going to listen to your lungs');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('fa0cd308-74bb-4f80-a032-3048a57d6d8a', 'fr', 'FR', '[fr]Breathe deeply');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('64e032ea-9f73-4903-aa02-0f8b1adfc291', 'fr', 'FR', '[fr]I am going to look in your throat');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ec304f64-acdb-4971-8b33-7cd5573aa0b6', 'fr', 'FR', '[fr]Open your mouth and stick out your tongue');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('b1ff3649-9723-4ac8-a934-f3285a9c924b', 'fr', 'FR', '[fr]I am going to take your temperature');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ae989b9c-5fcb-46e7-9421-b301b25fd561', 'fr', 'FR', '[fr]I am going to take your blood pressure');

-- de DE
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4d3a214a-138a-4670-ab40-2b5207d267d1', 'de', 'DE', 'Ja');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('3fef20bc-cf64-4429-a442-da209192657c', 'de', 'DE', 'Nein');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('83d26244-a138-4243-be60-e605198ed58d', 'de', 'DE', '[de] Hello');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('dfc99696-a8f9-4b30-b032-818bc936f84e', 'de', 'DE', '[de] Goodbye');

INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ee0bd950-4e96-42b1-9378-5c47ba6236ca', 'de', 'DE', 'Bitte');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4c95451d-8df9-4928-9667-1452242b6edd', 'de', 'DE', 'Danke');

-- zh CN
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4d3a214a-138a-4670-ab40-2b5207d267d1', 'zh', 'CN', '是');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('3fef20bc-cf64-4429-a442-da209192657c', 'zh', 'CN', '没有');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('83d26244-a138-4243-be60-e605198ed58d', 'zh', 'CN', '[zh] Hello');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('dfc99696-a8f9-4b30-b032-818bc936f84e', 'zh', 'CN', '[zh] Goodbye');

INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ee0bd950-4e96-42b1-9378-5c47ba6236ca', 'zh', 'CN', '请');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4c95451d-8df9-4928-9667-1452242b6edd', 'zh', 'CN', '谢谢你');

-- ja JP
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4d3a214a-138a-4670-ab40-2b5207d267d1', 'ja', 'JP', 'はい');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('3fef20bc-cf64-4429-a442-da209192657c', 'ja', 'JP', 'なし');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('83d26244-a138-4243-be60-e605198ed58d', 'ja', 'JP', '[ja] Hello');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('dfc99696-a8f9-4b30-b032-818bc936f84e', 'ja', 'JP', '[ja] Goodbye');

INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ee0bd950-4e96-42b1-9378-5c47ba6236ca', 'ja', 'JP', 'してください');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4c95451d-8df9-4928-9667-1452242b6edd', 'ja', 'JP', 'ありがとう');

-- ko KR
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4d3a214a-138a-4670-ab40-2b5207d267d1', 'ko', 'KR', '예');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('3fef20bc-cf64-4429-a442-da209192657c', 'ko', 'KR', '아니요');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('83d26244-a138-4243-be60-e605198ed58d', 'ko', 'KR', '[ko] Hello');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('dfc99696-a8f9-4b30-b032-818bc936f84e', 'ko', 'KR', '[ko] Goodbye');

INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('ee0bd950-4e96-42b1-9378-5c47ba6236ca', 'ko', 'KR', '제발');
INSERT INTO dictionary_entry_translation (entry_uuid, language, locale, translation) VALUES ('4c95451d-8df9-4928-9667-1452242b6edd', 'ko', 'KR', '감사합니다');

CREATE TABLE IF NOT EXISTS dictionary (
  context_uuid VARCHAR(36) NOT NULL
  , entry_uuid VARCHAR(36) NOT NULL
  , sort_order INTEGER NOT NULL
  , create_ts TIMESTAMP DEFAULT NOW()
  , delete_ts TIMESTAMP
  , PRIMARY KEY (context_uuid, entry_uuid)
  , FOREIGN KEY (context_uuid) REFERENCES dictionary_context(uuid) ON DELETE CASCADE
  , FOREIGN KEY (entry_uuid) REFERENCES dictionary_entry(uuid) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- General
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('0959dc0c-5ebb-4b96-ac12-8c612176b675', '83d26244-a138-4243-be60-e605198ed58d', '0');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('0959dc0c-5ebb-4b96-ac12-8c612176b675', 'dfc99696-a8f9-4b30-b032-818bc936f84e', '1');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('0959dc0c-5ebb-4b96-ac12-8c612176b675', '4d3a214a-138a-4670-ab40-2b5207d267d1', '2');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('0959dc0c-5ebb-4b96-ac12-8c612176b675', '3fef20bc-cf64-4429-a442-da209192657c', '3');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('0959dc0c-5ebb-4b96-ac12-8c612176b675', 'ee0bd950-4e96-42b1-9378-5c47ba6236ca', '4');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('0959dc0c-5ebb-4b96-ac12-8c612176b675', '4c95451d-8df9-4928-9667-1452242b6edd', '5');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('0959dc0c-5ebb-4b96-ac12-8c612176b675', '518580e0-e561-482e-b995-e61da953d1ec', '6');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('0959dc0c-5ebb-4b96-ac12-8c612176b675', '822ab852-d89c-4ffa-8ebb-e4082190c89f', '7');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('0959dc0c-5ebb-4b96-ac12-8c612176b675', '751316be-537c-4d69-8f0d-d8670dfa67f8', '8');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('0959dc0c-5ebb-4b96-ac12-8c612176b675', '374141ef-ed8c-4348-93cb-bcb15d345368', '9');

-- Medical-Patient
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('92e8eee5-10f1-447e-a7df-716ad16bebf2', '4d3a214a-138a-4670-ab40-2b5207d267d1', '0');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('92e8eee5-10f1-447e-a7df-716ad16bebf2', '3fef20bc-cf64-4429-a442-da209192657c', '1');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('92e8eee5-10f1-447e-a7df-716ad16bebf2', 'fa9ea43d-7ef7-4d10-a66a-426652032839', '2');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('92e8eee5-10f1-447e-a7df-716ad16bebf2', '27166da7-b96b-4ed1-a704-b454d8b44687', '3');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('92e8eee5-10f1-447e-a7df-716ad16bebf2', 'e7427bbc-5679-4fb0-8a50-f2a98e6c5f50', '4');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('92e8eee5-10f1-447e-a7df-716ad16bebf2', '59afe82b-aa1c-4231-a2b8-70577b704714', '5');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('92e8eee5-10f1-447e-a7df-716ad16bebf2', 'c607580f-0762-4948-81be-ffed2264aa6e', '6');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('92e8eee5-10f1-447e-a7df-716ad16bebf2', '56aa1e7c-6972-4f0d-9ea0-8ae9ec82d771', '7');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('92e8eee5-10f1-447e-a7df-716ad16bebf2', 'ac434b48-8741-43e0-9b8c-34aa47bbcbea', '8');
INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('92e8eee5-10f1-447e-a7df-716ad16bebf2', 'd690eac9-651b-44ad-91b9-3c5c0c9d069c', '9');

-- Medical-Provider
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', '4d3a214a-138a-4670-ab40-2b5207d267d1', '0');
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', '3fef20bc-cf64-4429-a442-da209192657c', '1');
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', 'fa9ea43d-7ef7-4d10-a66a-426652032839', '2');
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', '27166da7-b96b-4ed1-a704-b454d8b44687', '3');
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', 'b6a910c3-49b0-4269-95ca-541a049eae9e', '4');
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', 'd1bc447c-7f63-436b-b2c8-6a42418b7ec7', '5');
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', 'af5610d1-5479-4ea2-8072-5c94cc1e0d95', '6');
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', '78395a6e-6bf0-4a1c-9cbd-d146638f78e1', '7');
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', 'fa0cd308-74bb-4f80-a032-3048a57d6d8a', '8');
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', '64e032ea-9f73-4903-aa02-0f8b1adfc291', '9');
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', 'ec304f64-acdb-4971-8b33-7cd5573aa0b6', '10');
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', 'b1ff3649-9723-4ac8-a934-f3285a9c924b', '11');
-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', 'ae989b9c-5fcb-46e7-9421-b301b25fd561', '12');

-- INSERT INTO dictionary (context_uuid, entry_uuid, sort_order) VALUES ('4602831f-fb46-4fd7-ae97-80b930444520', '', \');
