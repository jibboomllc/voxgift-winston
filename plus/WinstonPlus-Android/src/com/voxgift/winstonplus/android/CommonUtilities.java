package com.voxgift.winstonplus.android;

import android.content.Context;
import android.content.Intent;

/**
 * Helper class providing methods and constants common to other classes in the
 * app.
 */
public final class CommonUtilities {

    /**
     * Base URL of the Demo Server (such as http://my_host:8080/gcm-demo)
     */
    static final String SERVER_URL = "https://voxgift.com/push/gcm";

    /**
     * Google API project id registered to use GCM.
     */
    static final String SENDER_ID = "1051268704650";

    /**
     * Tag used on log messages.
     */
    static final String TAG = "WinstonPlus";

    /**
     * Intent used to pass registration ID.
     */
    static final String REGISTRATION_ACTION =
            "com.voxgift.winstonplus.android.REGISTRATION";

    /**
     * Intent's extra that contains the ID.
     */
    static final String REGISTRATION_ID = "123";

    /**
     * Notifies UI to save registration ID.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param id registration ID.
     */
    static void saveRegistrationId(Context context, String id) {
        Intent intent = new Intent(REGISTRATION_ACTION);
        intent.putExtra(REGISTRATION_ID, id);
        context.sendBroadcast(intent);
    }

    /**
     * Intent used to display a message in the screen.
     */
//    static final String DISPLAY_MESSAGE_ACTION =
//            "com.voxgift.winstonplus.android.DISPLAY_MESSAGE";

    /**
     * Intent's extra that contains the message to be displayed.
     */
//    static final String EXTRA_MESSAGE = "message";

    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
//    static void displayMessage(Context context, String message) {
//        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
//        intent.putExtra(EXTRA_MESSAGE, message);
//        context.sendBroadcast(intent);
//    }
}
