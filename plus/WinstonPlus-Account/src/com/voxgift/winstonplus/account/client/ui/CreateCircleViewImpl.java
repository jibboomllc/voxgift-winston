package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class CreateCircleViewImpl extends Composite implements CreateCircleView
{
	private static CreateCircleViewImplUiBinder uiBinder = GWT.create(CreateCircleViewImplUiBinder.class);

	interface CreateCircleViewImplUiBinder extends UiBinder<Widget, CreateCircleViewImpl> {
	}

	@UiField HTML greeting;
	@UiField TextBox name;
	@UiField RadioButton centerMe;
	@UiField RadioButton centerOther;
	@UiField HTMLPanel centerOtherPanel;
	@UiField EmailTextBox emailField;
	@UiField TextBox firstNameField;
	@UiField TextBox lastNameField;
	@UiField TextArea members;
	@UiField Label statusLabel;
	@UiField Label errorLabel;
	@UiField Button create;
	@UiField Button skip;

	public CreateCircleViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}


	@Override
	public HasHTML getGreetingHTML() {
		return greeting;
	}

	@Override
	public Focusable getNameFocus() {
		return name;
	}

	@Override
	public HasText getNameText() {
		return name;
	}

	@Override
	public HasKeyUpHandlers getNameKeyUp() {
		return name;
	}

	@Override
	public HasClickHandlers getCenterMeClick() {
		return centerMe;
	}

	@Override
	public HasValue<Boolean> getCenterMeValue() {
		return centerMe;
	}

	@Override
	public HasClickHandlers getCenterOtherClick() {
		return centerOther;
	}

	@Override
	public HasValue<Boolean> getCenterOtherValue() {
		return centerOther;
	}

	@Override
	public HasVisibility getCenterOtherPanelVisible() {
		return centerOtherPanel;
	}

	@Override
	public Focusable getEmailFocus() {
		return emailField;
	}

	@Override
	public HasText getEmailText() {
		return emailField;
	}

	@Override
	public HasKeyUpHandlers getEmailKeyUp() {
		return emailField;
	}

	@Override
	public HasEnabled getEmailEnable() {
		return emailField;
	}

	@Override
	public Focusable getFirstNameFocus() {
		return firstNameField;
	}

	@Override
	public HasText getFirstNameText() {
		return firstNameField;
	}

	@Override
	public HasKeyUpHandlers getFirstNameKeyUp() {
		return firstNameField;
	}

	@Override
	public HasEnabled getFirstNameEnable() {
		return firstNameField;
	}

	@Override
	public Focusable getLastNameFocus() {
		return lastNameField;
	}
	
	@Override
	public HasText getLastNameText() {
		return lastNameField;
	}

	@Override
	public HasKeyUpHandlers getLastNameKeyUp() {
		return lastNameField;
	}

	@Override
	public HasEnabled getLastNameEnable() {
		return lastNameField;
	}

	@Override
	public Focusable getMembersFocus() {
		return members;
	}

	@Override
	public HasText getMembersText() {
		return members;
	}

	@Override
	public HasText getStatusText() {
		return statusLabel;
	}

	@Override
	public HasVisibility getStatusVisibility() {
		return statusLabel;
	}

	@Override
	public HasText getErrorText() {
		return errorLabel;
	}

	@Override
	public HasVisibility getErrorVisibility() {
		return errorLabel;
	}
	@Override
	public HasClickHandlers getCreateClick() {
		return create;
	}

	@Override
	public Focusable getCreateFocus() {
		return create;
	}

	@Override
	public HasEnabled getCreateEnable() {
		return create;
	}

	@Override
	public HasClickHandlers getSkipClick() {
		return skip;
	}

	@Override
	public Focusable getSkipFocus() {
		return skip;
	}

	@Override
	public HasEnabled getSkipEnable() {
		return skip;
	}

}
