package com.voxgift.winstonplus.account.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class LegalPlace extends Place
{
	private String docId;
	
	public LegalPlace(String token)
	{
		this.docId = token;
	}

	public String getDocId()
	{
		return docId;
	}

	public static class Tokenizer implements PlaceTokenizer<LegalPlace>
	{
		@Override
		public String getToken(LegalPlace place)
		{
			return place.getDocId();
		}

		@Override
		public LegalPlace getPlace(String token)
		{
			return new LegalPlace(token);
		}
	}
	
}
