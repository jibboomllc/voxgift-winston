package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasVisibility;

public interface CreateCircleView extends DecoratedView
{
	public HasHTML getGreetingHTML();
	
	public Focusable getNameFocus();
	public HasText getNameText();
	public HasKeyUpHandlers getNameKeyUp();

	public HasClickHandlers getCenterMeClick();
	public HasValue<Boolean> getCenterMeValue();
	public HasClickHandlers getCenterOtherClick();
	public HasValue<Boolean> getCenterOtherValue();
	public HasVisibility getCenterOtherPanelVisible();

	public Focusable getEmailFocus();
	public HasText getEmailText();
	public HasKeyUpHandlers getEmailKeyUp();
	public HasEnabled getEmailEnable();

	public Focusable getFirstNameFocus();
	public HasText getFirstNameText();
	public HasKeyUpHandlers getFirstNameKeyUp();
	public HasEnabled getFirstNameEnable();

	public Focusable getLastNameFocus();
	public HasText getLastNameText();
	public HasKeyUpHandlers getLastNameKeyUp();
	public HasEnabled getLastNameEnable();

	public Focusable getMembersFocus();
	public HasText getMembersText();

	public HasText getErrorText();

	public HasClickHandlers getCreateClick();
	public Focusable getCreateFocus();
	public HasEnabled getCreateEnable();

	public HasClickHandlers getSkipClick();
	public Focusable getSkipFocus();
	public HasEnabled getSkipEnable();

}