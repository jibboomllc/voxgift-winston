package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.IsWidget;

public interface DecoratedView extends IsWidget{
	
	public HasText getStatusText();
	public HasVisibility getStatusVisibility();

	public HasText getErrorText();
	public HasVisibility getErrorVisibility();
}
