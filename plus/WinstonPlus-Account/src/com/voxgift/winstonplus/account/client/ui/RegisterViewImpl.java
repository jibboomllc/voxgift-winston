package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class RegisterViewImpl extends Composite implements RegisterView
{
	private static RegisterViewImplUiBinder uiBinder = GWT.create(RegisterViewImplUiBinder.class);

	interface RegisterViewImplUiBinder extends UiBinder<Widget, RegisterViewImpl> {
	}

	@UiField EmailTextBox emailField;
	@UiField TextBox passwordField;
	@UiField TextBox firstNameField;
	@UiField TextBox lastNameField;
	@UiField Label statusLabel;
	@UiField Label errorLabel;
	@UiField Button register;
	@UiField Anchor back;

	public RegisterViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public Focusable getEmailFocus() {
		return emailField;
	}

	@Override
	public HasText getEmailText() {
		return emailField;
	}

	@Override
	public HasKeyUpHandlers getEmailKeyUp() {
		return emailField;
	}

	@Override
	public Focusable getPasswordFocus() {
		return passwordField;
	}

	@Override
	public HasText getPasswordText() {
		return passwordField;
	}

	@Override
	public HasKeyUpHandlers getPasswordKeyUp() {
		return passwordField;
	}

	@Override
	public Focusable getFirstNameFocus() {
		return firstNameField;
	}

	@Override
	public HasText getFirstNameText() {
		return firstNameField;
	}

	@Override
	public HasKeyUpHandlers getFirstNameKeyUp() {
		return firstNameField;
	}

	@Override
	public Focusable getLastNameFocus() {
		return lastNameField;
	}

	@Override
	public HasText getLastNameText() {
		return lastNameField;
	}

	@Override
	public HasKeyUpHandlers getLastNameKeyUp() {
		return lastNameField;
	}

	@Override
	public HasText getStatusText() {
		return statusLabel;
	}

	@Override
	public HasVisibility getStatusVisibility() {
		return statusLabel;
	}

	@Override
	public HasText getErrorText() {
		return errorLabel;
	}

	@Override
	public HasVisibility getErrorVisibility() {
		return errorLabel;
	}

	@Override
	public HasClickHandlers getRegisterClick() {
		return register;
	}

	@Override
	public Focusable getRegisterFocus() {
		return register;
	}

	@Override
	public HasEnabled getRegisterEnable() {
		return register;
	}

	@Override
	public HasClickHandlers getBackClick() {
		return back;
	}

	@Override
	public HasEnabled getBackEnable() {
		return back;
	}
}
