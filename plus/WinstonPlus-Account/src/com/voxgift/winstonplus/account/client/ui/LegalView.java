package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.HasHTML;

public interface LegalView extends DecoratedView
{
	public HasHTML getContentHtml();
	public HasClickHandlers getBackClick();
	public HasClickHandlers getBack2Click();
}