package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasText;

public interface LoginView extends DecoratedView
{
	public Focusable getEmailFocus();
	public HasText getEmailText();
	public HasKeyUpHandlers getEmailKeyUp();

	public Focusable getPasswordFocus();
	public HasText getPasswordText();
	public HasKeyUpHandlers getPasswordKeyUp();

	public HasClickHandlers getLoginClick();
	public Focusable getLoginFocus();
	public HasEnabled getLoginEnable();

	public HasClickHandlers getResetPasswordClick();
	public HasEnabled getResetPasswordEnable();

	public HasClickHandlers getBackClick();
	public HasEnabled getBackEnable();
}