package com.voxgift.winstonplus.account.client.activity;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.voxgift.winstonplus.account.client.ClientFactory;
import com.voxgift.winstonplus.account.client.Track;
import com.voxgift.winstonplus.account.client.place.LoginPlace;
import com.voxgift.winstonplus.account.client.place.RegisterPlace;
import com.voxgift.winstonplus.account.client.place.WelcomePlace;
import com.voxgift.winstonplus.account.client.ui.WelcomeView;

public class WelcomeActivity extends VGAbstractActivity {
	//private java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(getClass().getName());

	private ClientFactory clientFactory;

	public WelcomeActivity(WelcomePlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);
		final WelcomeView view = clientFactory.getWelcomeView();

        Scheduler.get().scheduleDeferred(new ScheduledCommand() {
	        public void execute () {
	    		view.getRegisterFocus().setFocus(true);
	        }
	    });

		addHandlerRegistration(view.getRegisterClick().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Track.trackEvent("WelcomeActivity", "click", "register");
				clientFactory.getPlaceController().goTo(new RegisterPlace());
			}
		}));

		addHandlerRegistration(view.getLoginClick().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Track.trackEvent("WelcomeActivity", "click", "login");
				clientFactory.getPlaceController().goTo(new LoginPlace());
			}
		}));

		panel.setWidget(view.asWidget());
	}
}
