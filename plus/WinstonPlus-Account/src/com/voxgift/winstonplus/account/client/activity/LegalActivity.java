package com.voxgift.winstonplus.account.client.activity;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.voxgift.winstonplus.account.client.ClientFactory;
import com.voxgift.winstonplus.account.client.Track;
import com.voxgift.winstonplus.account.client.place.LegalPlace;
import com.voxgift.winstonplus.account.client.ui.LegalView;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentServiceAsync;

public class LegalActivity extends DecoratedActivity {
	//private java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(getClass().getName());

	private String docId;

	private final RpcDocumentServiceAsync docSvc = GWT.create(RpcDocumentService.class);

	public LegalActivity(LegalPlace place, ClientFactory clientFactory) {

		super(clientFactory, clientFactory.getLegalView());
		
		//((ServiceDefTarget)docSvc).setServiceEntryPoint(DocumentService.SERVICEPATH);
		
		docId = place.getDocId();
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);
		final LegalView view = clientFactory.getLegalView();

		refreshView();

		ClickHandler backClickHandler = new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Track.trackEvent("LegalActivity", "click", "back");
				History.back();
			}
		};
		
		addHandlerRegistration(view.getBackClick().addClickHandler(backClickHandler));
		addHandlerRegistration(view.getBack2Click().addClickHandler(backClickHandler));

		panel.setWidget(view.asWidget());
	}

	public void refreshView() {
		final LegalView view = clientFactory.getLegalView();

		final AsyncCallback<String> callback = new AsyncCallback<String>() {
			public void onFailure(Throwable caught) {
				setErrorText("Server error retrieving document");
			}

			public void onSuccess(String result) {
				view.getContentHtml().setHTML(result);
				setStatusText(null);
			}
		};

		setStatusText("Loading..");
		docSvc.getDocument(docId, "en", "US", callback);
	}

	public void setPlace(LegalPlace place) {
		if (!place.getDocId().equals(place)) {
		    docId = place.getDocId();
		    refreshView();
		}
	}
}
