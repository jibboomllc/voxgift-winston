package com.voxgift.winstonplus.account.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class GoodbyePlace extends Place
{

	public static class Tokenizer implements PlaceTokenizer<GoodbyePlace>
	{
		@Override
		public String getToken(GoodbyePlace place)
		{
			return "";
		}

		@Override
		public GoodbyePlace getPlace(String token)
		{
			return new GoodbyePlace();
		}
	}
	
}
