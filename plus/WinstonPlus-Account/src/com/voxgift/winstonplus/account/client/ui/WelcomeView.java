package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * View interface. Extends IsWidget so a view impl can easily provide
 * its container widget. 
 * 
 * @author drfibonacci
 */
public interface WelcomeView extends IsWidget
{
	public HasClickHandlers getRegisterClick();
	public HasClickHandlers getLoginClick();
	public Focusable getRegisterFocus();
}