package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.dom.client.InputElement;
import com.google.gwt.user.client.ui.TextBoxBase;

public class EmailTextBox extends TextBoxBase {

    public EmailTextBox() {
        super(createEmailInput());
    }

    private static native InputElement createEmailInput() /*-{
        var input = document.createElement("input");
        input.setAttribute("type", "email");
        input.setAttribute("pattern", "[^ @]*@[^ @]*");
        return input;
    }-*/;
}