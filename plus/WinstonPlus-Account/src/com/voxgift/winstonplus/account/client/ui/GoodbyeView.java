package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.IsWidget;

public interface GoodbyeView extends IsWidget
{
	public HasHTML getCreateCircleResults();
	public HasVisibility getRegistrationReminderVisibility();
	public HasVisibility getInvitationReminderVisibility();
}