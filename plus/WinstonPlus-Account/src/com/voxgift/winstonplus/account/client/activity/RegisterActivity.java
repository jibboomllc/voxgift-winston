package com.voxgift.winstonplus.account.client.activity;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.voxgift.winstonplus.account.client.ClientFactory;
import com.voxgift.winstonplus.account.client.Track;
import com.voxgift.winstonplus.account.client.place.CreateCirclePlace;
import com.voxgift.winstonplus.account.client.place.RegisterPlace;
import com.voxgift.winstonplus.account.client.ui.RegisterView;
import com.voxgift.winstonplus.data.shared.FieldVerifier;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserServiceAsync;

public class RegisterActivity extends DecoratedActivity {
	//private java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(getClass().getName());

	private final RpcUserServiceAsync userSvc = GWT.create(RpcUserService.class);

	public RegisterActivity(RegisterPlace place, ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getRegisterView());
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);
		final RegisterView view = clientFactory.getRegisterView();

		setErrorText(null);

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
	        public void execute () {
	    		view.getEmailFocus().setFocus(true);
	        }
	    });

		addHandlerRegistration(view.getBackClick().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Track.trackEvent("RegisterActivity", "click", "back");
				History.back();
			}
		}));

		addHandlerRegistration(view.getEmailKeyUp().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					Track.trackEvent("RegisterActivity", "enterKey", "email");
					if (!FieldVerifier.isValidEmailAddress(view.getEmailText().getText().toLowerCase().trim())) {
						setErrorText ("Email address does not validate: "
								+ FieldVerifier.validEmailAddressDescription("en", "US") + ". ");
					} else {
						setErrorText(null);
						view.getPasswordFocus().setFocus(true);
					}
				}
			}
		}));

		addHandlerRegistration(view.getPasswordKeyUp().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					Track.trackEvent("RegisterActivity", "enterKey", "password");
					if (!FieldVerifier.isValidPassword(view.getPasswordText().getText())) {
						setErrorText ("Password does not validate: "
								+ FieldVerifier.validPasswordDescription("en", "US") + ". ");
					} else {
						setErrorText(null);
						view.getFirstNameFocus().setFocus(true);
					}
				}
			}
		}));

		addHandlerRegistration(view.getFirstNameKeyUp().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					Track.trackEvent("RegisterActivity", "enterKey", "firstName");
					view.getLastNameFocus().setFocus(true);
				}
			}
		}));

		class MyHandler implements ClickHandler, KeyUpHandler {
			public void onClick(ClickEvent event) {
				Track.trackEvent("RegisterActivity", "click", "register");
				sendDataToServer();
			}

			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					Track.trackEvent("RegisterActivity", "enterKey", "lastName");
					sendDataToServer();
				}
			}

			private void setWorking(boolean working) {
				view.getRegisterEnable().setEnabled(!working);
				view.getBackEnable().setEnabled(!working);
			}

			private void sendDataToServer() {
				// First, we validate the input.
				setStatusText("Validating data");
				String errorText = new String();

				if (!FieldVerifier.isValidEmailAddress(view.getEmailText().getText().toLowerCase().trim())) {
					errorText = errorText.concat("Email address does not validate: "
							+ FieldVerifier.validEmailAddressDescription("en", "US") + ". ");
				}

				if (!FieldVerifier.isValidPassword(view.getPasswordText().getText())) {
					errorText = errorText.concat("Password does not validate: "
							+ FieldVerifier.validPasswordDescription("en", "US") + ". ");
				}

				if (!errorText.isEmpty()) {
					setErrorText(errorText);
					return;
				}

				// Next we check if the user already exists 
				final AsyncCallback<String> callback = new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						setErrorText("Server error while verifing email address");
						setWorking(false);
					}

					public void onSuccess(String result) {
						if (result == null) {
							registerUser();
						} else {
							setErrorText(
									"Whoops, " + view.getEmailText().getText().toLowerCase().trim()
									+ " is already registered! Please register with a different email or go back and login.");
							setWorking(false);
						}
					}
				};

				setStatusText("Verifying new user");
				setWorking(true);
				userSvc.lookupIdByEmail(view.getEmailText().getText().toLowerCase().trim(), callback);
			}

			private void registerUser() {

				final AsyncCallback<String> verifyUserCallback = new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						setErrorText("Server error looking up user by email address");
						setWorking(false);
					}

					public void onSuccess(String result) {
						if (result == null) {
							setErrorText("Error looking up user by email address");
							setWorking(false);
						} else {
							Cookies.setCookie("userId", result);
							Cookies.setCookie("email", view.getEmailText().getText().toLowerCase().trim());
							Cookies.setCookie("firstName", view.getFirstNameText().getText());
							Cookies.setCookie("lastName", view.getLastNameText().getText());
							Cookies.setCookie("newUser", "1");
							setStatusText(null);
							setWorking(false);
							clientFactory.getPlaceController().goTo(new CreateCirclePlace());
						}
					}
				};

				final AsyncCallback<Boolean> registerCallback = new AsyncCallback<Boolean>() {
					public void onFailure(Throwable caught) {
						setErrorText("Server error registering user");
						setWorking(false);
					}

					public void onSuccess(Boolean result) {
						if (result == null || result == false) {
							setErrorText("Error registering user");
							setWorking(false);
						} else {
							setStatusText("Looking up user");
							userSvc.lookupIdByEmail(view.getEmailText().getText().toLowerCase().trim(), verifyUserCallback);
						}
					}
				};

				setStatusText("Registering user");
				setWorking(true);
				userSvc.register(view.getEmailText().getText().toLowerCase().trim(), view.getPasswordText().getText()
						, view.getFirstNameText().getText(), view.getLastNameText().getText()
						, registerCallback);
			}
		}

		// Add a handler to send the name to the server
		MyHandler handler = new MyHandler();
		view.getRegisterClick().addClickHandler(handler);
		view.getLastNameKeyUp().addKeyUpHandler(handler);

		panel.setWidget(view.asWidget());
	}
}
