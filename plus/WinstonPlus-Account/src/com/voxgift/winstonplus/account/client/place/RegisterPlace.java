package com.voxgift.winstonplus.account.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class RegisterPlace extends Place
{

	public static class Tokenizer implements PlaceTokenizer<RegisterPlace>
	{

		@Override
		public String getToken(RegisterPlace place)
		{
			return "";
		}

		@Override
		public RegisterPlace getPlace(String token)
		{
			return new RegisterPlace();
		}

	}
	
}
