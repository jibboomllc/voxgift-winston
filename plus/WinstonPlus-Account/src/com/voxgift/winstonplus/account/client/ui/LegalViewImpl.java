package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class LegalViewImpl extends Composite implements LegalView
{
	private static LegalViewImplUiBinder uiBinder = GWT.create(LegalViewImplUiBinder.class);

	interface LegalViewImplUiBinder extends UiBinder<Widget, LegalViewImpl> {
	}

	@UiField Label errorLabel;
	@UiField Label statusLabel;
	@UiField HTML content;
	@UiField Anchor back;
	@UiField Anchor back2;

	public LegalViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public HasText getStatusText() {
		return statusLabel;
	}

	@Override
	public HasVisibility getStatusVisibility() {
		return statusLabel;
	}

	@Override
	public HasText getErrorText() {
		return errorLabel;
	}

	@Override
	public HasVisibility getErrorVisibility() {
		return errorLabel;
	}

	@Override
	public HasHTML getContentHtml() {
		return content;
	}

	@Override
	public HasClickHandlers getBackClick() {
		return back;
	}

	@Override
	public HasClickHandlers getBack2Click() {
		return back2;
	}
}
