package com.voxgift.winstonplus.account.client.activity;

import com.google.gwt.activity.shared.AbstractActivity;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.voxgift.winstonplus.account.client.ClientFactory;
import com.voxgift.winstonplus.account.client.place.GoodbyePlace;
import com.voxgift.winstonplus.account.client.ui.GoodbyeView;

public class GoodbyeActivity extends AbstractActivity {
	private ClientFactory clientFactory;

	public GoodbyeActivity(GoodbyePlace place, ClientFactory clientFactory) {
		this.clientFactory = clientFactory;
	}

	@Override
	public void start(AcceptsOneWidget containerWidget, EventBus eventBus) {
		GoodbyeView view = clientFactory.getGoodbyeView();

		if (Cookies.getCookie("circleName") != null && !Cookies.getCookie("circleName").isEmpty()) {
			view.getCreateCircleResults().setHTML("You successfully created <strong>" 
					+ Cookies.getCookie("circleName") + "</strong>.");
		}
		
		view.getInvitationReminderVisibility().setVisible(
				Cookies.getCookie("circleName") != null && !Cookies.getCookie("circleName").isEmpty());
		
		view.getRegistrationReminderVisibility().setVisible(
				Cookies.getCookie("newUser") != null && Cookies.getCookie("newUser").equals("1"));
		
		containerWidget.setWidget(view.asWidget());
	}
}