package com.voxgift.winstonplus.account.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class CreateCirclePlace extends Place
{

	public static class Tokenizer implements PlaceTokenizer<CreateCirclePlace>
	{
		@Override
		public String getToken(CreateCirclePlace place)
		{
			return "";
		}

		@Override
		public CreateCirclePlace getPlace(String token)
		{
			return new CreateCirclePlace();
		}
	}
	
}
