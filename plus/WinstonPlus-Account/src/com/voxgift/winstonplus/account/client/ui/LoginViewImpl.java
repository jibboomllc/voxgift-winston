package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;

public class LoginViewImpl extends Composite implements LoginView
{
	private static LoginViewImplUiBinder uiBinder = GWT.create(LoginViewImplUiBinder.class);

	interface LoginViewImplUiBinder extends UiBinder<Widget, LoginViewImpl> {
	}

	@UiField EmailTextBox emailField;
	@UiField TextBox passwordField;
	@UiField Label errorLabel;
	@UiField Label statusLabel;
	@UiField Button login;
	@UiField Anchor resetPassword;
	@UiField Anchor back;
	
	public LoginViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public Focusable getEmailFocus() {
		return emailField;
	}

	@Override
	public HasText getEmailText() {
		return emailField;
	}

	@Override
	public HasKeyUpHandlers getEmailKeyUp() {
		return emailField;
	}

	@Override
	public Focusable getPasswordFocus() {
		return passwordField;
	}

	@Override
	public HasText getPasswordText() {
		return passwordField;
	}

	@Override
	public HasKeyUpHandlers getPasswordKeyUp() {
		return passwordField;
	}

	@Override
	public HasText getStatusText() {
		return statusLabel;
	}

	@Override
	public HasVisibility getStatusVisibility() {
		return statusLabel;
	}

	@Override
	public HasText getErrorText() {
		return errorLabel;
	}

	@Override
	public HasVisibility getErrorVisibility() {
		return errorLabel;
	}

	@Override
	public HasClickHandlers getLoginClick() {
		return login;
	}

	@Override
	public Focusable getLoginFocus() {
		return login;
	}

	@Override
	public HasEnabled getLoginEnable() {
		return login;
	}

	@Override
	public HasClickHandlers getResetPasswordClick() {
		return resetPassword;
	}

	@Override
	public HasEnabled getResetPasswordEnable() {
		return resetPassword;
	}

	@Override
	public HasClickHandlers getBackClick() {
		return back;
	}

	@Override
	public HasEnabled getBackEnable() {
		return back;
	}

}
