package com.voxgift.winstonplus.account.client.mvp;

import com.google.gwt.place.shared.PlaceHistoryMapper;
import com.google.gwt.place.shared.WithTokenizers;
import com.voxgift.winstonplus.account.client.place.AdminPlace;
import com.voxgift.winstonplus.account.client.place.CreateCirclePlace;
import com.voxgift.winstonplus.account.client.place.GoodbyePlace;
import com.voxgift.winstonplus.account.client.place.LoginPlace;
import com.voxgift.winstonplus.account.client.place.LegalPlace;
import com.voxgift.winstonplus.account.client.place.RegisterPlace;
import com.voxgift.winstonplus.account.client.place.WelcomePlace;

/**
 * PlaceHistoryMapper interface is used to attach all places which the
 * PlaceHistoryHandler should be aware of. This is done via the @WithTokenizers
 * annotation or by extending PlaceHistoryMapperWithFactory and creating a
 * separate TokenizerFactory.
 */
@WithTokenizers( { 
    AdminPlace.Tokenizer.class 
	, CreateCirclePlace.Tokenizer.class 
	, GoodbyePlace.Tokenizer.class 
	, LoginPlace.Tokenizer.class 
	, LegalPlace.Tokenizer.class 
	, RegisterPlace.Tokenizer.class 
	, WelcomePlace.Tokenizer.class
	})
public interface AppPlaceHistoryMapper extends PlaceHistoryMapper {
}
