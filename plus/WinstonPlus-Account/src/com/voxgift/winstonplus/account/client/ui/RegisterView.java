package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasText;

public interface RegisterView extends DecoratedView
{
	public Focusable getEmailFocus();
	public HasText getEmailText();
	public HasKeyUpHandlers getEmailKeyUp();

	public Focusable getPasswordFocus();
	public HasText getPasswordText();
	public HasKeyUpHandlers getPasswordKeyUp();

	public Focusable getFirstNameFocus();
	public HasText getFirstNameText();
	public HasKeyUpHandlers getFirstNameKeyUp();

	public Focusable getLastNameFocus();
	public HasText getLastNameText();
	public HasKeyUpHandlers getLastNameKeyUp();

	public HasClickHandlers getRegisterClick();
	public Focusable getRegisterFocus();
	public HasEnabled getRegisterEnable();

	public HasClickHandlers getBackClick();
	public HasEnabled getBackEnable();

	public HasText getErrorText();
}