package com.voxgift.winstonplus.account.client.activity;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.voxgift.winstonplus.account.client.ClientFactory;
import com.voxgift.winstonplus.account.client.Track;
import com.voxgift.winstonplus.account.client.place.CreateCirclePlace;
import com.voxgift.winstonplus.account.client.place.LoginPlace;
import com.voxgift.winstonplus.account.client.ui.LoginView;
import com.voxgift.winstonplus.data.shared.FieldVerifier;
import com.voxgift.winstonplus.data.shared.model.UserDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserServiceAsync;

public class LoginActivity extends DecoratedActivity {
	//private java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(getClass().getName());

	private final RpcUserServiceAsync userSvc = GWT.create(RpcUserService.class);

	public LoginActivity(LoginPlace place, ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getLoginView());
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);
		final LoginView view = clientFactory.getLoginView();

        Scheduler.get().scheduleDeferred(new ScheduledCommand() {
	        public void execute () {
	    		view.getEmailFocus().setFocus(true);
	        }
	    });
		setErrorText(null);

		addHandlerRegistration(view.getBackClick().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Track.trackEvent("LoginActivity", "click", "back");
				History.back();
			}
		}));

		addHandlerRegistration(view.getEmailKeyUp().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					Track.trackEvent("LoginActivity", "enterKey", "email");
					if (!FieldVerifier.isValidEmailAddress(view.getEmailText().getText().toLowerCase().trim())) {
						setErrorText ("Email address does not validate: "
								+ FieldVerifier.validEmailAddressDescription("en", "US") + ". ");
					} else {
						setErrorText(null);
						view.getPasswordFocus().setFocus(true);
					}
				}
			}
		}));

		class MyHandler implements ClickHandler, KeyUpHandler {
			public void onClick(ClickEvent event) {
				Track.trackEvent("LoginActivity", "click", "login");
				sendDataToServer();
			}

			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					Track.trackEvent("LoginActivity", "enterKey", "password");
					sendDataToServer();
				}
			}

			private void setWorking(boolean working) {
				view.getResetPasswordEnable().setEnabled(!working);
				view.getLoginEnable().setEnabled(!working);
				view.getBackEnable().setEnabled(!working);
			}

			private void sendDataToServer() {
				// First, we validate the input.
				setStatusText("Validating data");
				String errorText = new String();

				if (!FieldVerifier.isValidEmailAddress(view.getEmailText().getText().toLowerCase().trim())) {
					errorText = errorText.concat("Email address does not validate: "
							+ FieldVerifier.validEmailAddressDescription("en", "US") + ". ");
				}

				if (!FieldVerifier.isValidPassword(view.getPasswordText().getText())) {
					errorText = errorText.concat("Password does not validate: "
							+ FieldVerifier.validPasswordDescription("en", "US") + ". ");
				}

				if (!errorText.isEmpty()) {
					setErrorText(errorText);
					return;
				}

				final AsyncCallback<UserDetails> getUserCallback = new AsyncCallback<UserDetails>() {
					public void onFailure(Throwable caught) {
						setErrorText("Server error getting user details");
						setWorking(false);
					}

					public void onSuccess(UserDetails result) {
						if (result == null) {
							setErrorText("Error getting user details");
						} else {
							Cookies.setCookie("userId", result.getId());
							Cookies.setCookie("email", result.getPrimaryEmail());
							Cookies.setCookie("firstName", result.getFirstName());
							Cookies.setCookie("lastName", result.getLastName());
							Cookies.setCookie("newUser", "0");
							clientFactory.getPlaceController().goTo(new CreateCirclePlace());
							setStatusText(null);
						}
						setWorking(false);
					}
				};

				final AsyncCallback<String> loginCallback = new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						setErrorText("Server error logging in");
						setWorking(false);
					}

					public void onSuccess(String result) {
						if (result == null) {
							setErrorText("Sorry, but we it would appear that either the password you supplied is incorrect or you have not verified your email address!");
							setWorking(false);
						} else {
							setStatusText("Getting user data");
							userSvc.getUser(result, getUserCallback);
						}
					}
				};

				final AsyncCallback<String> verifyUserCallback = new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						setErrorText("Server error while verifing email address");
						setWorking(false);
					}

					public void onSuccess(String result) {
						if (result == null) {
							setErrorText("Sorry but " + view.getEmailText().getText().toLowerCase().trim() 
									+ " was not found! Please login using a different address or go back and register this address.");
							setWorking(false);
						} else {
							setStatusText("Logging in");
							userSvc.login(view.getEmailText().getText().toLowerCase().trim(), view.getPasswordText().getText(), loginCallback);
						}
					}
				};

				setWorking(true);
				setStatusText("Looking up user");
				userSvc.lookupIdByEmail(view.getEmailText().getText().toLowerCase().trim(), verifyUserCallback);
			}
		}

		// Add a handler to send the name to the server
		MyHandler handler = new MyHandler();
		view.getLoginClick().addClickHandler(handler);
		view.getPasswordKeyUp().addKeyUpHandler(handler);

		panel.setWidget(view.asWidget());
	}

}
