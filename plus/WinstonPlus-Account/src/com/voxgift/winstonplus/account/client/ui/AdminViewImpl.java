package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class AdminViewImpl extends Composite implements AdminView
{
	private static AdminViewImplUiBinder uiBinder = GWT.create(AdminViewImplUiBinder.class);

	interface AdminViewImplUiBinder extends UiBinder<Widget, AdminViewImpl> {
	}

	@UiField HTML greeting;
	@UiField EmailTextBox testEmail;
	@UiField Label statusLabel;
	@UiField Label errorLabel;
	@UiField Button sendTestEmail;

	public AdminViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}


	@Override
	public HasHTML getGreetingHTML() {
		return greeting;
	}

	@Override
	public Focusable getTestEmailFocus() {
		return testEmail;
	}

	@Override
	public HasText getTestEmailText() {
		return testEmail;
	}

	@Override
	public HasKeyUpHandlers getTestEmailKeyUp() {
		return testEmail;
	}

	@Override
	public HasText getStatusText() {
		return statusLabel;
	}

	@Override
	public HasVisibility getStatusVisibility() {
		return statusLabel;
	}

	@Override
	public HasText getErrorText() {
		return errorLabel;
	}

	@Override
	public HasVisibility getErrorVisibility() {
		return errorLabel;
	}

	@Override
	public HasClickHandlers getSendTestEmailClick() {
		return sendTestEmail;
	}

	@Override
	public Focusable getSendTestEmailFocus() {
		return sendTestEmail;
	}

	@Override
	public HasEnabled getSendTestEmailEnable() {
		return sendTestEmail;
	}
}
