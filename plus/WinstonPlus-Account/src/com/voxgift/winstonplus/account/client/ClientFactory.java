package com.voxgift.winstonplus.account.client;

import com.google.web.bindery.event.shared.EventBus;
import com.google.gwt.place.shared.PlaceController;
import com.voxgift.winstonplus.account.client.ui.AdminView;
import com.voxgift.winstonplus.account.client.ui.CreateCircleView;
import com.voxgift.winstonplus.account.client.ui.GoodbyeView;
import com.voxgift.winstonplus.account.client.ui.LegalView;
import com.voxgift.winstonplus.account.client.ui.LoginView;
import com.voxgift.winstonplus.account.client.ui.RegisterView;
import com.voxgift.winstonplus.account.client.ui.WelcomeView;

public interface ClientFactory
{
	EventBus getEventBus();
	PlaceController getPlaceController();
	
	String getSponsorId();
	
	AdminView getAdminView();
	CreateCircleView getCreateCircleView();
	GoodbyeView getGoodbyeView();
	LegalView getLegalView();
	LoginView getLoginView();
	RegisterView getRegisterView();
	WelcomeView getWelcomeView();
}
