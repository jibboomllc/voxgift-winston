package com.voxgift.winstonplus.account.client.activity;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.voxgift.winstonplus.account.client.ClientFactory;
import com.voxgift.winstonplus.account.client.place.AdminPlace;
import com.voxgift.winstonplus.account.client.ui.AdminView;
import com.voxgift.winstonplus.account.client.ui.CreateCircleView;
import com.voxgift.winstonplus.data.shared.FieldVerifier;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserServiceAsync;

public class AdminActivity extends DecoratedActivity {
	//private java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(getClass().getName());

	private boolean busy;

//	private final RpcCircleServiceAsync circleSvc = GWT.create(RpcCircleService.class);

	private final RpcUserServiceAsync userSvc = GWT.create(RpcUserService.class);

	public AdminActivity(AdminPlace place, ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getAdminView());
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);
		final AdminView view = clientFactory.getAdminView();

		setErrorText(null);
//			ownerName = ownerName.concat(Cookies.getCookie("email"));
		class MyHandler implements ClickHandler, KeyUpHandler {
			public void onClick(ClickEvent event) {
				if (!busy) {
					sendDataToServer();
				}
			}

			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER && !busy) {
					sendDataToServer();
				}
			}

			private void setWorking(boolean working) {
				busy = working;
				view.getSendTestEmailEnable().setEnabled(!working);
			}

			private void sendDataToServer() {
				setWorking(true);

				// First, we validate the input.
				setStatusText("Validating data");
				String errorText = new String();

				if (!FieldVerifier.isValidEmailAddress(view.getTestEmailText().getText().toLowerCase().trim())) {
					errorText = errorText.concat("Email address [" 
							+ view.getTestEmailText().getText().toLowerCase().trim() + "] does not validate: "
							+ FieldVerifier.validEmailAddressDescription("en", "US") + ". ");
				}

				if (!errorText.isEmpty()) {
					setErrorText(errorText);
					setWorking(false);
					return;
				}

				final AsyncCallback<Boolean> sendTestEmailCallback = new AsyncCallback<Boolean>() {
					public void onFailure(Throwable caught) {
						setErrorText("Server error sending email");
						setWorking(false);
					}

					public void onSuccess(Boolean result) {
						if (result == null) {
							setErrorText("Error sending email");
							setWorking(false);
						} else {
							setWorking(false);								
						}
					}
				};

				setWorking(true);
//				clientFactory.getSponsorId();
				userSvc.sendTestEmail(view.getTestEmailText().getText(), sendTestEmailCallback);
			}
		}

		// Add a handler to send the name to the server
		MyHandler handler = new MyHandler();
		view.getSendTestEmailClick().addClickHandler(handler);
		view.getTestEmailKeyUp().addKeyUpHandler(handler);

		panel.setWidget(view.asWidget());
	}

	public void setOtherEnabled(CreateCircleView view, boolean enabled) {
		view.getEmailEnable().setEnabled(enabled);
		view.getFirstNameEnable().setEnabled(enabled);
		view.getLastNameEnable().setEnabled(enabled);

		view.getCenterOtherPanelVisible().setVisible(enabled);
	}
}
