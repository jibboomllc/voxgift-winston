package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.Widget;

public class GoodbyeViewImpl extends Composite implements GoodbyeView
{
	private static GoodbyeViewImplUiBinder uiBinder = GWT.create(GoodbyeViewImplUiBinder.class);

	interface GoodbyeViewImplUiBinder extends UiBinder<Widget, GoodbyeViewImpl> {
	}

	@UiField HTML createCircleResults;
	@UiField HTML registrationReminder;
	@UiField HTML invitationReminder;

	public GoodbyeViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public HasHTML getCreateCircleResults() {
		return createCircleResults;
	}
	
	@Override
	public HasVisibility getRegistrationReminderVisibility() {
		return registrationReminder;
	}

	@Override
	public HasVisibility getInvitationReminderVisibility() {
		return invitationReminder;
	}
	
}
