package com.voxgift.winstonplus.account.client.ui;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.dom.client.HasKeyUpHandlers;
import com.google.gwt.user.client.ui.Focusable;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasHTML;
import com.google.gwt.user.client.ui.HasText;

public interface AdminView extends DecoratedView
{
	public HasHTML getGreetingHTML();
	
	public Focusable getTestEmailFocus();
	public HasText getTestEmailText();
	public HasKeyUpHandlers getTestEmailKeyUp();

	public HasText getErrorText();

	public HasClickHandlers getSendTestEmailClick();
	public Focusable getSendTestEmailFocus();
	public HasEnabled getSendTestEmailEnable();
}