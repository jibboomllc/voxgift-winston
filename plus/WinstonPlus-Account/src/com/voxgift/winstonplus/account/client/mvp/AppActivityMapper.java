package com.voxgift.winstonplus.account.client.mvp;

import com.google.gwt.activity.shared.Activity;
import com.google.gwt.activity.shared.ActivityMapper;
import com.google.gwt.place.shared.Place;
import com.voxgift.winstonplus.account.client.ClientFactory;
import com.voxgift.winstonplus.account.client.Track;
import com.voxgift.winstonplus.account.client.activity.AdminActivity;
import com.voxgift.winstonplus.account.client.activity.CreateCircleActivity;
import com.voxgift.winstonplus.account.client.activity.GoodbyeActivity;
import com.voxgift.winstonplus.account.client.activity.LegalActivity;
import com.voxgift.winstonplus.account.client.activity.LoginActivity;
import com.voxgift.winstonplus.account.client.activity.RegisterActivity;
import com.voxgift.winstonplus.account.client.activity.WelcomeActivity;
import com.voxgift.winstonplus.account.client.place.AdminPlace;
import com.voxgift.winstonplus.account.client.place.CreateCirclePlace;
import com.voxgift.winstonplus.account.client.place.GoodbyePlace;
import com.voxgift.winstonplus.account.client.place.LegalPlace;
import com.voxgift.winstonplus.account.client.place.LoginPlace;
import com.voxgift.winstonplus.account.client.place.RegisterPlace;
import com.voxgift.winstonplus.account.client.place.WelcomePlace;

public class AppActivityMapper implements ActivityMapper {

	private ClientFactory clientFactory;

	/**
	 * AppActivityMapper associates each Place with its corresponding
	 * {@link Activity}
	 * 
	 * @param clientFactory
	 *            Factory to be passed to activities
	 */
	public AppActivityMapper(ClientFactory clientFactory) {
		super();
		this.clientFactory = clientFactory;
	}

 	private AdminActivity adminActivity;
	private AdminActivity getAdminActivity(AdminPlace place) {
		if (adminActivity == null) {
			adminActivity = new AdminActivity(place, clientFactory);
		}
		return adminActivity;
	}

 	private CreateCircleActivity createCircleActivity;
	private CreateCircleActivity getCreateCircleActivity(CreateCirclePlace place) {
		if (createCircleActivity == null) {
			createCircleActivity = new CreateCircleActivity(place, clientFactory);
		}
		return createCircleActivity;
	}

 	private GoodbyeActivity goodbyeActivity;
	private GoodbyeActivity getGoodbyeActivity(GoodbyePlace place) {
		if (goodbyeActivity == null) {
			goodbyeActivity = new GoodbyeActivity(place, clientFactory);
		}
		return goodbyeActivity;
	}

 	private LegalActivity legalActivity;
	private LegalActivity getLegalActivity(LegalPlace place) {
		if (legalActivity == null) {
			legalActivity = new LegalActivity(place, clientFactory);
		} else {
			legalActivity.setPlace(place);
		}
		return legalActivity;
	}

 	private LoginActivity loginActivity;
	private LoginActivity getLoginActivity(LoginPlace place) {
		if (loginActivity == null) {
			loginActivity = new LoginActivity(place, clientFactory);
		}
		return loginActivity;
	}

 	private RegisterActivity registerActivity;
	private RegisterActivity getRegisterActivity(RegisterPlace place) {
		if (registerActivity == null) {
			registerActivity = new RegisterActivity(place, clientFactory);
		}
		return registerActivity;
	}

 	private WelcomeActivity welcomeActivity;
	private WelcomeActivity getWelcomeActivity(WelcomePlace place) {
		if (welcomeActivity == null) {
			welcomeActivity = new WelcomeActivity(place, clientFactory);
		}
		return welcomeActivity;
	}
	
	/**
	 * Map each Place to its corresponding Activity. This would be a great use
	 * for GIN.
	 */
	@Override
	public Activity getActivity(Place place) {
		String placeNameParts[] = place.getClass().getName().split("\\.");
		Track.trackPageview(placeNameParts[placeNameParts.length - 1]);
		
		// This is begging for GIN
		if (place instanceof AdminPlace)
			return getAdminActivity((AdminPlace) place);
		else if (place instanceof CreateCirclePlace)
			return getCreateCircleActivity((CreateCirclePlace) place);
		else  if (place instanceof GoodbyePlace)
			return getGoodbyeActivity((GoodbyePlace) place);
		else if (place instanceof LegalPlace)
			return getLegalActivity((LegalPlace) place);
		else if (place instanceof LoginPlace)
			return getLoginActivity((LoginPlace) place);
		else if (place instanceof RegisterPlace)
			return getRegisterActivity((RegisterPlace) place);
		else if (place instanceof WelcomePlace)
			return getWelcomeActivity((WelcomePlace) place);

		return null;
	}

}
