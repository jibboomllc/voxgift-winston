package com.voxgift.winstonplus.account.client;

import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.google.gwt.place.shared.PlaceController;
import com.voxgift.winstonplus.account.client.ui.AdminView;
import com.voxgift.winstonplus.account.client.ui.AdminViewImpl;
import com.voxgift.winstonplus.account.client.ui.CreateCircleView;
import com.voxgift.winstonplus.account.client.ui.CreateCircleViewImpl;
import com.voxgift.winstonplus.account.client.ui.GoodbyeView;
import com.voxgift.winstonplus.account.client.ui.GoodbyeViewImpl;
import com.voxgift.winstonplus.account.client.ui.LegalView;
import com.voxgift.winstonplus.account.client.ui.LegalViewImpl;
import com.voxgift.winstonplus.account.client.ui.LoginView;
import com.voxgift.winstonplus.account.client.ui.LoginViewImpl;
import com.voxgift.winstonplus.account.client.ui.RegisterView;
import com.voxgift.winstonplus.account.client.ui.RegisterViewImpl;
import com.voxgift.winstonplus.account.client.ui.WelcomeView;
import com.voxgift.winstonplus.account.client.ui.WelcomeViewImpl;

public class ClientFactoryImpl implements ClientFactory
{
	private static final EventBus eventBus = new SimpleEventBus();
	private static final PlaceController placeController = new PlaceController(eventBus);

	@Override
	public EventBus getEventBus()
	{
		return eventBus;
	}

	@Override
	public PlaceController getPlaceController()
	{
		return placeController;
	}

	@Override
	public String getSponsorId()
	{
		return "9acdfb83-845a-4c3e-8419-237098064b7f";
	}
	
	private static final AdminView adminView = new AdminViewImpl();

	@Override
	public AdminView getAdminView()
	{
		return adminView;
	}

	private static final CreateCircleView createCircleView = new CreateCircleViewImpl();

	@Override
	public CreateCircleView getCreateCircleView()
	{
		return createCircleView;
	}

	private static final GoodbyeView goodbyeView = new GoodbyeViewImpl();

	@Override
	public GoodbyeView getGoodbyeView()
	{
		return goodbyeView;
	}

	private static final LegalView legalView = new LegalViewImpl();

	@Override
	public LegalView getLegalView()
	{
		return legalView;
	}

	private static final LoginView loginView = new LoginViewImpl();

	@Override
	public LoginView getLoginView()
	{
		return loginView;
	}

	private static final RegisterView registerView = new RegisterViewImpl();

	@Override
	public RegisterView getRegisterView()
	{
		return registerView;
	}

	private static final WelcomeView welcomeView = new WelcomeViewImpl();

	@Override
	public WelcomeView getWelcomeView()
	{
		return welcomeView;
	}

}
