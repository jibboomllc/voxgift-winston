package com.voxgift.winstonplus.account.client.place;

import com.google.gwt.place.shared.Place;
import com.google.gwt.place.shared.PlaceTokenizer;

public class AdminPlace extends Place
{

	public static class Tokenizer implements PlaceTokenizer<AdminPlace>
	{
		@Override
		public String getToken(AdminPlace place)
		{
			return "";
		}

		@Override
		public AdminPlace getPlace(String token)
		{
			return new AdminPlace();
		}
	}
	
}
