package com.voxgift.winstonplus.account.client.activity;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.core.client.Scheduler.ScheduledCommand;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.voxgift.winstonplus.account.client.ClientFactory;
import com.voxgift.winstonplus.account.client.Track;
import com.voxgift.winstonplus.account.client.place.CreateCirclePlace;
import com.voxgift.winstonplus.account.client.place.GoodbyePlace;
import com.voxgift.winstonplus.account.client.ui.CreateCircleView;
import com.voxgift.winstonplus.data.shared.FieldVerifier;
import com.voxgift.winstonplus.data.shared.model.CircleStatus;
import com.voxgift.winstonplus.data.shared.model.CircleUserRole;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcCircleService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcCircleServiceAsync;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcMessageService;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcMessageServiceAsync;

public class CreateCircleActivity extends DecoratedActivity {
	//private java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(getClass().getName());

	private boolean busy;
	private String circleId;
	private String[] memberEmails;

	private final RpcCircleServiceAsync circleSvc = GWT.create(RpcCircleService.class);

	private final RpcMessageServiceAsync messageSvc = GWT.create(RpcMessageService.class);

	public CreateCircleActivity(CreateCirclePlace place, ClientFactory clientFactory) {
		super(clientFactory, clientFactory.getCreateCircleView());
	}

	/**
	 * Invoked by the ActivityManager to start a new Activity
	 */
	@Override
	public void start(AcceptsOneWidget panel, EventBus eventBus) {
		super.start(panel, eventBus);
		final CreateCircleView view = clientFactory.getCreateCircleView();

		setErrorText(null);

		String ownerName = new String();
		if (Cookies.getCookie("firstName") != null && !Cookies.getCookie("firstName").isEmpty()) {
			ownerName = ownerName.concat(Cookies.getCookie("firstName"));
		}
		if (Cookies.getCookie("lastName") != null && !Cookies.getCookie("lastName").isEmpty()) {
			if (!ownerName.isEmpty()) {
				ownerName = ownerName.concat(" ");
			}
			ownerName = ownerName.concat(Cookies.getCookie("lastName"));
		}
		if (ownerName.isEmpty()) {
			ownerName = ownerName.concat(Cookies.getCookie("email"));
		}

		if (Cookies.getCookie("newUser") == null || Cookies.getCookie("newUser").equals("0")) {
			view.getGreetingHTML().setHTML("<p>Welcome back, <strong>" + ownerName + "</strong>.</p>");
		} else {
			view.getGreetingHTML().setHTML("<p>Welcome, <strong>" + ownerName 
					+ "</strong>. Please click on the confirmation link in the registration email from Voxgift Support.</p>");
		}

		Scheduler.get().scheduleDeferred(new ScheduledCommand() {
	        public void execute () {
	    		view.getNameFocus().setFocus(true);
	        }
	    });

		view.getCenterOtherValue().setValue(true);
		setOtherEnabled(view, true);

		addHandlerRegistration(view.getNameKeyUp().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					Track.trackEvent("CreateCircleActivity", "enterKey", "name");

					if (!FieldVerifier.isValidName(view.getNameText().getText())) {
						setErrorText("Circle name does not validate: "
								+ FieldVerifier.validNameDescription("en", "US") + ". ");
					} else {
						setErrorText(null);
						if (view.getCenterOtherPanelVisible().isVisible()) {
							view.getEmailFocus().setFocus(true);
						} else {
							view.getMembersFocus().setFocus(true);
						}
					}
				}
			}
		}));


		addHandlerRegistration(view.getEmailKeyUp().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					Track.trackEvent("CreateCircleActivity", "enterKey", "email");
					if (!FieldVerifier.isValidEmailAddress(view.getEmailText().getText().toLowerCase().trim())) {
						setErrorText ("Email address does not validate: "
								+ FieldVerifier.validEmailAddressDescription("en", "US") + ". ");
					} else {
						setErrorText(null);
						view.getFirstNameFocus().setFocus(true);
					}
				}
			}
		}));

		addHandlerRegistration(view.getFirstNameKeyUp().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					Track.trackEvent("CreateCircleActivity", "enterKey", "firstName");
					view.getLastNameFocus().setFocus(true);
				}
			}
		}));

		addHandlerRegistration(view.getLastNameKeyUp().addKeyUpHandler(new KeyUpHandler() {
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					Track.trackEvent("CreateCircleActivity", "enterKey", "lastName");
					view.getMembersFocus().setFocus(true);
				}
			}
		}));

		addHandlerRegistration(view.getCenterMeClick().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Track.trackEvent("CreateCircleActivity", "click", "centerMe");
				setOtherEnabled(view, false);
			}
		}));

		addHandlerRegistration(view.getCenterOtherClick().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Track.trackEvent("CreateCircleActivity", "click", "centerOther");
				setOtherEnabled(view, true);
			}
		}));

		addHandlerRegistration(view.getSkipClick().addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				Track.trackEvent("CreateCircleActivity", "click", "skip");
				clientFactory.getPlaceController().goTo(new GoodbyePlace());
			}
		}));

		class MyHandler implements ClickHandler, KeyUpHandler {
			public void onClick(ClickEvent event) {
				if (!busy) {
					Track.trackEvent("CreateCircleActivity", "click", "create");
					sendDataToServer();
				}
			}

			public void onKeyUp(KeyUpEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER && !busy) {
					sendDataToServer();
				}
			}

			private void setWorking(boolean working) {
				busy = working;
				view.getCreateEnable().setEnabled(!working);
				view.getSkipEnable().setEnabled(!working);
			}

			private void sendDataToServer() {
				setWorking(true);
				circleId = null;
				if (!view.getMembersText().getText().isEmpty()) {
					memberEmails =  view.getMembersText().getText().toLowerCase().split("\\s+");
				}

				// First, we validate the input.
				setStatusText("Validating data");
				String errorText = new String();

				if (!FieldVerifier.isValidName(view.getNameText().getText())) {
					errorText = errorText.concat("Circle name does not validate: "
							+ FieldVerifier.validNameDescription("en", "US") + ". ");
				}

				if (view.getCenterOtherValue().getValue()
						&& !FieldVerifier.isValidEmailAddress(view.getEmailText().getText().toLowerCase().trim())) {
					errorText = errorText.concat("Email address [" 
							+ view.getEmailText().getText().toLowerCase().trim() + "] does not validate: "
							+ FieldVerifier.validEmailAddressDescription("en", "US") + ". ");
				}

				if (memberEmails != null) {
					for (int i = 0;  i < memberEmails.length; i++) {
						if (!FieldVerifier.isValidEmailAddress(memberEmails[i])) {
							errorText = errorText.concat("Email address [" 
									+ memberEmails[i] + "] does not validate: ");
						}
					}
				}

				if (!errorText.isEmpty()) {
					setErrorText(errorText);
					setWorking(false);
					return;
				}

				final String welcomeMessage = "Welcome to " + view.getNameText().getText();

				final AsyncCallback<String> postMessageCallback = new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						setErrorText("Server error posting message");
						setWorking(false);
					}

					public void onSuccess(String result) {
						if (result == null) {
							setErrorText("Error posting message");
							setWorking(false);
						} else {
							clientFactory.getPlaceController().goTo(new GoodbyePlace());
							setStatusText(null);
							setWorking(false);								
						}
					}
				};

				final AsyncCallback<Integer> inviteEmailsCallback = new AsyncCallback<Integer>() {
					public void onFailure(Throwable caught) {
						setErrorText("Server error inviting members");
						setWorking(false);
					}

					public void onSuccess(Integer result) {
						if (result == null || result < memberEmails.length) {
							setErrorText("Error inviting members");
							setWorking(false);
						} else {
							setStatusText("Posting welcome message");
							messageSvc.postMessage(circleId, Cookies.getCookie("userId"), welcomeMessage, postMessageCallback);
						}
					}
				};

				final AsyncCallback<String> inviteCallback = new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						setErrorText("Server error inviting user");
						setWorking(false);
					}

					public void onSuccess(String result) {
						if (result == null) {
							setErrorText("Error inviting user");
							setWorking(false);
						} else if (memberEmails != null && memberEmails.length > 0) {
							setStatusText("Inviting members");
							circleSvc.inviteUsersByEmails(circleId, memberEmails
									, CircleUserRole.FAMILY, inviteEmailsCallback);
						} else {
							setStatusText("Posting welcome message");
							messageSvc.postMessage(circleId, Cookies.getCookie("userId"), welcomeMessage, postMessageCallback);
						}
					}
				};

				final AsyncCallback<String> createCircleCallback = new AsyncCallback<String>() {
					public void onFailure(Throwable caught) {
						setErrorText("Server error creating circle");
						setWorking(false);
					}

					public void onSuccess(String result) {
						if (result == null) {
							setErrorText("Error creating circle");
							setWorking(false);
						} else {
							circleId = result;
							Cookies.setCookie("circleName", view.getNameText().getText());
							if (view.getCenterOtherValue().getValue()) {
								setStatusText("Inviting circle center");
								circleSvc.inviteUser(circleId, null
										, view.getEmailText().getText().toLowerCase().trim()
										, view.getFirstNameText().getText(), view.getLastNameText().getText()
										, CircleUserRole.CENTER, inviteCallback);
							} else if (memberEmails != null && memberEmails.length > 0) {
								setStatusText("Inviting members");
								circleSvc.inviteUsersByEmails(circleId, memberEmails
										, CircleUserRole.FAMILY, inviteEmailsCallback);
							} else {
								setStatusText("Posting welcome message");
								messageSvc.postMessage(circleId, Cookies.getCookie("userId"), welcomeMessage, postMessageCallback);
							}
						}
					}
				};

				setWorking(true);
				setStatusText("Creating circle");
				int ownerRoleId = view.getCenterOtherValue().getValue() ? CircleUserRole.FAMILY : CircleUserRole.CENTER;

				circleSvc.createCircle(Cookies.getCookie("userId"), view.getNameText().getText()
						, CircleStatus.OPEN, ownerRoleId, clientFactory.getSponsorId(), createCircleCallback);
			}
		}

		// Add a handler to send the name to the server
		MyHandler handler = new MyHandler();
		view.getCreateClick().addClickHandler(handler);

		panel.setWidget(view.asWidget());
	}

	public void setOtherEnabled(CreateCircleView view, boolean enabled) {
		view.getEmailEnable().setEnabled(enabled);
		view.getFirstNameEnable().setEnabled(enabled);
		view.getLastNameEnable().setEnabled(enabled);

		view.getCenterOtherPanelVisible().setVisible(enabled);
	}
}
