package com.voxgift.winstonplus.account.client.activity;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.web.bindery.event.shared.EventBus;
import com.voxgift.winstonplus.account.client.ClientFactory;
import com.voxgift.winstonplus.account.client.ui.DecoratedView;

public class DecoratedActivity extends VGAbstractActivity {

	protected ClientFactory clientFactory;

	private final DecoratedView decoratedView;

	public DecoratedActivity(ClientFactory clientFactory, DecoratedView decoratedView) {
		this.clientFactory = clientFactory;
		this.decoratedView = decoratedView;
	}

	public void start(AcceptsOneWidget panel, final EventBus eventBus) {
		decoratedView.getStatusText().setText(null);
		decoratedView.getStatusVisibility().setVisible(false);
		decoratedView.getErrorText().setText(null);
		decoratedView.getErrorVisibility().setVisible(false);
	}
	
	public void setStatusText(String text) {
		decoratedView.getStatusText().setText(text);
		decoratedView.getStatusVisibility().setVisible(text != null);
		decoratedView.getErrorText().setText(null);
		decoratedView.getErrorVisibility().setVisible(false);
	}

	public void setStatusText(String text, int delayMsec) {
		setStatusText(text);
		new Timer() {
			@Override
			public void run() {
				decoratedView.getStatusVisibility().setVisible(false);
				decoratedView.getStatusText().setText(null);
			}
		}.schedule(delayMsec);
	}

	public void setErrorText(String text) {
		decoratedView.getStatusText().setText(null);
		decoratedView.getStatusVisibility().setVisible(false);
		decoratedView.getErrorText().setText(text);
		decoratedView.getErrorVisibility().setVisible(text != null);
	}

}
