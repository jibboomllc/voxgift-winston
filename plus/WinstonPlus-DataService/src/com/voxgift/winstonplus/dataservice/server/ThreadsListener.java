/**
 * Class used to hook into the servlet init/destroy. Needed to
 * address mysql driver issue http://bugs.mysql.com/bug.php?id=65909
 * Which shows up in logs/catalina.out as:
 *   SEVERE: The web application [xxx] appears to have started a thread 
 *   named [Abandoned connection cleanup thread] but has failed to stop it. 
 *   This is very likely to create a memory leak.
 * 
 * Need to add to web.xml:
 * 
 *	<listener>
 *		<listener-class>com.voxgift.winstonplus.server.ThreadsListener</listener-class>
 *	</listener>
 * 
 * Per http://bugs.mysql.com/bug.php?id=68556 make sure to enable 
 * Tomcat's JreMemoryLeakPreventionListener (enabled by default on Tomcat 7),
 * and add this attribute to the <Listener> element (/Library/Tomcat/Home/conf/server.xml):
 *   classesToInitialize="com.mysql.jdbc.NonRegisteringDriver"
 */
package com.voxgift.winstonplus.dataservice.server;

import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import com.mysql.jdbc.AbandonedConnectionCleanupThread;

public class ThreadsListener implements ServletContextListener {
	
	private static final Logger LOGGER = Logger.getLogger(ThreadsListener.class.getName());

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		LOGGER.info("ServletContextListener initialized");
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		long id = 0;
		
		try {
			id = AbandonedConnectionCleanupThread.currentThread().getId();
		    AbandonedConnectionCleanupThread.shutdown();
		} catch (InterruptedException e) {
			LOGGER.warning("SEVERE problem cleaning up: " + e.getMessage());
		    e.printStackTrace();
		}
		
		// give the thread some time to finish
		
		try {
		    Thread.sleep(5000);
		} catch (InterruptedException e) {
			LOGGER.warning(e.getMessage());
		}
		
		LOGGER.info("ServletContextListener destroyed [" + id + "]");
	}

}
