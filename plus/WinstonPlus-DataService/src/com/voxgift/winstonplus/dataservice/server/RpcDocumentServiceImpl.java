package com.voxgift.winstonplus.dataservice.server;

import java.util.List;

import com.voxgift.winstonplus.data.client.rpc.DocumentService;
import com.voxgift.winstonplus.data.server.DocumentServiceImpl;
import com.voxgift.winstonplus.data.shared.model.DocumentDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDocumentService;

@SuppressWarnings("serial")
public class RpcDocumentServiceImpl extends ContextRemoteServiceServlet implements RpcDocumentService {

	DocumentService documentService = new DocumentServiceImpl();

	@Override
	public List<DocumentDetails> getDocuments(final String language, final String locale, final int typeId) {
		return documentService.getDocuments(language, locale, typeId);
	}

	@Override
	public String getDocument(final String documentId, final String language, final String locale) {
		return documentService.getDocument(documentId, language, locale);
	}
	
}
