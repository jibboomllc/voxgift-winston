package com.voxgift.winstonplus.dataservice.server;

import java.util.List;
import java.util.Map;
import com.voxgift.winstonplus.data.client.rpc.DictionaryService;
import com.voxgift.winstonplus.data.server.DictionaryServiceImpl;
import com.voxgift.winstonplus.data.shared.model.DictionaryDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcDictionaryService;

@SuppressWarnings("serial")
public class RpcDictionaryServiceImpl extends ContextRemoteServiceServlet implements RpcDictionaryService {

	DictionaryService dictionaryService = new DictionaryServiceImpl();

	@Override
	public List<DictionaryDetails> getDictionaries(String languageId,
			String localeId) {
		return dictionaryService.getDictionaries(languageId, localeId);
	}

	@Override
	public DictionaryDetails getFavoritesDictionary(String userId,
			String languageId, String localeId) {
		return dictionaryService.getFavoritesDictionary(userId, languageId, localeId);
	}

	@Override
	public String lookupTranslation(String id, String language, String locale) {
		return dictionaryService.lookupTranslation(id, language, locale);
	}

	@Override
	public Integer logUsage(String id, String language, String locale,
			String gender) {
		return dictionaryService.logUsage(id, language, locale, gender);
	}

	@Override
	public String createAdhocEntry(String langOut, String locOut,
			String messageText, String userId) {
		return dictionaryService.createAdhocEntry(langOut, locOut, messageText, userId);
	}

	@Override
	public String lookupAdhocEntry(String id, String language, String locale) {
		return dictionaryService.lookupAdhocEntry(id, language, locale);
	}

	@Override
	public String lookupAdhocEntryByText(String messageText, String language,
			String locale) {
		return dictionaryService.lookupAdhocEntryByText(messageText, language, locale);
	}

	@Override
	public Integer logAdhocEntryUsage(String id, String language,
			String locale, String gender) {
		return dictionaryService.logAdhocEntryUsage(id, language, locale, gender);
	}

	@Override
	public String addFavorite(String messageText, String userId,
			String language, String locale) {
		return dictionaryService.addFavorite(messageText, userId, language, locale);
	}

	@Override
	public Boolean setFavoritesSortOrder(Map<String, Integer> entries) {

		return dictionaryService.setFavoritesSortOrder(entries);
	}

	@Override
	public Boolean setFavoriteSortOrder(String id, int sortOrder) {

		return dictionaryService.setFavoriteSortOrder(id, sortOrder);
	}

	@Override
	public Boolean deleteFavorite(String id) {

		return dictionaryService.deleteFavorite(id);
	}

	@Override
	public Integer logFavoritesEntryUsage(String id) {

		return dictionaryService.logFavoritesEntryUsage(id);
	}
}
