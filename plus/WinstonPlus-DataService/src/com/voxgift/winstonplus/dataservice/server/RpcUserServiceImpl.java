package com.voxgift.winstonplus.dataservice.server;

import java.util.List;
import java.util.Map;
import com.voxgift.winstonplus.data.client.rpc.UserService;
import com.voxgift.winstonplus.data.server.UserServiceImpl;
import com.voxgift.winstonplus.data.shared.model.Circle;
import com.voxgift.winstonplus.data.shared.model.UserCircleDetails;
import com.voxgift.winstonplus.data.shared.model.UserDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcUserService;

@SuppressWarnings("serial")
public class RpcUserServiceImpl extends ContextRemoteServiceServlet implements RpcUserService {

	UserService userService = new UserServiceImpl();

	@Override
	public Boolean ping() {
		return userService.ping();
	}

	@Override
	public Boolean sendTestEmail(String email) {
		return userService.sendTestEmail(email);
	}

	@Override
	public String login(String email, String password) {
		return userService.login(email, password);
	}

	@Override
	public String loginWithTempPassword(String email, String password) {
		return userService.loginWithTempPassword(email, password);
	}

	@Override
	public String loginFromSession(String sid) {
		return userService.loginFromSession(sid);
	}

	@Override
	public Boolean associateUserWithPushRegistration(String userId,
			String regId, int pushServiceTypeId) {
		return userService.associateUserWithPushRegistration(userId, regId, pushServiceTypeId);
	}

	@Override
	public Boolean disassociateUserWithPushRegistration(String userId,
			String regId, int pushServiceTypeId) {
		return userService.disassociateUserWithPushRegistration(userId, regId, pushServiceTypeId);
	}

	@Override
	public Boolean disassociateUserWithAllPushRegistrations(String userId) {
		return userService.disassociateUserWithAllPushRegistrations(userId);
	}

	@Override
	public Boolean expireOtherUserSessions(String userId, String sid) {
		return userService.expireOtherUserSessions(userId, sid);
	}

	@Override
	public Boolean logout(String sid) {
		return userService.logout(sid);
	}

	@Override
	public Boolean createTempPassword(String email) {
		return userService.createTempPassword(email);
	}

	@Override
	public String lookupIdByEmail(String email) {
		return userService.lookupIdByEmail(email);
	}

	@Override
	public Boolean register(String email, String password) {
		return userService.register(email, password);
	}

	@Override
	public Boolean register(String email, String password, String firstName,
			String lastName) {
		return userService.register(email, password, firstName, lastName);
	}

	@Override
	public UserDetails getUser(String sid) {
		return userService.getUser(sid);
	}

	@Override
	public Map<String, UserCircleDetails> getUsersCircleSubscriptions(
			String userId) {
		return userService.getUsersCircleSubscriptions(userId);
	}

	@Override
	public List<Circle> getUsersCircles(String userId) {
		return userService.getUsersCircles(userId);
	}

	@Override
	public Boolean updateUser(String id, String primaryEmail, String password,
			String firstName, String lastName, String language, String locale,
			String gender) {
		return userService.updateUser(id, primaryEmail, password, firstName, lastName, language, locale, gender);
	}
}
