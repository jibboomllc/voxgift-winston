package com.voxgift.winstonplus.dataservice.server;

import com.voxgift.winstonplus.data.client.rpc.CircleService;
import com.voxgift.winstonplus.data.server.CircleServiceImpl;
import com.voxgift.winstonplus.data.shared.model.CircleDetails;
import com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcCircleService;

@SuppressWarnings("serial")
public class RpcCircleServiceImpl extends ContextRemoteServiceServlet implements RpcCircleService {

	CircleService circleService = new CircleServiceImpl();

	@Override
	public CircleDetails getCircle(String circleId) {
		return circleService.getCircle(circleId);
	}

	@Override
	public String createCircle(String ownerId, String name, int statusId, int ownerRoleId) {
		return circleService.createCircle(ownerId, name, statusId, ownerRoleId);
	}

	@Override
	public String createCircle(String ownerId, String name, int statusId, int ownerRoleId, String sponsorId) {
		return circleService.createCircle(ownerId, name, statusId, ownerRoleId, sponsorId);
	}

	@Override
	public Boolean updateCircleName(String circleId, String name) {
		return circleService.updateCircleName(circleId, name);
	}

	@Override
	public Boolean updateCircleStatus(String circleId, int statusId) {
		return circleService.updateCircleStatus(circleId, statusId);
	}

	@Override
	public CircleUserStatusDetails getCircleUserStatus(String circleId, String userId) {
		return circleService.getCircleUserStatus(circleId, userId);
	}

	@Override
	public Boolean setCircleUserStatus(String circleId, String userId, int statusId) {
		return circleService.setCircleUserStatus(circleId, userId, statusId);
	}

	@Override
	public Boolean updateUserRole(String circleId, String userId, int roleId) {
		return circleService.updateUserRole(circleId, userId, roleId);
	}

	@Override
	public String inviteUser(String circleId, String userId, String userEmail, String firstName, String lastName, int roleId) {
		return circleService.inviteUser(circleId, userId, userEmail, firstName, lastName, roleId);
	}

	@Override
	public Integer inviteUsersByEmails(String circleId, String[] emails, int roleId) {
		return circleService.inviteUsersByEmails(circleId, emails, roleId);
	}
}
