package com.voxgift.winstonplus.dataservice.server;

import javax.servlet.http.HttpServletRequest;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.google.gwt.user.server.rpc.SerializationPolicy;
 
/**
 * ContextRemoteServiceServlet is an extension of RemoteServiceServlet that
 * loads the RPC serialization policy based on the context name rather than URL if it is
 * not successfully loaded using the GWT's default method. This is useful if
 * you're proxying requests to RPC servlets from one server to another and the
 * paths to the servlets don't match perfectly (causing the RPC policy file to fail to load).
 *
 *
 * In my apache config I added...

ProxyPass /app/ ajp://localhost:8009/App-0.0.1-SNAPSHOT/
RequestHeader edit X-GWT-Module-Base ^(.*)/app/(.*)$ $1/App-0.0.1-SNAPSHOT/$2

INFO: userServiceServlet: ERROR: The module path requested,
 /c/WinstonPlus-1.0-SNAPSHOT/WinstonPlus/
, is not in the same web application as this servlet,
 /WinstonPlus-1.0-SNAPSHOT
.  Your module may not be properly configured or your client and server code maybe out of date.
Jun 21, 2013 8:33:05 AM org.apache.catalina.core.ApplicationContext log
INFO: userServiceServlet: WARNING: Failed to get the SerializationPolicy
 '7DDADEDF081294DA91CE5F1E38F05744'
  for module
   'http://192.168.1.103/c/WinstonPlus-1.0-SNAPSHOT/WinstonPlus/'
    ; a legacy, 1.3.3 compatible, serialization policy will be used.
  You may experience SerializationExceptions as a result.

ProxyPass        /c/WinstonPlus-1.0-SNAPSHOT/WinstonPlus/rpc balancer://mycluster/WinstonPlus-1.0-SNAPSHOT/WinstonPlus/rpc
ProxyPassReverse /c/WinstonPlus-1.0-SNAPSHOT/WinstonPlus/rpc http://192.168.1.103:8080/WinstonPlus-1.0-SNAPSHOT/WinstonPlus/rpc
RequestHeader edit X-GWT-Module-Base ^(.*)/c/WinstonPlus-1.0-SNAPSHOT/(.*) $1/WinstonPlus-1.0-SNAPSHOT/$2

INFO: documentServiceServlet: ERROR: The module path requested, 
   /c/rmhc-austin.org/registration/winstonplus_account/
, is not in the same web application as this servlet, 
   /WinstonPlus-RemoteService-0.1-dev.  
Your module may not be properly configured or your client and server code maybe out of date.

ProxyPass        /winstonplus/rpc balancer://mycluster/WinstonPlus-RemoteService-0.1-dev/winstonplus/rpc
ProxyPassReverse /winstonplus/rpc http://192.168.1.103:8080/WinstonPlus-RemoteService-0.1-dev/winstonplus/rpc
RequestHeader edit X-GWT-Module-Base ^(.*)/c/(.*)/winstonplus_account/(.*)$ $1/WinstonPlus-RemoteService-0.1-dev/$3


 */
public class ContextRemoteServiceServlet extends RemoteServiceServlet {
 
    private static final long serialVersionUID = -4332306688541651819L;

	@Override
	protected SerializationPolicy doGetSerializationPolicy(
	        HttpServletRequest request, String moduleBaseURL, String strongName) {
	    //get the base url from the header instead of the body this way 
	    //apache reverse proxy with rewrite on the header can work
	    String moduleBaseURLHdr = request.getHeader("X-GWT-Module-Base");
		
	    // this.log("X-GWT-Module-Base = " + request.getHeader("X-GWT-Module-Base"));
		// this.log("moduleBaseURL     = " + moduleBaseURL);
	 
	    if(moduleBaseURLHdr != null){
	        moduleBaseURL = moduleBaseURLHdr;
	    }

	    return super.doGetSerializationPolicy(request, moduleBaseURL, strongName);
	}

}