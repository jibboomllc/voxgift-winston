package com.voxgift.winstonplus.dataservice.server;

import java.util.Date;
import com.voxgift.winstonplus.data.client.rpc.MessageService;
import com.voxgift.winstonplus.data.server.MessageServiceImpl;
import com.voxgift.winstonplus.data.shared.model.MessageDetails;
import com.voxgift.winstonplus.dataservice.client.rpc.RpcMessageService;

@SuppressWarnings("serial")
public class RpcMessageServiceImpl extends ContextRemoteServiceServlet implements RpcMessageService {

	MessageService messageService = new MessageServiceImpl();

	@Override
	public MessageDetails[] getMessages(String circleId, Date startDate, Integer limit) {
		return messageService.getMessages(circleId, startDate, limit);
	}

	@Override
	public String postMessage(String circleId, String userId, String messageText) {
		return messageService.postMessage(circleId, userId, messageText);
	}

	@Override
	public String saveMessage(String circleId, String userId, String messageText) {
		return messageService.saveMessage(circleId, userId, messageText);
	}

	@Override
	public Boolean pushMessage(String circleId, String userId, String messageId) {
		return messageService.pushMessage(circleId, userId, messageId);
	}

}
