package com.voxgift.winstonplus.dataservice.client.rpc;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.voxgift.winstonplus.data.shared.model.CircleDetails;
import com.voxgift.winstonplus.data.shared.model.CircleUserStatusDetails;

public interface RpcCircleServiceAsync {

	void getCircle(final String circleId
			, AsyncCallback<CircleDetails> callback);
	void createCircle(final String ownerId, final String name, final int statusId, final int ownerRoleId
			, AsyncCallback<String> callback);
	void createCircle(final String ownerId, final String name, final int statusId, final int ownerRoleId, final String sponsorId
			, AsyncCallback<String> callback);
	void updateCircleName(final String circleId, final String name
			, AsyncCallback<Boolean> callback);
	void updateCircleStatus(final String circleId, final int statusId
			, AsyncCallback<Boolean> callback);

	void getCircleUserStatus(final String circleId, final String userId
			, AsyncCallback<CircleUserStatusDetails> callback);
	void setCircleUserStatus(final String circleId, final String userId, final int statusId 
			, AsyncCallback<Boolean> callback);
	void updateUserRole(final String circleId, final String userId, final int roleId
			, AsyncCallback<Boolean> callback);
	void inviteUser(String circleId, String userId, final String userEmail, final String firstName, final String lastName, final int roleId
			, AsyncCallback<String> callback);
	void inviteUsersByEmails(final String circleId, final String[] emails, final int roleId
			, AsyncCallback<Integer> callback);

}
