package com.voxgift.winstonplus.dataservice.client.rpc;

import java.util.Date;

import com.google.gwt.user.client.rpc.AsyncCallback;

import com.voxgift.winstonplus.data.shared.model.MessageDetails;

public interface RpcMessageServiceAsync {
	void getMessages(String circleId, Date startDate, Integer limit
			, AsyncCallback<MessageDetails[]> callback);
	void postMessage(String circleId, String userId, String messageText
			, AsyncCallback<String> callback);
	void saveMessage(String circleId, String userId, String messageText
			, AsyncCallback<String> callback);
	void pushMessage(String circleId, String userId, String messageId
			, AsyncCallback<Boolean> callback);
}
