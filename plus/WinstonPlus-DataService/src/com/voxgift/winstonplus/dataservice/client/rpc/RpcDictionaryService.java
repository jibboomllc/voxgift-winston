package com.voxgift.winstonplus.dataservice.client.rpc;

import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import com.voxgift.winstonplus.data.shared.model.DictionaryDetails;

@RemoteServiceRelativePath("rpc/dictionary")
public interface RpcDictionaryService extends RemoteService {

	List<DictionaryDetails> getDictionaries(String languageId, String localeId);
	DictionaryDetails getFavoritesDictionary(String userId, String languageId, String localeId);
	String lookupTranslation(String id, String language, String locale);
	Integer logUsage(String id, String language, String locale, String gender);

	String createAdhocEntry(String langOut, String locOut, String messageText, String userId);
	String lookupAdhocEntry(String id, String language, String locale);
	String lookupAdhocEntryByText(String messageText, String language, String locale);
	Integer logAdhocEntryUsage(String id, String language, String locale, String gender);

	String addFavorite(String messageText, String userId, String language, String locale);
	Boolean setFavoritesSortOrder(Map<String, Integer> entries);
	Boolean setFavoriteSortOrder(String id, int sortOrder);
	Boolean deleteFavorite(String id);
	Integer logFavoritesEntryUsage(String id);
}

