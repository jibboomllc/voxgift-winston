package com.voxgift.winstonplus.dataservice.client.rpc;

import java.util.List;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.voxgift.winstonplus.data.shared.model.DocumentDetails;

@RemoteServiceRelativePath("rpc/document")
public interface RpcDocumentService extends RemoteService {
	List<DocumentDetails> getDocuments(final String language, final String locale, final int typeId);
	String getDocument(final String documentId, final String language, final String locale);
}
