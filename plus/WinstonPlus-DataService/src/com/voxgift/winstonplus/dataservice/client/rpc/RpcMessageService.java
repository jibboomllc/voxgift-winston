package com.voxgift.winstonplus.dataservice.client.rpc;

import java.util.Date;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import com.voxgift.winstonplus.data.shared.model.MessageDetails;

@RemoteServiceRelativePath("rpc/message")
public interface RpcMessageService extends RemoteService {

	MessageDetails[] getMessages(String circleId, Date startDate, Integer limit);
	String postMessage(String circleId, String userId, String messageText);
	String saveMessage(String circleId, String userId, String messageText);
	Boolean pushMessage(String circleId, String userId, String messageId);
}
