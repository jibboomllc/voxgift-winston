package com.voxgift.winstonplus.dataservice.client.rpc;

import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.voxgift.winstonplus.data.shared.model.Circle;
import com.voxgift.winstonplus.data.shared.model.UserCircleDetails;
import com.voxgift.winstonplus.data.shared.model.UserDetails;

public interface RpcUserServiceAsync {

	void ping(AsyncCallback<Boolean> callback);

	void sendTestEmail(String email
			, AsyncCallback<Boolean> callback);

	void login(final String email, final String password
			, AsyncCallback<String> callback);

	void loginWithTempPassword(final String email, final String password
			, AsyncCallback<String> callback);

	void loginFromSession(final String sid
			, AsyncCallback<String> callback);

	void associateUserWithPushRegistration(final String userId, final String regId, final int pushServiceTypeId
			, AsyncCallback<Boolean> callback);

	void disassociateUserWithPushRegistration(final String userId, final String regId, final int pushServiceTypeId
			, AsyncCallback<Boolean> callback);

	void disassociateUserWithAllPushRegistrations(final String userId
			, AsyncCallback<Boolean> callback);

	void expireOtherUserSessions(final String userId, final String sid
			, AsyncCallback<Boolean> callback);

	void logout(final String sid
			, AsyncCallback<Boolean> callback);

	void createTempPassword(final String email
			, AsyncCallback<Boolean> callback);

	void lookupIdByEmail(final String email
			, AsyncCallback<String> callback);

	void register(final String email, final String password
			, AsyncCallback<Boolean> callback);
	
	void register(final String email, final String password, final String firstName, final String lastName
			, AsyncCallback<Boolean> callback);

	void getUser(final String sid
			, AsyncCallback<UserDetails> callback);
	void getUsersCircleSubscriptions(final String userId 
			, AsyncCallback<Map<String, UserCircleDetails>> callback);
	void getUsersCircles(final String userId 
			, AsyncCallback<List<Circle>> callback);

//	void insertUser (final String primaryEmail, final String password
//			, final String firstName, final String lastName
//			, final String language, final String locale, final String gender
//			, AsyncCallback<String> callback);
	void updateUser (final String id, final String primaryEmail, final String password
			, final String firstName, final String lastName
			, final String language, final String locale, final String gender
			, AsyncCallback<Boolean> callback);

	//	void updateUserEmails (String id, List<String> emails
	//			, AsyncCallback<Boolean> callback);

}
