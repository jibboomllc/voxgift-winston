package com.voxgift.winstonplus.dataservice.client.rpc;

import java.util.List;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.voxgift.winstonplus.data.shared.model.DocumentDetails;

public interface RpcDocumentServiceAsync {
	void getDocuments(final String language, final String locale, final int typeId
			, AsyncCallback<List<DocumentDetails>> callback);
	void getDocument(final String documentId, final String language, final String locale
			, AsyncCallback<String> callback);
}
