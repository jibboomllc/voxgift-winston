package com.voxgift.winstonplus.dataservice.client.rpc;

import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.voxgift.winstonplus.data.shared.model.Circle;
import com.voxgift.winstonplus.data.shared.model.UserCircleDetails;
import com.voxgift.winstonplus.data.shared.model.UserDetails;

@RemoteServiceRelativePath("rpc/user")
public interface RpcUserService extends RemoteService {
	public Boolean ping();
	public Boolean sendTestEmail(String email);
	public String login(final String email, final String password);
	public String loginWithTempPassword(final String email, final String password);
	public String loginFromSession(final String sid);
	public Boolean associateUserWithPushRegistration(final String userId, final String regId, final int pushServiceTypeId);
	public Boolean disassociateUserWithPushRegistration(final String userId, final String regId, final int pushServiceTypeId);
	public Boolean disassociateUserWithAllPushRegistrations(final String userId);
	public Boolean expireOtherUserSessions(final String userId, final String sid);
	public Boolean logout(final String sid);
	public Boolean createTempPassword(final String email);
	public String lookupIdByEmail(final String email);
	public Boolean register(final String email, final String password);
	public Boolean register(final String email, final String password, final String firstName, final String lastName);
	
	public UserDetails getUser(final String sid);
	public Map<String, UserCircleDetails> getUsersCircleSubscriptions(final String userId);
	public List<Circle> getUsersCircles(final String userId);

//	public String insertUser (final String primaryEmail, final String password
//			, final String firstName, final String lastName
//			, final String language, final String locale, final String gender);
	public Boolean updateUser (final String id, final String primaryEmail, final String password
			, final String firstName, final String lastName
			, final String language, final String locale, final String gender);
//	Boolean updateUserEmails (String id, List<String> emails);
}
