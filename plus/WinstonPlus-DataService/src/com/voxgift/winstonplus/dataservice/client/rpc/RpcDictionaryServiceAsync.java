package com.voxgift.winstonplus.dataservice.client.rpc;

import java.util.List;
import java.util.Map;

import com.google.gwt.user.client.rpc.AsyncCallback;

import com.voxgift.winstonplus.data.shared.model.DictionaryDetails;

public interface RpcDictionaryServiceAsync {
	void getDictionaries(String languageId, String localeId
			, AsyncCallback<List<DictionaryDetails>> callback);
	void getFavoritesDictionary(String userId, String languageId, String localeId
			, AsyncCallback<DictionaryDetails> callback);
	void lookupTranslation(String id, String language, String locale
			, AsyncCallback<String> callback);
	void logUsage(String id, String language, String locale, String gender
			, AsyncCallback<Integer> callback);

	void createAdhocEntry(String langOut, String locOut, String messageText, String userId
			, AsyncCallback<String> callback);
	void lookupAdhocEntry(String id, String language, String locale
			, AsyncCallback<String> callback);
	void lookupAdhocEntryByText(String messageText, String language, String locale
			, AsyncCallback<String> callback);
	void logAdhocEntryUsage(String id, String language, String locale, String gender
			, AsyncCallback<Integer> callback);

	void addFavorite(String messageText, String userId, String language, String locale
			, AsyncCallback<String> callback);
	void setFavoritesSortOrder(Map<String, Integer> entries
			, AsyncCallback<Boolean> callback);
	void setFavoriteSortOrder(String id, int sortOrder
			, AsyncCallback<Boolean> callback);
	void deleteFavorite(String id
			, AsyncCallback<Boolean> callback);
	void logFavoritesEntryUsage(String id
			, AsyncCallback<Integer> callback);
}
