<?php
namespace controller;

class legal extends controller {
    const LEGAL_DOCUMENT_VIEW = "document";
    const DOC_ID_EULA = "99f68786-65e2-4663-b9f2-4ebf5fa4b02f";
    const DOC_ID_PRIVACY = "7934c64c-5233-4cb7-9e58-460fe4143a00";
    
    private $content;

    function __construct($controllerName, $actionName) {
        //$this->controllerName = $controllerName;
        //$this->actionName = $actionName;
        parent::__construct($controllerName, $actionName);
        $this->viewName = self::LEGAL_DOCUMENT_VIEW;
    }

    public function eula($params) {
        $this->getDocument(self::DOC_ID_EULA);
    }

    public function privacy($params) {
        $this->getDocument(self::DOC_ID_PRIVACY);
    }

    private function getDocument($docId) {
        $sql = "SELECT title, content FROM documents WHERE uuid = '" . $docId . "';";
        $statement = $this->getDocumentsDb()->query($sql);
        $row = $statement->fetch(\PDO::FETCH_ASSOC);
        $this->pageTitle = $row['title'];
        $this->content = $row['content'];
        include self::LAYOUT_TEMPLATE;
    }
}
