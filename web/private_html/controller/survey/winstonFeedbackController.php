<?php
namespace controller\survey;

class WinstonFeedbackController extends InputController {
  private $usage;
  private $requestedPhrase;
  private $requestedLanguage;
  private $generalFeedback;
  private $contactInformation;

  function __construct($name) {
    parent::__construct($name);
    $this->path .= '_feedback';

    $this->usage = '';
    $this->requestedPhrase = '';
    $this->requestedLanguage = '';
    $this->generalFeedback = '';
    $this->contactInformation = '';

  }

  function readCookies() {
    parent::readCookies();

    if(isset($_SESSION[$this->path.'_usage'])) {
      $this->usage = $_SESSION[$this->path.'_usage'];
    }
    if(isset($_SESSION[$this->path.'_requestedPhrase'])) {
      $this->requestedPhrase = $_SESSION[$this->path.'_requestedPhrase'];
    }
    if(isset($_SESSION[$this->path.'_requestedLanguage'])) {
      $this->requestedLanguage = $_SESSION[$this->path.'_requestedLanguage'];
    }
    if(isset($_SESSION[$this->path.'_generalFeedback'])) {
      $this->generalFeedback = $_SESSION[$this->path.'_generalFeedback'];
    }
    if(isset($_SESSION[$this->path.'_contactInformation'])) {
      $this->contactInformation = $_SESSION[$this->path.'_contactInformation'];
    }
  }

  function writeCookies() {
    parent::writeCookies();

    $_SESSION[$this->path.'_usage'] = $this->usage;
    $_SESSION[$this->path.'_requestedPhrase'] = $this->requestedPhrase;
    $_SESSION[$this->path.'_requestedLanguage'] = $this->requestedLanguage;
    $_SESSION[$this->path.'_generalFeedback'] = $this->generalFeedback;
    $_SESSION[$this->path.'_contactInformation'] = $this->contactInformation;
  }

////////
  function persistData() {
    $sql = "
      INSERT INTO survey_winston_feedback_v1 (device_id, language_code
        , hw_manufacturer, hw_model, os_version
        , app_version_name, app_package_name, lat, lon
        , usage_fb, languages_fb, phrases_fb
        , general_fb, contact_info
        ) VALUES (?, ?
        , ?, ?, ?
        , ?, ?, ?, ?
        , ?, ?, ?
        , ?, ?
        )
      ";
    $q = \controller\controller::getSurveyDb()->prepare($sql);
    $q->execute(
      array($this->deviceId, $this->languageCode
        , $this->hwManufacturer, $this->hwModel, $this->osVersion
        , $this->appVersionName, $this->appPackageName, $this->lat, $this->lon
        , $this->usage, $this->requestedLanguage, $this->requestedPhrase
        , $this->generalFeedback, $this->contactInformation
      ));

  }
  
  function getResultsColumns() {
    return (array('Submitted', 'OS', 'Usage', 'Languages', 'Phrases', 'General', 'Contact'));
  }
  
  function getResultsQuery() {
    return ('
      SELECT swf.id AS id
      , swf.create_ts AS Submitted
      , swf.usage_fb AS "Usage"
      , swf.languages_fb AS Languages
      , swf.phrases_fb AS Phrases
      , swf.general_fb AS General
      , swf.contact_info AS Contact
      , IF(LENGTH(device_id) > 16, "iOS", CONCAT("Android ", os_version)) AS OS
      FROM survey_winston_feedback_v1 AS swf
      ORDER BY swf.id DESC
      ');
  }

  function getCurrentValue() {
    $currentValue = null;
    switch ($this->currentPage) {
      case 'usage':
        $currentValue = $this->usage;
        break;
      case 'requestedPhrase':
        $currentValue = $this->requestedPhrase;
        break;
      case 'requestedLanguage':
        $currentValue = $this->requestedLanguage;
        break;
      case 'generalFeedback':
        $currentValue = $this->generalFeedback;
        break;
      case 'contactInformation':
        $currentValue = $this->contactInformation;
        break;
      default:
        break;
    }
    return $currentValue;
  }

  function nextPage($params) {
    $newPage = null;
    switch ($this->currentPage) {
      case 'first':
        $newPage = 'usage';
        break;
      case 'usage':
        $this->usage = $params['usage'];
        $newPage = 'requestedPhrase';
        break;
      case 'requestedPhrase':
        $this->requestedPhrase = $params['requestedPhrase'];
        $newPage = 'requestedLanguage';
        break;
      case 'requestedLanguage':
        $this->requestedLanguage = $params['requestedLanguage'];
        $newPage = 'generalFeedback';
        break;
      case 'generalFeedback':
        $this->generalFeedback = $params['generalFeedback'];
        $newPage = 'contactInformation';
        break;
      case 'contactInformation':
        $this->contactInformation = $params['contactInformation'];
        $newPage = 'submit';
        break;
      case 'submit':
        $this->persistData();
        $newPage = 'last';
        break;
      case 'last':
        $this->deleteCookies();
        $newPage = $this->firstPage;
        break;
      default:
        print ("DRATS -- case not defined on Next switch, case=".$this->currentPage);
        break;
    }
    return $newPage;
  }
}
?>
