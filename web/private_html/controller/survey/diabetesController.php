<?php
namespace controller\survey;

class DiabetesController extends InputController {
  private $numberOfTests;
  private $firstReading;
  private $lowReading;
  private $highReading;
  private $ketonesTested;
  private $numberOfInjections;

  function __construct($name) {
    parent::__construct($name);
    $this->path .= '_diabetes';

    $this->numberOfTests = '';
    $this->firstReading = '';
    $this->lowReading = '';
    $this->highReading = '';
    $this->ketonesTested = '';
    $this->numberOfInjections = '';
    
    $this->lowTreatDidNot = FALSE;
    $this->lowTreatJuice = FALSE;
    $this->lowTreatGluTab = FALSE;
    $this->lowTreatMilk = FALSE;
    $this->lowTreatCandy = FALSE;
    $this->lowTreatOther = FALSE;

    $this->sitesArms = FALSE;
    $this->sitesLegs = FALSE;
    $this->sitesButt = FALSE;
    $this->sitesLback = FALSE;
    $this->sitesStomach = FALSE;
    $this->sitesOther = FALSE;

    $this->symptomsShaky = FALSE;
    $this->symptomsLhead = FALSE;
    $this->symptomsSweaty = FALSE;
    $this->symptomsHungry = FALSE;
    $this->symptomsHeadache = FALSE;
  }

  function readCookies() {
    parent::readCookies();

    if(isset($_SESSION[$this->path.'_numberOfTests'])) {
      $this->numberOfTests = $_SESSION[$this->path.'_numberOfTests'];
    }
    if(isset($_SESSION[$this->path.'_firstReading'])) {
      $this->firstReading = $_SESSION[$this->path.'_firstReading'];
    }
    if(isset($_SESSION[$this->path.'_lowReading'])) {
      $this->lowReading = $_SESSION[$this->path.'_lowReading'];
    }
    if(isset($_SESSION[$this->path.'_highReading'])) {
      $this->highReading = $_SESSION[$this->path.'_highReading'];
    }
    if(isset($_SESSION[$this->path.'_ketonesTested'])) {
      $this->ketonesTested = $_SESSION[$this->path.'_ketonesTested'];
    }
    if(isset($_SESSION[$this->path.'_numberOfInjections'])) {
      $this->numberOfInjections = $_SESSION[$this->path.'_numberOfInjections'];
    }

    if(isset($_SESSION[$this->path.'_lowTreatDidNot'])) {
      $this->lowTreatDidNot = $_SESSION[$this->path.'_lowTreatDidNot'];
    }
    if(isset($_SESSION[$this->path.'_lowTreatJuice'])) {
      $this->lowTreatJuice = $_SESSION[$this->path.'_lowTreatJuice'];
    }
    if(isset($_SESSION[$this->path.'_lowTreatGluTab'])) {
      $this->lowTreatGluTab = $_SESSION[$this->path.'_lowTreatGluTab'];
    }
    if(isset($_SESSION[$this->path.'_lowTreatMilk'])) {
      $this->lowTreatMilk = $_SESSION[$this->path.'_lowTreatMilk'];
    }
    if(isset($_SESSION[$this->path.'_lowTreatCandy'])) {
      $this->lowTreatCandy = $_SESSION[$this->path.'_lowTreatCandy'];
    }
    if(isset($_SESSION[$this->path.'_lowTreatOther'])) {
      $this->lowTreatOther = $_SESSION[$this->path.'_lowTreatOther'];
    }

    if(isset($_SESSION[$this->path.'_sitesArms'])) {
      $this->sitesArms = $_SESSION[$this->path.'_sitesArms'];
    }
    if(isset($_SESSION[$this->path.'_sitesLegs'])) {
      $this->sitesLegs = $_SESSION[$this->path.'_sitesLegs'];
    }
    if(isset($_SESSION[$this->path.'_sitesButt'])) {
      $this->sitesButt = $_SESSION[$this->path.'_sitesButt'];
    }
    if(isset($_SESSION[$this->path.'_sitesLback'])) {
      $this->sitesLback = $_SESSION[$this->path.'_sitesLback'];
    }
    if(isset($_SESSION[$this->path.'_sitesStomach'])) {
      $this->sitesStomach = $_SESSION[$this->path.'_sitesStomach'];
    }
    if(isset($_SESSION[$this->path.'_sitesOther'])) {
      $this->sitesOther = $_SESSION[$this->path.'_sitesOther'];
    }

    if(isset($_SESSION[$this->path.'_symptomsShaky'])) {
      $this->symptomsShaky = $_SESSION[$this->path.'_symptomsShaky'];
    }
    if(isset($_SESSION[$this->path.'_symptomsLhead'])) {
      $this->symptomsLhead = $_SESSION[$this->path.'_symptomsLhead'];
    }
    if(isset($_SESSION[$this->path.'_symptomsSweaty'])) {
      $this->symptomsSweaty = $_SESSION[$this->path.'_symptomsSweaty'];
    }
    if(isset($_SESSION[$this->path.'_symptomsHungry'])) {
      $this->symptomsHungry = $_SESSION[$this->path.'_symptomsHungry'];
    }
    if(isset($_SESSION[$this->path.'_symptomsHeadache'])) {
      $this->symptomsHeadache = $_SESSION[$this->path.'_symptomsHeadache'];
    }
  }

  function writeCookies() {
    parent::writeCookies();

    $_SESSION[$this->path.'_numberOfTests'] = $this->numberOfTests;
    $_SESSION[$this->path.'_firstReading'] = $this->firstReading;
    $_SESSION[$this->path.'_lowReading'] = $this->lowReading;
    $_SESSION[$this->path.'_highReading'] = $this->highReading;
    $_SESSION[$this->path.'_ketonesTested'] = $this->ketonesTested;
    $_SESSION[$this->path.'_numberOfInjections'] = $this->numberOfInjections;

    $_SESSION[$this->path.'_lowTreatDidNot'] = $this->lowTreatDidNot;
    $_SESSION[$this->path.'_lowTreatJuice'] = $this->lowTreatJuice;
    $_SESSION[$this->path.'_lowTreatGluTab'] = $this->lowTreatGluTab;
    $_SESSION[$this->path.'_lowTreatMilk'] = $this->lowTreatMilk;
    $_SESSION[$this->path.'_lowTreatCandy'] = $this->lowTreatCandy;
    $_SESSION[$this->path.'_lowTreatOther'] = $this->lowTreatOther;

    $_SESSION[$this->path.'_sitesArms'] = $this->sitesArms;
    $_SESSION[$this->path.'_sitesLegs'] = $this->sitesLegs;
    $_SESSION[$this->path.'_sitesButt'] = $this->sitesButt;
    $_SESSION[$this->path.'_sitesLback'] = $this->sitesLback;
    $_SESSION[$this->path.'_sitesStomach'] = $this->sitesStomach;
    $_SESSION[$this->path.'_sitesOther'] = $this->sitesOther;

    $_SESSION[$this->path.'_symptomsShaky'] = $this->symptomsShaky;
    $_SESSION[$this->path.'_symptomsLhead'] = $this->symptomsLhead;
    $_SESSION[$this->path.'_symptomsSweaty'] = $this->symptomsSweaty;
    $_SESSION[$this->path.'_symptomsHungry'] = $this->symptomsHungry;
    $_SESSION[$this->path.'_symptomsHeadache'] = $this->symptomsHeadache;
  }

////////
  function persistData() {
    $sql = "
      INSERT INTO survey_diabetes_daily_v1 (device_id, language_code, number_of_tests
        , first_reading, low_reading, high_reading
        , ketones_tested, number_of_injections
        , low_treat_did_not, low_treat_juice, low_treat_glu_tab
        , low_treat_milk, low_treat_candy, low_treat_other
        , sites_arms, sites_legs, sites_butt
        , sites_lback, sites_stomach, sites_other
        , symptoms_shaky, symptoms_lhead, symptoms_sweaty
        , symptoms_hungry, symptoms_headache
        ) VALUES (?, ?, ?
        , ?, ?, ?
        , ?, ?
        , ?, ?, ?
        , ?, ?, ?
        , ?, ?, ?
        , ?, ?, ?
        , ?, ?, ?
        , ?, ?
        )
      ";
    $q = \controller\controller::getSurveyDb()->prepare($sql);
    $q->execute(
      array($this->deviceId, $this->languageCode, $this->numberOfTests
        , $this->firstReading, $this->lowReading, $this->highReading
        , $this->ketonesTested, $this->numberOfInjections
        , $this->lowTreatDidNot, $this->lowTreatJuice, $this->lowTreatGluTab
        , $this->lowTreatMilk, $this->lowTreatCandy, $this->lowTreatOther
        , $this->sitesArms, $this->sitesLegs, $this->sitesButt
        , $this->sitesLback, $this->sitesStomach, $this->sitesOther
        , $this->symptomsShaky, $this->symptomsLhead, $this->symptomsSweaty
        , $this->symptomsHungry, $this->symptomsHeadache
      ));
  }
  
  function getResultsColumns() {
    return (array('Submitted', 'Number Of Tests'
      , 'First Reading', 'Low Reading', 'High Reading', 'Ketones Tested'
      , 'Number Of Injections', 'Low Sugar Treatment', 'Sites', 'Symptoms'));
  }
  
  function getResultsQuery() {
    return ('
      SELECT sdd.id AS id
      , sdd.create_ts AS Submitted
      , sdd.number_of_tests AS `Number Of Tests`
      , sdd.first_reading AS `First Reading`
      , sdd.low_reading AS `Low Reading`
      , sdd.high_reading AS `High Reading`
      , sdd.ketones_tested AS `Ketones Tested`
      , sdd.number_of_injections AS `Number Of Injections`
      , TRIM(TRAILING ", " FROM CONCAT(
        IF(sdd.low_treat_did_not, "None, ", "")
        , IF(sdd.low_treat_juice, "Juice, ", "")
        , IF(sdd.low_treat_glu_tab, "GlucoseTab, ", "")
        , IF(sdd.low_treat_milk, "Milk, ", "")
        , IF(sdd.low_treat_candy, "Candy, ", "")
        , IF(sdd.low_treat_other, "Other, ", "")
        )) AS `Low Sugar Treatment`
      , TRIM(TRAILING ", " FROM CONCAT(
        IF(sdd.sites_arms, "Arms, ", "")
        , IF(sdd.sites_legs, "Legs, ", "")
        , IF(sdd.sites_butt, "Butt, ", "")
        , IF(sdd.sites_lback, "Lower back, ", "")
        , IF(sdd.sites_stomach, "Stomach, ", "")
        , IF(sdd.sites_other, "Other, ", "")
        )) AS `Sites`
      , TRIM(TRAILING ", " FROM CONCAT(
        IF(sdd.symptoms_shaky, "Shaky, ", "")
        , IF(sdd.symptoms_lhead, "Light headed, ", "")
        , IF(sdd.symptoms_sweaty, "Sweaty, ", "")
        , IF(sdd.symptoms_hungry, "Hungry, ", "")
        , IF(sdd.symptoms_headache, "Headache, ", "")
        )) AS `Symptoms`
      FROM survey_diabetes_daily_v1 AS sdd
      ORDER BY sdd.id DESC
      ');
  }

  function getCurrentValue() {
    $currentValue = null;
    switch ($this->currentPage) {

      case 'lowTreatments':
        $currentValue = array();
        $currentValue['lowTreatDidNot'] = $this->lowTreatDidNot;
        $currentValue['lowTreatJuice'] = $this->lowTreatJuice;
        $currentValue['lowTreatGluTab'] = $this->lowTreatGluTab;
        $currentValue['lowTreatMilk'] = $this->lowTreatMilk;
        $currentValue['lowTreatCandy'] = $this->lowTreatCandy;
        $currentValue['lowTreatOther'] = $this->lowTreatOther;
        break;
      case 'shotSites':
        $currentValue = array();
        $currentValue['sitesArms'] = $this->sitesArms;
        $currentValue['sitesLegs'] = $this->sitesLegs;
        $currentValue['sitesButt'] = $this->sitesButt;
        $currentValue['sitesLback'] = $this->sitesLback;
        $currentValue['sitesStomach'] = $this->sitesStomach;
        $currentValue['sitesOther'] = $this->sitesOther;
        break;
      case 'symptoms':
        $currentValue = array();
        $currentValue['symptomsShaky'] = $this->symptomsShaky;
        $currentValue['symptomsLhead'] = $this->symptomsLhead;
        $currentValue['symptomsSweaty'] = $this->symptomsSweaty;
        $currentValue['symptomsHungry'] = $this->symptomsHungry;
        $currentValue['symptomsHeadache'] = $this->symptomsHeadache;
        break;
      default:
        break;
    }
    return $currentValue;
  }

  function nextPage($params) {
    $newPage = null;
    switch ($this->currentPage) {
      case 'first':
        $newPage = 'numberOfTests';
        break;
      case 'numberOfTests':
        $this->numberOfTests = $params['numberOfTests'];
        if ($this->numberOfTests == 0) {
          $newPage = 'numberOfInjections';
        } else {
          $newPage = 'firstReading';
        }
        break;
      case 'firstReading':
        $this->firstReading = $params['firstReading'];
        if ($this->numberOfTests == 1) {
          if ($this->firstReading == '>300') {
            $newPage = 'ketonesTested';
          } else if ($this->firstReading == '<70') {
            $newPage = 'lowTreatments';
          } else {
            $newPage = 'numberOfInjections';
          }
        } else {
          $newPage = 'highReading';
        }
        break;
      case 'highReading':
        $this->highReading = $params['highReading'];
        $newPage = 'lowReading';
        break;
      case 'lowReading':
        $this->lowReading = $params['lowReading'];
        if ($this->firstReading == '>300' || $this->highReading == '>300' || $this->lowReading == '>300') {
          $newPage = 'ketonesTested';
        } else if ($this->firstReading == '<70' || $this->highReading == '<70' || $this->lowReading == '<70') {
          $newPage = 'lowTreatments';
        } else {
          $newPage = 'numberOfInjections';
        }
        break;
      case 'ketonesTested':
        $this->ketonesTested = $params['ketonesTested'];
        if ($this->firstReading == '<70' || $this->highReading == '<70' || $this->lowReading == '<70') {
          $newPage = 'lowTreatments';
        } else {
          $newPage = 'numberOfInjections';
        }
        break;
      case 'lowTreatments':
        $this->lowTreatDidNot = array_key_exists('lowTreatDidNot', $params);
        $this->lowTreatJuice = array_key_exists('lowTreatJuice', $params);
        $this->lowTreatGluTab = array_key_exists('lowTreatGluTab', $params);
        $this->lowTreatMilk = array_key_exists('lowTreatMilk', $params);
        $this->lowTreatCandy = array_key_exists('lowTreatCandy', $params);
        $this->lowTreatOther = array_key_exists('lowTreatOther', $params);
        $newPage = 'numberOfInjections';
        break;
      case 'numberOfInjections':
        $this->numberOfInjections = $params['numberOfInjections'];
        if ($this->numberOfInjections == 0) {
          $newPage = 'symptoms';
        } else {
          $newPage = 'shotSites';
        }
        break;
      case 'shotSites':
        $this->sitesArms = array_key_exists('sitesArms', $params);
        $this->sitesLegs = array_key_exists('sitesLegs', $params);
        $this->sitesButt = array_key_exists('sitesButt', $params);
        $this->sitesLback = array_key_exists('sitesLback', $params);
        $this->sitesStomach = array_key_exists('sitesStomach', $params);
        $this->sitesOther = array_key_exists('sitesOther', $params);
        $newPage = 'symptoms';
        break;
      case 'symptoms':
        $this->symptomsShaky = array_key_exists('symptomsShaky', $params);
        $this->symptomsLhead = array_key_exists('symptomsLhead', $params);
        $this->symptomsSweaty = array_key_exists('symptomsSweaty', $params);
        $this->symptomsHungry = array_key_exists('symptomsHungry', $params);
        $this->symptomsHeadache = array_key_exists('symptomsHeadache', $params);
        $newPage = 'submit';
        break;
      case 'submit':
        $this->persistData();
        $newPage = 'last';
        break;
      case 'last':
        $this->deleteCookies();
        $newPage = $this->firstPage;
        break;
      default:
        print ("DRATS -- case not defined on Next switch, case=".$this->currentPage);
        break;
    }
    return $newPage;
  }
}
?>
