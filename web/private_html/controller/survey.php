<?php
namespace controller;

class survey extends controller {
    const EMBEDDED_LAYOUT_TEMPLATE = "view/layoutEmbedded.phtml";

    private $survey = null;
    private $inputController = null;
    private $resultsColumns = null;
    private $resultsQuery = null;

    public function index($params) {    
        $sql = "
            SELECT id, name, description
            FROM surveys
            ORDER BY name ASC
            ";
        $statement = $this->getSurveyDb()->query($sql);
        
        include self::LAYOUT_TEMPLATE;
    }

    public function questions($params) {
        $surveyName = $this->getSurveyName($params);
        include self::LAYOUT_TEMPLATE;
    }

    public function results($params) {
        $surveyName = $this->getSurveyName($params);
        $inputControllerClassName = 'controller\\survey\\'.$surveyName;
        $inputController = new $inputControllerClassName();
        $cols = $inputController->getResultsColumns();
        $statement = $this->getSurveyDb()->query($inputController->getResultsQuery());
        include self::LAYOUT_TEMPLATE;
    }
    
    public function input($params) {    
        $surveyName = $this->getSurveyName($params);
        $inputControllerClassName = 'controller\\survey\\'.$surveyName;
        $inputController = new $inputControllerClassName();

        if ($surveyName == null || $inputController == null) {
            print("404 or something");
        } else {
            if (array_key_exists('language_code', $params)) {
                $inputController->setLanguageCode($params['language_code']);
            }
            if (array_key_exists('device_id', $params)) {
                $inputController->setDeviceID($params['device_id']);
            }
            
            if (array_key_exists('hw_manufacturer', $params)) {
                $inputController->setHwManufacturer($params['hw_manufacturer']);
            }
            if (array_key_exists('hw_model', $params)) {
                $inputController->setHwModel($params['hw_model']);
            }
            if (array_key_exists('os_version', $params)) {
                $inputController->setOsVersion($params['os_version']);
            }
            if (array_key_exists('app_version_name', $params)) {
                $inputController->setAppVersionName($params['app_version_name']);
            }
            if (array_key_exists('app_package_name', $params)) {
                $inputController->setAppPackageName($params['app_package_name']);
            }
            if (array_key_exists('lat', $params)) {
                $inputController->setLat($params['lat']);
            }
            if (array_key_exists('lon', $params)) {
                $inputController->setLon($params['lon']);
            }
            
            $currentPage = $inputController->newPage(array_merge($params, $_REQUEST));
            $baseUrl = "/survey/input/survey/".$surveyName."/page";
            $currentValue = $inputController->getCurrentValue();
            
            $this->viewName = "input/".$surveyName;
            
            include self::EMBEDDED_LAYOUT_TEMPLATE;
        }
    }
    
    private function getSurveyName($params) {    
        if (array_key_exists('survey', $params)) {
            return($params['survey']);
        } else {
            return('winston_feedback_v1'); //'diabetes_daily_v1'; // FIXME Un-hardcode this instead use db to look up and then put in session
        }
    }
}
?>
