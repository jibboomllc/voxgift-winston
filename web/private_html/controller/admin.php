<?php
namespace controller;
use PDO;

class admin extends controller {
    const MENU_TEMPLATE = "view/admin/menu.phtml";
    private $error_string = "";

    function __construct($controllerName, $actionName) {
        parent::__construct($controllerName, $actionName);
        if (session_id() == '') {
            session_start();
        }
        if (!isset($_SESSION['sess_username'])) {
            $this->viewName = "home";
        }
    }

    public function home($params) {
        include self::LAYOUT_TEMPLATE;
    }

    public function login($params) {
        if ($_POST['username'] == 'admin' && $_POST['password'] == 'adm1n') {
            session_regenerate_id();
            $_SESSION['sess_username'] = $_POST['username'];
            session_write_close();
            $this->viewName = "dashboard";
            $this->dashboard($params);
        } else {
            $this->error_string = "Invalid username and/or password. Please try again.";
            $this->viewName = "home";
        }
        include self::LAYOUT_TEMPLATE;
    }

    public function logout($params) {
        unset($_SESSION['sess_username']);
        session_write_close();
        $this->viewName = "home";
        include self::LAYOUT_TEMPLATE;
    }

    public function dashboard($params) {

        // survey response stats
        $sql = "
            SELECT 
                COUNT(id) AS numResponses
                , MAX(s.create_ts) AS lastResponse
            FROM survey_winston_feedback_v1 AS s
            ";
        $statement = $this->getSurveyDb()->query($sql);
        $winstonData = array();
        if ($statement && $row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $winstonData = $row;
        }

        // circle, user and message stats
        $circleDb = $this->getCircleDb();
        $sql = "
            SELECT 
                COUNT(*) AS numCircles
                , MAX(c.create_ts) AS lastCircleDate
                , SUM(cus.countUsers) AS numUsers
                , SUM(ms.countMessages) AS numMessages
                , MAX(ms.lastMessageDate) AS lastMessage
            FROM circle AS c
            LEFT JOIN (
                SELECT m.circle_uuid AS circleUuid
                    , COUNT(*) AS countMessages 
                    , MAX(m.create_ts) AS lastMessageDate
                FROM message AS m 
                ) AS ms
            ON c.uuid = ms.circleUuid
            LEFT JOIN (
                SELECT cu.circle_uuid AS circleUuid
                    , COUNT(*) AS countUsers 
                FROM circle_users AS cu 
                GROUP BY cu.circle_uuid
                ) AS cus
            ON c.uuid = cus.circleUuid
            ";
        $statement = $circleDb->query($sql);
        $winstonPlusCircleData = array();
        if ($statement && $row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $winstonPlusCircleData = $row;
        }

        // user stats
        $sql = "
            SELECT 
                COUNT(u.uuid) AS numUsers
                , MAX(u.create_ts) AS lastUserDate
            FROM user AS u
            ";
        $statement = $circleDb->query($sql);
        $winstonPlusUserData = array();
        if ($statement && $row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $winstonPlusUserData = $row;
        }
        
        // dictionary stats
        $sql = "
            SELECT 
                COUNT(d.entry_uuid) AS numEntries
                , SUM(d.usage_count) AS usageCount
                , MAX(d.last_usage_ts) AS lastUsageDate
            FROM dictionary_entry_translation AS d
            ";
        $statement = $circleDb->query($sql);
        $winstonPlusStandardDictionaryData = array();
        if ($statement && $row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $winstonPlusStandardDictionaryData = $row;
        }
        $sql = "
            SELECT 
                COUNT(d.uuid) AS numEntries
                , SUM(d.usage_count) AS usageCount
                , MAX(d.last_usage_ts) AS lastUsageDate
            FROM favorites_dictionary_entry AS d
            ";
        $statement = $circleDb->query($sql);
        $winstonPlusFavoritesDictionaryData = array();
        if ($statement && $row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $winstonPlusFavoritesDictionaryData = $row;
        }
        $sql = "
            SELECT 
                COUNT(d.uuid) AS numEntries
                , SUM(d.usage_count) AS usageCount
                , MAX(d.last_usage_ts) AS lastUsageDate
            FROM adhoc_dictionary_entry AS d
            ";
        $statement = $circleDb->query($sql);
        $winstonPlusAdhocDictionaryData = array();
        if ($statement && $row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $winstonPlusAdhocDictionaryData = $row;
        }

        include self::LAYOUT_TEMPLATE;
    }

    public function winston($params) {

        $sql = "
            SELECT 
                *
                , IF(LENGTH(device_id) > 16, 'iOS', CONCAT('Android ', os_version)) AS os
            FROM survey_winston_feedback_v1 AS s
            ORDER BY create_ts DESC
            ";
        if (isset($params['limit'])) {
            $sql .= " LIMIT " . $params['limit'];
        }
        $statement = $this->getSurveyDb()->query($sql);
        $winstonData = array();
        if ($statement) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $winstonData[] = $row;
            }
        }

        include self::LAYOUT_TEMPLATE;
    }

    public function winstonplus($params) {

        // circle, user and message stats per sponsor
        $sql = "
            SELECT s.name AS sponsorName
                , COUNT(*) AS numCircles
                , MIN(c.create_ts) AS firstCircleDate
                , MAX(c.create_ts) AS lastCircleDate
                , SUM(cus.countUsers) AS numUsers
                , SUM(ms.countMessages) AS numMessages
                , MIN(ms.firstMessageDate) AS firstMessage
                , MAX(ms.lastMessageDate) AS lastMessage
                , c.sponsor_uuid
            FROM circle AS c
            LEFT JOIN sponsor AS s 
            ON s.uuid = c.sponsor_uuid
            LEFT JOIN (
                SELECT m.circle_uuid AS circleUuid
                    , COUNT(*) AS countMessages 
                    , MIN(m.create_ts) AS firstMessageDate
                    , MAX(m.create_ts) AS lastMessageDate
                FROM message AS m 
                GROUP BY m.circle_uuid
                ) AS ms
            ON c.uuid = ms.circleUuid
            LEFT JOIN (
                SELECT cu.circle_uuid AS circleUuid
                    , COUNT(*) AS countUsers 
                FROM circle_users AS cu 
                GROUP BY cu.circle_uuid
                ) AS cus
            ON c.uuid = cus.circleUuid
            GROUP BY c.sponsor_uuid
            ";
        $statement = $this->getCircleDb()->query($sql);
        $winstonPlusData = array();
        if ($statement) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $winstonPlusData[] = $row;
            }
        }

        include self::LAYOUT_TEMPLATE;
    }

    public function pbparse($param) {
        include self::LAYOUT_TEMPLATE;

       /* the following creates & d/l a pb file based on a proto file 
        * PLUS it creates from the proto file a pb_prot[NAME].php file
        * 
        * Java equivalent is something like:
        * C:\dev\voxgift\winston\trunk\server\php\private_html\pb>\dev\voxgift\winston\trunk\tools\protoc\protoc.exe --java_out=. request.proto
        */
/*      
        require_once('library/pb4php/parser/pb_parser.php');
        $test = new PBParser();
        $test->parse($meta["path"].'/pb/applications.proto', $meta["path"].'/pb/applications.php');
*/
    }
}
