<?php
namespace controller;

class controller {
    const LAYOUT_TEMPLATE = "view/layout.phtml";
    const PROPERTY_FILE = "index.ini";
    const VG_DB_PROPERTY = "db_vg";
    const CIRCLE_DB_PROPERTY = "db_circle";
    
    protected $controllerName;
    protected $actionName;
    protected $viewName;
    protected $pageTitle;
    
    function __construct($controllerName, $actionName) {
        $this->controllerName = $controllerName;
        $this->actionName = $actionName;
        $this->viewName = $actionName;
    }
    
    public static function getProperty($name) {
        $property = null;
        if(is_readable(get_include_path() . self::PROPERTY_FILE)) {
            $property_data = parse_ini_file(get_include_path() . self::PROPERTY_FILE, TRUE);
            //print_r($property_data);
            if($property_data != null && isset($property_data[$name])) {
                $property = $property_data[$name];
            }
        }
        return $property;
    }
    
    public static function getDb($connInfo) {
        
        // Connect to an ODBC database using driver invocation
        try {
            $db = new \PDO($connInfo['dsn'], $connInfo['user'], $connInfo['password']);
        } catch (\PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }

        return $db;
    }

    public static function getSurveyDb() {
        return self::getDb(self::getProperty(self::VG_DB_PROPERTY));
    }

    public static function getCircleDb() {
        return self::getDb(self::getProperty(self::CIRCLE_DB_PROPERTY));
    }

    public static function getDocumentsDb() {
        return self::getDb(self::getProperty(self::CIRCLE_DB_PROPERTY));
    }
}