<?php
		namespace L10n;
	
		include_once('controllers/l10n/common.php');
		include_once('controllers/l10n/app.php');
		include_once('controllers/l10n/phrases.php');
		include_once('controllers/l10n/surveys.php');

		function compareFiles($languages, $surveys, $basedir, $androiddir, $iosdir, $serverdir, $serverdir2) {
      $files = array();
      $languages[] = "en"; 
      foreach ($languages as $language) {
          $src = App\getTranslationDir($basedir, $language) .'/'. App\findNewestTransltion($basedir, $language);
          if (file_exists($src)) {
              $dest = App\getTranslationDir($androiddir, $language) .'/'. App\FNAME . '.' . App\FEXT ;
              $files[] = array("lang" => $language, "src" => $src, "dest" => $dest, "comp" => comp($src, $dest), "color" => "#0000ff");
              $dest = App\getTranslationDir($iosdir, $language) .'/'. App\FNAME . '.' . App\FEXT ;
              $files[] = array("lang" => $language, "src" => $src, "dest" => $dest, "comp" => comp($src, $dest), "color" => "#ff0000");
          }
  
          $src = Phrases\getTranslationDir($basedir, $language) .'/'. Phrases\findNewestTransltion($basedir, $language);
          if (file_exists($src)) {
              $dest = Phrases\getTranslationDir($androiddir, $language) .'/'. Phrases\FNAME . '.' . Phrases\FEXT ;
              $files[] = array("lang" => $language, "src" => $src, "dest" => $dest, "comp" => comp($src, $dest), "color" => "#0000ff");
              $dest = Phrases\getTranslationDir($iosdir, $language) .'/'. Phrases\FNAME . '.' . Phrases\FEXT ;
              $files[] = array("lang" => $language, "src" => $src, "dest" => $dest, "comp" => comp($src, $dest), "color" => "#ff0000");
          }
          
          foreach ($surveys as $survey) {
              $src = Surveys\getTranslationDir($basedir, $language) .'/'. Surveys\findNewestTransltion($basedir, $language, $survey);
              if (!is_dir($src) && file_exists($src)) {
                  $dest = Surveys\getTranslationDir($serverdir, $language) .'/'. $survey . '.' . Surveys\FEXT;
                  $files[] = array("lang" => $language, "src" => $src, "dest" => $dest, "comp" => comp($src, $dest), "color" => "#00ffff");
                  $dest = Surveys\getTranslationDir($serverdir2, $language) .'/'. $survey . '.' . Surveys\FEXT;
                  $files[] = array("lang" => $language, "src" => $src, "dest" => $dest, "comp" => comp($src, $dest), "color" => "#ff00ff");
              }
          }
      }
      return $files;
    }
		
		function comp($src, $dest) {
				if (file_exists($dest)) {
						if (md5_file($src) == md5_file($dest)) {
								return (0);
						} else {
								return (1);
						}
				}
				return (-1);
		}

    function updateFiles($files) {
        $updatedFiles = array();
        foreach ($files as $index=>$file) {
            if($file["comp"]) {
              copy($file["src"], $file["dest"]);
              $updatedFiles[] = $file;
            }
        }
        return $updatedFiles;
    }
?>
