<?php
  function checkLang($lang_in) {
		if (!isset($lang_in)) {
			die('No language specified; this should be in the query string.');
		}
		$lang = addslashes(htmlspecialchars(strip_tags($lang_in)));
		/*
		$region = addslashes(htmlspecialchars(strip_tags($meta["params"]['region'])));
		if (strlen($region) > 0) {
			$lang .= '-r' . strtoupper($region);
		}
		*/
		if (preg_match('/^[a-z][a-z](-r[A-Z][A-Z])?$/', $lang) == 0) {
			die($lang . ' is not a valid language/locale code; should either be two letters (e.g. nl) or two letters dash two capitals (e.g. pt-rBR)');
		}
		
		return $lang;
	}
	
	function findTranslation($translations, $name) {
		foreach ($translations as $translation) {
			if ($translation['name'] == $name) {
				return $translation;
			}
		}
	}
	
	function dp($in) {
		echo '[<pre>';
		print_r($in);
		echo '</pre>]<br />';
	}

	function encodeForHtml($in) {
		return htmlspecialchars($in);
	}
	
  function encodeForInput($in) {
		return str_replace(array('&','"'), array('&amp;','&quot;'), $in);
	}

  function outputCSV($data) {
    $outstream = fopen("php://output", "w");
    function __outputCSV(&$vals, $key, $filehandler) {
        fputcsv($filehandler, $vals); // add parameters if you want
    }
    array_walk($data, "__outputCSV", $outstream);
    fclose($outstream);
  }

  function toArray($lang, $strings, $translations, &$wcounttotal, &$wcountmissing) {
    $count = 0;
    $buffer = array();
    $row = array("count", "id",  "english", "wcount", $lang);
    $buffer[] = $row;
		$wcounttotal = 0;
		$wcountmissing = 0;

    foreach ($strings as $string) {
      $name = $string['name'];
      $transtext = '';
      
      if ($string['type'] == 'string') {
        $value = $string['value'];
        if (isset($translations)) {
          $trans = findTranslation($translations, $name);
          $transtext = $trans['value'];
        }
      } else if ($string['type'] == 'stringarray') {
        $value = implode($arraySeparator, $string['values']);
        if (isset($translations)) {
          $trans = findTranslation($translations, $name);
          $transtext = implode($arraySeparator, $trans['values']);
        }
      }
      
      // Gather some stats
      $wcount = str_word_count($value);
			$wcounttotal += $wcount;
			if (str_word_count($transtext) == 0){
				$wcountmissing += $wcount;
			}

      $row = array(++$count, $name,  $value, $wcount, $transtext);
      $buffer[] = $row;
    }
    
    return $buffer;
  }
?>
 