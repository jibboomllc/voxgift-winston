<?php
  namespace L10n\Surveys;

  const FEXT = "xml";
	
  function getTranslationDir($basedir, $lang) {
    if ($lang == "en") {
		  return ($basedir . '/../survey/input/content');
    } else {
		  return ($basedir . '/../survey/input/content-' . $lang);
    }
	}

  function findNewestTransltion($basedir, $lang, $survey) {
    $newesttranslation = null;

		// Get the last translation file, if it exists
		if ($transdir = @opendir(getTranslationDir($basedir, $lang))) {

			while (false !== ($surveyfile = readdir($transdir))) {
				// If it is a $survey XML file, save it in the array
				if (substr($surveyfile, 0, strlen($survey)+1) == $survey . '.') {
					$existingfiles[] = $surveyfile;
				}
			}
			closedir($transdir);
			
			// Files should have been found here
			if (!isset($existingfiles) || count($existingfiles) <= 0) {
			//	die ('Every resource directory \'content-{lang}\' should at least have a single translation file.');
			} else {
				// Sort the list of all $survey[.timestamp].xml files
				rsort($existingfiles);
				// Use the newest custom translation or $survey.xml as a fallback
				$newesttranslation = $existingfiles[0];
				if ($newesttranslation == $survey.'.xml' && count($existingfiles) > 1) {
					$newesttranslation = $existingfiles[1];
				}
			}
		}
		return $newesttranslation;
	}

	function parseStrings($file) {
    $out = array();
		
		$xmldoc = new \DOMDocument();
		$xmldoc->load($file);//'diabetes_daily_v1.xml');
	
	  // first read the pages
		$pages = $xmldoc->getElementsByTagName( "page" );
		foreach( $pages as $page ) {
      $out_record = array();
			$out_record['type'] = 'string';
			$name = $page->getAttribute( "name" );
      
			$tag_names = array("title", "details", "hint");
			
			foreach ($tag_names as $tag_name) {
				$value = NULL;
				$elements = $page->getElementsByTagName($tag_name);
				if ($elements->length > 0) {
					$value = $elements->item(0)->nodeValue;
					if (strlen($value) > 0) {
						//$out_record['name'] = '//page[@name=\'' . $name . '\']/' . $tag_name;
						$out_record['name'] = 'page_' . $name . '_' . $tag_name;
						$out_record['value'] = $value;
						$out[] = $out_record;
					}
				}
			}
		}

	  // next read the option_lists
		$option_lists = $xmldoc->getElementsByTagName( "option_list" );
		foreach( $option_lists as $option_list ) {
      $out_record = array();
			$out_record['type'] = 'string';
			$name = $option_list->getAttribute( "name" );
      
			$tag_names = array("option");
			
			foreach ($tag_names as $tag_name) {
				$value = NULL;
				$elements = $option_list->getElementsByTagName($tag_name);
				foreach ($elements as $element) {
					$value = $element->nodeValue;
					if (strlen($value) > 0) {
						//$out_record['name'] = '//option_list[@name=\'' . $name . '\']/' . $tag_name. '[@id=\'' . $element->getAttribute( "id" ) . '\']/';
						$out_record['name'] = 'optionlist_' . $name . '_' . $tag_name. '_' . $element->getAttribute( "id" );
						$out_record['value'] = $value;
						$out[] = $out_record;
					}
				}
			}
		}
		return $out;
	}

  function saveData($basedir, $lang, $langname, $frommail, $sendmail, $survey) {
		$xmldoc = new \DOMDocument();
		$xmldoc->load(getTranslationDir($basedir, "en") . '/' . $survey . '.xml');
	
	  // first update the pages
		$pages = $xmldoc->getElementsByTagName( "page" );
		foreach( $pages as $page ) {
			$name = $page->getAttribute( "name" );
      
			$tag_names = array("title", "details", "hint");
			
			foreach ($tag_names as $tag_name) {
				$value = NULL;
				$elements = $page->getElementsByTagName($tag_name);
				if ($elements->length > 0) {
					$value = $elements->item(0)->nodeValue;
					if (strlen($value) > 0) {
						//$key = '//page[@name=\'' . $name . '\']/' . $tag_name;
						$key = 'page_' . $name . '_' . $tag_name;

						$newValue = str_replace("\n","\\n",$_POST[$key]);
						$elements->item(0)->nodeValue =  $newValue;
					}
				}
			}
		}

	  // next update the option_lists
		$option_lists = $xmldoc->getElementsByTagName( "option_list" );
		foreach( $option_lists as $option_list ) {
			$name = $option_list->getAttribute( "name" );
      
			$tag_names = array("option");
			
			foreach ($tag_names as $tag_name) {
				$value = NULL;
				$elements = $option_list->getElementsByTagName($tag_name);
				foreach ($elements as $element) {
					$value = $element->nodeValue;
					if (strlen($value) > 0) {
						//$key = '//option_list[@name=\'' . $name . '\']/' . $tag_name. '[@id=\'' . $element->getAttribute( "id" ) . '\']/';
						$key = 'optionlist_' . $name . '_' . $tag_name. '_' . $element->getAttribute( "id" );
						
						$newValue = str_replace("\n","\\n",$_POST[$key]);
						$element->nodeValue =  $newValue;
					}
				}
			}
		}
		
		// Save the new translation with a new unique number (to prevent incorrect overwriting)
		$newfilename = $survey . '.' . time() . '.xml';
		$transdir = getTranslationDir($basedir, $lang);
		if (!is_dir($transdir)) {
			mkdir($transdir);
		}
		$newfilepath = $transdir . '/' . $newfilename;
		$xmldoc->save($newfilepath);
		
		// Send an e-mail to notify of the new translation
		if (isset($sendmail) && $sendmail != "") {
			
			# Anti-header-injection
			# By Victor Benincasa <vbenincasa(AT)gmail.com>
			//foreach($_REQUEST as $fields => $value) if(@eregi("TO:", $value) || @eregi("CC:", $value) || 
			//	@eregi("CCO:", $value) || @eregi("Content-Type", $value)) exit("ERROR: Code injection attempt denied! " .
			//	"Please don't use the following sequences in your message: 'TO:', 'CC:', 'CCO:' or 'Content-Type'.");

			mail(
				$sendmail,
				"Survey $survey  $langname ($lang) translation updated",
				"The $langname ($lang) translation of your Winston survey $survey" . 
				"has been updated from " . $_SERVER['REMOTE_ADDR'] . "." .
				 "\n\nTranslator: " . $_SESSION['login']['useremail'] . 
				"\n\nThe new file was stored at http://www.voxgift.com/$newfilepath",
				(isset($frommail) && $frommail == '')? null: "From: $frommail");
		}
		return $newfilename;
	}
?>
