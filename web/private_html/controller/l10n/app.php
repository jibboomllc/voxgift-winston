<?php
  namespace L10n\App;

  const FNAME = "strings";
  const FEXT = "xml";
	
	function getTranslationDir($basedir, $lang) {
    if ($lang == "en") {
		  return ($basedir . '/values');
    } else {
		  return ($basedir . '/values-' . $lang);
    }
	}

  function findNewestTransltion($basedir, $lang) {
    $newesttranslation = null;

		// Get the last translation file, if it exists
		if ($transdir = @opendir(getTranslationDir($basedir, $lang))) {
		
			while (false !== ($stringsfile = readdir($transdir))) {
				// If it is a strings XML file, save it in the array
				if (substr($stringsfile, 0, 8) == 'strings.') {
					$existingfiles[] = $stringsfile;
				}
			}
			closedir($transdir);
			
			// Files should have been found here
			if (!isset($existingfiles) || count($existingfiles) <= 0) {
				die ('Every resource directory \'values-{lang}\' should at least have a single translation file.');
			}
			
			// Sort the list of all strings[.timestamp].xml files
			rsort($existingfiles);
			// Use the newest custom translation or strings.xml as a fallback
			$newesttranslation = $existingfiles[0];
			if ($newesttranslation == 'strings.xml' && count($existingfiles) > 1) {
				$newesttranslation = $existingfiles[1];
			}
		}
		
		return $newesttranslation;
	}

  function parseStrings($file) {
		$lines = file($file);
		$multiline = "";
		foreach ($lines as $line) {
			
			// Empty lines
			$line = trim($line);
			if ($line == '') {
				//$out[]['type'] = 'empty';
				
			// <string> lines
			} else if (substr($line, 0, 8) == '<string ' && substr($line, -9) == '</string>') {
				$out[]['type'] = 'string';
				$namePos = strpos($line, 'name="') + 6;
				$stringPos = strPos($line, '>', $namePos) + 1;
				$name = substr($line, $namePos, strpos($line, '"', $namePos) - $namePos);
				$string = substr($line, $stringPos, strrpos($line, '<') - $stringPos);
				$out[count($out)-1]['name'] = $name;
				$out[count($out)-1]['value'] = $string;
				$multiline = "";
				
			} else if ((substr(trim($line), 0, 8) == '<string ') && ($multiline == "")) {
				// Support multi line strings
				$multiline = $line;
			
			} else if (substr($line, -9) == '</string>') {
				$multiline .= $line;
				$out[]['type'] = 'string';
				$namePos = strpos($multiline, 'name="') + 6;
				$stringPos = strPos($multiline, '>', $namePos) + 1;
				$name = substr($multiline, $namePos, strpos($multiline, '"', $namePos) - $namePos);
				$string = substr($multiline, $stringPos, strrpos($multiline, '<') - $stringPos);
				$out[count($out)-1]['name'] = $name;
				$out[count($out)-1]['value'] = $string;
				$multiline = "";
				
			// <string-array> lines
			} else if (substr($line, 0, 13) == '<string-array') {
				$out[]['type'] = 'stringarray';
				$namePos = strpos($line, 'name="') + 6;
				$name = substr($line, $namePos, strpos($line, '"', $namePos) - $namePos);
				$out[count($out)-1]['name'] = $name;
				$multiline = "";
				
			// <item> lines
			} else if (substr($line, 0, 6) == '<item>') {
				$val = substr($line, 6, strrpos($line, '<') - 6);
				$out[count($out)-1]['values'][] = $val;
				$multiline = "";
				
			} else if ($multiline != ""){
				$multiline .= $line;
			}
		}
		return $out;
	}
	
  function saveData($basedir, $lang, $langname, $frommail, $sendmail) {
	
		// Traverse through the lines of the original strings file
		$lines = file($basedir . '/values/strings.xml');
		$outfile = "";
		$multiline = "";
		foreach ($lines as $line) {
			
			// <string> lines
			$line = trim($line);
			if (substr(trim($line), 0, 8) == '<string ' && substr($line, -9) == '</string>') {
				$namePos = strpos($line, 'name="') + 6;
				$stringPos = strPos($line, '>', $namePos) + 1;
				$name = substr($line, $namePos, strpos($line, '"', $namePos) - $namePos);
				//$newValue = stripslashes($_POST[$name]);
				$newValue = str_replace("\n","\\n",$_POST[$name]);
				if (trim($newValue) != '') {
					$outfile .= substr($line, 0, $stringPos) . $newValue . substr($line, strrpos($line, '<')) . "\n";
				}
				$multiline = "";
				
			} else if (substr(trim($line), 0, 8) == '<string ') {
				// Support multi line strings
				$multiline = $line;
			
			} else if (substr($line, -9) == '</string>') {
				$multiline .= $line;
				$namePos = strpos($multiline, 'name="') + 6;
				$stringPos = strPos($multiline, '>', $namePos) + 1;
				$name = substr($multiline, $namePos, strpos($multiline, '"', $namePos) - $namePos);
				$newValue = str_replace("\n","\\n",stripslashes($_POST[$name]));
				if (trim($newValue) != '') {
					$outfile .= substr($multiline, 0, $stringPos) . $newValue . substr($multiline, strrpos($multiline, '<')) . "\n";
				}
				$multiline = "";
			// <string-array> lines
			} else if (substr($line, 0, 13) == '<string-array') {
				$namePos = strpos($line, 'name="') + 6;
				$name = substr($line, $namePos, strpos($line, '"', $namePos) - $namePos);
				$newValues = explode($arraySeparator, str_replace("\n","\\n",stripslashes($_POST[$name])));
				$n = 0;
				$outfile .= $line . "\n";
				$multiline = "";
			// <item> lines
			} else if (substr($line, 0, 6) == '<item>') {
				$outfile .= substr($line, 0, 6) . $newValues[$n] . substr($line, strrpos($line, '<')) . "\n";
				$n++;
				$multiline = "";
			} else {
				if ($multiline != "") {
					$multiline .= $line;
				} else {
					$outfile .= $line . "\n";
				}
			}
		}
				
		// Save the new translation with a new unique number (to prevent incorrect overwriting)
		$newfilename = 'strings.' . time() . '.xml';
		if (!is_dir($basedir . '/values-' . $lang)) {
			mkdir($basedir . '/values-' . $lang);
		}
		$newfilepath = $basedir . '/values-' . $lang . '/' . $newfilename;
		file_put_contents($newfilepath, $outfile);
		
		// Send an e-mail to notify of the new translation
		if (isset($sendmail) && $sendmail != "") {
			# Anti-header-injection
			# By Victor Benincasa <vbenincasa(AT)gmail.com>
			//foreach($_REQUEST as $fields => $value) if(@eregi("TO:", $value) || @eregi("CC:", $value) || 
			//	@eregi("CCO:", $value) || @eregi("Content-Type", $value)) exit("ERROR: Code injection attempt denied! " .
			//	"Please don't use the following sequences in your message: 'TO:', 'CC:', 'CCO:' or 'Content-Type'.");

			mail(
				$sendmail,
				"$langname ($lang) app UI translation updated",
				"The $langname ($lang) translation of your Android string resource file " . 
				"has been updated from " . $_SERVER['REMOTE_ADDR'] . "." .
				 "\n\nTranslator: " . $_SESSION['login']['useremail'] . 
				"\n\nThe new file was stored at http://www.voxgift.com/$newfilepath",
				(isset($frommail) && $frommail == '')? null: "From: $frommail");
				
		}
		return $newfilename;
	}
?>
