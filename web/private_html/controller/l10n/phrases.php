<?php
  namespace L10n\Phrases;

  const FNAME = "dictionary";
  const FEXT = "csv";

  function getTranslationDir($basedir, $lang) {
    if ($lang == "en") {
		  return ($basedir . '/raw');
    } else {
		  return ($basedir . '/raw-' . $lang);
    }
	}

  function findNewestTransltion($basedir, $lang) {
    $newesttranslation = null;

		// Get the last translation file, if it exists
		if ($transdir = @opendir(getTranslationDir($basedir, $lang))) {
		
			while (false !== ($dictionaryfile = readdir($transdir))) {
				// If it is a dictionary CSV file, save it in the array
				if (substr($dictionaryfile, 0, 11) == (FNAME . '.')) {
					$existingfiles[] = $dictionaryfile;
				}
			}
			closedir($transdir);
			
			// Files should have been found here
			if (!isset($existingfiles) || count($existingfiles) <= 0) {
				die ('Every resource directory \'raw-{lang}\' should at least have a single translation file.');
			}
			
			// Sort the list of all dictionary[.timestamp].csv files
			rsort($existingfiles);
			// Use the newest custom translation or dictionary.csv as a fallback
			$newesttranslation = $existingfiles[0];
			if ($newesttranslation ==  (FNAME . '.' . FEXT) && count($existingfiles) > 1) {
				$newesttranslation = $existingfiles[1];
			}
			
		}
		
		return $newesttranslation;
	}

	function parseStrings($file) {
		$lines = file($file);
		$multiline = "";
		foreach ($lines as $line) {
			
			// Empty lines
			$line = trim($line);
			if ($line == '') {
				//$out[]['type'] = 'empty';
			} else {
				$parts = explode(',', $line);
				$out[]['type'] = 'string';
				$out[count($out)-1]['name'] = $parts[0];
				$out[count($out)-1]['value'] = substr($line, strlen($parts[0])+1);
			}
		}
		return $out;
	}

  function saveData($basedir, $lang, $langname, $frommail, $sendmail) {
		// Traverse through the lines of the original dictionary file
		$lines = file($basedir . '/raw/dictionary.csv');
		$outfile = "";
		foreach ($lines as $line) {
			$line = trim($line);
			if ($line == '') {
  			$outfile .= $line . "\n";
			} else {
        //echo $line."<br/>";
				$parts = explode(',', $line);
				$newValue = str_replace("\n","\\n",$_POST[$parts[0]]);
				if (trim($newValue) != '') {
					$outfile .=  $parts[0] . "," . $newValue . "\n";
				}
			}

			if (substr(trim($line), 0, 8) == '<string ' && substr($line, -9) == '</string>') {
				$namePos = strpos($line, 'name="') + 6;
				$stringPos = strPos($line, '>', $namePos) + 1;
				$name = substr($line, $namePos, strpos($line, '"', $namePos) - $namePos);
				//$newValue = stripslashes($_POST[$name]);
				$newValue = str_replace("\n","\\n",$_POST[$name]);
				if (trim($newValue) != '') {
					$outfile .= substr($line, 0, $stringPos) . $newValue . substr($line, strrpos($line, '<')) . "\n";
				}
			}
		}
		// Save the new translation with a new unique number (to prevent incorrect overwriting)
		$newfilename = 'dictionary.' . time() . '.csv';
		if (!is_dir($basedir . '/raw-' . $lang)) {
			mkdir($basedir . '/raw-' . $lang);
		}
		$newfilepath = $basedir . '/raw-' . $lang . '/' . $newfilename;
		file_put_contents($newfilepath, $outfile);
		
		// Send an e-mail to notify of the new translation
		if (isset($sendmail) && $sendmail != "") {

			# Anti-header-injection
			# By Victor Benincasa <vbenincasa(AT)gmail.com>
			//foreach($_REQUEST as $fields => $value) if(@eregi("TO:", $value) || @eregi("CC:", $value) || 
			//	@eregi("CCO:", $value) || @eregi("Content-Type", $value)) exit("ERROR: Code injection attempt denied! " .
			//	"Please don't use the following sequences in your message: 'TO:', 'CC:', 'CCO:' or 'Content-Type'.");

			mail(
				$sendmail,
				"$langname ($lang) Phrases translation updated",
				"The $langname ($lang) translation of your Winston library file " . 
				"has been updated from " . $_SERVER['REMOTE_ADDR'] . "." .
				"\n\nTranslator: " . $_SESSION['login']['useremail'] . 
				"\n\nThe new file was stored at http://www.voxgift.com/$newfilepath",
				(isset($frommail) && $frommail == '')? null: "From: $frommail");
				
		}
		return $newfilename;
	}

?>
