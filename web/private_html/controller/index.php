<?php
namespace controller;

class index extends controller {
    public function home($params) {
//        $file = get_include_path() . self::LAYOUT_TEMPLATE;
//        echo $file . "<br/>" ; if( file_exists($file)) echo 't'; else echo 'f';
        include self::LAYOUT_TEMPLATE;
    }
    
    public function about($params) {
        include self::LAYOUT_TEMPLATE;
    }

    public function winston($params) {
        include self::LAYOUT_TEMPLATE;
    }

    public function winstonplus($params) {
        include self::LAYOUT_TEMPLATE;
    }

    public function winstonpro($params) {
        include self::LAYOUT_TEMPLATE;
    }

    public function trainer($params) {
        include self::LAYOUT_TEMPLATE;
    }
}
