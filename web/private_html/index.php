<?php
// the following requires php 5.4 so we'll define our own function
  //spl_autoload_extensions(".php"); // comma-separated list
  //spl_autoload_register(); // since namespace = dir, we can use the default function

// function to parse based on \ in namespace
spl_autoload_register(function ($classname) {
    $classname = ltrim($classname, "\\");
    preg_match('/^(.+)?([^\\\\]+)$/U', $classname, $match);
	$classname = str_replace("\\", "/", $match[1])
		. str_replace("\\", "/", $match[2])
        . ".php";
    include_once $classname;
});

const DEFAULT_CONTROLLER = "index";
const DEFAULT_ACTION = "home";

$controllerName = DEFAULT_CONTROLLER;
$actionName = DEFAULT_ACTION;
$params = array(); // if we are going to accept regular (?key=value;), we should set this to $_POST

// parse URI
$data = preg_split("/\//", $_SERVER['REQUEST_URI'],NULL,PREG_SPLIT_NO_EMPTY);
if (sizeof($data)>0) {
  $controllerName = $data[0];
  if (sizeof($data)>1) {
	$actionName = $data[1];
	if (sizeof($data)>2) {
	  for ($i=2; $i<sizeof($data); $i++) {
		if (sizeof($data)>$i+1) {
		  $params[$data[$i]] = urldecode ($data[++$i]);
		} else {
		  $params[$data[$i]] = "";
		}
	  }
	}
  }
}

$controllerClassName = 'controller\\'.$controllerName;
$controller = new $controllerClassName($controllerName, $actionName);
$controller->$actionName($params);

?>
