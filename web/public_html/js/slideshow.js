function Slide(title, text, links, imageFile) {
	this.title = title;
	this.text = text;
	this.links = links;
	this.imageFile = imageFile;
}

function Slideshow(slides) {
	this.slides = slides;	
	this.image = null;
	this.title = null;
	this.caption = null;
	this.more = null;
	this.slideIndex = 0;
	this.play = true;
	this.btnPause = null;
}

Slideshow.prototype.initSlideElements = function() {
	this.image = document.getElementById("ss_image");
	this.title = document.getElementById("ss_title");
	this.caption = document.getElementById("ss_caption");
	this.more = document.getElementById("ss_more");
	this.btnPause = document.getElementById("ss_btn_pp");
}

Slideshow.prototype.changeSlide = function() {
	if (this.play) {
		this.image.setAttribute("src", this.slides[this.slideIndex].imageFile);
		this.title.innerHTML = this.slides[this.slideIndex].title;
		this.caption.innerHTML = this.slides[this.slideIndex].text;
		this.more.innerHTML = this.slides[this.slideIndex].links;
		this.slideIndex++;
		if (this.slideIndex >= this.slides.length) {
			this.slideIndex = 0;
		}
	}
}

Slideshow.prototype.togglePlay = function() {
	this.play = !this.play;
	if (this.play) {
		this.btnPause.setAttribute("class", "button_pause");
		this.btnPause.setAttribute("title", "Pause");
	} else {
		this.btnPause.setAttribute("class", "button_play");
		this.btnPause.setAttribute("title", "Play");
	}
}

Slideshow.prototype.showControls = function() {
	this.btnPause.style.visibility = "visible";
}

Slideshow.prototype.hideControls = function() {
	this.btnPause.style.visibility = "hidden";
}

