function pageUpdate($pageName) {
	var
		$http,
		$self = arguments.callee;

	if (window.XMLHttpRequest) {
		$http = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		try {
			$http = new ActiveXObject('Msxml2.XMLHTTP');
		} catch(e) {
			$http = new ActiveXObject('Microsoft.XMLHTTP');
		}
	}

	if ($http) {
		$http.onreadystatechange = function()
		{
			if (/4|^complete$/.test($http.readyState)) {
				document.getElementById('ReloadThis').innerHTML = $http.responseText;
				setTimeout(function(){$self($pageName);}, 10000);
			}
		};
		$http.open('GET', $pageName, true);
		$http.send(null);
	}

}
