function googleSectionalElementInit() {
  new google.translate.SectionalElement({
    sectionalNodeClassName: 'goog-trans-section',
    controlNodeClassName: 'goog-trans-control',
    background: '#f4fa58'
  }, 'google_sectional_element');
}
function copy_text(label, from, to) {
	var labelStr = label.textContent||label.innerText;
	var fromStr = from.textContent||from.innerText;
	var str = fromStr.replace(labelStr, '');
	to.value = str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

var showOnlyMissing = false;

function flipShowOnlyMissing() {
	showOnlyMissing = !showOnlyMissing;
	var tbl = document.getElementById('translationtable');
	// Traverse through the table rows (skipping the first header row)
	for (var i = 1; i < tbl.rows.length; i++) {
		if (tbl.rows[i].cells.length > 5 && tbl.rows[i].cells[5].children != null) {
			// Get the input field, which is in the third cell
			var input = tbl.rows[i].cells[5].children[0];
			if (input.value != '' && !showOnlyMissing) {
				// Show this row (again)
				tbl.rows[i].style.display = 'table-row';
			} else if (input.value != '') {
				// Hide this row
				tbl.rows[i].style.display = 'none';
			}
		}
	}
	// Change the button text
	var but = document.getElementById('showmissing');
	but.value = showOnlyMissing? 'Show all rows': 'Show only missing translations';
}
