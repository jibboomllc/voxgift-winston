-- CREATE DATABASE `jibboomc_vg`
-- USE `jibboomc_vg`;

DROP TABLE IF EXISTS feedback_log;
CREATE TABLE feedback_log (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , device_id VARCHAR(128)
  , hw_manufacturer VARCHAR(128)
  , hw_model VARCHAR(128)
  , os_version VARCHAR(128)
  , app_version_name VARCHAR(32)
  , app_package_name VARCHAR(128)
  , lat DOUBLE
  , lon DOUBLE
  , usage_fb VARCHAR(512)
  , languages_fb VARCHAR(256)
  , phrases_fb VARCHAR(256)
  , general_fb TEXT
  , create_ts TIMESTAMP DEFAULT NOW()
);

DROP TABLE IF EXISTS facility;
CREATE TABLE facility (
  id INT NOT NULL PRIMARY KEY AUTO_INCREMENT
  , name VARCHAR(128)
  , address1 VARCHAR(128)
  , address2 VARCHAR(128)
  , city VARCHAR(128)
  , state VARCHAR(128)
  , zip VARCHAR(32)
  , phone VARCHAR(128)
  , fax VARCHAR(128)
  , url VARCHAR(128)
  , type VARCHAR(128)
  , contact VARCHAR(128)
  , contact_email VARCHAR(128)
  , overview TEXT
  , description TEXT
  , data_source VARCHAR(128)
  , create_ts TIMESTAMP DEFAULT NOW()
);
