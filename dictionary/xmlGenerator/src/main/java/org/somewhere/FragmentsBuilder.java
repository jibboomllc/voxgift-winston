package org.somewhere;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class FragmentsBuilder {

    private final Set<FragmentNode> nodes;
    
    public FragmentsBuilder() {
        nodes = Sets.newHashSet();
    }
    
    public void addLine(String[] line) {
        Preconditions.checkNotNull(line);
        Preconditions.checkArgument(line.length > 2, "Assuming (currently) that there are at least 2 columns in csv.");
        List<String> lineList = Lists.newArrayList(line);
        Iterator<String> lineIterator = lineList.iterator();
        lineIterator.next(); //dumping first column
        String firstActualColumn = lineIterator.next();
        boolean found = false;
        for (FragmentNode existingFragmentChain : nodes) {
            if (existingFragmentChain.getFragment() == firstActualColumn) {
                existingFragmentChain.appendList(lineIterator);
                found = true;
                break;
            }
        }
        if (!found) {
            FragmentNode fragmentNodeBeignBuilt = new FragmentNode(firstActualColumn);
            fragmentNodeBeignBuilt.appendList(lineIterator);
            nodes.add(fragmentNodeBeignBuilt);
        }
    }
}
