package org.somewhere;

import java.io.OutputStream;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang3.StringUtils;
import org.somewhere.schema.ObjectFactory;
import org.somewhere.schema.SentenceType;
import org.somewhere.schema.SentencesGrammar;

import com.google.common.base.Preconditions;

public class SentencesBuilder {

	private final ObjectFactory factory;
	private final SentencesGrammar grammarRoot;
	private final JAXBContext jaxbContext;
	private final List<SentenceType> sentences;
	
	public SentencesBuilder() throws JAXBException {
		this.jaxbContext = JAXBContext.newInstance("org.somewhere.schema");
		this.factory = new ObjectFactory();
		this.grammarRoot = factory.createSentencesGrammar();
		this.sentences = grammarRoot.getSentences();
	}
	
	public void addLine(String[] line) {
		Preconditions.checkNotNull(line);
		Preconditions.checkArgument(line.length > 2, "Assuming (currently) that there are at least 2 columns in csv.");
		SentenceType currentSentence = this.factory.createSentenceType();
		currentSentence.setName(line[0]);
		StringBuilder str = new StringBuilder();
		for (int i = 1; i < line.length; i++) {
			if (StringUtils.isNotBlank(line[i])) {
				str.append(line[i]);
			}
			str.append(" ");
		}
		String value = StringUtils.stripEnd(str.toString(), null);
		currentSentence.setValue(value);
		sentences.add(currentSentence);
	}

	public void write(OutputStream out) throws JAXBException {
		Preconditions.checkNotNull(out);
		Marshaller m = this.jaxbContext.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		m.marshal(grammarRoot, out);
	}
}
