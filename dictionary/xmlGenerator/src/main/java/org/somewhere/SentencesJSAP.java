package org.somewhere;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import javax.xml.bind.JAXBException;

import au.com.bytecode.opencsv.CSVReader;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.stringparsers.FileStringParser;

public class SentencesJSAP {

    private final static String CSV_INPUT_ID = "csvInput";
    private final static String OUTPUT_ID = "output";

    private final JSAP jsap = new JSAP();

    private final FlaggedOption inputOption;
    private final FlaggedOption outputOption;

    public SentencesJSAP() {

        FileStringParser inputFileParser = FileStringParser.getParser().setMustBeFile(true).setMustExist(true);
        inputOption = new FlaggedOption(CSV_INPUT_ID);
        inputOption.setRequired(JSAP.REQUIRED);
        inputOption.setShortFlag('f');
        inputOption.setLongFlag("csvInput");
        inputOption.setHelp("The input file to read in a known format.");
        inputOption.setStringParser(inputFileParser);

        FileStringParser outputFileParser = FileStringParser.getParser().setMustBeFile(true).setMustExist(false);
        outputOption = new FlaggedOption(OUTPUT_ID);
        outputOption.setRequired(JSAP.REQUIRED);
        outputOption.setShortFlag('o');
        outputOption.setLongFlag("output");
        outputOption.setHelp("A nonexistant filename that will recieve the output.");
        outputOption.setStringParser(outputFileParser);
    }

    public void doIt(String[] args) throws JSAPException, JAXBException {
        jsap.registerParameter(inputOption);
        jsap.registerParameter(outputOption);

        JSAPResult config = jsap.parse(args);

        if (!config.success()) {
            // print out specific error messages describing the problems
            // with the command line, THEN print usage, THEN print full
            // help. This is called "beating the user with a clue stick."
            for (Iterator<?> errs = config.getErrorMessageIterator(); errs.hasNext();) {
                System.err.println("Error: " + errs.next());
            }

            usage();
        }
        SentencesBuilder builder = new SentencesBuilder();
        readFile(config, builder);

        writeFile(config, builder);
    }

    void readFile(JSAPResult config, SentencesBuilder builder) {
        File input = config.getFile(CSV_INPUT_ID);
        FileReader reader = null;
        try {
            reader = new FileReader(input);
            CSVReader csvReader = new CSVReader(reader, ',');

            // Throw out first line header;
            csvReader.readNext();

            String[] currentLine = csvReader.readNext();
            while (currentLine != null) {
                builder.addLine(currentLine);
                currentLine = csvReader.readNext();
            }
        } catch (IOException e) {
            e.printStackTrace();
            usage();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    System.err.println("Issue closing reader!!!");
                    e.printStackTrace();
                }
            }
        }
    }

    void writeFile(JSAPResult config, SentencesBuilder builder) throws JAXBException {
        FileOutputStream output = null;
        try {
            output = new FileOutputStream(config.getFile(OUTPUT_ID));
            builder.write(output);
        } catch (IOException e) {
            e.printStackTrace();
            usage();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    System.err.println("Issue closing reader!!!");
                    e.printStackTrace();
                }
            }
        }
    }

    private void usage() {
        System.err.println();
        System.err.println("Usage: java " + SentencesJSAP.class.getName());
        System.err.println("                " + jsap.getUsage());
        System.err.println();
        System.err.println(jsap.getHelp());
        System.exit(1);
    }

    public static final void main(String[] args) throws Exception {
        SentencesJSAP main = new SentencesJSAP();
        main.doIt(args);
    }
}
