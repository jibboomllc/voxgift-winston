package org.somewhere;

import java.util.Iterator;
import java.util.Set;

import com.google.common.collect.Sets;

public class FragmentNode {

    private final String fragment;
    private int index;
    private final Set<FragmentNode> nexts;
    
    public FragmentNode(String fragment) {
        this.fragment = fragment;
        this.nexts = Sets.newHashSet();
    }
    
    public FragmentNode(FragmentNode source) {
        this.fragment = source.fragment;
        this.nexts = Sets.newHashSet();
        for (FragmentNode currentFromNexts : source.nexts) {
            this.nexts.add(new FragmentNode(currentFromNexts));
        }
    }
    
    public String getFragment() {
        return this.fragment;
    }
    
    public Set<FragmentNode> getNexts() {
        Set<FragmentNode> returnVal = Sets.newHashSet();
        for (FragmentNode currentFromNexts : this.nexts) {
            returnVal.add(new FragmentNode(currentFromNexts));
        }
        return returnVal;
    }
    
    public void appendList(Iterator<String> nextFragments) {
        if (!nextFragments.hasNext()) {
            return;
        }
        String next = nextFragments.next();
        FragmentNode found = null;
        for (FragmentNode current : nexts) {
            if (current.getFragment() == next) {
                found = current;
                break;
            }
        }
        if (found == null) {
            FragmentNode nextFragmentChain = new FragmentNode(next);
            nextFragmentChain.appendList(nextFragments);
            this.nexts.add(nextFragmentChain);
        } else {
            found.appendList(nextFragments);
        }
    }
    
    public int hashCode() {
        return this.fragment.hashCode();
    }
    
    public void setIndex(int index) {
        this.index = index;
    }
    
    public int getIndex() {
        return this.index;
    }
}
