package com.voxgift.winston.library;

import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class CircularSet {

    private final List<String> data;
    private int index;

    public CircularSet(CircularSet original) {
        this.data = Lists.newArrayList(original.data);
        this.index = 0;
    }
    
    public CircularSet(Set<String> data) {
        this.data = Lists.newArrayList(data);
        this.index = 0;
    }

    public String getCurrent() {
        return this.data.get(index);
    }

    public String getAfter() {
        if (index + 1 >= this.data.size()) {
            return this.data.get(0);
        } else {
            return this.data.get(index+1);
        }
    }

    public String getBefore() {
        if ((index - 1) < 0) {
            return this.data.get(this.data.size() - 1);
        } else {
            return this.data.get(this.index - 1);
        }
    }

    public void increaseIndex() {
        this.index++;
        if (this.index >= this.data.size()) {
            this.index = 0;
        }
    }

    public void decreaseIndex() {
        this.index--;
        if (this.index < 0) {
            this.index = this.data.size() - 1;
        }
    }

    public Set<String> allStrings() {
        return Sets.newHashSet(this.data);
    }
}