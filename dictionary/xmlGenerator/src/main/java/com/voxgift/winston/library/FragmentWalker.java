package com.voxgift.winston.library;

import java.text.Collator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Stack;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class FragmentWalker {

    private final Set<FragmentNode> start;
    
    private final Stack<FragmentNode> pathToStart;
    
    private FragmentNode current;
    
    public FragmentWalker(Set<FragmentNode> start) {
        // We must deeply and defensively copy the nodes to prohibit mutation of the tree. 
        this.start = Sets.newHashSet();
        for (FragmentNode current : start) {
            this.start.add(new FragmentNode(current));
        }
        this.pathToStart = new Stack<FragmentNode>();
    }
    
    public CircularSet getOptions() {
        Set<String> returnSet = Sets.newTreeSet(Collator.getInstance());
        if (current == null) {
            for (FragmentNode current : this.start) {
                returnSet.add(current.getFragment());
            }
        } else {
            for (FragmentNode current : this.current.getNexts()) {
                returnSet.add(current.getFragment());
            }
        }
        return new CircularSet(returnSet);
    }

    public List<String> pathFromStart() {
        List<String> returnVal = Lists.newLinkedList();
        for (FragmentNode current : this.pathToStart) {
            returnVal.add(0, current.getFragment());
        }
        return returnVal;
    }
    
    public String getCurrentFragments() {
        StringBuilder strBuilder = new StringBuilder();
        List<String> fragmentsSelected = pathFromStart();
        for (String current: fragmentsSelected) {
            strBuilder.append(current);
            strBuilder.append(" ");
        }
        if (current != null) {
            strBuilder.append(current.getFragment());
            strBuilder.append(" ");
        }
        return strBuilder.toString().trim();
    }

    public void useOption(String option) {
        FragmentNode choice = null;
        if (current == null) {
            choice = findChoice(option, this.start);
        } else {
            choice = findChoice(option, this.current.getNexts());
        }
        if (choice == null) {
            throw new NoSuchElementException("option is not in the set of available choices.");
        }
        if (this.current != null) {
            this.pathToStart.add(this.current);
        }
        this.current = choice;
    }

    @VisibleForTesting
    FragmentNode findChoice(String choice, final Set<FragmentNode> nodes) {
        for (FragmentNode current : nodes) {
            if (current.getFragment().equals(choice)) {
                return current;
            }
        }
        return null;
    }
    
    public void start() {
        this.current = null;
        this.pathToStart.clear();
    }

    public boolean isLeaf() {
        if (current == null) {
            return this.start.isEmpty();
        } else {
            return this.current.getNexts().isEmpty();
        }
    }
    
    public void decrementChoice() {
        if (pathToStart.isEmpty()) {
            this.current = null;
            return;
        }
        this.current = this.pathToStart.lastElement();
        this.pathToStart.pop();
    }
}