package com.voxgift.winston.library;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class FragmentsBuilder {

    private final Set<FragmentNode> nodes;
    
    public FragmentsBuilder(InputStream input) throws IOException {
        nodes = Sets.newHashSet();
        CSVReader csvReader = new CSVReader(new InputStreamReader(input), ',');
        csvReader.readNext();
        String[] currentLine = csvReader.readNext();
        while (currentLine != null) {
            this.addLine(currentLine);
            currentLine = csvReader.readNext();
        }
    }
    
    public FragmentsBuilder() {
        nodes = Sets.newHashSet();
    }
    
    
    public void addLine(String[] line) {
        Preconditions.checkNotNull(line);
        Preconditions.checkArgument(line.length > 2, "Assuming (currently) that there are at least 2 columns in csv.");
        List<String> lineList = Lists.newArrayList(line);
        Iterator<String> lineIterator = lineList.iterator();
        lineIterator.next(); //dumping first column
        String firstActualColumn = lineIterator.next();
        boolean found = false;
        for (FragmentNode existingFragmentChain : nodes) {
            if (existingFragmentChain.getFragment().equals(firstActualColumn)) {
                existingFragmentChain.appendList(lineIterator);
                found = true;
                break;
            }
        }
        if (!found) {
            FragmentNode fragmentNodeBeignBuilt = new FragmentNode(firstActualColumn);
            fragmentNodeBeignBuilt.appendList(lineIterator);
            nodes.add(fragmentNodeBeignBuilt);
        }
    }
    
    public Set<FragmentNode> getRootNodes() {
        return this.nodes;
    }
}
