package org.somewhere;

import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import javax.xml.bind.JAXBException;

import org.junit.Before;
import org.junit.Test;

public class SentencesBuilderTest {

	private SentencesBuilder objUnderTest;
	
	@Before
	public void setUp() throws JAXBException {
		objUnderTest = new SentencesBuilder();
	}
	
	private void simpleSetUp(OutputStream out) throws JAXBException {
		String[] line = {"I_am_hungry", "I", "am", "hungry"};
		objUnderTest.addLine(line);
		objUnderTest.write(out);
	}
	
	@Test
	public void basicNameTest() throws JAXBException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		simpleSetUp(output);
		assertThat(output.toString(), containsString("I_am_hungry"));
	}
	
	@Test
	public void xmlNameTest() throws JAXBException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		simpleSetUp(output);
		assertThat(output.toString(), containsString("name=\"I_am_hungry\""));
	}
	
	@Test
	public void xmlValueTest() throws JAXBException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		simpleSetUp(output);
		assertThat(output.toString(), containsString("value=\"I am hungry\""));
	}
	
	@Test
	public void xmlHeaderTest() throws JAXBException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		simpleSetUp(output);
		assertThat(output.toString(), startsWith("<?xml"));
	}
	
	@Test
	public void xmlGrammarStartTest() throws JAXBException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		simpleSetUp(output);
		assertThat(output.toString(), containsString("<grammar>"));
	}
	
	@Test
	public void xmlGrammarEndTest() throws JAXBException {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		simpleSetUp(output);
		assertThat(output.toString(), containsString("</grammar>"));
	}
	
	@Test
	public void extraSpaces() throws JAXBException {
		String[] line = {"That's_all_folks!", "That's", "all", "folks!", "  ", ""};
		objUnderTest.addLine(line);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		objUnderTest.write(output);
		assertThat(output.toString(), containsString("value=\"That's all folks!\""));
	}
	
}
