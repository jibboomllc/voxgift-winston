package org.somewhere;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.Lists;

public class FragmentNodeTest {

    @Test
    public void appendLine_emptyIterator() {
        FragmentNode objUnderTest = new FragmentNode("first");
        List<String> emptyList = Lists.newArrayList();
        objUnderTest.appendList(emptyList.iterator());
        
        assertTrue(objUnderTest.getNexts().isEmpty());
    }
    
    @Test
    public void appendLine_fewItems() {
        FragmentNode objUnderTest = new FragmentNode("first");
        List<String> list = Lists.newArrayList();
        list.add("second");
        list.add("third");
        list.add("fourth");
        objUnderTest.appendList(list.iterator());
        
        Iterator<FragmentNode> itr = objUnderTest.getNexts().iterator();
        FragmentNode fragmentNode = itr.next();
        assertThat(fragmentNode.getFragment(), equalTo("second"));
        assertThat(fragmentNode.getNexts().size(), equalTo(1));
    }
    
    @Test
    public void appendLine_testingBranching() {
        FragmentNode objUnderTest = new FragmentNode("first");
        List<String> list = Lists.newArrayList();
        list.add("firstNested");
        list.add("secondNested");
        list.add("thirdNested1");
        objUnderTest.appendList(list.iterator());
        
        list = Lists.newArrayList();
        list.add("firstNested");
        list.add("secondNested");
        list.add("thirdNested2");
        objUnderTest.appendList(list.iterator());
        
        Iterator<FragmentNode> itr = objUnderTest.getNexts().iterator();
        FragmentNode firstNestedNode = itr.next();
        assertThat(firstNestedNode.getFragment(), equalTo("firstNested"));
        
        itr = firstNestedNode.getNexts().iterator();
        FragmentNode secondNestedNode = itr.next();
        assertThat(secondNestedNode.getFragment(), equalTo("secondNested"));
        assertThat(secondNestedNode.getNexts().size(), equalTo(2));
        
        Set<FragmentNode> fragmentNodeSet = secondNestedNode.getNexts();
        boolean foundThirdNested1 = false;
        boolean foundThirdNested2 = false;
        for (FragmentNode current : fragmentNodeSet) {
            if (current.getFragment().equals("thirdNested1")) {
                foundThirdNested1 = true;
            }
            if (current.getFragment().equals("thirdNested2")) {
                foundThirdNested2 = true;
            }
        }
        assertTrue(foundThirdNested1);
        assertTrue(foundThirdNested2);
        
    }
    
}
