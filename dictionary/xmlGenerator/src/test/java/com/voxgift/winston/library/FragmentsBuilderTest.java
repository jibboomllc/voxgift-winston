package com.voxgift.winston.library;

import static com.voxgift.winston.library.Utility.randStr;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Set;

import org.junit.Test;

public class FragmentsBuilderTest {
   
    @Test
    public void MultipleRoots() {
        FragmentsBuilder objUnderTest = new FragmentsBuilder();
        objUnderTest.addLine(new String[] {"JUNK", "Root0", randStr(), randStr(), randStr()});
        objUnderTest.addLine(new String[] {"JUNK", "Root1", randStr(), randStr(), randStr()});
        objUnderTest.addLine(new String[] {"JUNK", "Root2", randStr(), randStr(), randStr()});
        objUnderTest.addLine(new String[] {"JUNK", "Root3", randStr(), randStr(), randStr()});
        objUnderTest.addLine(new String[] {"JUNK", "Root4", randStr(), randStr(), randStr()});
        objUnderTest.addLine(new String[] {"JUNK", "Root2", randStr(), randStr(), randStr()});
        objUnderTest.addLine(new String[] {"JUNK", "Root4", randStr(), randStr(), randStr()});
        objUnderTest.addLine(new String[] {"JUNK", "Root3", randStr(), randStr(), randStr()});
        objUnderTest.addLine(new String[] {"JUNK", "Root1", randStr(), randStr(), randStr()});
        objUnderTest.addLine(new String[] {"JUNK", "Root1", randStr(), randStr(), randStr()});
        objUnderTest.addLine(new String[] {"JUNK", "Root1", randStr(), randStr(), randStr()});
        
        assertThat(objUnderTest.getRootNodes().size(), equalTo(5));
    }
    

    @Test
    public void predictableTree2() {
        FragmentsBuilder builder = new FragmentsBuilder();
        builder.addLine(new String[] { "JUNK", "I", "am", "thirsty", "" });
        builder.addLine(new String[] { "JUNK", "I", "am", "hungry", "" });
        assertThat(builder.getRootNodes().size(), equalTo(1));
        FragmentNode node_I = (FragmentNode)builder.getRootNodes().toArray()[0];
        FragmentNode node_am  = node_I.getNexts().toArray(new FragmentNode[1])[0];
        assertThat(node_am.getFragment(), equalTo("am"));
        FragmentNode node_thirstyOrHungry = node_am.getNexts().toArray(new FragmentNode[2])[0];
        assertThat(node_thirstyOrHungry.isLeafNode(), equalTo(true));
    }
}
