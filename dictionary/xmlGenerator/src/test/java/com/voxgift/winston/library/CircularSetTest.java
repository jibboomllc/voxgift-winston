package com.voxgift.winston.library;
import java.text.Collator;
import java.util.Set;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.junit.Test;

import com.google.common.collect.Sets;


public class CircularSetTest {

    private CircularSet objUnderTest;
    
    private void simpleSetOfThreeSetUp() {
        Set<String> data = Sets.newTreeSet(Collator.getInstance());
        data.add("a");
        data.add("b");
        data.add("c");
        objUnderTest = new CircularSet(data);
    }
    
    @Test
    public void before_initialWith3() {
        simpleSetOfThreeSetUp();
        assertThat(objUnderTest.getBefore(), equalTo("c"));
    }
    
    @Test
    public void current_initialWith3() {
        simpleSetOfThreeSetUp();
        assertThat(objUnderTest.getCurrent(), equalTo("a"));
    }
    
    @Test
    public void after_initialWith3() {
        simpleSetOfThreeSetUp();
        assertThat(objUnderTest.getAfter(), equalTo("b"));
    }
    
    @Test
    public void increase_testCurrent_initialWith3() {
        simpleSetOfThreeSetUp();
        objUnderTest.increaseIndex();
        assertThat(objUnderTest.getCurrent(), equalTo("b"));
    }
    
    @Test
    public void increase_testAfter_initialWith3() {
        simpleSetOfThreeSetUp();
        objUnderTest.increaseIndex();
        assertThat(objUnderTest.getAfter(), equalTo("c"));
    }
    
    @Test
    public void increase__testBefore_initialWith3() {
        simpleSetOfThreeSetUp();
        objUnderTest.increaseIndex();
        assertThat(objUnderTest.getBefore(), equalTo("a"));
    }
    
    @Test
    public void decrease_testCurrent_initialWith3() {
        simpleSetOfThreeSetUp();
        objUnderTest.decreaseIndex();
        assertThat(objUnderTest.getCurrent(), equalTo("c"));
    }
    
    @Test
    public void decrease_testAfter_initialWith3() {
        simpleSetOfThreeSetUp();
        objUnderTest.decreaseIndex();
        assertThat(objUnderTest.getAfter(), equalTo("a"));
    }
    
    @Test
    public void decrease__testBefore_initialWith3() {
        simpleSetOfThreeSetUp();
        objUnderTest.decreaseIndex();
        assertThat(objUnderTest.getBefore(), equalTo("b"));
    }
}
