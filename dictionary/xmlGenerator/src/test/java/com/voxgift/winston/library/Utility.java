package com.voxgift.winston.library;

import java.util.UUID;

public class Utility {

    public static final String randStr() {
        return UUID.randomUUID().toString();
    }
}
