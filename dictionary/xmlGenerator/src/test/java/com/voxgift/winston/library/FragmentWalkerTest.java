package com.voxgift.winston.library;

import static com.voxgift.winston.library.Utility.randStr;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;

public class FragmentWalkerTest {

    private Set<FragmentNode> initializeRandomTree() {
        FragmentsBuilder builder = new FragmentsBuilder();
        builder.addLine(new String[] { "JUNK", "Root0", randStr(), randStr(), randStr() });
        builder.addLine(new String[] { "JUNK", "Root1", "A", "B", randStr() });
        builder.addLine(new String[] { "JUNK", "Root2", randStr(), randStr(), randStr() });
        builder.addLine(new String[] { "JUNK", "Root3", randStr(), randStr(), randStr() });
        builder.addLine(new String[] { "JUNK", "Root4", randStr(), randStr(), randStr() });
        builder.addLine(new String[] { "JUNK", "Root2", randStr(), randStr(), randStr() });
        builder.addLine(new String[] { "JUNK", "Root4", randStr(), randStr(), randStr() });
        builder.addLine(new String[] { "JUNK", "Root3", randStr(), randStr(), randStr() });
        builder.addLine(new String[] { "JUNK", "Root1", "A", "B", randStr() });
        builder.addLine(new String[] { "JUNK", "Root1", "A", randStr(), randStr() });
        builder.addLine(new String[] { "JUNK", "Root1", randStr(), "B", randStr() });
        return builder.getRootNodes();
    }

    private Set<FragmentNode> initializePredictableTree() {
        FragmentsBuilder builder = new FragmentsBuilder();
        builder.addLine(new String[] { "JUNK", "I", "am", "thirsty" });
        builder.addLine(new String[] { "JUNK", "I", "am", "hungry" });
        builder.addLine(new String[] { "JUNK", "I", "am", "tired" });
        builder.addLine(new String[] { "JUNK", "I", "was", "sleepy" });
        builder.addLine(new String[] { "JUNK", "I", "was", "tired" });
        builder.addLine(new String[] { "JUNK", "I", "was", "exahusted" });
        builder.addLine(new String[] { "JUNK", "I", "thank", "you" });
        builder.addLine(new String[] { "JUNK", "You", "are", "a jerk"});
        builder.addLine(new String[] { "JUNK", "We", "want", "to party"});
        builder.addLine(new String[] { "JUNK", "They", "need", "to wash"});
        builder.addLine(new String[] { "JUNK", "They", "need", "to eat"});
        builder.addLine(new String[] { "JUNK", "They", "need", "to sleep"});
        builder.addLine(new String[] { "JUNK", "They", "need", "leave", "now"});
        builder.addLine(new String[] { "JUNK", "They", "need", "leave", "later"});
        return builder.getRootNodes();
    }

    private Set<FragmentNode> initializePredictableTree2() {
        FragmentsBuilder builder = new FragmentsBuilder();
        builder.addLine(new String[] { "JUNK", "I", "am", "thirsty" });
        builder.addLine(new String[] { "JUNK", "I", "am", "hungry" });
        builder.addLine(new String[] { "JUNK", "I", "am", "tired" });
        builder.addLine(new String[] { "JUNK", "I", "was", "sleepy" });
        builder.addLine(new String[] { "JUNK", "I", "was", "tired" });
        builder.addLine(new String[] { "JUNK", "I", "was", "exahusted" });
        builder.addLine(new String[] { "JUNK", "I", "love"});
        builder.addLine(new String[] { "JUNK", "I", "thank", "you" });
        builder.addLine(new String[] { "JUNK", "You", "are", "a jerk"});
        builder.addLine(new String[] { "JUNK", "We", "want", "to party"});
        builder.addLine(new String[] { "JUNK", "They", "need", "to wash"});
        builder.addLine(new String[] { "JUNK", "They", "need", "to eat"});
        builder.addLine(new String[] { "JUNK", "They", "need", "to sleep"});
        builder.addLine(new String[] { "JUNK", "They", "need", "leave", "now"});
        builder.addLine(new String[] { "JUNK", "They", "need", "leave", "later"});
        return builder.getRootNodes();
    }
    
    @Test
    public void increaseAndDecrease() {
        FragmentWalker objUnderTest = new FragmentWalker(initializeRandomTree());
        CircularSet circularSet = objUnderTest.getOptions();
        circularSet.increaseIndex();
        circularSet.increaseIndex();
        circularSet.decreaseIndex();
        circularSet.decreaseIndex();
        circularSet.decreaseIndex();
        circularSet.decreaseIndex();
        objUnderTest.useOption(circularSet.getCurrent());  // increase depth to 1.
        circularSet = objUnderTest.getOptions();
        circularSet.increaseIndex();
        objUnderTest.useOption(circularSet.getCurrent());
        objUnderTest.decrementChoice();
        objUnderTest.decrementChoice();
        assertTrue(objUnderTest.getOptions().allStrings().contains("Root1"));
    }
    
    @Test
    public void decreaseOnInitial() {
        FragmentWalker objUnderTest = new FragmentWalker(initializeRandomTree());
        objUnderTest.decrementChoice();
        objUnderTest.decrementChoice();
    }
    
    @Test
    public void incrementTillEnd() {
        FragmentWalker objUnderTest = new FragmentWalker(initializeRandomTree());
        CircularSet circularSet = objUnderTest.getOptions();
        objUnderTest.useOption(circularSet.getCurrent());
        circularSet = objUnderTest.getOptions();
        objUnderTest.useOption(circularSet.getCurrent());
        circularSet = objUnderTest.getOptions();
        objUnderTest.useOption(circularSet.getCurrent());
        circularSet = objUnderTest.getOptions();
        objUnderTest.useOption(circularSet.getCurrent());
        assertTrue(objUnderTest.isLeaf());
    }
    
    @Test
    public void earlyLeaf() {
        FragmentWalker objUnderTest = new FragmentWalker(initializePredictableTree2());
        objUnderTest.useOption("I");
        objUnderTest.useOption("love");
        assertTrue(objUnderTest.isLeaf());
    }

}
