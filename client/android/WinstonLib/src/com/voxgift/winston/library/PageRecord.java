// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PageRecord {
  private long mId;
  private String mRequest;
  private String mRequestor;
  private Date mRequested;
  private Date mAcknowledged;
  private Date mRespondedTo;
  private String mAcknowledgedBy;
  private String mRespondedToBy;

  public PageRecord(long id, String request, String requestor
    , Date requested, Date acknowledged, Date respondedTo
    , String acknowledgedBy, String respondedToBy) {
    mId = id;
    mRequest = request;
    mRequestor = requestor;
    mRequested = requested;
    mAcknowledged = acknowledged;
    mRespondedTo = respondedTo;
    mAcknowledgedBy = acknowledgedBy;
    mRespondedToBy = respondedToBy;
  }

  public PageRecord(long id, String request, String requestor
    , long requested, long acknowledged, long respondedTo
    , String acknowledgedBy, String respondedToBy) {
    mId = id;
    mRequest = request;
    mRequestor = requestor;
    mRequested = new Date(requested);
    mAcknowledged = new Date(acknowledged);
    mRespondedTo = new Date(respondedTo);
    mAcknowledgedBy = acknowledgedBy;
    mRespondedToBy = respondedToBy;
  }

  public String getFormattedDate(SimpleDateFormat formatter, Date date) {
    if (!date.equals(new Date(0L))) {
      return formatter.format(date);
    }
    return ("    ");
  }

  public long getId() {
    return mId;
  }

  public String getRequest() {
    return mRequest;
  }

  public String getRequestor() {
    return mRequestor;
  }

  public Date getRequested() {
    return mRequested;
  }

  public String getRequested(SimpleDateFormat formatter) {
    return getFormattedDate(formatter, mRequested);
  }

  public Date getAcknowledged() {
    return mAcknowledged;
  }

  public String getAcknowledged(SimpleDateFormat formatter) {
    return getFormattedDate(formatter, mAcknowledged);
  }

  public Date getRespondedTo() {
    return mRespondedTo;
  }

  public String getRespondedTo(SimpleDateFormat formatter) {
    return getFormattedDate(formatter, mRespondedTo);
  }


  public String getAcknowledgedBy() {
    return mAcknowledgedBy;
  }

  public String getRespondedToBy() {
    return mRespondedToBy;
  }


  public String toString() {
    return this.mRequest;
  }
}
