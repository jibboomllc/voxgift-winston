// Copyright 2011-2014 Voxgift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.voxgift.winston.library.DragNDrop.DragNDropAdapter;
import com.voxgift.winston.library.DragNDrop.DragNDropListActivity;

public class CustomDictionaryActivity extends DragNDropListActivity {
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.customdictionaryview);
        
        ArrayList<String> content = new ArrayList<String>();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        try {
          JSONArray jArray = new JSONArray(settings.getString("custom", "[]"));
          for (int i=0; i < jArray.length(); i++) {
            try {
                content.add(jArray.getString(i));
            } catch (JSONException e) {
             // Oops
            }
          }
        } catch (JSONException e) {
           // Oops
        }

        super.setContent(content);
        super.onCreate(savedInstanceState);

        getListView().setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editItem(position);
            }
        });

    }

    @Override
    protected void onPause() {
        save();
        super.onPause();
    }

    private void editItem(final int position) {
        final ListAdapter adapter = getListAdapter();
        //android.util.Log.v("winston", "position=" + position + ", getItem()=" + adapter.getItem(position));

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle(R.string.custom);
        alert.setMessage(R.string.custom_summary);

        // Set an EditText view to get user input 
        final EditText input = new EditText(this);
        input.setText(adapter.getItem(position).toString());
        alert.setView(input);

        alert.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int whichButton) {
            if (adapter instanceof DragNDropAdapter) {
                ((DragNDropAdapter)adapter).onEdit(position, input.getText().toString());
                getListView().invalidateViews();
            }

          }
        });

        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
            // Canceled.
          }
        });

        alert.show();
    }

    public void addItem(View v) {
        final ListAdapter adapter = getListAdapter();

        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle(R.string.custom);
        alert.setMessage(R.string.custom_summary);

        // Set an EditText view to get user input 
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int whichButton) {
            if (adapter instanceof DragNDropAdapter) {
                if (input.getText().toString().length() > 0) {
                    ((DragNDropAdapter)adapter).onAdd(input.getText().toString());
                    getListView().invalidateViews();
                }
            }

          }
        });

        alert.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int whichButton) {
            // Canceled.
          }
        });

        alert.show();
    }

    public void done(View v) {
        save();
        this.finish();
    }

    public void save() {
        ListAdapter adapter = getListAdapter();
        //android.util.Log.v("winston", "getCount=" + adapter.getCount() + ", getItem(0)=" + adapter.getItem(0));

        JSONArray jArray = new JSONArray();
        for (int i=0; i < adapter.getCount(); i++) {
            jArray.put(adapter.getItem(i));
        }
        //android.util.Log.v("winston", "str=" + jArray.toString());

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = settings.edit();
        //editor.putString("custom", "[\"foo\",\"bar\"]");
        editor.putString("custom", jArray.toString());
        editor.commit();
    }
}