// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

/***
 * Based on http://developer.android.com/resources/articles/speech-input.html.
 */
public class SpeakActivity extends WinstonActivity {
  public static final int TITLE_ID = R.string.speak;
  public static final int SUBTITLE_ID = R.string.speak_button_prompt;
  public static final int ICON_ID = R.drawable.ic_speak;

  private ImageButton speakButton;
  private static final int VOICE_RECOGNITION_REQUEST_CODE = 1234;
  private static final String EXTRA_CALLING_PACKAGE = "calling_package";

  private List<Button> pageButtons;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.speak);

    TextView textView = (TextView) findViewById(R.id.titleLabel);
    textView.setText(TITLE_ID);

    speakButton = (ImageButton) findViewById(R.id.speak_button);

    // Check to see if a recognition activity is present
    PackageManager pm = getPackageManager();
    List<ResolveInfo> activities = pm.queryIntentActivities(
      new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0);

    if (activities.size() != 0) {
      speakButton.setOnClickListener(new OnClickListener() {
        @Override
        public void onClick(View v) {
          startVoiceRecognitionActivity();
        }
      });
    } else {
      speakButton.setEnabled(false);
    }

    pageButtons = new ArrayList<Button>();
    pageButtons.add((Button) findViewById(R.id.page_button0));
    pageButtons.add((Button) findViewById(R.id.page_button1));
    pageButtons.add((Button) findViewById(R.id.page_button2));
    pageButtons.add((Button) findViewById(R.id.page_button3));
    pageButtons.add((Button) findViewById(R.id.page_button4));

    for (Button pageButton : pageButtons) {
      pageButton.setEnabled(false);
      pageButton.setOnClickListener(new MyOnClickListener(pageButton));
    }
  }

  public class MyOnClickListener implements OnClickListener {
    private Button button;

    public MyOnClickListener(Button b) {
      this.button = b;
    }

    @Override
    public void onClick(View v) {
      WinstonApp winstonApp = (WinstonApp) (SpeakActivity.this.getApplication());
      winstonApp.page(this.button.getText().toString());
    }
  }

  /**
   * Fire an intent to start the speech recognition activity.
   */
  private void startVoiceRecognitionActivity() {
    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

    // Specify the calling package to identify your application
    intent.putExtra(EXTRA_CALLING_PACKAGE, getClass().getPackage().getName());

    // Display an hint to the user about what he should say.
    intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getResources().getString(R.string.speak_hint));

    // Given an hint to the recognizer about what the user is going to say
    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
      RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);

    // Specify how many results you want to receive. The results will be sorted
    // where the first result is the one with higher confidence.
    intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);

    startActivityForResult(intent, VOICE_RECOGNITION_REQUEST_CODE);
  }

  /**
   * Handle the results from the recognition activity.
   */
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
      // Fill the list view with the strings the recognizer thought it could have heard
      ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
      for (int i = 0; i < matches.size() && i < pageButtons.size(); i++) {
        pageButtons.get(i).setEnabled(true);
        pageButtons.get(i).setText(matches.get(i));
      }
    }

    super.onActivityResult(requestCode, resultCode, data);
  }

}
