// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

public class GrammarString {
  private String str;
  private Long id;
  private Long next;

  public GrammarString(Long i, Long n, String s) {
    this.id = i;
    this.next = n;
    this.str = s;
  }

  public Long getId() {
    return this.id;
  }

  public Long getNext() {
    return this.next;
  }

  public String toString() {
    return this.str;
  }
}
