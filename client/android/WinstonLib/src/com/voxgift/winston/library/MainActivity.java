// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.Menu;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

public class MainActivity extends WinstonActivity {

  protected List<Class<?>> mMenuItems;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
    mMenuItems = new ArrayList<Class<?>>();
    //android.util.Log.d("winston", "appname = " + R.id.appName);
    final TextView tv = (TextView) findViewById(R.id.appName);
    //android.util.Log.d("winston", "tv = " + tv);
    ViewTreeObserver vto = tv.getViewTreeObserver();
    vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
      @Override
      public void onGlobalLayout() {
        resizeText(tv);
      }
    });
  }

  // Override the default since we don't want to finish
  // this activity.
  @Override
  public void startActivity(Class<?> cls) {
    startActivity(new Intent(this, cls));
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    //inflater.inflate(R.menu.main_options, menu);
    inflater.inflate(R.menu.options, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == R.id.home) {
      // we are already here so eat this
      return true;
    } else {
      return super.onOptionsItemSelected(item);
    }
  }

  public void resizeText(int resourceId) {
    TextView textView = (TextView) findViewById(resourceId);
    resizeText(textView);
  }

  public void resizeText(TextView textView) {
    CharSequence text = textView.getText();
    TextPaint textPaint = textView.getPaint();
    float width = (float) (textView.getWidth() * 0.80);

    //android.util.Log.d("winston", "top: " + textView.getTop() + ", ts:" + textPaint.getTextSize());
    if (width > 0) {
      while (text == TextUtils.ellipsize(text, textPaint, width, TextUtils.TruncateAt.END)) {
        textPaint.setTextSize(textPaint.getTextSize() + 5);
      }
      while (text != TextUtils.ellipsize(text, textPaint, width, TextUtils.TruncateAt.END)) {
        textPaint.setTextSize(textPaint.getTextSize() - 5);
      }
    }
  }
}
