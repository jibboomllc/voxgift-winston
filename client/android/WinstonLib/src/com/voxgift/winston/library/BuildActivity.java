// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.io.IOException;
import java.io.InputStream;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.voxgift.winston.library.widget.FragometerSpinner;
import com.voxgift.winston.library.widget.FragometerSpinner.OnFragmentGroupingChangeListener;
import com.voxgift.winston.library.widget.FragometerSpinner.OnWordChangeListener;

public class BuildActivity extends WinstonActivity implements OnFragmentGroupingChangeListener, OnWordChangeListener {
  public static final int TITLE_ID = R.string.build;
  public static final int SUBTITLE_ID = R.string.build_button_prompt;
  public static final int ICON_ID = R.drawable.ic_build;

  private FragometerSpinner fragometerSpinner;
  private FragmentWalker fragmentWalker;
  private TextView textDisplay;
  private Vibrator mVibrator;
  private Button mTalkButton;
  private Button mPageButton;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //android.util.Log.v("winston", "BuildActivity.onCreate START");
    setContentView(R.layout.build);

    TextView textView = (TextView) findViewById(R.id.titleLabel);
    textView.setText(TITLE_ID);

    FragmentsBuilder builder = null;
    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    if (settings.getString("setting", "general").equals("custom")) {
          builder = new FragmentsBuilder(getDisplayDictionary());
    } else {
      InputStream in = getResources().openRawResource(R.raw.dictionary);
      try {
          builder = new FragmentsBuilder(in, getDisplayDictionary());
      } catch (IOException e) {
          //android.util.Log.e("winston", "Unable to read CSV file.");
          throw new RuntimeException(e);
      }
    }
    fragmentWalker = new FragmentWalker(builder.getRootNodes());
    /* */
    fragometerSpinner = (FragometerSpinner) findViewById(R.id.fragometer_spinner);
    fragometerSpinner.setOnChangeListListner(this);
    fragometerSpinner.setOnWorkdChangeListener(this);
    fragometerSpinner.setChoices(fragmentWalker.getOptions());
    textDisplay = (TextView) findViewById(R.id.valuedisplay);
    //android.util.Log.v("winston", "BuildActivity.onCreate CALL fragometer.setFragments");

    mVibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);

    /* */
    //android.util.Log.v("winston", "BuildActivity.onCreate --> creating buttons");
    mTalkButton = (Button) findViewById(R.id.talk_button);
    mTalkButton.setOnClickListener(new OnClickListener() {
    @Override
      public void onClick(View v) {
        WinstonApp winstonApp = (WinstonApp) (BuildActivity.this.getApplication());
        winstonApp.speak(BuildActivity.this.getText());

        // May return null if a EasyTracker has not yet been initialized with a
        // property ID.
        EasyTracker easyTracker = EasyTracker.getInstance(BuildActivity.this);

        // MapBuilder.createEvent().build() returns a Map of event fields and values
        // that are set and sent with the hit.
        easyTracker.send(MapBuilder
            .createEvent("Build",         // Event category (required)
                         "Speak",         // Event action (required)
                         BuildActivity.this.getText(),   // Event label
                         null)            // Event value
            .build()
        );
      }
    });

    mPageButton = (Button) findViewById(R.id.page_button);
    mPageButton.setOnClickListener(new OnClickListener() {
    @Override
      public void onClick(View v) {
        WinstonApp winstonApp = (WinstonApp) (BuildActivity.this.getApplication());
        winstonApp.page(BuildActivity.this.getText());

        // May return null if a EasyTracker has not yet been initialized with a
        // property ID.
        EasyTracker easyTracker = EasyTracker.getInstance(BuildActivity.this);

        // MapBuilder.createEvent().build() returns a Map of event fields and values
        // that are set and sent with the hit.
        easyTracker.send(MapBuilder
            .createEvent("Build",         // Event category (required)
                         "Page",         // Event action (required)
                         BuildActivity.this.getText(),   // Event label
                         null)            // Event value
            .build()
        );
      }
    });

    wordChanged();

    //android.util.Log.v("winston", "BuildActivity.onCreate DONE");
  }

  public String getText() {
    String currentFragments = this.fragmentWalker.getCurrentFragments();
    if (currentFragments.length() > 0) {
      currentFragments = currentFragments + " ";
    }
    return currentFragments + this.fragometerSpinner.getCurrentChoice();
  }

  @Override
  public void increaseChangeList(String choice) {
    this.fragmentWalker.useOption(choice);
    if (this.fragmentWalker.isLeaf()) {
      mVibrator.vibrate(50);
      this.fragmentWalker.decrementChoice();
    }
    CircularSet options = this.fragmentWalker.getOptions();
    this.fragometerSpinner.setChoices(options);
    this.fragometerSpinner.invalidate();
  }

  @Override
  public void decreaseChangeList() {
    if (this.fragmentWalker.isRoot()) {
      mVibrator.vibrate(50);
    }

    this.fragmentWalker.decrementChoice();
    this.fragometerSpinner.setChoices(this.fragmentWalker.getOptions());
    this.fragometerSpinner.invalidate();
  }

  @Override
  public void wordChanged() {

    if (this.fragometerSpinner.getCurrentChoice() != null) {
      String str = this.fragmentWalker.getCurrentFragments();
      if (str.length() > 0) {
        str += " ";
      }
      String htmlStr = str + "<u>" + this.fragometerSpinner.getCurrentChoice() + "</u>";

      this.textDisplay.setText(Html.fromHtml(htmlStr));
      this.textDisplay.invalidate();

      str += this.fragometerSpinner.getCurrentChoice();

      mTalkButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_talk, 0, 0);
      mTalkButton.setEnabled(true);
      mPageButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_page, 0, 0);
      mPageButton.setEnabled(true);
    } else {
      mTalkButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_talk_grey, 0, 0);
      mTalkButton.setEnabled(false);
      mPageButton.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_page_grey, 0, 0);
      mPageButton.setEnabled(false);
    }

  }
}
