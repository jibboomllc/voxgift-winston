// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.HttpEntity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class MessagelogAdapter extends BaseAdapter {

  private Context mContext;
  private List<MessageRecord> mMessageLogs;
  private String mDeviceId;
  private String mIpAddress;

  public MessagelogAdapter(Context context, List<MessageRecord> messageLogs, String deviceId, String ipAddress) {
    mContext = context;
    mMessageLogs = messageLogs;
    mDeviceId = deviceId;
    mIpAddress = ipAddress;
  }

  public int getCount() {
    return mMessageLogs.size();
  }

  public Object getItem(int position) {
    return mMessageLogs.get(position);
  }

  public long getItemId(int position) {
    return position;
  }

  public View getView(int position, View convertView, ViewGroup viewGroup) {
    SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
    final MessageRecord messageRecord = mMessageLogs.get(position);

    // populate with std info
    if (convertView == null) {
      LayoutInflater inflater = (LayoutInflater) mContext
              .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      convertView = inflater.inflate(R.layout.message_row, null);
    }

    TextView tvTopic = (TextView) convertView.findViewById(R.id.tvTopic);
    tvTopic.setText(messageRecord.getCreated(formatter) + ' ' + messageRecord.getTopic());

    TextView tvNote = (TextView) convertView.findViewById(R.id.tvNote);
    tvNote.setText(messageRecord.getNote());

    return convertView;
  }
}
