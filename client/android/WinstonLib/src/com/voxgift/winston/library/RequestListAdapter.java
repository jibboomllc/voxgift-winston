// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.HttpEntity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class RequestListAdapter extends BaseAdapter {

  private Context mContext;
  private List<PageRecord> mPageLogs;
  private String mDeviceId;
  private String mIpAddress;

  public RequestListAdapter(Context context, List<PageRecord> pageLogs, String deviceId, String ipAddress) {
    mContext = context;
    mPageLogs = pageLogs;
    mDeviceId = deviceId;
    mIpAddress = ipAddress;
  }

  public int getCount() {
    return mPageLogs.size();
  }

  public Object getItem(int position) {
    return mPageLogs.get(position);
  }

  public long getItemId(int position) {
    return position;
  }

  public View getView(int position, View convertView, ViewGroup viewGroup) {
    SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
    final PageRecord pageRecord = mPageLogs.get(position);

    // populate with std info
    if (convertView == null) {
      LayoutInflater inflater = (LayoutInflater) mContext
              .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      convertView = inflater.inflate(R.layout.request_row, null);
    }
    TextView tvName = (TextView) convertView.findViewById(R.id.tvName);
    tvName.setText(pageRecord.getRequested(formatter) + ' ' + pageRecord.getRequestor());

    TextView tvRequest = (TextView) convertView.findViewById(R.id.tvRequest);
    tvRequest.setText(pageRecord.getRequest());

    TextView tvLastAck = (TextView) convertView.findViewById(R.id.tvLastAck);
    Date notAcked = new Date(0L);
    if (!pageRecord.getAcknowledged().equals(notAcked)) {
      tvLastAck.setText(pageRecord.getAcknowledged(formatter) + ' ' + pageRecord.getAcknowledgedBy());
    } else {
      tvLastAck.setText("");
    }

    Button btnAck = (Button) convertView.findViewById(R.id.btnAck);
    btnAck.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        postAction("ack", pageRecord.getId());
      }
    });

    Button btnClose = (Button) convertView.findViewById(R.id.btnClose);
    btnClose.setOnClickListener(new OnClickListener() {
      public void onClick(View view) {
        postAction("close", pageRecord.getId());
      }
    });

    return convertView;
  }

  public void postAction(String action, long id) {
    String postString = "http://" + mIpAddress + "/rest/" + action;
    try {
      postString += "/id/" + id;
      postString += "/device_id/" + URLEncoder.encode(mDeviceId, "UTF-8");
    } catch (UnsupportedEncodingException ex) {
      throw new RuntimeException("Broken VM does not support UTF-8");
    }

    HttpClient httpclient = new DefaultHttpClient();
    HttpPost httppost = new HttpPost(postString);

    try {
      //android.util.Log.v("winston", postString);
      HttpResponse response = httpclient.execute(httppost);
      HttpEntity entity = response.getEntity();
      if (entity != null) {
        //android.util.Log.v("winston", postString + ": success");
        ((RequestsActivity) mContext).updateList();

      }
    } catch (ClientProtocolException e) {
        //android.util.Log.e("winston", "ClientProtocolException: " + e);
    } catch (IOException e) {
        //android.util.Log.e("winston", "IOException: " + e);
    }
  }

}
