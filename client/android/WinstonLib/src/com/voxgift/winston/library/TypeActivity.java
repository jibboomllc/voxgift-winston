// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class TypeActivity extends WinstonActivity {
  public static final int TITLE_ID = R.string.type;
  public static final int SUBTITLE_ID = R.string.type_button_prompt;
  public static final int ICON_ID = R.drawable.ic_type;

  private EditText mInputText;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.type);

    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    float textSize = (float) 22.0 * Float.valueOf(settings.getString("typeTextSize", "1.0"));

    TextView textView = (TextView) findViewById(R.id.titleLabel);
    textView.setText(TITLE_ID);

    mInputText = (EditText) findViewById(R.id.input_text);
    mInputText.setTextSize(textSize);

    Button talkButton = (Button) findViewById(R.id.talk_button);
    talkButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        WinstonApp winstonApp = (WinstonApp) (TypeActivity.this.getApplication());
        winstonApp.speak(mInputText.getText().toString());
        // May return null if a EasyTracker has not yet been initialized with a
        // property ID.
        EasyTracker easyTracker = EasyTracker.getInstance(TypeActivity.this);

        // MapBuilder.createEvent().build() returns a Map of event fields and values
        // that are set and sent with the hit.
        easyTracker.send(MapBuilder
            .createEvent("Type",          // Event category (required)
                         "Speak",         // Event action (required)
                         mInputText.getText().toString(), // Event label
                         null)            // Event value
            .build()
        );
      }
    });

    Button pageButton = (Button) findViewById(R.id.page_button);
    pageButton.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        WinstonApp winstonApp = (WinstonApp) (TypeActivity.this.getApplication());
        winstonApp.page(mInputText.getText().toString());
        // May return null if a EasyTracker has not yet been initialized with a
        // property ID.
        EasyTracker easyTracker = EasyTracker.getInstance(TypeActivity.this);

        // MapBuilder.createEvent().build() returns a Map of event fields and values
        // that are set and sent with the hit.
        easyTracker.send(MapBuilder
            .createEvent("Type",          // Event category (required)
                         "Page",          // Event action (required)
                         mInputText.getText().toString(), // Event label
                         null)            // Event value
            .build()
        );
      }
    });
  }
}
