// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.HttpEntity;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

public class StatusActivity extends WinstonActivity {
  public static final int TITLE_ID = R.string.status;
  public static final int SUBTITLE_ID = R.string.status_button_prompt;
  public static final int ICON_ID = R.drawable.ic_status;
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (!isNetworkAvailable()) {
      showDialog(DIALOG_NO_CONN_ID);
    } else {
      setContentView(R.layout.status);

      TextView textView = (TextView) findViewById(R.id.titleLabel);
      textView.setText(TITLE_ID);

      SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
      Boolean onDuty = settings.getBoolean("onDuty", true);
      int availability = settings.getInt("availability", 1);

      ToggleButton btnOnDuty = (ToggleButton) findViewById(R.id.on_duty);
      btnOnDuty.setChecked(onDuty);
      btnOnDuty.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View v) {
          enableAvailability(((ToggleButton) v).isChecked());
        }
      });
      enableAvailability(onDuty);

      RadioGroup rgAvailability = (RadioGroup) findViewById(R.id.availability);
      rgAvailability.check(R.id.rdb1 - 1 + availability);

      Button btnCancel = (Button) findViewById(R.id.btnCancel);
      btnCancel.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View v) {
          finish();
        }
      });

      Button btnSave = (Button) findViewById(R.id.btnSave);
      btnSave.setOnClickListener(new View.OnClickListener()
      {
        public void onClick(View v) {
          // save settings
          SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
          Editor editor = settings.edit();
          editor.putBoolean("onDuty", ((ToggleButton) findViewById(R.id.on_duty)).isChecked());
          editor.putInt("availability", ((RadioGroup) findViewById(R.id.availability)).getCheckedRadioButtonId() - R.id.rdb1 + 1);
          editor.commit();

          // persist to server
          String deviceId = Secure.getString(StatusActivity.this.getContentResolver(), Secure.ANDROID_ID);
          String ipAddress = settings.getString("ipAddress", null);
          Boolean onDuty = settings.getBoolean("onDuty", true);
          Integer availability = settings.getInt("availability", 1);

          String postString = "http://" + ipAddress + "/rest/status";
          try {
            postString += "/device_id/" + URLEncoder.encode(deviceId, "UTF-8");
            postString += "/on_duty/" + onDuty;
            postString += "/availability/" + availability;
          } catch (UnsupportedEncodingException ex) {
            showDialog(DIALOG_UE_EX_ID);
            throw new RuntimeException("Broken VM does not support UTF-8");
          }

          HttpClient httpclient = new DefaultHttpClient();
          HttpPost httppost = new HttpPost(postString);

          try {
            //android.util.Log.v("winston", postString);
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
              //android.util.Log.v("winston", postString + ": success");
            }
          } catch (ClientProtocolException e) {
              //android.util.Log.e("winston", "ClientProtocolException: " + e);
              showDialog(DIALOG_CP_EX_ID);
          } catch (IOException e) {
              //android.util.Log.e("winston", "IOException: " + e);
              showDialog(DIALOG_IO_EX_ID);
          }

          // close activity
          finish();
        }
      });

    }
  }

  public void onDestroy() {
    super.onDestroy();
  }

  public void enableAvailability(boolean enabled) {
    String hexColor = (enabled) ? "#f4f0f0f0" : "#88f0f0f0";
    TextView tvAvailabilityLabel = (TextView) findViewById(R.id.availability_label);
    tvAvailabilityLabel.setTextColor(Color.parseColor(hexColor));
    RadioGroup rgAvailability = (RadioGroup) findViewById(R.id.availability);
    rgAvailability.setEnabled(enabled);
    for (int i = 0; i < rgAvailability.getChildCount(); i++) {
      ((RadioButton) rgAvailability.getChildAt(i)).setEnabled(enabled);
    }
  }

}
