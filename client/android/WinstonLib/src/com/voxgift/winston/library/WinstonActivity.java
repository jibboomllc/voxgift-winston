// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.speech.tts.TextToSpeech;
import android.speech.tts.TextToSpeech.OnInitListener;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class WinstonActivity extends Activity implements OnInitListener {
  private static final int MY_DATA_CHECK_CODE = 0;
  private TextToSpeech mTts;
  private int mTtsStatus = TextToSpeech.ERROR;
  private boolean mTalk;

  public static final String DEFAULT_LANG = "en";

  public static final int DIALOG_NO_CONN_ID = 2;
  public static final int DIALOG_UE_EX_ID = 3;
  public static final int DIALOG_CP_EX_ID = 4;
  public static final int DIALOG_IO_EX_ID = 5;

  public static final String REFRESH_DATA_INTENT = new String("com.voxgift.winston.action.REFRESH_DATA_INTENT");

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //android.util.Log.d("winston", "+++WinstonActivity.onCreate");

    Intent checkIntent = new Intent();
    checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
    startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);

    this.setVolumeControlStream(AudioManager.STREAM_MUSIC);
  }

  @Override
  public void onInit(int status) {
    //android.util.Log.d("winston", "+++WinstonActivity.onInit");
    mTtsStatus = status;
    if (status == TextToSpeech.SUCCESS) {
      SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
      WinstonApp winstonApp = (WinstonApp) (getApplication());
      setTtsLanguage(settings.getString("outputLanguage", DEFAULT_LANG));
      winstonApp.setTalk(settings.getString("outputLanguage", DEFAULT_LANG), mTts, this);
    } else if (status == TextToSpeech.ERROR) {
      Toast.makeText(this,  "Error occurred while initializing Text-To-Speech engine", Toast.LENGTH_LONG).show();
      mTalk = false;
    }
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    //android.util.Log.d("winston", "WinstonActivity.onSaveInstanceState");
  }

  @Override
  public void onRestoreInstanceState(Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    //android.util.Log.d("winston", "WinstonActivity.onRestoreInstanceState");
  }

  @Override
  protected void onResume() {
    super.onResume();
    //android.util.Log.d("winston", "+++WinstonActivity.onResume");
    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    WinstonApp winstonApp = (WinstonApp) (getApplication());

    winstonApp.setSendReceive(Secure.getString(this.getContentResolver(), Secure.ANDROID_ID)
      , settings.getBoolean("sendReceiveMode", false)
      , settings.getString("ipAddress", "demo.voxgift.com"));

    mTalk = settings.getBoolean("talkMode", true);
    setTtsLanguage(settings.getString("outputLanguage", DEFAULT_LANG));
    winstonApp.setTalk(settings.getString("outputLanguage", DEFAULT_LANG), mTts, this);
  }

  @Override
  protected void onStop() {
    super.onStop();
    //android.util.Log.d("winston", "^++WinstonActivity.onStop");
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if (mTts != null) {
      mTts.shutdown();
    }
  }

  /**
   *
   */
  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    MenuInflater inflater = getMenuInflater();
    inflater.inflate(R.menu.options, menu);
    return true;
  }

  public void startActivity(Class<?> cls) {
    finish();
    startActivity(new Intent(this, cls));
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    Resources res = getResources();
    String packageName = getBaseContext().getPackageName();

    if (item.getItemId() == res.getIdentifier("build", "id", packageName)) {
      startActivity(BuildActivity.class);
    } else if (item.getItemId() == res.getIdentifier("card", "id", packageName)) {
      startActivity(CardActivity.class);
    } else if (item.getItemId() == res.getIdentifier("feedback", "id", packageName)) {
      startActivity(FeedbackActivity.class);
    } else if (item.getItemId() == res.getIdentifier("pagelog", "id", packageName)) {
      startActivity(PagelogActivity.class);
    } else if (item.getItemId() == res.getIdentifier("requests", "id", packageName)) {
      startActivity(RequestsActivity.class);
    } else if (item.getItemId() == res.getIdentifier("settings", "id", packageName)) {
      startActivity(EditPreferences.class);
    } else if (item.getItemId() == res.getIdentifier("speak", "id", packageName)) {
      startActivity(SpeakActivity.class);
    } else if (item.getItemId() == res.getIdentifier("status", "id", packageName)) {
      startActivity(StatusActivity.class);
    } else if (item.getItemId() == res.getIdentifier("type", "id", packageName)) {
      startActivity(TypeActivity.class);
    } else if (item.getItemId() == R.id.home) {
      finish();
    } else if (item.getItemId() == R.id.settings) {
      startActivity(new Intent(this, EditPreferences.class));
    } else {
      return super.onOptionsItemSelected(item);
    }
    return true;
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return true;
  }

  /**
   *
   */
  public void buildActivity(View v)  {
    startActivity(BuildActivity.class);
  }

  public void cardActivity(View v) {
    startActivity(CardActivity.class);
  }

    public void requestsActivity(View v)  {
    startActivity(RequestsActivity.class);
  }

  public void pagelogActivity(View v)  {
    startActivity(PagelogActivity.class);
  }

  public void speakActivity(View v) {
    startActivity(SpeakActivity.class);
  }

  public void statusActivity(View v) {
    startActivity(StatusActivity.class);
  }

  public void typeActivity(View v) {
    startActivity(TypeActivity.class);
  }

  /**
   *  TODO: Revist this logic. To support custom dictionary, this either returns strings or stringIDs
   *  and is similar to what was done to WinstonApp.getSentenceIn
   */
  public String[] getDisplayDictionary() {
    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    if (settings.getString("setting", "general").equals("custom")) {
        ArrayList<String> dictionary = new ArrayList<String>();
        try {
          JSONArray jArray = new JSONArray(settings.getString("custom", "[]"));
          for (int i=0; i < jArray.length(); i++)
          {
            try {
                dictionary.add(jArray.getString(i));
            } catch (JSONException e) {
             // Oops
            }
          }
        } catch (JSONException e) {
           // Oops
        }

      return dictionary.toArray(new String[dictionary.size()]);
    } else {
      String resourceName = settings.getString("setting", "general") + "_" + settings.getString("role", "patient");
      return getResources().getStringArray(getResources().getIdentifier(resourceName, "array", getBaseContext().getPackageName()));
    }
  }

  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == MY_DATA_CHECK_CODE) {
      // Should do something like if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
      //  and if success, create the TTS instance
      //  else .. startActivity(installIntent);
      //    Intent installIntent = new Intent();
      //    installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
      //    startActivity(installIntent);
      // But, since we are getting false negatives, we'll
      // switch to en IFF we get a TextToSpeech.LANG_MISSING_DATA later on
      mTts = new TextToSpeech(this, this);
    }
  }

  public boolean setTtsLanguage(String language) {
    //android.util.Log.d("winston", "+++WinstonActivity.setTtsLanguage: " + language);
    if (mTtsStatus == TextToSpeech.SUCCESS && language != null) {
      Locale locale = new Locale(language);
      int languageCheck = mTts.isLanguageAvailable(locale);
      if (languageCheck >= 0) {
        mTts.setLanguage(locale);
        return true;
      } else {
        String errorStr;
        switch (languageCheck) {
          case TextToSpeech.LANG_MISSING_DATA:
            errorStr = "The language data is missing.";
            break;
          case TextToSpeech.LANG_NOT_SUPPORTED:
            errorStr = "You must install the appropriate speech synthesizer (text-to-speech) language in order to use [" + locale.toString() + "] is not supported.";
            break;
          default:
            errorStr = "Unknown TTS error.";
            break;
          }

        errorStr += " Changing to default [" + DEFAULT_LANG + "].";
        Toast.makeText(this, errorStr, Toast.LENGTH_LONG).show();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        Editor editor = settings.edit();
        editor.putString("outputLanguage", DEFAULT_LANG);
        editor.commit();
        mTts.setLanguage(new Locale(DEFAULT_LANG));
        return true;
      }
    } else {
      return false;
    }
  }

  public boolean isNetworkAvailable() {
    WinstonApp winstonApp = (WinstonApp) (getApplication());
    return winstonApp.isNetworkAvailable();
  }

  public void goMenu(View v)  {
    openOptionsMenu();
  }

  public void goBack(View v)  {
    this.finish();
  }

  protected Dialog buildDialog(String str) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage(str)
           .setCancelable(true)
           .setPositiveButton("OK", new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id) {
                WinstonActivity.this.finish();
              }
           });
    AlertDialog alert = builder.create();
    return alert;
  }

  protected Dialog buildDialog(String step, String error) {
    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setMessage(error + " error during " + step
            + " step. Please verify that you have a connection and that your URL is set correctly.")
           .setCancelable(true)
           .setPositiveButton("OK", new DialogInterface.OnClickListener() {
              public void onClick(DialogInterface dialog, int id) {
                WinstonActivity.this.finish();
              }
           });
    AlertDialog alert = builder.create();
    return alert;
  }

  @Override
  protected Dialog onCreateDialog(int id) {
    Dialog dialog;
    switch(id) {
    case DIALOG_NO_CONN_ID:
        dialog = buildDialog(getResources().getString(R.string.error_no_conn));
        break;
    case DIALOG_UE_EX_ID:
        dialog = buildDialog("UnsupportedEncodingException");
        break;
    case DIALOG_CP_EX_ID:
        dialog = buildDialog("ClientProtocolException");
        break;
    case DIALOG_IO_EX_ID:
        dialog = buildDialog("IOException");
        break;
    default:
        dialog = null;
    }
    return dialog;
  }
/*
  public static void fixBackgroundRepeat(View view) {
    Drawable bg = view.getBackground();
    if (bg != null) {
      if (bg instanceof BitmapDrawable) {
        BitmapDrawable bmp = (BitmapDrawable) bg;
        bmp.mutate(); // make sure that we aren't sharing state anymore
        bmp.setTileModeXY(TileMode.REPEAT, TileMode.REPEAT);
      }
    }

  }
*/
}
