// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;

public class FragmentsBuilder {

  private final Set<FragmentNode> nodes;

  public FragmentsBuilder(InputStream input) throws IOException {
    nodes = new HashSet<FragmentNode>();
    CSVReader csvReader = new CSVReader(new InputStreamReader(input), ',');
    csvReader.readNext();
    String[] currentLine = csvReader.readNext();
    while (currentLine != null) {
      this.addLine(currentLine);
      currentLine = csvReader.readNext();
    }
  }

  public FragmentsBuilder(InputStream input, String[] displayDictionary) throws IOException {
    List<String> displayDictionaryList = new ArrayList<String>(Arrays.asList(displayDictionary));
    nodes = new HashSet<FragmentNode>();
    CSVReader csvReader = new CSVReader(new InputStreamReader(input), ',');
    csvReader.readNext();
    String[] currentLine = csvReader.readNext();
    while (currentLine != null) {
      if (displayDictionaryList.contains(currentLine[0])) {
        this.addLine(currentLine);
      }
      currentLine = csvReader.readNext();
    }
  }

  public FragmentsBuilder(String[] displayDictionary) {
    nodes = new HashSet<FragmentNode>();
    String line;
    for (int i = 0; i < displayDictionary.length; i++) {
      line = ',' + displayDictionary[i]; // addLine expects a first column
      this.addLine(line.split(","));
    }
  }

  public FragmentsBuilder() {
    nodes = new HashSet<FragmentNode>();
  }

  public void addLine(String[] line) {
    if (line == null) {
      throw new IllegalArgumentException("Line is null.");
    } else if (line.length < 2) {
      throw new IllegalArgumentException("Assuming (currently) that there are at least 2 columns in csv.");
    } else {
      List<String> lineList = Arrays.asList(line);
      Iterator<String> lineIterator = lineList.iterator();
      lineIterator.next(); //dumping first column
      String firstActualColumn = lineIterator.next();
      boolean found = false;
      for (FragmentNode existingFragmentChain : nodes) {
        if (existingFragmentChain.getFragment().equals(firstActualColumn)) {
          existingFragmentChain.appendList(lineIterator);
          found = true;
          break;
        }
      }
      if (!found) {
        FragmentNode fragmentNodeBeignBuilt = new FragmentNode(firstActualColumn);
        fragmentNodeBeignBuilt.appendList(lineIterator);
        nodes.add(fragmentNodeBeignBuilt);
      }
    }
  }

  public Set<FragmentNode> getRootNodes() {
    return this.nodes;
  }
}
