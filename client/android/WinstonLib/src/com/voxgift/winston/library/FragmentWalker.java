// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.text.Collator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;
import java.util.Stack;

public class FragmentWalker {

  private final Set<FragmentNode> start;

  private final Stack<FragmentNode> pathToStart;

  private FragmentNode mCurrent;

  public FragmentWalker(Set<FragmentNode> start) {
    // We must deeply and defensively copy the nodes to prohibit mutation of the tree.
    this.start = new HashSet<FragmentNode>();
    for (FragmentNode current : start) {
      this.start.add(new FragmentNode(current));
    }
    this.pathToStart = new Stack<FragmentNode>();
  }

  public CircularSet getOptions() {
    Set<String> returnSet = new TreeSet<String>(Collator.getInstance());
    if (mCurrent == null) {
      for (FragmentNode current : this.start) {
        returnSet.add(current.getFragment());
      }
    } else {
      for (FragmentNode current : this.mCurrent.getNexts()) {
        returnSet.add(current.getFragment());
      }
    }
    return new CircularSet(returnSet);
  }

  public List<String> pathFromStart() {
    List<String> returnVal = new LinkedList<String>();
    for (FragmentNode current : this.pathToStart) {
      returnVal.add(0, current.getFragment());
    }
    return returnVal;
  }

  public String getCurrentFragments() {
    StringBuilder strBuilder = new StringBuilder();
    List<String> fragmentsSelected = pathFromStart();
    for (String current : fragmentsSelected) {
      strBuilder.append(current);
      strBuilder.append(" ");
    }
    if (mCurrent != null) {
      strBuilder.append(mCurrent.getFragment());
      strBuilder.append(" ");
    }
    return strBuilder.toString().trim();
  }

  public void useOption(String option) {
    FragmentNode choice = null;
    if (mCurrent == null) {
      choice = findChoice(option, this.start);
    } else {
      choice = findChoice(option, this.mCurrent.getNexts());
    }
    if (choice == null) {
      throw new NoSuchElementException("option is not in the set of available choices.");
    }
    if (this.mCurrent != null) {
      this.pathToStart.add(this.mCurrent);
    }
    this.mCurrent = choice;
  }

  //@VisibleForTesting
  FragmentNode findChoice(String choice, final Set<FragmentNode> nodes) {
    for (FragmentNode current : nodes) {
      if (current.getFragment().equals(choice)) {
        return current;
      }
    }
    return null;
  }

  public void start() {
    this.mCurrent = null;
    this.pathToStart.clear();
  }

  public boolean isLeaf() {
    if (mCurrent == null) {
      return this.start.isEmpty();
    } else {
      return this.mCurrent.getNexts().isEmpty();
    }
  }

  public boolean isRoot() {
    if (this.mCurrent == null) {
      return true;
    }
    return false;
  }

  public void decrementChoice() {
    if (pathToStart.isEmpty()) {
      this.mCurrent = null;
      return;
    }
    this.mCurrent = this.pathToStart.lastElement();
    this.pathToStart.pop();
  }
}
