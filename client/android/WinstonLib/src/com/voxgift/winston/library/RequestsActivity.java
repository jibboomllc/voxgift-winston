// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.HttpEntity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class RequestsActivity extends WinstonActivity {
  public static final int TITLE_ID = R.string.requests;
  public static final int SUBTITLE_ID = R.string.requests_button_prompt;
  public static final int ICON_ID = R.drawable.ic_pagelog;

  static final int MAX_DISPLAY_REQUESTS = 16;

  static final int DIALOG_OR_UE_EX_ID = 21;
  static final int DIALOG_OR_CP_EX_ID = 22;
  static final int DIALOG_OR_IO_EX_ID = 23;
  static final int DIALOG_AC_UE_EX_ID = 24;
  static final int DIALOG_AC_CP_EX_ID = 25;
  static final int DIALOG_AC_IO_EX_ID = 26;

  private String mDeviceId;
  private String mIpAddress;
  private DataUpdateReceiver mDataUpdateReceiver;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    //android.util.Log.v("winston", "RequestsActivity.onCreate");
    if (!isNetworkAvailable()) {
      showDialog(DIALOG_NO_CONN_ID);
    } else {
      checkIntent(getIntent());

      mDeviceId = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);

      SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
      mIpAddress = settings.getString("ipAddress", null);

      setContentView(R.layout.requests);

      TextView textView = (TextView) findViewById(R.id.titleLabel);
      textView.setText(TITLE_ID);

      updateList();
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (mDataUpdateReceiver != null) {
      unregisterReceiver(mDataUpdateReceiver);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (mDataUpdateReceiver == null) {
      mDataUpdateReceiver = new DataUpdateReceiver();
    }
    IntentFilter intentFilter = new IntentFilter(WinstonActivity.REFRESH_DATA_INTENT);
    registerReceiver(mDataUpdateReceiver, intentFilter);
  }

  private class DataUpdateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      if (intent.getAction().equals(WinstonActivity.REFRESH_DATA_INTENT)) {
        updateList();
      }
    }
  }

  @Override
  protected Dialog onCreateDialog(int id) {
    Dialog dialog;
    switch(id) {
    case DIALOG_NO_CONN_ID:
        dialog = buildDialog(getResources().getString(R.string.error_no_conn));
        break;
    case DIALOG_OR_UE_EX_ID:
        dialog = buildDialog("OpenRequests", "UnsupportedEncodingException");
        break;
    case DIALOG_OR_CP_EX_ID:
        dialog = buildDialog("OpenRequests", "ClientProtocolException");
        break;
    case DIALOG_OR_IO_EX_ID:
        dialog = buildDialog("OpenRequests", "IOException");
        break;
    case DIALOG_AC_UE_EX_ID:
        dialog = buildDialog("Ack/Close", "UnsupportedEncodingException");
        break;
    case DIALOG_AC_CP_EX_ID:
        dialog = buildDialog("Ack/Close", "ClientProtocolException");
        break;
    case DIALOG_AC_IO_EX_ID:
        dialog = buildDialog("Ack/Close", "IOException");
        break;
    default:
        dialog = null;
    }
    return dialog;
  }

  protected void checkIntent(Intent data) {
    if (data != null) {
      //android.util.Log.i("winston", "RequestsActivity.checkIntent: action = " + data.getAction() + ", data != null");
      String notification = data.getStringExtra("notification");
      if (notification != null) {
        //android.util.Log.i("winston", "RequestsActivity.checkIntent: notification = " + notification);
        Toast.makeText(this, notification, Toast.LENGTH_SHORT).show();
      }
    } else {
      //android.util.Log.i("winston", "RequestsActivity.checkIntent: data == null");
    }
  }

  public void updateList() {
    List<PageRecord> pageLogs = getOpenRequests(MAX_DISPLAY_REQUESTS);
    createList(R.id.RequestList, pageLogs);
  }

  public List<PageRecord> getOpenRequests(int limit) {
    if (limit > 0 && mIpAddress != null) {
      String postString = "http://" + mIpAddress + "/rest/open_requests";
      try {
        postString += "/device_id/" + URLEncoder.encode(mDeviceId, "UTF-8");
        postString += "/limit/" + limit;
      } catch (UnsupportedEncodingException ex) {
        showDialog(DIALOG_OR_UE_EX_ID);
        throw new RuntimeException("Broken VM does not support UTF-8");
      }

      HttpClient httpclient = new DefaultHttpClient();
      HttpPost httppost = new HttpPost(postString);

       try {
        //android.util.Log.v("winston", postString);
        HttpResponse response = httpclient.execute(httppost);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
          List<PageRecord> pageRecords = new ArrayList<PageRecord>();
          InputStream instream = entity.getContent();
          Request.RequestLog requestLog = Request.RequestLog.parseFrom(instream);
          for (Request.RequestEntry entry : requestLog.getRequestEntryList()) {
            pageRecords.add(new PageRecord(entry.getId()
              , entry.getRequest(), entry.getRequestor()
              , new Date(new Long(entry.getReqTs()) * 1000)
              , new Date(new Long(entry.getAckTs()) * 1000)
              , new Date(0L)
              , entry.getAckName()
              , entry.getResName()
              ));
          }
          return pageRecords;
        }
      } catch (ClientProtocolException e) {
          //android.util.Log.e("winston", "ClientProtocolException: " + e);
          showDialog(DIALOG_OR_CP_EX_ID);
      } catch (IOException e) {
          //android.util.Log.e("winston", "IOException: " + e);
          showDialog(DIALOG_OR_IO_EX_ID);
      }
    }

    List<PageRecord> pageRecords = new ArrayList<PageRecord>();
    pageRecords.add(new PageRecord(0L, "---", "---", System.currentTimeMillis(), 0L, 0L, "", ""));

    return pageRecords;
  }

  public void createList(int id, List<PageRecord> pageLogs) {
    ListView list = (ListView) findViewById(id);
    list.setClickable(true);

    if (pageLogs != null) {
      RequestListAdapter adapter = new RequestListAdapter(this, pageLogs, mDeviceId, mIpAddress);
      list.setAdapter(adapter);
    }
  }

}
