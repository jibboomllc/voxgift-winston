// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library.widget;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.voxgift.winston.library.CircularSet;
import com.voxgift.winston.library.widget.FragometerSpinner.OnWordChangeListener;

public class FragometerSpinner extends View {

  private static final float IDEAL_ASPECT_RATIO = 1.66f;

  private static final Drawable BACKGROUND = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[] {
    0x88888888, 0x88FFFFFF, 0x88888888 });
  /**
   * The Paint used to draw the digit string. We set it to be anti-aliased, white and centered horizontally.
   */
  private static final Paint PAINT = new Paint(Paint.ANTI_ALIAS_FLAG);

  // TODO: Externalize these settings.
  private static final float MIN_TEXT_SIZE = 20;
  private static final float TEXT_RESIZE_STEP = 10;

  private OnFragmentGroupingChangeListener onFragmentGroupingChangeListener;
  private OnWordChangeListener onWordChangeListener;
  private CircularSet choices;
  private float mTouchStartY;
  private float mTouchLastY;
  private float mTouchStartX;
  private float mTouchLastX;
  private float mDigitY;
  private float mDigitAboveY;
  private float mDigitBelowY;
  private float mHeight;
  private int mWidth;
  private int mDigitX;

  /*
   * Simple constructor used when creating a view from code.
   */
  public FragometerSpinner(Context context) {
    super(context);
    initialize();
  }

  /*
   * This is called when a view is being constructed from an XML file, supplying attributes that were specified in the XML file.
   */
  public FragometerSpinner(Context context, AttributeSet attrs) {
    super(context, attrs);
    initialize();
  }

  /*
   * Perform inflation from XML and apply a class-specific base style. This constructor of View allows subclasses to use their own base style when
   * they are inflating.
   */
  public FragometerSpinner(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
    initialize();
  }

  private void initialize() {
    PAINT.setColor(Color.BLACK);
    PAINT.setTextAlign(Align.CENTER);
    setDigitYValues();
  }

  private void setDigitYValues() {
    mDigitY = findCenterY("center");
    mDigitAboveY = findCenterY("above") - mHeight;
    mDigitBelowY = mHeight + findCenterY("below");
  }

  private float findCenterY(String text) {
    Rect bounds = new Rect();
    PAINT.getTextBounds(text, 0, text.length(), bounds);

    int textHeight = Math.abs(bounds.height());

    float result = mHeight - ((mHeight - textHeight) / 2);

    return result;
  }

  /*
   * This is where all of the drawing for the spinner is done.
   */
  @Override
  protected void onDraw(Canvas canvas) {
    // if our super has to do any drawing, do that first
    super.onDraw(canvas);

    // draw the background so it is below the digit
    BACKGROUND.draw(canvas);

    if (choices != null && !choices.isEmpty()) {
      // draw the digit text using our calculated position and Paint
      canvas.drawText(choices.getCurrent(), mDigitX, mDigitY, PAINT);

      canvas.drawText(choices.getBefore(), mDigitX, mDigitAboveY, PAINT);
      canvas.drawText(choices.getAfter(), mDigitX, mDigitBelowY, PAINT);
    }
  }

  /*
   * Called whenever the size of our View changes
   */
  @Override
  protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    super.onSizeChanged(w, h, oldw, oldh);
    // resize the background gradient
    BACKGROUND.setBounds(0, 0, w, h);

    mWidth = w;
    mHeight = h;

    mDigitX = mWidth / 2;

    // set the text paint to draw appropriately-sized text
    PAINT.setTextSize(this.calculateTextSize(w, h));

    setDigitYValues();
  }

  private float calculateTextSize(int w, int h) {
    Rect bounds = new Rect();
    float hTemp = h;
    // //android.util.Log.d("winston", "WordometerSpinner.calculateTextSize #strings = " + this.mStrings.size() + ", this.mSelected = " + this.mSelected);

    List<String> wordsToMeasure = new ArrayList<String>();
    if (this.choices != null) {
      wordsToMeasure.addAll(this.choices.allStrings());
    }
    for (String str : wordsToMeasure) {
      PAINT.setTextSize(hTemp);
      PAINT.getTextBounds(str, 0, str.length(), bounds);
      // //android.util.Log.v("winston", str+ " hTemp = " +hTemp+ " bounds h,w " + "bounds.height= " +bounds.height()+ ", bounds.width= " +bounds.width());
      for (; bounds.width() > w && hTemp > MIN_TEXT_SIZE; hTemp -= TEXT_RESIZE_STEP) {
          // //android.util.Log.v("winston", str+ " hTemp = " +hTemp+ " bounds h,w " + "bounds.height= " +bounds.height()+ ", bounds.width= " +bounds.width());
          PAINT.setTextSize(hTemp);
          PAINT.getTextBounds(str, 0, str.length(), bounds);
      }
    }

    return hTemp;
  }

  @Override
  public boolean onTouchEvent(MotionEvent event) {
    // Pull out the Action value from the event for processing
    int action = event.getAction();

    if (action == MotionEvent.ACTION_DOWN) {
      mTouchStartY = event.getY();
      mTouchLastY = mTouchStartY;

      mTouchStartX = event.getX();
      mTouchLastX = mTouchStartX;
      return true;
    } else if (action == MotionEvent.ACTION_MOVE) {
      float currentY = event.getY();

      float delta = mTouchLastY - currentY;
      mTouchLastY = currentY;

      mDigitY -= delta;
      mDigitAboveY -= delta;
      mDigitBelowY -= delta;

      // calculate the overall delta (beginning to now)
      float totalDelta = mTouchStartY - currentY;

      // If we have scrolled an entire number, change numbers while
      // keeping the scroll
      if (Math.abs(totalDelta) > mHeight) {
        // need to either increase or decrease value
        float postDelta = Math.abs(totalDelta) - mHeight;

        if (totalDelta > 0) {
          // go DOWN a number
          // setCurrentDigit(mDigitBelow);
          mTouchStartY -= mHeight;

          mDigitY -= postDelta;
          mDigitBelowY -= postDelta;
          mDigitAboveY -= postDelta;
        } else {
          // go UP a number
          // setCurrentDigit(mDigitAbove);
          mTouchStartY += mHeight;

          mDigitY += postDelta;
          mDigitBelowY += postDelta;
          mDigitAboveY += postDelta;
        }
      }

      invalidate();

      return true;
    } else if (action == MotionEvent.ACTION_UP) {
      float currentY = event.getY();

      // delta: negative means a down 'scroll'
      float deltaY = mTouchStartY - currentY;

      if (Math.abs(deltaY) > (mHeight / 3)) {
        // higher numbers are 'above' the current, so a scroll down
        // _increases_ the value
        if (deltaY < 0) {
          this.choices.decreaseIndex();
          if (onWordChangeListener != null) {
            onWordChangeListener.wordChanged();
          }
        } else {
          this.choices.increaseIndex();
          if (onWordChangeListener != null) {
            onWordChangeListener.wordChanged();
          }
        }
      } else {
        float currentX = event.getX();
        float deltaX = mTouchStartX - currentX;

        if (Math.abs(deltaX) > (mWidth / 10)) {
          if (deltaX > 0) {
            // //android.util.Log.v("odo", "scroll right");
            if (onFragmentGroupingChangeListener != null) {
              onFragmentGroupingChangeListener.increaseChangeList(this.choices.getCurrent());
              if (onWordChangeListener != null) {
                onWordChangeListener.wordChanged();
              }
            }
          } else {
            // //android.util.Log.v("odo", "scroll left");
            if (onFragmentGroupingChangeListener != null) {
              onFragmentGroupingChangeListener.decreaseChangeList();
              if (onWordChangeListener != null) {
                onWordChangeListener.wordChanged();
              }
            }
          }
        }
      }
      setDigitYValues();
      invalidate();
      return true;
    }
    return false;
  }

  /*
   * Measure the view and its content to determine the measured width and the measured height.
   */
  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    // get width and height size and mode
    int wSpec = MeasureSpec.getSize(widthMeasureSpec);

    int hSpec = MeasureSpec.getSize(heightMeasureSpec);
    int hMode = MeasureSpec.getMode(heightMeasureSpec);

    int width = wSpec;
    int height = hSpec;

    // ideal height for the number display
    int idealHeight = (int) (wSpec * IDEAL_ASPECT_RATIO);

    if (idealHeight < hSpec) {
      height = idealHeight;
    }
    //android.util.Log.v("winston", "WordometerSpinner.onMeasure setMeasuredDimension w,h = " + width + ", " + height);
    setMeasuredDimension(width, height);
  }

  public interface OnFragmentGroupingChangeListener {
    void increaseChangeList(String choice);

    void decreaseChangeList();
  }

  public interface OnWordChangeListener {
    void wordChanged();
  }

  public void setChoices(CircularSet choices) {
    this.choices = choices;
  }

  public void setOnChangeListListner(OnFragmentGroupingChangeListener listener) {
    this.onFragmentGroupingChangeListener = listener;
  }

  public void setOnWorkdChangeListener(OnWordChangeListener listener) {
    this.onWordChangeListener = listener;
  }

  public String getCurrentChoice() {
    return this.choices.getCurrent();
  }
}
