// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceGroup;
import android.preference.Preference.OnPreferenceClickListener;

import com.voxgift.winston.library.DragNDrop.DragNDropAdapter;
import com.voxgift.winston.library.DragNDrop.DragNDropListActivity;
import com.voxgift.winston.library.DragNDrop.DragNDropListView;

public class EditPreferences
    extends PreferenceActivity
    implements OnSharedPreferenceChangeListener {
  public static final int TITLE_ID = R.string.settings;
  public static final int SUBTITLE_ID = R.string.settings_button_prompt;
  public static final int ICON_ID = R.drawable.ic_settings;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    this.addPreferencesFromResource(R.xml.settings);
    this.initSummaries(this.getPreferenceScreen());

    this.getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

    Preference custom = findPreference("custom");
    custom.setOnPreferenceClickListener(new OnPreferenceClickListener() {
      public boolean onPreferenceClick(Preference pref) {
        startActivity(new Intent(EditPreferences.this, CustomDictionaryActivity.class));
        //startActivity(new Intent(EditPreferences.this, DragNDropListActivity.class));
        
        //showCustomDialog();
        return false;
      }
    });

    Preference inputLanguage = findPreference("input_language");
    inputLanguage.setOnPreferenceClickListener(new OnPreferenceClickListener() {
      public boolean onPreferenceClick(Preference pref) {
        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCALE_SETTINGS), 0);
        return false;
      }
    });

  }

  /** Set the summaries of all preferences.
    */
  private void initSummaries(PreferenceGroup pg) {
    for (int i = 0; i < pg.getPreferenceCount(); ++i) {
      Preference p = pg.getPreference(i);
      if (p instanceof PreferenceGroup) {
        this.initSummaries((PreferenceGroup) p); // recursion
      } else {
        this.setSummary(p);
      }
    }
  }

  /** Set the summaries of the given preference.
    */
  private void setSummary(Preference pref) {
    // react on type or key
    if (pref instanceof ListPreference) {
      ListPreference listPref = (ListPreference) pref;
      pref.setSummary(listPref.getEntry());
    } else if (pref instanceof EditTextPreference) {
      EditTextPreference textPref = (EditTextPreference) pref;
      pref.setSummary(textPref.getText());
    }
  }

  /** used to change the summary of a preference.
    */
  public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
    Preference pref = findPreference(key);
    this.setSummary(pref);
  }
}
