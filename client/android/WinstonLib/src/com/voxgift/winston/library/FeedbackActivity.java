// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

public class FeedbackActivity extends WinstonActivity {
  public static final int TITLE_ID = R.string.feedback;
  public static final int SUBTITLE_ID = R.string.feedback_button_prompt;
  public static final int ICON_ID = R.drawable.ic_feedback;

  private Location mLocation;
  private LocationManager mLocationManager;

  private String mDeviceId;
  private String mIpAddress;

  private WebView mWebView;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    if (!isNetworkAvailable()) {
      showDialog(DIALOG_NO_CONN_ID);
      finish();
    } else {
      setContentView(R.layout.feedback);

      TextView textView = (TextView) findViewById(R.id.titleLabel);
      textView.setText(TITLE_ID);

      mDeviceId = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);

      mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
      mLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

      String url = BuildValues.FEEDBACK_URI;

      try {
        PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        url += "/device_id/" + URLEncoder.encode(mDeviceId, "UTF-8");
        url += "/hw_manufacturer/" + URLEncoder.encode(Build.MANUFACTURER, "UTF-8");
        url += "/hw_model/" + URLEncoder.encode(Build.MODEL, "UTF-8");
        url += "/os_version/" + URLEncoder.encode(Build.VERSION.RELEASE, "UTF-8");
        url += "/app_version_name/" + URLEncoder.encode(pInfo.versionName, "UTF-8");
        url += "/app_package_name/" + URLEncoder.encode(pInfo.packageName, "UTF-8");
        url += "/lat/" + new Double((mLocation != null) ? mLocation.getLatitude() : 0.0).toString();
        url += "/lon/" + new Double((mLocation != null) ? mLocation.getLongitude() : 0.0).toString();
        url += "/language_code/" +  URLEncoder.encode(Locale.getDefault().toString(), "UTF-8");
      } catch (PackageManager.NameNotFoundException ex) {
        throw new RuntimeException("PackageManager.NameNotFoundException");
      } catch (UnsupportedEncodingException ex) {
        throw new RuntimeException("Broken VM does not support UTF-8");
      }
      //android.util.Log.d("winston", "Start @: " + url);
      mWebView = (WebView) findViewById(R.id.webview);
      mWebView.getSettings().setJavaScriptEnabled(true);
      mWebView.setWebViewClient(new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView wView, String newUrl) {
          //android.util.Log.d("winston", newUrl);
          if (newUrl.contains("page/close") || newUrl.contains("page/cancel")) {
            FeedbackActivity.this.finish();
          } else {
            wView.loadUrl(newUrl);
          }
          return true;
        }
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
          Toast.makeText(FeedbackActivity.this, getResources().getString(R.string.error_no_conn), Toast.LENGTH_SHORT).show();
          FeedbackActivity.this.finish();
        }
      });
      mWebView.loadUrl(url);
    }
  }

  @Override
  public void goBack(View v)  {
    mWebView.loadUrl("javascript:clickPreviousLink()");
  }
}
