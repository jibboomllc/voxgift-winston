// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

public class CircularSet {

  private final List<String> data;
  private int index;

  public CircularSet(CircularSet original) {
    this.data = new ArrayList<String>(original.data);
    this.index = 0;
  }

  public CircularSet(Set<String> data) {
    this.data = new ArrayList<String>(data);
    this.index = 0;
  }

  public boolean isEmpty() {
    return this.data.isEmpty();
  }
  
  public String getCurrent() {
    if (this.data.isEmpty()) {
      return null;
    } else {
      return this.data.get(index);
    }
  }

  public String getAfter() {
    if (this.data.isEmpty()) {
      return null;
    } else if (index + 1 >= this.data.size()) {
      return this.data.get(0);
    } else {
      return this.data.get(index + 1);
    }
  }

  public String getBefore() {
    if (this.data.isEmpty()) {
      return null;
    } else if ((index - 1) < 0) {
      return this.data.get(this.data.size() - 1);
    } else {
      return this.data.get(this.index - 1);
    }
  }

  public void increaseIndex() {
    this.index++;
    if (this.index >= this.data.size()) {
      this.index = 0;
    }
  }

  public void decreaseIndex() {
    this.index--;
    if (this.index < 0) {
      this.index = this.data.size() - 1;
    }
  }

  public Set<String> allStrings() {
    return new HashSet<String>(this.data);
  }
}
