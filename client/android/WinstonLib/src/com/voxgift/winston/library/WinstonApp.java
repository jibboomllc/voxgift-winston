// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.speech.tts.TextToSpeech;
import android.util.DisplayMetrics;
import android.widget.Toast;

import au.com.bytecode.opencsv.CSVReader;

import com.voxgift.winston.library.service.PushService;

public class WinstonApp extends Application {
  private Activity mActivity;
  private TextToSpeech mTts;
  private Map<String, String> mSentencesIn;
  private Map<String, String> mSentencesInReverse;
  private Locale mSentencesInLocale;
  private Map<String, String> mSentencesOut;
  private Locale mSentencesOutLocale;
  private String mOutputLanguage;

  private MediaPlayer mPageSound;
  private boolean mSendReceive;
  private String mIpAddress;
  private String mDeviceId;

  public WinstonApp() {
    super();
    mSendReceive = false;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

    if (!settings.contains("outputLanguage")) {
      Editor editor = settings.edit();
      editor.putString("outputLanguage", Locale.getDefault().getLanguage());
      editor.commit();
    }

    try {
      mSentencesInLocale = Locale.getDefault();
      mSentencesIn = readSentences(0, 1);
      mSentencesInReverse = readSentences(1, 0);

      mSentencesOutLocale = mSentencesInLocale;
      mSentencesOut = readSentences(0, 1);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

/***
 *
 */
  public Map<String, String> readSentences(int outKeyCol, int outValueCol)
  throws IOException {
    Map<String, String> sentenceMap = new TreeMap<String, String>();

    //android.util.Log.v("winston", "WinstonApp.readSentences : Parsing mSentences.xml");

    InputStream in = getResources().openRawResource(R.raw.dictionary);

    CSVReader csvReader = new CSVReader(new InputStreamReader(in), ',');
    csvReader.readNext();
    String[] currentLine = csvReader.readNext();
    String[] cols = new String[2];
    while (currentLine != null && currentLine.length >= 2) {
      cols[0] = currentLine[0];
      cols[1] = currentLine[1];
      for (int i = 2; i < currentLine.length; i++) {
        cols[1] += " " + currentLine[i];
      }
      sentenceMap.put(cols[outKeyCol].trim(), cols[outValueCol].trim());
      currentLine = csvReader.readNext();
    }
    return sentenceMap;
  }

  public void setTalk(String outputLanguage, TextToSpeech tts, Activity activity) {
    mOutputLanguage = outputLanguage;
    mTts = tts;
    mActivity = activity;

    if (!Locale.getDefault().equals(mSentencesInLocale)) {
      try {
        //android.util.Log.v("winston", "WinstonApp: reading mSentencesIn");
        mSentencesInLocale = Locale.getDefault();
        mSentencesIn = readSentences(0, 1);
        mSentencesInReverse = readSentences(1, 0);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    Locale outLocale = new Locale(mOutputLanguage);
    if (!outLocale.equals(mSentencesOutLocale)) {

      Configuration conf = this.getResources().getConfiguration();
      conf.locale = outLocale;
      DisplayMetrics metrics = new DisplayMetrics();
      mActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);

      // Kludge: the constructor below resets the resources so we will need to set then back as appropriate
      Resources outputResources = new Resources(getAssets(), metrics, conf);
      try {
        //android.util.Log.v("winston", "WinstonApp: reading mSentencesOut");
        mSentencesOutLocale = outLocale;
        mSentencesOut = readSentences(0, 1);
      } catch (IOException e) {
        e.printStackTrace();
      }
      conf.locale = mSentencesInLocale;
      Resources temp = new Resources(getAssets(), metrics, conf);
    }
  }

  public void setSendReceive(String deviceId, boolean sendReceive, String ipAddress) {
    mDeviceId = deviceId;
    mIpAddress = ipAddress;
    setSendReceive(sendReceive);
  }

  private void setSendReceive(boolean sendReceive) {
    //android.util.Log.d("winston", "WinstonApp.setSendReceive: " + sendReceive + ", mDeviceId = [" + mDeviceId + "]");
    mSendReceive = sendReceive;
    if (sendReceive) {
      Editor editor = getSharedPreferences(PushService.TAG, MODE_PRIVATE).edit();
      editor.putString(PushService.PREF_DEVICE_ID, mDeviceId);
      editor.putInt(PushService.PREF_NOTIF_ICON, R.drawable.ic_status_bar);
      editor.commit();
      PushService.actionStart(getApplicationContext());
    } else {
      PushService.actionStop(getApplicationContext());
    }
  }

/***
 *
 */

  public boolean hasTranslation(String str) {
    //android.util.Log.v("winston", "WinstonApp.hasTranslation: 1-mSentencesInReverse.containsKey(" + str + ")");
    if (mSentencesInReverse.containsKey(str) && mSentencesOut.containsKey(mSentencesInReverse.get(str))) {
      //android.util.Log.v("winston", "WinstonApp.hasTranslation: 2-mSentencesOut.containsKey(" + mSentencesInReverse.get(str) + ")");
      return true;
    }
    return false;
  }

  public String getSentenceIn(String str) {
    if (mSentencesIn.containsKey(str)) {
      return mSentencesIn.get(str);
    } else {
      return str; // TODO Verify change OK, that is, we used to return null but changed to return in str to support custom
    }
  }

  public void speak(String str) {
    //android.util.Log.v("winston", "WinstonApp.speak: 1-mSentencesInReverse.containsKey(" + str + ")");
    if (mSentencesInReverse.containsKey(str) && mSentencesOut.containsKey(mSentencesInReverse.get(str))) {
      //android.util.Log.v("winston", "WinstonApp.speak: 2-mSentencesOut.containsKey(" + mSentencesInReverse.get(str) + ")");
    }

    if (mTts != null) {
      if (mSentencesInReverse.containsKey(str) && mSentencesOut.containsKey(mSentencesInReverse.get(str))) {
        speakNow(mSentencesOut.get(mSentencesInReverse.get(str)));
      } else {
        speakNow(str);
      }
    }
  }

  public void speakNow(String str) {
    if (mTts != null) {
      Toast.makeText(this, getResources().getString(R.string.talk) + ": " + str, Toast.LENGTH_SHORT).show();
      mTts.speak(str, TextToSpeech.QUEUE_ADD, null);
    }
  }

/***
 *
 */
  public void page(String str) {
    if (mSendReceive) {
      if (str != null && str.length() > 0) {
        if (!isNetworkAvailable()) {
          //showDialog(DIALOG_NO_CONN_ID);
        } else {
          String postString = "http://" + mIpAddress + "/rest/request";
          try {
            postString += "/req_device_id/" + URLEncoder.encode(mDeviceId, "UTF-8");
            postString += "/request/" +  URLEncoder.encode(str, "UTF-8");
          } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("Broken VM does not support UTF-8");
          }

          HttpClient httpclient = new DefaultHttpClient();
          HttpPost httppost = new HttpPost(postString);

          try {
            //android.util.Log.v("winston", postString);
            HttpResponse response = httpclient.execute(httppost);

            BufferedReader rd = new BufferedReader(new InputStreamReader(
              response.getEntity().getContent()));
            String line = "";
            while ((line = rd.readLine()) != null) {
              //android.util.Log.v("winston", line);
              if (line.startsWith("Auth=")) {
                String key = line.substring(5);
                // Do something with the key
              }
            }
          } catch (ClientProtocolException e) {
              //android.util.Log.e("winston", "ClientProtocolException");
          } catch (IOException e) {
              //android.util.Log.e("winston", "IOException: " + e);
          }

          Toast.makeText(this, getResources().getString(R.string.page) + ": " + str, Toast.LENGTH_SHORT).show();
          mPageSound = MediaPlayer.create(this, R.raw.page);
          mPageSound.start();
        }
      }
    }
  }

  public boolean isNetworkAvailable() {
    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = cm.getActiveNetworkInfo();
    // android.util.Log.v("winston", "networkInfo = " + networkInfo);
    // if no network is available networkInfo will be null, otherwise check if we are connected
    if (networkInfo != null && networkInfo.isConnected()) {
        return true;
    }
    return false;
  }

}
