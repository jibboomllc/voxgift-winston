// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MainMenuAdapter extends ArrayAdapter {
  public static final String TAG = MainMenuAdapter.class.getSimpleName();
  private final Activity mParentActivity;
  private final List<Class<?>> mChildActivities;
  
  @SuppressWarnings("unchecked") // Just for this one statement
  public MainMenuAdapter(Activity parentActivity, List<Class<?>> childActivities) {
    super(parentActivity, R.layout.main_menu_row, childActivities);
    //android.util.Log.d(TAG, "MainMenuAdapter::constructor");
    mParentActivity = parentActivity;
    mChildActivities = childActivities;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    View rowView = convertView;
    MainMenuView mmView = null;

    if (rowView == null) {
      // Get a new instance of the row layout view
      LayoutInflater inflater = mParentActivity.getLayoutInflater();
      rowView = inflater.inflate(R.layout.main_menu_row, null);

      // Hold the view objects in an object,
      // so they don't need to be re-fetched
      mmView = new MainMenuView();
      mmView.title = (TextView) rowView.findViewById(R.id.title);
      mmView.subtitle = (TextView) rowView.findViewById(R.id.subtitle);
      mmView.icon = (ImageView) rowView.findViewById(R.id.icon);

      // Cache the view objects in the tag,
      // so they can be re-accessed later
      rowView.setTag(mmView);
    } else {
      mmView = (MainMenuView) rowView.getTag();
    }

    // Transfer the data from the data object
    // to the view objects
    Class<?> currentChoice = mChildActivities.get(position);
    mmView.title.setText(currentChoice.getSimpleName());
    try {
      mmView.title.setText((Integer) (currentChoice.getField("TITLE_ID").get(null)));
      mmView.subtitle.setText((Integer) (currentChoice.getField("SUBTITLE_ID").get(null)));
      mmView.icon.setImageResource((Integer) (currentChoice.getField("ICON_ID").get(null)));
    } catch (NoSuchFieldException e) {
        throw new RuntimeException("NoSuchFieldException");
    } catch (IllegalAccessException e) {
        throw new RuntimeException("IllegalAccessException");
    }
    return rowView;
  }

  protected static class MainMenuView {
    protected TextView title;
    protected TextView subtitle;
    protected ImageView icon;
  }
}
