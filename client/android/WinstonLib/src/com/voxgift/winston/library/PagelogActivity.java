// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.HttpEntity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings.Secure;

import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class PagelogActivity extends WinstonActivity {
  public static final int TITLE_ID = R.string.pagelog;
  public static final int SUBTITLE_ID = R.string.pagelog_button_prompt;
  public static final int ICON_ID = R.drawable.ic_pagelog;

  static final int MAX_DISPLAY_REQUESTS = 16;

  static final int DIALOG_PL_UE_EX_ID = 11;
  static final int DIALOG_PL_CP_EX_ID = 12;
  static final int DIALOG_PL_IO_EX_ID = 13;

  private String mDeviceId;
  private String mIpAddress;
  private DataUpdateReceiver mDataUpdateReceiver;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    //android.util.Log.v("winston", "PagelogActivity.onCreate");
    if (!isNetworkAvailable()) {
      showDialog(DIALOG_NO_CONN_ID);
    } else {
      checkIntent(getIntent());

      mDeviceId = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);

      SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
      mIpAddress = settings.getString("ipAddress", null);

      List<PageRecord> pageLogs = getPageLogs(MAX_DISPLAY_REQUESTS);

      setContentView(R.layout.pagelog);

      TextView textView = (TextView) findViewById(R.id.titleLabel);
      textView.setText(TITLE_ID);

      createList(R.id.Pagelog, pageLogs);
    }
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (mDataUpdateReceiver != null) {
      unregisterReceiver(mDataUpdateReceiver);
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (mDataUpdateReceiver == null) {
      mDataUpdateReceiver = new DataUpdateReceiver();
    }
    IntentFilter intentFilter = new IntentFilter(WinstonActivity.REFRESH_DATA_INTENT);
    registerReceiver(mDataUpdateReceiver, intentFilter);
  }

  private class DataUpdateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
      if (intent.getAction().equals(WinstonActivity.REFRESH_DATA_INTENT)) {
        List<PageRecord> pageLogs = getPageLogs(MAX_DISPLAY_REQUESTS);
        createList(R.id.Pagelog, pageLogs);
      }
    }
  }

  @Override
  protected Dialog onCreateDialog(int id) {
    Dialog dialog;
    switch(id) {
    case DIALOG_NO_CONN_ID:
        dialog = buildDialog(getResources().getString(R.string.error_no_conn));
        break;
    case DIALOG_PL_UE_EX_ID:
        dialog = buildDialog("getPageLogs", "UnsupportedEncodingException");
        break;
    case DIALOG_PL_CP_EX_ID:
        dialog = buildDialog("getPageLogs", "ClientProtocolException");
        break;
    case DIALOG_PL_IO_EX_ID:
        dialog = buildDialog("getPageLogs", "IOException");
        break;
    default:
        dialog = null;
    }
    return dialog;
  }

  public void createList(int id, List<PageRecord> pageLogs) {
    ListView list = (ListView) findViewById(id);
    list.setClickable(true);

    if (pageLogs != null) {
      PagelogAdapter adapter = new PagelogAdapter(this, pageLogs, mDeviceId, mIpAddress);
      list.setAdapter(adapter);
    }
  }

  protected void checkIntent(Intent data) {
    if (data != null) {
      //android.util.Log.i("winston", "checkIntent: action = " + data.getAction() + ", data != null");
      String notification = data.getStringExtra("notification");
      if (notification != null) {
        //android.util.Log.i("winston", "checkIntent: notification = " + notification);
        Toast.makeText(this, notification, Toast.LENGTH_SHORT).show();
      }
    } else {
      //android.util.Log.i("winston", "checkIntent: data == null");
    }
  }

  public List<PageRecord> getPageLogs(int limit) {
    //android.util.Log.v("winston", "getPageLogs mIpAddress = " + mIpAddress);
    if (limit > 0 && mIpAddress != null) {
      String postString = "http://" + mIpAddress + "/rest/request_log";
      try {
        postString += "/device_id/" + URLEncoder.encode(mDeviceId, "UTF-8");
        postString += "/limit/" + limit;
      } catch (UnsupportedEncodingException ex) {
        showDialog(DIALOG_PL_UE_EX_ID);
        throw new RuntimeException("Broken VM does not support UTF-8");
      }

      HttpClient httpclient = new DefaultHttpClient();
      HttpPost httppost = new HttpPost(postString);

       try {
        //android.util.Log.v("winston", postString);
        HttpResponse response = httpclient.execute(httppost);
        HttpEntity entity = response.getEntity();
        if (entity != null) {
          List<PageRecord> pageRecords = new ArrayList<PageRecord>();
          InputStream instream = entity.getContent();
          Request.RequestLog requestLog = Request.RequestLog.parseFrom(instream);
          for (Request.RequestEntry entry : requestLog.getRequestEntryList()) {

            //android.util.Log.v("winston", Integer.toString(entry.getReqTs()) + " -- " + System.currentTimeMillis());

            pageRecords.add(new PageRecord(entry.getId(), entry.getRequest(), null
              , new Date(new Long(entry.getReqTs()) * 1000)
              , new Date(new Long(entry.getAckTs()) * 1000)
              , new Date(new Long(entry.getResTs()) * 1000)
              , entry.getAckName()
              , entry.getResName()
            ));
          }
          return pageRecords;
        }
      } catch (ClientProtocolException e) {
          //android.util.Log.e("winston", "ClientProtocolException: " + e);
          showDialog(DIALOG_PL_CP_EX_ID);
      } catch (IOException e) {
          //android.util.Log.e("winston", "IOException: " + e);
          showDialog(DIALOG_PL_IO_EX_ID);
      }
    }

    List<PageRecord> pageRecords = new ArrayList<PageRecord>();
    pageRecords.add(new PageRecord(0L, "---", "---", System.currentTimeMillis(), 0L, 0L, "", ""));

    return pageRecords;
  }
}
