// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MessageRecord {
  private long mId;
  private String mTopic;
  private String mNote;
  private Date mCreated;

  public MessageRecord(long id, String topic, String note
    , Date created) {
    mId = id;
    mTopic = topic;
    mNote = note;
    mCreated = created;
  }

  public MessageRecord(long id, String topic, String note
    , long created) {
    mId = id;
    mTopic = topic;
    mNote = note;
    mCreated = new Date(created);
  }

  public String getFormattedDate(SimpleDateFormat formatter, Date date) {
    if (!date.equals(new Date(0L))) {
      return formatter.format(date);
    }
    return ("    ");
  }

  public long getId() {
    return mId;
  }

  public String getTopic() {
    return mTopic;
  }

  public String getNote() {
    return mNote;
  }

  public Date getCreated() {
    return mCreated;
  }

  public String getCreated(SimpleDateFormat formatter) {
    return getFormattedDate(formatter, mCreated);
  }


  public String toString() {
    return this.mTopic;
  }
}
