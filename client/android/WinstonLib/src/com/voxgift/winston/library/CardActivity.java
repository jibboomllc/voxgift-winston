// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston.library;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.MapBuilder;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

public class CardActivity extends WinstonActivity {
  public static final int TITLE_ID = R.string.card;
  public static final int SUBTITLE_ID = R.string.card_button_prompt;
  public static final int ICON_ID = R.drawable.ic_card;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.card_grid);

    TextView textView = (TextView) findViewById(R.id.titleLabel);
    textView.setText(TITLE_ID);

    GridView g = (GridView) findViewById(R.id.myGrid);

    SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    float textSize = (float) 22.0 * Float.valueOf(settings.getString("cardsTextSize", "1.0"));
    int countDisplayed = Integer.valueOf(settings.getString("cardsShown", "6"));
    int countRows = 1;
    int countCols = 1;

    Display display = getWindowManager().getDefaultDisplay();
    if (countDisplayed > 1 && countDisplayed < 4) {
      if (display.getWidth() > display.getHeight()) {
        countCols = countDisplayed;
        countRows = 1;
      } else {
        countCols = 1;
        countRows = countDisplayed;
      }
    } else if (countDisplayed < 9) {
      countCols = 2;
      countRows = (countDisplayed + 1) / countCols;
    } else {
      countCols = 3;
      countRows = (countDisplayed + 1) / countCols;
    }
    g.setNumColumns(countCols);
    g.setAdapter(new CardAdapter(this, getDisplayDictionary(), textSize, countCols, countRows));
  }

  public class CardAdapter extends BaseAdapter {
    private Activity mActivity;
    private String[] mStringIDs;
    private float mTextSize;
    private int mCountCols;
    private int mCountRows;
    private MyOnClickListener[] mMyOnClickListeners;
    private MyOnLongClickListener[] mMyOnLongClickListeners;

    public CardAdapter(Activity a, String[] s, float t, int c, int r) {
      mActivity = a;
      mStringIDs = s;
      mTextSize = t;
      mCountCols = c;
      mCountRows = r;

      mMyOnClickListeners = new MyOnClickListener[mStringIDs.length];
      for (int i = 0; i < mStringIDs.length; i++) {
        mMyOnClickListeners[i] = new MyOnClickListener(mStringIDs[i]);
      }

      mMyOnLongClickListeners = new MyOnLongClickListener[mStringIDs.length];
      for (int i = 0; i < mStringIDs.length; i++) {
        mMyOnLongClickListeners[i] = new MyOnLongClickListener(mStringIDs[i]);
      }
    }

    public int getCount() {
      return mStringIDs.length;
    }

    public Object getItem(int position) {
      return position;
    }

    public long getItemId(int position) {
      return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
      int cardHeight = (parent.getHeight() - 50) / mCountRows;
      int cardWidth = (parent.getWidth() - 20) / mCountCols;
    
      Button button;
      //android.util.Log.v("winston", "CardAdapter.getView(" + position + ") id=" + mStringIDs[position]);
      button = new Button(mActivity);
      button.setBackgroundResource(R.drawable.btn_card_custom);
      button.setLayoutParams(new GridView.LayoutParams(cardWidth, cardHeight));
      button.setTextColor(Color.BLACK);
      button.setTextSize(mTextSize);
      button.setOnClickListener(mMyOnClickListeners[position]);
      button.setOnLongClickListener(mMyOnLongClickListeners[position]);
      button.setText(((WinstonApp) mActivity.getApplication()).getSentenceIn(mStringIDs[position]));
      button.setLongClickable(true);
      return button;
    }

    private class MyOnClickListener implements Button.OnClickListener {
      private final String mId;

      public MyOnClickListener(String id) {
        mId = id;
      }

      @Override
      public void onClick(View v) {
        WinstonApp winstonApp = (WinstonApp) (mActivity.getApplication());
        winstonApp.speak(winstonApp.getSentenceIn(mId));
        //android.util.Log.d("winston", "CardAdapter.MyOnClickListener:onItemClick: " + mId);

        // May return null if a EasyTracker has not yet been initialized with a
        // property ID.
        EasyTracker easyTracker = EasyTracker.getInstance(CardActivity.this);

        // MapBuilder.createEvent().build() returns a Map of event fields and values
        // that are set and sent with the hit.
        easyTracker.send(MapBuilder
            .createEvent("Cards",         // Event category (required)
                         "Speak",         // Event action (required)
                         winstonApp.getSentenceIn(mId),   // Event label
                         null)            // Event value
            .build()
        );
      }
    }

    private class MyOnLongClickListener implements Button.OnLongClickListener {
      private final String mId;

      public MyOnLongClickListener(String id) {
        mId = id;
      }

      @Override
      public boolean onLongClick(View v) {
        WinstonApp winstonApp = (WinstonApp) (mActivity.getApplication());
        winstonApp.page(winstonApp.getSentenceIn(mId));

        // May return null if a EasyTracker has not yet been initialized with a
        // property ID.
        EasyTracker easyTracker = EasyTracker.getInstance(CardActivity.this);

        // MapBuilder.createEvent().build() returns a Map of event fields and values
        // that are set and sent with the hit.
        easyTracker.send(MapBuilder
            .createEvent("Cards",         // Event category (required)
                         "Page",          // Event action (required)
                         winstonApp.getSentenceIn(mId),   // Event label
                         null)            // Event value
            .build()
        );
        return true;
      }
    }
  }
}
