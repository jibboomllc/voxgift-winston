/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.voxgift.winston.library;

public final class R {
    public static final class array {
        /**  dentist office resources 
         */
        public static int dentist_patient=0x7f090008;
        public static int dentist_provider=0x7f090009;
        /**  general resources 
         */
        public static int general_patient=0x7f090004;
        public static int general_provider=0x7f090005;
        public static int language_values=0x7f09000d;
        public static int languages=0x7f09000c;
        /**  medical resources 
         */
        public static int medical_patient=0x7f090006;
        public static int medical_provider=0x7f090007;
        /**  orthodontist office resources 
         */
        public static int orthodontist_patient=0x7f09000a;
        public static int orthodontist_provider=0x7f09000b;
        public static int role_values=0x7f090001;
        /**  roles 
         */
        public static int roles=0x7f090000;
        public static int setting_values=0x7f090003;
        /**  dictionaries 
         */
        public static int settings=0x7f090002;
        public static int text_size_strings=0x7f09000e;
        public static int text_size_values=0x7f09000f;
    }
    public static final class attr {
    }
    public static final class bool {
        /** Enable automatic activity tracking
         */
        public static int ga_autoActivityTracking=0x7f060000;
        /** Enable automatic exception tracking
         */
        public static int ga_reportUncaughtExceptions=0x7f060001;
    }
    public static final class dimen {
        public static int border_offset=0x7f080001;
        public static int border_width=0x7f080000;
    }
    public static final class drawable {
        public static int background=0x7f020000;
        public static int backrepeat=0x7f020001;
        public static int border=0x7f020002;
        public static int btn_ack=0x7f020003;
        public static int btn_ack_normal=0x7f020004;
        public static int btn_ack_pressed=0x7f020005;
        public static int btn_card=0x7f020006;
        public static int btn_card_2=0x7f020007;
        public static int btn_card_custom=0x7f020008;
        public static int btn_close=0x7f020009;
        public static int btn_close_normal=0x7f02000a;
        public static int btn_close_pressed=0x7f02000b;
        public static int button=0x7f02000c;
        public static int button_text_color=0x7f02000d;
        public static int ic_ack=0x7f02000e;
        public static int ic_add=0x7f02000f;
        public static int ic_back=0x7f020010;
        public static int ic_build=0x7f020011;
        public static int ic_card=0x7f020012;
        public static int ic_circle=0x7f020013;
        public static int ic_close=0x7f020014;
        public static int ic_delete=0x7f020015;
        public static int ic_feedback=0x7f020016;
        public static int ic_help=0x7f020017;
        public static int ic_home=0x7f020018;
        public static int ic_launcher=0x7f020019;
        public static int ic_message=0x7f02001a;
        public static int ic_move=0x7f02001b;
        public static int ic_next=0x7f02001c;
        public static int ic_page=0x7f02001d;
        public static int ic_page_grey=0x7f02001e;
        public static int ic_pagelog=0x7f02001f;
        public static int ic_settings=0x7f020020;
        public static int ic_speak=0x7f020021;
        public static int ic_status=0x7f020022;
        public static int ic_status_bar=0x7f020023;
        public static int ic_talk=0x7f020024;
        public static int ic_talk_grey=0x7f020025;
        public static int ic_type=0x7f020026;
        public static int nav_btn_black=0x7f020027;
        public static int navbar_background=0x7f020028;
        public static int rounded_layout=0x7f020029;
    }
    public static final class id {
        public static int LinearLayout00=0x7f0c0012;
        public static int LinearLayout01=0x7f0c0014;
        public static int Messagelog=0x7f0c0017;
        public static int Pagelog=0x7f0c001c;
        public static int RLayout02=0x7f0c001e;
        public static int RequestList=0x7f0c0021;
        public static int addButton=0x7f0c0006;
        public static int appName=0x7f0c000d;
        public static int availability=0x7f0c002b;
        public static int availability_label=0x7f0c002a;
        public static int backButton=0x7f0c0018;
        public static int btnAck=0x7f0c001f;
        public static int btnCancel=0x7f0c0032;
        public static int btnClose=0x7f0c0020;
        public static int btnDelete=0x7f0c0009;
        public static int btnMove=0x7f0c0008;
        public static int btnSave=0x7f0c0031;
        public static int doneButton=0x7f0c0005;
        public static int fragometer_spinner=0x7f0c0003;
        public static int home=0x7f0c0037;
        public static int icon=0x7f0c000f;
        public static int input_text=0x7f0c0035;
        public static int ivIcon=0x7f0c0013;
        public static int mainLayout=0x7f0c000c;
        public static int menuList=0x7f0c000e;
        public static int myGrid=0x7f0c0004;
        public static int navBar=0x7f0c0000;
        public static int on_duty=0x7f0c0029;
        public static int page_button=0x7f0c0034;
        public static int page_button0=0x7f0c0024;
        public static int page_button1=0x7f0c0025;
        public static int page_button2=0x7f0c0026;
        public static int page_button3=0x7f0c0027;
        public static int page_button4=0x7f0c0028;
        public static int prompt=0x7f0c0022;
        public static int rdb1=0x7f0c002c;
        public static int rdb2=0x7f0c002d;
        public static int rdb3=0x7f0c002e;
        public static int rdb4=0x7f0c002f;
        public static int rdb5=0x7f0c0030;
        public static int settings=0x7f0c0036;
        public static int speak_button=0x7f0c0023;
        public static int subtitle=0x7f0c0011;
        public static int talk_button=0x7f0c0033;
        public static int talkpage=0x7f0c0001;
        public static int title=0x7f0c0010;
        public static int titleLabel=0x7f0c0007;
        public static int tvLabel=0x7f0c000a;
        public static int tvLastAck=0x7f0c001a;
        public static int tvName=0x7f0c001d;
        public static int tvNote=0x7f0c0016;
        public static int tvRequest=0x7f0c0019;
        public static int tvRespondedTo=0x7f0c001b;
        public static int tvTopic=0x7f0c0015;
        public static int valuedisplay=0x7f0c0002;
        public static int webview=0x7f0c000b;
    }
    public static final class layout {
        public static int build=0x7f030000;
        public static int card_grid=0x7f030001;
        public static int customdictionaryview=0x7f030002;
        public static int dragitem=0x7f030003;
        public static int dragndroplistview=0x7f030004;
        public static int feedback=0x7f030005;
        public static int main=0x7f030006;
        public static int main_menu_row=0x7f030007;
        public static int message_row=0x7f030008;
        public static int messagelog=0x7f030009;
        public static int navbar=0x7f03000a;
        public static int page_row=0x7f03000b;
        public static int pagelog=0x7f03000c;
        public static int request_row=0x7f03000d;
        public static int requests=0x7f03000e;
        public static int speak=0x7f03000f;
        public static int status=0x7f030010;
        public static int talkpage=0x7f030011;
        public static int type=0x7f030012;
    }
    public static final class menu {
        public static int main_options=0x7f0b0000;
        public static int options=0x7f0b0001;
    }
    public static final class raw {
        public static int dictionary=0x7f050000;
        public static int page=0x7f050001;
    }
    public static final class string {
        public static int acknowledged_by=0x7f07004f;
        public static int app_name=0x7f070007;
        public static int at_lunch=0x7f070042;
        public static int availability=0x7f07003a;
        public static int available=0x7f07003f;
        public static int back=0x7f070010;
        public static int build=0x7f070011;
        public static int build_button_prompt=0x7f070053;
        public static int build_prompt=0x7f070046;
        public static int buttongrid_prompt=0x7f070045;
        public static int cancel=0x7f07003c;
        public static int card=0x7f070012;
        public static int card_button_prompt=0x7f070052;
        public static int cards_shown=0x7f070028;
        public static int cards_text_size=0x7f070029;
        public static int chinese=0x7f07000c;
        public static int com_voxgift_winston_library_BuildActivity=0x7f070001;
        public static int com_voxgift_winston_library_CardActivity=0x7f070002;
        public static int com_voxgift_winston_library_CustomDictionaryActivity=0x7f070005;
        public static int com_voxgift_winston_library_EditPreferences=0x7f070006;
        public static int com_voxgift_winston_library_FeedbackActivity=0x7f070004;
        /**  The screen names that will appear in reports 
 Uses Winston's iOS class names since that client was released first 
         */
        public static int com_voxgift_winston_library_MainActivity=0x7f070000;
        public static int com_voxgift_winston_library_TypeActivity=0x7f070003;
        public static int copyright=0x7f07005b;
        public static int custom=0x7f070023;
        public static int custom_placeholder=0x7f070031;
        public static int custom_summary=0x7f070030;
        public static int dentist=0x7f070026;
        public static int dictionary_category=0x7f07001c;
        public static int done=0x7f07003e;
        public static int edit=0x7f07003d;
        public static int english=0x7f070008;
        public static int error_no_conn=0x7f070037;
        public static int feedback=0x7f070017;
        public static int feedback_button_prompt=0x7f070059;
        public static int french=0x7f070009;
        public static int general=0x7f070024;
        public static int general_category=0x7f070044;
        public static int german=0x7f07000a;
        public static int home=0x7f07000f;
        public static int in_meeting=0x7f070043;
        public static int input_language=0x7f07002b;
        public static int ip_address=0x7f070035;
        public static int ip_address_summary=0x7f070036;
        public static int japanese=0x7f07000d;
        public static int korean=0x7f07000e;
        public static int last_ack=0x7f07004e;
        public static int loading=0x7f07005c;
        public static int medical=0x7f070025;
        public static int not_available=0x7f070040;
        public static int on_break=0x7f070041;
        public static int on_duty=0x7f070039;
        public static int orthodontist=0x7f070027;
        public static int output_category=0x7f07002c;
        public static int output_language=0x7f07002d;
        public static int output_language_summary=0x7f07002f;
        public static int page=0x7f070019;
        public static int pagelog=0x7f070016;
        public static int pagelog_button_prompt=0x7f070054;
        public static int pagelog_prompt=0x7f070047;
        public static int patient=0x7f070021;
        public static int provider=0x7f070022;
        public static int requests=0x7f070015;
        public static int requests_button_prompt=0x7f070055;
        public static int requests_prompt=0x7f070048;
        public static int responded_to_by=0x7f070050;
        public static int role=0x7f07001d;
        public static int role_summary=0x7f07001e;
        public static int save=0x7f07003b;
        public static int send_receive=0x7f070033;
        public static int send_receive_category=0x7f070032;
        public static int send_receive_summary=0x7f070034;
        public static int setting=0x7f07001f;
        public static int setting_summary=0x7f070020;
        public static int settings=0x7f070018;
        public static int settings_button_prompt=0x7f07005a;
        public static int spanish=0x7f07000b;
        public static int speak=0x7f070014;
        public static int speak_button_prompt=0x7f070057;
        public static int speak_hint=0x7f07004b;
        public static int speak_prompt=0x7f070049;
        public static int start_page_prompt=0x7f070051;
        public static int status=0x7f07001b;
        public static int status_button_prompt=0x7f070058;
        public static int status_category=0x7f070038;
        public static int status_prompt=0x7f07004a;
        public static int talk=0x7f07001a;
        public static int talk_summary=0x7f07002e;
        public static int type=0x7f070013;
        public static int type_button_prompt=0x7f070056;
        public static int type_hint=0x7f07004d;
        public static int type_prompt=0x7f07004c;
        public static int type_text_size=0x7f07002a;
    }
    public static final class style {
        public static int Button=0x7f0a0000;
        public static int ButtonText=0x7f0a0003;
        public static int CarbonFiber=0x7f0a0005;
        public static int NavButton=0x7f0a0002;
        public static int NavText=0x7f0a0001;
        public static int RoundedLayout=0x7f0a0004;
        public static int TransparentExpandableListView=0x7f0a0007;
        public static int TransparentListView=0x7f0a0006;
    }
    public static final class xml {
        public static int settings=0x7f040000;
    }
}
