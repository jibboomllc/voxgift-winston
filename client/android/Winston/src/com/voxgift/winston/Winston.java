// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winston;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.voxgift.winston.library.CardActivity;
import com.voxgift.winston.library.BuildActivity;
import com.voxgift.winston.library.EditPreferences;
import com.voxgift.winston.library.FeedbackActivity;
import com.voxgift.winston.library.MainMenuAdapter;
import com.voxgift.winston.library.TypeActivity;
import com.voxgift.winston.library.MainActivity;

public class Winston extends MainActivity {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mMenuItems.add(CardActivity.class);
    mMenuItems.add(BuildActivity.class);
    mMenuItems.add(TypeActivity.class);
    mMenuItems.add(FeedbackActivity.class);
    mMenuItems.add(EditPreferences.class);
    ListView lv = (ListView) findViewById(R.id.menuList);
    lv.setAdapter(new MainMenuAdapter(this, mMenuItems));

    lv.setOnItemClickListener(new OnItemClickListener() {
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent newIntent = new Intent(Winston.this, (mMenuItems.get(position)));
        Winston.this.startActivity(newIntent);
      }
    });
  }

}
