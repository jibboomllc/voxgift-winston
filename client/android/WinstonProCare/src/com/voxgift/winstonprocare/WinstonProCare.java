// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

package com.voxgift.winstonprocare;

import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.voxgift.winston.library.RequestsActivity;
import com.voxgift.winston.library.CardActivity;
import com.voxgift.winston.library.BuildActivity;
import com.voxgift.winston.library.StatusActivity;
import com.voxgift.winston.library.MainMenuAdapter;
import com.voxgift.winston.library.MainActivity;

public class WinstonProCare extends MainActivity {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    mMenuItems.add(RequestsActivity.class);
    mMenuItems.add(CardActivity.class);
    mMenuItems.add(BuildActivity.class);
    mMenuItems.add(StatusActivity.class);
    ListView lv = (ListView) findViewById(R.id.menuList);
    lv.setAdapter(new MainMenuAdapter(this, mMenuItems));

    lv.setOnItemClickListener(new OnItemClickListener() {
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent newIntent = new Intent(WinstonProCare.this, (mMenuItems.get(position)));
        WinstonProCare.this.startActivity(newIntent);
      }
    });
  }

}
