// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "VGWinstonAppViewController.h"
@interface VGWinstonAppViewController ()

@end

@implementation VGWinstonAppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self && self.buttonFlags) {
        //[self.buttonFlags setObject:[NSNumber numberWithBool:NO] forKey:@"requests"];
        // FIXME should be build option
        [self.buttonFlags setObject:[NSNumber numberWithBool:NO] forKey:@"requests"];
        
        self.appTitle = [NSMutableString stringWithFormat:@"Winston"];
    }
    return self;
}

@end
