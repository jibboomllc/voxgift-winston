// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>

#import "VGWinstonAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VGWinstonAppDelegate class]));
    }
}
