// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "VGWinstonAppDelegate.h"
#import "VGWinstonAppViewController.h"

@implementation VGWinstonAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.navController = [[UINavigationController alloc] init];
    VGWinstonAppViewController *appViewController = [[VGWinstonAppViewController alloc] init];
	[self.navController pushViewController:appViewController animated:FALSE];
	return [super application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions];
}

@end
