// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <AVFoundation/AVFoundation.h>
#import "AndroidXMLParser.h"
#import "GAI.h"
#import "parseCSV.h"
#import "StringsXMLParser.h"
#import "StringArraysXMLParser.h"
#import "VGAppDelegate.h"
#import "VGAppViewController.h"

@implementation VGAppDelegate

@synthesize window = _window;
@synthesize navController = _navController;
@synthesize libResourceBundle;
@synthesize dictStrings;
@synthesize dictStringArrays;
@synthesize inputDictionary;
@synthesize outputDictionary;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self initUserDefaults : [NSUserDefaults standardUserDefaults]];

    // Lib res ref: http://iphone.galloway.me.uk/iphone-sdktutorials/ios-library-with-resources/
    
    self.libResourceBundle = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"WinstonLibRes" withExtension:@"bundle"]];
    
    dictStrings  = [[NSMutableDictionary alloc] init];
    dictStringArrays  = [[NSMutableDictionary alloc] init];
    inputDictionary  = [[NSMutableDictionary alloc] init];
    outputDictionary  = [[NSMutableDictionary alloc] init];
    [self readAllResources];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	//[self.window addSubview:[self.navController view]];
    if ([self.window respondsToSelector:@selector(setRootViewController:)]) {
        self.window.rootViewController = self.navController;
    }
    
    [self.window makeKeyAndVisible];
    
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    // [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Optional: set while debugging
    // [[GAI sharedInstance] setDryRun:YES];

    // Initialize tracker.
    // __attribute__((unused)) id<GAITracker> tracker =
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-28027958-4"];

    return YES;
}

- (void)initUserDefaults : (NSUserDefaults *)defaults
{
    if ([defaults objectForKey:@"prefRole"] == nil) {
        [defaults setObject:@"patient" forKey:@"prefRole"];
    }
    
    if ([defaults objectForKey:@"prefSetting"] == nil) {
        [defaults setObject:@"general" forKey:@"prefSetting"];
    }
    
    if ([defaults objectForKey:@"prefNumberOfCards"] == nil) {
        [defaults setObject:@"6" forKey:@"prefNumberOfCards"];
    }
    
    if ([defaults objectForKey:@"prefCardsTextSize"] == nil) {
        [defaults setObject:@"1.0" forKey:@"prefCardsTextSize"];
    }
    
    if ([defaults objectForKey:@"prefTypeTextSize"] == nil) {
        [defaults setObject:@"1.0" forKey:@"prefTypeTextSize"];
    }
    
    if ([defaults objectForKey:@"prefOutputLanguageLocale"] == nil) {
        [defaults setObject:[AVSpeechSynthesisVoice currentLanguageCode] forKey:@"prefOutputLanguageLocale"];
    }

    if ([defaults objectForKey:@"prefSpeechRate"] == nil) {
        [defaults setObject:[NSString stringWithFormat:@"%f", AVSpeechUtteranceMaximumSpeechRate/4] forKey:@"prefSpeechRate"];
    }
    
    if ([defaults objectForKey:@"prefIpAddress"] == nil) {
        // FIXME should be build option
        // [defaults setObject:@"108.166.65.150" forKey:@"prefIpAddress"];
        [defaults setObject:@"www.voxgift.com" forKey:@"prefIpAddress"];
    }
}

- (void) readAllResources
{
    // read non-translated strings
    NSString *file = [libResourceBundle pathForResource:@"output_locales" ofType:@"xml" inDirectory:@"/res/values"];

    [dictStrings addEntriesFromDictionary : [self getValuesFromXMLFile: file : [[StringsXMLParser alloc] initXMLParser]]];
    [dictStringArrays addEntriesFromDictionary : [self getValuesFromXMLFile: file : [[StringArraysXMLParser alloc] initXMLParser]]];

    file = [libResourceBundle pathForResource:@"display_dictionaries" ofType:@"xml" inDirectory:@"/res/values"];  
    [dictStringArrays addEntriesFromDictionary : [self getValuesFromXMLFile: file : [[StringArraysXMLParser alloc] initXMLParser]]];
 
    // read translated strings
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [self setInputLanguage:[self getBestFitLanguageLocal:[NSLocale preferredLanguages]]];
    [self setOutputLanguage:[defaults stringForKey:@"prefOutputLanguageLocale"]];
}

- (NSString *)getBestFitLanguageLocal:(NSArray *) forRepresentations {
    unsigned long index = NSNotFound;
    NSString *representation = [[NSString alloc] init];
    
    for(representation in forRepresentations) {
        index = [[[self dictStringArrays] valueForKey:@"language_locale_values"] indexOfObject:representation];
        if (index == NSNotFound) {
            index = [[[self dictStringArrays] valueForKey:@"language_values"] indexOfObject:[representation substringToIndex:2]];
        }
        if (index != NSNotFound) {
            break;
        }
    }
    if (index == NSNotFound) {
        index = 0; // en-US
    }
    return [[self dictStringArrays] valueForKey:@"language_locale_values"][index];
}

- (NSDictionary *)getValuesFromXMLFile : (NSString *) xmlPath : (AndroidXMLParser *) delegate {
    NSDictionary *values = Nil;
    
    if (xmlPath != Nil) {
        //NSLog(@"xmlPath: %@", xmlPath);

        NSData *xmlData = [NSData dataWithContentsOfFile:xmlPath];
        
        if (xmlData != Nil) {
            NSXMLParser *nsXmlParser = [[NSXMLParser alloc] initWithData:xmlData];
            
            if (nsXmlParser != Nil) {
                [nsXmlParser setDelegate:delegate];
                
                if ([nsXmlParser parse]) {
                    //NSLog(@"No errors - value count : %i", parser.values.count);
                    values = delegate.values;        
                } else {
                    NSLog(@"Error parsing document! - %@", xmlPath);
                }            
            }
        }
    }
    return values;
}

- (NSString *)setInputLanguage:(NSString *) inLanguage
{
    NSString *outStr = [self getBestFitLanguageLocal:[NSArray arrayWithObject:inLanguage]];
    
    // save the full xx-YY string in user prefs
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:outStr forKey:@"prefInputLanguage"];

    // start reading the translated resources reading from least desirable to most updating the dictionaries as we go
    
    // seed with the unadorned (without xx-YY) which happens to be en-US
    [inputDictionary setDictionary: [self readDictionary:[[NSString alloc] initWithFormat: @"/res/raw"]]];
    NSString *directory = [[NSString alloc] initWithFormat: @"/res/values"];
    NSString *stringsFilePath = [libResourceBundle pathForResource:@"strings" ofType:@"xml" inDirectory:directory];
    [dictStrings addEntriesFromDictionary : [self getValuesFromXMLFile: stringsFilePath : [[StringsXMLParser alloc] initXMLParser]]];

    
    // next try without the locale (without -YY)
    directory = [[NSString alloc] initWithFormat: @"/res/values-%@",[outStr substringToIndex:2]];
    stringsFilePath = [libResourceBundle pathForResource:@"strings" ofType:@"xml" inDirectory:directory];
    if (stringsFilePath != nil) {
        
        [inputDictionary addEntriesFromDictionary: [self readDictionary:[[NSString alloc] initWithFormat: @"/res/raw-%@",[outStr substringToIndex:2]]]];
        [dictStrings addEntriesFromDictionary : [self getValuesFromXMLFile: stringsFilePath : [[StringsXMLParser alloc] initXMLParser]]];
    }

    // finally try the full xx-YY path
    directory = [[NSString alloc] initWithFormat: @"/res/values-%@",outStr];
    stringsFilePath = [libResourceBundle pathForResource:@"strings" ofType:@"xml" inDirectory:directory];
    if (stringsFilePath != nil) {
        
        [inputDictionary addEntriesFromDictionary: [self readDictionary:[[NSString alloc] initWithFormat: @"/res/raw-%@",outStr]]];
        [dictStrings addEntriesFromDictionary : [self getValuesFromXMLFile: stringsFilePath : [[StringsXMLParser alloc] initXMLParser]]];
    }
    
    return outStr;
}

- (NSString *)setInputLanguage00:(NSString *) inLanguage
{
    NSString *outStr = [self getBestFitLanguageLocal:[NSArray arrayWithObject:inLanguage]];
    
    // first try the full xx-YY path
    [inputDictionary setDictionary: [self readDictionary:[[NSString alloc] initWithFormat: @"/res/raw-%@",outStr]]];
    NSString *directory = [[NSString alloc] initWithFormat: @"/res/values-%@",outStr];
    NSString *stringsFilePath = [libResourceBundle pathForResource:@"strings" ofType:@"xml" inDirectory:directory];
    
    if (inputDictionary == nil || [inputDictionary count] == 0 || stringsFilePath == nil) {
        
        // next try without the locale (without -YY)
        [inputDictionary setDictionary: [self readDictionary:[[NSString alloc] initWithFormat: @"/res/raw-%@",[outStr substringToIndex:2]]]];
        NSString *directory = [[NSString alloc] initWithFormat: @"/res/values-%@",[outStr substringToIndex:2]];
        stringsFilePath = [libResourceBundle pathForResource:@"strings" ofType:@"xml" inDirectory:directory];
        
        if (inputDictionary == nil || [inputDictionary count] == 0 || stringsFilePath == nil) {
            
            // fail over to the unadorned (without xx-YY) which happens to be en-US
            [inputDictionary setDictionary: [self readDictionary:[[NSString alloc] initWithFormat: @"/res/raw"]]];
            directory = [[NSString alloc] initWithFormat: @"/res/values"];
            stringsFilePath = [libResourceBundle pathForResource:@"strings" ofType:@"xml" inDirectory:directory];
        }
    }
    
    [dictStrings addEntriesFromDictionary : [self getValuesFromXMLFile: stringsFilePath : [[StringsXMLParser alloc] initXMLParser]]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:outStr forKey:@"prefInputLanguage"];
    
    return outStr;
}

- (NSString *)setOutputLanguage:(NSString *) outLanguage;
{
    NSString *outStr = [self getBestFitLanguageLocal:[NSArray arrayWithObject:outLanguage]];
    
    [outputDictionary setDictionary: [self readDictionary:[[NSString alloc] initWithFormat: @"/res/raw-%@",outStr]]];
    if (outputDictionary == nil || [outputDictionary count] == 0) {
        [outputDictionary setDictionary: [self readDictionary:[[NSString alloc] initWithFormat: @"/res/raw-%@",[outStr substringToIndex:2]]]];
        if (outputDictionary == nil || [outputDictionary count] == 0) {
            [outputDictionary setDictionary: [self readDictionary:[[NSString alloc] initWithFormat: @"/res/raw"]]];
        }
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:outStr forKey:@"prefOutputLanguageLocale"];
    
    return outStr;
}

- (NSDictionary *) readDictionary:(NSString *) directory
{   
    NSMutableDictionary *dictionary = nil;
    NSString *csvFilePath = [libResourceBundle pathForResource:@"dictionary" ofType:@"csv" inDirectory:directory];

    if (csvFilePath != nil) {
        dictionary = [[NSMutableDictionary alloc] init];
        CSVParser *parser = [[CSVParser alloc] init];  
        [parser setEncoding:NSUTF8StringEncoding];  
        [parser openFile:csvFilePath];  
        
        NSMutableArray *csvContent = [parser parseFile];  
        NSString *key;
        NSString *value;
        NSMutableArray *line;
        for (int i = 0; i < [csvContent count]; i++) {  
            //NSLog(@"content of line %d: %@", i, [csvContent objectAtIndex:i]);  
            line = [csvContent objectAtIndex:i];
            key = [line objectAtIndex:0];
            value = [line objectAtIndex:1];
            for (int j = 2; j < [line count]; j++) {  
                value = [[NSString alloc] initWithFormat:@"%@ %@", value, [line objectAtIndex:j]]; 
            }
            value = [value stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            //NSLog(@"k,v %d: %@, [%@]", i, key, value);
            [dictionary setObject:value forKey:key];
        }  
        [parser closeFile];  
    }
    
    return dictionary;
}  

- (NSString *)checkString:(NSString *) inStr 
{
    NSString *outStr = [[NSString alloc] initWithString:inStr];
    NSRange range = [inStr rangeOfString:@"@string/"];
    if (range.location != NSNotFound) {
        outStr = [[inStr substringFromIndex:NSMaxRange(range)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        outStr = [dictStrings valueForKey:outStr];
    }

//    NSLog(outStr);
    return outStr;
}

/////////////////////////////////////////////////////////////////////

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
