// Generated by the protocol buffer compiler.  DO NOT EDIT!

#import "ProtocolBuffers.h"

@class RequestEntry;
@class RequestEntry_Builder;
@class RequestLog;
@class RequestLog_Builder;

@interface RequestRoot : NSObject {
}
+ (PBExtensionRegistry*) extensionRegistry;
+ (void) registerAllExtensions:(PBMutableExtensionRegistry*) registry;
@end

@interface RequestEntry : PBGeneratedMessage {
@private
  BOOL hasId_:1;
  BOOL hasReqTs_:1;
  BOOL hasAckTs_:1;
  BOOL hasResTs_:1;
  BOOL hasRequest_:1;
  BOOL hasRequestor_:1;
  BOOL hasReqDeviceId_:1;
  BOOL hasAckDeviceId_:1;
  BOOL hasAckName_:1;
  BOOL hasResDeviceId_:1;
  BOOL hasResName_:1;
  int32_t id;
  int32_t reqTs;
  int32_t ackTs;
  int32_t resTs;
  NSString* request;
  NSString* requestor;
  NSString* reqDeviceId;
  NSString* ackDeviceId;
  NSString* ackName;
  NSString* resDeviceId;
  NSString* resName;
}
- (BOOL) hasId;
- (BOOL) hasRequest;
- (BOOL) hasRequestor;
- (BOOL) hasReqDeviceId;
- (BOOL) hasReqTs;
- (BOOL) hasAckDeviceId;
- (BOOL) hasAckTs;
- (BOOL) hasAckName;
- (BOOL) hasResDeviceId;
- (BOOL) hasResTs;
- (BOOL) hasResName;
@property (readonly) int32_t id;
@property (readonly, retain) NSString* request;
@property (readonly, retain) NSString* requestor;
@property (readonly, retain) NSString* reqDeviceId;
@property (readonly) int32_t reqTs;
@property (readonly, retain) NSString* ackDeviceId;
@property (readonly) int32_t ackTs;
@property (readonly, retain) NSString* ackName;
@property (readonly, retain) NSString* resDeviceId;
@property (readonly) int32_t resTs;
@property (readonly, retain) NSString* resName;

+ (RequestEntry*) defaultInstance;
- (RequestEntry*) defaultInstance;

- (BOOL) isInitialized;
- (void) writeToCodedOutputStream:(PBCodedOutputStream*) output;
- (RequestEntry_Builder*) builder;
+ (RequestEntry_Builder*) builder;
+ (RequestEntry_Builder*) builderWithPrototype:(RequestEntry*) prototype;

+ (RequestEntry*) parseFromData:(NSData*) data;
+ (RequestEntry*) parseFromData:(NSData*) data extensionRegistry:(PBExtensionRegistry*) extensionRegistry;
+ (RequestEntry*) parseFromInputStream:(NSInputStream*) input;
+ (RequestEntry*) parseFromInputStream:(NSInputStream*) input extensionRegistry:(PBExtensionRegistry*) extensionRegistry;
+ (RequestEntry*) parseFromCodedInputStream:(PBCodedInputStream*) input;
+ (RequestEntry*) parseFromCodedInputStream:(PBCodedInputStream*) input extensionRegistry:(PBExtensionRegistry*) extensionRegistry;
@end

@interface RequestEntry_Builder : PBGeneratedMessage_Builder {
@private
  RequestEntry* result;
}

- (RequestEntry*) defaultInstance;

- (RequestEntry_Builder*) clear;
- (RequestEntry_Builder*) clone;

- (RequestEntry*) build;
- (RequestEntry*) buildPartial;

- (RequestEntry_Builder*) mergeFrom:(RequestEntry*) other;
- (RequestEntry_Builder*) mergeFromCodedInputStream:(PBCodedInputStream*) input;
- (RequestEntry_Builder*) mergeFromCodedInputStream:(PBCodedInputStream*) input extensionRegistry:(PBExtensionRegistry*) extensionRegistry;

- (BOOL) hasId;
- (int32_t) id;
- (RequestEntry_Builder*) setId:(int32_t) value;
- (RequestEntry_Builder*) clearId;

- (BOOL) hasRequest;
- (NSString*) request;
- (RequestEntry_Builder*) setRequest:(NSString*) value;
- (RequestEntry_Builder*) clearRequest;

- (BOOL) hasRequestor;
- (NSString*) requestor;
- (RequestEntry_Builder*) setRequestor:(NSString*) value;
- (RequestEntry_Builder*) clearRequestor;

- (BOOL) hasReqDeviceId;
- (NSString*) reqDeviceId;
- (RequestEntry_Builder*) setReqDeviceId:(NSString*) value;
- (RequestEntry_Builder*) clearReqDeviceId;

- (BOOL) hasReqTs;
- (int32_t) reqTs;
- (RequestEntry_Builder*) setReqTs:(int32_t) value;
- (RequestEntry_Builder*) clearReqTs;

- (BOOL) hasAckDeviceId;
- (NSString*) ackDeviceId;
- (RequestEntry_Builder*) setAckDeviceId:(NSString*) value;
- (RequestEntry_Builder*) clearAckDeviceId;

- (BOOL) hasAckTs;
- (int32_t) ackTs;
- (RequestEntry_Builder*) setAckTs:(int32_t) value;
- (RequestEntry_Builder*) clearAckTs;

- (BOOL) hasAckName;
- (NSString*) ackName;
- (RequestEntry_Builder*) setAckName:(NSString*) value;
- (RequestEntry_Builder*) clearAckName;

- (BOOL) hasResDeviceId;
- (NSString*) resDeviceId;
- (RequestEntry_Builder*) setResDeviceId:(NSString*) value;
- (RequestEntry_Builder*) clearResDeviceId;

- (BOOL) hasResTs;
- (int32_t) resTs;
- (RequestEntry_Builder*) setResTs:(int32_t) value;
- (RequestEntry_Builder*) clearResTs;

- (BOOL) hasResName;
- (NSString*) resName;
- (RequestEntry_Builder*) setResName:(NSString*) value;
- (RequestEntry_Builder*) clearResName;
@end

@interface RequestLog : PBGeneratedMessage {
@private
  NSMutableArray* mutableRequestEntryList;
}
- (NSArray*) requestEntryList;
- (RequestEntry*) requestEntryAtIndex:(int32_t) index;

+ (RequestLog*) defaultInstance;
- (RequestLog*) defaultInstance;

- (BOOL) isInitialized;
- (void) writeToCodedOutputStream:(PBCodedOutputStream*) output;
- (RequestLog_Builder*) builder;
+ (RequestLog_Builder*) builder;
+ (RequestLog_Builder*) builderWithPrototype:(RequestLog*) prototype;

+ (RequestLog*) parseFromData:(NSData*) data;
+ (RequestLog*) parseFromData:(NSData*) data extensionRegistry:(PBExtensionRegistry*) extensionRegistry;
+ (RequestLog*) parseFromInputStream:(NSInputStream*) input;
+ (RequestLog*) parseFromInputStream:(NSInputStream*) input extensionRegistry:(PBExtensionRegistry*) extensionRegistry;
+ (RequestLog*) parseFromCodedInputStream:(PBCodedInputStream*) input;
+ (RequestLog*) parseFromCodedInputStream:(PBCodedInputStream*) input extensionRegistry:(PBExtensionRegistry*) extensionRegistry;
@end

@interface RequestLog_Builder : PBGeneratedMessage_Builder {
@private
  RequestLog* result;
}

- (RequestLog*) defaultInstance;

- (RequestLog_Builder*) clear;
- (RequestLog_Builder*) clone;

- (RequestLog*) build;
- (RequestLog*) buildPartial;

- (RequestLog_Builder*) mergeFrom:(RequestLog*) other;
- (RequestLog_Builder*) mergeFromCodedInputStream:(PBCodedInputStream*) input;
- (RequestLog_Builder*) mergeFromCodedInputStream:(PBCodedInputStream*) input extensionRegistry:(PBExtensionRegistry*) extensionRegistry;

- (NSArray*) requestEntryList;
- (RequestEntry*) requestEntryAtIndex:(int32_t) index;
- (RequestLog_Builder*) replaceRequestEntryAtIndex:(int32_t) index with:(RequestEntry*) value;
- (RequestLog_Builder*) addRequestEntry:(RequestEntry*) value;
- (RequestLog_Builder*) addAllRequestEntry:(NSArray*) values;
- (RequestLog_Builder*) clearRequestEntryList;
@end

