// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "VGOutputViewController.h"

@interface VGCardsViewController : VGOutputViewController {
//    NSMutableArray *viewButtons;
}

@property UIView* cardsView;
@property (strong) NSMutableArray *viewButtons;
@property (strong) UILabel *selectedCard;

@end
