// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

//  http://stackoverflow.com/questions/8869445/how-can-i-use-mqtt-in-ios

#import "UIDevice+Identifier.h"
#import "UIDevice+Orientation.h"
#import "UIImage+WinstonLib.h"
#import "UIViewController+MaxFrame.h"
#import "iToast.h"

#import "VGRequestCell.h"
#import "VGRequestsViewController.h"

#define ROW_HEIGHT 60

@interface VGRequestsViewController ()
@property (nonatomic, retain) RequestLog *requestLog;
@end

@implementation VGRequestsViewController

@synthesize response;
@synthesize requestLog;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Open requests";
    //self.view.backgroundColor = [UIColor blackColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.rowHeight = ROW_HEIGHT;
                               
    [self getOpenRequests];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	
	self.requestLog = nil;
	self.response = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
	return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.requestLog.requestEntryList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"RequestCell";

    VGRequestCell *cell = (VGRequestCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        CGRect screenRect=[self maxUsableFrame];
        cell = [[VGRequestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.frame = CGRectMake(0.0, 0.0, screenRect.size.width, ROW_HEIGHT);
        UIButton *ackButton = (UIButton *)[cell getAckButton];
        [ackButton addTarget:self action:@selector(ackRequest:event:) forControlEvents:UIControlEventTouchUpInside];
        UIButton *closeButton = (UIButton *)[cell getCloseButton];
        [closeButton addTarget:self action:@selector(closeRequest:event:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [cell setRequestEntry:[self.requestLog.requestEntryList objectAtIndex:indexPath.row]];

    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//////////////////////////////////////////////////////////////////////////////
- (void)ackRequest:(id)sender event:(id)event {
    CGPoint touchPosition = [[[event allTouches] anyObject] locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchPosition];
    if (indexPath != nil)  {
        RequestEntry *requestEntry = [self.requestLog.requestEntryList objectAtIndex:indexPath.row];
        [self ackRequest:requestEntry.id];
        [[[iToast makeText:[NSString stringWithFormat:@"You acknowledged: %@!", requestEntry.request]]setGravity:iToastGravityBottom] show];
    }
}

- (void)closeRequest:(id)sender event:(id)event {
    CGPoint touchPosition = [[[event allTouches] anyObject] locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchPosition];
    if (indexPath != nil)  {
       RequestEntry *requestEntry = [self.requestLog.requestEntryList objectAtIndex:indexPath.row];
        [[[iToast makeText:[NSString stringWithFormat:@"You closed: %@!", requestEntry.request]]setGravity:iToastGravityBottom] show];
    }
}


/////////////////////////////////////////////////////////////////////////////////////////////
/*
cd ~/Development/winston/trunk/server/php/private_html/pb 

~/Development/ProtocolBuffers-2.2.0-Source/src/protoc --proto_path=. --objc_out=~/Development/winston/trunk/client/ios/WinstonLib ./request.proto
~/Development/ProtocolBuffers-2.2.0-Source/src/protoc --proto_path=. --objc_out=../../../../client/ios/WinstonLib ./request.proto

*/

/////////////////////////////////////////////////////////////////////////////////////////////

- (void)getOpenRequests {
    [self postAction:[self getPostURL: @"open_requests"]];
}

- (void)ackRequest:(int32_t) reqId {
    [self postAction:[NSString stringWithFormat:@"%@/id/%d",[self getPostURL: @"ack"], reqId]];
}

- (void)closeRequest:(int32_t) reqId {
    [self postAction:[NSString stringWithFormat:@"%@/id/%d",[self getPostURL: @"close"], reqId]];
}

/////////////////////////////////////////////////////////////////////////////////////////////
- (NSString *) getPostURL:(NSString*) action {
    NSString *strUrl =  [NSString stringWithFormat:@"http://108.166.65.150/rest/%@/device_id/%@", action, [[UIDevice currentDevice] uniqueDeviceIdentifier]];
    return strUrl;
}

- (void)postAction:(NSString*)strUrl {
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSLog(@"%@",strUrl);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection) {
        NSLog(@"Connecting...");
    } else {
        NSLog(@"Error");
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSLog(@"didReceiveData (%lu bytes)", (unsigned long)data.length);
    NSRange textRange;
    textRange =[response rangeOfString:@"text/html"];
    
    if(textRange.location != NSNotFound) {
        response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    } else { // application/octet-stream
        self.requestLog = [RequestLog parseFromData:(data)];
        [self.tableView reloadData];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"Finished loading: %@",response);
    connection = nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)newResponse {
    NSLog(@"didReceiveResponse...%@", newResponse.MIMEType);
    response = newResponse.MIMEType;
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"%@",[error localizedDescription]);
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                message:[error localizedDescription]
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"") 
                      otherButtonTitles:nil] show];
    
}

/////////////////////////////////////////////////////////////////////////////////////////////

@end
