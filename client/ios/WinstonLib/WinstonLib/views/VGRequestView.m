// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "Request.pb.h"
#import "UIImage+WinstonLib.h"

#import "VGRequestView.h"

@implementation VGRequestView

@synthesize requestEntry;
@synthesize dateFormatter;
@synthesize abbreviation;
@synthesize ackButton;
@synthesize closeButton;
@synthesize highlighted;
@synthesize editing;


- (id)initWithFrame:(CGRect)frame {
	
	if (self = [super initWithFrame:frame]) {
		
		/*
		 Cache the formatter. Normally you would use one of the date formatter styles (such as NSDateFormatterShortStyle), but here we want a specific format that excludes seconds.
		 */
		dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"h:mm a"];
		self.opaque = YES;
		self.backgroundColor = [UIColor blackColor];
        ackButton = [UIButton buttonWithType:UIButtonTypeCustom];
        closeButton = [UIButton buttonWithType:UIButtonTypeCustom];

	}
	return self;
}

- (void)setRequestEntry:(RequestEntry *)newRequestEntry {
	
	// If the time zone wrapper changes, update the date formatter and abbreviation string.
	if (requestEntry != newRequestEntry) {
        requestEntry = newRequestEntry;
        /*
		[requestEntry release];
		requestEntry = [newTimeZoneWrapper retain];
		
		[dateFormatter setTimeZone:timeZoneWrapper.timeZone];
		
		NSString *string = [[NSString alloc] initWithFormat:@"%@ (%@)", timeZoneWrapper.abbreviation, timeZoneWrapper.gmtOffset];
		self.abbreviation = string;
		[string release];
         */
	}
	// May be the same wrapper, but the date may have changed, so mark for redisplay.
	[self setNeedsDisplay];
}


- (void)setHighlighted:(BOOL)lit {
	// If highlighted state changes, need to redisplay.
	if (highlighted != lit) {
		highlighted = lit;	
		[self setNeedsDisplay];
	}
}


- (void)drawRect:(CGRect)rect {
	
#define LEFT_COLUMN_OFFSET 10
#define LEFT_COLUMN_WIDTH 40
	
#define MIDDLE_COLUMN_OFFSET 50
#define MIDDLE_COLUMN_WIDTH 180

#define RIGHT_COLUMN_OFFSET 220
#define RIGHT_COLUMN_WIDTH 100
	
#define UPPER_ROW_TOP 5
#define MIDDLE_ROW_TOP 23
#define LOWER_ROW_TOP 38
	
#define MAIN_FONT_SIZE 16
#define MIN_MAIN_FONT_SIZE 14
#define SECONDARY_FONT_SIZE 11
#define MIN_SECONDARY_FONT_SIZE 9

	// Color and font for the main text items (time zone name, time)
	UIColor *mainTextColor = nil;
//	UIFont *mainFont = [UIFont systemFontOfSize:MAIN_FONT_SIZE];

	// Color and font for the secondary text items (GMT offset, day)
	UIColor *secondaryTextColor = nil;
//	UIFont *secondaryFont = [UIFont systemFontOfSize:SECONDARY_FONT_SIZE];
	
	// Choose font color based on highlighted state.
	if (self.highlighted) {
		mainTextColor = [UIColor whiteColor];
		secondaryTextColor = [UIColor whiteColor];
	}
	else {
		mainTextColor = [UIColor whiteColor];
		secondaryTextColor = [UIColor lightGrayColor];
		self.backgroundColor = [UIColor blackColor];
	}
	
	CGRect contentRect = self.bounds;
	
	// In this example we will never be editing, but this illustrates the appropriate pattern.
    if (!self.editing) {
		
		CGFloat boundsX = contentRect.origin.x;
		CGPoint point;
		
		//CGFloat actualFontSize;
		//CGSize size;

        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"hh:mm"];
        
        // Set the color for the main text items.
		[mainTextColor set];
		
        // Draw the icon top left;
        point = CGPointMake(boundsX + LEFT_COLUMN_OFFSET, UPPER_ROW_TOP);
        [[UIImage winstonLibImageNamed:@"ic_page.png"] drawAtPoint:point];

        // Draw the requestor's name top left; use the NSString UIKit method to scale the font size down if the text does not fit in the given area
        point = CGPointMake(boundsX + MIDDLE_COLUMN_OFFSET, UPPER_ROW_TOP);
        NSString *text = [NSString stringWithFormat:@"%@ %@", [dateFormat stringFromDate:[NSDate dateWithTimeIntervalSince1970:requestEntry.reqTs]] , requestEntry.requestor];

//FIXME		[text drawAtPoint:point forWidth:MIDDLE_COLUMN_WIDTH withFont:mainFont minFontSize:MIN_MAIN_FONT_SIZE actualFontSize:NULL lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
        
		// Set the color for the secondary text items.
		[secondaryTextColor set];

		// Draw the details bottom left; use the NSString UIKit method to scale the font size down if the text does not fit in the given area.
		point = CGPointMake(boundsX + MIDDLE_COLUMN_OFFSET, MIDDLE_ROW_TOP);
// FIXME		[requestEntry.request drawAtPoint:point forWidth:MIDDLE_COLUMN_WIDTH withFont:secondaryFont minFontSize:MIN_SECONDARY_FONT_SIZE actualFontSize:NULL lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
        
        point = CGPointMake(boundsX + MIDDLE_COLUMN_OFFSET, LOWER_ROW_TOP);
        [[UIImage winstonLibImageNamed:@"ic_ack.png"] drawAtPoint:point];
        if (requestEntry.ackTs != 0) {
            point.x += [UIImage winstonLibImageNamed:@"ic_ack.png"].size.width + 5.0;
            text = [NSString stringWithFormat:@"%@ %@", [dateFormat stringFromDate:[NSDate dateWithTimeIntervalSince1970:requestEntry.ackTs]] , requestEntry.ackName];
// FIXME            [text drawAtPoint:point forWidth:MIDDLE_COLUMN_WIDTH withFont:secondaryFont minFontSize:MIN_SECONDARY_FONT_SIZE actualFontSize:NULL lineBreakMode:NSLineBreakByTruncatingTail baselineAdjustment:UIBaselineAdjustmentAlignBaselines];
        }
        
        // draw buttons
        UIImage *btnImage = [UIImage winstonLibImageNamed:@"btn_ack_normal.png"];

        point = CGPointMake(contentRect.origin.x + contentRect.size.width - (2 * btnImage.size.width) - 5.0, UPPER_ROW_TOP);
        [ackButton setImage:btnImage forState:UIControlStateNormal];
        [ackButton setImage:[UIImage winstonLibImageNamed:@"btn_ack_pressed.png"] forState:UIControlStateHighlighted];
        [ackButton setFrame:CGRectMake(point.x, point.y, btnImage.size.width, btnImage.size.height)];
        [self addSubview:ackButton];        

        point.x += btnImage.size.width + 5.0;
        [closeButton setImage:[UIImage winstonLibImageNamed:@"btn_close_normal.png"] forState:UIControlStateNormal];
        [closeButton setImage:[UIImage winstonLibImageNamed:@"btn_close_pressed.png"] forState:UIControlStateHighlighted];
        [closeButton setFrame:CGRectMake(point.x, point.y, btnImage.size.width, btnImage.size.height)];
        [self addSubview:closeButton];        
                
	}

}


- (void)dealloc {
	requestEntry = nil;
	dateFormatter = nil;
	abbreviation = nil;
    ackButton = nil;
    closeButton = nil;
    //[super dealloc];
}


@end
