// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>
#import "Request.pb.h"

@interface VGRequestsViewController : UITableViewController {
	RequestLog *requestLog;
}
@property (nonatomic, retain) NSString *response;

@end
