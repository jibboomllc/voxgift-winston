// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>
#import "Request.pb.h"

@interface VGRequestView : UIView {
	RequestEntry *requestEntry;
	NSDateFormatter *dateFormatter;
	NSString *abbreviation;
	UIButton *ackButton;
    UIButton *closeButton;
    BOOL highlighted;
	BOOL editing;
}

@property (nonatomic, retain) RequestEntry *requestEntry;
@property (nonatomic, retain) NSDateFormatter *dateFormatter;
@property (nonatomic, retain) NSString *abbreviation;
@property (nonatomic, retain) UIButton *ackButton;
@property (nonatomic, retain) UIButton *closeButton;
@property (nonatomic, getter=isHighlighted) BOOL highlighted;
@property (nonatomic, getter=isEditing) BOOL editing;

@end
