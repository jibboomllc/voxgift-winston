// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <AVFoundation/AVFoundation.h>
#import "UIDevice+Orientation.h"
#import "UIViewController+MaxFrame.h"
#import "VGAppDelegate.h"
#import "VGConfCardsViewController.h"
#import "VGConfFontViewController.h"
#import "VGConfCustomEntriesViewController.h"
#import "VGConfOutputLanguageViewController.h"
#import "VGConfRoleViewController.h"
#import "VGConfSettingViewController.h"
#import "VGConfigurationViewController.h"

@implementation VGConfigurationViewController
@synthesize dictionaryOptions, ioOptions, theTableView;


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];

    [self buildView];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [theTableView reloadData];
    [self layoutView];
}

/////////////////////////////////////////////////////////////////////////////////////////////

- (void)buildView
{    
    [super buildView];
    
    CGRect screenRect = [self maxUsableFrame];
    theTableView = [[UITableView alloc] initWithFrame:CGRectMake(screenRect.origin.x, screenRect.origin.y, screenRect.size.width, screenRect.size.height) style:UITableViewStyleGrouped];
    
    theTableView.delegate = self;
    theTableView.dataSource = self;
    theTableView.backgroundColor = [UIColor clearColor];
   [self.view addSubview:theTableView];
    
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.title = [[appDelegate dictStrings] valueForKey:@"settings"];
    
    dictionaryOptions = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[[appDelegate dictStrings] valueForKey:@"role"], [[appDelegate dictStrings] valueForKey:@"setting"], [[appDelegate dictStrings] valueForKey:@"custom"], nil] forKeys:[NSArray arrayWithObjects:@"prefRole", @"prefSetting", @"prefCustomEntries", nil]];
    ioOptions = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[[appDelegate dictStrings] valueForKey:@"cards_shown"], [[appDelegate dictStrings] valueForKey:@"cards_text_size"], [[appDelegate dictStrings] valueForKey:@"type_text_size"], [[appDelegate dictStrings] valueForKey:@"output_language"], [[appDelegate dictStrings] valueForKey:@"speech_rate"], nil] forKeys:[NSArray arrayWithObjects:@"prefNumberOfCards", @"prefCardsTextSize", @"prefTypeTextSize", @"prefOutputLanguageLocale", @"prefSpeechRate", nil]];
    
}

- (void)layoutView
{
    [super layoutView];

    theTableView.frame =[self maxUsableFrame];
}

/////////////////////////////////////////////////////////////////////////////////////////////

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return [dictionaryOptions count];
    } else {
        return [ioOptions count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"DisclosureCell";
    if (indexPath.section == 1 && indexPath.row == 4) {
        cellIdentifier = @"SliderCell";
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        if ([cellIdentifier isEqual: @"DisclosureCell"]) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        } else {
            UISlider *slider = [[UISlider alloc] initWithFrame:CGRectMake(cell.contentView.bounds.size.width * 0.5, 0, cell.contentView.bounds.size.width * 0.5 - 10, cell.contentView.bounds.size.height)];
            slider.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
            slider.userInteractionEnabled = YES;
            slider.tag = 1;
            [cell.contentView addSubview:slider];
        }
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.textLabel.text = [dictionaryOptions objectForKey:@"prefRole"];
            int index = (int)[[[appDelegate dictStringArrays] valueForKey:@"role_values"] indexOfObject:[defaults objectForKey:@"prefRole"]];
            cell.detailTextLabel.text = [appDelegate checkString:[[[appDelegate dictStringArrays] valueForKey:@"roles"] objectAtIndex:index]];
        } else if (indexPath.row == 1) {
            cell.textLabel.text = [dictionaryOptions objectForKey:@"prefSetting"];
            int index = (int)[[[appDelegate dictStringArrays] valueForKey:@"setting_values"] indexOfObject:[defaults objectForKey:@"prefSetting"]];
            cell.detailTextLabel.text = [appDelegate checkString:[[[appDelegate dictStringArrays] valueForKey:@"settings"] objectAtIndex:index]];
        } else {
            cell.textLabel.text = [dictionaryOptions objectForKey:@"prefCustomEntries"];
            cell.detailTextLabel.text = [[appDelegate dictStrings] valueForKey:@"custom_summary"];
        }
    } else {
        if (indexPath.row == 0) {
            cell.textLabel.text = [ioOptions objectForKey:@"prefNumberOfCards"];
            cell.detailTextLabel.text = [defaults objectForKey:@"prefNumberOfCards"];
        } else if (indexPath.row == 1) {
            cell.textLabel.text = [ioOptions objectForKey:@"prefCardsTextSize"];
            cell.detailTextLabel.text = [[NSString alloc] initWithFormat: @"%@x",[defaults objectForKey:@"prefCardsTextSize"]];
        } else if (indexPath.row == 2) {
            cell.textLabel.text = [ioOptions objectForKey:@"prefTypeTextSize"];
            cell.detailTextLabel.text = [[NSString alloc] initWithFormat: @"%@x",[defaults objectForKey:@"prefTypeTextSize"]];
        } else if (indexPath.row == 3) {
            cell.textLabel.text = [ioOptions objectForKey:@"prefOutputLanguageLocale"];
            int index = (int)[[[appDelegate dictStringArrays] valueForKey:@"language_locale_values"] indexOfObject:[defaults objectForKey:@"prefOutputLanguageLocale"]];
            cell.detailTextLabel.text = [appDelegate checkString:[[[appDelegate dictStringArrays] valueForKey:@"languages"] objectAtIndex:index]];
        } else {
            cell.textLabel.text = [ioOptions objectForKey:@"prefSpeechRate"];
            UISlider *slider = (UISlider *)[cell viewWithTag:1];
            slider.minimumValue = AVSpeechUtteranceMinimumSpeechRate;
            slider.maximumValue = AVSpeechUtteranceMaximumSpeechRate;
            slider.value = [defaults floatForKey:@"prefSpeechRate"];
            [slider addTarget:self action:@selector(setSpeechRate:) forControlEvents:UIControlEventValueChanged];
            
        }
    }

    return cell;
}

-(void)setSpeechRate:(UISlider *)slider
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSString stringWithFormat:@"%f", slider.value] forKey:@"prefSpeechRate"];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //NSLog(@"You pressed : %@", [NSString stringWithFormat:[tableView cellForRowAtIndexPath:indexPath].textLabel.text ]  );
    VGConfViewController *configView;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            configView = [[VGConfRoleViewController alloc] init];
        } else if (indexPath.row == 1) {
            configView = [[VGConfSettingViewController alloc] init];
        } else {
            VGConfCustomEntriesViewController *configView2 = [[VGConfCustomEntriesViewController alloc] init];
            [self.navigationController pushViewController:configView2 animated:TRUE];
            return;
        }
    } else {
        if (indexPath.row == 0) {
            configView = [[VGConfCardsViewController alloc] init];
        } else if (indexPath.row == 1) {
            configView = [[VGConfFontViewController alloc] init];
            VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
            [configView setTitle:[[appDelegate dictStrings] valueForKey:@"cards_text_size"]];
            [configView setPrefKey:@"prefCardsTextSize"];
        } else if (indexPath.row == 2) {
            configView = [[VGConfFontViewController alloc] init];
            VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
            [configView setTitle:[[appDelegate dictStrings] valueForKey:@"type_text_size"]];
            [configView setPrefKey:@"prefTypeTextSize"];
        } else if (indexPath.row == 3) {
            configView = [[VGConfOutputLanguageViewController alloc] init];
        } else {
        }
    }
    [self.navigationController pushViewController:configView animated:TRUE];
}

/////////////////////////////////////////////////////////////////////////////////////////////

@end
