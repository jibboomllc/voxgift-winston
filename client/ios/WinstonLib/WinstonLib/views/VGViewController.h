// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface VGViewController : GAITrackedViewController {
}    
- (void) buildView;
- (void) layoutView;

@property (strong) UIImageView *appBackground;

@end
