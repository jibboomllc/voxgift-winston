// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "UIViewController+MaxFrame.h"
#import "UIImage+WinstonLib.h"
#import "VGViewController.h"

@implementation VGViewController
@synthesize appBackground;

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self buildView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self layoutView];
}

- (void)viewDidAppear:(BOOL)animated {
    self.screenName = NSStringFromClass(self.class);
    [super viewDidAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration 
{
    [self layoutView];
}


/////////////////////////////////////////////////////////////////////////////////////////////

- (void)buildView
{
    self.appBackground = [[UIImageView alloc] initWithImage:[UIImage winstonLibImageNamed:@"background.jpg"]];
    [self.view addSubview:self.appBackground];
    
}

-(void) layoutView 
{
    CGRect screenRect = [self maxUsableFrame];

    self.appBackground.frame = CGRectMake((screenRect.size.width - self.appBackground.image.size.width) / 2.0, (screenRect.size.height - self.appBackground.image.size.height) / 2.0, self.appBackground.image.size.width, self.appBackground.image.size.height);

}
@end

