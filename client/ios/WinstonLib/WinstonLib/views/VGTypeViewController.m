// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

//  http://stackoverflow.com/questions/8869445/how-can-i-use-mqtt-in-ios


#import <AVFoundation/AVFoundation.h>
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "UIDevice+Identifier.h"
#import "UIDevice+Orientation.h"
#import "UIPlaceHolderTextView.h"
#import "UIImage+WinstonLib.h"
#import "UIViewController+MaxFrame.h"

#import "VGAppDelegate.h"
#import "VGTypeViewController.h"
#import "VGCardsViewController.h"
#import "VGConfigurationViewController.h"

@interface VGTypeViewController ()

@end

@implementation VGTypeViewController
@synthesize inputText;
@synthesize viewButtons;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view = [[UIView alloc] init];
    
    [self buildView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [self.inputText setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]*[defaults floatForKey:@"prefTypeTextSize"]]];
    
    //NSLog(@"viewWillAppear %f", [defaults floatForKey:@"prefTypeTextSize"]);
}

/////////////////////////////////////////////////////////////////////////////////////////

- (void)buildView
{    
    [super buildView];
    
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [[appDelegate dictStrings] valueForKey:@"type"];
    
    self.inputText = [[UIPlaceHolderTextView alloc] init];
    self.inputText.backgroundColor = [UIColor whiteColor];
    self.inputText.textColor = [UIColor blackColor];
    [self.inputText.layer setBorderColor:[UIColor grayColor].CGColor];
    [self.inputText.layer setBorderWidth:1.5f];
    [self.inputText.layer setCornerRadius:10.0f];
     
    self.inputText.delegate = self;
    self.inputText.placeholder = [[appDelegate dictStrings] valueForKey:@"type_hint"];
    [self.view addSubview:self.inputText];

    // create buttons
    self.viewButtons = [[NSMutableArray alloc] init];    
    
    UIButton *talkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [talkButton addTarget:self action:@selector(speak:) forControlEvents:UIControlEventTouchUpInside];
    //[talkButton setTitle:[[appDelegate dictStrings] valueForKey:@"talk"] forState:UIControlStateNormal];
    [talkButton setImage:[UIImage winstonLibImageNamed:@"ic_talk.png"] forState:UIControlStateNormal];
    
    [self.viewButtons addObject: talkButton];
    
    /** Winston Pro feature
    // FIXME should be build option

    UIButton *pageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [pageButton addTarget:self action:@selector(page:) forControlEvents:UIControlEventTouchUpInside];
    [pageButton setTitle:[[appDelegate dictStrings] valueForKey:@"page"] forState:UIControlStateNormal];
    [pageButton setImage:[UIImage winstonLibImageNamed:@"ic_page.png"] forState:UIControlStateNormal];
    
    [self.viewButtons addObject: pageButton];            
    **/
    
    UIButton *cardsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cardsButton addTarget:self action:@selector(cards:) forControlEvents:UIControlEventTouchUpInside];
    [cardsButton setImage:[UIImage winstonLibImageNamed:@"ic_card.png"] forState:UIControlStateNormal];
    [self.viewButtons addObject: cardsButton];
    
    UIButton *configurationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [configurationButton addTarget:self action:@selector(configuration:) forControlEvents:UIControlEventTouchUpInside];
    [configurationButton setImage:[UIImage winstonLibImageNamed:@"ic_settings.png"] forState:UIControlStateNormal];
    [self.viewButtons addObject: configurationButton];
    
    // update buttons and add to view
    for (UIButton *button in self.viewButtons) {
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        
        [button setBackgroundColor:[UIColor whiteColor]];
        [button.layer setBorderColor:[UIColor grayColor].CGColor];
        [button.layer setBorderWidth:1.5f];
        [button.layer setCornerRadius:10.0f];
        
        [self.view addSubview:button];
    }

}

- (void)layoutView {
    [super layoutView];
    
    CGRect screenRect = [self maxUsableFrame];
    
    // set drawing rectangles
    CGRect inputRect = screenRect;
    inputRect.origin.y += 10.0;
    inputRect.origin.x += 10.0;
    inputRect.size.height -= 20.0;
    inputRect.size.width -= 20.0;
    
    CGRect buttonsRect = screenRect;
    CGFloat buttonWidth = 50.0;
    buttonsRect.size.width = buttonWidth + 10.0;
    inputRect.size.width -= buttonsRect.size.width;
    buttonsRect.origin.x = inputRect.origin.x + inputRect.size.width;
    
    self.inputText.frame = inputRect;
    
    // place buttons
    CGFloat buttonHeight = 50.0;//((buttonsRect.size.height - 10.0) / self.viewButtons.count) - 10.0;
    buttonsRect.origin.y += 10.0;
    buttonsRect.origin.x += 10.0;
    buttonsRect.size.height -= 20.0;
    
    int i = 0;
    for (UIButton *button in self.viewButtons) {
        button.frame = CGRectMake(buttonsRect.origin.x, buttonsRect.origin.y + (i * (buttonHeight + 10.0)), buttonWidth, buttonHeight);
        
        //CGSize sizeLabel = [[button titleForState:UIControlStateNormal] sizeWithFont:button.titleLabel.font];
        NSDictionary *attributes = @{ NSFontAttributeName: button.titleLabel.font};
        CGSize sizeLabel = [[button titleForState:UIControlStateNormal] sizeWithAttributes:attributes];
        
        CGFloat imageEdgeInsetX = -1 * (buttonWidth - sizeLabel.width - (button.imageView.image.size.width) - 15.0);
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, imageEdgeInsetX, 0, 0)];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        i++;
    }
}

- (void)layoutViewOld {
    [super layoutView];

    CGRect screenRect = [self maxUsableFrame];
    
    // set drawing rectangles
    CGRect inputRect = screenRect;
    inputRect.origin.y += 10.0;
    inputRect.origin.x += 10.0;
    inputRect.size.height -= 20.0;
    inputRect.size.width -= 20.0;

    CGRect buttonsRect = screenRect;
    CGFloat buttonHeight = 40.0;
    buttonsRect.size.height = buttonHeight + 20.0;
    inputRect.size.height -= buttonsRect.size.height;
    buttonsRect.origin.y = inputRect.origin.y + inputRect.size.height;
    
    self.inputText.frame = inputRect;

    // place buttons
    CGFloat buttonWidth = ((buttonsRect.size.width - 10.0) / self.viewButtons.count) - 10.0;
    buttonsRect.origin.y += 10.0;
    buttonsRect.origin.x += 10.0;
    buttonsRect.size.width -= 20.0;
  
    int i = 0;
    for (UIButton *button in self.viewButtons) {
        button.frame = CGRectMake(buttonsRect.origin.x + (i * (buttonWidth + 10.0)), buttonsRect.origin.y, buttonWidth, buttonHeight);

        //CGSize sizeLabel = [[button titleForState:UIControlStateNormal] sizeWithFont:button.titleLabel.font];
        NSDictionary *attributes = @{ NSFontAttributeName: button.titleLabel.font};
        CGSize sizeLabel = [[button titleForState:UIControlStateNormal] sizeWithAttributes:attributes];
        
        CGFloat imageEdgeInsetX = -1 * (buttonWidth - sizeLabel.width - (button.imageView.image.size.width) - 15.0);
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, imageEdgeInsetX, 0, 0)];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        i++;
    }
}

- (void)speak:(UIControlEvents *) sender {
    //NSLog(@"Talk %@", self.inputText.text);
    [self speakMsg: self.inputText.text];
    
    NSMutableDictionary *event =
    [[GAIDictionaryBuilder createEventWithCategory:@"Type"
                                            action:@"Speak"
                                             label:self.inputText.text
                                             value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
}


- (void)page:(UIControlEvents *) sender {
    //NSLog(@"Page %@", self.inputText.text);
    [self pageMsg: self.inputText.text];
    NSMutableDictionary *event =
    [[GAIDictionaryBuilder createEventWithCategory:@"Type"
                                            action:@"Page"
                                             label:self.inputText.text
                                             value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
}

- (void)cards:(UIControlEvents *) sender {
    UIViewController *viewController = [[VGCardsViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:TRUE];

    NSMutableDictionary *event =
    [[GAIDictionaryBuilder createEventWithCategory:@"Type"
                                            action:@"Cards"
                                             label:nil
                                             value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];

}

- (void)configuration:(UIControlEvents *) sender {
    UIViewController *viewController = [[VGConfigurationViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:TRUE];
    
    NSMutableDictionary *event =
    [[GAIDictionaryBuilder createEventWithCategory:@"Type"
                                            action:@"Configuration"
                                             label:nil
                                             value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

@end
