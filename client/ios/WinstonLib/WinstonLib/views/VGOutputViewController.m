// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

//  http://stackoverflow.com/questions/8869445/how-can-i-use-mqtt-in-ios


#import <AVFoundation/AVFoundation.h>
#import "NSString+URLEncode.h"
#import "UIDevice+Identifier.h"
#import "UIDevice+Orientation.h"
#import "UIViewController+MaxFrame.h"

#import "iToast.h"

#import "VGAppDelegate.h"
#import "VGOutputViewController.h"

@interface VGOutputViewController ()

@end

@implementation VGOutputViewController
@synthesize response;
@synthesize pageAudioPlayer;
@synthesize speakSpeechSynthesizer;
- (void)viewDidLoad
{
    [super viewDidLoad];
 
    NSBundle *libResourceBundle = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"WinstonLibRes" withExtension:@"bundle"]];

    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/res/raw/page.mp3", [libResourceBundle resourcePath]]];
    NSError *err;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&err];

    NSData *songFile = [[NSData alloc] initWithContentsOfURL:url options:NSDataReadingMappedIfSafe error:&err ];
    pageAudioPlayer = [[AVAudioPlayer alloc] initWithData:songFile error:&err];
    if (pageAudioPlayer == nil) {
        NSLog(@"%@",[err description]);
    } else {        
        [pageAudioPlayer prepareToPlay];
        [pageAudioPlayer setVolume:50.0];
    }

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidUnload {
    [self setResponse:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

//////////////////////////////////////////////////////////////////////////////////////

- (void)speakMsg:(NSString *)strSpeak {
    for (AVSpeechSynthesisVoice *voice in [AVSpeechSynthesisVoice speechVoices]) {
        NSLog(@"%@ %@", voice.language, voice.name);
    }
    
    NSString *txt = [[NSString stringWithFormat:@"%@", strSpeak] stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    //NSLog(@"%@",txt);
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSSet *set = [[appDelegate inputDictionary] keysOfEntriesPassingTest:^(id key, id value, BOOL *stop)
                  {
                      //NSLog(@"[%@]?=[%@]",txt,value);
                      if ([txt isEqualToString:value])
                          return YES;
                      else
                          return NO;
                  }];
    
    if (set != nil && set.count > 0){
            txt = [[appDelegate outputDictionary] valueForKey:set.anyObject];
    }
    
    if (txt != nil) {
        //[engine speak:txt];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        AVSpeechSynthesizer *synthesizer = [[AVSpeechSynthesizer alloc]init];
        AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:txt];
        [utterance setRate: [defaults floatForKey:@"prefSpeechRate"]];
        [utterance setVoice:[AVSpeechSynthesisVoice voiceWithLanguage:[defaults stringForKey:@"prefOutputLanguageLocale"]]];
        
        [synthesizer speakUtterance:utterance];
        
        [[[iToast makeText: [NSString stringWithFormat:@"Speak: %@", txt]]setGravity:iToastGravityBottom] show];
    }
}
//////////////////////////////////////////////////////////////////////////////////////

- (void)pageMsg:(NSString *)strPage {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSString *strUrl =  [NSString stringWithFormat:@"http://%@/rest/request/req_device_id/%@/request/%@", [defaults stringForKey:@"prefIpAddress"], [[UIDevice currentDevice] uniqueDeviceIdentifier]
                         , strPage];
    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSLog(@"%@",strUrl);
    
    //  NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrl]];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:15.0];
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if (connection) {
        NSLog(@"Connecting...");
    } else {
        NSLog(@"Error");
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSLog(@"didReceiveData...");
    response = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSRange textRange;
    textRange =[response rangeOfString:@"submitted"];
    
    if(textRange.location != NSNotFound) {
        NSLog(@"%@",response);
        if (pageAudioPlayer != nil) {
            [pageAudioPlayer play];
        }

    } else {
        NSLog(@"%@",response);
    }
    connection = nil;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"didReceiveResponse...");
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"%@",[error localizedDescription]);
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"")
                                message:[error localizedDescription]
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", @"") 
                      otherButtonTitles:nil] show];
    
}


/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

@end
