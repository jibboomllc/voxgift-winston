// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "VGAppDelegate.h"
#import "VGConfRoleViewController.h"

@implementation VGConfRoleViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [[appDelegate dictStrings] valueForKey:@"role"];
    
    self.keys = [[appDelegate dictStringArrays] valueForKey:@"role_values"];
    self.options = [[appDelegate dictStringArrays] valueForKey:@"roles"];
}

- (void)buildCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //cell.textLabel.text = [self.options objectAtIndex:indexPath.row];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([[self.keys objectAtIndex:indexPath.row] isEqual: [defaults objectForKey:@"prefRole"]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[self.keys objectAtIndex:indexPath.row] forKey:@"prefRole"];
    [tableView reloadData];
    
    //NSLog(@"You pressed : %@", [NSString stringWithFormat:[tableView cellForRowAtIndexPath:indexPath].textLabel.text ]  );
}

/////////////////////////////////////////////////////////////////////////////////////////////

@end
