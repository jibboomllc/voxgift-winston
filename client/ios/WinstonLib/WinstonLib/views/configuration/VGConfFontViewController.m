// Copyright 2011-2014 Voxgift, Inc.
// All rights reserved.

//#import "VGAppDelegate.h"
#import "VGConfFontViewController.h"

@implementation VGConfFontViewController


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.options = [NSMutableArray arrayWithObjects:@"0.50", @"1.0", @"1.5", @"2.0", @"2.5", @"3.5", @"4.0", @"4.5", @"5.0", nil];
}

- (void)buildCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.textLabel.text = [[NSString alloc] initWithFormat: @"%@x",[self.options objectAtIndex:indexPath.row]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
    if ([[self.options objectAtIndex:indexPath.row] isEqual: [defaults objectForKey:self.prefKey]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[self.options objectAtIndex:indexPath.row] forKey:self.prefKey];
    [tableView reloadData];
    
    //NSLog(@"You pressed : %@", [NSString stringWithFormat:[tableView cellForRowAtIndexPath:indexPath].textLabel.text ]  );
}

/////////////////////////////////////////////////////////////////////////////////////////////

@end
