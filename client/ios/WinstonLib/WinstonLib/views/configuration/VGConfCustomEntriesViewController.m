// Copyright 2011-2013 Voxgift, Inc.
// All rights reserved.

#import "VGAppDelegate.h"
#import "VGConfCustomEntriesViewController.h"

@interface VGConfCustomEntriesViewController ()

@end

@implementation VGConfCustomEntriesViewController

@synthesize options;
@synthesize inputNavigationController;
@synthesize itemInputController;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.clearsSelectionOnViewWillAppear = NO;
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [[appDelegate dictStrings] valueForKey:@"custom"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.options = [NSMutableArray arrayWithArray:[defaults arrayForKey:@"prefCustomEntries"]];
    
    self.itemInputController = [[VGItemInputViewController alloc] init];
    self.itemInputController.modalPresentationStyle = UIModalPresentationFormSheet;
    self.itemInputController.delegate = self;
    
    self.inputNavigationController = [[UINavigationController alloc] initWithRootViewController:self.itemInputController];
    
    self.tableView.allowsSelectionDuringEditing = YES;
    //[self.tableView setEditing:YES animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addItem:sender
{
    [self.itemInputController setIndexPath:Nil];
    [self presentViewController:self.inputNavigationController animated:YES completion:Nil];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    if(editing == YES)
    {
        UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addItem:)];
        self.navigationItem.leftBarButtonItem = addButton;
    } else {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:self.options forKey:@"prefCustomEntries"];
        
        self.navigationItem.leftBarButtonItem = nil;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.options.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // static NSString *CellIdentifier = @"Cell";
    // UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    cell.textLabel.text = [appDelegate checkString:[options objectAtIndex:indexPath.row]];
    
    return cell;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [self.options removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        [self.itemInputController setIndexPath:Nil];
        [self presentViewController:self.itemInputController animated:YES completion:Nil];
    }
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    if (toIndexPath.row > fromIndexPath.row) {
        [self.options insertObject:[self.options objectAtIndex:fromIndexPath.row] atIndex:toIndexPath.row + 1];
        [self.options removeObjectAtIndex:fromIndexPath.row];
    } else {
        [self.options insertObject:[self.options objectAtIndex:fromIndexPath.row] atIndex:toIndexPath.row];
        [self.options removeObjectAtIndex:fromIndexPath.row + 1];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
     UIAlertView *messageAlert = [[UIAlertView alloc]
     initWithTitle:@"Row Selected" message:@"You've selected a row" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
     
     // Display Alert Message
     [messageAlert show];
     */
    if(self.editing) {
        [self.itemInputController setIndexPath:indexPath];
        [self presentViewController:self.inputNavigationController animated:YES completion:Nil];
    }
}

#pragma mark - VGItemInputViewController delegate

- (void)didDismissItemViewControllerAtIndexPath:(NSIndexPath *) indexPath withItem:(NSString*) item
{
    // NSLog(@"%@", self.presentedViewController);
    if (![self.presentedViewController isBeingDismissed]) {
        [self dismissViewControllerAnimated:YES completion:Nil];
        
        if (item != Nil) {
            if (indexPath == Nil) {
                [self.options addObject:item];
                NSIndexPath *oNewIndexPath = [NSIndexPath indexPathForItem:[options count] - 1 inSection:0];
                [self.tableView beginUpdates];
                [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:oNewIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [self.tableView endUpdates];
            } else {
                [self.options replaceObjectAtIndex:indexPath.row withObject:item];
                [self.tableView reloadData];
            }
        }
    }
}

- (NSString*)getItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath != Nil) {
        return [self.options objectAtIndex:indexPath.row];
    }
    return Nil;
}

@end
