// Copyright 2011-2013 Voxgift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>
#import "VGItemInputViewController.h"

@interface VGConfCustomEntriesViewController : UITableViewController <VGItemViewControllerDelegate>
{
    NSMutableArray *options;
    UINavigationController *inputNavigationController;
    VGItemInputViewController *itemInputController;
}
@property (nonatomic, retain) NSMutableArray *options;
@property (nonatomic, retain) UINavigationController *inputNavigationController;
@property (nonatomic, retain) VGItemInputViewController *itemInputController;

@end
