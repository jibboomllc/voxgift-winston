// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "VGAppDelegate.h"
#import "VGConfOutputLanguageViewController.h"

@implementation VGConfOutputLanguageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [[appDelegate dictStrings] valueForKey:@"output_language"];
    
    self.keys = [[appDelegate dictStringArrays] valueForKey:@"language_locale_values"];
    self.options = [[appDelegate dictStringArrays] valueForKey:@"languages"];
}

- (void)buildCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //cell.textLabel.text = [self.options objectAtIndex:indexPath.row];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([[self.keys objectAtIndex:indexPath.row] isEqual: [defaults objectForKey:@"prefOutputLanguageLocale"]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate setOutputLanguage:[self.keys objectAtIndex:indexPath.row ]];

    
    [tableView reloadData];
    
    //NSLog(@"You pressed : %@", [NSString stringWithFormat:[tableView cellForRowAtIndexPath:indexPath].textLabel.text ]  );
}

/////////////////////////////////////////////////////////////////////////////////////////////

@end
