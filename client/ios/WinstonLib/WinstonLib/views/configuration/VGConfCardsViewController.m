// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "VGAppDelegate.h"
#import "VGConfCardsViewController.h"

@implementation VGConfCardsViewController


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];

    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [[appDelegate dictStrings] valueForKey:@"cards_shown"];
    
    self.options = [NSMutableArray arrayWithObjects:@"1", @"2", @"3", @"4", @"6", @"8", @"10", @"12", nil];
}

- (void)buildCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.textLabel.text = [self.options objectAtIndex:indexPath.row];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
    if ([[self.options objectAtIndex:indexPath.row] isEqual: [defaults objectForKey:@"prefNumberOfCards"]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[self.options objectAtIndex:indexPath.row] forKey:@"prefNumberOfCards"];
    [tableView reloadData];
    
    //NSLog(@"You pressed : %@", [NSString stringWithFormat:[tableView cellForRowAtIndexPath:indexPath].textLabel.text ]  );
}

/////////////////////////////////////////////////////////////////////////////////////////////

@end
