// Copyright 2011-2013 Voxgift, Inc.
// All rights reserved.

#import "VGAppDelegate.h"
#import "VGItemInputViewController.h"

@interface VGItemInputViewController ()

@end

@implementation VGItemInputViewController

@synthesize textField;
@synthesize indexPath;

/////////////////////////////////////////////////////////////////////////////////////////

- (void)buildView
{
    [super buildView];

    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [[appDelegate dictStrings] valueForKey:@"custom"];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveAction)];
    self.navigationItem.rightBarButtonItem = saveButton;

    CGRect rect = CGRectMake(0, 60.0, self.view.bounds.size.width, 40.0);
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(10, 10, 0, 10);
    CGRect rFrame = UIEdgeInsetsInsetRect(rect, contentInsets);
    self.textField = [[UITextField alloc] initWithFrame:rFrame];
    self.textField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.textField.borderStyle = UITextBorderStyleLine;
    self.textField.backgroundColor = [UIColor whiteColor];
    self.textField.placeholder = [[appDelegate dictStrings] valueForKey:@"custom_placeholder"];
    self.textField.delegate = self;
    [self.textField setReturnKeyType:UIReturnKeyDone];
    
    [self.view addSubview:self.textField];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.indexPath != Nil) {
        self.textField.text = [self.delegate getItemAtIndexPath:self.indexPath];
    }
    [self.textField becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) cancelAction
{
    [self.delegate didDismissItemViewControllerAtIndexPath:Nil withItem: Nil];
}

- (void) saveAction
{
    [self.delegate didDismissItemViewControllerAtIndexPath:self.indexPath withItem:self.textField.text];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField*)textFieldIn
{
    [self.delegate didDismissItemViewControllerAtIndexPath:self.indexPath withItem:self.textField.text];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textFieldIn
{
    [self.delegate didDismissItemViewControllerAtIndexPath:self.indexPath withItem:self.textField.text];
    return YES;
}

@end
