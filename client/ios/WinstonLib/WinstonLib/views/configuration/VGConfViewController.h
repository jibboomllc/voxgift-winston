// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>
#import "VGViewController.h"

@interface VGConfViewController : VGViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) NSArray *keys;
@property (nonatomic, retain) NSMutableArray *options;
@property (nonatomic, retain) UITableView *theTableView;
@property (nonatomic, retain) NSString *prefKey;

@end
