//
//  VGItemInputViewController.h
//  WinstonLib
//
//  Created by Tom Giesberg on 11/24/13.
//
//

#import "VGViewController.h"

@protocol VGItemViewControllerDelegate <NSObject>

- (void)didDismissItemViewControllerAtIndexPath:(NSIndexPath *) indexPath withItem:(NSString*) item;
- (NSString*)getItemAtIndexPath:(NSIndexPath *) indexPath;

@end

@interface VGItemInputViewController : VGViewController<UITextFieldDelegate>

@property (nonatomic, retain) UITextField *textField;
@property (nonatomic, retain) NSIndexPath *indexPath;
@property (nonatomic, weak) id<VGItemViewControllerDelegate> delegate;

@end
