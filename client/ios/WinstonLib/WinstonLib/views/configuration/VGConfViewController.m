// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "UIDevice+Orientation.h"
#import "UIViewController+MaxFrame.h"
#import "VGAppDelegate.h"
#import "VGConfViewController.h"

@implementation VGConfViewController
@synthesize keys, options, theTableView, prefKey;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view = [[UIView alloc] init];
    [self buildView];
}


/////////////////////////////////////////////////////////////////////////////////////////

- (void)buildView
{    
    [super buildView];
    
    CGRect screenRect = [self maxUsableFrame];
    theTableView = [[UITableView alloc] initWithFrame:CGRectMake(screenRect.origin.x, screenRect.origin.y, screenRect.size.width, screenRect.size.height) style:UITableViewStyleGrouped];
    theTableView.backgroundColor = [UIColor clearColor];
    theTableView.delegate = self;
    theTableView.dataSource = self;
    [self.view addSubview:theTableView];
    
}

/////////////////////////////////////////////////////////////////////////////////////////////

- (void)layoutView
{
    [super layoutView];
    
    CGRect screenRect = [self maxUsableFrame];
    theTableView.frame = CGRectMake(screenRect.origin.x, screenRect.origin.y, screenRect.size.width, screenRect.size.height);
    
}

/////////////////////////////////////////////////////////////////////////////////////////////

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section
    return [options count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    cell.textLabel.text = [appDelegate checkString:[options objectAtIndex:indexPath.row]];

    [self buildCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (void)buildCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

/////////////////////////////////////////////////////////////////////////////////////////////

@end
