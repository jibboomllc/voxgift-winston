// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "VGAppDelegate.h"
#import "VGConfSettingViewController.h"

@implementation VGConfSettingViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [[appDelegate dictStrings] valueForKey:@"setting"];
    
    self.keys = [[appDelegate dictStringArrays] valueForKey:@"setting_values"];
    self.options = [[appDelegate dictStringArrays] valueForKey:@"settings"];
}

- (void)buildCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //cell.textLabel.text = [self.options objectAtIndex:indexPath.row];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if ([[self.keys objectAtIndex:indexPath.row] isEqual: [defaults objectForKey:@"prefSetting"]]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[self.keys objectAtIndex:indexPath.row] forKey:@"prefSetting"];
    [tableView reloadData];
    
    //NSLog(@"You pressed : %@", [NSString stringWithFormat:[tableView cellForRowAtIndexPath:indexPath].textLabel.text ]  );
}

/////////////////////////////////////////////////////////////////////////////////////////////

@end
