// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <AVFoundation/AVFoundation.h>
#import "UIDevice+Identifier.h"
#import "UIViewController+MaxFrame.h"

#import "VGAppDelegate.h"
#import "VGFeedbackViewController.h"

@implementation VGFeedbackViewController
@synthesize webView;
@synthesize activityIndicatorView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view = [[UIView alloc] init];
    
    [self buildView];

    // change the back button and add an event handler
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithTitle:[[appDelegate dictStrings] valueForKey:@"back"]
                                     style:UIBarButtonItemStyleBordered
                                    target:self
                                    action:@selector(handleBack:)];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [self teardownView];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strUrl =  [NSString stringWithFormat:@"http://%@/survey/input/device_id/%@/hw_manufacturer/Apple/hw_model/%@/os_version/%@/app_version_name/%@/app_package_name/%@/language_code/%@"
                         , [defaults stringForKey:@"prefIpAddress"]
                         , [[UIDevice currentDevice] uniqueDeviceIdentifier]
                         , [[UIDevice currentDevice] model]
                         , [[UIDevice currentDevice] systemVersion]
                         , [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleNameKey]
                         , [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey]
                         , [defaults stringForKey:@"prefInputLanguage"]];

    strUrl = [strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSLog(strUrl);
    NSURL *url = [[NSURL alloc] initWithString: strUrl];
    // skipping since probably not needed
    // [[NSURLCache sharedURLCache] removeCachedResponseForRequest:[[NSURLRequest alloc] initWithURL: url]];
    [self.webView loadRequest:[[NSURLRequest alloc] initWithURL: url]]; 
    [self layoutView];
}

/////////////////////////////////////////////////////////////////////////////////////////

- (void)buildView
{    
    [super buildView];
    
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [[appDelegate dictStrings] valueForKey:@"feedback"];
    
    self.webView = [[UIWebView alloc] init];
    self.webView.backgroundColor = [UIColor clearColor];
    [self.webView.layer setBorderColor:[UIColor grayColor].CGColor];
    [self.webView.layer setBorderWidth:1.5f];
    [self.webView.layer setCornerRadius:10.0f];
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
    
    self.activityIndicatorView = [[UIActivityIndicatorView alloc] init];
    [self.view addSubview:self.activityIndicatorView];

}

- (void)teardownView
{    
    //[super teardownView];
    self.activityIndicatorView = nil;   
    self.webView = nil;
    self.view = nil;
}

- (void)layoutView 
{
    [super layoutView];

    CGRect inputRect = [self maxUsableFrame];
    inputRect.origin.y += 10.0;
    inputRect.origin.x += 10.0;
    inputRect.size.height -= 20.0;
    inputRect.size.width -= 20.0;
    self.webView.frame = inputRect;

    inputRect = [UIScreen mainScreen].applicationFrame;
    inputRect.origin.y = 0.0;
    inputRect.origin.x -= 20.0;
    inputRect.size.height = 0.0;
    inputRect.size.width = 0.0;
    self.activityIndicatorView.frame = inputRect;
    
    // skipping since including it requires that you exit the app to restart
    // [self.webView reload]; 
}

/////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request            
 navigationType:(UIWebViewNavigationType)navigationType {
    //NSLog(@"%@",[[request URL] lastPathComponent]);
    if ([[[request URL] lastPathComponent] isEqual: @"cancel"]
      || [[[request URL] lastPathComponent] isEqual: @"close"]) {
        //do close window magic here!!
        
        [self.navigationController popViewControllerAnimated:YES];
        return NO;
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [[appDelegate dictStrings] valueForKey:@"loading"];
    [self.activityIndicatorView startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [[appDelegate dictStrings] valueForKey:@"feedback"];
    [self.activityIndicatorView stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if ([error code] != NSURLErrorCancelled) {
        NSLog(@"didFailLoadWithError %@",  error.localizedDescription);
        self.title = error.localizedDescription;
        [self.activityIndicatorView stopAnimating];
    }
}

- (void) handleBack:(id)sender
{
    // do your custom handler code here
    //NSLog(@"Back");
    
    [self.webView stringByEvaluatingJavaScriptFromString:@"clickPreviousLink()"];   
    
    // make sure you do this!
    // pop the controller
    //[self.navigationController popViewControllerAnimated:YES];
}

/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////

@end
