// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "VGViewController.h"

@interface VGOutputViewController : VGViewController

- (void)speakMsg:(NSString *)strSpeak;
- (void)pageMsg:(NSString *)strPage;

@property (nonatomic, retain) NSString *response;
@property (nonatomic, strong) AVAudioPlayer *pageAudioPlayer;
@property (nonatomic, strong) AVSpeechSynthesizer *speakSpeechSynthesizer;
@end
