// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "UIPlaceHolderTextView.h"
#import "VGOutputViewController.h"

@interface VGTypeViewController : VGOutputViewController <UITextViewDelegate> {
    UIPlaceHolderTextView *inputText;
    NSMutableArray *viewButtons;
}

@property (strong) UIPlaceHolderTextView *inputText;
@property (strong) NSMutableArray *viewButtons;

@end
