// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

//  http://stackoverflow.com/questions/8869445/how-can-i-use-mqtt-in-ios


#import <AVFoundation/AVFoundation.h>
#import "GAI.h"
#import "GAIDictionaryBuilder.h"
#import "UIDevice+Identifier.h"
#import "UIDevice+Orientation.h"
#import "UIImage+WinstonLib.h"
#import "UIViewController+MaxFrame.h"

#import "iToast.h"

#import "VGAppDelegate.h"
#import "VGCardsViewController.h"
#import "VGTypeViewController.h"
#import "VGConfigurationViewController.h"

@interface VGCardsViewController ()

@end

@implementation VGCardsViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.view = [[UIView alloc] init];
    
    [self buildView];

    [super viewWillAppear:animated];
}
/////////////////////////////////////////////////////////////////////////////////////////

- (void)buildView
{    
    [super buildView];
    
    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [[appDelegate dictStrings] valueForKey:@"card"];

    NSMutableArray *dictionary = [self readDictionary];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.cardsView = [self createCards:dictionary displaying:(int)[defaults integerForKey:@"prefNumberOfCards"]];
    [self.view addSubview: self.cardsView];
    self.selectedCard = nil;
    
    // create buttons
    self.viewButtons = [[NSMutableArray alloc] init];
    
    UIButton *talkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [talkButton addTarget:self action:@selector(speak:) forControlEvents:UIControlEventTouchUpInside];
    [talkButton setImage:[UIImage winstonLibImageNamed:@"ic_talk.png"] forState:UIControlStateNormal];
    [self.viewButtons addObject: talkButton];
    
    UIButton *typeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [typeButton addTarget:self action:@selector(type:) forControlEvents:UIControlEventTouchUpInside];
    [typeButton setImage:[UIImage winstonLibImageNamed:@"ic_type.png"] forState:UIControlStateNormal];
    [self.viewButtons addObject: typeButton];
    

    UIButton *configurationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [configurationButton addTarget:self action:@selector(configuration:) forControlEvents:UIControlEventTouchUpInside];
    [configurationButton setImage:[UIImage winstonLibImageNamed:@"ic_settings.png"] forState:UIControlStateNormal];
    [self.viewButtons addObject: configurationButton];

    // update buttons and add to view
    for (UIButton *button in self.viewButtons) {
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        
        [button setBackgroundColor:[UIColor whiteColor]];
        [button.layer setBorderColor:[UIColor grayColor].CGColor];
        [button.layer setBorderWidth:1.5f];
        [button.layer setCornerRadius:10.0f];
        
        [self.view addSubview:button];
    }
    
}

- (void)layoutView {
    [super layoutView];
    
    CGRect screenRect = [self maxUsableFrame];
    
    // set drawing rectangles
    CGRect inputRect = screenRect;
    inputRect.origin.y += 10.0;
    inputRect.origin.x += 10.0;
    inputRect.size.height -= 20.0;
    inputRect.size.width -= 20.0;
    
    CGRect buttonsRect = screenRect;
    CGFloat buttonWidth = 50.0;
    buttonsRect.size.width = buttonWidth + 10.0;
    inputRect.size.width -= buttonsRect.size.width;
    buttonsRect.origin.x = inputRect.origin.x + inputRect.size.width;
    
    self.cardsView.frame = inputRect;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [self layoutCards:(UIScrollView *)self.cardsView displaying:(int)[defaults integerForKey:@"prefNumberOfCards"]];
    
    // place buttons
    CGFloat buttonHeight = 50.0;//((buttonsRect.size.height - 10.0) / self.viewButtons.count) - 10.0;
    buttonsRect.origin.y += 10.0;
    buttonsRect.origin.x += 10.0;
    buttonsRect.size.height -= 20.0;
    
    int i = 0;
    for (UIButton *button in self.viewButtons) {
        button.frame = CGRectMake(buttonsRect.origin.x, buttonsRect.origin.y + (i * (buttonHeight + 10.0)), buttonWidth, buttonHeight);
        
        //CGSize sizeLabel = [[button titleForState:UIControlStateNormal] sizeWithFont:button.titleLabel.font];
        NSDictionary *attributes = @{ NSFontAttributeName: button.titleLabel.font};
        CGSize sizeLabel = [[button titleForState:UIControlStateNormal] sizeWithAttributes:attributes];
        
        CGFloat imageEdgeInsetX = -1 * (buttonWidth - sizeLabel.width - (button.imageView.image.size.width) - 15.0);
        [button setImageEdgeInsets:UIEdgeInsetsMake(0, imageEdgeInsetX, 0, 0)];
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
        i++;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////

- (UIView *)createCards:(NSArray *)dictionary displaying:(int)countDisplayed {
    //CGRect screenRect=[[UIScreen mainScreen] applicationFrame];
    UIScrollView *scrollView=[[UIScrollView alloc] init];//WithFrame:screenRect];
    scrollView.canCancelContentTouches = YES;

    for (NSString *dictionaryEntry in dictionary) {
        UIView *card = [self createCard:dictionaryEntry];
        [scrollView addSubview:card];
    }
    [self layoutCards:scrollView displaying:countDisplayed];
    return scrollView;
    
}

- (void)layoutCards:(UIScrollView *)parentView displaying:(int)countDisplayed {

    CGRect screenRect = self.cardsView.frame;//[self maxUsableFrame];
    int countRows = 1;
    int countCols = 1;
    if (countDisplayed > 1 && countDisplayed < 4) {
        if (screenRect.size.width > screenRect.size.height) {
            countCols = countDisplayed;
            countRows = 1;
        } else {
            countCols = 1;
            countRows = countDisplayed;
        }
    } else if (countDisplayed < 9) {
        countCols = 2;
        countRows = (countDisplayed + 1 ) / countCols;
    } else {
        countCols = 3;
        countRows = (countDisplayed + 1 ) / countCols;
    }
    
    CGFloat cardSpacing = 5.0;
    CGFloat cardHeight = ((int)((screenRect.size.height - cardSpacing) / countRows)) - cardSpacing;
    CGFloat cardWidth = ((int)((screenRect.size.width - cardSpacing) / countCols)) - cardSpacing;
    
    parentView.contentSize=CGSizeMake(countCols * (cardWidth + cardSpacing) + cardSpacing, ((parentView.subviews.count / countCols) + 1)  * (cardHeight + cardSpacing) + cardSpacing);
    
    UIImage *img = [[UIImage winstonLibImageNamed:@"btn_card.9.jpg"]stretchableImageWithLeftCapWidth:16 topCapHeight:20];
    UIGraphicsBeginImageContext(CGSizeMake(cardWidth, cardHeight));
    [img drawInRect:CGRectMake(0, 0, cardWidth, cardHeight)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    int i = 0;
    for (UIView *card in parentView.subviews) {
        CGRect rect = CGRectMake((i % countCols) * (cardWidth + cardSpacing) + cardSpacing, (i / countCols) * (cardHeight + cardSpacing) + cardSpacing, cardWidth, cardHeight);
        card.frame = rect;
        card.backgroundColor = [UIColor colorWithPatternImage:newImage];
        i++;
    }
    
}

- (UILabel *)createCard:(NSString *)title {
    UILabel *card = [[UILabel alloc] init];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHandler:)];
    tapGesture.cancelsTouchesInView = NO;        
    [card addGestureRecognizer:tapGesture];
/*** WinstonPro feature
    UILongPressGestureRecognizer *longpressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressHandler:)];
    longpressGesture.minimumPressDuration = 1;
    longpressGesture.cancelsTouchesInView = NO;        
    [card addGestureRecognizer:longpressGesture];
***/
    card.text = title;
    card.numberOfLines = 0; // Dynamic number of lines
    card.lineBreakMode = NSLineBreakByWordWrapping;
    card.textAlignment = NSTextAlignmentCenter;
    card.textColor = [UIColor blackColor];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    card.font = [UIFont systemFontOfSize:[UIFont systemFontSize]*[defaults floatForKey:@"prefCardsTextSize"]];
    
    card.userInteractionEnabled = YES;
    return card;
}

- (void)tapHandler:(UITapGestureRecognizer *)recognizer {
     if (recognizer.state == UIGestureRecognizerStateEnded) {
        UIColor *selectedColor =[[UIColor alloc] initWithRed:247.0/255.0 green:154.0/255.0 blue:45.0/255.0 alpha:1.0];
        UILabel *card = (UILabel *)recognizer.view;
        [self speakMsg: [card text]];
        
        if (self.selectedCard != nil) {
            self.selectedCard.textColor = [UIColor blackColor];
        }
        self.selectedCard = card;
        self.selectedCard.textColor = selectedColor;

        NSMutableDictionary *event =
        [[GAIDictionaryBuilder createEventWithCategory:@"Cards"
                                                action:@"Speak"
                                                 label:[card text]
                                                 value:nil] build];
        [[GAI sharedInstance].defaultTracker send:event];
    }
 }

- (void)longPressHandler:(UILongPressGestureRecognizer *)recognizer {
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        NSString *text = [(UILabel *)recognizer.view text];
        [self pageMsg:text];
        
        NSMutableDictionary *event =
        [[GAIDictionaryBuilder createEventWithCategory:@"Cards"
                                                action:@"Page"
                                                 label:text
                                                 value:nil] build];
        [[GAI sharedInstance].defaultTracker send:event];
    }
}

- (NSMutableArray *)readDictionary {
    NSMutableArray *cards = [[NSMutableArray alloc] init];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    if ([[defaults stringForKey:@"prefSetting"]isEqualToString:@"custom"]) {
        for (NSString *entry in [defaults arrayForKey:@"prefCustomEntries"]) {
            if ([entry length] > 0) {
                [cards addObject: entry];
            }
        }
    } else {
        NSString *settingRole = [[NSString alloc] initWithFormat:@"%@_%@", [defaults stringForKey:@"prefSetting"], [defaults stringForKey:@"prefRole"]];
        VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
        //NSLog(@"sr=%@",settingRole);
        NSArray *entries = [[appDelegate dictStringArrays] valueForKey:settingRole];
        //NSLog(@"en=%@",entries);
        for (NSString *entry in entries) {
            [cards addObject: [[appDelegate inputDictionary] valueForKey:entry]];
        }
    }
    return cards;
}

- (void)speak:(UIControlEvents *) sender {
    if (self.selectedCard != nil) {
        [self speakMsg: self.selectedCard.text];
        
        NSMutableDictionary *event =
        [[GAIDictionaryBuilder createEventWithCategory:@"Cards"
                                                action:@"Speak"
                                                 label:self.selectedCard.text
                                                 value:nil] build];
        [[GAI sharedInstance].defaultTracker send:event];
    }
}

- (void)configuration:(UIControlEvents *) sender {
    UIViewController *viewController = [[VGConfigurationViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:TRUE];
    
    NSMutableDictionary *event =
    [[GAIDictionaryBuilder createEventWithCategory:@"Cards"
                                            action:@"Configuration"
                                             label:nil
                                             value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
}

- (void)type:(UIControlEvents *) sender {
    UIViewController *viewController = [[VGTypeViewController alloc] init];
    [self.navigationController pushViewController:viewController animated:TRUE];
    
    NSMutableDictionary *event =
    [[GAIDictionaryBuilder createEventWithCategory:@"Cards"
                                            action:@"Type"
                                             label:nil
                                             value:nil] build];
    [[GAI sharedInstance].defaultTracker send:event];
    
}

/////////////////////////////////////////////////////////////////////////////////////////////

@end
