// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>
#import "Request.pb.h"

@class VGRequestView;

@interface VGRequestCell : UITableViewCell {
	VGRequestView *requestView;
}

- (void)setRequestEntry:(RequestEntry *)newRequestEntry;
- (UIButton *) getAckButton;
- (UIButton *) getCloseButton;

@property (nonatomic, retain) VGRequestView *requestView;

- (void)redisplay;

@end
