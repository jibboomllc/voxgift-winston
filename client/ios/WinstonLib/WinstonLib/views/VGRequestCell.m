// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.
    
#import "Request.pb.h"
#import "VGRequestCell.h"
#import "VGRequestView.h"
//#import "CustomTableViewCellAppDelegate.h"


@implementation VGRequestCell

@synthesize requestView;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {

	if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
		
		// Create a time zone view and add it as a subview of self's contentView.
		CGRect rFrame = CGRectMake(0.0, 0.0, self.contentView.bounds.size.width, self.contentView.bounds.size.height);
		requestView = [[VGRequestView alloc] initWithFrame:rFrame];
		requestView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		[self.contentView addSubview:requestView];
	}
	return self;
}


- (void)setRequestEntry:(RequestEntry *)newRequestEntry {
	// Pass the time zone wrapper to the view
	requestView.requestEntry = newRequestEntry;
}


- (UIButton *) getAckButton {
    return requestView.ackButton;
}


- (UIButton *) getCloseButton {
    return requestView.closeButton;
}

- (void)redisplay {
	[requestView setNeedsDisplay];
}


- (void)dealloc {
	requestView = nil;
//    [super dealloc];
}


@end
