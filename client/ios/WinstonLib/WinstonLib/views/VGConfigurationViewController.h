// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>
#import "VGViewController.h"

@interface VGConfigurationViewController : VGViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) NSDictionary *dictionaryOptions;
@property (nonatomic, retain) NSDictionary *ioOptions;
@property (nonatomic, retain) UITableView *theTableView;


@end
