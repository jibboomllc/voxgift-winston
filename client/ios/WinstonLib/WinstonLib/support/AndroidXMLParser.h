// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <Foundation/Foundation.h>

@interface AndroidXMLParser : NSObject <NSXMLParserDelegate> {
    NSMutableString *currentElementKey;
    NSMutableString *currentElementValue;
    
    NSMutableDictionary *values;
}

@property (nonatomic, retain) NSMutableDictionary *values;

- (AndroidXMLParser *) initXMLParser;

@end