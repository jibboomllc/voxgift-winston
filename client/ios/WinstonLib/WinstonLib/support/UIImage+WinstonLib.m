// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

// Ref: http://iphone.galloway.me.uk/iphone-sdktutorials/ios-library-with-resources/

#include <UIKit/UIKit.h>

#import "UIImage+WinstonLib.h"

@implementation UIImage (WinstonLib)

+ (UIImage*)winstonLibImageNamed:(NSString*)name {
    UIImage *imageFromMainBundle = [UIImage imageNamed:name];
    if (imageFromMainBundle) {
        return imageFromMainBundle;
    }
    NSBundle* bundle = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"WinstonLibRes" withExtension:@"bundle"]];

    NSString *filePath = [[NSString alloc] initWithFormat: @"%@/res/drawable/%@",[bundle resourcePath], name];
    UIImage *imageFromWinstonLibraryBundle = [UIImage imageWithContentsOfFile:filePath];
    return imageFromWinstonLibraryBundle;
}

@end