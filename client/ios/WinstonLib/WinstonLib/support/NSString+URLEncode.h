// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

@interface NSString (NSString_Extended)
@property (nonatomic, readonly) NSString *urlencode;
@end
