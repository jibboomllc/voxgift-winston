// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "StringArraysXMLParser.h"

@implementation StringArraysXMLParser

- (StringArraysXMLParser *) initXMLParser {
    self = [super initXMLParser];
    
    inArray = FALSE;
    arrayValues = [[NSMutableArray alloc] init];

    return self;
}

// Parse the start of an element
// Implement method called NSXMLParser when it hits the start of an element:

- (void)parser:(NSXMLParser *)parser 
didStartElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName 
    attributes:(NSDictionary *)attributeDict {
	
    if ([elementName isEqualToString:@"string-array"]) {
        currentElementKey = [attributeDict objectForKey:@"name"];
        inArray = TRUE;
    }
}

// Parse an element value
// Implement method called NSXMLParser when it hits an element value. In this method we capture the element value into currentElementValue ad hoc string:

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if (!currentElementValue) {
        // init the ad hoc string with the value     
        currentElementValue = [[NSMutableString alloc] initWithString:string];
    } else {
        // append value to the ad hoc string    
        [currentElementValue appendString:string];
    }
}  

// Parse the end of an element
// Implement method called NSXMLParser when it hits the end of an element:

 - (void)parser:(NSXMLParser *)parser 
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"resources"]) {
        // We reached the end of the XML document
        return;
    }
    
    if ([elementName isEqualToString:@"string-array"]) {
        NSString *trimmedKey = [currentElementKey stringByTrimmingCharactersInSet:
                                   [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [values addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:arrayValues, trimmedKey, nil]]; 
        //NSLog(@"arrayValues: %@", arrayValues);
        //[arrayValues removeAllObjects];
        arrayValues = [[NSMutableArray alloc] init];
        

        inArray = FALSE;
    } else if ([elementName isEqualToString:@"item"] && inArray) {
        NSString *trimmedString = [currentElementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [arrayValues addObject:trimmedString];
    }
    
    currentElementValue = nil;
}

@end
