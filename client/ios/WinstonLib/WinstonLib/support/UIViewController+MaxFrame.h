// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>

@interface UIViewController (MaxFrame)
@property (nonatomic, readonly) CGRect maxUsableFrame;
@end
