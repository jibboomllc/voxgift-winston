// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <Foundation/Foundation.h>
#import "AndroidXMLParser.h"

@interface StringArraysXMLParser : AndroidXMLParser {
    BOOL inArray;
    NSMutableArray *arrayValues;
}

- (StringArraysXMLParser *) initXMLParser;

@end