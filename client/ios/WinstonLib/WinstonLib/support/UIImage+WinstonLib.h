// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

// Ref: http://iphone.galloway.me.uk/iphone-sdktutorials/ios-library-with-resources/

#include <UIKit/UIKit.h>

@interface UIImage (WinstonLib)
+ (UIImage*)winstonLibImageNamed:(NSString*)name;
@end

