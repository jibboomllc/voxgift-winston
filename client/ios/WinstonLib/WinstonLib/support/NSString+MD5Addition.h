// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <Foundation/Foundation.h>

@interface NSString(MD5Addition)

- (NSString *) stringFromMD5;
//@property (nonatomic, readonly) (NSString *) stringFromMD5;

@end
