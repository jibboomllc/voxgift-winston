// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#include <UIKit/UIKit.h>

@interface UIDevice (Orientation)
@property (nonatomic, readonly) BOOL isLandscape;
@end
