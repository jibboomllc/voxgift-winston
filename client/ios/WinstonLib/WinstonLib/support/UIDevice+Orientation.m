// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "UIDevice+Orientation.h"

@implementation UIDevice (Orientation)
- (BOOL) isLandscape
{
    return
    (self.orientation == UIDeviceOrientationLandscapeLeft) ||
    (self.orientation == UIDeviceOrientationLandscapeRight);
}
@end
