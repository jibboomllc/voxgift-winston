// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

@interface NSString (MD5)
@property (nonatomic, readonly) NSString *stringFromMD5;
@end
