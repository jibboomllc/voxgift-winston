// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "UIViewController+MaxFrame.h"

// Extension to UIViewController to return the maxiumum usable frame size for a view
@implementation UIViewController (MaxFrame)

- (CGRect) maxUsableFrame {
    
    static CGFloat const kNavigationBarPortraitHeight = 64; // 44
//    static CGFloat const kNavigationBarLandscapeHeight = 64; // 34
    static CGFloat const kToolBarHeight = 49;
    
    // Start with the screen size minus the status bar if present
    CGRect maxFrame = [UIScreen mainScreen].applicationFrame;
   /*
    // If the orientation is landscape left or landscape right then swap the width and height
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
        CGFloat temp = maxFrame.size.height;
        maxFrame.size.height = maxFrame.size.width;
        maxFrame.size.width = temp;

        temp = maxFrame.origin.x;
        maxFrame.origin.x = maxFrame.origin.y;
        maxFrame.origin.x = temp;
    }
 */
    // Remove the status bar height
    maxFrame.origin.x = 0.0;
    maxFrame.origin.y = 0.0;
    
    // Take into account if there is a navigation bar present and visible (note that if the NavigationBar may
    // not be visible at this stage in the view controller's lifecycle.  If the NavigationBar is shown/hidden
    // in the loadView then this provides an accurate result.  If the NavigationBar is shown/hidden using the
    // navigationController:willShowViewController: delegate method then this will not be accurate until the
    // viewDidAppear method is called.
    if (self.navigationController) {
        if (self.navigationController.navigationBarHidden == NO) {
 /*
            // Depending upon the orientation reduce the height accordingly
            if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)) {
                maxFrame.size.height -= kNavigationBarLandscapeHeight;
                maxFrame.origin.y += kNavigationBarLandscapeHeight;
            }
            else {
*/
                maxFrame.size.height -= kNavigationBarPortraitHeight;
                maxFrame.origin.y += kNavigationBarPortraitHeight;
//            }
        }
    }
    
    // Take into account if there is a toolbar present and visible
    if (self.tabBarController) {
        if (!self.tabBarController.view.hidden) maxFrame.size.height -= kToolBarHeight;
    }
    return maxFrame;
}

@end
