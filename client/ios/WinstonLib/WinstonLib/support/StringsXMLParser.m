// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "StringsXMLParser.h"

@implementation StringsXMLParser

- (StringsXMLParser *) initXMLParser {
    self = [super initXMLParser];
    return self;
}

// Parse the start of an element
// Implement method called NSXMLParser when it hits the start of an element:

- (void)parser:(NSXMLParser *)parser 
didStartElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qualifiedName 
    attributes:(NSDictionary *)attributeDict {
	
    if ([elementName isEqualToString:@"string"]) {
        currentElementKey = [attributeDict objectForKey:@"name"];
    }
}

// Parse an element value
// Implement method called NSXMLParser when it hits an element value. In this method we capture the element value into currentElementValue ad hoc string:

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if (!currentElementValue) {
        // init the ad hoc string with the value     
        currentElementValue = [[NSMutableString alloc] initWithString:string];
    } else {
        // append value to the ad hoc string    
        [currentElementValue appendString:string];
    }
}  

// Parse the end of an element
// Implement method called NSXMLParser when it hits the end of an element:

- (void)parser:(NSXMLParser *)parser 
 didEndElement:(NSString *)elementName
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"resources"]) {
        // We reached the end of the XML document
        return;
    }
    
    if ([elementName isEqualToString:@"string"]) {
        NSString *trimmedKey = [currentElementKey stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        NSString *trimmedValue = [currentElementValue stringByTrimmingCharactersInSet:
                                  [NSCharacterSet whitespaceAndNewlineCharacterSet]];
        [values addEntriesFromDictionary:[NSDictionary dictionaryWithObjectsAndKeys:trimmedValue, trimmedKey, nil]]; 
    }
    
    currentElementValue = nil;
}

@end
