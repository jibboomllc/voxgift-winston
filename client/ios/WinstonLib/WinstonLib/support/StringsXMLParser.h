// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <Foundation/Foundation.h>
#import "AndroidXMLParser.h"

@interface StringsXMLParser : AndroidXMLParser {
}

- (StringsXMLParser *) initXMLParser;

@end