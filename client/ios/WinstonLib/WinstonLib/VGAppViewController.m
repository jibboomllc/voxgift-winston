// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import "UIDevice+Orientation.h"
#import "UIImage+WinstonLib.h"
#import "UIViewController+MaxFrame.h"

#import "VGAppDelegate.h"
#import "VGAppViewController.h"
#import "VGCardsViewController.h"
#import "VGConfigurationViewController.h"
#import "VGFeedbackViewController.h"
#import "VGRequestsViewController.h"
#import "VGTypeViewController.h"


@interface __VGTableViewCell: NSObject
@property NSString *text;
@property NSString *detailText;
@property UIImage *image;
@property UIViewController *viewController;
@end

@implementation __VGTableViewCell
@synthesize text;
@synthesize detailText;
@synthesize image;
@synthesize viewController;
@end

@implementation VGAppViewController
@synthesize theTableView;
@synthesize appTitle;
@synthesize buttonFlags;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.buttonFlags = [[NSMutableDictionary alloc] init];
        [self.buttonFlags setObject:[NSNumber numberWithBool:YES] forKey:@"cards"];
        [self.buttonFlags setObject:[NSNumber numberWithBool:YES] forKey:@"type"];
        [self.buttonFlags setObject:[NSNumber numberWithBool:YES] forKey:@"requests"];
        [self.buttonFlags setObject:[NSNumber numberWithBool:YES] forKey:@"feedback"];
        [self.buttonFlags setObject:[NSNumber numberWithBool:YES] forKey:@"configuration"];
        
        self.appTitle = [NSMutableString stringWithFormat:@"foo"];
    }
    return self;
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBarHidden = YES;
    
    [self buildView];
}

- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;  
    [self layoutView];
}
/////////////////////////////////////////////////////////////////////////////////////////////

- (void)buildView
{    
    [super buildView];

    VGAppDelegate *appDelegate = (VGAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.title = [[appDelegate dictStrings] valueForKey:@"back"];
    
    // create title
    UILabel *appTitleLabel = [[UILabel alloc] init];
    appTitleLabel.text = [self appTitle];
    appTitleLabel.font = [UIFont boldSystemFontOfSize:200.0];
    appTitleLabel.numberOfLines = 1;
    //appTitleLabel.minimumScaleFactor = 20.0;
    appTitleLabel.adjustsFontSizeToFitWidth = YES;
    appTitleLabel.textColor = [[UIColor alloc] initWithRed:130.0/255.0 green:130.0/255.0 blue:130.0/255.0 alpha:1.0];    
    appTitleLabel.backgroundColor = [UIColor clearColor];
    [appTitleLabel setBaselineAdjustment:UIBaselineAdjustmentAlignCenters];
    CGRect screenRectUR = [UIScreen mainScreen].applicationFrame;
    appTitleLabel.frame = CGRectMake(screenRectUR.size.width * 0.05, (screenRectUR.size.width * (0.025)), screenRectUR.size.width * 0.5, screenRectUR.size.width * 0.2);
    [self.view addSubview:appTitleLabel];
    
    // create navigation panel
    theTableView = [[UITableView alloc] init];
    theTableView.delegate = self;
    theTableView.dataSource = self;
    theTableView.backgroundColor = [UIColor clearColor];
    
    [self.view addSubview:theTableView];
    
    self.navigationOptions = [[NSMutableArray alloc] init];    
    
    if ([[self.buttonFlags objectForKey:@"cards"] boolValue]) {
        __VGTableViewCell *cardsOption = [[__VGTableViewCell alloc] init];
        [cardsOption setText:[[appDelegate dictStrings] valueForKey:@"card"]];
        [cardsOption setDetailText:[[appDelegate dictStrings] valueForKey:@"card_button_prompt"]];
        [cardsOption setImage:[UIImage winstonLibImageNamed:@"ic_card.png"]];
        [cardsOption setViewController: [[VGCardsViewController alloc] init]];
        
        [self.navigationOptions addObject: cardsOption];            
    }
    
    if ([[self.buttonFlags objectForKey:@"type"] boolValue]) {
        __VGTableViewCell *typeOption = [[__VGTableViewCell alloc] init];
        [typeOption setText:[[appDelegate dictStrings] valueForKey:@"type"]];
        [typeOption setDetailText:[[appDelegate dictStrings] valueForKey:@"type_button_prompt"]];
        [typeOption setImage:[UIImage winstonLibImageNamed:@"ic_type.png"]];
        [typeOption setViewController: [[VGTypeViewController alloc] init]];
        
        [self.navigationOptions addObject: typeOption];
    }
    
    if ([[self.buttonFlags objectForKey:@"requests"] boolValue]) {
        __VGTableViewCell *requestsOption = [[__VGTableViewCell alloc] init];
        [requestsOption setText:[[appDelegate dictStrings] valueForKey:@"requests"]];
        [requestsOption setDetailText:[[appDelegate dictStrings] valueForKey:@"requests_button_prompt"]];
        [requestsOption setImage:[UIImage winstonLibImageNamed:@"ic_pagelog.png"]];
        [requestsOption setViewController: [[VGRequestsViewController alloc] init]];
        
        [self.navigationOptions addObject: requestsOption];            
    }
    
    if ([[self.buttonFlags objectForKey:@"feedback"] boolValue]) {
        __VGTableViewCell *configurationOption = [[__VGTableViewCell alloc] init];
        [configurationOption setText:[[appDelegate dictStrings] valueForKey:@"feedback"]];
        [configurationOption setDetailText:[[appDelegate dictStrings] valueForKey:@"feedback_button_prompt"]];
        [configurationOption setImage:[UIImage winstonLibImageNamed:@"ic_feedback.png"]];
        [configurationOption setViewController: [[VGFeedbackViewController alloc] init]];
        
        [self.navigationOptions addObject: configurationOption];
    }
    
    if ([[self.buttonFlags objectForKey:@"configuration"] boolValue]) {
        __VGTableViewCell *configurationOption = [[__VGTableViewCell alloc] init];
        [configurationOption setText:[[appDelegate dictStrings] valueForKey:@"settings"]];
        [configurationOption setDetailText:[[appDelegate dictStrings] valueForKey:@"settings_button_prompt"]];
        [configurationOption setImage:[UIImage winstonLibImageNamed:@"ic_settings.png"]];
        [configurationOption setViewController: [[VGConfigurationViewController alloc] init]];
        
        [self.navigationOptions addObject: configurationOption];
    }
    
    [self layoutView: screenRectUR.size.width * 0.2];
    
}

-(void) layoutView: (float) offset {
    [super layoutView];
    
    CGRect screenRect = [self maxUsableFrame];
    
    // place buttons - assumes all are visible
    CGRect cellFrame = [theTableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    cellFrame.size.height = (cellFrame.size.height == 0.0) ? 44.0 : cellFrame.size.height;
    CGFloat tableHeight = [self.navigationOptions count] * cellFrame.size.height;
    
    if (offset < 0.0) { // bottom align table
        theTableView.frame = CGRectMake(0.0, screenRect.size.height - tableHeight, screenRect.size.width, tableHeight);
    } else {
        theTableView.frame = CGRectMake(0.0, offset, screenRect.size.width, tableHeight);
    }
    
}

/////////////////////////////////////////////////////////////////////////////////////////////

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.navigationOptions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    __VGTableViewCell *cellData = [self.navigationOptions objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [cellData text];
    cell.detailTextLabel.text = [cellData detailText];
    cell.imageView.image = [cellData image];

    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
	UIViewController *viewController = [[self.navigationOptions objectAtIndex:indexPath.row] viewController];
	
	self.navigationController.navigationBarHidden = NO;  
	[self.navigationController pushViewController:viewController animated:TRUE];
}

/////////////////////////////////////////////////////////////////////////////////////////////

@end

