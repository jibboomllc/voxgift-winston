// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>

@interface VGAppDelegate : UIResponder <UIApplicationDelegate>

- (void)readAllResources;
- (NSString *)checkString : (NSString *) inStr;
- (NSString *)setInputLanguage : (NSString *) inLanguage;
- (NSString *)setOutputLanguage : (NSString *) outLanguage;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navController;

@property (strong, nonatomic) NSBundle *libResourceBundle;
@property (strong, nonatomic) NSMutableDictionary *dictStrings;
@property (strong, nonatomic) NSMutableDictionary *dictStringArrays;

@property (strong, nonatomic) NSMutableDictionary *inputDictionary;
@property (strong, nonatomic) NSMutableDictionary *outputDictionary;
@end
