// Copyright 2011-2012 VoxGift, Inc.
// All rights reserved.

#import <UIKit/UIKit.h>
#import "VGViewController.h"

@interface VGAppViewController : VGViewController  <UITableViewDataSource, UITableViewDelegate> {
    
    NSMutableString *appTitle;
    NSMutableDictionary *buttonFlags;
    
}

@property (strong) NSMutableString *appTitle;
@property (strong) NSMutableDictionary *buttonFlags;
@property (strong) NSMutableArray *navigationOptions;

@property (nonatomic, retain) UITableView *theTableView;
@end
